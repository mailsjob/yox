<%@page import="org.springframework.security.core.userdetails.*"%>
<%@page import="org.springframework.security.core.*"%>
<%@page import="org.springframework.security.web.*"%>
<%@page import="org.springframework.security.authentication.*"%>
<%@page import="lab.s2jh.core.web.captcha.BadCaptchaException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8" />
<title>未来付-用户注册</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="MobileOptimized" content="320">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="${base}/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${base}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${base}/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="${base}/assets/plugins/select2/select2_metro.css" />

<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="${base}/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
<link href="${base}/assets/css/style.css" rel="stylesheet" type="text/css" />
<link href="${base}/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
<link href="${base}/assets/css/plugins.css" rel="stylesheet" type="text/css" />
<link href="${base}/assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color" />

<link rel="stylesheet" type="text/css"
	href="${base}/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
<link rel="stylesheet" type="text/css"
	href="${base}/assets/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/plugins/bootstrap-editable/inputs-ext/address/address.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/plugins/bootstrap-toastr/toastr.min.css" />

<!-- <link rel="stylesheet" type="text/css" href="${base}/assets/plugins/jquery-ui/redmond/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" type="text/css" href="${base}/assets/extras/jquery-jqgrid/plugins/ui.multiselect.css">
<link rel="stylesheet" type="text/css" href="${base}/assets/extras/jquery-jqgrid/css/ui.jqgrid.css">
<link rel="stylesheet" type="text/css" href="${base}/assets/app/bootstrap-jqgrid.css" /> -->

<link rel="stylesheet" type="text/css" href="${base}/assets/extras/kindeditor/themes/default/default.css">

<link rel="stylesheet" type="text/css" href="${base}/assets/extras/jquery-ztree/css/zTreeStyle/zTreeStyle.css">

<link rel="stylesheet" type="text/css" href="${base}/assets/extras/tooltipster/css/tooltipster.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/extras/tooltipster/css/themes/tooltipster-light.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/extras/tooltipster/css/themes/tooltipster-noir.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/extras/tooltipster/css/themes/tooltipster-punk.css" />
<link rel="stylesheet" type="text/css" href="${base}/assets/extras/tooltipster/css/themes/tooltipster-shadow.css" />

<link href="${base}/assets/css/pages/tasks.css" rel="stylesheet" type="text/css" />
<link href="${base}/assets/css/pages/search.css" rel="stylesheet" type="text/css" />
<link href="${base}/assets/css/pages/lock.css" rel="stylesheet" type="text/css" />

<link href="${base}/assets/app/custom.css?_=${buildVersion}" rel="stylesheet" type="text/css" />

<link href="${base}/pub/wlf-user-signin.css?_=${buildVersion}" rel="stylesheet" type="text/css" />

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="${base}/pub/favicon.ico" />
<style>
.minshen_mine {
	height: 32px;
	text-align: left;
}

.minshen_mine a {
	height: 30px;
	width: 90px;
	line-height: 30px;
	display: block;
	color: #fff;
	text-decoration: none;
	float: left;
	text-align: center;
}

.minshen_mine a:hover {
	background: url('${base}/resources/wlf/topItem.gif');
	height: 30px;
	width: 90px;
	color: #666;
}

.ldee_booth {
	width: 950px;
	overflow: hidden;
	margin: 0 auto;
}

.ldee_booth_header {
	height: 144px;
}

.ldee_booth_lay_center table {
	font-size: 12px;
	font-family: "微软雅黑", "黑体";
	border-top: solid 1px #f1f1f1;
	border-left: solid 1px #f1f1f1;
}

.ldee_booth_lay_center table td {
	padding: 5px;
	border-bottom: solid 1px #f1f1f1;
	border-right: solid 1px #f1f1f1;
}

.ledd_j {
	border: solid 1px #ddd;
	height: 20px;
	line-height: 20px;
	width: 340px;
}

.ledd_j2 {
	border: solid 1px #ddd;
	height: 20px;
	line-height: 20px;
	width: 270px;
}

.ldee_table {
	margin-top: 10px;
	color: #666;
}

.buttom_botht {
	width: 100px;
	height: 30px;
	border: none;
	line-height: 30px;
	background: #35aa47;
	float: left;
	margin-left: 10px;
	text-align: center;
	font-size: 14px;
	color: #FFFFFF;
	text-decoration: none;
}

.buttom_botht2 {
	width: 80px;
	height: 30px;
	line-height: 30px;
	background: #d8d8d8;
	float: left;
	margin-left: 10px;
	text-align: center;
	font-size: 14px;
	color: #333;
	text-decoration: none;
}

.textarea_booth {
	border: solid 1px #eee;
}

.ldee_booth_lay_center table td p {
	text-align: left;
}

.ldee_booth_zc {
	height: 110px;
	width: 20%;
	float: left;
	overflow: hidden;
}

.ldee_booth_zc_one {
	height: 45px;
	width: 145px;
	margin: 35px auto;
}

.ldee_booth_lay_top {
	overflow: hidden;
}

.ldee_booth_zc span {
	display: block;
	text-align: center;
	height: 45px;
	float: left;
	width: 80px;
	line-height: 45px;
	font-size: 18px;
	font-family: "微软雅黑", "宋体";
	color: #666;
}

.ldee_table p {
	line-height: 25px;
	color: #666;
}

.ldee_booth_zc img {
	float: left;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body style="background-color: #FFFFFF !important;">
	<div class="ldee_booth">
		<!-- BEGIN LOGO -->
		<div style="background:url('${base}/resources/wlf/top.jpg');height:145px;margin:0 auto;width:950px;">
			<div style="height: 87px;"></div>
			<div class="minshen_mine">
				<a style="margin-left: 10px;" href="http://creditcard.cmbc.com.cn/Index.aspx" target="_blank">首页 </a> <a
					href="http://creditcard.cmbc.com.cn/CreditCards/CardShow/cardshow.html" target="_blank">卡片展示</a> <a
					href="http://creditcard.cmbc.com.cn/zengzhifuwu/traveljy.aspx" target="_blank">功能服务 </a> <a
					href="http://creditcard.cmbc.com.cn/promotioninfo/index.aspx" target="_blank">最新动态 </a> <a
					href="http://creditcard.cmbc.com.cn/ReservationCenter/index.aspx" target="_blank">预订中心 </a> <a
					href="http://creditcard.cmbc.com.cn/shanghufenqi/fqfk/fqfk.html" target="_blank">分期付款 </a> <a
					href="http://shop.cmbc.com.cn/mall/" target="_blank">积分计划 </a> <a
					href="http://mall.cmbc.com.cn/webFrontIndexAction!index" target="_blank">网上商城 </a> <a
					href="http://creditcard.cmbc.com.cn/shanghufenqi/fqfk/fqfk.html" target="_blank">特惠商户 </a>
			</div>
		</div>
		<!-- END LOGO -->
		<!--第一步-->
		<form action="${base}/pub/wlf-user!doCreate" method="post" id="wlfForm" autocomplete="off">
			<div class="ldee_booth_lay" id="step1" align="center">
				<div class="ldee_booth_lay_top">
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boo_1.jpg" /><span>注册须知</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_2.jpg" /><span>填写资料</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_3.jpg" /><span>授信额度</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_4.jpg" /><span>阅读合约</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_5.jpg" /><span>完成注册</span>
						</div>
					</div>
				</div>
				<div class="ldee_booth_lay_center">
					<table width="950" class="ldee_table" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="3" style="padding: 20px;">

								<p align="left" class="style7">
									<strong>1．申请"未来付"需要具备什么样的条件？<br>
									</strong>如果您是全日制正规高等院校的本科、硕士或者博士在读生即可
								</p>
								<p align="left" class="style7">
									<strong>2. 若申请“未来付”，您应准备那些基本贷款申请资料<br>
									</strong>以下三项基本资料必须提供：<br> 1. 身份证<br> 2. 学生证<br> 3. 常用存折或常用银行卡信息清单（六个月以上）
								</p>
								<p align="left" class="style7">
									建议提供的资信证明如下：<br> 4. 借书卡、宿舍卡<br> 5. 成绩单、奖学金证明<br> 6. 班干部证明、社团活动证明<br> 7.
									各种荣誉证明、回报社会证书（如献血、义务支教等）<br> 8. 手机缴费、每年学费和住宿费扣款（或缴费）证明<br> 9．勤工助学证明<br> 10. 信用卡对账单、个人信用报告
								</p>
								<p align="left" class="style7">除了基本资料外，多提供一份资信证明，就多一项信用加分。</p>
								<p align="left" class="style7">
									<strong>3. 未来付可用于购买哪些商品？<br>
									</strong>未来付可用于未来付网站所列的所有商品。
								</p>
								<p align="left" class="style7">
									<strong>4. 您可获得的贷款额度是多少？<br>
									</strong>我们将根据您填写学籍基本信息自动计算出一个初始额度，如果您想获取更多额度，请填写更多信息，经过自动计算和审核后生效。
								</p>
								<p align="left" class="style7">
									<strong>5. 您可申请的还款期限？<br>
									</strong>所有贷款均在毕业半年以后开始分期偿还，每月偿还500元人民币。
								</p>
								<p align="left" class="style7">
									<strong>6. “未来付”需要支付的费用是怎样的？<br>
									</strong>“未来付”用户无需支付年费、账户管理费等其它任何费用，到期归还商品剩余未付款项即可。
								</p>
								<p align="left" class="style7">
									<strong>7. 申请后多久可以开通“未来付”？<br>
									</strong>在您的资料完整后，一般一周时间即可开通“未来付”服务。
								</p>

							</td>
						</tr>
					</table>
					<table width="950" border="0" cellspacing="0" cellpadding="0">
						<tr>

							<td height="50">
								<button id="toStep2" type="button" style="margin-left: 20px" class="buttom_botht">下一步</button>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<!--第二步-->
			<div class="ldee_booth_lay" id="step2" style="display: none" align="center">
				<div class="ldee_booth_lay_top">
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_1.jpg" /><span>注册须知</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boo_2.jpg" /><span>填写资料</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_3.jpg" /><span>授信额度</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_4.jpg" /><span>阅读合约</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_5.jpg" /><span>完成注册</span>
						</div>
					</div>
				</div>
				<div class="ldee_booth_lay_center">
					<table width="950" class="ldee_table" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="45" colspan="2" bgcolor="#f1f1f1" align="left">&nbsp;<strong>学籍信息验证</strong></td>
						</tr>
						<tr>
							<td width="110" height="45" align="right">学生姓名<span style="color: #F00">*</span>：
							</td>
							<td width="840" height="45" align="left"><input class="form-control" name="trueName" type="text" /></td>
						</tr>
						<tr>
							<td width="110" height="45" align="right">性别<span style="color: #F00">*</span>：
							</td>
							<td width="840" height="45" align="left">&nbsp;&nbsp;<s:radio name="sex" list="#{'M':'男','F':'女'}"></s:radio></td>
						</tr>
						<tr>
							<td height="45" align="right">身份证号<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><input class="form-control" name="idCard" type="text" /></td>
						</tr>
						<tr>
							<td height="45" align="right">院校<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><s:select cssClass="form-control" name="studentInfo.schoolInfo.id"
									list="schoolInfos" emptyOption="true"></s:select></td>
						</tr>
						<tr>
							<td height="45" align="right">院系<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><s:select cssClass="form-control" name="studentInfo.academyInfo.id" list="#{}"></s:select></td>
						</tr>
						<tr>
							<td height="45" align="right">专业<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><s:select cssClass="form-control" name="studentInfo.majorInfo.id" list="#{}"></s:select></td>
						</tr>
						<tr>
							<td width="100" height="45" align="right">班级：</td>
							<td width="450" height="45" align="left"><input class="form-control" name="banji" type="text"
								 /></td>
						</tr>
						<tr>
							<td height="45" align="right">层次<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><s:select cssClass="form-control" name="studentInfo.eduLevel"
									list="#application.enums.eduLevelEnum" emptyOption="true"></s:select></td>
						</tr>
						<tr>
							<td height="45" align="right">学制<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><s:select cssClass="form-control" name="studentInfo.degreeCategory"
									list="#{'2':'2年','3':'3年','4':'4年','5':'5年及以上'}" emptyOption="true"></s:select></td>
						</tr>
						<tr>
							<td height="45" align="right">学历类型<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><s:select cssClass="form-control" name="studentInfo.degreeMode"
									list="#{'TZ':'统招','CR':'成人','ZK':'自考'}" emptyOption="true"></s:select></td>
						</tr>
						<tr>
							<td height="45" align="right">入学日期<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><s3:datetextfield name="enterTime"></s3:datetextfield></td>
						</tr>
						<tr>
							<td height="45" align="right">验证码<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left">
								<div>
									<div style="float: left">
										<input class="form-control"  name="jcaptcha" id="jcaptcha"  type="text"
										onfocus="if($('#captchTips').is(':visible')){$('#captchTips').hide();$('#refreshCaptchImg').click()}" />
									</div>
									<div style="float: left">
			                        	<span id="captchTips">&nbsp;</span><span id="captchDisplay" style="display:none">
			                            <a href="javascript:void(0)" tabindex="-1" id="refreshCaptchImg"
			                            onclick="$('#captchDisplay').show();$('#jcaptchaImage').attr('src','${base}/pub/jcaptcha.servlet?_='+new Date().getTime())">
			                            <img align="middle" id="jcaptchaImage"/></a>
			                            </span>
			                          </div>
		                         </div>
                            </td>
						</tr>
						<tr>
							<td height="45" align="right">手机号<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left">
								<div class="input-group">
									<input class="form-control" name="mobilePhone" type="text" /> <span class="input-group-btn">
										<button type="button" class="btn green" id="getSmsCode">获取校验码</button> &nbsp;<span id="smsCodeResp"></span>
									</span>
								</div>
							</td>
						</tr>
						<tr>
							<td height="45" align="right">手机校验码<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><input class="form-control" name="checkCode" type="text" /></td>
						</tr>
						
					</table>
					<table width="950" class="ldee_table" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="45" colspan="2" bgcolor="#f1f1f1" align="left">&nbsp;<strong>设置未来付账号</strong></td>
						</tr>
						<tr>
							<td width="110" height="45" align="right">登录账号<span style="color: #F00">*</span>：
							</td>
							<td width="840" height="45" align="left"><input class="form-control" name="loginid" type="text"
								 /></td>
						</tr>
						<tr>
							<td height="45" align="right">输入登录密码<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><input class="form-control" name="password" type="password"  /></td>

						</tr>
						<tr>
							<td height="45" align="right">再次输入密码<span style="color: #F00">*</span>：
							</td>
							<td height="45" align="left"><input class="form-control" name="rpassword" type="password"  /></td>
						</tr>
						
					</table>
					<table width="950" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="45" align="left"><label style="margin-left: 20px"><input type="checkbox"
									name="step2Accept" />&nbsp; <span style="color: #F00">*</span>同意调用教育部接口查询个人信息 </label></td>
						</tr>
						<tr>
							<td height="50" align="left">
								<button id="backStep1" type="button" style="margin-left: 20px" class="buttom_botht">上一步</button>
								<button type="button" id="toStep3" class="buttom_botht">下一步</button>
							</td>
						</tr>
					</table>


				</div>
			</div>
			<!--第三步-->
			<div class="ldee_booth_lay" id="step3" style="display: none" align="center">
				<div class="ldee_booth_lay_top">
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_1.jpg" /><span>注册须知</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_2.jpg" /><span>填写资料</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boo_3.jpg" /><span>授信额度</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_4.jpg" /><span>阅读合约</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_5.jpg" /><span>完成注册</span>
						</div>
					</div>
				</div>
				<div class="ldee_booth_lay_center">
					<table width="950" class="ldee_table" border="0" cellspacing="0" cellpadding="0">
						</tr>
						<td height="30" colspan="3" bgcolor="#f1f1f1" align="left">&nbsp;<strong>基本授信额度</strong></td>
						</tr>
						<tr>
							<td height="30" colspan="3" align="left">系统根据你的学籍信息自动计算出你的基本授信额度为：<font size="22px" color="red"> <span
									id="respCredit"></span></font>(此额度值可通过后续进一步完善个人资料提升)
							</td>
						</tr>
					</table>
					<table width="950" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="50">
								<button id="backStep2" type="button" style="margin-left: 50px" class="buttom_botht">上一步</button>
								<button type="button" id="toStep4" class="buttom_botht">下一步</button>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<!--第四步-->
			<div class="ldee_booth_lay" id="step4" style="display: none" align="center">
				<div class="ldee_booth_lay_top">
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_1.jpg" /><span>注册须知</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_2.jpg" /><span>填写资料</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_3.jpg" /><span>授信额度</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boo_4.jpg" /><span>阅读合约</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_5.jpg" /><span>完成注册</span>
						</div>
					</div>
				</div>
				<div class="ldee_booth_lay_center">
					<table width="950" class="ldee_table" style="valign: center" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="30" colspan="3" style="padding: 10px; text-align: left;">

								<table border=0 style="border: none; font-family: 宋体">
									<tr>
										<td style="width: 80px; height: 30px; border: none;">甲方</td>
										<td style='width: 200px; border: none; border-bottom: solid windowtext 1.0pt; text-align: left'>北京未来付网络技术有限公司</td>
										<td style="width: 200px; border: none; text-align: left">（以下简称“甲方”）</td>
									</tr>
									<tr>
										<td style="width: 80px; height: 30px; border: none;">乙方</td>
										<td style='border: none; border-bottom: solid windowtext 1.0pt; text-align: left'>未来付申请人</td>
										<td style="width: 200px; border: none; text-align: left">（以下简称“乙方”）</td>
									</tr>
									<tr>
										<td style="width: 80px; height: 30px; border: none;">托管方</td>
										<td style='border: none; border-bottom: solid windowtext 1.0pt; text-align: left'>中国民生银行</td>
										<td style="width: 200px; border: none; text-align: left">（以下简称“托管方”）</td>
									</tr>
								</table>

								<p class=MsoNormal style='text-indent: 21.0pt'>
									<span lang=EN-US>&nbsp;</span>
								</p>

								<p class=MsoNormal style='text-indent: 21.0pt'>
									<span style='font-family: 宋体'>北京未来付网络技术有限公司（以下简称“甲方”）委托（以下简称“托管方”）向未来付申请人（以下简称“乙方”）就未来付的申请、使用和发放贷款用于支付未来付商城“未来付”部分消费额等相关事宜签订合约如下：</span>
								</p>

								<p class=MsoNormal style='text-indent: 21.0pt'>
									<span lang=EN-US>&nbsp;</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; text-indent: 0cm; line-height: 150%'>
									<b><span lang=EN-US style='font-family: "Times New Roman", "serif"'>第一条<span
											style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
										style='font-family: 宋体'>申请</span></b>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>1、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方保证向甲方提供的所有申请资料是正确、完整、真实的，同意甲方向任何有关方面（包括但不限于中国人民银行个人信用信息基础数据库、有关部门、单位或个人）了解和查询其财产、资信、个人信用信息等情况，并保留相关资料。甲方有权收集、处理、传递及应用乙方的个人相关资料。乙方同意甲方通过托管方向中国人民银行个人信用信息基础数据库报送其个人信用信息，如甲方出于为乙方提供与未来付有关服务的目的，乙方同意甲方将其个人资料及信用状况披露给甲方认为必须的第三方，包括但不限于甲方分支机构、控股子公司、甲方的服务机构、代理人、外包作业机构、合作方以及相关资信机构。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>2、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方如果发生姓名、国籍、住所地、学习单位、身份证或身份证明文件的号码、有效期等身份信息变更，应在合理期限内与甲方联系并办理相关资料变更手续，否则甲方将依法终止与乙方办理新的业务，由此产生的损失由乙方负责。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>3、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>甲方根据乙方的资信状况、个人相关资料等，有权决定是否批准乙方的未来付服务申请。无论申请是否获得批准，乙方提供的申请资料均不予退还。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>4、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方在使用未来付服务或未完全清偿未来付贷款期间不允许再进行其他借贷服务，否则未来付将视为乙方无完全能力清偿所有欠款，有权要求乙方立即一次性清偿所有欠款，并追究其违约责任。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>5、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方有义务按照托管方要求提供申请借贷所需的全部材料，并按照托管方要求配合审查，否则托管方有权不予放贷。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>6、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方因使用未来付而发生的各种收费款项和须承担的各种费用、利息均由甲方计入乙方未来付账户，乙方对该账户的债务无条件承担偿还责任。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>7、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方必须为全日制在校在读学生，如涉嫌虚假甲方拥有向乙方追责的权利。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; text-indent: 0cm; line-height: 150%'>
									<b><span lang=EN-US style='font-family: "Times New Roman", "serif"'>第二条<span
											style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
										style='font-family: 宋体'>授信额度</span></b>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>1、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>甲方有权根据《未来付偿还能力评估模型》中甲方认为有必要的的各项指标信息进行搜集、分析，并用以核定乙方的授信额度。甲方有权根据乙方情况的变化随时调整其授信额度，该调整一经甲方作出即对乙方具有约束力。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>2、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>甲方在对乙方评估做出授信额度（上限）后，托管方须根据授信额度以预授权形式向乙方发放，授信金额<u>元</u>。所授信的资金只可用于支付乙方在未来付商城购物时所产生的“未来付”部分款项，不可取现，不可支付乙方在未来付商城购物时所产生的“首付”部分款项。
									</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; text-indent: 0cm; line-height: 150%'>
									<b><span lang=EN-US style='font-family: "Times New Roman", "serif"'>第三条<span
											style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
										style='font-family: 宋体'>使用</span></b>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>1、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>甲方通过甲方指定的途径向乙方提供查询密码、交易密码。对于乙方密码保管不善造成的损失，由乙方自行承担。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>2、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>凡使用密码进行的交易均视为乙方本人所为，依据密码等电子信息为乙方提供的结算等各类交易，交易所产生的电子信息均为该项交易的有效凭据。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>3、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>未来付账户的有效期限根据乙方在读时限而定，毕业自动停止使用，但乙方使用未来付所发生的债权债务关系不因未来付账户的逾期失效而消灭。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>4、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方与特约商户发生交易纠纷时，应有双方自行协商解决，乙方不得以纠纷为由拒绝偿还因使用未来付而发生的债务。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>5、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方在毕业后由于继续深造需继续使用未来付账户的，需根据甲方所提供的申请渠道提交相应申请，经甲方核准后方可继续使用未来付账户。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>6、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方有义务保管好未来付账户、密码等信息，因保管不善将密码告知他人或供他人使用而造成的一切损失由乙方自行承担。若乙方故意将未来付账户出售、出租、转借或以其他方式交由他人使用视为违约。甲方有权收回未来付账户使用权，并由乙方承担由此造成的一切损失。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>7、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方在校期间如不想继续使用未来付账户，乙方应根据甲方所提供的渠道申请停用，在偿还所有欠款和利息并经由甲方确认后方可正式停用。凡正式停用前所发生经济损失均由乙方承担。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>8、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>未来付账户的所有权归属甲方，甲方有权依照国家有关规定收回或停用乙方账户，乙方应继续承担偿还全部欠款和利息的义务，且乙方未偿还的账款视为甲方收回或停用之日全部到期并一次清偿。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>9、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>甲方有权随时因其认定的正当理由或风险管控因素，取消乙方的未来付交易，并中止或停止乙方使用未来付账户的权利。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>10、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>、乙方应保证在安全环境下于互联网上使用未来付服务，否则乙方须对非安全环境下使用未来付所导致的风险和损失自行负责。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; text-indent: 0cm; line-height: 150%'>
									<b><span lang=EN-US style='font-family: "Times New Roman", "serif"'>第四条<span
											style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
										style='font-family: 宋体'>利息</span></b>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>1、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方在未来付商城中未产生购物消费时不产生任何利息。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>2、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方在未来付商城成功发生交易并使用未来付服务后，即成功向甲方借贷，须开始支付借贷款额利息。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>3、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方在借贷发生后，须每月定期向银行支付贷款总额的利息部分，利息按照年利率</span><span
										lang=EN-US>3%</span><span style='font-family: 宋体'>进行计算。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>4、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>乙方须自行在甲方所提供的利息查询渠道中查询当月需归还利息数，并按时缴纳利息。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 21.25pt; text-indent: 0cm; line-height: 150%'>
									<span lang=EN-US style='font-family: "Times New Roman", "serif"'>5、<span
										style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span style='font-family: 宋体'>本合约中的收费项目或标准变化、利率调整等一经公布（包括但不限于网站公告、未来付账户、电子邮件、客户热线等方式），即为有效，无须另行通知乙方，修改后的条款对甲、乙、丙三方均由约束力。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; text-indent: 0cm; line-height: 150%'>
									<b><span lang=EN-US style='font-family: "Times New Roman", "serif"'>第五条<span
											style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
										style='font-family: 宋体'>还款</span></b>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>乙方自毕业后之日起次月开始分期还款，否则，自乙方应还款之日起按日利率万分之五记收违约金至清偿日止，甲方按月记收复利。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>乙方消费信息应在未来付公司所提供的渠道中自主查询，若在消费最后确认日前乙方未向甲方提出异议，则视同其已认可此笔消费。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>乙方应根据未来付账户中所规定的还款日期准时足额还款。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>乙方每月分期还款额度为</span><span lang=EN-US>500</span><span style='font-family: 宋体'>元，甲方有权因认定的风险管控因素或其他正当理由，调整乙方的信用额度。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>乙方如发生工作单位、通讯地址、电话信息的变更，应当及时与甲方联系办理变更手续。否则，由此产生的损失由乙方承担。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>如乙方经甲方核准进行提前还款交易，利息按照消费发生时间至还款当日合计天数进行计算，乙方应一次完全清偿。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; text-indent: 0cm; line-height: 150%'>
									<b><span lang=EN-US style='font-family: "Times New Roman", "serif"'>第六条<span
											style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
										style='font-family: 宋体'>违约责任</span></b>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>乙方若未依约还款或有违规、欺诈行为造成甲方经济损失，甲方有权催收、依法追索。甲方因此而支出的费用（包括律师费和诉讼费）均由乙方承担。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>在乙方未能依约还款时，甲方有权要求乙方在合作的人力资源公司进行工作，并对欠款进行清偿。甲方同时保留依照法律程序进行追索的权利。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; text-indent: 0cm; line-height: 150%'>
									<b><span lang=EN-US style='font-family: "Times New Roman", "serif"'>第七条<span
											style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
										style='font-family: 宋体'>其他</span></b>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>为防范风险，保障甲乙丙三方利益，乙方有义务配合甲方及丙方的调查，并按甲方要求及时提供相关文件资料。否则，由此造成的损失由乙方承担。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>由于不可抗力因素（包括自然灾害、如台风、洪水、冰雹；突发事件、如火灾；政府行为，如征收、征用；社会异常事件，如罢工、骚乱）所发生的可在三个月内申请延迟还款，债务与债权关系不得因不可抗力而免除。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>甲方、乙方与丙方在履行本合约中发生的争议，由三方协商解决，协商不成提起诉讼的，由合约签订地的人民法院管辖。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>本合约解释权归属甲方，本合约及所依据的章程修改、收费项目或标准变化、利率调整等，一经甲方修改和正式公布，即为有效，无须另行通知乙方，并对甲、乙双方均由约束力。</span>
								</p>

								<p class=MsoListParagraph style='margin-left: 0cm; line-height: 150%'>
									<span lang=EN-US><span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span></span><span style='font-family: 宋体'>本合约适用于中华人民共和国法律，在合约履行中如发生争议，甲乙双方应进行协商和调解，协商或调解不成提起诉讼的，由合约签订地的人民法院管辖、未尽事宜除甲、乙双方另有约定外，均按照甲方业务规定和金融惯例办理。</span>
								</p>

							</td>
						</tr>

					</table>

					<table width="950" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<tr>

							<td height="30" align="left"><label style="margin-left: 50px"><input type="checkbox"
									name="step4Accept" value="" />&nbsp; <span style="color: #F00">*</span> 本人已仔细阅读并接受该合约的相关条款</label></td>
						</tr>

						<td height="50">
							<button id="backStep3" type="button" style="margin-left: 50px" class="buttom_botht">上一步</button>
							<button id="toStep5" type="button" style="width: 200px" class="buttom_botht">接受合约 提交注册</button>
						</td>
						</tr>
					</table>

				</div>
			</div>
			<div class="ldee_booth_lay" id="step5" style="display: none" align="center">
				<div class="ldee_booth_lay_top">
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_1.jpg" /><span>注册须知</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_2.jpg" /><span>填写资料</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_3.jpg" /><span>授信额度</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boot_4.jpg" /><span>阅读合约</span>
						</div>
					</div>
					<div class="ldee_booth_zc">
						<div class="ldee_booth_zc_one">
							<img src="${base}/resources/wlf/boo_5.jpg" /><span>完成注册</span>
						</div>
					</div>
				</div>
				<div class="ldee_booth_lay_center">
					<div class="tab-pane" id="tab5">
						<h3 class="block">恭喜，注册成功</h3>
					</div>
					<table width="950" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="50">
								<button id="toWlf" type="button" style="margin-left: 50px" class="buttom_botht">前往未来付</button>
								<button type="button" id="perfectInfo" class="buttom_botht" style="width: 200px">完善资料，申请更大额度</button>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</form>
		<!-- BEGIN COPYRIGHT -->
		<div class="copyright" style="text-align: center;">
			<a href="http://creditcard.cmbc.com.cn/index.aspx" target="_blank"> <img src="${base}/resources/wlf/footer.gif" /></a>
		</div>
		<!-- END COPYRIGHT -->


		<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
		<!-- BEGIN CORE PLUGINS -->


		<script src="${base}/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
		<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
		<script src="${base}/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"
			type="text/javascript"></script>
		<script src="${base}/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->
		<script type="text/javascript" src="${base}/assets/plugins/select2/select2.min.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/jquery-validation/localization/messages_zh.js"></script>

		<script type="text/javascript" src="${base}/assets/plugins/fuelux/js/spinner.min.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script type="text/javascript"
			src="${base}/assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.zh-CN.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
		<script type="text/javascript"
			src="${base}/assets/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
		<script type="text/javascript" src="${base}/assets/plugins/clockface/js/clockface.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
		<script src="${base}/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
		<script type="text/javascript" src="${base}/assets/plugins/jquery.pulsate.min.js"></script>
		<script src="${base}/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
		<script src="${base}/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
		<script src="${base}/assets/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
		<script type="text/javascript"
			src="${base}/assets/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-editable/inputs-ext/address/address.js"></script>
		<script type="text/javascript" src="${base}/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script type="text/javascript" src="${base}/assets/extras/jquery.base64.js"></script>
		<script src="${base}/assets/plugins/flot/jquery.flot.js"></script>
		<script src="${base}/assets/plugins/flot/jquery.flot.resize.js"></script>
		<script src="${base}/assets/plugins/flot/jquery.flot.pie.js"></script>
		<script src="${base}/assets/plugins/flot/jquery.flot.stack.js"></script>
		<script src="${base}/assets/plugins/flot/jquery.flot.crosshair.js"></script>
		<script src="${base}/assets/plugins/flot/jquery.flot.time.js"></script>

		<script src="${base}/assets/extras/pinyin.js?_=${buildVersion}"></script>
		<script src="${base}/assets/extras/JSPinyin.js?_=${buildVersion}"></script>

		<script src="${base}/assets/extras/jquery-jqgrid/plugins/ui.multiselect.js?_=${pageScope.buildVersion}"></script>
		<script src="${base}/assets/extras/jquery-jqgrid/js/i18n/grid.locale-cn.js?_=${buildVersion}"></script>
		<script src="${base}/assets/extras/jquery-jqgrid/js/jquery.jqGrid.src.js?_=${buildVersion}"></script>

		<script src="${base}/assets/extras/jquery-ztree/js/jquery.ztree.all-3.5.js?_=${buildVersion}"></script>

		<script src="${base}/assets/extras/jquery.address/jquery.address-1.5.min.js?_=${buildVersion}"></script>

		<script src="${base}/assets/extras/kindeditor/kindeditor-ext.js?_=${buildVersion}"></script>
		<script type="text/javascript">
            var WEB_ROOT = "${base}";
        <%out.println("KindEditor.options.uploadJson = '"
					+ request.getContextPath()
					+ "/pub/image-upload!execute;JSESSIONID=" + session.getId()
					+ "'");%>
            
        </script>

		<script type="text/javascript" src="${base}/assets/extras/tooltipster/js/jquery.tooltipster.min.js"></script>

		<script src="${base}/assets/scripts/app.js"></script>
		<script src="${base}/assets/extras/jquery.form.js"></script>
		<script src="${base}/assets/extras/bootstrap-contextmenu.js"></script>
		<script src="${base}/assets/extras/taffydb/taffy-min.js"></script>
		<script src="${base}/assets/app/dynamic-table.js"></script>
		<script src="${base}/assets/app/util.js"></script>
		<script src="${base}/assets/app/global.js"></script>
		<script src="${base}/assets/app/grid.js"></script>
		<script src="${base}/assets/app/form-validation.js"></script>
		<script src="${base}/assets/app/page.js"></script>
		<script src="${base}/resources/js/biz.js"></script>


		<script type="text/javascript">
            $(function() {

                // console.profile('Profile Sttart');

                App.init();
                Util.init();
                Global.init();
                FormValidation.init();

                Biz.init();
                //console.profileEnd();
                $("#toStep2").click(function() {

                    $("#step1").hide();
                    $("#step2").show();
                    $("#step3").hide();
                    $("#step4").hide();
                    $("#step5").hide();

                });
                $("#backStep1").click(function() {
                    $("#step1").show();
                    $("#step2").hide();
                    $("#step3").hide();
                    $("#step4").hide();
                    $("#step5").hide();
                });
                $("#toStep3").click(function() {
                    var trueName = $("input[name='trueName']").val().trim();
                    var idCard = $("input[name='idCard']").val().trim();
                    var mobilePhone = $("input[name='mobilePhone']").val().trim();
                    var checkCode = $("input[name='checkCode']").val().trim();
                    var loginid = $("input[name='loginid']").val().trim();
                    var password = $("input[name='password']").val().trim();
                    var rpassword = $("input[name='rpassword']").val().trim();
                   
                    var schoolId = $("select[name='studentInfo.schoolInfo.id']").val();
                    var academyId = $("select[name='studentInfo.academyInfo.id']").val();
                    var majorId = $("select[name='studentInfo.majorInfo.id']").val();
                    var enterTime = $("input[name='enterTime']").val();
                    var eduLevel = $("select[name='studentInfo.eduLevel']").val();
                    var degreeCategory = $("select[name='studentInfo.degreeCategory']").val();
                    var degreeMode = $("select[name='studentInfo.degreeMode']").val();
                   
                 	var step3WarnMsg = '';
                    if (checkCode == '') {
                        step3WarnMsg = step3WarnMsg + "校验码不能为空\n";
                    }
                    if (trueName == '') {
                        step3WarnMsg = step3WarnMsg + "真实姓名不能为空\n";
                    }
                    if ($("input[name='sex']:checked").val()!='F'&&$("input[name='sex']:checked").val()!='M') {
                        step3WarnMsg = step3WarnMsg + "性别不能为空\n";
                    }
                    if (idCard == '') {
                        step3WarnMsg = step3WarnMsg + "身份证号不能为空\n";

                    }
                    if (schoolId==null||schoolId==''||schoolId=='null') {
                        step3WarnMsg = step3WarnMsg + "学校不能为空\n";

                    }
                    if (academyId==null||academyId==''||academyId=='null') {
                        step3WarnMsg = step3WarnMsg + "学院不能为空\n";

                    }
                    if (majorId==null||majorId==''||majorId=='null') {
                        step3WarnMsg = step3WarnMsg + "专业不能为空\n";

                    }
                    if (eduLevel=='') {
                        step3WarnMsg = step3WarnMsg + "教育层次不能为空\n";
                    }
                    if (degreeCategory == '') {
                        step3WarnMsg = step3WarnMsg + "学制不能为空\n";
                    }
                    if (degreeMode == '') {
                        step3WarnMsg = step3WarnMsg + "学历类型不能为空\n";

                    }
                    if (enterTime == '') {
                        step3WarnMsg = step3WarnMsg + "入学时间不能为空\n";

                    }
                    if (mobilePhone == '') {
                        step3WarnMsg = step3WarnMsg + "手机号不能为空\n";

                    }
                    if (loginid == '') {
                        step3WarnMsg = step3WarnMsg + "登陆号不能为空\n";
                    }
                    if (password == '') {
                        step3WarnMsg = step3WarnMsg + "密码不能为空\n";
                    }
                    if (rpassword != password) {
                        step3WarnMsg = step3WarnMsg + "两次密码不一致\n";
                    }
                   
                    if (!$("input[name='step2Accept']").attr("checked")) {
                        step3WarnMsg = step3WarnMsg + "请先授权查询个人信息";
                    }
                    if (step3WarnMsg != '') {
                        alert(step3WarnMsg);
                        return false;
                    }

                    $(this).ajaxJsonSync("${base}/pub/wlf-user!findWlfStudent", {
                        trueName : trueName,
                        idCard : idCard,
                        checkCode : checkCode,
                        loginid : loginid,
                        password : password,
                        schoolId : schoolId,
                        academyId: academyId,
                        majorId: majorId,
                        eduLevel :eduLevel,
                        degreeCategory: degreeCategory,
                        degreeMode :degreeMode
                    }, function(response) {
                        if (response.type == "error" || response.type == "warning" || response.type == "failure") {
                            Global.notify("error", response.message);
                        } else {

                            $("#respCredit").empty().append(response.credit);
                            $("#step1").hide();
                            $("#step2").hide();
                            $("#step3").show();
                            $("#step4").hide();
                            $("#step5").hide();

                        }
                    });

                });
                $("#toStep4").click(function() {
                    $("#step1").hide();
                    $("#step2").hide();
                    $("#step3").hide();
                    $("#step4").show();
                    $("#step5").hide();

                });
                $("#backStep2").click(function() {
                    $("#step1").hide();
                    $("#step2").show();
                    $("#step3").hide();
                    $("#step4").hide();
                    $("#step5").hide();

                });

                $("#backStep3").click(function() {
                    $("#step1").hide();
                    $("#step2").hide();
                    $("#step3").show();
                    $("#step4").hide();
                    $("#step5").hide();

                });
                $("#backStep4").click(function() {
                    $("#step1").hide();
                    $("#step2").hide();
                    $("#step3").hide();
                    $("#step4").show();
                    $("#step5").hide();

                });

                $("#getSmsCode").click(function() {
                    var jcaptcha = $("input[name='jcaptcha']").val().trim();
                    if (jcaptcha == '') {
                        alert("清先输入验证码\n")
                        return false;
                    }
                    var mobilePhone = $("input[name='mobilePhone']").val().trim();
                    if (mobilePhone == '') {
                        alert("手机号不能为空\n")
                        return false;
                    }
                    $(this).ajaxJsonUrl("${base}/pub/wlf-user!getSmsCheckCode", function(response) {
                        if (response.type == "error" || response.type == "warning" || response.type == "failure") {
                            Global.notify("error", response.message);
                        } else {
                            $("#smsCodeResp").empty().append("校验码已发送");
                        }
                    }, {
                        mobilePhone : mobilePhone,
                        jcaptcha:jcaptcha
                    });
                });
                $("#toStep5").click(function() {
                    if (!$("input[name='step4Accept']").attr("checked")) {
                        alert("请先接受上述条款");
                        return false;
                    }
                    $("#wlfForm").ajaxSubmit({
                        dataType : "json",
                        method : "post",
                        success : function(response) {
                            if (response.type == 'success') {
                                $("#step1").hide();
                                $("#step2").hide();
                                $("#step3").hide();
                                $("#step4").hide();
                                $("#step5").show();

                            } else if (response.type == 'failure') {
                                alert(response.message);
                            } else {
                                alert("系统错误");
                            }
                        },
                        error : function(xhr, e, status) {
                            Global.notify("error", "系统处理异常，请联系管理员");
                        }
                    })

                });

                $("#toWlf").click(function() {
                    window.open("http://shop.weilaifu.com", "_self");
                });
                $("#perfectInfo").click(function() {
                    window.open("http://shop.weilaifu.com/weilaifu/FillInfo", "_self");
                })

                $("select[name='studentInfo.schoolInfo.id']").change(function() {
                    var val = $(this).val();
                    var $academyInfo = $("select[name='studentInfo.academyInfo.id']");
                    $academyInfo.empty();
                    if (val != "") {
                        $(this).ajaxJsonSync("${base}/pub/wlf-user!academyInfos", {
                            school : val
                        }, function(data) {
                            for ( var key in data) {
                                $academyInfo.append("<option value='"+key+"'>" + data[key] + "</option>")
                            }
                            $academyInfo.append("<option value=''>请选择院系</option>");
                        })
                    }
                    $academyInfo.select2("val", "");
                });

                $("select[name='studentInfo.academyInfo.id']").change(function() {
                    var val = $(this).val();
                    var school = $("select[name='studentInfo.schoolInfo.id']").val();
                    var $majorInfo = $("select[name='studentInfo.majorInfo.id']");
                    $majorInfo.empty();
                    if (val != "") {
                        $(this).ajaxJsonSync("${base}/pub/wlf-user!majorInfos", {
                            academy : val,
                            school : school
                        }, function(data) {
                            for ( var key in data) {
                                $majorInfo.append("<option value='"+key+"'>" + data[key] + "</option>")
                            }
                            $majorInfo.append("<option value=''>请选择专业</option>");
                        })
                    }
                    $majorInfo.select2("val", "");
                });

                $("#regionParent").change(function() {
                    var p = $(this).val();
                    var $city = $("#city");
                    $city.empty();
                    if (p != "") {
                        $(this).ajaxJsonUrl("${base}/pub/wlf-user!cities?province=" + p, function(data) {
                            for ( var key in data) {
                                $city.append("<option value='"+key+"'>" + data[key] + "</option>")
                            }
                        })
                    }
                });

                $("#bornTime").datepicker({
                    format : 'yyyy-mm-dd',
                    language : 'zh-CN',
                    autoclose : true
                });

                Page.initAjaxBeforeShow();

            });
        </script>


	</div>
</body>
<!-- END BODY -->
</html>
