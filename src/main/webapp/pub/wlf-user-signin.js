$(function() {

    // default form wizard
    $('#form_wizard_1').bootstrapWizard({
        'nextSelector' : '.button-next',
        'previousSelector' : '.button-previous',
        onTabClick : function(tab, navigation, index, clickedIndex) {
            return false;
        },
        onNext : function(tab, navigation, index) {
            //alert("onNext" + index);
            App.scrollTo($('#form_wizard_1'), 0);
        },
        onPrevious : function(tab, navigation, index) {
            App.scrollTo($('#form_wizard_1'), 0);
        },
        onTabShow : function(tab, navigation, index) {
            //alert("onTabShow");
            var total = navigation.find('li').length;
            var current = index + 1;
            var $percent = (current / total) * 100;
            $('#form_wizard_1').find('.progress-bar').css({
                width : $percent + '%'
            });

            if (index == 2) {
                $('#form_wizard_1').find(".button-next").hide();
                $('#form_wizard_1').find(".button-submit").show();
            } else if (index == 3) {
                $('#form_wizard_1').find(".button-previous").hide();
                $('#form_wizard_1').find(".button-submit").hide();
                $('#form_wizard_1').find(".button-done").show();
            } else {
                $('#form_wizard_1').find(".button-next").show();
                $('#form_wizard_1').find(".button-submit").hide();
            }

        }
    });

});