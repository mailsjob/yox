$(function() {
    $(".form-o2o-vip-home-index-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-commodity").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                    title : '选取商品',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'commodity.id' : rowdata.id,
                            'commodity.display' : rowdata.display
                        });
                    }
                })
            });
            $form.find(".fa-select-customerProfile").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                    title : '选取店铺',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'customerProfile.id' : rowdata.id,
                            'customerProfile.shopName' : rowdata.shopName
                        });
                    }
                })
            });
        }
    });
});