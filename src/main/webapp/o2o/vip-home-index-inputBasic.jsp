<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form
    class="form-horizontal form-bordered form-label-stripped form-validation form-o2o-vip-home-index-inputBasic"
    action="${base}/o2o/vip-home-index!doSave" method="post"
    data-editrulesurl="${base}/o2o/vip-home-index!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit"
            data-grid-reload=".grid-o2o-vip-home-index-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">商品</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-commodity"></i>
                            <s:textfield name="commodity.display" />
                            <s:hidden name="commodity.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">排序</label>
                    <div class="controls">
                        <s:textfield name="orderIndex" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">附近店铺</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-customerProfile"></i>
                            <s:textfield name="customerProfile.shopName" />
                            <s:hidden name="customerProfile.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit"
            data-grid-reload=".grid-o2o-vip-home-index-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/o2o/vip-home-index-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>