$(function () {
    $(".grid-o2o-live-info-index").data("gridOptions", {
        url: WEB_ROOT + '/o2o/live-info!findByPage',
        colModel: [{
            label: '直播图片',
            name: 'livePic',
            width: 255,
            editable: true,
            formatter: Biz.md5CodeImgViewFormatter,
            frozen: true,
            align: 'left'
        }, {
            label: '流水号',
            name: 'id',
            hidden: true
        }, {
            label: '开始时间',
            name: 'startTime',
            width: 150,
            formatter: 'datetime',
            editable: true,
            align: 'center'
        }, {
            label: '结束时间',
            name: 'endTime',
            width: 150,
            formatter: 'datetime',
            editable: true,
            align: 'center'
        }, {
            label: '直播链接',
            name: 'liveLink',
            width: 255,
            editable: true,
            align: 'left'
        }, {
            label: '排序号',
            name: 'orderIndex',
            width: 60,
            editable: true,
            align: 'right'
        }, {
            label: '是否可用',
            name: 'enable',
            width: 200,
            formatter: 'checkbox',
            editable: true,
            align: 'center'
        }],
        gridComplete: function () {
            $('.blockUI.blockOverlay').css('display', 'none');
        },
        editurl: WEB_ROOT + '/o2o/live-info!doSave',
        delurl: WEB_ROOT + '/o2o/live-info!doDelete',
        fullediturl: WEB_ROOT + '/o2o/live-info!inputTabs'
    });
});
