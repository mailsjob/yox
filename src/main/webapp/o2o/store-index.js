$(function() {
    $(".grid-o2o-store-index").data("gridOptions", {
        url : WEB_ROOT + '/o2o/store!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '图片',
            name : 'boss.shopCodePic',
            index : 'boss',
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true
        }, {
            label : '店铺名称',
            name : 'boss.shopName',
            index : 'boss',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '店铺待审核名称',
            name : 'customerProfile.shopNameAudit',
            index : 'customerProfile',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '老板',
            name : 'boss.display',
            index : 'boss',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '收银员',
            name : 'cashier.display',
            index : 'cashier',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '发货员',
            name : 'delivery.display',
            index : 'delivery',
            width : 200,
            editable : true,
            align : 'left'
        } ],
        postData : {
            "search['FETCH_boss']" : "LEFT",
            "search['FETCH_cashier']" : "LEFT",
            "search['FETCH_delivery']" : "LEFT"
        },
        editurl : WEB_ROOT + '/o2o/store!doSave',
        delurl : WEB_ROOT + '/o2o/store!doDelete',
        fullediturl : WEB_ROOT + '/o2o/store!inputTabs'
    });
});
