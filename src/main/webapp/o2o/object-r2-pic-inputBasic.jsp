<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form
    class="form-horizontal form-bordered form-label-stripped form-validation form-o2o-object-r2-pic-inputBasic"
    action="${base}/o2o/object-r2-pic!doSave" method="post"
    data-editrulesurl="${base}/o2o/object-r2-pic!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit"
            data-grid-reload=".grid-o2o-object-r2-pic-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">图片地址MD5</label>
                <div class="controls">
                    <s:hidden name="picUrl" data-singleimage="true" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">SID</label>
                    <div class="controls">
                        <s:textfield name="objectSid" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">排序号</label>
                    <div class="controls">
                        <s:textfield name="orderIndex" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">图片类型</label>
                    <div class="controls">
                        <s:select name="objectType" list="#application.enums.objectPicTypeEnum" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit"
            data-grid-reload=".grid-o2o-object-r2-pic-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/o2o/object-r2-pic-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>