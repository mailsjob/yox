<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
    <div class="col-md-12">
        <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-sharedPosts-selection">
            <div class="form-group">
                <label class="sr-only">客户</label> <input type="text" name="search['']" class="form-control input-xlarge" placeholder="昵称、姓名、手机号码、邮件地址..." />
            </div>
            <button class="btn default" type="reset">
                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
            </button>
            <button class="btn green" type="submmit">
                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
            </button>

        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="grid-sharedPosts-selection"></table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-sharedPosts-selection").data("gridOptions", {
            url : '${base}/o2o/shared-posts!findByPage',
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true
            }, {
                label : '对象名称',
                name : 'display',
                align : 'left'
            } ],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
