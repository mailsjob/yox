$(function() {
    $(".grid-o2o-app-auth-info-index").data("gridOptions", {
        url : WEB_ROOT + '/o2o/app-auth-info!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : 'app名',
            name : 'appName',
            editable : true,
            align : 'left'
        }, {
            label : 'app包名',
            name : 'appPackageName',
            editable : true,
            align : 'left'
        }, {
            label : '关联用户',
            name : 'customerProfile.display',
            index : 'customerProfile',
            editable : false,
            formatter : function(cellValue, options, rowdata, action) {
                return rowdata.customerProfile.id + ' ' + cellValue;
            },
            align : 'left'
        }, {
            label : '手机号',
            name : 'mobile',
            editable : true,
            align : 'left'
        }, {
            label : 'app下载地址',
            name : 'appDownLoadUrl',
            editable : true,
            align : 'left'
        }, {
            label : '备注',
            name : 'memo',
            editable : true,
            align : 'left'
        } ],
        postData : {
            "search['FETCH_customerProfile']" : "INNER"
        },
        editurl : WEB_ROOT + '/o2o/app-auth-info!doSave',
        delurl : WEB_ROOT + '/o2o/app-auth-info!doDelete'
    });
});
