<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-o2o-live-r2-commodity-inputBasic"
	action="${base}/o2o/live-r2-commodity!doSave" method="post" 
	data-editrulesurl="${base}/o2o/live-r2-commodity!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-o2o-live-r2-commodity-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">直播信息</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-live"></i>
							<s:textfield name="live.liveLink" />
							<s:hidden name="live.id" />
						</div>
					</div>
				</div>
			</div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-commodity"></i>
							<s:textfield name="commodity.display" />
							<s:hidden name="commodity.id" />
						</div>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否可用</label>
					<div class="controls">
		                <s:radio name="enable" list="#application.enums.booleanLabel"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">排序号</label>
					<div class="controls">
		                <s:textfield name="orderIndex" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品图</label>
					<div class="controls">
						<p class="form-control-static" style="padding: 20px 0 4px 25px;">
							<s:hidden name="commodityImg" data-singleimage="true"/>
						</p>
					</div>
				</div>
            </div>
        </div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-o2o-live-r2-commodity-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/o2o/live-r2-commodity-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>