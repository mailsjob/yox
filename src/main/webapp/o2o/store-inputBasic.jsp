<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-o2o-store-inputBasic" action="${base}/o2o/store!doSave" method="post" data-editrulesurl="${base}/o2o/store!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-o2o-store-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">老板</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-boss"></i>
                            <s:textfield name="boss.display" />
                            <s:hidden name="boss.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">收银员</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-cashier"></i>
                            <s:textfield name="cashier.display" />
                            <s:hidden name="cashier.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">发货员</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-delivery"></i>
                            <s:textfield name="delivery.display" />
                            <s:hidden name="delivery.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-o2o-store-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/o2o/store-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>