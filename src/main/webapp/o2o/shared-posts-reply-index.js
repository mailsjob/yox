$(function() {
    $(".grid-o2o-shared-posts-reply-index").data("gridOptions", {
        url : WEB_ROOT + '/o2o/shared-posts-reply!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '客户',
            name : 'user.display',
            index : 'user',
            align : 'left'
        }, {
            label : '用户晒单',
            name : 'sharedPosts.display',
            index : 'sharedPosts',
            align : 'left'
        }, {
            label : '评论内容',
            name : 'replyContent',
            width : 255,
            editable: true,
            align : 'left'
        } ],
        postData: {
           "search['FETCH_user']" : "LEFT",
           "search['FETCH_sharedPosts']" : "LEFT"
        },
        editurl : WEB_ROOT + '/o2o/shared-posts-reply!doSave',
        delurl : WEB_ROOT + '/o2o/shared-posts-reply!doDelete',
        fullediturl : WEB_ROOT + '/o2o/shared-posts-reply!inputTabs'
    });
});
