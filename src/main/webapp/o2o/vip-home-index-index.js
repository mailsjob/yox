$(function() {
    $(".grid-o2o-vip-home-index-index").data("gridOptions", {
        url : WEB_ROOT + '/o2o/vip-home-index!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '附近店铺',
            name : 'customerProfile.shopName',
            index : 'customerProfile',
            editable : true,
            align : 'left'
        }, {
            label : '商品',
            name : 'commodity.display',
            index : 'commodity',
            editable : true,
            align : 'left'
        }, {
            label : '排序',
            name : 'orderIndex',
            editable : true,
            align : 'right'
        } ],
        postData : {
            "search['FETCH_customerProfile']" : "LEFT",
            "search['FETCH_commodity']" : "LEFT",
            "search['EQ_commodity.solrFilterType']" : "SHOP"
        },
        editurl : WEB_ROOT + '/o2o/vip-home-index!doSave',
        delurl : WEB_ROOT + '/o2o/vip-home-index!doDelete',
        fullediturl : WEB_ROOT + '/o2o/vip-home-index!inputTabs'
    });
});
