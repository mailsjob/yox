$(function() {
    $(".form-o2o-shared-posts-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-user").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                    title : '选取账户',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'user.id' : rowdata.id,
                            'user.display' : rowdata.display
                        });
                    }
                })
            });

            $form.find(".fa-select-object").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                    title : '选取商品',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'object.id' : rowdata.id,
                            'object.display' : rowdata.display
                        });
                    }
                })
            });
        }
    });
});