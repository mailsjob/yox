$(function() {
    $(".grid-o2o-flash-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/single-goods!findByPage',
        colModel : [ {
            label : '橱窗图',
            name : 'pic',
            width : 80,
            editable : false,
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '流水号',
            name : 'id'
        }, {
            label : '关联商品',
            name : 'commodity.display',
            index : 'commodity.title_OR_commodity.sku',
            width : 200,
            editable : false,
            align : 'left'
        }, {
            label : '商品标题',
            name : 'title',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '单价',
            name : 'price',
            width : 60,
            formatter : 'number',
            editable : true,
            align : 'right'
        }, {
            label : '已售数量',
            name : 'soldQuantity',
            width : 60,
            formatter : 'number',
            editable : true,
            align : 'right'
        }, {
            label : '最多购买数量',
            name : 'userBoughtMaxQuatity',
            width : 60,
            formatter : 'number',
            editable : true,
            align : 'right'
        }, {
            label : '单日限购数',
            name : 'dailyUserCanBuyQuatity',
            width : 60,
            formatter : 'number',
            editable : true,
            align : 'right'
        }, {
            label : '最大可售数量',
            name : 'maxSellQuantity',
            width : 60,
            formatter : 'number',
            editable : true,
            align : 'right'
        }, {
            label : '起售日期',
            name : 'sellBeginTime',
            width : 150,
            sorttype : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '停售日期',
            name : 'sellEndTime',
            width : 150,
            sorttype : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '单品描述',
            name : 'htmlContent',
            width : 200,
            editable : true,
            align : 'left',
            hidden : true
        }, {
            label : '类型',
            name : 'type',
            align : 'center',
            editable : true,
            width : 100,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('singleGoodsTypeEnum')
            },
            responsive : 'sm'
        }, {
            label : '闪购位置',
            name : 'siteType',
            align : 'center',
            editable : true,
            width : 100,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('mYTSiteTypeEnum')
            }
        } ],
        inlineNav : {
            add : false
        },
        postData : {
            "search['EQ_siteType']" : "YMMD"
        },
        editurl : WEB_ROOT + '/myt/md/single-goods!doSave',
        delurl : WEB_ROOT + '/myt/md/single-goods!doDelete',
        fullediturl : WEB_ROOT + '/myt/md/single-goods!inputTabs'
    });
});
