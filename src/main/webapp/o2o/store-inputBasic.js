$(function() {
    $(".form-o2o-store-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            // 老板
            $form.find(".fa-select-boss").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                    title : '选取账户',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'boss.id' : rowdata.id,
                            'boss.display' : rowdata.display
                        });
                    }
                })
            });
            // 收银员
            $form.find(".fa-select-cashier").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                    title : '选取账户',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'cashier.id' : rowdata.id,
                            'cashier.display' : rowdata.display
                        });
                    }
                })
            });
            // 发货员
            $form.find(".fa-select-delivery").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                    title : '选取账户',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'delivery.id' : rowdata.id,
                            'delivery.display' : rowdata.display
                        });
                    }
                })
            });
        }
    });
});