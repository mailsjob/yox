<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<div class="tabbable tabbable-primary">
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row search-form-default">
                <div class="col-md-12">
                    <form action="#" method="get" class="form-inline form-validation form-search-init"
                          data-grid-search=".grid-o2o-live-info-selection">
                        <div class="input-group">
                            <div class="input-cont">
                                <input type="text" name="search['CN_code']" class="form-control" placeholder="代码...">
                            </div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
                                    <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
                                </button>
								<button class="btn default hidden-inline-xs" type="reset">
                                    <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
                                </button>
							</span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-o2o-live-info-selection"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $(".grid-o2o-live-info-selection").data("gridOptions", {
            url: WEB_ROOT + '/o2o/live-info!findByPage',
            colModel: [{
                label: '直播图片',
                name: 'livePic',
                width: 255,
                editable: true,
                formatter: Biz.md5CodeImgViewFormatter,
                frozen: true,
                align: 'left'
            }, {
                label: '流水号',
                name: 'id',
                hidden: true
            }, {
                label: '开始时间',
                name: 'startTime',
                width: 150,
                formatter: 'datetime',
                editable: true,
                align: 'center'
            }, {
                label: '结束时间',
                name: 'endTime',
                width: 150,
                formatter: 'datetime',
                editable: true,
                align: 'center'
            }, {
                label: '直播链接',
                name: 'liveLink',
                width: 255,
                editable: true,
                align: 'left'
            }, {
                label: '排序号',
                name: 'orderIndex',
                width: 60,
                editable: true,
                align: 'right'
            }, {
                label: '是否可用',
                name: 'enable',
                width: 200,
                formatter: 'checkbox',
                editable: true,
                align: 'center'
            }],
            gridComplete: function () {
                $('.blockUI.blockOverlay').css('display', 'none');
            },
            rowNum: 10,
            multiselect: false,
            toppager: false,
            onSelectRow: function (id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
<%@ include file="/common/ajax-footer.jsp" %>