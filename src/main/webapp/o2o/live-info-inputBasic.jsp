<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-o2o-live-info-inputBasic"
	action="${base}/o2o/live-info!doSave" method="post" 
	data-editrulesurl="${base}/o2o/live-info!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-o2o-live-info-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">开始时间</label>
					<div class="controls">
		                <s3:datetextfield name="startTime" format="timestamp" data-timepicker="true"/>
					</div>
				</div>
            </div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">结束时间</label>
					<div class="controls">
						<s3:datetextfield name="endTime" format="timestamp" data-timepicker="true"/>
					</div>
				</div>
			</div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">排序号</label>
					<div class="controls">
		                <s:textfield name="orderIndex" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否可用</label>
					<div class="controls">
		                <s:radio name="enable" list="#application.enums.booleanLabel"/>
					</div>
				</div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">直播链接</label>
					<div class="controls">
						<s:textfield name="liveLink" />
					</div>
				</div>
			</div>
		</div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">直播图片</label>
					<div class="controls">
						<p class="form-control-static" style="padding: 20px 0 4px 25px;">
							<s:hidden name="livePic" data-singleimage="true"/>
						</p>
					</div>
				</div>
            </div>
        </div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-o2o-live-info-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/o2o/live-info-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>