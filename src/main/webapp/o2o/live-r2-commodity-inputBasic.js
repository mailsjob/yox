$(function() {
    $(".form-o2o-live-r2-commodity-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-live").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/o2o/live-info!forward?_to_=selection',
                    title : '选取直播',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'live.id' : rowdata.id,
                            'live.liveLink' : rowdata.liveLink
                        });

                    }
                })
            });

            $form.find(".fa-select-commodity").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                    title : '选取商品',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'commodity.id' : rowdata.id,
                            'commodity.display' : rowdata.display
                        });

                    }
                })
            });
        }
    });
});