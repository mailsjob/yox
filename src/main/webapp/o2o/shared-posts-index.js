$(function() {
    $(".grid-o2o-shared-posts-index").data("gridOptions", {
        url : WEB_ROOT + '/o2o/shared-posts!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '用户',
            name : 'user.display',
            index : 'user',
            align : 'left'
        }, {
            label : '对象商品',
            name : 'object.display',
            index : 'object',
            align : 'left'
        }, {
            label : '对象名字',
            name : 'objectName',
            align : 'left'
        }, {
            label : '晒单内容',
            name : 'postsContent',
            align : 'left'
        }, {
            label : '对象图片',
            name : 'objectPic',
            formatter : Biz.md5CodeImgViewFormatter,
            align : 'left'
        }, {
            label : '晒单图1',
            name : 'postsSharedPic1',
            formatter : Biz.md5CodeImgViewFormatter,
            align : 'left'
        }, {
            label : '晒单图2',
            name : 'postsSharedPic2',
            formatter : Biz.md5CodeImgViewFormatter,
            align : 'left'
        }, {
            label : '晒单图3',
            name : 'postsSharedPic3',
            formatter : Biz.md5CodeImgViewFormatter,
            align : 'left'
        }, {
            label : '对象值',
            name : 'objectExtValue',
            align : 'left'
        }, {
            label : '对象值1',
            name : 'objectExtValue1',
            align : 'left'
        }, {
            label : '审核状态',
            name : 'postsStatus',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('postsStatusEnum')
            },
            editable : true,
            align : 'center'
        } ],
        postData : {
            "search['FETCH_user']" : "LEFT",
            "search['FETCH_object']" : "LEFT"
        },
        editurl : WEB_ROOT + '/o2o/shared-posts!doSave',
        delurl : WEB_ROOT + '/o2o/shared-posts!doDelete',
        fullediturl : WEB_ROOT + '/o2o/shared-posts!inputTabs'
    });
});
