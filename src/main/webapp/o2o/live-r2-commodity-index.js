$(function () {
    $(".grid-o2o-live-r2-commodity-index").data("gridOptions", {
        url: WEB_ROOT + '/o2o/live-r2-commodity!findByPage',
        colModel: [{
            label: '流水号',
            name: 'id',
            hidden: true
        }, {
            label: '直播信息',
            name: 'live.livePic',
            index: 'live',
            formatter: Biz.md5CodeImgViewFormatter,
            frozen: true,
            editable: true,
            align: 'left'
        }, {
            label: '商品',
            name: 'commodity.display',
            index: 'commodity',
            editable: true,
            align: 'left'
        }, {
            label: '是否可用',
            name: 'enable',
            formatter: 'checkbox',
            editable: true,
            align: 'center'
        }, {
            label: '排序号',
            name: 'orderIndex',
            editable: true,
            align: 'right'
        }, {
            label: '商品图',
            name: 'commodityImg',
            editable: true,
            formatter: Biz.md5CodeImgViewFormatter,
            frozen: true,
            align: 'left'
        }],
        postData: {
            "search['FETCH_commodity']": "INNER",
            "search['FETCH_live']": "INNER"
        },
        gridComplete: function () {
            $('.blockUI.blockOverlay').css('display', 'none');
        },
        editurl: WEB_ROOT + '/o2o/live-r2-commodity!doSave',
        delurl: WEB_ROOT + '/o2o/live-r2-commodity!doDelete',
        fullediturl: WEB_ROOT + '/o2o/live-r2-commodity!inputTabs'
    });
});
