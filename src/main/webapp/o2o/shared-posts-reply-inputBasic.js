$(function() {
    $(".form-o2o-shared-posts-reply-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-user").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                    title : '选取账户',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'user.id' : rowdata.id,
                            'user.display' : rowdata.display
                        });
                    }
                })
            });

            $form.find(".fa-select-sharedPosts").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/o2o/shared-posts!forward?_to_=selection',
                    title : '选取晒单',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'sharedPosts.id' : rowdata.id,
                            'sharedPosts.display' : rowdata.display
                        });
                    }
                })
            });
        }
    });
});