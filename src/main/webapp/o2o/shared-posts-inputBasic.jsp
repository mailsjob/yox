<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-o2o-shared-posts-inputBasic" action="${base}/o2o/shared-posts!doSave" method="post" data-editrulesurl="${base}/o2o/shared-posts!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-o2o-shared-posts-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">用户</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-user"></i>
                            <s:textfield name="user.display" />
                            <s:hidden name="user.id" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">对象商品</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-object"></i>
                            <s:textfield name="object.display" />
                            <s:hidden name="object.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">对象名字</label>
                    <div class="controls">
                        <s:textfield name="objectName" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">对象类型</label>
                    <div class="controls">
                        <s:select name="objectType" list="#application.enums.objectTypeEnum" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">对象值</label>
                    <div class="controls">
                        <s:textfield name="objectExtValue" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">对象值1</label>
                    <div class="controls">
                        <s:textfield name="objectExtValue1" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">晒单内容</label>
                    <div class="controls">
                        <s:textfield name="postsContent" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">晒单图1</label>
                    <div class="controls">
                        <s:hidden name="postsSharedPic1" data-singleimage="true" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">晒单图2</label>
                    <div class="controls">
                        <s:hidden name="postsSharedPic2" data-singleimage="true" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">晒单图3</label>
                    <div class="controls">
                        <s:hidden name="postsSharedPic3" data-singleimage="true" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">对象图片</label>
                    <div class="controls">
                        <s:hidden name="objectPic" data-singleimage="true" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">点赞数</label>
                    <div class="controls">
                        <s:textfield name="postsLoveCount" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">回复数</label>
                    <div class="controls">
                        <s:textfield name="postsReplyCount" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">审核状态</label>
                    <div class="controls">
                        <s:select name="postsStatus" list="#application.enums.postsStatusEnum" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-o2o-shared-posts-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/o2o/shared-posts-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>