/**
 * 优蜜美店 销售订单列表 <br>
 *
 */
$(function () {
    $(".grid-o2o-order").data("gridOptions", {
        calcRowAmount: function (rowdata, src) {
            if (src == 'actualAmount') {
                rowdata['discountAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['actualAmount']);
            } else {
                rowdata['actualAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['discountAmount']);
            }
        },
        updateRowAmount: function (src) {
            var $grid = $(this);
            var rowdata = $grid.jqGrid("getEditingRowdata");
            $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
            $grid.jqGrid("setEditingRowdata", rowdata);
        },
        url: WEB_ROOT + '/myt/sale/box-order!findByPage',
        colModel: [{
            name: 'id',
            hidden: true
        }, {
            label: '下单时间',
            name: 'orderTime',
            align: 'left',
            sorttype: 'date'
        }, {
            label: '付款类型',
            name: 'payMode',
            align: 'center',
            stype: 'select',
            searchoptions: {
                value: Biz.getPayMode()
            }
        }, {
            label: '来源',
            name: 'orderFrom',
            align: 'center',
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('orderFromEnum')
            },
            width: 80
        }, {
            label: '订单类型',
            name: 'orderMode',
            align: 'center',
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('boxOrderModeEnum')
            },
            width: 80
        }, {
            label: '状态',
            name: 'orderStatus',
            align: 'center',
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('boxOrderStatusEnum')
            },
            width: 80
        }, {
            label: '订单号',
            name: 'orderSeq',
            width: 200,
            align: 'left',
            formatter: function (cellValue, options, rowdata, action) {
                var html = cellValue;
                if (rowdata.circle) {
                    html += '<span class="badge badge-info">周</span>'
                }
                if (rowdata.splitPayMode != 'NO') {
                    html += '<span class="badge badge-success">分</span>'
                }
                if (rowdata.firstPayment) {
                    html += '<span class="badge badge-info">首</span>';
                }
                return html;
            }
        }, {
            label: '下单客户',
            name: 'customerProfile.display',
            index: 'customerProfile.trueName_OR_customerProfile.nickName',
            align: 'left',
            width: 80,
            formatter: function (cellValue, options, rowdata, action) {
                var url = WEB_ROOT + "/myt/customer/customer-profile!view?id=" + rowdata.customerProfile.id;
                return '<a href="' + url + '" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
            }
        }, {
            label: '客户编号',
            name: 'customerProfile.id',
            width: 60,
            align: 'center'
        }, {
            label: '客户来源',
            name: 'customerProfile.customerFrom',
            stype: 'select',
            searchoptions: {
                value: {
                    "": "",
                    "NONE": "美月淘",
                    "LAMAVC": "优蜜美店",
                    "SD": "圣大"
                }
            },
            align: 'center'
        }, {
            label: '客户电话',
            name: 'customerProfile.mobilePhone',
            width: 120,
            align: 'left'
        }, {
            label: '收货人',
            name: 'receivePerson',
            width: 80,
            align: 'left'
        }, {
            label: '收货人电话',
            name: 'mobilePhone',
            width: 120,
            align: 'left'
        }, {
            label: '原始金额',
            name: 'originalAmount',
            width: 80,
            formatter: 'currency',
            editable: true,
            editoptions: {
                dataInit: function (elem) {
                    var $elem = $(elem);
                    $elem.attr("readonly", true);
                }
            }
        }, {
            label: '折扣金额',
            name: 'discountAmount',
            editable: true,
            formatter: 'currency',
            editrules: {
                required: true,
            },
            editoptions: {
                dataInit: function (elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function () {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            }
        }, {
            label: '应付金额',
            name: 'actualAmount',
            editable: true,
            formatter: 'currency',
            editrules: {
                required: true,
            },
            editoptions: {
                dataInit: function (elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function () {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            }
        }, {
            label: '已付总金额',
            width: 70,
            name: 'actualPayedAmount',
            formatter: 'currency'
        }, {
            label: '改价备注',
            name: 'modifierMemo',
            editable: true,
            editrules: {
                required: true
            },
            width: 200
        }, {
            label: '怡亚通状态',
            name: 'yytPostStatus',
            align: 'center',
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('yytPostStatusEnum')
            },
            width: 80
        }, {
            label: '提货方式',
            name: 'receiveType',
            align: 'center',
            editable: true,
            width: 100,
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('commodityReceiveTypeEnum')
            }
        }, {
            label: '是否是首单',
            name: 'firstPayment',
            align: 'center',
            editable: true,
            edittype: 'checkbox',
            cellattr: function (rowId, cellValue, rowdata, cm) {
                if (rowdata.firstPayment && rowdata.actualAmount >= 100) {
                    return "class='badge-success'"
                }
            },
            width: 80
        }],
        postData: {
            "search['FETCH_customerProfile']": "INNER",
            "search['FETCH_intermediaryCustomerProfile']": "LEFT"
        },
        addable: false,
        inlineNav: {
            add: false
        },
        editurl: WEB_ROOT + '/myt/sale/box-order!modifyPrice',
        fullediturl: WEB_ROOT + '/myt/sale/box-order!inputTabs',
        footerrow: true,
        footerLocalDataColumn: ['actualAmount', 'actualPayedAmount'],
        subGrid: true,
        subGridRowExpanded: function (subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                calcRowAmount: function (rowdata, src) {
                    if (src == 'actualAmount') {
                        rowdata['discountAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['actualAmount']);
                    } else {
                        rowdata['actualAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['discountAmount']);
                    }
                },
                updateRowAmount: function (src) {
                    var $grid = $(this);
                    var rowdata = $grid.jqGrid("getEditingRowdata");
                    $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
                    $grid.jqGrid("setEditingRowdata", rowdata);
                },
                url: WEB_ROOT + '/myt/sale/box-order!boxOrderDetails?id=' + row_id,
                colModel: [{
                    label: '行项号',
                    name: 'sn',
                    align: 'center',
                    width: 60
                }, {
                    label: '预约发货日期',
                    name: 'reserveDeliveryTime',
                    editable: true,
                    align: 'center',
                    stype: 'date'
                }, {
                    label: '行项状态',
                    name: 'orderDetailStatus',
                    width: 80,
                    align: 'center',
                    stype: 'select',
                    searchoptions: {
                        value: Util.getCacheEnumsByType('boxOrderDetailStatusEnum')
                    }
                }, {
                    label: '原始金额',
                    name: 'originalAmount',
                    formatter: 'currency',
                    editable: true,
                    editoptions: {
                        dataInit: function (elem) {
                            var $elem = $(elem);
                            $elem.attr("readonly", true);
                        }
                    }
                }, {
                    label: '折扣金额',
                    name: 'discountAmount',
                    editable: true,
                    formatter: 'currency',
                    editrules: {
                        required: true,
                    },
                    editoptions: {
                        dataInit: function (elem) {
                            var $grid = $(this);
                            var $elem = $(elem);
                            $elem.keyup(function () {
                                $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                            });
                        }
                    }
                }, {
                    label: '应付金额',
                    name: 'actualAmount',
                    editable: true,
                    formatter: 'currency',
                    editrules: {
                        required: true,
                    },
                    editoptions: {
                        dataInit: function (elem) {
                            var $grid = $(this);
                            var $elem = $(elem);
                            $elem.keyup(function () {
                                $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                            });
                        }
                    }
                }, {
                    label: '已付金额',
                    name: 'payAmount',
                    formatter: 'currency'
                }, {
                    label: '改价备注',
                    name: 'modifierMemo',
                    editable: true,
                    editrules: {
                        required: true,
                    },
                    width: 200
                }],
                cmTemplate: {
                    sortable: false
                },
                rowNum: -1,
                loadonce: true,
                multiselect: false,
                filterToolbar: false,
                editurl: WEB_ROOT + '/myt/sale/box-order-detail!modifyPrice',
                fullediturl: WEB_ROOT + '/myt/sale/box-order-detail!inputTabs',
                toppager: false,
                subGrid: true,
                subGridRowExpanded: function (subgrid_id, row_id) {
                    Grid.initSubGrid(subgrid_id, row_id, {
                        calcRowAmount: function (rowdata, src) {
                            if (src == 'originalAmount') {
                                rowdata['price'] = MathUtil.div(rowdata['originalAmount'], rowdata['quantity']);
                            } else {
                                rowdata['originalAmount'] = MathUtil.mul(rowdata['price'], rowdata['quantity']);
                            }
                            if (src == 'actualAmount') {
                                rowdata['discountAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['actualAmount']);
                            } else {
                                rowdata['actualAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['discountAmount']);
                            }
                        },
                        updateRowAmount: function (src) {
                            var $grid = $(this);
                            var rowdata = $grid.jqGrid("getEditingRowdata");
                            $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
                            $grid.jqGrid("setEditingRowdata", rowdata);
                        },
                        url: WEB_ROOT + '/myt/sale/box-order-detail!boxOrderDetailCommodities?id=' + row_id,
                        colModel: [{
                            label: '商品',
                            name: 'commodity.display',
                            width: 250
                        }, {
                            label: '销售单价',
                            name: 'price',
                            editoptions: {
                                dataInit: function (elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function () {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            formatter: 'currency',
                            width: 100
                        }, {
                            label: '销售数量',
                            name: 'quantity',
                            formatter: 'number',
                            width: 100,
                            editrules: {
                                number: true
                            },
                            editoptions: {
                                dataInit: function (elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function () {
                                        $grid.data("gridOptions").updateRowAmount.call($grid);
                                    });
                                }
                            }
                        }, {
                            label: '原始金额',
                            name: 'originalAmount',
                            formatter: 'currency',
                            editoptions: {
                                dataInit: function (elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function () {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            width: 100
                        }, {
                            label: '折扣金额',
                            name: 'discountAmount',
                            editable: true,
                            editrules: {
                                required: true
                            },
                            editoptions: {
                                dataInit: function (elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function () {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            formatter: 'currency'
                        }, {
                            label: '金额小计',
                            name: 'actualAmount',
                            editable: true,
                            editrules: {
                                required: true
                            },
                            editoptions: {
                                dataInit: function (elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function () {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            formatter: 'currency'
                        }, {
                            label: '已发货数量',
                            name: 'deliveriedQuantity',
                            formatter: 'number',
                            width: 80
                        }, {
                            label: '发货仓库',
                            name: 'storageLocation.id',
                            width: 120,
                            stype: 'select',
                            searchoptions: {
                                value: Biz.getStockDatas()
                            }
                        }, {
                            label: '锁定库存量',
                            name: 'salingLockedQuantity',
                            formatter: 'number',
                            width: 80
                        }, {
                            label: '改价备注',
                            name: 'modifierMemo',
                            editable: true,
                            editrules: {
                                required: true,
                            },
                            width: 200
                        }],
                        cmTemplate: {
                            sortable: false
                        },
                        editurl: WEB_ROOT + '/myt/sale/box-order-detail-commodity!modifyPrice',
                        rowNum: -1,
                        toppager: false,
                        filterToolbar: false,
                        multiselect: false
                    });
                }
            });
        }
    });
});
