$(function() {
    $(".grid-o2o-object-r2-pic-index").data("gridOptions", {
        url : WEB_ROOT + '/o2o/object-r2-pic!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '图片地址MD5',
            name : 'picUrl',
            width : 80,
            search : false,
            formatter : Biz.md5CodeImgViewFormatter,
            align : 'left'
        }, {
            label : '图片类型',
            name : 'objectType',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('objectPicTypeEnum')
            },
            editable : true,
            align : 'center'
        }, {
            label : 'SID',
            name : 'objectSid',
            editable : true,
            align : 'right'
        }, {
            label : '排序号',
            name : 'orderIndex',
            editable : true,
            align : 'right'
        } ],
        editurl : WEB_ROOT + '/o2o/object-r2-pic!doSave',
        delurl : WEB_ROOT + '/o2o/object-r2-pic!doDelete',
        fullediturl : WEB_ROOT + '/o2o/object-r2-pic!inputTabs'
    });
});
