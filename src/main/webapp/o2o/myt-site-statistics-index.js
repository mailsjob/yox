$(function() {
    var $form = $(this);
    $form.find(".fa-select-vipCustomer").click(function() {
        $(this).popupDialog({
            url : WEB_ROOT + '/myt/vip/vip-customer-profile!forward?_to_=selection',
            title : '选取APP客户',
            callback : function(rowdata) {
                $form.setFormDatas({
                    'vipCustomerProfile.id' : rowdata.id,
                    'vipCustomerProfile.display' : rowdata.display
                });
            }
        })
    });
    $(".grid-o2o-myt-site-statistics-index").data("gridOptions", {
        url : WEB_ROOT + '/o2o/myt-site-statistics!findByDate',
        colModel : [ {
            label : '流量统计',
            name : 'count(id)'
        } ],
        multiselect : false
    });
});
