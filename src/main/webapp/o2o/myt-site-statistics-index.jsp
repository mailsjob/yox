<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">列表查询</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-o2o-myt-site-statistics-index">
						<div class="input-group">
							<div class="form-group">
                                <input type="text" style="width: 180px !important;" name="date" class="form-control input-medium input-daterangepicker grid-param-data" placeholder="日期区间" pattern="">
                            </div>
                            <div class="form-group">
                                <div class="input-icon right">
                                    <i class="fa fa-ellipsis-horizontal fa-select-vipCustomer"></i>
                                    <s:textfield name="vipCustomerProfile.display" placeholder="客户名称" />
                                    <s:hidden name="vipCustomerProfile.id" />
                                </div>
                            </div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default hidden-inline-xs" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-o2o-myt-site-statistics-index"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/o2o/myt-site-statistics-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
