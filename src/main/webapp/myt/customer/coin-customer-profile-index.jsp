<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">积分充值</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/customer/customer-coins-history">积分变更明细</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/customer/coin-customer-profile-importPage.jsp">批量积分充值</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-coin-customer-profile-index">
						<div class="form-group">
							<label class="sr-only">客户</label> <input type="text"
								name="search['CN_nickName_OR_trueName_OR_mobilePhone_OR_email']" class="form-control input-xlarge"
								placeholder="昵称、姓名、手机号码、邮件地址..." />
						</div>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>

					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-coin-customer-profile-index"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/customer/coin-customer-profile-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
