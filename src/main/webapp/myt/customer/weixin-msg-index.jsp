<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">微信信息列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-weixin-msg-list">
						<div class="form-group">
							<input type="text" name="search['CN_fromWeixinName_OR_weixinMsg']" class="form-control input-xlarge"
								placeholder="微信内容，微信名..." />
						</div>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>

					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-weixin-msg-list"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-weixin-msg-list").data("gridOptions", {
            url : '${base}/myt/customer/weixin-msg!findByPage',
            colModel : [ {
                label : '用户',
                name : 'msgId'
            }, {
                label : '时间',
                name : 'msgCreatedDt',
                sorttype : 'date'
            }, {
                label : '微信名',
                name : 'fromWeixinName'

            }, {
                label : '内容',
                name : 'weixinMsg',
                width : 120
            }, {
                label : '内容分类',
                name : 'msgType',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('msgTypeEnum')
                }
            }, {
                label : '操作类型',
                name : 'actionType',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('actionTypeEnum')
                }
            } ],

            fullediturl : "${base}/myt/customer/weixin-msg!edit?id=<s:property value='#parameters.id'/>",
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : "${base}/myt/customer/weixin-msg!weixinMsgReplies?id=" + row_id,
                    colModel : [ {
                        label : '回复时间',
                        name : 'replyTime',
                        sorttype : 'date',
                        width : 120
                    }, {
                        label : '回复内容',
                        name : 'replyMsg',
                        width : 100
                    }, {
                        label : '内容分类',
                        name : 'replyType',
                        width : 120,
                        align : 'center',
                        stype : 'select',
                        searchoptions : {
                            value : Util.getCacheEnumsByType('replyTypeEnum')
                        }
                    }, {
                        label : '备注',
                        name : 'memo',
                        width : 100
                    }, {
                        label : '回复人',
                        name : 'replyUser.display',
                        width : 50
                    } ],
                    sortorder : "desc",
                    sortname : "replyTime",
                    multiselect : false
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
