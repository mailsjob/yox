$(function () {
    $(".grid-myt-customer-customer-coins-history-index").data("gridOptions", {
        url: WEB_ROOT + '/myt/customer/customer-coins-history!findByPage',
        colModel: [{
            label: '流水号',
            name: 'id',
            hidden: true
        }, {
            label : '变更时间',
            name : 'createdDate',
            align : 'left',
            sorttype : 'date'
        },{
            label: '操作类型',
            name: 'operationType',
            formatter: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('operationTypeEnum')
            },
            editable: true,
            align: 'center'
        }, {
            label: '变更前积分',
            name: 'coinsBefore',
            formatter: 'number',
            editable: true,
            align: 'right'
        }, {
            label: '变更后积分',
            name: 'coinsAfter',
            formatter: 'number',
            editable: true,
            align: 'right'
        }, {
            label: '本次操作变更积分',
            name: 'coins',
            formatter: 'number',
            editable: true,
            align: 'right'
        }, {
            label: '充值积分',
            name: 'baseNewScore',
            formatter: 'number'
        }, {
            label: '日增长积分',
            name: 'givenNewScore',
            formatter: 'number'
        }, {
            label: '订单',
            name: 'order.display',
            index: 'order',
            editable: true,
            align: 'left'
        }, {
            label: '客户',
            name: 'customerProfile.display',
            index: 'customerProfile',
            editable: true,
            align: 'left'
        }, {
            label: '备注',
            name: 'memo',
            editable: true,
            align: 'left'
        }],
        postData: {
            "search['FETCH_customerProfile']": "LEFT",
            "search['FETCH_order']": "LEFT"
        }
    });
});
