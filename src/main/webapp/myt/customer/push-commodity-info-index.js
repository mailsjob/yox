$(function() {
    $(".grid-myt-customer-push-commodity-info-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/customer/push-commodity-info!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        },{
            name : 'commodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '商品',
            name : 'commodity.display',
            index : 'commodity',
            width : 200,
            editable: true,
            editoptions : {
                placeholder : '双击选取',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    // 弹出框的选取回调函数
                    var selectCommodity = function(item) {
                    	console.info(item);
                        $grid.jqGrid('setEditingRowdata', {
                        	'commodity.id':item['id'],
                        	'commodity.display':item['title']
                        });
                    }
                    $elem.dblclick(function() {
                        var rowdata = $grid.jqGrid("getEditingRowdata");
                        $(this).popupDialog({
                            url : WEB_ROOT+'/myt/md/commodity!forward?_to_=selection',
                            title : '选取单品商品',
                            callback : function(item) {
                                selectCommodity(item);
                            }
                        })
                    });
                }
            },
            align : 'left'
        }, {
            label : '商品链接',
            name : 'commodityUrl',
            width : 200,
            editable: true,
            align : 'left'
        }],
        editurl : WEB_ROOT + '/myt/customer/push-commodity-info!doSave',
        delurl : WEB_ROOT + '/myt/customer/push-commodity-info!doDelete'
    });
});
