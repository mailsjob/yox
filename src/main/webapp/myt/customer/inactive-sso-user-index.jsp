<%-- =========================================== --%>
<%--  美月淘运营   客户管理  待激活用户列表  待激活用户   --%>
<%-- =========================================== --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">待激活用户</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-customer-inactive-sso-user">
						<div class="form-group">
							<input type="text" name="search['CN_loginid_OR_mobilePhone_OR_email']" class="form-control input-large"
								placeholder="登陆账号/电话/邮箱..." />
						</div>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-customer-inactive-sso-user"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-customer-inactive-sso-user").data("gridOptions", {
            url : '${base}/myt/customer/inactive-sso-user!findByPage',
            postData : {
                "search['EQ_sysImport']" : true,
                /* "search['EQ_returnVisit']" : false, */
                "search['NU_lastLogonTime']" : true
            },
            colModel : [ {
                name : 'id',
                editable : false,
                hidden : true
            }, {
                label : '登陆账号',
                width : 80,
                name : 'loginid'
            }, {
                label : '电话',
                width : 100,
                name : 'mobilePhone'
            }, {
                label : '客户姓名',
                width : 80,
                name : 'realName'
            },{
                label : '邮箱',
                width : 150,
                name : 'email'
            }, {
                label : '注册时间',
                name : 'registerTime',
                width : 200,
                sorttype : 'date'
            }, {
                label : '是否电话回访',
                name : 'returnVisit',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            } ],
            sortorder : "desc",
            sortname : "registerTime",
			editurl : "${base}/myt/customer/inactive-sso-user!doSave"
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
