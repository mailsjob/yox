<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="page-edit">
	<form class="form-horizontal form-bordered form-label-stripped form-validation customer-reissue-sign"
		action="${base}/myt/customer/customer-profile!reissueSign" method="post">
		<s:hidden name="id" />
		<s:token />
		<div class="form-actions right">
			<button class="btn default btn-cancel" type="button">取消</button>
			<button class="btn blue" type="submit" data-grid-reload=".grid-sms-log-list">
				<i class="fa fa-check"></i> 补发签到
			</button>
		</div>
		<div class="form-body">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">金额:</label>
								<div class="controls">
									<s:textfield name="reissueSignAmount" requiredLabel="true"/>	
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label col-md-4">备注</label>
								<div class="col-md-6">
								<s:textarea name="reissueSignMemo"  rows="3"></s:textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<%@ include file="/common/ajax-footer.jsp"%>
