$(function() {
    $(".form-edit-customer-profile").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-c2cShopInfo").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/c2c/c2c-shop-info!forward?_to_=selection',
                    title : '选取集市店铺',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'c2cShopInfo.id' : rowdata.id,
                            'c2cShopInfo.shopName' : rowdata.shopName
                        });
                    }
                })
            });
        }
    });
    $('#btnRelease').click(function() {
        if($('#weixinId').val()) {
            $(this).ajaxJsonSync(
                    WEB_ROOT + '/myt/customer/customer-profile!releaseWeixin', 
                    {weixinId:$('#weixinId').val()},
                    function(response) {
                        if(response) {
                            Global.notify(response.type, response.message);
                        } else {
                            Global.notify('error', '解绑失败请重试');
                        }
                    }
                );
        } else {
            Global.notify('error', '该用户尚未绑定微信账号');
        }
    });
});