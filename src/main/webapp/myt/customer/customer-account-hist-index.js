$(function() {
    $(".grid-myt-customer-customer-account-hist").data("gridOptions", {
        url : WEB_ROOT + "/myt/customer/customer-account-hist!findByPage",
        colModel : [ {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName_OR_customerProfile.trueName_OR_customerProfile.id',
            width : 80
        }, {
            label : '金额',
            name : 'amount',
            align : 'center',
            width : 80
        }, {
            label : '申请时间',
            name : 'occurTime',
            sorttype : 'date',
            width : 150
        }, {
            label : '认证类型',
            name : 'occurType',
            align : 'center',
            width : 80,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('customerAccountHistOccurTypeEnum')
            }
        }, {
            label : '认证状态',
            name : 'accountType',
            align : 'center',
            width : 80,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('customerAccountHistAccountTypeEnum')
            }
        }, {
            label : '说明',
            name : 'sysMemo',
            width : 400
        }, {
            label : '(支付宝)凭证号',
            name : 'inOutAccountNo',
            width : 200
        }, {
            label : '身份证号',
            name : 'idCardNo',
            align : 'center',
            width : 150
        }, {
            label : '手机号',
            name : 'mobileNo',
            align : 'center',
            width : 150
        } ],
        multiselect : false
    });
});