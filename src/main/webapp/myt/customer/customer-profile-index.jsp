<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">客户列表</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/customer/contact-way-index.jsp">联系方式</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/customer/name-certificate">客户实名认证</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/customer/withdrawal-apply">提现申请</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/customer/customer-account-hist">收支记录</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/customer/score">积分管理</a></li>
		
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-customer-customer-profile-index">
						<div class="form-group">
							<label class="sr-only">客户</label> <input type="text"
								name="search['CN_nickName_OR_trueName_OR_mobilePhone_OR_email']" class="form-control input-xlarge"
								placeholder="昵称、姓名、手机号码、邮件地址..." />
						</div>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>

					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-customer-customer-profile-index"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/customer/customer-profile-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
