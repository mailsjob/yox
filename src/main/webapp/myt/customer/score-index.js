$(function() {
    $(".grid-myt-customer-score-index").data("gridOptions", {
        url : WEB_ROOT + "/myt/customer/score!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        },{
            name : 'customerProfile.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName',
            width : 200,
            editable: true,
            editoptions : {
            	updatable : false,
                placeholder : '输入客户信息提示选取或双击选取',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var selectCustomer = function(item) {
                        $grid.jqGrid("setEditingRowdata",{
                            'customerProfile.id' : item.id,
                            'customerProfile.display' : item.display
                        });
                    }
                    $elem.dblclick(function() {
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                            title : '选取客户',
                            callback : function(item) {
                                selectCustomer(item);
                            }
                        })
                    }).autocomplete({
                        autoFocus : true,
                        source : function(request, response) {
                            var data = Biz.queryCacheCustomerProfileDatas(request.term);
                            return response(data);
                        },
                        minLength : 2,
                        select : function(event, ui) {
                            var item = ui.item;
                            this.value = item.display;

                            selectCustomer(item);
                            event.stopPropagation();
                            event.preventDefault();
                            return false;
                        },
                        change : function(event, ui) {
                            if (ui.item == null || ui.item == undefined) {
                                $elem.val("");
                                $elem.focus();
                            }
                        }
                    }).focus(function() {
                        $elem.select();
                    });
                }
            },
            align : 'left'
        },{
            label : '类型',
            name : 'eventType',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('scoreEventTypeEnum')
            },
            width : 200,
            editable: true,
            align : 'center'
        }, {
            label : '分额',
            name : 'scoreAmount',
            width : 80,
            editable: true,
            align : 'right',
            editoptions : {
                updatable : false
            }
        }, {
            label : '积分产生时间',
            name : 'recordTime',
            width : 90,
            formatter: 'timestamp',
            editable: true,
            align : 'center'
        }, {
            label : '系统备注',
            name : 'sysMemo',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '备注',
            name : 'memo',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            name : 'boxOrder.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '订单',
            name : 'boxOrder.display',
            index : 'boxOrder.id',
            width : 150,
            editable: true,
            editoptions : {
                placeholder : '输入订单信息提示选取或双击选取',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var selectCustomer = function(item) {
                        $grid.jqGrid("setEditingRowdata",{
                            'boxOrder.id' : item.id,
                            'boxOrder.display' : item.orderSeq
                        });
                    };
                    $elem.dblclick(function() {
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/sale/box-order!forward?_to_=selection',
                            title : '选取订单',
                            callback : function(item) {
                                selectCustomer(item);
                            }
                        })
                    });
                }
            },
            align : 'left'
        }, {
            name : 'boxOrderDetail.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '订单行项',
            name : 'boxOrderDetail.display',
            index : 'boxOrderDetail.id',
            width : 150,
            editable: true,
            editoptions : {
                placeholder : '输入订单行项信息提示选取或双击选取',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var selectOrderDetail = function(item) {
                        $grid.jqGrid("setEditingRowdata",{
                            'boxOrderDetail.id' : item.id,
                            'boxOrderDetail.display' : item.sn
                        });
                    };
                    $elem.dblclick(function() {
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/sale/box-order-detail!forward?_to_=selection',
                            title : '选取订单行项',
                            callback : function(item) {
                                selectOrderDetail(item);
                            }
                        })
                    });
                }
            },
            align : 'left'
        }, {
            label : '有效时间',
            name : 'effectiveTime',
            width : 90,
            formatter: 'timestamp',
            editable: true,
            align : 'center'
        }, {
            label : '积分待定',
            name : 'scorePending',
            width : 100,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }, {
            label : '事件',
            name : 'event',
            width : 80,
            editable: true,
            align : 'right'
        }, {
            label : '积分事件描述',
            name : 'eventDescription',
            width : 200,
            editable: true,
            align : 'left'
        } ],
        editurl : WEB_ROOT + '/myt/customer/score!doSave'
        /*delurl : WEB_ROOT + '/myt/customer/score!doDelete'*/
    });
});
