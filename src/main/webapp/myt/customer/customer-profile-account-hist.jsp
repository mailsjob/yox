<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-customer-profile-account-hist" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-customer-customer-profile-account-hist" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-customer-customer-profile-account-hist").data("gridOptions", {
            url : "${base}/myt/customer/customer-profile!customerAccountHists?id=<s:property value='#parameters.id'/>",
			colModel : [{
                label : '主键',
                name : 'id',
                hidden:true,
                width : 25
            }, {
                label : '时间',
                name : 'occurTime',
                width : 120,
                align : 'center'
            }, {
                label : '账户类型',
                name : 'accountType',
                width : 50,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('customerAccountHistAccountTypeEnum')
                }
            }, {
                label : '操作类型',
                name : 'occurType',
                width : 50,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('customerAccountHistOccurTypeEnum')
                }
            }, {
                label : '金额',
                name : 'amount',
                formatter : 'currency',
                width : 50,
                align : 'right'
            }, {
                label : '备注',
                name : 'sysMemo',
                width : 300,
                align : 'left'
            }, {
                label : '订单号',
                name : 'boxOrder.orderSeq',
                width : 100,
                align : 'center'
            }, {
                label : '行项编号',
                name : 'boxOrderDetail.sn',
                width : 80,
                align : 'center'
            } ],
            sortorder : "desc",
            sortname : "occurTime",
            multiselect : false,
            operations : function(itemArray) {
                var $grid = $(this);
                var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>补发签到</a></li>');
                $select.children("a").bind("click", function(e) {
                    e.preventDefault();
                    $grid.popupDialog({
                        url : WEB_ROOT + "/myt/customer/customer-profile!forward?_to_=reissue-sign&id="+<s:property value="#parameters.id"/>,
                        title:'补发签到'
                     }) 
                 });
                itemArray.push($select);
            	/* var $grid = $(this);
                var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>补发签到</a></li>');
                $select.children("a").bind("click", function(e) {
                    e.preventDefault();
                    var url = WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=reissue-sign';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  补发签到？",
                        data : {
                            id : <s:property value='#parameters.id'/>
                        }
                    })
                 });
                itemArray.push($select);*/
            } 
        });
    });
    //$("#twoDimensionTable").toggle();
</script>
<%@ include file="/common/ajax-footer.jsp"%>