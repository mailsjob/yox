<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-customer-coin-importCoins"
	action="${base}/myt/customer/customer-profile!importCoins" method="post"
	enctype="multipart/form-data" >
	<s:token />
	<div class="form-body">
        <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-3">Excel</label>
					<div class="col-md-4">
						<input type="file" class="default" name="customerExcel"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-3">充值模板</label>
					<div class="col-md-4">
						<p class="form-control-static">
							<a href="${base}/myt/customer/coin-customer-profile-import-coins.xlsx">coin-customer-profile-import-coins.xlsx</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-customer-coin-importCoins">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>