<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="page-edit">
	<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-weixin-msg"
		action="${base}/myt/customer/weixin-msg!doSend" method="post">
		<s:hidden name="id" />
		<s:hidden name="version" />
		<s:token />
		<div class="form-actions right">
			<button class="btn default btn-cancel" type="button">取消</button>
			<button class="btn blue" type="submit" data-grid-reload=".grid-sms-log-list">
				<i class="fa fa-check"></i> 立即发送
			</button>

		</div>
		<div class="form-body">
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label col-md-4">发送时间</label>
								<div class="col-md-8">
									<s:date name="msgCreatedDt" format="yyyy-MM-dd HH:mm:ss" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label col-md-4">内容</label>
								<div class="col-md-8">
									<s:textarea name="weixinMsg" rows="3" readonly="true" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">内容分类</label>
								<div class="controls">
									<s:property value="#application.enums.msgTypeEnum[msgType]" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">操作类型</label>
								<div class="controls">
									<s:property value="#application.enums.actionTypeEnum[actionType]" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-4">回复内容</label>
						<div class="col-md-8">
							<s:textarea name="replyMsg" rows="8" requiredLabel="true" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<%@ include file="/common/ajax-footer.jsp"%>
