$(function() {
    $(".grid-customer-customer-profile-index").data("gridOptions", {
        url : WEB_ROOT + "/myt/customer/customer-profile!findByPage",
        colNames : [ '流水号', '客户类型', '客户来源', '用户类型', '友好显示', '昵称', '客户名称', '姓名', '性别', '电话', '邮件', '兼职薪水', '收货地址', '最后登录时间' ],
        colModel : [ {
            name : 'id',
            formatter : function(cellValue, options, rowdata, action) {
                var url = WEB_ROOT + "/myt/customer/customer-profile!view?id=" + rowdata.id;
                return '<a href="' + url + '" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
            }
        }, {
            name : 'customerNameType',
            align : 'center',
            width : 120,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheDictDatasByType('IYOUBOX_CUSTOMER_NAME_TYPE')
            }
        }, {
            name : 'customerFrom',
            align : 'center',
            width : 120,
            stype : 'select',
            searchoptions : {
                value : {
                    "" : "",
                    "NONE" : "美月淘",
                    "LAMAVC" : "优蜜美店"
                }
            }
        }, {
            name : 'isMeivd',
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : {
                    "" : "",
                    "true" : "美月淘",
                    "false" : "优蜜美店"
                }
            }
        }, {
            name : 'friendlyName',
            align : 'center',
            hidden : true,
            width : 100
        }, {
            name : 'nickName',
            align : 'center',
            width : 120
        }, {
            name : 'customerName',
            align : 'center',
            width : 100
        }, {
            name : 'trueName',
            editable : true,
            align : 'center',
            width : 120
        }, {
            name : 'sex',
            align : 'center',
            editable : true,
            width : 60,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('genericSexEnum')
            }
        }, {
            name : 'mobilePhone',
            editable : true,
            width : 100
        }, {
            name : 'email',
            editable : true,
            width : 200
        }, {
            name : 'partTimeSalary',
            hidden : false,
            width : 200,
            editable : true
        }, {
            name : 'deliveryAddr',
            hidden : true,
            width : 200,
            editable : true
        }, {
            name : 'meivdLastLogOnTime',
            sorttype : 'date'
        } ],
        addable : false,
        editcol : 'id',
        editurl : WEB_ROOT + "/myt/customer/customer-profile!doSave",
        delurl : WEB_ROOT + "/myt/customer/customer-profile!doDelete",
        fullediturl : WEB_ROOT + "/myt/customer/customer-profile!inputTabs",
        viewurl : WEB_ROOT + "/myt/customer/customer-profile!viewTabs"
    });
});