<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<div class="popup">
    <label class="sr-only">充值金额：</label>
    <input type="text" id="coinValue"/>
    <input type="button" id="submit" value="提交" onclick="callCoin()">
</div>
<script type="text/javascript">
    function callCoin() {
        debugger;
        var coinValue = $("#coinValue").val();
        $val1 = $("#submit");
        $val2 = $val1.closest(".modal");
        $val2.modal("hide");
        var callback = $val2.data("callback");
        if (callback) {
            callback.call($val2, coinValue);
            $val2.hide();
        }
    }
</script>
<%@ include file="/common/ajax-footer.jsp" %>