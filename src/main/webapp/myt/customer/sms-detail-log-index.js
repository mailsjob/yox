$(function() {
    $(".grid-myt-customer-sms-detail-log-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/customer/sms-detail-log!findByPage',
        colModel : [ {
            label : '电话',
            name : 'mobile',
            width : 80
        }, {
            label : '状态',
            name : 'state',
            width : 50,
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('messageStateEnum')
            },
            align : "center"
        }, {
            label : '发送通道',
            name : 'smsChannel',
            width : 50,
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('smsDetailChannelEnum')
            },
            align : "center"
        }, {
            label : '发送时间',
            name : 'sendTime',
            sorttype : 'date',
            width : 100
        }, {
            label : '内容',
            name : 'content',
            width : 200
        }, {
            label : '备注',
            name : 'memo',
            width : 100
        } ]
        /*editurl : WEB_ROOT + '/myt/customer/sms-detail-log!doSave',
        delurl : WEB_ROOT + '/myt/customer/sms-detail-log!doDelete',
        fullediturl : WEB_ROOT + '/myt/customer/sms-detail-log!inputTabs'*/
    });
});
