$(function() {
    $(".grid-myt-customer-name-certificate").data("gridOptions", {
        url : WEB_ROOT + "/myt/customer/name-certificate!findByPage",
        colModel : [ {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName_OR_customerProfile.trueName_OR_customerProfile.id',
            width : 80
        }, {
            label : '真实姓名',
            name : 'trueName',
            width : 60
        }, {
            label : '身份证号',
            name : 'idCardNo',
            align : 'center',
            width : 150
        }, {
            label : '手机号',
            name : 'mobileNo',
            align : 'center',
            width : 100
        }, {
            label : '相片1',
            name : 'photo1',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '相片2',
            name : 'photo2',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '相片3',
            name : 'photo3',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '相片4',
            name : 'photo4',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '说明',
            name : 'comment',
            editable : true,
            width : 200
        }, {
            label : '认证类型',
            name : 'certificateType',
            align : 'center',
            width : 80,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('certificateTypeEnum')
            }
        }, {
            label : '认证状态',
            name : 'certificateStatus',
            align : 'center',
            width : 80,
            editable : true,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('certificateStatusEnum')
            }
        } ],
        editurl : WEB_ROOT + "/myt/customer/name-certificate!doSave"
    });
});