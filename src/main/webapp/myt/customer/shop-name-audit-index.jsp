<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row search-form-default">
                <div class="col-md-12">
                    <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-shop-name-audit-index">
                        <div class="form-group">
                            <label class="sr-only">待审核店铺管理</label> <input type="text" name="search['CN_shopName_OR_mobilePhone']" class="form-control input-xlarge" placeholder="店铺名称、手机号..." />
                        </div>
                        <button class="btn default" type="reset">
                            <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
                        </button>
                        <button class="btn green" type="submmit">
                            <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
                        </button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-shop-name-audit-index"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-shop-name-audit-index").data("gridOptions", {
            url : '${base}/myt/customer/shop-name-audit!findByPage',
            colModel : [ {
                name : 'id',
                formatter : function(cellValue, options, rowdata, action) {
                    var url = WEB_ROOT + "/myt/customer/customer-profile!view?id=" + rowdata.id;
                    return '<a href="' + url + '" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
                }
            }, {
                label : '真实姓名',
                name : 'trueName',
                width : 100
            }, {
                label : '昵称',
                name : 'nickName',
                width : 100
            }, {
                label : '手机号码',
                name : 'mobilePhone',
                width : 100
            }, {
                label : '当前名称',
                name : 'shopName',
                width : 100
            }, {
                label : '待审核名称',
                name : 'shopNameAudit',
                width : 100
            }, {
                label : '审核状态',
                name : 'shopNameAuditStatus',
                align : 'center',
                width : 80,
                stype : 'select',
                editable : true,
                searchoptions : {
                    value : Util.getCacheEnumsByType('shopNameAuditStatusEnum')
                }
            } ],
            postData : {
                "search['NN_shopNameAudit_OR_shopName']" : true,
            },
            editurl : "${base}/myt/customer/shop-name-audit!saveShopName",
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>