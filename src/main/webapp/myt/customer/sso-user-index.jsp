<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">sso用户列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-customer-sso-user">
						<div class="form-group">
							<input type="text" name="search['CN_loginid_OR_nick_OR_mobilePhone_OR_email_OR_providerUid']" class="form-control input-large"
								placeholder="登陆账号/昵称/电话/邮箱..." />
						</div>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-customer-sso-user"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-customer-sso-user").data("gridOptions", {
            url : '${base}/myt/customer/sso-user!findByPage',
            colModel : [ {
                name : 'id',
                editable : false,
                hidden : true
            }, {
                label : '启用',
                name : 'enabled',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            }, {
                label : '登陆账号',
                width : 80,
                name : 'loginid'
            }, {
                label : '第三方的登录id',
                width : 80,
                name : 'providerUid'
            }, {
                label : '用户类型',
                width : 100,
                name : 'providerType'
            }, {
                label : '昵称',
                name : 'nick',
                width : 120
            }, {
                label : '电话',
                width : 100,
                name : 'mobilePhone'
            }, {
                label : '邮箱',
                width : 150,
                name : 'email'
            }, {
                label : '注册时间',
                name : 'registerTime',
                sorttype : 'date'
            }, {
                label : '登陆次数',
                name : 'totalLogonTimes',
                width : 60
            }, {
                label : '最后登陆时间',
                name : 'lastLogonTime',
                sorttype : 'date'
            } ],
            multiselect : true,
            sortorder : "desc",
            sortname : "registerTime",
            operations : function(itemArray) {

                var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-refresh"></i> 重置密码</a></li>');
                $select.children("a").bind("click", function(e) {
                    e.preventDefault();
                    var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                    var rowDatas = $grid.jqGrid("getSelectedRowdata");
                    if(rowDatas){
                        var uid = rowDatas.id;
                        $(this).popupDialog({
                            url : '${base}/myt/customer/sso-user!resetPasswordPage?id=' + $(uid).html(),
                            title : '人工重置密码'
                        })    
                    }else{
                        alert("请选择一个用户");
                    }
                });
                itemArray.push($select);
            }
			// editurl : "${base}/myt/customer/sso-user!doSave"
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
