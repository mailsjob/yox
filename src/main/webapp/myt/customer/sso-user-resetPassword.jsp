<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="page-edit">
	<form class="form-horizontal form-bordered form-label-stripped form-validation sso-user-resetPassword"
		action="${base}/myt/customer/sso-user!resetPassword" method="post">
		<s:hidden name="id" />
		<s:hidden name="version" />
		<s:token />
		<div class="form-actions right">
			<button class="btn default btn-cancel" type="button">取消</button>
			<button class="btn blue" type="submit" data-grid-reload=".grid-sms-log-list">
				<i class="fa fa-check"></i> 立即重置
			</button>

		</div>
		<div class="form-body">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">登录名:</label>
								<div class="controls">
									<p class="form-control-static">
										<s:property value="loginid" />
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label col-md-4">手机号</label>
								<div class="col-md-6">
									<s:textfield name="mobilePhone" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<%@ include file="/common/ajax-footer.jsp"%>
