<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="form-horizontal form-bordered form-label-stripped">
	<h4 class="form-section">基本信息</h4>
	<div class="row">
		<div class="col-md-2">
			<p class="form-control-static" style="padding: 20px 0 4px 25px;">
				<img src="<s:property value='imgUrlPrefix'/><s:property value='headPhoto' />" style="width: 150px; height: 200px;" alt="用户头像"/>
			</p>
		</div>
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">客户名:</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="customerName" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">真实姓名:</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="trueName" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">昵称:</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="nickName" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">生日:</label>
						<div class="controls">
							<p class="form-control-static">
								 <s:property value="birthday" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">性别:</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="#application.enums.genericSexEnum[sex]" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">电话:</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="mobilePhone" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">邮箱:</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="email" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">兼职薪水:</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="partTimeSalary" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">最后登录时间:</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="lastLogOnTime" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">最后签到时间:</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="lastCheckInTime" />
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			&nbsp;
		</div>
		<div class="col-md-10">
			<div class="form-group">
				<label class="control-label">地址:</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="deliveryAddr" />
					</p>
				</div>
			</div>
		</div>
	</div>
	
	<h4 class="form-section">收货地址信息</h4>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">真实姓名</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="trueName" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">联系电话</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="mobilePhone" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">详细地址</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="deliveryAddr" />
					</p>
				</div>
			</div>
		</div>
	</div>
	
	<h4 class="form-section">SSO用户信息</h4>
	<div class="row">
		<div class="col-md-2">
			<p class="form-control-static" style="padding: 20px 0 4px 25px;">
				<img src="<s:property value='ssoUser.imgUrl' />" style="width: 150px; height: 200px;" alt="用户头像"/>
			</p>
		</div>
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">登陆ID</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="ssoUser.loginid" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">昵称</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="ssoUser.nick" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">第三方登陆ID</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="ssoUser.providerUid" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">第三方的分类</label>
						<div class="controls">
							<p class="form-control-static">
								 <s:property value="ssoUser.providerType" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">邮件</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="ssoUser.email" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">电话:</label>
						<div class="controls">
							<p class="form-control-static">
								 <s:property value="ssoUser.mobilePhone" />
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/common/ajax-footer.jsp"%>