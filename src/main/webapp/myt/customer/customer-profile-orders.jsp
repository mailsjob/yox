<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-customer-profile-orders" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-customer-customer-profile-orders" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-customer-customer-profile-orders").data("gridOptions", {
            url : "${base}/myt/customer/customer-profile!boxOrders?id=<s:property value='#parameters.id'/>",
            colNames : [ '状态', '订单号', '订单名称<br>订单组号', '下单客户<br>推荐客户', '收货人', '订单总金额', '已付总金额', '下单时间', '付款模式' ],
            colModel : [ {
                name : 'orderStatus',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('boxOrderStatusEnum')
                }

            }, {
                name : 'orderSeq',
                width : 180,
                align : 'left'
            }, {
                name : 'orderGroupSeq_OR_title',
                index : 'title',
                formatter : function(cellValue, options, rowdata, action) {

                    var html = rowdata.title;
                    if (rowdata.orderGroupSeq) {
                        html += ("<br/>" + rowdata.orderGroupSeq);
                    }
                    if (html) {
                        return html;
                    }
                    return '';
                },
                align : 'left'
            }, {
                name : 'customerProfile.nickName_OR_customerProfile.trueName',
                formatter : function(cellValue, options, rowdata, action) {
                    var html = rowdata.customerProfile.display;
                    if (rowdata.intermediaryCustomerProfile) {
                        html += ("<br/>" + rowdata.intermediaryCustomerProfile.display);
                    }
                    return html;
                },
                hidden : true,
                width : 150,
                align : 'left'
            }, {
                name : 'receivePerson',
                index : 'receivePerson',
                width : 80,
                align : 'left'
            }, {
                name : 'actualAmount',
                index : 'actualAmount',
                formatter : 'currency'
            }, {
                name : 'actualPayedAmount',
                index : 'actualPayedAmount',
                formatter : 'currency'
            }, {
                name : 'orderTime',
                index : 'orderTime',
                sorttype : 'date'
            }, {
                name : 'splitPayMode',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('boxOrderSplitPayModeEnum')
                }

            } ],
            postData : {
                "search['FETCH_customerProfile']" : "INNER",
                "search['FETCH_intermediaryCustomerProfile']" : "LEFT"
            },
            multiselect : false,
            footerrow : true,
            footerLocalDataColumn : [ 'actualAmount', 'actualPayedAmount' ],
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : '${base}/myt/sale/box-order!getBoxOrderDetails?id=' + row_id,
                    colModel : [ {
                        label : '行项号',
                        name : 'sn',
                        align : 'center',
                        width : 60
                    }, {
                        label : '预约发货日期',
                        name : 'reserveDeliveryTime',
                        editable : true,
                        align : 'center',
                        stype : 'date'
                    }, {
                        label : '行项状态',
                        name : 'orderDetailStatus',
                        width : 80,
                        align : 'center',
                        stype : 'select',
                        searchoptions : {
                            value : Util.getCacheEnumsByType('boxOrderDetailStatusEnum')
                        }
                    }, {
                        label : '应付金额',
                        name : 'amount',
                        formatter : 'currency'
                    }, {
                        label : '已付金额',
                        name : 'payAmount',
                        formatter : 'currency'
                    } ],
                    cmTemplate : {
                        sortable : false
                    },
                    rowNum : -1,
                    multiselect : false,
                    filterToolbar : false,
                    editurl : "${base}/myt/sale/box-order-detail!doSave",
                    toppager : false,
                    subGrid : true,
                    subGridRowExpanded : function(subgrid_id, row_id) {
                        Grid.initSubGrid(subgrid_id, row_id, {
                            url : "${base}/myt/sale/box-order!getBoxOrderDetailCommodities?detailSid=" + row_id,
                            colModel : [ {
                                label : '商品',
                                name : 'commodity.display',
                                align : 'center',
                                width : 250
                            }, {
                                label : '销售单价',
                                name : 'price',
                                editable : true,
                                formatter : 'currency',
                                width : 100
                            }, {
                                label : '销售数量',
                                name : 'quantity',
                                editable : true,
                                formatter : 'number',
                                width : 100
                            }, {
                                label : '金额小计',
                                name : 'amount',
                                editable : true,
                                formatter : 'currency',
                                width : 100
                            }, {
                                label : '已发货数量',
                                name : 'deliveriedQuantity',
                                formatter : 'number',
                                width : 80
                            } ],
                            cmTemplate : {
                                sortable : false
                            },
                            editurl : "${base}/myt/sale/box-order-detail-commodity!doSave",
                            rowNum : -1,
                            toppager : false,
                            filterToolbar : false,
                            multiselect : false
                        });
                    }
                });
            }
        });
    });
    //$("#twoDimensionTable").toggle();
</script>
<%@ include file="/common/ajax-footer.jsp"%>