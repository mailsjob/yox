$(function() {
	$(".grid-myt-customer-contact-way").data("gridOptions", {
        url : WEB_ROOT + "/myt/customer/customer-profile!findByPage",
        postData : {
            "search['NB_mobilePhone']" : true
        },
        colModel : [ {
            label : '联系人',
            name : 'trueName',
            align : 'center',
            width : 150
        }, {
            label : '手机号',
            name : 'mobilePhone',
            align : 'center',
            width : 150
        }, {
            label : 'EMail',
            name : 'email',
            align : 'center',
            width : 200
        }, {
            label : '单位地址',
            name : 'deliveryAddr',
            align : 'left',
            width : 600
        } ],
        rowNum : 100,
        rowList : [ 100, 200, 500, 1000, 2000, 10000 ],
        multiselect : false
    });
});