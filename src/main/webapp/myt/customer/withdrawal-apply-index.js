$(function() {
    $(".grid-myt-customer-withdrawal-apply").data("gridOptions", {
        url : WEB_ROOT + "/myt/customer/withdrawal-apply!findByPage",
        colModel : [ {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName_OR_customerProfile.trueName_OR_customerProfile.id',
            width : 80
        }, {
            label : '申请时间',
            name : 'applyTime',
            sorttype : 'date',
            width : 100
        }, {
            label : '身份证号',
            name : 'applierIdcardNo',
            width : 150
        }, {
            label : '手机号',
            name : 'applierMobile',
            width : 100
        }, {
            label : '金额',
            name : 'applyAmount',
            align : 'center',
            width : 80
        }, {
            label : '提现支付宝账号/提现美月淘ID',
            name : 'applierAlipayAccount',
            width : 100
        }, {
            label : '(支付宝)凭证号',
            name : 'inOutAccountNo',
            editable : true,

            width : 200
        }, {
            label : '说明',
            name : 'memo',
            editable : true,
            width : 200
        }, {
            label : '状态',
            name : 'applyStatus',
            align : 'center',
            width : 80,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('withdrawalApplyStautsEnum')
            }
        } ],
        operations : function(itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>批量提现</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/customer/withdrawal-apply!apply';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  提现？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select);
        },
        editurl : WEB_ROOT + "/myt/customer/withdrawal-apply!doSave"
    });
});