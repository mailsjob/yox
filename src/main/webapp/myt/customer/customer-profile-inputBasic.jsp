<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<style>
<!--
form.form-edit-customer-profile div.portlet-title {
	margin-bottom: 0;
	color: #424242;
}

form.form-edit-customer-profile div.portlet-title h4 i {
	margin-right: 5px;
}
-->
</style>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-customer-profile" action="${base}/myt/customer/customer-profile!doSave" method="post">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-customer-profile-list">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="portlet">
            <div class="portlet-title"></div>
            <div class="portlet-body">
                <div class="portlet">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <p class="form-control-static" style="padding: 20px 0 4px 25px;">
                                    <img src="<s:property value='imgUrlPrefix'/><s:property value='headPhoto' />" style="width: 150px; height: 200px;" alt="用户头像" />
                                </p>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">客户名:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="customerName" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">真实姓名:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <a data-name="trueName" href="javascript:;" class="editable editable-click x-editable" style="padding: 0;">
                                                	<s:property value="trueName" />
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">昵称:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="nickName" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">生日:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="birthday" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">性别:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="#application.enums.genericSexEnum[sex]" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">电话:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="mobilePhone" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">邮箱:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="email" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">兼职薪水:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="partTimeSalary" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">最后登录时间:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="lastLogOnTime" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">最后签到时间:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="lastCheckInTime" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">地址:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="deliveryAddr" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">绑定合作伙伴</label>
                                <div class="controls">
                                    <s:select name="bindPartner.id" list="partnersMap" />
                                </div>
                            </div>
                        </div>
                        <%-- delete by harvey put forward by jason
						<div class="col-md-6" >
							<div class="form-group">
								<label class="control-label">绑定合作伙伴</label>
								<div class="controls">
									<s:select name="bindUser.id" list="usersMap"  />
								</div>
							</div>
						</div>--%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">微信账户解绑</label>
                        <div class="controls">
                            <s:textfield name="weixinId" id="weixinId" readonly="true" />
                        </div>
                    </div>
                </div>
                <div>
                    <input id="btnRelease" class="btn" style="margin-top: 5px" type="button" value="解绑">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">管理备注:</label>
                        <div class="controls">
                            <s:property value="adminMemo" escape="false" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet">
                <div class="portlet-title">
                    <h4>
                        <i class="fa fa-reorder"></i>客户收货地址信息
                    </h4>
                </div>
                <div class="portlet-body">
                    <s:if test="%{#request.receiveAddressList}">
                        <s:iterator value="%{#request.receiveAddressList}" var="receiveAddress" status="status">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">收货地址:</label>
                                        <div class="controls">
                                            <p class="form-control-static">
                                                <s:property value="#receiveAddress.detailAddress" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </s:iterator>
                    </s:if>
                    <s:else>
                        <span>未查到客户相关收货地址信息</span>
                    </s:else>
                </div>
            </div>
            <div class="portlet">
                <div class="portlet-title">
                    <h4>
                        <i class="fa fa-reorder"></i>宝宝信息
                    </h4>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">性别:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="#application.enums.genericSexEnum[babySex]" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">状态:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="babyStatus" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">生日:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="babyBirthday" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">体重:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="babyWight" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">身高:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="babyHeight" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet">
                <div class="portlet-title">
                    <h4>
                        <i class="fa fa-reorder"></i>消费积分信息
                    </h4>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">消费总金额:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="spentMoney" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">现金帐户:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="currentAccount" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">定金帐户：</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="earnestMoneyAccount" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">签到金金额:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="checkInAccount" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">冻结金额:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="frozenAccount" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">经验值:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="score" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">冻结分数:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="frozenScore" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">有效订单数量:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="validOrderCount" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">签到次数:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="checkInCount" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">提成比率:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="rewardRate" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">提成计算模式:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="rewardMode" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">节省的钱数:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="savedMoney" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">积分:</label>
                                <div class="controls">
                                    <s:textfield name="coins" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet">
                <div class="portlet-title">
                    <h4>
                        <i class="fa fa-reorder"></i>其他信息
                    </h4>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">客户类型:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="customerNameTypeMap[customerNameType]" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">客户等级:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="#application.enums.customerLevelEnum[customerLevel]" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">客户来源:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="customerFrom" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">客户购买类型:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="#application.enums.customerBuyTypeEnum[customerBuyType]" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">用户类型:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="#application.enums.genericSexEnum[babySex]customerType" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">信息状态:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="infoStatus" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">客户认证类型:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="#application.enums.certificateTypeEnum[certificateType]" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">推荐客户:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="intermediaryCustomerProfile.display" />
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">好友数:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="friendCount" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">粉丝数:</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:property value="fansCount" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">集市店铺:</label>
                                <div class="controls">
                                    <div class="input-icon right">
                                        <i class="fa fa-ellipsis-horizontal fa-select-c2cShopInfo"></i>
                                        <s:textfield name="c2cShopInfo.shopName" />
                                        <s:hidden name="c2cShopInfo.id" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 优蜜美店店铺信息管理 -->
            <div class="portlet">
                <div class="portlet-title">
                    <h4>
                        <i class="fa fa-reorder"></i>优蜜美店店铺
                    </h4>
                </div>
                <div class="portlet-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">店铺二维码:</label>
                            <div class="controls">
                                <p class="form-control-static" style="padding: 20px 0 4px 25px;">
                                <s:hidden name="shopCodePic" data-singleimage="true" />
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">店铺名称:</label>
                                <div class="controls">
                                    <s:textfield name="shopName" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">待审核店铺名称:</label>
                                <div class="controls">
                                    <s:textfield name="shopNameAudit" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">请求接口时间:</label>
                                <div class="controls">
                                    <s:textfield name="apiWaitTime" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">是否触屏:</label>
                                <div class="controls">
                                    <s:radio name="isToch" list="#application.enums.isTochEnum" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label">店铺描述:</label>
                            <div class="controls">
                                <s:textfield name="shopDescription" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <!-- 附近铺信息管理 -->
            <div class="portlet">
                <div class="portlet-title">
                    <h4>
                        <i class="fa fa-reorder"></i>附近店铺
                    </h4>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">最后一次记录的经度:</label>
                                <div class="controls">
                                    <s:textfield name="lastLng" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">最后一次记录的纬度:</label>
                                <div class="controls">
                                    <s:textfield name="lastLat" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">店铺范围:（单位：米）</label>
                                <div class="controls">
                                    <p class="form-control-static">
                                        <s:textfield name="scope" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">是否是附近店铺:</label>
                                <div class="controls">
                                    <s:radio name="toBusiness" list="#application.enums.booleanLabel" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"> 店铺图 </label>
                                <div class="controls">
                                    <s:iterator var="item" status="status" value="shopPagePics">
                                        <s:select name="%{'shopPagePics['+#status.index+'].objectType'}" list="#application.enums.objectPicTypeEnum" />
                                        <s:hidden name="%{'shopPagePics['+#status.index+'].picUrl'}" data-multiimage="edit" data-pk="%{#item.id}" data-index-val="%{#item.orderIndex}" data-index-prop="orderIndex" />
                                    </s:iterator>
                                    <s:hidden name="shopPagePics[X].picUrl" data-multiimage="btn" data-index-prop="orderIndex" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-cb-lottery-award-list">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/customer/customer-profile-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>