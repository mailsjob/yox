$(function() {
    $(".grid-myt-customer-import-user-aim-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/customer/import-user-aim!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '月份',
            name : 'month',
            width : 60,
            editable: true,
            align : 'right'
        }, {
            label : '目标数',
            name : 'aimNum',
            width : 60,
            editable: true,
            align : 'right'
        } ],
        editurl : WEB_ROOT + '/myt/customer/import-user-aim!doSave',
        delurl : WEB_ROOT + '/myt/customer/import-user-aim!doDelete'
    });
});
