<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">发送列表</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/customer/sms-detail-log">详情列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-sms-log-list">
						<div class="form-group">
							<input type="text" name="search['CN_sendContent_OR_sendToList']" class="form-control input-xlarge"
								placeholder="短信内容、手机号码..." />
						</div>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>

					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-sms-log-list"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-sms-log-list").data("gridOptions", {
            url : '${base}/myt/customer/sms-log!findByPage',
            colNames : [ '流水号', '短信内容', '创建时间', '发送时间', '发送用户', '发送结果' ],
            colModel : [ {
                name : 'id'
            }, {
                name : 'sendContent'
            }, {
                name : 'createdDate',
                sorttype : 'date'
            }, {
                name : 'sendTime',
                sorttype : 'date'
            }, {
                name : 'sendUserUid',
                width : 120
            }, {
                name : 'hasFailure',
                width : 120,
                align : 'center',
                formatter : function(cellValue, options, rowdata, action) {
                    if (rowdata.sendTime) {
                        if (!cellValue) {
                            return '<span class="label label-success">全部成功</span>';
                        } else {
                            return '<span class="label label-danger">有失败</span>';
                        }
                    } else {
                        return "未发送";
                    }
                }
            } ],
            delurl : "${base}/myt/customer/sms-log!doDelete",
            fullediturl : "${base}/myt/customer/sms-log!edit?id=<s:property value='#parameters.id'/>",
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : "${base}/myt/customer/sms-detail-log!findByPage?search['EQ_smsLog.id']=" + row_id,
                    colModel : [ {
                        label : '电话',
                        name : 'mobile',
                        width : 80
                    }, {
                        label : '状态',
                        name : 'state',
                        width : 50
                    }, {
                        label : '发送通道',
                        name : 'smsChannel',
                        width : 50
                    }, {
                        label : '发送时间',
                        name : 'sendTime',
                        sorttype : 'date',
                        width : 100
                    }, {
                        label : '内容',
                        name : 'content',
                        width : 200
                    }, {
                        label : '备注',
                        name : 'memo',
                        width : 100
                    } ],
                    multiselect : false
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
