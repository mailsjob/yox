<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form action="#" method="get" class="form-inline form-validation  form-search-init"
			data-grid-search=".grid-myt-customer-withdrawal-apply">
			<div class="input-group">
				<div class="form-group">
				<input type="text" name="search['CN_customerProfile.nickName_OR_customerProfile.trueName_OR_applierIdcardNo_OR_applierMobile_OR_applierAlipayAccount_OR_inOutAccountNo']"
							 placeholder="客户、真实姓名、身份证号、手机号、提现支付宝账号/提现美月淘ID、(支付宝)凭证号..." class="form-control input-xlarge"/>
				</div>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
				<button class="btn default" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-myt-customer-withdrawal-apply"></table>
	</div>
</div>
<script src="${base}/myt/customer/withdrawal-apply-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
