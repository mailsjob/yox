$(function () {
    $(".grid-coin-customer-profile-index").data("gridOptions", {
        url: WEB_ROOT + "/myt/customer/customer-profile!findByPage",
        colModel: [{
            label: '流水号',
            name: 'id'
        }, {
            label: '客户来源',
            name: 'customerFrom',
            align: 'center',
            width: 120,
            stype: 'select',
            searchoptions: {
                value: {
                    "": "",
                    "NONE": "美月淘",
                    "LAMAVC": "优蜜美店",
                    "SD": "盛大"
                }
            }
        }, {
            label: '昵称',
            name: 'nickName',
            align: 'center',
            width: 120
        }, {
            label: '客户名称',
            name: 'customerName',
            align: 'center',
            width: 100
        }, {
            label: '姓名',
            name: 'trueName',
            align: 'center',
            width: 120
        }, {
            label: '性别',
            name: 'sex',
            align: 'center',
            width: 60,
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('genericSexEnum')
            }
        }, {
            label: '电话',
            name: 'mobilePhone',
            width: 100
        }, {
            label: '邮件',
            name: 'email',
            width: 200
        }, {
            label: '总积分',
            name: 'coins',
            formatter: 'number',
            width: 200
        }, {
            label: '充值积分',
            name: 'baseNewScore',
            formatter: 'number',
            width: 200
        }, {
            label: '日增长累积积分',
            name: 'givenNewScore',
            formatter: 'number',
            width: 200
        }, {
            label: '是否可以自增长',
            name: 'given',
            stype: 'select',
            editable: true,
            searchoptions: {
                value: Util.getCacheEnumsByType('booleanLabel')
            },
            align: 'center'
        }],
        operations: function (itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>充值</a></li>');
            $select.children("a").bind("click", function (e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                $select.popupDialog({
                    url: WEB_ROOT + '/myt/customer/coin-customer-profile!forward?_to_=recharge',
                    title: '充值',
                    callback: function (coinValue) {
                        if (ids) {
                            var url = WEB_ROOT + '/myt/customer/customer-profile!recharge';
                            $grid.ajaxPostURL({
                                url: url,
                                success: function () {
                                    $grid.refresh();
                                },
                                confirmMsg: false,
                                data: {
                                    coinValue: coinValue,
                                    ids: ids.join(",")
                                }
                            })
                        }
                    }
                })
            });
            itemArray.push($select);
        },
        editurl: WEB_ROOT + "/myt/customer/customer-profile!doSave"
    });
});