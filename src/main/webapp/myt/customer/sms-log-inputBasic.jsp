<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="page-edit">
    <form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-sms-log" action="${base}/myt/customer/sms-log!doSave" method="post">
        <s:hidden name="id" />
        <s:hidden name="version" />
        <s:token />
        <div class="form-actions">
            <button class="btn blue" type="submit" data-grid-reload=".grid-sms-log-list">
                <i class="fa fa-check"></i> 保存草稿
            </button>
            <button class="btn default btn-cancel" type="button">取消</button>
            <button class="btn blue" type="submit" data-form-action="${base}/myt/customer/sms-log!doSend" data-grid-reload=".grid-sms-log-list" data-confirm="确认立即发送短信？">
                <i class="fa fa-arrow-right"></i> 立即发送
            </button>
            <s:if test="failureToList!=null">
                <div style="float: right">
                    <button class="btn blue" type="submit" data-form-action="${base}/myt/customer/sms-log!doSendFailure" data-grid-reload=".grid-sms-log-list" data-confirm="确认立即发送短信？">
                        <i class="fa fa-arrow-right"></i> 重发失败短信
                    </button>
                </div>
            </s:if>
        </div>
        <div class="form-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">短信通道</label>
                        <div class="controls">
                            <s:select name="smsChannel" list="#application.enums.smsChannelEnum" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">短信模板</label>
                        <div class="controls">
                            <s:textfield name="smsModel" maxlength="6" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 show-sms-detail-logs">
                <table class="grid-sms-detail-logs"></table>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>号码列表
                            </div>
                            <div class="actions">
                                <a class="btn yellow btn-sm btn-sms-select-customer-profiles" href="javascript:;"><i class="fa fa-plus"></i> 从客户列表选取</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group tooltipster" title="多个号码填写格式：一个号码一行" data-tooltipster-position="right">
                                <s:textarea name="sendToList" cssStyle="height: 400px" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>短信内容
                            </div>
                            <div class="actions">
                                <div class="btn-group">
                                    <a data-toggle="dropdown" href="javascript:;" class="btn purple"> <i class="fa fa-plus"></i> 插入表达式 <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right btn-exps">
                                        <li><a href="javascript:;" data-exp="<s:property value="'$'+'{customerProfile.friendlyName}'"/>">客户友好显示 【取值规则：昵称-》真实姓名-》'客户'】</a></li>
                                        <li><a href="javascript:;" data-exp="<s:property value="'#coupons#=50'"/>">887101【海外专营店店搬家 】</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group tooltipster" title="尽量控制文字数量在70字单条短信<br>超过将可能自动拆分为多条进行发送" data-tooltipster-position="right">
                                <s:textarea name="sendContent" cssStyle="height: 400px" />
                            </div>
                        </div>
                    </div>
                </div>
                <s:if test="failureToList!=null">
                    <div class="col-md-3">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>失败列表
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="form-group tooltipster" title="多个号码填写格式：一个号码一行" data-tooltipster-position="left">
                                    <s:textarea name="failureToList" cssStyle="height: 400px" />
                                </div>
                            </div>
                        </div>
                    </div>
                </s:if>
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-md-12">
            <table class="grid-sms-select-customer-profiles"></table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-sms-select-customer-profiles").data("gridOptions", {
            url : "${base}/myt/customer/customer-profile!findByPage?search['NB_mobilePhone']=true",
            colModel : [ {
                name : 'id',
                formatter : function(cellValue, options, rowdata, action) {
                    var url = "${base}/myt/customer/sms-log!view?id=" + rowdata.id;
                    return '<a href="'+url+'" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
                }
            }, {
                label : '昵称',
                name : 'nickName',
                align : 'center',
                width : 100
            }, {
                label : '姓名',
                name : 'trueName',
                editable : true,
                align : 'center',
                width : 100
            }, {
                label : '性别',
                name : 'sex',
                align : 'center',
                editable : true,
                width : 60,
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('genericSexEnum')
                }
            }, {
                label : '电话',
                name : 'mobilePhone',
                editable : true,
                width : 100
            }, {
                label : '邮件',
                name : 'email',
                editable : true,
                width : 200
            }, {
                label : '客户类型',
                name : 'customerNameType',
                align : 'center',
                width : 100,
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheDictDatasByType('IYOUBOX_CUSTOMER_NAME_TYPE')
                }
            }, {
                label : '最后登录时间',
                name : 'lastLogOnTime',
                sorttype : 'date'
            } ],
            multiboxonly : false,
            operations : function(items) {
                var $grid = $(this);
                var $addtolist = $('<li data-position="multi"><a href="javascript:;">批量添加到号码列表</a></li>');
                $addtolist.children("a").bind("click", function(e) {
                    e.preventDefault();
                    var selectedRows = $grid.jqGrid('getGridParam', 'selarrrow');
                    var $sendToList = $grid.closest(".page-edit").find("textarea[name='sendToList']");
                    var curToList = $sendToList.val();
                    var tobeAddList = [];
                    $.each(selectedRows, function(i, rowid) {
                        var rowdata = $grid.jqGrid("getRowData", rowid);
                        var mobilePhone = rowdata.mobilePhone;
                        if (curToList.indexOf(mobilePhone) == -1) {
                            tobeAddList.push(mobilePhone);
                        }
                    })
                    if (tobeAddList.length > 0) {
                        if (curToList == '') {
                            $sendToList.val(tobeAddList.join("\n"));
                        } else {
                            $sendToList.val(curToList + "\n" + tobeAddList.join("\n"));
                        }
                    }
                });
                items.push($addtolist);
            }
        });

        $(".form-edit-sms-log").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                $(".btn-sms-select-customer-profiles", $form).click(function() {
                    Grid.initGrid($('.grid-sms-select-customer-profiles', $form.closest(".page-edit")));
                    App.scrollTo($('.grid-sms-select-customer-profiles', $form.closest(".page-edit")));
                })
                $(".btn-exps  a", $form).click(function() {
                    var $sendContent = $form.find('textarea[name="sendContent"]');
                    $sendContent.val($sendContent.val() + $(this).attr("data-exp"));
                })
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
