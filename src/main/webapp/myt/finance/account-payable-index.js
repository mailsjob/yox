$(function() {
    $(".grid-myt-finance-account-payable-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/finance/account-payable!findByPage',
        colModel : [ {
            label : '往来单位类型',
            name : 'unitType',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('bizTradeUnitTypeEnum')
            },
            width : 80,
            align : 'center'
        }, {
            label : '往来单位名称',
            name : 'unitName',
            width : 150,
            editable : true,
            align : 'left'
        }, {
            label : '应付款金额',
            name : 'accountsPayable',
            width : 70,
            formatter : 'currency'
        }, {
            label : '应收款金额',
            name : 'accountsReceivable',
            width : 70,
            formatter : 'currency'
        }, {
            label : '待结算金额',
            name : 'accountsSettlement',
            width : 70,
            formatter : 'currency'
        } ],
        postData : {
            "search['GT_accountsSettlement']" : "0"
        },
        footerrow : true,
        footerLocalDataColumn : [ 'accountsPayable','accountsReceivable' ,'accountsSettlement'],
        sortname : 'accountsSettlement',
        multiselect : false,
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + '/myt/finance/account-in-out!findByPage',
                colModel : [ {
                    label : '流水号',
                    name : 'id'
                }, {
                    label : '凭证类型',
                    name : 'voucherType',
                    stype : 'select',
                    searchoptions : {
                        value : Util.getCacheEnumsByType('voucherTypeEnum')
                    },
                    width : 80,
                    align : 'center'
                }, {
                    label : '凭证号',
                    name : 'voucher',
                    width : 128,
                    align : 'center'
                }, {
                    label : '会计科目',
                    name : 'accountSubject.display',
                    index : 'accountSubject.code_OR_accountSubject.name',
                    width : 200,
                    align : 'left'
                }, {
                    label : '摘要',
                    name : 'accountSummary',
                    width : 200,
                    align : 'left'
                }, {
                    label : '借贷方向',
                    name : 'accountDirection',
                    width : 60,
                    formatter : function(cellValue, options, rowdata, action) {
                        if (cellValue) {
                            return "借"
                        } else {
                            return "贷"
                        }
                    },
                    align : 'center'
                }, {
                    label : '记账金额',
                    name : 'amount',
                    width : 60,
                    sorttype : 'number',
                    align : 'right'
                }, {
                    label : '过账时间',
                    name : 'documentDate',
                    width : 150,
                    sorttype : 'date',
                    align : 'center'
                }, {
                    label : '记账日期',
                    name : 'postingDate',
                    width : 90,
                    sorttype : 'date',
                    align : 'center'
                } ],
                multiselect : false,
                postData : {
                    "search['FETCH_accountSubject']" : "INNER",
                    "search['IN_accountSubject.code']" : "2202,1122,1123",
                    "search['EQ_bizTradeUnit.id']" : row_id
                }
            });
        }
    });
});
