<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form method="get" class="form-inline form-validation" data-grid-search=".grid-myt-finance-account-subject-sum-index"
			action="#">
			<div class="form-group">
				<input type="text" name="search['CN_accountSubject.code_OR_accountSubject.name']" class="form-control input-large"
					placeholder="会计科目代码/名称...">
			</div>
			<div class="form-group">
				<input type="text" name="search['BT_postingDate']" class="form-control input-medium input-daterangepicker grid-param-data"
					placeholder="记账期间" required="true">
			</div>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-myt-finance-account-subject-sum-index"></table>
	</div>
</div>
<script src="${base}/myt/finance/account-in-out-sum.js" />
<%@ include file="/common/ajax-footer.jsp"%>