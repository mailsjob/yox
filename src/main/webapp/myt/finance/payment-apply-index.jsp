<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-myt-finance-payment-apply">
						<div class="input-group">
							<div class="form-group">
								<input type="text" name="search['CN_voucher_OR_bizTradeUnit.unitName_OR_bizVoucher']" class="form-control  input-large"
									placeholder="单号、往来单位、业务凭证号...">
							</div>
							 <div class="form-group">
								<div class="btn-group">
									只看已付款的数据 <s:checkbox name="search['NN_payTime']" />
								</div>
							</div> 
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default hidden-inline-xs" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-finance-payment-apply" data-voucher='<s:property value="#parameters.voucher"/>'></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/finance/payment-apply-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
