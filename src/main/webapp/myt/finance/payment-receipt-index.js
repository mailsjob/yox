$(function() {
    $(".grid-myt-finance-payment-receipt").data("gridOptions", {
        url : WEB_ROOT + "/myt/finance/payment-receipt!findByPage?search['EQ_receiptType']=FKD",
        colModel : [ {
            label : '业务凭证号',
            name : 'bizVoucher',
            align : 'center',
            width : 150
        }, {
            label : '业务凭证类型',
            name : 'bizVoucherType',
            stype : 'select',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('voucherTypeEnum')
            },
            width : 100
        }, {
            label : '付款完结时间',
            name : 'payTime',
            sorttype : 'date',
            width : 100
        }, {
            label : '凭证号',
            name : 'voucher',
            align : 'center',
            width : 150
        }, {
            label : '凭证日期',
            name : 'voucherDate',
            stype : 'date',
            width : 80
        }, {
            label : '凭证状态',
            name : 'voucherState',
            searchoptions : {
                value : Util.getCacheEnumsByType('voucherStateEnum')
            },
            hidden : true,
            width : 100
        }, {
            label : '结算金额',
            name : 'totalAmount',
            formatter : 'currency',
            width : 60
        }, {
            label : '往来单位',
            name : 'bizTradeUnit.display',
            index : 'bizTradeUnit.unitName',
            width : 150,
            align : 'center'
        }, {
            label : '记账摘要',
            name : 'accountSummary',
            width : 200,
            align : 'left'
        }, {
            label : '最近操作',
            name : 'lastOperationSummary',
            width : 200,
            align : 'left'
        }, {
            label : '当前任务',
            name : 'activeTaskName',
            width : 100,
            align : 'left'
        }, {
            label : '经办人',
            name : 'voucherUser.display',
            index:'voucherUser.signinid_OR_voucherUser.nick',
            width : 80,
            align : 'center'
        }, {
            label : '经办部门',
            name : 'voucherDepartment.display',
            width : 60,
            align : 'center'
        }, {
            label : '参考凭证号',
            name : 'referenceVoucher',
            hidden : true,
            width : 100
        } ],
        editcol : 'voucher',
        addable : false,
        footerrow : true,
        footerLocalDataColumn : [ 'totalAmount' ],
        fullediturl : WEB_ROOT + "/myt/finance/payment-receipt!inputTabs",
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/finance/payment-receipt!tradeReceiptDetails?id=" + row_id,
                colModel : [ {
                    label : '会计科目',
                    name : 'accountSubject.display',
                    width : 150,
                    align : 'left'
                }, {
                    label : '金额',
                    name : 'amount',
                    width : 80,
                    formatter : 'currency'
                }, {
                    label : '关联付款凭证号',
                    name : 'paymentVouchers',
                    width : 100
                }, {
                    label : '付款参考信息',
                    name : 'paymentReference',
                    width : 200
                }, {
                    label : '记账摘要',
                    name : 'accountSummary',
                    width : 150
                }, {
                    label : '备注',
                    name : 'memo',
                    width : 150
                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
});
