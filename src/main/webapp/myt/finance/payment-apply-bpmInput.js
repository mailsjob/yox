$(function() {
    $(".form-myt-finance-payment-apply-bpmInput").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            var $grid = $form.find(".grid-myt-finance-payment-apply-bpmInput");

            Biz.setupBizTradeUnitSelect($form);
            $form.find("input[name='accountSubject.display']").treeselect({
                url : WEB_ROOT + "/myt/finance/account-subject!findPaymentAccountSubjectsForPayable",
                callback : {
                    onSingleClick : function(event, treeId, treeNode) {
                        $form.setFormDatas({
                            'accountSubject.id' : treeNode.id,
                            'accountSubject.display' : treeNode.display
                        }, true);
                    },
                    onClear : function(event) {
                        $form.setFormDatas({
                            'accountSubject.id' : '',
                            'accountSubject.display' : ''
                        }, true);
                    }
                }
            });
           
        }
    });

   
});
