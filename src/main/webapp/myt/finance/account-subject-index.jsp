<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-menu">会计科目列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div id="tab-menu" class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search"
						data-grid-search=".grid-account-subject-list">

						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_code_OR_name']" class="form-control" placeholder="代码、名称...">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-account-subject-list" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-account-subject-list").data("gridOptions", {
            url : "${base}/myt/finance/account-subject!findByPage",
            colNames : [ '名称', '代码', '借贷方向', '描述' ],
            colModel : [ {
                name : 'name',
                width : 100,
                editable : true,
                align : 'left'
            }, {
                name : 'code',
                width : 60,
                align : 'center',
                editable : true

            }, {
                name : 'balanceDirection',
                width : 40,
                align : 'center',
                edittype : 'checkbox',
                editable : true
            }, {
                name : 'memo',
                editable : true
            } ],
            cmTemplate : {
                sortable : false
            },
            sortorder : "asc",
            sortname : 'code',
            multiselect : false,
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initRecursiveSubGrid(subgrid_id, row_id, "parent.id");
            },
            editurl : "${base}/myt/finance/account-subject!doSave",
            delurl : "${base}/myt/finance/account-subject!doDelete",
            inlineNav : {
                add : true
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>