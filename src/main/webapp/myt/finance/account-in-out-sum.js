$(function() {
    $(".grid-myt-finance-account-subject-sum-index").data("gridOptions", {
        url : WEB_ROOT + "/myt/finance/account-in-out!findByGroupAccountSubject",
        colModel : [ {
            name : 'accountSubject.id',
            hidden : true,
            hidedlg : true
        }, {
            label : '科目代码',
            name : 'accountSubject.code',
            width : 60,
            align : 'center',
            editable : true

        }, {
            label : '科目名称',
            name : 'accountSubject.name',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '本期发生额',
            name : 'sum(directionAmount)',
            width : 60,
            sorttype : 'number',
            align : 'right'
        } ],
        cmTemplate : {
            sortable : false
        },
        sortname : "accountSubject.code",
        footerrow : true,
        footerLocalDataColumn : [ 'sum(directionAmount)' ],
        multiselect : false,
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            var $grid = $(this);
            var rowdata = $grid.getRowData(row_id);
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/finance/account-in-out!findByPage",
                colModel : [ {
                    label : '流水号',
                    name : 'id'
                }, {
                    label : '凭证类型',
                    name : 'voucherType',
                    stype : 'select',
                    searchoptions : {
                        value : Util.getCacheEnumsByType('voucherTypeEnum')
                    },
                    width : 60,
                    align : 'center'
                }, {
                    label : '凭证号',
                    name : 'voucher',
                    width : 128,
                    align : 'center'
                }, {
                    label : '会计科目',
                    name : 'accountSubject.display',
                    index : 'accountSubject.code_OR_accountSubject.name',
                    width : 200,
                    align : 'left'
                }, {
                    label : '摘要',
                    name : 'accountSummary',
                    width : 200,
                    align : 'left'
                }, {
                    label : '借贷方向',
                    name : 'accountDirection',
                    width : 60,
                    formatter : function(cellValue, options, rowdata, action) {
                        if (cellValue) {
                            return "借"
                        } else {
                            return "贷"
                        }
                    },
                    align : 'center'
                }, {
                    label : '发生金额',
                    name : 'directionAmount',
                    width : 60,
                    sorttype : 'number',
                    align : 'right'
                }, {
                    label : '记账日期',
                    name : 'postingDate',
                    width : 90,
                    sorttype : 'date',
                    align : 'center'
                }, {
                    label : '往来单位类型',
                    name : 'bizTradeUnit.unitType',
                    stype : 'select',
                    searchoptions : {
                        value : Util.getCacheEnumsByType('bizTradeUnitTypeEnum')
                    },
                    width : 80,
                    align : 'center'
                }, {
                    label : '往来单位名称',
                    name : 'bizTradeUnit.unitName',
                    width : 150,
                    editable : true,
                    align : 'left'
                }, {
                    label : '结存账期',
                    name : 'accountSettlement.display',
                    index : 'accountSettlement',
                    width : 100,
                    align : 'left'
                }, {
                    label : '过账时间',
                    name : 'documentDate',
                    width : 150,
                    sorttype : 'date',
                    align : 'center'
                } ],
                footerrow : true,
                footerLocalDataColumn : [ 'directionAmount' ],
                postData : {
                    "search['EQ_accountSubject.id']" : rowdata['accountSubject.id'],
                    "search['BT_postingDate']" : $grid.getDataFromBindSeachForm("search['BT_postingDate']"),
                    "search['FETCH_accountSubject']" : "INNER",
                    "search['FETCH_accountSettlement']" : "LEFT",
                    "search['FETCH_bizTradeUnit']" : "LEFT"
                }
            });
        }
    });
});