$(function() {
   $(".form-myt-finance-payment-receipt-inputBasic").data("formOptions", {
        updateTotalAmount : function() {
            var $form = $(this);

            var $grid = $form.find(".grid-myt-finance-payment-receipt-inputBasic");
            var userData = {
                "commodity.display" : "合计："
            };
            userData.amount = $grid.jqGrid('sumColumn', 'amount');
            $grid.jqGrid("footerData", "set", userData, true);
            $form.setFormDatas({
                totalAmount : userData.amount
            });
        },
        bindEvents : function() {
            var $form = $(this);
            var $grid = $form.find(".grid-myt-finance-payment-receipt-inputBasic");
            Biz.setupBizTradeUnitSelect($form);

            $form.find("input[name='tradeTarget']").click(function() {
                $(this).closest(".form-group").find("input,select").attr("disabled", true);
            });

            $form.find("input[name='accountSubject.display']").treeselect({
                url : WEB_ROOT + "/myt/finance/account-subject!findPaymentAccountSubjectsForPayable",
                callback : {
                    onSingleClick : function(event, treeId, treeNode) {
                        $form.setFormDatas({
                            'accountSubject.id' : treeNode.id,
                            'accountSubject.display' : treeNode.display
                        }, true);
                    },
                    onClear : function(event) {
                        $form.setFormDatas({
                            'accountSubject.id' : '',
                            'accountSubject.display' : ''
                        }, true);
                    }
                }
            });
        }
    });

    $(".grid-myt-finance-payment-receipt-inputBasic").data("gridOptions", {
        calcRowAmount : function(rowdata, src) {
        },
        updateRowAmount : function(src) {
            var $grid = $(this);
            var rowdata = $grid.jqGrid("getEditingRowdata");
            $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
            $grid.jqGrid("setEditingRowdata", rowdata);
        },
        batchEntitiesPrefix : "tradeReceiptDetails",
        url : function() {
            var pk = $(this).attr("data-pk");
            if (pk) {
                return WEB_ROOT + "/myt/finance/payment-receipt!tradeReceiptDetails?id=" + pk;
            }
        },
        colModel : [ {
            label : '所属交易凭据主键',
            name : 'tradeReceipt.id',
            hidden : true,
            hidedlg : true,
            editable : true,
            formatter : function(cellValue, options, rowdata, action) {
                var pk = $(this).attr("data-pk");
                return pk ? pk : "";
            }
        }, {
            name : 'accountSubject.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '出账会计科目',
            name : 'accountSubject.display',
            editable : true,
            width : 120,
            editrules : {
                required : true
            },
            editoptions : {
                placeholder : '单击选择会计科目',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.treeselect({
                        url : WEB_ROOT + "/myt/finance/account-subject!findPaymentAccountSubjects",
                        callback : {
                            onSingleClick : function(event, treeId, treeNode) {
                                $elem.val(treeNode.name);
                                var rowdata = $grid.jqGrid("getEditingRowdata");
                                rowdata['accountSubject.id'] = treeNode.id;
                                $grid.jqGrid("setEditingRowdata", rowdata);
                            },
                            onClear : function(event) {
                                var rowdata = $grid.jqGrid("getEditingRowdata");
                                rowdata['accountSubject.id'] = '';
                                $elem.val("");
                                $grid.jqGrid("setEditingRowdata", rowdata);
                            }
                        }
                    });
                }
            },
            align : 'left'
        }, {
            label : '金额',
            name : 'amount',
            width : 80,
            formatter : 'currency',
            editable : true,
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.change(function() {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            },
            responsive : 'sm'
        }, {
            label : '关联凭证',
            name : 'paymentVouchers',
            width : 100,
            editable : true

        } , {
            label : '付款参考信息',
            name : 'paymentReference',
            edittype : 'textarea',
            width : 300,
            editable : true
        }, {
            label : '记账摘要',
            name : 'accountSummary',
            width : 150
        }, {
            label : '备注',
            name : 'memo',
            width : 150,
            editable : true

        } ],
        footerrow : true,
        beforeInlineSaveRow : function(rowid) {
            var $grid = $(this);
            $grid.data("gridOptions").updateRowAmount.call($grid);
        },
        afterInlineSaveRow : function(rowid) {
            var $grid = $(this);
            // 整个Form相关金额字段更新
            var $form = $grid.closest("form");
            $form.data("formOptions").updateTotalAmount.call($form);
        },
        afterInlineDeleteRow : function(rowid) {
            var $grid = $(this);
            // 整个Form相关金额字段更新
            var $form = $grid.closest("form");
            $form.data("formOptions").updateTotalAmount.call($form);
        },
        userDataOnFooter : true
    });

});
