<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-finance-payment-apply-bpmInput"
	action="${base}/myt/finance/payment-apply!bpmSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
				data-ajaxify-reload="_closest-ajax-container">
				<i class="fa fa-check"></i>
				<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
			</s3:button>
			<button class="btn default btn-cancel" type="button">取消</button>
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4 col-md-offset-8">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:textfield name="voucher" readonly="%{notNew}" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:select name="voucherUser.id" list="usersMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:select name="voucherDepartment.id" list="departmentsMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<s3:datetextfield name="voucherDate" format="date" current="true" readonly="%{notNew}" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">收款单位</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-biz-trade-unit"></i>
							<s:textfield name="bizTradeUnit.display" readonly="%{notNew}" value="%{#parameters.bizTradeUnitName}" />
							<s:hidden name="bizTradeUnit.id" value="%{#parameters.bizTradeUnitId}" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">结算科目</label>
					<div class="controls">
						<s:textfield name="accountSubject.display" requiredLabel="true" />
						<s:hidden name="accountSubject.id" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">申请付款金额</label>
					<div class="controls">
						<s:textfield name="bizShouldPayAmount" readonly="%{notNew}" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">业务凭证号</label>
					<div class="controls">
						<s:textfield name="bizVoucher" value="%{#parameters.bizVoucher}" readonly="%{notNew}" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">凭证类型</label>
					<div class="controls">
						<s:select name="bizVoucherType" list="#application.enums.voucherTypeEnum" readonly="%{notNew}" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">记账摘要</label>
					<div class="controls">
						<s:textfield name="accountSummary" />
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">提交审批</label>
					<div class="controls text-warning">
						<s:checkbox name="submitToAudit" disabled="%{submitDate!=null}" label="勾选此项将会立即提交审批流程，在此期间不可修改数据！" value="true" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
			data-ajaxify-reload="_closest-ajax-container">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>

</form>
<script src="${base}/myt/finance/payment-apply-bpmInput.js" />
<%@ include file="/common/ajax-footer.jsp"%>
