$(function() {
    $(".grid-myt-finance-account-in-out-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/finance/account-in-out!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id'
        }, {
            label : '凭证类型',
            name : 'voucherType',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('voucherTypeEnum')
            },
            width : 60,
            align : 'center'
        }, {
            label : '凭证号',
            name : 'voucher',
            width : 128,
            align : 'center'
        }, {
            label : '会计科目',
            name : 'accountSubject.display',
            index : 'accountSubject.code_OR_accountSubject.name',
            width : 200,
            align : 'left'
        }, {
            label : '摘要',
            name : 'accountSummary',
            width : 200,
            align : 'left'
        }, {
            label : '借贷方向',
            name : 'accountDirection',
            width : 60,
            formatter : function(cellValue, options, rowdata, action) {
                if (cellValue) {
                    return "借"
                } else {
                    return "贷"
                }
            },
            align : 'center'
        }, {
            label : '记账金额',
            name : 'amount',
            width : 60,
            formatter : 'currency',
            align : 'right'
        }, {
            label : '记账日期',
            name : 'postingDate',
            width : 90,
            sorttype : 'date',
            align : 'center'
        }, {
            label : '往来单位类型',
            name : 'bizTradeUnit.unitType',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('bizTradeUnitTypeEnum')
            },
            width : 80,
            align : 'center'
        }, {
            label : '往来单位名称',
            name : 'bizTradeUnit.unitName',
            width : 150,
            editable : true,
            align : 'left'
        }, {
            label : '结存账期',
            name : 'accountSettlement.display',
            index : 'accountSettlement',
            width : 100,
            align : 'left'
        }, {
            label : '过账时间',
            name : 'documentDate',
            width : 150,
            sorttype : 'date',
            align : 'center'
        } ],
        footerrow : true,
        footerLocalDataColumn : [ 'amount' ],
        postData : {
            "search['FETCH_accountSubject']" : "INNER",
            "search['FETCH_accountSettlement']" : "LEFT",
            "search['FETCH_bizTradeUnit']" : "LEFT"
        },
        editurl : WEB_ROOT + '/myt/finance/account-in-out!doSave',
        delurl : WEB_ROOT + '/myt/finance/account-in-out!doDelete',
        fullediturl : WEB_ROOT + '/myt/finance/account-in-out!inputTabs'
    });
});