$(function () {
    $(".grid-myt-finance-pay-notify-history-index").data("gridOptions", {
        url: WEB_ROOT + '/myt/finance/pay-notify-history!findByPage',
        colModel: [{
            label: '流水号',
            name: 'id'
        }, {
            label: '付款流水号',
            name: 'paySeq',
            width: 200,
            align: 'left'
        }, {
            label: '上传支付网关唯一号码',
            name: 'outTradeNo',
            width: 200,
            align: 'center'
        }, {
            label: '支付网关交易号',
            name: 'tradeNo',
            width: 200,
            align: 'left'
        }, {
            label: '状态',
            name: 'processStatus',
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('processStatusEnum')
            },
            width: 80,
            align: 'center'
        }, {
            label: '付款类型',
            name: 'payType',
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('payTypeEnum')
            },
            width: 80,
            align: 'center'
        }, {
            label: '付款方式',
            name: 'payMode',
            width: 60,
            align: 'center'
        }, {
            label: '交易金额',
            name: 'totalFee',
            width: 60,
            formatter: 'currency',
            align: 'right'
        }, {
            label: '买家邮箱',
            name: 'buyerEmail',
            width: 120,
            align: 'center'
        }, {
            label: '交易信息',
            name: 'processInfo',
            index: 'processInfo',
            width: 100,
            align: 'left'
        }, {
            label: '创建时间',
            name: 'createdDate',
            width: 100,
            align: 'center'
        }, {
            label: '客户',
            name: 'customerProfile.id',
            width: 100,
            align: 'center'
        }],
        multiselect: false,
        footerrow : true,
        footerLocalDataColumn : [ 'totalFee' ]
    });
});