<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="form-horizontal form-bordered form-label-stripped">
	<div class="row">
		<s:if test="#attr.taskVariablesVar['auditLevel1Time']!=null">
			<div class="col-md-6">
				<div class="portlet gren">
					<div class="portlet-title">
						<div class="caption">一线审核</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<label class="control-label">通过:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.booleanLabel[#attr.taskVariablesVar['auditLevel1Pass']]" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审核人员:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#attr.taskVariablesVar['auditLevel1User']" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审核时间:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:date name="#attr.taskVariablesVar['auditLevel1Time']" format="yyyy-MM-dd HH:mm:ss" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审核意见:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#attr.taskVariablesVar['auditLevel1Explain']" />
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</s:if>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">经办人</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="voucherUser.display" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">部门</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="voucherDepartment.display" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">凭证编号</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="voucher" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">业务凭证号</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="bizVoucher"/>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">业务凭证类型</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="#application.enums.voucherTypeEnum[bizVoucherType]" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">业务申请付款金额</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="bizShouldPayAmount" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">结算科目</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="accountSubject.display" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">收款单位</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="bizTradeUnit.display" />
					</p>
				</div>
			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">记账摘要</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="accountSummary" />
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/common/ajax-footer.jsp"%>
