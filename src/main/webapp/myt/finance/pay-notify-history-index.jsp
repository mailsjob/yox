<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">列表查询</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-myt-finance-pay-notify-history-index">
						<div class="form-group" style="width: 500px;">
							<input type="text" name="search['CN_paySeq_OR_outTradeNo_OR_tradeNo_OR_buyerEmail']" 
							class="form-control" placeholder="付款流水号、上传支付网关唯一号码、支付网关交易号、买家邮箱...">
						</div>
						<div class="form-group">
							<input id="search-created-date" type="text" name="search['BT_createdDate']" class="form-control input-medium input-daterangepicker"
								placeholder="创建日期">
						</div>
						<script type="text/javascript">
							Date.prototype.Format = function(fmt){ //author: meizz   
							  var o = {   
							    "M+" : this.getMonth()+1,                 //月份   
							    "d+" : this.getDate(),                    //日   
							    "h+" : this.getHours(),                   //小时   
							    "m+" : this.getMinutes(),                 //分   
							    "s+" : this.getSeconds(),                 //秒   
							    "q+" : Math.floor((this.getMonth()+3)/3), //季度   
							    "S"  : this.getMilliseconds()             //毫秒   
							  };   
							  if(/(y+)/.test(fmt))   
							    fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
							  for(var k in o)   
							    if(new RegExp("("+ k +")").test(fmt))   
							  fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
							  return fmt;   
							}  
							$(function(){
								var now = new Date().Format("yyyy-MM-dd");
								$("input#search-created-date").val(now+" ～ "+now);
							});
						</script>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-finance-pay-notify-history-index"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/finance/pay-notify-history-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
