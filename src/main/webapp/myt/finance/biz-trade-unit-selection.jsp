<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form action="#" method="get" class="form-inline form-validation form-search-init"
			data-grid-search=".grid-myt-finance-biz-trade-unit-selection">
			<div class="form-group">
				<input type="text" name="search['CN_unitName']" class="form-control input-xlarge" placeholder="往来单位名称..." />
			</div>
			<div class="form-group">
				<s:checkboxlist name="search['IN_unitType']" list="#application.enums.bizTradeUnitTypeEnum" />
			</div>
			<button class="btn default" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>

		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-myt-finance-biz-trade-unit-selection"></table>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-finance-biz-trade-unit-selection").data("gridOptions", {
            url : '${base}/myt/finance/biz-trade-unit!findByPage',
            colModel : [ {
                label : '往来单位类型',
                name : 'unitType',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('bizTradeUnitTypeEnum')
                },
                width : 80,
                align : 'center'
            }, {
                label : '往来单位名称',
                name : 'unitName',
                width : 300,
                editable : true,
                align : 'left'
            }, {
                name : 'display',
                hidden : true,
                width : 150
            } ],
            sortname : 'unitName',
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
