<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form
	class="form-horizontal form-bordered form-label-stripped form-validation form-myt-finance-receivables-receipt-inputBasic"
	action="${base}/myt/finance/receivables-receipt!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
				data-ajaxify-reload="_closest-ajax-container">
				<i class="fa fa-check"></i>
				<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
			</s3:button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<button class="btn red pull-right btn-post-url" type="button"  
				data-url="${base}/myt/finance/receivables-receipt!chargeAgainst?id=<s:property value='%{id}'/>"
				data-confirm="确认冲销当前收款单？"  data-close-container="true">
				<s:property value="%{disallowChargeAgainst!=null?disallowChargeAgainst:'红冲'}" />
			</button> 
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4 col-md-offset-8">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:textfield name="voucher" readonly="%{notNew}" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:select name="voucherUser.id" list="usersMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:select name="voucherDepartment.id" list="departmentsMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<s3:datetextfield name="voucherDate" format="date" current="true" readonly="%{notNew}" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">申请收款金额</label>
					<div class="controls">
						<s:textfield name="bizShouldPayAmount" readonly="%{notNew}" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">付款单位</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-biz-trade-unit"></i>
							<s:textfield name="bizTradeUnit.display" readonly="%{notNew}" />
							<s:hidden name="bizTradeUnit.id" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">结算科目</label>
					<div class="controls">
						<s:textfield name="accountSubject.display" readonly="%{notNew}" />
						<s:hidden name="accountSubject.id" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">业务凭证号</label>
					<div class="controls">
						<s:textfield name="bizVoucher" readonly="%{notNew}" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">业务凭证类型</label>
					<div class="controls">
						<s:select name="bizVoucherType" list="#application.enums.voucherTypeEnum" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">摘要说明</label>
					<div class="controls">
						<s:textfield name="accountSummary" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-finance-receivables-receipt-inputBasic" data-grid="items" data-pk='<s:property value="id"/>'
					data-readonly='<s:property value="%{notNew}"/>'></table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="grid-myt-finance-payment-receipt-toPayList"></table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">总计收款额</label>
					<div class="controls">
						<s:textfield name="totalAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">关联凭证</label>
					<div class="controls">
						<s:textfield name="paymentVouchers" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收款备注</label>
					<div class="controls">
						<s:textarea name="paymentReference" rows="5" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
			data-ajaxify-reload="_closest-ajax-container">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>

</form>
<script src="${base}/myt/finance/receivables-receipt-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
