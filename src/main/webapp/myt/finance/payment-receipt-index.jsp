<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">列表查询</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-myt-finance-payment-receipt">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_voucher_OR_bizTradeUnit.unitName_OR_bizVoucher']" class="form-control"
									placeholder="单号、往来单位、业务凭证号...">
							</div>
							<span class="input-group-btn">
								<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
									凭证状态 <i class="fa fa-angle-down"></i>
								</button>
								<div class="dropdown-menu hold-on-click dropdown-checkboxes">
									<s:checkboxlist name="search['IN_voucherState']" list="#application.enums.voucherStateEnum"
										value="#application.enums.voucherStateEnum.keys.{? #this.name()!='REDW'}" />
								</div>
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default hidden-inline-xs" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-finance-payment-receipt"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/finance/payment-receipt-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
