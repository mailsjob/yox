<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">

		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">明细账</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/finance/account-payable">应付账款</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/finance/account-receivable">应收账款</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/finance/account-in-out!forward?_to_=sum">总帐</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/sale/sale-delivery-detail!forward?_to_=sum">销售商品毛利</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/sale/sale-delivery!forward?_to_=sum">销售单毛利</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/sale/sale-delivery!forward?_to_=saler-sum">销售人员业绩</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-myt-finance-account-in-out-index">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['EQ_voucher']" class="form-control" placeholder="凭证号...">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default hidden-inline-xs" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-finance-account-in-out-index"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/finance/account-in-out-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
