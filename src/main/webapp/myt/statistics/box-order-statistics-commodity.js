$(function() {
    $(".grid-myt-sale-box-order-statistics-commodity").data("gridOptions", {
        url : WEB_ROOT + '/myt/statistics/box-order-statistics!dispatcher',
        colModel : [ {
            label : '商品sku',
            width : 100,
            name : 'commoditySku',
            align : 'center'
        }, {
            label : '商品名称',
            width : 300,
            name : 'commodityTitle'
        }, {
            label : '数量',
            align : 'center',
            width : 100,
            name : 'quantity'
        }, {
            label : '订单行号',
            align : 'center',
            width : 100,
            name : 'orderNum',
            hidden : true
        } ],
        /*grouping : true,
        groupingView : {
            groupField : [ 'boxOrderDetail.reserveDeliveryMonth' ],
            groupOrder : [ 'asc' ],
            groupCollapse : false
        },*/
        sortname : 'quantity',
        sortorder: 'desc',
        rowNum : 500,
    });
});