$(function() {
    $(".grid-myt-import-user-statistics").data("gridOptions", {
        url : WEB_ROOT + '/myt/statistics/import-user-statistics!dispatcher',
        colModel : [ {
            label : '导入日期',
            width : 100,
            name : 'registerTime',
            align : 'center'
        }, {
            label : '总导入数量',
            width : 300,
            align : 'center',
            name : 'total'
        }, {
            label : '未激活用户数量',
            align : 'center',
            width : 100,
            name : 'notActivated'
        }, {
            label : '已激活用户数量',
            align : 'center',
            width : 100,
            name : 'activated'
        }],
        /*grouping : true,
        groupingView : {
            groupField : [ 'boxOrderDetail.reserveDeliveryMonth' ],
            groupOrder : [ 'asc' ],
            groupCollapse : false
        },*/
        sortname : 'registerTime',
        sortorder: 'desc',
        rowNum : 500,
    });
});