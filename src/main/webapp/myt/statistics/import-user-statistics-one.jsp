<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form method="get" class="form-inline form-validation form-search-init"
			data-grid-search=".grid-myt-import-user-statistics" action="#">
			<div class="form-group">
				<div class="input-group input-medium">
				<input type="text" name="date"
					class="form-control input-medium input-daterangepicker grid-param-data" placeholder="选择导入日期" pattern="" >
				</div>
			</div>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-myt-import-user-statistics" ></table>
	</div>
</div>
<script src="${base}/myt/statistics/import-user-statistics-one.js" />
<%@ include file="/common/ajax-footer.jsp"%>