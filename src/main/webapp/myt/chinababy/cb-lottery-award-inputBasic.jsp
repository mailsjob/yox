<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-cb-lottery-award"
	action="${base}/myt/chinababy/cb-lottery-award!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-cb-lottery-award-list">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">图片</label>
					<div class="col-md-8">
						<s:hidden name="awardImg" data-singleimage="true" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">抽奖码</label>
							<div class="col-md-8">
								<s:textfield name="lotteryCode" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">奖品码</label>
							<div class="col-md-8">
								<s:textfield name="awardCode" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">奖品名</label>
							<div class="col-md-8">
								<s:textfield name="awardName" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">奖品类型</label>
							<div class="col-md-8">
								<s:select name="awardType" list="#application.enums.awardTypeEnum" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">启用标识</label>
					<div class="col-md-8">
						<s:radio name="enable" list="#application.enums.booleanLabel"/>
					</div>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">地区</label>
					<div class="col-md-8">
						<s:select list="regionsMap" name="region.id"></s:select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">总数</label>
					<div class="col-md-8">
						<s:textfield name="awardTotal" />
					</div>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">中奖率</label>
					<div class="col-md-8">
						<s:textfield name="winRate" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">待发送数</label>
					<div class="col-md-8">
						<s:textfield name="awardWinUnsent" />
					</div>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">剩余数</label>
					<div class="col-md-8">
						<s:textfield name="awardRemain" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">发送数</label>
					<div class="col-md-8">
						<s:textfield name="awardSent" />
					</div>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">排序号</label>
					<div class="col-md-8">
						<s:textfield name="orderIndex" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">商品</label>
					<div class="col-md-10">
						<div class="input-icon right">
							<i class="fa fa-search fa-search-commodity"></i> <i class="fa fa-times fa-times-commodity input-icon-secondary"></i>
							<s:textfield name="commodity.display" readonly="true" />
							<s:hidden name="commodity.id"/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">备注</label>
					<div class="col-md-10">
						<s:textarea name="memo" rows="3"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-cb-lottery-award-list">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {
        $(".form-edit-cb-lottery-award").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                $form.find("i.fa-search-commodity").click(function() {
                    $(this).popupDialog({
                        url : '${base}/myt/md/commodity!forward?_to_=selection',
                        title : '选取商品',
                        callback : function(json) {
                            $form.find("[name$='commodity.display']").val(json.sku + " " + json.title);
                            $form.find("[name$='commodity.id']").val(json.id);
                        }
                    })
                });

                $form.find("i.fa-times-commodity").click(function() {
                    $form.find("[name$='commodity.display']").val("");
                    $form.find("[name$='commodity.id']").val("");
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>