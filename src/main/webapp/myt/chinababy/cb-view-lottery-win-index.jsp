<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab">获奖列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search"
						data-grid-search=".grid-view-lottery-win-list">
						<div class="input-group">
							<div class="input-cont">
								<input type="text"
									name="search['CN_customerProfile.customerName_OR_customerProfile.nickName_OR_customerProfile.trueName']"
									class="form-control" placeholder="vip卡号、客户">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>

								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-view-lottery-win-list" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-view-lottery-win-list").data("gridOptions", {
            url : '${base}/myt/chinababy/cb-view-lottery-win!findByPage',
            colNames : [  '流水号','奖品类型', '领奖时间' ,'客户vip卡号','客户电话','客户地址','客户','宝宝性别','宝宝生日','销售单号', '物流公司','物流单号','状态'],
            colModel : [ {
                name : 'id'                            
            }, {
                name : 'awardType',
                align : 'center',
                width:100,
                stype : 'select',
                searchoptions : { 
                    value : Util.getCacheEnumsByType('awardTypeEnum') 
               	}
               
            }, {
                name : 'createdDate',
                sorttype : 'date',
              	align : 'center'
            }, {
                name : 'customerProfile.customerName',
               	width:80
            }, {
                name : 'customerProfile.mobilePhone'
			}, {
                name : 'customerProfile.deliveryAddr'
            }, {
                name : 'customerProfile.display',
                index : 'customerProfile.nickName',
                width : 150,
                align : 'left',
                formatter : function(cellValue, options, rowdata, action) {
                    var url = "${base}/myt/customer/customer-profile!view?id=" + rowdata.customerProfile.id;
                    return '<a href="'+url+'" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
                }
            }, {
                name : 'customerProfile.babySex',
                width : 70,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('genericSexEnum')
                }
            }, {
                name : 'customerProfile.babyBirthday',
                width : 150,
                sorttype : 'date',
                align : 'center'
            }, {
                name : 'saleDeliveryNo',
                width:200,
                align : 'left',
                editable : true
            }, {
                editable:true,
                name : 'logistics.id',
                align : 'center',
                stype : 'select',
                editoptions : {
                    value : Biz.getLogisticsDatas()
                },
                width : 200
            }, {
                name : 'logisticsNo',
                width:200,
                align : 'left',
                editable : true
            }, {
                name : 'winStatus',
                 width:100,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('winStatusEnum')
                },
                editable : true
            } ],
            postData : {
              "search['FETCH_customerProfile']" : "INNER"
            },
            editurl : "${base}/myt/chinababy/cb-view-lottery-win!doSave",
            editcol : 'id',
            operations : function(itemArray) {

                var $select3 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 申通</a></li>');
                $select3.children("a").bind("click", function(e) {
                    e.preventDefault();
                    var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                    var url = WEB_ROOT + "/rpt/jasper-report!preview?format=XLS&report=EXPRESS_SHENTONG2&contentDisposition=attachment";
                    var rowDatas = $grid.jqGrid("getSelectedRowdatas");
                    for (i = 0; i < rowDatas.length; i++) {
                        var rowData = rowDatas[i];
                        url += "&reportParameters['LOTTERY_WIN_IDS']=" + rowData['id'];

                    }
                    window.open(url, "_blank");
                });
                itemArray.push($select3);

            },
            sortorder : "desc",
            sortname : "createdDate",
            contextMenu : false,
            multiselect : true,
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : "${base}/myt/chinababy/cb-lottery-win!getLotteryWinDetails?id=" + row_id,
                    colNames : [ '奖品','商品','地区'],
                    colModel : [{
                         name : 'awardName',
                         width:120
                            	
                     }, {
                        name : 'commodity.display'
                      	
                    }, {
                        name : 'lotteryAward.region.name',
                        width:100,
                        align : 'left'
                    }],
                    filterToolbar : false,
                    toppager : false,
                    pager:false,
                   	multiselect : false
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
