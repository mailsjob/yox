<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">获奖列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search" data-grid-search="#grid-cbLotteryWin-list"
						action="#">
						<div class="input-group">
							<div class="input-cont">
								<input type="text"
									name="search['CN_awardName_OR_lotteryCode_OR_awardCode_commodity.title_OR_customerProfile.nickName']"
									class="form-control" placeholder="奖品名/奖品码/抽奖码">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table id="grid-cbLotteryWin-list" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
    $(function() {
        $("#grid-cbLotteryWin-list").data("gridOptions", {
            url : '${base}/myt/chinababy/cb-lottery-win!findByPage',
            colNames : [ '流水号', '时间', '显示客户名', '奖品类型', '状态', '客户id', '客户vip卡号',  '客户电话','客户地址', '客户名','订单号','销售单号','物流公司', '物流单号' ],
            colModel : [ {
                name : 'id'
            }, {
                name : 'createdDate',
                width : 150,
                sorttype : 'date',
                align : 'center'
            }, {
                name : 'displayCustomerName',
                width : 100,
                editable : true,
                align : 'left'
            }, {
                name : 'awardType',
                width : 100,
                editable : true,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('winAwardTypeEnum')
                }
            }, {
                name : 'winStatus',
                width : 100,
                editable : true,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('winStatusEnum')
                }
            }, {
                name : 'customerProfile.id',
                hidden : true
            }, {
                name : 'customerProfile.customerName',
                width : 80
            }, {
                name : 'customerProfile.mobilePhone'
			}, {
                name : 'customerProfile.deliveryAddr'

            }, {
                name : 'customerProfile.display',
                index : 'customerProfile.nickName',
                width : 60,
                align : 'left',
                formatter : function(cellValue, options, rowdata, action) {
                    var url = "${base}/myt/customer/customer-profile!view?id=" + rowdata.customerProfile.id;
                    return '<a href="'+url+'" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
                }
            }, {
                name : 'boxOrder.orderSeq',
                width:200,
                align : 'left'
            }, {
                name : 'saleDeliveryNo',
                width:200,
                align : 'left',
                editable : true
            }, {
                editable:true,
                name : 'logistics.id',
                align : 'center',
                stype : 'select',
                editoptions : {
                    value : Biz.getLogisticsDatas()
                },
                width : 200
            }, {
                name : 'logisticsNo',
                width:200,
                align : 'left',
                editable : true
            } ],
			sortorder : "desc",
            sortname : "createdDate",
            editurl : "${base}/myt/chinababy/cb-lottery-win!doUpdate",
            //delurl : "${base}/myt/chinababy/cb-lottery-win!doDelete",
            fullediturl : "${base}/myt/chinababy/cb-lottery-win!inputTabs",
            viewurl : "${base}/myt/chinababy/cb-lottery-win!viewTabs",
            operations : function(itemArray) {
                var $grid = $(this);
                var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>生成订单</a></li>');
                $select.children("a").bind("click", function(e) {
                    e.preventDefault();
                    var ids = $grid.getAtLeastOneSelectedItem();
                    if (ids) {
                        var url = '${base}/myt/chinababy/cb-lottery-win!biuldOrder';
                        $grid.ajaxPostURL({
                            url : url,
                            success : function() {
                                $grid.refresh();
                            },
                            confirmMsg : "确认 生成订单？",
                            data : {
                                ids : ids.join(",")
                            }
                        })
                    }
                });
                itemArray.push($select);
                var $select2 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>更新发货状态</a></li>');
                $select2.children("a").bind("click", function(e) {
                    e.preventDefault();
                    var ids = $grid.getAtLeastOneSelectedItem();
                    if (ids) {
                        var url = '${base}/myt/chinababy/cb-lottery-win!updateDelivery';
                        $grid.ajaxPostURL({
                            url : url,
                            success : function() {
                                $grid.refresh();
                            },
                            confirmMsg : '确认更新？',
                            data : {
                                ids : ids.join(",")
                            }
                        })
                    }
                 });
                itemArray.push($select2);
            },
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : "${base}/myt/chinababy/cb-lottery-win!getLotteryWinDetails?id=" + row_id,
                    colNames : [ '流水号', '奖品', '显示奖品名', '商品', '地区' ],
                    colModel : [ {
                        name : 'id'
                    }, {
                        name : 'lotteryAward.display',
                        width : 250
                    }, {
                        name : 'awardName',
                        width : 120
				 	}, {
                        name : 'commodity.display'
					}, {
                        name : 'lotteryAward.region.name',
                        width : 100,
                        align : 'left'
                    } ],
                    filterToolbar : false,
                    toppager : false,
                    multiselect : false,
                    //editurl : "${base}/myt/chinababy/cb-lottery-win-detail!doUpdate",
                    //delurl : "${base}/myt/chinababy/cb-lottery-win-detail!doDelete"

                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>