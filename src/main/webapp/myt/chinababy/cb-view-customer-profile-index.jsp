<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">客户列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-cb-view-customer-profile-list">
						<div class="form-group">
							<label class="sr-only">客户</label> <input type="text"
								name="search['CN_nickName_OR_trueName_OR_mobilePhone_OR_email']" class="form-control input-xlarge"
								placeholder="昵称、姓名、手机号码、邮件地址..." />
						</div>
						<div class="form-group">
							<input type="checkbox" name="search['NN_cbLotteryWins.customerProfile']" value="true" />有领奖记录
						</div>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>

					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-cb-view-customer-profile-list"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-cb-view-customer-profile-list").data("gridOptions", {
            url : '${base}/myt/chinababy/cb-view-customer-profile!findByPage',
            colNames : [ '流水号', 'vip号', '姓名', '性别', '电话', '邮件', '收货地址', '注册时间','宝宝性别','宝宝生日' ],
            colModel : [ {
                name : 'id'
            }, {
                name : 'customerName',
                width : 100,
                align : 'center'
            }, {
                name : 'trueName',
                width : 100,
                align : 'center'
            }, {
                name : 'sex',
                index : 'sex',
                width : 50,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('genericSexEnum')
                }

            }, {
                name : 'mobilePhone',
                width : 100
            }, {
                name : 'email',
                width : 150
            }, {
                name : 'deliveryAddr'
            }, {
                name : 'createdDate',
                width : 200,
                sorttype : 'date'
            }, {
                name : 'babySex',
                width : 70,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('genericSexEnum')
                }
            }, {
                name : 'babyBirthday',
                width : 150,
                sorttype : 'date',
                align : 'center'
            }],
            multiselect : false,
            subGrid : true,
            viewurl : WEB_ROOT + "/myt/customer/customer-profile!view",
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : "${base}/myt/chinababy/cb-view-customer-profile!getLotteryWin?id=" + row_id,
                    colNames : [ '时间', '奖品类型', '发货订单', '状态' ],
                    colModel : [ {
                        name : 'createdDate',
                        align : 'center'
                    }, {
                        name : 'awardType',
                        editable : true,
                        align : 'center',
                        stype : 'select',
                        searchoptions : {
                            value : Util.getCacheEnumsByType('winAwardTypeEnum')
                        }
                    }, {
                        name : 'boxOrder.orderSeq',
                        align : 'left'
                    }, {
                        name : 'winStatus',
                        editable : true,
                        align : 'center',
                        stype : 'select',
                        searchoptions : {
                            value : Util.getCacheEnumsByType('winStatusEnum')
                        }
                    } ],
                    filterToolbar : false,
                    toppager : false,
                    pager : false,
                    multiselect : false,
                    caption : "领奖信息",
                    subGrid : true,
                    subGridRowExpanded : function(subgrid_id, row_id) {
                        Grid.initSubGrid(subgrid_id, row_id, {
                            url : "${base}/myt/chinababy/cb-lottery-win!getLotteryWinDetails?id=" + row_id,
                            colNames : [ '奖品', '商品', '地区' ],
                            colModel : [ {
                                name : 'awardName',
                                width : 120

                            }, {
                                name : 'commodity.display'

                            }, {
                                name : 'lotteryAward.region.name',
                                width : 100,
                                align : 'left'
                            } ],
                            filterToolbar : false,
                            caption : "奖品详情",
                            toppager : false,
                            pager : false,
                            multiselect : false
                        });
                    }
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
