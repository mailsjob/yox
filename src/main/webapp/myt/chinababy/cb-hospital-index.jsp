<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">医院列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search"
						data-grid-search=".grid-myt-chinababy-cb-hospital" action="#">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_hospitalName']" class="form-control" placeholder="医院名称">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-chinababy-cb-hospital" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-chinababy-cb-hospital").data("gridOptions", {
            url : '${base}/myt/chinababy/cb-hospital!findByPage',
            colNames : [ '流水号', '医院名称', '地区', '备注', '启用标识' ],
            colModel : [ {
                name : 'id'
            }, {
                name : 'hospitalName',
                editable : true,
                align : 'left'
            }, {
                name : 'regionNames',
                index : 'cbHospitalR2Regions.region.name',
                align : 'left'
            }, {
                name : 'momo',
                editable : true,
                align : 'left'
            }, {
                name : 'enable',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            } ],

            editurl : "${base}/myt/chinababy/cb-hospital!doSave",
            delurl : "${base}/myt/chinababy/cb-hospital!doDelete",
            fullediturl : "${base}/myt/chinababy/cb-hospital!inputTabs",
            viewurl : "${base}/myt/chinababy/cb-hospital!viewTabs"
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>