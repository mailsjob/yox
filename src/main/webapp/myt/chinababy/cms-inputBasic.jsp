<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-md-cms-inputBasic"
	action="${base}/myt/md/cms!doSave" method="post" 
	data-editrulesurl="${base}/myt/md/cms!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-cms-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">代码</label>
					<div class="controls">
		                <s:textfield name="staticId" />
					</div>
				</div>
            </div>
             <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">布局</label>
					<div class="controls">
		                <s:textfield name="layout" />
					</div>
				</div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">标题</label>
					<div class="controls">
		                <s:textfield name="title" />
					</div>
				</div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">分类</label>
					<div class="controls">
		                <%-- <s:select name="category" list="categoryMap" /> --%>
		                 <s:textfield name="category" />
					</div>
				</div>
            </div>
             <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">页面关键词</label>
					<div class="controls">
		                <s:textfield name="keyWords" />
					</div>
				</div>
            </div>
         </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">首页描述</label>
					<div class="controls">
						<s:textarea name="pageDescription" rows="2"></s:textarea>
		             </div>
				</div>
            </div>
        </div>
        <div class="row">
				<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">HTML文本</label>
					<div class="controls">
						<s:textarea name="htmlContent" data-htmleditor="kindeditor" data-height="600px" />
					</div>
				</div>
			</div>
    </div>
    </div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-cms-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>