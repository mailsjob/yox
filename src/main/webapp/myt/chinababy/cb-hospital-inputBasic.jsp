<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation"
	action="${base}/myt/chinababy/cb-hospital!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-chinababy-cb-hospital">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">启用标识</label>
					<div class="controls">
						<s:radio name="enable" list="#application.enums.booleanLabel" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">医院名称</label>
					<div class="controls">
						<s:textfield name="hospitalName" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">备注</label>
					<div class="controls">
						<s:textarea name="memo" rows="3" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">地区（多选）</label>
					<div class="controls">
						<table class="table table-cb-hospital-regions" data-dynamic-table="true">
							<thead>
								<tr>
									<th>区域<span class="required">*</span></th>
								</tr>
							</thead>
							<tbody>
								<s:iterator value="cbHospitalR2Regions" var="item" status="status">
									<tr>
										<s:hidden name="%{'cbHospitalR2Regions['+#status.index+'].extraAttributes.operation'}" value="update" />
										<s:hidden name="%{'cbHospitalR2Regions['+#status.index+'].cbHospital.id'}" />
										<td><s:textfield name="%{'cbHospitalR2Regions['+#status.index+'].region.display'}"
												data-toggle="dropdownselect" data-url="${base}/myt/md/region!forward?_to_=selection"
												data-selected="%{cbHospitalR2Regions[#status.index].region.display}" /> <s:hidden
												name="%{'cbHospitalR2Regions['+#status.index+'].region.id'}" /></td>
									</tr>
								</s:iterator>
								<tr class="dynamic-table-row-template">
									<s:hidden name="cbHospitalR2Regions[X].extraAttributes.operation" value="create" />
									<s:hidden name="cbHospitalR2Regions[X].cbHospital.id" value="%{id}" />
									<td><s:textfield name="%{'cbHospitalR2Regions[X].region.display'}" data-toggle="dropdownselect"
											data-url="${base}/myt/md/region!forward?_to_=selection" data-selected="%{parent.id}" /> <s:hidden
											name="%{'cbHospitalR2Regions[X].region.id'}" /></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {
        $(".table-cb-hospital-regions").data("dynamicTableOptions", {
            afterAdd : function($tr) {
                $tr.find("input[name$='region.display']").data('data-dropdownselect-callback', function(json) {
                    $tr.find("[name$='region.display']").val(json.display);
                    $tr.find("[name$='region.id']").val(json.id);
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>