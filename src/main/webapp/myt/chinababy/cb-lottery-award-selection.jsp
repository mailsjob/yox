<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row">
	<table class="grid-cb-lottery-award-selection" data-grid="table"></table>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-cb-lottery-award-selection").data("gridOptions", {
            url : '${base}/myt/chinababy/cb-lottery-award!findByPage',
            colNames : ['抽奖码','奖品名', '奖品码','奖品类型','商品','启用标识'],
            colModel : [
                        {name:'lotteryCode',width:150},
            {name:'awardName',width:150},
	   		{name:'awardCode',width:150},
	   		{name:'awardType',width:150},
	   		{name:'commodity.title'},
	   		{name:'enable',width:150, edittype : 'checkbox',align : 'center'}
	   		
	   		],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }

            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
