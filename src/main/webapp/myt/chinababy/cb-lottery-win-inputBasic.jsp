<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-cb-lottery-win"
	action="${base}/myt/chinababy/cb-lottery-win!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-cb-lottery-win-list">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button" data-tab-close-active="$(this).closest('.tabbable-primary')">取消</button>
	</div>
	<div class="form-body">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">获奖信息</div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">客户</label>
							<div class="controls">
								<div class="input-icon right">
									<i class="fa fa-search fa-search-customerProfile"></i> <i
										class="fa fa-times fa-times-customerProfile input-icon-secondary"></i>
									<s:textfield name="customerProfile.display" requiredLabel="true" readonly="true" />
									<s:hidden name="customerProfile.id" />
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">显示客户名</label>
							<div class="controls">
								<s:textfield name="displayCustomerName"></s:textfield>
							</div>
						</div>
					</div>
				</div>
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">奖品类型</label>
							<div class="controls">
								<s:select name="awardType" list="#application.enums.awardTypeEnum" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">领取状态</label>
							<div class="controls">
								<s:select name="winStatus" list="#application.enums.winStatusEnum" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label ">备注</label>
							<div class="controls">
								<s:textarea name="memo" rows="3"/>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">获奖明细</div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-cb-lottery-win-cbLotteryWinDetails" data-dynamic-table="true">
								<thead>
									<tr>
										<th style="width: 100px">关联奖品<span class="required">*</span></th>
										<th style="width: 100px">商品</th>
										<th style="width: 100px">显示奖品名</th>
										<th style="width: 100px">图片</th>

									</tr>
								</thead>
								<tbody>
									<s:iterator value="cbLotteryWinDetails" status="status" var="item">
										<tr>

											<s:hidden name="%{'cbLotteryWinDetails['+#status.index+'].id'}" />
											<s:hidden name="%{'cbLotteryWinDetails['+#status.index+'].cbLotteryWin.id'}" />
											<s:hidden name="%{'cbLotteryWinDetails['+#status.index+'].extraAttributes.operation'}" value="update" />
											<td>
												<div class="input-icon right">
													<i class="fa fa-search fa-search-lotteryAward"></i>
													<s:textfield name="%{'cbLotteryWinDetails['+#status.index+'].lotteryAward.display'}" requiredLabel="true"
														readonly="true" />
													<s:hidden name="%{'cbLotteryWinDetails['+#status.index+'].lotteryAward.id'}" requiredLabel="true" />
												</div>
											</td>
											<td>
												<div class="input-icon right">
													<i class="fa fa-search fa-search-commodity"></i>
													<s:textfield name="%{'cbLotteryWinDetails['+#status.index+'].commodity.sku'}" readonly="true" />
													<span class="help-inline help-commodity-title"><s:property value="#item.commodity.title" /></span>
													<s:hidden name="%{'cbLotteryWinDetails['+#status.index+'].commodity.id'}" />
												</div>
											</td>
											<td><s:textfield name="%{'cbLotteryWinDetails['+#status.index+'].awardName'}" /></td>
											<td><span class="help-inline help-awardImg"> <img
													src='<s:property value='imgUrlPrefix'/><s:property value="#item.awardImg" />'
													style="width: 80px; height: 60px;" />
											</span>
												<button class="btn btn-small" type="button" onclick="uploadImg(this)">点击上传图片</button> <s:hidden
													name="%{'cbLotteryWinDetails['+#status.index+'].awardImg'}" /></td>

										</tr>
									</s:iterator>
									<tr class="dynamic-table-row-template">
										<s:hidden name="cbLotteryWinDetails[x].extraAttributes.operation" value="create" />

										<s:hidden name="cbLotteryWinDetails[x].cbLotteryWin.id" value="%{model.id}" />
										<td>
											<div class="input-icon right">
												<i class="fa fa-search fa-search-lotteryAward"></i>
												<s:textfield name="%{'cbLotteryWinDetails[x].lotteryAward.display'}" requiredLabel="true" readonly="true" />
												<span class="help-inline help-lotteryAward-title"></span>
												<s:hidden name="%{'cbLotteryWinDetails[x].lotteryAward.id'}" requiredLabel="true" />
											</div>
										</td>
										<td>
											<div class="input-icon right">
												<i class="fa fa-search fa-search-commodity"></i>
												<s:textfield name="%{'cbLotteryWinDetails[x].commodity.sku'}" readonly="true" />
												<span class="help-inline help-commodity-title"><s:property value="#item.commodity.title" /></span>
												<s:hidden name="%{'cbLotteryWinDetails[x].commodity.id'}" />
											</div>
										</td>
										<td><s:textfield name="cbLotteryWinDetails[x].awardName" /></td>
										<td><span class="help-inline help-awardImg"></span>
										<button class="btn btn-small" type="button" onclick="uploadImg(this)">点击上传图片</button> <s:hidden
												name="%{'cbLotteryWinDetails[x].awardImg'}" /></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-cb-lottery-award-list">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {

        $(".form-edit-cb-lottery-win").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);

                $form.find("i.fa-search-customerProfile").click(function() {
                    $(this).popupDialog({
                        url : '${base}/myt/customer/customer-profile!forward?_to_=selection',
                        title : '选取客户',
                        callback : function(json) {
                            $form.find("[name$='customerProfile.display']").val(json.nickName + " " + json.trueName);
                            $form.find("[name$='customerProfile.id']").val(json.id);
                        }
                    })
                });

                $form.find("i.fa-times-customerProfile").click(function() {
                    $form.find("[name$='customerProfile.display']").val("");
                    $form.find("[name$='customerProfile.id']").val("");
                });

            }

        });
        $(".table-cb-lottery-win-cbLotteryWinDetails").data("dynamicTableOptions", {
            afterAdd : function($tr) {
                $tr.find("i.fa-search-lotteryAward").click(function() {
                    $(this).popupDialog({
                        url : '${base}/myt/chinababy/cb-lottery-award!forward?_to_=selection',
                        title : '选取奖品',
                        callback : function(json) {
                            $tr.find("[name$='lotteryAward.display']").val(json.lotteryCode + " " + json.awardName + " " + json.awardCode);
                            $tr.find("[name$='lotteryAward.id']").val(json.id);
                        }
                    })
                });
                $tr.find("i.fa-search-commodity").click(function() {
                    $(this).popupDialog({
                        url : '${base}/myt/md/commodity!forward?_to_=selection',
                        title : '选取商品',
                        callback : function(json) {
                            $tr.find("[name$='commodity.sku']").val(json.sku);
                            $tr.find("[name$='commodity.id']").val(json.id);
                            $tr.find(".help-commodity-title").html(json.title);

                            // $tr.find("input[name$='awardImg']").val(json.smallPic);

                            $tr.find(".help-awardImg").html("<img src='http://img.iyoubox.com/GetFileByCode.aspx?code=" + json.smallPic + "' style='width: 100px; height: 80px;'/>");
                        }
                    })
                });
            }
        });
    });
    function uploadImg(btn) {
        var form = $("form .form-edit-cb-lottery-win");
        form.picsEditor = KindEditor.editor({
            allowFileManager : false,
            uploadJson : '<s:property value="kindEditorImageUploadUrl"/>'
        });
        var picTR = $(btn).closest("tr");
        form.picsEditor.loadPlugin('image', function() {
            form.picsEditor.plugin.imageDialog({
                clickFn : function(url, title, width, height, border, align) {
                    var code = url.split('=')[1]
                    picTR.find(".help-awardImg").html("<img src='http://img.iyoubox.com/GetFileByCode.aspx?code=" + code + "' style='width: 80px; height: 60px;'/>");
                    picTR.find("input[name$='awardImg']").val(code);
                    form.picsEditor.hideDialog();
                }
            })

        })
    }
</script>
<%@ include file="/common/ajax-footer.jsp"%>

