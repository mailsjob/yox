<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">奖品列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search" data-grid-search=".grid-cb-lottery-award-list"
						action="#">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_awardName_OR_lotteryCode_OR_awardCode']" class="form-control"
									placeholder="奖品名/奖品码/抽奖码">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-cb-lottery-award-list" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-cb-lottery-award-list").data("gridOptions", {
            url : '${base}/myt/chinababy/cb-lottery-award!findByPage',
            colNames : [ '流水号', '抽奖码', '奖品码', '奖品名', '总数', '待发送数', '发送数', '剩余数', '中奖率', '排序号', '地区', '奖品类型', '是否可用' ],
            colModel : [ {
                name : 'id'
            }, {
                name : 'lotteryCode',
                editable : true,
                align : 'left'
            }, {
                name : 'awardCode',
                editable : true,
                align : 'left'
            }, {
                name : 'awardName',
                editable : true,
                align : 'left'
            }, {
                name : 'awardTotal',
                editable : true,
                sorttype : 'number',
                align : 'right'
            }, {
                name : 'awardWinUnsent',
                editable : true,
                sorttype : 'number',
                align : 'right'
            }, {
                name : 'awardSent',
                editable : true,
                sorttype : 'number',
                align : 'right'
            }, {
                name : 'awardRemain',
                editable : true,
                sorttype : 'number',
                align : 'right'
            }, {
                name : 'winRate',
                sorttype : 'number',
                editable : true,
                align : 'right'
            }, {
                name : 'orderIndex',
                editable : true,
                sorttype : 'number',
                align : 'right'
            }, {
                name : 'region.name',
                align : 'left'
            }, {
                name : 'awardType',
                editable : true,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('awardTypeEnum')
                }

            }, {
                name : 'enable',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            } ],
            postData : {
                "search['FETCH_region']" : "INNER",
                "search['FETCH_commodity']" : "LEFT"
            },
            editurl : "${base}/myt/chinababy/cb-lottery-award!doSave",
            delurl : "${base}/myt/chinababy/cb-lottery-award!doDelete",
            fullediturl : "${base}/myt/chinababy/cb-lottery-award!inputTabs",
            viewurl : "${base}/myt/chinababy/cb-lottery-award!viewTabs"
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>