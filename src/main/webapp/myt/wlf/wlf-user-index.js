$(function() {
    $(".grid-myt-wlf-wlf-user").data("gridOptions", {
        url : WEB_ROOT + "/myt/wlf/wlf-user!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '登录账号',
            width : 80,
            name : 'loginid'
        }, {
            label : '真实姓名',
            width : 80,
            name : 'trueName'
        }, {
            label : '性别',
            name : 'sex',
            width : 50,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('genericSexEnum')
            }
        }, {
            label : '身份证号',
            width : 150,
            name : 'idCard'
        }, {
            label : '电话',
            width : 120,
            name : 'mobilePhone'
        }, {
            label : '邮件',
            width : 150,
            name : 'email'
        }, {
            label : '入学时间',
            width : 120,
            name : 'enterTime',
            sorttype : 'date'
        }, {
            label : '教育层次',
            name : 'eduLevel',
            width : 50,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('eduLevelEnum')
            }
        }, {
            label : '申请时间',
            width : 120,
            name : 'applyTime',
            sorttype : 'date'
        }, {
            label : '授信额度',
            width : 120,
            formatter : 'currency',
            name : 'weilaifuMaxAccount'
        }, {
            label : '审核状态',
            name : 'auditStatus',
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('auditStatusEnum')
            }
        } ],
        operations : function(itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>审核</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/wlf/wlf-user!doAudit';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  审核？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }

            });
            itemArray.push($select);
        },
        editcol : 'loginid',
        addable : false,
        editurl : WEB_ROOT + "/myt/wlf/wlf-user!doSave",
        fullediturl : WEB_ROOT + "/myt/wlf/wlf-user!inputTabs"

    });
});