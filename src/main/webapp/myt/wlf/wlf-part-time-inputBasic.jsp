<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-wlf-wlf-part-time-inputBasic"
	action="${base}/myt/wlf/wlf-part-time!doSave" method="post" 
	data-editrulesurl="${base}/myt/wlf/wlf-part-time!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-wlf-wlf-part-time-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">标题</label>
					<div class="controls">
		                <s:textfield name="title" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">单位</label>
					<div class="controls">
		                <s:textfield name="unit" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">图片</label>
					<div class="controls">
		                <s:hidden name="pic" data-singleimage="true" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">链接</label>
					<div class="controls">
		                <s:textfield name="url" />
					</div>
				</div>
            </div>
        </div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-wlf-wlf-part-time-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/wlf/wlf-part-time-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>