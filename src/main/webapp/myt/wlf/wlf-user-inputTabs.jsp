<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-custom tabbable-secondary">
	<ul class="nav nav-tabs">
		<li class="tools pull-right"><a href="javascript:;" class="btn default reload"><i class="fa fa-refresh"></i></a></li>
		<li class="active"><a data-toggle="tab"
			href="${base}/myt/wlf/wlf-user!edit?id=<s:property value='#parameters.id'/>">授信审核</a></li>
		<li><a data-toggle="tab"
			href="${base}/myt/wlf/wlf-user!forward?_to_=info&id=<s:property value='#parameters.id'/>">基本信息</a></li>
	</ul>
</div>
<%@ include file="/common/ajax-footer.jsp"%>