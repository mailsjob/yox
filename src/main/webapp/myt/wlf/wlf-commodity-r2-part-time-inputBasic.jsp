<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-wlf-wlf-commodity-r2-part-time-inputBasic"
	action="${base}/myt/wlf/wlf-commodity-r2-part-time!doSave" method="post" 
	data-editrulesurl="${base}/myt/wlf/wlf-commodity-r2-part-time!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-wlf-wlf-commodity-r2-part-time-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品信息表</label>
					<div class="controls">
		                <s:textfield name="commodity" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">兼职信息表</label>
					<div class="controls">
		                <s:textfield name="partTime" />
					</div>
				</div>
            </div>
        </div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-wlf-wlf-commodity-r2-part-time-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/wlf/wlf-commodity-r2-part-time-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>