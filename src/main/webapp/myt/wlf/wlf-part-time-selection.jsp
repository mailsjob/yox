<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form method="get" class="form-inline form-validation form-search-init"
			data-grid-search=".grid-commodity-stock-selection" action="#">
			<div class="form-group">
				<s:textfield name="search['CN_title']"
					cssClass="form-control input-xlarge" placeholder="兼职信息标题" />
			</div>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-commodity-stock-selection"></table>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-commodity-stock-selection").data("gridOptions", {
            url : "${base}/myt/wlf/wlf-part-time!findByPage",
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true                          
            }, {
                label : '标题',
                name : 'title',
                width : 200,
                editable: true,
                align : 'left'
            }, {
                label : '单位',
                name : 'unit',
                width : 50,
                editable: true,
                align : 'left'
            }, {
                label : '图片',
                name : 'pic',
                width : 128,
                formatter : Biz.md5CodeImgViewFormatter,
                editable: true,
                align : 'left'
            }, {
                label : '链接',
                name : 'url',
                width : 200,
                editable: true,
                align : 'left'
            } ],
           
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }

            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
