$(function() {
	var commodityId = $(".grid-myt-wlf-wlf-commodity-r2-part-time-index").attr("data-pk");
	$(".grid-myt-wlf-wlf-commodity-r2-part-time-index").data("gridOptions", {
        url : WEB_ROOT + "/myt/wlf/wlf-commodity-r2-part-time!findByPage?search['EQ_commodity.id']="+commodityId,
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        },{
            name : 'partTime.id',
            hidden : true,
            hidedlg : true,
            editable : true
        },{
            label : '兼职信息表',
            name : 'partTime.title',
            index : 'partTime',
            width : 200,
            editable: true,
            align : 'left',
            search: false,
        	editoptions : {
                placeholder : '输入兼职信息',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                   // $elem.wrap('<div class="input-icon right"/>');
                   // $elem.before('<i class="fa fa-ellipsis-horizontal fa-select-commodity"></i>');
                    var selectCommodity = function(item) {
                        var $curRow = $elem.closest("tr.jqgrow");
                        // 强制覆盖已有值
                        $grid.jqGrid("setEditingRowdata", {
                        	'partTime.id' : item['id'],
                            'partTime.title' : item['title'],
                        }, true);

                    }
                   // $(".fa-select-commodity").click(function() {
                    $elem.dblclick(function(event) {
                        var rowdata = $grid.jqGrid("getEditingRowdata");
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/wlf/wlf-part-time!forward?_to_=selection',
                            postData : {
                            	//sku : rowdata['commodity.sku']
                            },
                            title : '选取兼职信息',
                            callback : function(item) {
                                selectCommodity(item);
                            }
                        })
                        event.stopPropagation();
                    })
                }
        	}
        } ],
//        postData: {
//           "search['FETCH_partTime']" : "INNER",
//           "search['FETCH_commodity']" : "INNER"
//        },
        editurl : WEB_ROOT + '/myt/wlf/wlf-commodity-r2-part-time!doSave?commodity.id='+commodityId,
        delurl : WEB_ROOT + '/myt/wlf/wlf-commodity-r2-part-time!doDelete'
    });
});
