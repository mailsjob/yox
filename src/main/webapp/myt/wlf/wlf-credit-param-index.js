$(function() {
    $(".grid-myt-wlf-wlf-credit-param-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/wlf/wlf-credit-param!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id'
        }, {
            label : '类型',
            name : 'creditPrimaryType',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('creditPrimaryTypeEnum')
            },
            width : 80,
            editable : true,
            align : 'center'
        }, {
            label : '代码',
            name : 'primaryCode',
            width : 255,
            editable : true,
            align : 'left'
        }, {
            label : '授信额度',
            name : 'creditValue',
            width : 60,
            editable : true,
            align : 'right'
        } ],
        grouping : true,
        groupingView : {
            groupField : [ 'creditPrimaryType' ]
        },
        editurl : WEB_ROOT + '/myt/wlf/wlf-credit-param!doSave',
        editrulesurl : WEB_ROOT + '/myt/wlf/wlf-credit-param!buildValidateRules',
        delurl : WEB_ROOT + '/myt/wlf/wlf-credit-param!doDelete',
        fullediturl : WEB_ROOT + '/myt/wlf/wlf-credit-param!inputTabs'
    });
});
