<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="container-fluid data-view">
    <div class="well form-horizontal">
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="commodity" label="商品信息表"/>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="partTime" label="兼职信息表"/>
            </div>
        </div>
    </div>    
</div>