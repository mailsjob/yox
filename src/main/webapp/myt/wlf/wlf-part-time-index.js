$(function() {
    $(".grid-myt-wlf-wlf-part-time-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/wlf/wlf-part-time!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : false                          
        }, {
            label : '标题',
            name : 'title',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '单位',
            name : 'unit',
            width : 50,
            editable: true,
            align : 'left'
        }, {
            label : '图片',
            name : 'pic',
            width : 128,
            search : false,
            formatter : Biz.md5CodeImgViewFormatter,
            editable: false,
            align : 'left'
        }, {
            label : '链接',
            name : 'url',
            width : 200,
            editable: true,
            align : 'left'
        } ],
        editcol:'title',
        editurl : WEB_ROOT + '/myt/wlf/wlf-part-time!doSave',
        delurl : WEB_ROOT + '/myt/wlf/wlf-part-time!doDelete',
        fullediturl : WEB_ROOT + '/myt/wlf/wlf-part-time!inputTabs'
    });
});
