<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="container-fluid data-view">
    <div class="well form-horizontal">
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="title" label="标题"/>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="unit" label="单位"/>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="pic" label="图片"/>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="url" label="链接"/>
            </div>
        </div>
    </div>    
</div>