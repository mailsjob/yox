$(function() {
    $(".grid-myt-purchase-purchase-receipt").data("gridOptions", {
        url : WEB_ROOT + '/myt/purchase/purchase-receipt!findByPage',
        colModel : [ {
            label : '凭证号',
            name : 'voucher',
            align : 'center',
            width : 140
        }, {
            label : '凭证日期',
            name : 'voucherDate',
            align : 'center',
            stype : 'date',
            width : 100
        }, {
            label : '凭证状态',
            name : 'voucherState',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('voucherStateEnum')
            },
            formatter : function(cellValue, options, rowdata, action) {
                if (cellValue == 'REDW') {
                    return '<span class="badge">红冲</span>'
                } else if (cellValue == 'DRAFT') {
                    return '<span class="badge badge-warning">草稿</span>'
                } else if (cellValue == 'POST') {
                    return '<span class="badge badge-success">提交</span>'
                }
                return cellValue;
            },
            width : 100
        }, {
            label : '供货商',
            name : 'supplier.display',
            index : 'supplier.code_OR_supplier.abbr',
            width : 100
        }, {
            label : '外部参考来源',
            hidden : true,
            name : 'externalSource',
            width : 100
        }, {
            label : '外部参考凭证号（快递单号）',
            hidden:true,
            name : 'externalVoucher',
            width : 150
        }, {
            label : '入库物流单号',
            name : 'logisticsNo',
            width : 150
        }, {
            label : '采购订单凭证号',
            name : 'purchaseOrder.voucher',
            width : 150
        }, {
            label : '经办人',
            name : 'voucherUser.display',
            index : 'voucherUser.aclCode_OR_voucherUser.signinid_OR_voucherUser.signinid.nick',
            width : 100
        }, {
            label : '经办部门',
            name : 'voucherDepartment.display',
            index : 'voucherDepartment.title',
            width : 100
        }, {
            label : '采购单总税',
            name : 'totalTaxAmount',
            formatter : 'currency',
            hidden : true,
            width : 100
        }, {
            label : '采购总金额',
            name : 'totalCostAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '运费',
            name : 'totalDeliveryAmount',
            formatter : 'currency',
            hidden : true,
            width : 100
        } ],
        editcol : 'voucher',
        fullediturl : WEB_ROOT + "/myt/purchase/purchase-receipt!inputTabs",
        multiselect : false,
        postData : {
            "search['FETCH_purchaseOrder']" : "LEFT"
        },
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/purchase/purchase-receipt!purchaseReceiptDetails?id=" + row_id,
                colModel : [ {
                    label : '销售（发货）商品',
                    name : 'commodity.display',
                    align : 'left'
                }, {
                    label : '发货仓库',
                    name : 'storageLocation.id',
                    width : 80,
                    stype : 'select',
                    formatter : 'select',
                    searchoptions : {
                        value : Biz.getStockDatas()
                    },
                }, {
                    label : '单位',
                    name : 'measureUnit',
                    editable : true,
                    width : 80
                }, {
                    label : '数量',
                    name : 'quantity',
                    width : 80,
                    formatter : 'number'
                }, {
                    label : '采购单价',
                    name : 'price',
                    width : 80,
                    formatter : 'currency'

                }, {
                    label : '原价金额',
                    name : 'originalAmount',
                    width : 80,
                    formatter : 'currency'

                }/*, {
                                                    label : '折扣率(%)',
                                                    name : 'discountRate',
                                                    width : 80,
                                                    formatter : 'number'
                                                }, {
                                                    label : '折扣额',
                                                    name : 'discountAmount',
                                                    width : 80,
                                                    formatter : 'currency'
                                                }, {
                                                    label : '折后金额',
                                                    name : 'amount',
                                                    width : 80,
                                                    formatter : 'currency'
                                                }*/, {
                    label : '税率(%)',
                    name : 'taxRate',
                    width : 80,
                    hidden : true,
                    formatter : 'number'

                }, {
                    label : '税额',
                    name : 'taxAmount',
                    width : 80,
                    hidden : true,
                    formatter : 'currency'
                }, {
                    label : '含税总金额',
                    name : 'commodityAndTaxAmount',
                    width : 80,
                    hidden : true,
                    formatter : 'currency'
                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
});