$(function() {
   $(".form-myt-purchase-purchase-return-order-bpmCreateReceivablesReceipt").data("formOptions", {
       bindEvents : function() {
           var $form = $(this);
           $form.find("input[name='accountSubject.display']").treeselect({
               url : WEB_ROOT + "/myt/finance/account-subject!findPaymentAccountSubjectsForRecorded",
               callback : {
                   onSingleClick : function(event, treeId, treeNode) {
                       $form.setFormDatas({
                           'accountSubject.id' : treeNode.id,
                           'accountSubject.display' : treeNode.display
                       }, true);
                   },
                   onClear : function(event) {
                       $form.setFormDatas({
                           'accountSubject.id' : '',
                           'accountSubject.display' : ''
                       }, true);
                   }
               }
           });
       },
       updateTotalAccountAmount : function() {
           var $form = $(this);
           var $grid = $form.find(".grid-myt-puchase-return-order-bpmCreateReceivablesReceipt");
           var ids = $grid.jqGrid("getDataIDs");
           var sumAmount = $grid.jqGrid('sumColumn', 'amount');
           $form.setFormDatas({
               actualPayedAmount : sumAmount,
           });
       }
 });

$(".grid-myt-puchase-return-order-bpmCreateReceivablesReceipt").data("gridOptions", {
        calcRowAmount : function(rowdata, src) {
        },
        updateRowAmount : function(src) {
            var $grid = $(this);
            var rowdata = $grid.jqGrid("getEditingRowdata");
            $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
            $grid.jqGrid("setEditingRowdata", rowdata);
        },
        /*url : function() {
            var bizVoucher = $(this).attr("data-voucher");
            if (bizVoucher) {
                return WEB_ROOT + "/myt/finance/payment-receipt-detail!findByPage?search['EQ_tradeReceipt.bizVoucher']="+bizVoucher;
            }
        },*/
        batchEntitiesPrefix : "tradeReceiptDetails",
        colModel : [  {
            name : 'accountSubject.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '入账会计科目',
            name : 'accountSubject.display',
            editable : true,
            width : 200,
            editrules : {
                required : true
            },
            editoptions : {
                placeholder : '单击选择会计科目',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.treeselect({
                        url : WEB_ROOT + "/myt/finance/account-subject!findPaymentAccountSubjects",
                        callback : {
                            onSingleClick : function(event, treeId, treeNode) {
                                $elem.val(treeNode.name);
                                var rowdata = $grid.jqGrid("getEditingRowdata");
                                rowdata['accountSubject.id'] = treeNode.id;
                                $grid.jqGrid("setEditingRowdata", rowdata);
                            },
                            onClear : function(event) {
                                var rowdata = $grid.jqGrid("getEditingRowdata");
                                rowdata['accountSubject.id'] = '';
                                $elem.val("");
                                $grid.jqGrid("setEditingRowdata", rowdata);
                            }
                        }
                    });
                }
            },
            align : 'left'
        }, {
            label : '金额',
            name : 'amount',
            width : 80,
            formatter : 'currency',
            editable : true,
            editrules : {
                required : true
            },
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.change(function() {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            },
            responsive : 'sm'
        }, {
            label : '关联凭证',
            name : 'paymentVouchers',
            width : 100,
            editable : true

        } , {
            label : '收款参考信息',
            name : 'paymentReference',
            edittype : 'textarea',
            width : 300,
            editable : true
        }, {
            label : '记账摘要',
            name : 'accountSummary',
            editable : true,
            width : 150
        }, {
            label : '备注',
            name : 'memo',
            width : 150,
            editable : true

        } ],
        footerrow : true,
        beforeInlineSaveRow : function(rowid) {
            var $grid = $(this);
            $grid.data("gridOptions").updateRowAmount.call($grid);
        },
        afterInlineSaveRow : function(rowid) {
            var $grid = $(this);
            // 整个Form相关金额字段更新
            var $form = $grid.closest("form");
            $form.data("formOptions").updateTotalAccountAmount.call($form);
        },
        afterInlineDeleteRow : function(rowid) {
            var $grid = $(this);
            // 整个Form相关金额字段更新
            var $form = $grid.closest("form");
            $form.data("formOptions").updateTotalAccountAmount.call($form);
        },
        userDataOnFooter : true
    });
});
