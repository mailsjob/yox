<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation"
	action="${base}/myt/purchase/purchase-order!bpmReceiptAudit" method="post">
	<s:hidden name="taskId" value="%{#parameters.taskId}" />
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body control-label-sm">
		<s:set var="taskVariablesVar" value="taskVariables" />
		<s:if test="%{storageMode.name()=='AUTO'}">
		<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="alert alert-warning">库存模式为“德国库模式”，采购流程无‘录入物流’和‘收货入库’流程，请确认德国库已实际收货，再在此确认。</div>
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">库存模式</label>
						<div class="controls">
							<s:radio name="storageMode" list="#application.enums.storageModeEnum" disabled="true" />
						</div>
					</div>
				</div>
			</div>
		</s:if>
		<s:elseif test="%{storageMode.name()=='SA'}">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<p class="form-control-static">库存模式为“代发模式”，采购流程无‘收货入库’操作，请确认代发虚拟库已实际收货，再在此确认。</p>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">库存模式</label>
						<div class="controls">
							<s:radio name="storageMode" list="#application.enums.storageModeEnum" disabled="true" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">发货日期</label>
						<div class="controls">
							<s3:datetextfield format="timestamp" name="deliveryTime"
								data-timepicker="true" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">物流公司</label>
						<div class="controls">
							<s:select name="logistics.id" list="logisticsNameMap" disabled="true" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">物流单号</label>
						<div class="controls">
							<s:textfield name="logisticsNo" readonly="true" />
						</div>
					</div>
				</div>

			</div>
			
		</s:elseif>
		<s:else>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="alert alert-info">库存模式为“北京库模式”，请对比采购订单商品、系统收货入库商品、实际库存增值，无误在此确认。</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">库存模式</label>
						<div class="controls">
							<s:radio name="storageMode" list="#application.enums.storageModeEnum" disabled="true" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">发货日期</label>
						<div class="controls">
							<s3:datetextfield format="timestamp" name="deliveryTime" readonly="true" data-timepicker="true" />
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">物流公司</label>
						<div class="controls">
							<s:select name="logistics.id" list="logisticsNameMap" disabled="true" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">物流单号</label>
						<div class="controls">
							<s:textfield name="logisticsNo" readonly="true" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-purchase-order-bpmReceiptAudit" data-readonly="true" data-grid="items"
						data-pk='<s:property value="#parameters.id"/>'></table>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">入库备注</label>
						<div class="controls">
							<s:textarea name="receiptMemo" rows="3"  readonly="true"/>
						</div>
					</div>
				</div>
			</div>
		</s:else>
		
		<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">审核备注</label>
						<div class="controls">
							<s:textarea name="receiptAuditMemo" rows="3"  />
						</div>
					</div>
				</div>
			</div>

		<div class="form-actions right">
			<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
				<i class="fa fa-check"></i> 保存
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
		</div>
</form>
<div class="purchase-order-content ajaxify"
	data-url="${base}/myt/purchase/purchase-order!view?id=<s:property value='#parameters.id'/>"></div>
<script src="${base}/myt/purchase/purchase-order-bpmReceiptAudit.js" />
<%@ include file="/common/ajax-footer.jsp"%>
