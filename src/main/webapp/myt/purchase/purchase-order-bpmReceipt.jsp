<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form
	class="form-horizontal form-bordered form-label-stripped form-validation form-myt-purchase-purchase-order-bpmReceipt"
	action="${base}/myt/purchase/purchase-order!bpmReceipt" method="post">
	<s:hidden name="taskId" value="%{#parameters.taskId}" />
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:hidden name="allReceipt" />
	<s:token />
	<div class="form-actions">

		<button class="btn yellow" type="submit" data-ajaxify-reload=".ajaxify-tasks"
			onclick="Util.findClosestFormInputByName(this, 'allReceipt').val('false')">
			<i class="fa fa-file-text"></i> 部分收货
		</button>
		<button class="btn default" type="button">取消</button>
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks"
			onclick="Util.findClosestFormInputByName(this, 'allReceipt').val('true')">
			<i class="fa fa-file-text"></i> 收货并完结任务
		</button>
		<button class="btn green pull-right btn-post-url" type="button"  data-ajaxify-reload=".ajaxify-tasks"
						data-url="${base}/myt/purchase/purchase-order!completeTask?id=<s:property value='%{id}'/>&taskId=<s:property value='%{#parameters.taskId}' />"
						data-confirm="确认完结当前任务？">
						只完结任务
					</button>  
		<%-- <button class="btn blue" type="submit" data-form-action="${base}/myt/purchase/purchase-order!addPay"
			 data-prevent-close="true">
			<i class="fa fa-plus"></i> 添加付款且保留任务
		</button> --%>
	</div>
	<div class="form-body control-label-sm">
		
				<div class="portlet">
					<div class="portlet-title">
						 <div class="caption">
							<i class="fa fa-reorder"></i>收货入库记录
						</div>
						<div class="tools">
							<a class="expand" href="javascript:;"></a>
						</div>
					</div>
					<div class="portlet-body display-hide">
						<s:if test="bpmReceipts.size()==0">
							<div class="row">
								<div class="alert alert-warning">
									<strong>提示：</strong><font color="red">暂无收货入库记录</font>
								</div>
							</div>
						</s:if>

						<s:else>
							<s:iterator value="bpmReceipts" status="status" id="bpmReceipt">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">入库时间</label>
											<div class="controls">
												<s3:datetextfield name="#bpmReceipt.createdDate" format="timestamp" readonly="true" />
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">收货单凭证号</label>
											<div class="controls">
												<s:textfield name="#bpmReceipt.voucher" readonly="true" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">物流单号</label>
											<div class="controls">
												<s:textfield name="#bpmReceipt.logisticsNo" readonly="true" />
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">经办人</label>
											<div class="controls">
												<s:select name="#bpmReceipt.voucherUser.id" list="usersMap" disabled="true" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label">收货入库备注</label>
											<div class="controls">
												<s:textfield name="#bpmReceipt.memo" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered table-striped table-hover">
												<thead>
													<tr>
														<th align="center" style="width: 20px">#</th>
														<th align="center" style="width: 400px">商品</th>
														<th align="center" style="width: 100px">库存地</th>
														<th align="center" style="width: 100px">批次号</th>
														<th align="center" style="width: 100px">有效期</th>
														<th align="center" style="width: 100px">数量</th>
														<th align="center" style="width: 100px">价格</th>
														<th align="center" style="width: 100px">小计</th>
														<th align="center" style="width: 150px">折扣率(%)</th>
														<th align="center" style="width: 100px">折扣额</th>
														<th align="center" style="width: 100px">折后金额</th>
														<th align="center" style="width: 100px">商家运费</th>
														<th align="center" style="width: 100px">其他成本</th>
														<th align="center" style="width: 100px">成本单价</th>
														<th align="center" style="width: 100px">成本金额</th>
													</tr>
												</thead>
												<tbody>
													<s:iterator value="purchaseReceiptDetails" status="status" id="items">
														<tr>
															<td><s:property value="#s.count" /></td>
															<td><s:property value="#items.commodity.display" /></td>
															<td align="right"><s:property value="#items.storageLocation.title" /></td>
															<td align="right"><s:property value="#items.batchNo" /></td>
															<td align="center"><s:date name="#items.expireDate" format="yyyy-MM-dd" /></td>
															<td align="center"><s:property value="#items.quantity" /></td>
															<td align="right"><s:property value="#items.price" /></td>
															<td align="right"><s:property value="#items.originalAmount" /></td>
															<td align="right"><s:property value="#items.discountRate" /></td>
															<td align="right"><s:property value="#items.discountAmount" /></td>
															<td align="right"><s:property value="#items.amount" /></td>
															<td align="right"><s:property value="#items.deliveryAmount" /></td>
															<td align="right"><s:property value="#items.roadAmount" /></td>
															<td align="right"><s:property value="#items.costPrice" /></td>
															<td align="right"><s:property value="#items.costAmount" /></td>
														</tr>
													</s:iterator>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<hr style="border: 1px dashed #000; height: 1px">
										</div>
									</div>
								</div>
							</s:iterator>
						</s:else>
					</div>
				</div>
			
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">经办人</label>
					<div class="controls">
						<s:select name="voucherUser.id" list="usersMap" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">部门</label>
					<div class="controls">
						<s:select name="voucherDepartment.id" list="departmentsMap" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">记账日期</label>
					<div class="controls">
						<s3:datetextfield name="voucherDate" format="date" current="true" />
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">供货商</label>
					<div class="controls">
						<s:select name="supplier.id" list="suppliersMap" disabled="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货物流公司</label>
					<div class="controls">
						<s:select name="logistics.id" list="logisticsNameMap" />
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">收货物流单号</label>
					<div class="controls">
						<s:textfield name="logisticsNo" requiredLabel="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">入库说明</label>
					<div class="controls">
						<s:textfield name="purchaseMemo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-puchase-order-bpmReceipt" data-grid="items" data-pk='<s:property value="#parameters.id"/>'
					data-clone='<s:property value="#parameters.clone"/>'></table>
			</div>
		</div>
		<div class="row ">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">整单折扣额</label>
					<div class="controls">
						<s:textfield name="totalDiscountAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">折后商品总金额 </label>
					<div class="controls">
						<s:textfield name="amount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">商家运费 </label>
					<div class="controls">
						<s:textfield name="totalDeliveryAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">其他分摊费用 </label>
					<div class="controls">
						<s:textfield name="extraAttributes.totalRoadAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">已付总金额</label>
					<div class="controls">
						<s:textfield name="actualPayedAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">入库成本总金额 </label>
					<div class="controls">
						<s:textfield name="extraAttributes.totalCostAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>采购付款记录
						</div>
						<div class="tools">
							<a class="collapse" href="javascript:;"></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<table class="grid-myt-purchase-order-bpmReceipt-accountSubjects" data-grid="items"
									data-externalVoucher='<s:property value="voucher"/>'></table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label class="control-label">付款(分摊)<br />总金额
									</label>
									<div class="controls">
										<div class="input-group">
											<s:textfield name="paymentRoadAmount" />
											<div class="input-group-btn">
												<button tabindex="-1" class="btn default btn-delivery-by-amount" type="button">按金额自动分摊</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label class="control-label">付款(非分摊)<br />费用总金额
									</label>
									<div class="controls">
										<s:textfield name="paymentCommodityAmount" readonly="true" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>

	<div class="form-actions right">
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>

<div class="purchase-order-content ajaxify"
	data-url="${base}/myt/purchase/purchase-order!view?id=<s:property value='#parameters.id'/>"></div>
<script src="${base}/myt/purchase/purchase-order-bpmReceipt.js" />
<%@ include file="/common/ajax-footer.jsp"%>
