<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation" action="${base}/myt/purchase/purchase-order!bpmLaterPay" method="post">
    <s:hidden name="taskId" value="%{#parameters.taskId}" />
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
            <i class="fa fa-check"></i> 提交付款并完成任务
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body control-label-sm">
        <s:set var="taskVariablesVar" value="taskVariables" />
        <div class="row">
            <div class="col-md-9">
                <div class="portlet gren">
                    <div class="portlet-title">
                        <div class="caption">采购付款</div>
                        <div class="actions">
                            <a class="btn yellow btn-sm" href="javascript:;" title="点击此按钮，然后键盘Ctrl+V粘贴数据，系统自动解析相关数据到对应表单输入元素"><i class="fa fa-clipboard"></i> 粘贴解析数据</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="form-group">
                            <label class="control-label">三方订单号</label>
                            <div class="controls">
                                <s:textfield name="accountInOut.tradeNo" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">支付金额</label>
                                    <div class="controls">
                                        <s:textfield name="accountInOut.amount" value="%{totalAmount}" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">收款人信息</label>
                                    <div class="controls">
                                        <s:textfield name="accountInOut.target" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">记账日期</label>
                                    <div class="controls">
                                        <s3:datetextfield name="accountInOut.documentDate" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">记账日期</label>
                                    <div class="controls">
                                        <s3:datetextfield name="accountInOut.postingDate" current="true" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">支付方式</label>
                                    <div class="controls">
                                        <s:select name="accountInOut.payMode" list="payModeMap" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">支付凭证号</label>
                                    <div class="controls">
                                        <s:textfield name="accountInOut.payNo" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">付款备注</label>
                            <div class="controls">
                                <s:textarea name="accountInOut.adminMemo" rows="3" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="note note-success">
                    <h4 class="block">采购总金额： <span class="span-total-amount"><s:property value="totalAmount" /></span>
                    </h4>
                </div>
                <s:if test="actualPayedAmount!=null">
                    <div class="note note-info">
                        <h4 class="block">已付总金额：<span class="span-payed-amount"><s:property value="actualPayedAmount" /></span>
                        </h4>
                    </div>
                </s:if>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
            <i class="fa fa-check"></i> 提交付款并完成任务
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <%-- <%@ include file="purchase-order-viewAudit.jsp"%> --%>
</form>
<div class="purchase-order-content ajaxify" data-url="${base}/myt/purchase/purchase-order!view?id=<s:property value='#parameters.id'/>"></div>
<script type="text/javascript">
    $(function() {

    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
