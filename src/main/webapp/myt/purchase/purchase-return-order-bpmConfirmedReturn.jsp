<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation"
	action="${base}/myt/purchase/purchase-return-order!bpmConfirmedReturn" method="post">
	<s:hidden name="taskId" value="%{#parameters.taskId}" />
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 确认退货成功
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<div class="purchase-return-order-content ajaxify"
	data-url="${base}/myt/purchase/purchase-return-order!view?id=<s:property value='#parameters.id'/>"></div>
<%@ include file="/common/ajax-footer.jsp"%>
