$(function() {
   $(".grid-myt-purchase-order-bpmReceiptAudit").data("gridOptions",{
              url : function() {
                   var purchaseOrderId = $(this).attr("data-pk");
                   if (purchaseOrderId) {
                       return WEB_ROOT + "/myt/purchase/purchase-receipt-detail!findByPage?search['EQ_purchaseOrderDetail.purchaseOrder.id']="+purchaseOrderId;
                   }
               },
               colModel : [{
                   label : '入库时间',
                   name : 'createdDate',
                   width : 80,
                   sorttype : 'date'
               } , {
                   label : '物流号',
                   name : 'purchaseReceipt.logisticsNo',
                   width : 200,
                   align : 'left'
               }, {
                   label : '入库商品',
                   name : 'commodity.display',
                   width : 200,
                   align : 'left'
               }, {
                   label : '收货仓库',
                   name : 'storageLocation.id',
                   width : 150,
                   stype : 'select',
                   searchoptions : {
                       value : Biz.getStockDatas()
                   }
               }, {
                   label : '批次号',
                   name : 'batchNo',
                   width : 80
               }, {
                   label : '批次过期日期',
                   name : 'expireDate',
                   width : 80,
                   sorttype : 'date'
               }, {
                   label : '单位',
                   name : 'measureUnit',
                   width : 80
               }, {
                   label : '数量',
                   name : 'quantity',
                   width : 80,
                   formatter : 'number'
               }, {
                   label : '单价',
                   name : 'price',
                   width : 80,
                   formatter : 'currency'
                }, {
                   label : '原始金额',
                   name : 'originalAmount',
                   width : 80,
                   formatter : 'currency'
                  
               }, {
                   label : '折扣率(%)',
                   name : 'discountRate',
                   width : 80,
                   formatter : 'number'
                   
               }, {
                   label : '折扣额',
                   name : 'discountAmount',
                   width : 80,
                   align : 'right'
               }, {
                   label : '折后金额',
                   name : 'amount',
                   width : 80,
                   formatter : 'currency',
                   responsive : 'sm'
               }, {
                   label : '商家运费',
                   name : 'deliveryAmount',
                   width : 80,
                   formatter : 'currency'
               }, {
                   label : '其他分摊费用',
                   name : 'roadAmount',
                   width : 80,
                   formatter : 'currency'
               }, {
                   label : '入库成本单价',
                   name : 'costPrice',
                   width : 80,
                   formatter : 'currency'
               }, {
                   label : '入库总成本',
                   name : 'costAmount',
                   width : 80,
                   formatter : 'currency'
               }]
               
           });
});
