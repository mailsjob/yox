<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form method="get" class="form-inline form-validation form-search-init"
			data-grid-search=".grid-purchase-purchase-receipt-select-stock-move" action="#">
			<div class="form-group">
				<input type="text" name="search['CN_voucher_OR_logisticsNo']" class="form-control input-xlarge"
					placeholder="订单号、物流单号..." />
			</div>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
		</form>
	</div>
</div>
<div class="row">
	<table class="grid-purchase-purchase-receipt-select-stock-move" data-grid="table"></table>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-purchase-purchase-receipt-select-stock-move").data("gridOptions", {
            url : "${base}/myt/stock/commodity-stock-move!findByPage?search['EQ_voucherState']=POST",
            colModel : [  {
                label : '主键',
                name : 'id',
                hidden:true,
                width : 50
            }, {
                label : '凭证号',
                name : 'voucher',
                width : 100
            }, {
                label : '记账日期',
                name : 'voucherDate',
                stype : 'date',
                width : 100
            }, {
                label : '凭证状态',
                name : 'voucherState',
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('voucherStateEnum')
                },

                width : 80
            }, {
                label : '经办人',
                name : 'voucherUser.display',
                width : 80
            }, {
                label : '经办部门',
                name : 'voucherDepartment.display',
                width : 100
            },{
                label : '发货Code',
                name : 'originStorageLocation.code',
                hidden : true
            },  {
                label : '发货仓库',
                name : 'originStorageLocation.id',
                width : 100,
                stype : 'select',
                formatter : 'select',
                searchoptions : {
                    value : Biz.getStockDatas()
                }
            }, {
                label : '目的仓库',
                name : 'destinationStorageLocation.id',
                width : 100,
                stype : 'select',
                formatter : 'select',
                searchoptions : {
                    value : Biz.getStockDatas()
                }
            }, {
                label : '物流公司',
                name : 'logistics.display',
                width : 100
            }, {
                label : '快递单号',
                name : 'logisticsNo',
                width : 100
            }, {
                label : '运费',
                name : 'logisticsAmount',
                hidden : true,
                width : 100
            } ],
            sortorder : "asc",
            sortname : "voucherDate",
            multiselect : false,
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : WEB_ROOT + "/myt/stock/commodity-stock-move!commodityStockMoveDetails?id=" + row_id,
                    colModel : [ {
                        name : 'id',
                        hidden : true,
                        responsive : 'sm'
                    }, {
                        name : 'commodity.id',
                        hidden : true,
                        width : 50
                    }, {
                        label : '商品',
                        name : 'commodity.display',
                        align : 'left'
                    }, {
                        label : '批次号',
                        name : 'batchNo',
                        width : 120,
                        align : 'center'
                    }, {
                        label : '到期日期',
                        name : 'expireDate',
                        width : 80,
                        sorttype : 'date',
                        align : 'center'
                    }, {
                        label : '单位',
                        name : 'measureUnit',
                        editable : true,
                        width : 60
                    }, {
                        label : '数量',
                        name : 'quantity',
                        width : 50,
                        formatter : 'number'
                    }, {
                        label : '已入库数量',
                        name : 'inStockQuantity',
                        width : 50,
                        align : 'center'
                    }, {
                        label : '商品成本单价',
                        name : 'price',
                        width : 60,
                        formatter : 'currency'
                    }, {
                        label : '商品成本金额',
                        name : 'amount',
                        width : 60,
                        formatter : 'currency'
                    }, {
                        label : '运费',
                        name : 'deliveryAmount',
                        width : 60,
                        formatter : 'currency'
                    }, {
                        label : '入库成本单价',
                        name : 'costPrice',
                        width : 60,
                        formatter : 'currency'
                    }, {
                        label : '入库总成本',
                        name : 'costAmount',
                        width : 60,
                        formatter : 'currency'
                    } ],
                    loadonce : true,
                    rowNum : -1,
                    multiselect : true,
                    operations : function(itemArray) {
                        var $grid = $(this);
                        var $select = $('<li><a href="javascript:;"><i class="fa fa-check-square-o"></i> 选取</a></li>');
                        $select.children("a").bind("click", function(e) {
                            e.preventDefault();
                            var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                            var $dialog = $grid.closest(".modal");
                            $dialog.modal("hide");
                            var callback = $dialog.data("callback");
                            if (callback) {
                                var $prow = $grid.closest('.ui-subgrid').prev();
                                var $pgrid = $prow.closest('.ui-jqgrid-btable');
                                var prowid = $prow.attr("id");
                                var data = {
                                    master : $pgrid.jqGrid("getRowData", prowid),
                                    rows : $grid.jqGrid("getSelectedRowdatas")
                                };
                                //alert(data.master['boxOrder.receivePerson']);
                                callback.call($grid, data);
                            }
                        });
                        itemArray.push($select);
                    }
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
