<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form
	class="form-horizontal form-bordered form-label-stripped form-validation form-myt-purchase-purchase-receipt-inputBasic"
	action="${base}/myt/purchase/purchase-receipt!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:hidden name="submitToStock" />
	<s:hidden name="purchaseOrder.id" />
	<s:hidden name="commodityStockMove.id" />
	<s:token />
	<div class="form-actions">
		<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
			data-grid-reload=".grid-myt-purchase-purchase-receipt">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
		<s:if test="%{voucherState.name()!='DRAFT'}">
			<s3:button disabled="%{disallowChargeAgainst!=null}" cssClass="btn red" type="submit"
				data-grid-reload=".grid-myt-purchase-purchase-receipt"
				data-form-action="${base}/myt/purchase/purchase-receipt!chargeAgainst" data-confirm="确认冲销当前采购单？">
				<s:property value="%{disallowChargeAgainst!=null?disallowChargeAgainst:'红冲'}" />
			</s3:button>
		</s:if>
		<div class="clearfix pull-right" style="margin-left: 10px">
			<a class="btn yellow btn-select-commodity-stock-move" href="javascript:;"><i class="fa fa-indent"> 从调拨单选取</i></a>
		</div>
		<div class="clearfix pull-right" style="margin-left: 10px">
			<a class="btn yellow btn-select-purchase-order" href="javascript:;"><i class="fa fa-indent"> 从采购订单选取</i></a>
		</div>

		<div class="clearfix pull-right" style="margin-left: 5px">
			<div class="input-group">
				<span class="input-group-addon">商品条码 <i class="fa fa-barcode"></i></span> <input type="text" name="barcode"
					class="form-control">
			</div>
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4 col-md-offset-8">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:textfield name="voucher" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:select name="voucherUser.id" list="usersMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:select name="voucherDepartment.id" list="departmentsMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<s3:datetextfield name="voucherDate" format="date" current="true" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">供货商</label>
					<div class="controls">
						<s:select name="supplier.id" list="suppliersMap" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">外部单号</label>
					<div class="controls">
						<s:textfield name="externalVoucher" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">外部标识</label>
					<div class="controls">
						<s:select name="externalSource" list="#{'MYT':'美月淘','IYB':'哎呦盒子','JD':'京东','TAOBAO':'淘宝','TMALL':'天猫'}" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">附加说明</label>
					<div class="controls">
						<s:textfield name="memo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-purchase-purchase-receipt-inputBasic" data-grid="items"
					data-readonly="<s:property value="%{voucherState.name()=='DRAFT'?false:true}"/>"
					data-pk='<s:property value="#parameters.id"/>' data-clone='<s:property value="#parameters.clone"/>'></table>
			</div>
		</div>
		<div class="row hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">折后商品总金额 </label>
					<div class="controls">
						<s:textfield name="amount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		
		<div class="row hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">整单折扣</label>
					<div class="controls">
						<s:textfield name="totalDiscountAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row hide">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">商家运费 </label>
					<div class="controls">
						<s:textfield name="totalDeliveryAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">其他分摊费用 </label>
					<div class="controls">
						<s:textfield name="totalRoadAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">已付总金额</label>
					<div class="controls">
						<s:textfield name="payedAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">入库成本总金额 </label>
					<div class="controls">
						<s:textfield name="totalCostAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>其他成本
						</div>
						<div class="tools">
							<a class="collapse" href="javascript:;"></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<table class="grid-myt-purchase-purchase-receipt-inputBasic-accountSubjects" data-grid="items"
									data-externalVoucher='<s:property value="externalVoucher"/>'></table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label class="control-label">付款(分摊)<br />总金额
									</label>
									<div class="controls">
										<div class="input-group">
											<s:textfield name="paymentRoadAmount" />
											<div class="input-group-btn">
												<button tabindex="-1" class="btn default btn-delivery-by-amount" type="button">按金额自动分摊</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label class="control-label">付款(非分摊)<br />费用总金额
									</label>
									<div class="controls">
										<s:textfield name="paymentCommodityAmount" readonly="true" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="form-actions right">
		<div class="clearfix pull-left" style="margin-left: 10px">
			<a class="btn yellow btn-select-purchase-order" href="javascript:;"><i class="fa fa-indent"> 从采购订单选取</i></a>
		</div>
		<div class="clearfix pull-left" style="margin-left: 10px">
			<a class="btn yellow btn-select-commodity-stock-move" href="javascript:;"><i class="fa fa-indent"> 从调拨单选取</i></a>
		</div>
		<button class="btn blue" type="submit" data-ajaxify-reload="_closest-ajax-container">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/purchase/purchase-receipt-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
