$(function() {
    $(".form-purchase-purchase-receipt-inputScan").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);

            //扫描枪输入处理
            $form.find("input[name='barcode']").barcodeScanSupport({
                onEnter : function() {
                    var $el = $(this);
                    var sid = $form.find("select[name='storageLocation.id']").val();
                    var barcode = $el.val();
                    if (sid == '' || barcode == '') {
                        alert("必须选取库存地和输入商品条码");
                        return;
                    }
                    if (barcode != '') {
                        var url = WEB_ROOT + "/myt/md/commodity!findByBarcode";
                        url += "?barcode=" + barcode;
                        $el.ajaxJsonUrl(url, function(data) {
                            $form.find(".commodity-display").html(data.display);
                            if (data.commodityVaryPrice) {
                                $form.find("input[name='costPrice']").val(data.commodityVaryPrice.lastPurchasePrice);
                            }
                            $form.find("input[name='commodity.id']").val(data.id);
                            $form.find("input[name='measureUnit']").val(data.measureUnit);

                            $form.find('[type="submit"]').attr("disabled", false);
                            var $quantity = $form.find("input[name='quantity']");
                            $quantity.val("").focus();

                            $form.find(".commodity-info").show();
                        })
                        $el.focus();
                    }
                }
            });
        },
        successCallback : function() {
            var $form = $(this);
            $form.find(".commodity-info").hide();

            $form.find('[type="submit"]').attr("disabled", true);
            var $barcode = $form.find("input[name='barcode']");
            $barcode.val("");
            $barcode.focus();
        }
    });

});