<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation" action="${base}/myt/purchase/purchase-order!inStoreFinish" method="post">
    <s:hidden name="taskId" value="%{#parameters.taskId}" />
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">

        <button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
            <i class="fa fa-check"></i> 入库完成
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body control-label-sm">
        <s:set var="taskVariablesVar" value="taskVariables" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">入库时间</label>
                    <div class="controls">

                        <s3:datetextfield name="inStoreDate" format="date" current="true" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">入库备注</label>
                    <div class="controls">
                        <s:textfield name="inStoreMemo" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%-- <%@ include file="purchase-order-viewAudit.jsp"%> --%>
</form>
<div class="purchase-order-content ajaxify" data-url="${base}/myt/purchase/purchase-order!view?id=<s:property value='#parameters.id'/>"></div>
<script type="text/javascript">
    $(function() {

    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
