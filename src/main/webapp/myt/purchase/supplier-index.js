$(function() {
    $(".grid-purchase-supplier-index").data("gridOptions", {
        url : WEB_ROOT + "/myt/purchase/supplier!findByPage",
        colNames : [ '主键','代码', '名称', '别名', '电话', '类型' ],
        colModel : [ {
            name:'id',
            hidden:true
        },{
            name : 'code',
            width : 150,
            editable : true
        }, {
            name : 'abbr',
            width : 150,
            editable : true,
            align : 'left'
        }, {
            name : 'alias',
            width : 150,
            editable : true,
            align : 'left'
        }, {
            name : 'contactPhone',
            editable : true,
            width : 150,
            align : 'left'
        }, {
            name : 'supplierType',
            editable : true,
            align : 'center',
            width : 100,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('supplierTypeEnum')
            }
        } ],
        sortorder:'desc',
        sortname:'id',
        editurl : WEB_ROOT + "/myt/purchase/supplier!doSave",
        delurl : WEB_ROOT + "/myt/purchase/supplier!doDelete"
    });
});