<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation"
	action="${base}/myt/purchase/purchase-return-order!bpmSendGoods" method="post">
	<s:hidden name="taskId" value="%{#parameters.taskId}" />
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body control-label-sm">
		<s:set var="taskVariablesVar" value="taskVariables" />
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">物流公司</label>
					<div class="controls">
						<s:select name="logistics.id" list="logisticsNameMap"/> 
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">物流单号</label>
					<div class="controls">
						<s:textfield name="logisticsNo" rows="3" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">整单运费</label>
					<div class="controls">
						<s:textfield name="totalDeliveryAmount" />
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<div class="purchase-return-order-content ajaxify"
	data-url="${base}/myt/purchase/purchase-return-order!view?id=<s:property value='#parameters.id'/>"></div>
<%@ include file="/common/ajax-footer.jsp"%>
