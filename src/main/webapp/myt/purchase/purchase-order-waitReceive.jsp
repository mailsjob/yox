<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation"
	action="${base}/myt/purchase/purchase-order!bpmSaveSupplierInfo" method="post">
	<s:hidden name="taskId" value="%{#parameters.taskId}" />
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue btn-post-url" type="button"
			data-url="${base}/myt/purchase/purchase-order!bpmSave?id=<s:property value='%{id}' />&taskId=<s:property value='%{#parameters.taskId}' />"
			data-confirm="确认直接完成当前任务？" data-ajaxify-reload=".ajaxify-tasks" data-close-container="true">
			<i class="fa fa-check"></i> 等待收货完成
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="portlet gren">
		<s:set var="taskVariablesVar" value="taskVariables" />
		<div class="portlet-title">
			<div class="caption">供货商发货信息</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">供货商：</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="supplier.display" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">发货时间：</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="#attr.taskVariablesVar['supplierSendTime']" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">物流公司：</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="#attr.taskVariablesVar['logisticsName']" />
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">物流单号：</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="#attr.taskVariablesVar['logisticsNo']" />
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<div class="purchase-order-content ajaxify"
	data-url="${base}/myt/purchase/purchase-order!view?id=<s:property value='#parameters.id'/>"></div>
<%@ include file="/common/ajax-footer.jsp"%>
