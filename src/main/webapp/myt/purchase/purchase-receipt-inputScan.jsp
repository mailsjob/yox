<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-purchase-purchase-receipt-inputScan"
	action="${base}/myt/purchase/purchase-receipt-detail!doSave" method="post">
	<s:hidden name="id" />
	<div class="form-actions form-inline">
		<s3:button type="submit" cssClass="btn blue" disabled="true">
			<i class="fa fa-check"></i> 保存
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body control-label-sm">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收货库存地</label>
					<div class="controls">
						<s:select list="{}" data-cache="Biz.getStockDatas()" name="storageLocation.id" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">商品条码</label>
					<div class="controls">
						<div class="input-group ">
							<s:textfield name="barcode" />
							<s:hidden name="commodity.id" />
							<span class="input-group-addon"><i class="fa fa-barcode"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet hide">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="list-group">
					<a class="list-group-item" href="javascript:;">Dapibus ac facilisis in</a> <a class="list-group-item"
						href="javascript:;">Morbi leo risus <span class="badge badge-info">3</span>
					</a> <a class="list-group-item" href="javascript:;">Porta ac consectetur ac <span class="badge badge-default">3</span></a>
					<a class="list-group-item" href="javascript:;">Vestibulum at eros</a>
				</div>
			</div>
		</div>
		<div class="portlet commodity-info" style="display: none">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="form-group">
					<label class="control-label">收货商品</label>
					<div class="controls">
						<p class="form-control-static commodity-display"></p>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label">计量单位</label>
					<div class="controls">
						<s:textfield name="measureUnit" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label">收货数量</label>
					<div class="controls">
						<s:textfield name="quantity" placeholder="输入总计收货入库数量" />
					</div>
				</div>
				<div class="form-group form-group-cost-price">
					<label class="control-label">入库成本单价</label>
					<div class="controls">
						<s:textfield name="costPrice" placeholder="输入本批次商品入库成本单价" />
						<span class="help-inline">注意：如果有其他相关费用如运费等，请折算计入成本单价</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<s3:button type="submit" cssClass="btn blue" disabled="true">
			<i class="fa fa-check"></i> 保存
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/purchase/purchase-receipt-inputScan.js" />
<%@ include file="/common/ajax-footer.jsp"%>
