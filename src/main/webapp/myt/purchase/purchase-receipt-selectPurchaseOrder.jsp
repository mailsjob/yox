<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form method="get" class="form-inline form-validation form-search-init"
			data-grid-search=".grid-purchase-purchase-receipt-select-order" action="#">
			<div class="form-group">
				<input type="text" name="search['CN_voucher_OR_logisticsNo']" class="form-control input-xlarge"
					placeholder="订单号、物流单号..." />
			</div>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-purchase-purchase-receipt-select-order" data-grid="table"></table>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-purchase-purchase-receipt-select-order").data("gridOptions", {
            url : '${base}/myt/purchase/purchase-order!findByPage',
            colModel : [ {
                label : '订单号',
                name : 'voucher',
                width : 100,
                align : 'center'
            }, {
                label : '供货商',
                name : 'supplier.id',
                hidden : true,
                width : 50
            }, {
                label : '供货商',
                name : 'supplier.display',
                align : 'left',
                width : 100
            }, {
                label : '发货时间',
                name : 'deliveryTime',
                sorttype : 'date',
                align : 'center',
                width : 150
            }, {
                label : '物流单号 ',
                name : 'logisticsNo',
                align : 'left',
                width : 100
            }, {
                label : '备注',
                name : 'purchaseMemo',
                width : 100
            }, {
                label : '当前活动',
                name : 'activeTaskName',
                align : 'center',
                width : 100
            }, {
                label : '采购总金额',
                name : 'totalAmount',
                width : 50,
                formatter : 'currency'
            }, {
                label : '付款模式',
                name : 'payMode',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('purchaseOrderPayModeEnum')
                }
            }, {
                label : '已预付金额',
                name : 'actualPayedAmount',
                width : 50,
                formatter : 'currency'
            } ],
            rowNum : 10,
            multiselect : false,
            postData : {
                "search['NN_logisticsNo']" : true
            },
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : "${base}/myt/purchase/purchase-order!purchaseOrderDetails?id=" + row_id,
                    colModel : [ {
                        name : 'id',
                        hidden : true,
                        responsive : 'sm'
                    }, {
                        name : 'commodity.id',
                        hidden : true,
                        width : 50
                    }, {
                        label : '商品',
                        name : 'commodity.display',
                        width : 250
                    }, {
                        name : 'storageLocation.id',
                        hidden : true,
                        width : 80
                    }, {
                        label : '库存地',
                        name : 'storageLocation.display',
                        width : 80
                    }, {
                        label : '采购单价',
                        name : 'price',
                        formatter : 'currency',
                        width : 100
                    }, {
                        label : '采购数量',
                        name : 'quantity',
                        formatter : 'number',
                        width : 100
                    }, {
                        label : '已收货数量',
                        name : 'recvQuantity',
                        width : 50,
                        align : 'center'
                    }, {
                        label : '单位',
                        name : 'measureUnit',
                        width : 80
                    }, {
                        label : '原始金额',
                        name : 'originalAmount',
                        formatter : 'currency',
                        width : 100
                    }, {
                        label : '折扣率',
                        name : 'discountRate',
                        formatter : 'number',
                        width : 80
                    }, {
                        label : '折扣额',
                        name : 'discountAmount',
                        formatter : 'currency',
                        width : 80
                    }, {
                        label : '折后金额',
                        name : 'amount',
                        formatter : 'currency',
                        width : 80
                    }/* , {
                                                                                                                                                label : '税率',
                                                                                                                                                name : 'taxRate',
                                                                                                                                                formatter : 'number',
                                                                                                                                                width : 80
                                                                                                                                            }, {
                                                                                                                                                label : '税额',
                                                                                                                                                name : 'taxAmount',
                                                                                                                                                formatter : 'currency',
                                                                                                                                                width : 80
                                                                                                                                            }, {
                                                                                                                                                label : '含税金额',
                                                                                                                                                name : 'commodityAndTaxAmount',
                                                                                                                                                formatter : 'currency',
                                                                                                                                                width : 80
                                                                                                                                            } */, {
                        label : '分摊运费',
                        name : 'deliveryAmount',
                        formatter : 'currency',
                        width : 80
                    }, {
                        label : '成本单价',
                        name : 'costPrice',
                        formatter : 'currency',
                        width : 80
                    } ],
                    rowNum : -1,
                    loadonce : true,
                    multiselect : true,
                    operations : function(itemArray) {
                        var $grid = $(this);
                        var $select = $('<li><a href="javascript:;"><i class="fa fa-check-square-o"></i> 选取</a></li>');
                        $select.children("a").bind("click", function(e) {
                            e.preventDefault();
                            var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                            var $dialog = $grid.closest(".modal");
                            $dialog.modal("hide");
                            var callback = $dialog.data("callback");
                            if (callback) {
                                var $prow = $grid.closest('.ui-subgrid').prev();
                                var $pgrid = $prow.closest('.ui-jqgrid-btable');
                                var prowid = $prow.attr("id");
                                var data = {
                                    master : $pgrid.jqGrid("getRowData", prowid),
                                    rows : $grid.jqGrid("getSelectedRowdatas")
                                };

                                callback.call($grid, data);
                            }
                        });
                        itemArray.push($select);
                    }
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
