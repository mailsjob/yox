
$(function() {
    $(".form-myt-stock-commodity-stock-move-printer").data("formOptions", {
        successCallback : function(response, submitButton) {
            var url = WEB_ROOT + "/rpt/jasper-report!preview";
            url += "?report=COMMODITY_STOCK_MOVE_LIST";
            url += "&reportParameters['COMMODITY_STOCK_MOVE_IDS']=" + response.userdata.voucher;
            window.open(url, "_blank");
        }
    });
});