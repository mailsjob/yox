<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="row search-form-default">
	<div class="col-md-12">
		<form action="#" method="get" class="form-inline form-validation  form-search-init"
			data-grid-search=".grid-myt-stock-stock-in-out-changeStatistics">
			<div class="input-group">
				<div class="form-group">
					<input type="text" style="width: 300px"
						name="search['CN_commodityStock.commodity.sku_OR_commodityStock.commodity.title_OR_commodityStock.storageLocation.code_OR_commodityStock.storageLocation.title_OR_voucher']"
						class="form-control" placeholder="商品名称、编号，库存地、凭证号...">
				</div>
				<div class="form-group">
					<input type="text" name="search['BT_createdDate']" class="form-control input-medium input-daterangepicker"
						placeholder="创建日期">
				</div>
				<div class="form-group">
					<div class="btn-group">
						<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
							类型 <i class="fa fa-angle-down"></i>
						</button>
						<div class="dropdown-menu hold-on-click dropdown-checkboxes">
							<s:checkboxlist name="search['IN_voucherType']" list="#application.enums.voucherTypeEnum"/>
						</div>
					</div>
				</div>
				<span class="form-group">
					<button class="btn green" type="submmit">
						<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
					</button>
					<button class="btn default hidden-inline-xs" type="reset">
						<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
					</button>
				</span>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-myt-stock-stock-in-out-changeStatistics"></table>
	</div>
</div>
<script src="${base}/myt/stock/stock-in-out-changeStatistics.js" />
<%@ include file="/common/ajax-footer.jsp"%>
