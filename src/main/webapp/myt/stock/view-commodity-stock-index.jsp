<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">商品库存汇总</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-view-commodity-stock-list">
						<div class="form-group">
							<input type="text" name="searchValue" class="form-control input-large" placeholder="商品编码，名称..." />
						</div>
						<div class="form-group">
							<div class="input-group input-medium">
								<span class="input-group-addon">截止时间 </span>
								<s3:datetextfield name="endTime" format="date" current="true" />
							</div>
						</div>
						<div class="form-group">
							<div class="input-group input-medium">
								<span class="input-group-addon">库存地</span>
								<s:select name="storageLocationSid" list="storageLocationMap" />
							</div>
						</div>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>

					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-view-commodity-stock-list"></table>

				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-view-commodity-stock-list").data("gridOptions", {
            url : '${base}/myt/stock/view-commodity-stock!findByPage',
            colNames : [],
            colModel : [],
            jqPivot : {
                rowTotalsText : '行项总计',
                xDimension : [ {
                    label : '商品',
                    dataName : 'title',
                    width : 200
                } ],
                yDimension : [ {
                    dataName : 'storage_display',
                    width : 200
                } ],
                aggregates : [ {
                    member : 'cur_stock_quantity',
                    aggregator : 'sum',
                    formatter : 'number',
                    align : 'right',
                    summaryType : 'sum',
                    width : 80,
                    label : '当前库存量'
                }, {
                    member : 'pre_in_quantit',
                    width : 80,
                    label : '采购在途量'
                }, {
                    member : 'pre_out_quantity',
                    aggregator : 'sum',
                    width : 80,
                    label : '预计出货量',
                    formatter : 'number',
                    align : 'right',
                    summaryType : 'sum'
                } ],
                rowTotals : true,
                colTotals : true
            }
        });

    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
