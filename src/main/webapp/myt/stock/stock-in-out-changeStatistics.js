$(function() {
    $(".grid-myt-stock-stock-in-out-changeStatistics").data("gridOptions", {
        url : WEB_ROOT + '/myt/stock/stock-in-out!findByPage',
        colModel : [  {
            label : '主键',
            name : 'id',
            width : 50,
            hidden : true
        },{
            label : '时间',
            name : 'createdDate',
            sorttype : 'date',
            width : 120,
            align : 'center'
        },{
            label : '库存记录主键',
            name : 'commodityStock.id',
            width : 100,
            hidden : true
        }, {
            label : '商品',
            name : 'commodityStock.commodity.display',
            index : 'commodity.sku_OR_commodity.title',
            width : 250
        }, {
            label : '库存地',
            name : 'commodityStock.storageLocation.id',
            width : 120,
            editrules : {
                required : true
            },
            stype : 'select',
            searchoptions : {
                value : Biz.getStockDatas()
            },
            align : 'left'
        },  {
            label : '批次号',
            name : 'commodityStock.batchNo',
            width : 100
        }, {
            label : '凭证号',
            name : 'voucher',
            width : 140,
            align : 'center'
        }, {
            label : '类型',
            name : 'voucherType',
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('voucherTypeEnum')
            }
        }, {
            label : '库存数量变化',
            name : 'diffQuantity',
            width : 80,
            align : 'right'
        },{
            label : '商品金额变化',
            name : 'diffStockAmount',
            width : 80,
            align : 'right'
        }, {
            label : '锁定量变化',
            name : 'diffSalingQuantity',
            width : 80,
            align : 'right'
        }, {
            label : '在途量变化',
            name : 'diffPurchasingQuantity',
            width : 80,
            align : 'right'
        },{
            label : '操作人',
            name : 'createdBy',
            align : 'center',
            width : 50
        }],
        sortorder : "desc",
        sortname : "createdDate",
        multiselect : false,
        footerrow : true,
        footerLocalDataColumn : [ 'diffQuantity', 'diffStockAmount','diffSalingQuantity','diffPurchasingQuantity'],
        postData : {
            "search['FETCH_commodityStock.commodity']" : "INNER.INNER",
            "search['FETCH_commodityStock.storageLocation']" : "INNER.INNER"
        }
    });
});