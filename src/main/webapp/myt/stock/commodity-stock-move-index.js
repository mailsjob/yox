$(function() {
    $(".grid-myt-stock-commodity-stock-move").data("gridOptions", {
        url : WEB_ROOT + '/myt/stock/commodity-stock-move!findByPage',
        colModel : [ {
            label : '凭证号',
            name : 'voucher',
            width : 140
        }, {
            label : '凭证日期',
            name : 'voucherDate',
            stype : 'date',
            width : 100
        }, {
            label : '凭证状态',
            name : 'voucherState',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('voucherStateEnum')
            },
            formatter : function(cellValue, options, rowdata, action) {
                if (cellValue == 'REDW') {
                    return '<span class="badge">红冲</span>'
                } else if (cellValue == 'DRAFT') {
                    return '<span class="badge badge-warning">草稿</span>'
                } else if (cellValue == 'POST') {
                    return '<span class="badge badge-success">提交</span>'
                }
                return cellValue;
            },
            width : 100
        }, {
            label : '经办人',
            name : 'voucherUser.display',
            width : 80
        }, {
            label : '经办部门',
            name : 'voucherDepartment.display',
            width : 80
        }, {
            label : '发货仓库',
            name : 'originStorageLocation.id',
            width : 120,
            stype : 'select',

            searchoptions : {
                value : Biz.getStockDatas()
            }
        }, {
            label : '目的仓库',
            name : 'destinationStorageLocation.id',
            width : 120,
            stype : 'select',

            searchoptions : {
                value : Biz.getStockDatas()
            }
        }, {
            label : '物流公司',
            name : 'logistics.display',
            width : 100
        }, {
            label : '快递单号',
            name : 'logisticsNo',
            width : 100
        }, {
            label : '运费',
            name : 'logisticsAmount',
            hidden : true,
            width : 100
        } ],
        sortorder : "desc",
        sortname : "voucherDate",
        editcol : 'voucher',
        fullediturl : WEB_ROOT + "/myt/stock/commodity-stock-move!inputTabs",
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/stock/commodity-stock-move!commodityStockMoveDetails?id=" + row_id,
                colModel : [ {
                    label : '商品',
                    name : 'commodity.display',
                    align : 'left'
                }, {
                    label : '单位',
                    name : 'measureUnit',
                    editable : true,
                    width : 60
                }, {
                    label : '数量',
                    name : 'quantity',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '已入库数量',
                    name : 'inStockQuantity',
                    width : 50,
                    formatter : function(cellValue, options, rowdata, action) {
                        var state = null;
                        if (cellValue == undefined) {
                            cellValue = 0;
                        }
                        cellValue = parseFloat(cellValue);
                        var quantity = parseFloat(rowdata.quantity);
                        if (cellValue <= quantity) {
                            state = 'badge-info';
                        } else {
                            state = 'badge-danger';
                        }
                        return '<span class="badge ' + state + '">' + cellValue + '</span>';
                    },
                    align : 'center'
                }, {
                    label : '商品成本单价',
                    name : 'price',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '商品成本金额',
                    name : 'amount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '商品成本金额',
                    name : 'deliveryAmount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '入库成本单价',
                    name : 'costPrice',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '入库总成本',
                    name : 'costAmount',
                    width : 60,
                    formatter : 'currency'
                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
});
