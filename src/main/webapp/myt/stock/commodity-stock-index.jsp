<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">分批次库存管理</a></li>
		<li><a class="tab-default" data-toggle="tab"
			href="${base}/myt/stock/commodity-stock!forward?_to_=sumByStorageLocation">按库存地汇总库存量</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/stock/commodity-stock!forward?_to_=sumByCommodity">按商品汇总库存量</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/stock/stock-in-out">库存变更流水记录</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/stock/stock-in-out!forward?_to_=changeStatistics">库存变更统计</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-myt-stock-commodity-stock">

						<div class="form-group">
							<input type="text" name="search['CN_commodity.sku_OR_commodity.title_OR_commodity.erpProductId']" class="form-control input-xlarge"
								placeholder="商品编码、名称、erp商品编码...">
						</div>
						<div class="form-group">
							<div class="btn-group">
								查看库存预警数据
								<s:checkbox name="search['LE_diffWarnStock']" fieldValue="0" />
							</div>
						</div>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>


					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-stock-commodity-stock"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/stock/commodity-stock-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
