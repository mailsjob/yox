<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-stock-commodity-stock-move-inputBasic"
	action="${base}/myt/stock/commodity-stock-move!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:hidden name="voucherState" value="POST" />
	<s:token />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
				data-grid-reload=".grid-myt-stock-commodity-stock-move">
				<i class="fa fa-check"></i>
				<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
			</s3:button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<button class="btn yellow" type="button" data-toggle="panel"
				data-url="${base}/myt/finance/payment-apply!bpmNew?bizVoucher=<s:property value='voucher'/>&bizVoucherType=DH">创建付款单申请</button>
			<s:if test="%{voucherState.name()!='DRAFT'}">
				<s3:button disabled="%{disallowChargeAgainst!=null}" cssClass="btn red pull-right" type="submit"
					data-grid-reload=".grid-myt-stock-commodity-stock-move" data-form-action="${base}/myt/stock/commodity-stock-move!chargeAgainst"
					data-confirm="确认冲销当前销售单？">
					<s:property value="%{disallowChargeAgainst!=null?disallowChargeAgainst:'红冲'}" />
				</s3:button>
			</s:if>
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">凭证状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.voucherStateEnum[voucherState]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 ">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:textfield name="voucher" readonly="%{notNew}" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:select name="voucherUser.id" list="usersMap" disabled="%{notNew}" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:select name="voucherDepartment.id" list="departmentsMap" disabled="%{notNew}" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<s3:datetextfield name="voucherDate" format="date" current="true" readonly="%{notNew}" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">出发仓库</label>
					<div class="controls">
						<s:select name="originStorageLocation.id" list="storageLocationMap"  disabled="%{notNew}" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">目的仓库</label>
					<div class="controls">
						<s:select name="destinationStorageLocation.id" list="storageLocationMap" disabled="%{notNew}"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">备注说明</label>
					<div class="controls">
						<s:textfield name="memo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-stock-commodity-stock-move-inputBasic" data-grid="items"
					data-readonly="<s:property value="%{voucherState.name()=='DRAFT'?false:true}"/>"
					data-pk='<s:property value="#parameters.id"/>' data-clone='<s:property value="#parameters.clone"/>'></table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">运费</label>
					<div class="controls">

						<div class="input-group">
							<s:textfield name="totalDeliveryAmount" />
							<div class="input-group-btn">
								<button tabindex="-1" class="btn default btn-delivery-by-amount" type="button">按金额自动分摊</button>
								<button tabindex="-1" data-toggle="dropdown" class="btn default dropdown-toggle" type="button">
									<i class="fa fa-angle-down"></i>
								</button>
								<ul role="menu" class="dropdown-menu pull-right">
									<li><a href="javascript:alert('TODO');">按数量自动分摊</a></li>
									<li><a href="javascript:alert('TODO');">平均分摊</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">入库总成本</label>
					<div class="controls">
						<s:textfield name="totalCostAmount" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6" ">
					<div class="form-group">
						<label class="control-label">快递公司</label>
						<div class="controls">
							<s:select name="logistics.id" list="logisticsNameMap"  />
						</div>
					</div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">快递单号</label>
					<div class="controls">
						<s:textfield name="logisticsNo" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-actions right">
		<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
				data-grid-reload=".grid-myt-stock-commodity-stock-move">
				<i class="fa fa-check"></i>
				<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
			</s3:button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<s:if test="%{voucherState.name()!='DRAFT'}">
				<s3:button disabled="%{disallowChargeAgainst!=null}" cssClass="btn red" type="submit"
					data-grid-reload=".grid-myt-stock-commodity-stock-move" data-form-action="${base}/myt/stock/commodity-stock-move!chargeAgainst"
					data-confirm="确认冲销当前销售单？">
					<s:property value="%{disallowChargeAgainst!=null?disallowChargeAgainst:'红冲'}" />
				</s3:button>
			</s:if>
	</div>
</form>
<script src="${base}/myt/stock/commodity-stock-move-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
