<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form method="get" class="form-inline form-validation form-search-init"
			data-grid-search=".grid-commodity-stock-selection" action="#">
			<div class="form-group">
				<s:textfield name="search['CN_commodity.sku_OR_commodity.barcode_OR_commodity.title']"
					cssClass="form-control input-xlarge" placeholder="商品编码/条码/名称..." />
			</div>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-commodity-stock-selection"></table>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-commodity-stock-selection").data("gridOptions", {
            url : "${base}/myt/stock/commodity-stock!findByPage",
            postData : {
                "search['FETCH_storageLocation']" : "INNER"
            },
            colModel : [ {
                name : 'id',
                hidden : true,
                hidedlg : true
            }, {
                name : 'commodity.id',
                hidden : true,
                hidedlg : true
            }, {
                name : 'commodity.display',
                hidden : true,
                hidedlg : true
            }, {
                name : 'commodity.measureUnit',
                hidden : true,
                hidedlg : true
            },{
                name : 'commodity.commodityVaryPrice.level1AgentSalePrice',
                hidden : true,
                hidedlg : true
            },{
                name : 'commodity.commodityVaryPrice.level2AgentSalePrice',
                hidden : true,
                hidedlg : true
            },{
                name : 'commodity.commodityVaryPrice.level3AgentSalePrice',
                hidden : true,
                hidedlg : true
            },{
                label : '商品主图',
                name : 'commodity.smallPic',
                width : 50,
                align : 'center',
                formatter : Biz.md5CodeImgViewFormatter
            }, {
                label : '商品编码',
                name : 'commodity.sku',
                searchoptions : {
                    defaultValue : "<s:property value='%{#parameters.sku}'/>"
                }, 
                align : 'center',
                width : 100
            }, {
                label : '商品名称',
                name : 'commodity.title',
                width : 200
            }, {
                label : '库存地',
                name : 'storageLocation.id',
                width : 180,
                editable : true,
                align:'center',
                stype : 'select',
                searchoptions : {
                    defaultValue : "<s:property value='%{#parameters.storageLocationId}'/>",
                    value : Biz.getStockDatas()
                }
            }, {
                label : '库存地',
                name : 'storageLocation.display',
                hidden : true
            }, {
                label : '指导售价',
                name : 'commodity.price',
                formatter : 'currency'
            }, {
                label : '批次号',
                name : 'batchNo',
                width : 120,
                align : 'center'
            },{
                label : '品牌',
                name : 'commodity.brand.title',
                width : 120,
                align : 'center'
            }, {
                label : '到期日期',
                name : 'expireDate',
                width : 80,
                sorttype : 'date',
                align : 'center'
            }, {
                label : '当前成本价',
                name : 'costPrice',
                width : 60,
                sorttype : 'currency',
                align : 'right'
            }, {
                label : '当前实物库存',
                name : 'curStockQuantity',
                width : 60,
                sorttype : 'number',
                align : 'right'
            }, {
                label : '待出库存',
                name : 'salingTotalQuantity',
                width : 60,
                sorttype : 'number',
                align : 'right'
            }, {
                label : '待入库存',
                name : 'purchasingTotalQuantity',
                width : 60,
                sorttype : 'number',
                align : 'right'
            }, {
                label : '库存报警阀值',
                name : 'stockThresholdQuantity',
                width : 60,
                sorttype : 'number',
                align : 'right'
            }, {
                label : '计算可用库存',
                name : 'availableQuantity',
                width : 60,
                sorttype : 'number',
                align : 'right'
            }/* , {
                label : '辅助参数',
                name : 'isOnSelected',
                width : 60,
                hidden:true
            }  */ ],
            postData : {
                "search['FETCH_commodity.category']" : "INNER.LEFT",
                "search['FETCH_commodity.brand']" : "INNER.LEFT",
                "search['FETCH_storageLocation']" : "INNER"
            },
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var isGift = BooleanUtil.toBoolean("${param.isGift}");
                var $grid = $(this);
                var rowdata = $grid.jqGrid("getRowData", id);
                document.getElementById(id).style.backgroundColor="#C1FFC1";  
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    rowdata.id = id;
                    rowdata.gift = isGift;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
