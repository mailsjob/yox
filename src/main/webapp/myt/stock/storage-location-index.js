$(function() {
    $(".grid-stock-storage-location-index").data("gridOptions", {
        url : WEB_ROOT + "/myt/stock/storage-location!findByPage",
        colNames : [ '代码', '名称','库存模式', '地址','停用' ],
        colModel : [ {
            name : 'code',
            width : 150,
            editable : true,
            align : 'center'
        }, {
            name : 'title',
            editable : true,
            width : 200,
            align : 'left'
        }, {
            label : '库存模式',
            stype : 'select',
            name : 'storageMode',
            align : 'center',
            editable:true,
            searchoptions : {
                value : Util.getCacheEnumsByType('storageModeEnum')
            },
            
            width : 80
        }, {
            name : 'addr',
            editable : true,
            width : 200,
            align : 'left'
        }, {
            name : 'disabled',
            editable : true,
            width : 20,
            edittype : 'checkbox'
        }  ],
        editurl : WEB_ROOT + "/myt/stock/storage-location!doSave",
        delurl : WEB_ROOT + "/myt/stock/storage-location!doDelete"
    });
});