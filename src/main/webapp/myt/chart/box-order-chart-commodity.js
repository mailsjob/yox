
$(function() {

    $(".form-chart-myt-box-order-chart-commodity").data("formOptions", {
        submitHandler : function(form) {
            var $form = $(form);
            var formdata = $form.serialize();
            var $chart1 = $(".chart-myt-box-order-chart-commdity-detail");
            mapData1=$chart1.ajaxJsonSync(WEB_ROOT + "/myt/chart/box-order-chart!commodityDetailDatas", formdata);
            var ticks1=mapData1['ticks'];
            var datas1=mapData1['datas'];
            //$chart1.attr("style","height:"+datas1.length*100);
            $chart1.attr("style","height:1000");
            $chart1.plot({
                data : [ {
                    data : function() {
                       return datas1;
                    }
                 } ],
                options : {
                    series: {
                        bars: {
                            show: true
                        },
                        lines: {
                            show: false
                        },
                        points: {
                            show:false
                        }
                    },
                    bars: {
                        align: "center",
                        barWidth: 0.5,
                        horizontal: true,
                        fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1}] },
                        lineWidth: 1
                    },
                    xaxis: {
                        axisLabel: "购买量",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 10,
                        max: datas1[0].quantity,
                        tickColor: "#5E5E5E",
                        color: "black"
                    },

                    yaxis: {
                        axisLabel: "商品",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 3,
                        tickColor: "#5E5E5E",
                        ticks: ticks1,
                        color: "black"
                    }

                }
            });
            $chart1.UseTooltip();
            //图2
            var $chart2 = $(".chart-myt-box-order-chart-commdity-order");
            mapData2=$chart2.ajaxJsonSync(WEB_ROOT + "/myt/chart/box-order-chart!commodityOrderDatas", formdata);
            var ticks2=mapData2['ticks'];
            var datas2=mapData2['datas'];
            $chart2.attr("style","height:"+datas2.length*100);
            $chart2.plot({
                data : [ {
                    data : function() {
                       return datas2;
                    }
                 } ],
                options : {
                    series: {
                        bars: {
                            show: true
                        },
                        lines: {
                            show: false
                        },
                        points: {
                            show:false
                        }
                    },
                    bars: {
                        align: "center",
                        barWidth: 0.5,
                        horizontal: true,
                        fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1}] },
                        lineWidth: 1
                    },
                    xaxis: {
                        axisLabel: "购买量",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 10,
                        max: datas2[0].quantity,
                        tickColor: "#5E5E5E",
                        color: "black"
                    },

                    yaxis: {
                        axisLabel: "商品",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 3,
                        tickColor: "#5E5E5E",
                        ticks: ticks2,
                        color: "black"
                    }

                }
            });
            $chart2.UseTooltip();
            //图3
            /*var $chart3 = $(".chart-myt-box-order-chart-commdity-visit");
            mapData3=$chart3.ajaxJsonSync(WEB_ROOT + "/myt/chart/box-order-chart!commodityOrderDatas", formdata);
            var ticks3=mapData3['ticks'];
            var datas3=mapData3['datas'];
            $chart3.attr("style","height:"+datas3.length*100);
            $chart3.plot({
                data : [ {
                    data : function() {
                       return datas3;
                    }
                 } ],
                options : {
                    series: {
                        bars: {
                            show: true
                        },
                        lines: {
                            show: false
                        },
                        points: {
                            show:false
                        }
                    },
                    bars: {
                        align: "center",
                        barWidth: 0.5,
                        horizontal: true,
                        fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1}] },
                        lineWidth: 1
                    },
                    xaxis: {
                        axisLabel: "购买量",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 10,
                        max: datas3[0].quantity,
                        tickColor: "#5E5E5E",
                        color: "black"
                    },

                    yaxis: {
                        axisLabel: "商品",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 3,
                        tickColor: "#5E5E5E",
                        ticks: ticks2,
                        color: "black"
                    }

                }
            });
            $chart3.UseTooltip();*/
         } 
    });
    
});
var previousPoint = null, previousLabel = null;

$.fn.UseTooltip = function () {
    $(this).bind("plothover", function (event, pos, item) {
        if (item) {
            if ((previousLabel != item.series.label) ||
         (previousPoint != item.dataIndex)) {
                previousPoint = item.dataIndex;
                previousLabel = item.series.label;
                $("#tooltip").remove();

                var x = item.datapoint[0];
                var y = item.datapoint[1];

                var color = item.series.color;
                showTooltip(item.pageX,
                item.pageY,
                color,
                item.series.yaxis.ticks[y].label.split("(")[0] +
                " : <strong><font color='red'>" + x + "</font></strong> ");
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
};

function showTooltip(x, y, color, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y - 10,
        left: x + 10,
        border: '2px solid ' + color,
        padding: '3px',
        'font-size': '9px',
        'border-radius': '5px',
        'background-color': '#fff',
        'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        opacity: 0.9
    }).appendTo("body").fadeIn(200);
}

 