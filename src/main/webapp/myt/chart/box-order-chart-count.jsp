<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form method="get" class="form-inline form-validation form-search-init form-chart-myt-box-order-chart-count" action="#">
	<div class="row search-form-default">
		<div class="col-md-12">
			<div class="form-group ">
				<div class="input-group input-medium">
					<span class="input-group-addon">开始日期</span>
					<s3:datetextfield name="dateFrom" offset="-30d" />
				</div>
			</div>
			<div class="form-group">
				<div class="input-group input-medium">
					<span class="input-group-addon">截止日期</span>
					<s3:datetextfield name="dateTo" current="true" />
				</div>
			</div>
			<div class="form-group">
				<div class="input-group input-small">
					<span class="input-group-addon">统计模式</span>
					<s:select name="groupBy" list="#{'day':'按天','month':'按月','year':'按年'}" value="'day'" data-allowClear="false" />
				</div>
			</div>
			<div class="form-group">
				<div class="btn-group">
					<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
						来源 <i class="fa fa-angle-down"></i>
					</button>
					<div class="dropdown-menu hold-on-click dropdown-checkboxes">
						<s:checkboxlist name="orderFrom" list="#application.enums.orderFromEnum"
							value="#application.enums.orderFromEnum.keys" />
					</div>
				</div>
			</div>
			<button class="btn green focus" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="portlet box red">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i>销售订单走势折线图
					</div>
					<div class="tools">
						<a href="javascript:;" class="collapse"></a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="chart chart-myt-box-order-chart-count" style="height: 400px"></div>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
$(function() {

    $(".form-chart-myt-box-order-chart-count").data("formOptions", {
        submitHandler : function(form) {
            var $form = $(form);
            var $chart = $(".chart-myt-box-order-chart-count");
            $chart.plot({
                data : [ {
                    data : function() {
                        var formdata = $form.serialize();
                        return $chart.ajaxJsonSync(WEB_ROOT + "/myt/chart/box-order-chart!quantityDatas", formdata);
                    },
                    label : "订单数量"
                }, {
                    data : function() {
                        var formdata = $form.serialize();
                        return $chart.ajaxJsonSync(WEB_ROOT + "/myt/chart/box-order-chart!amountDatas", formdata);
                    },
                    label : "订单金额",
                    yaxis : 2
                } ],
                options : {
                    series: {
                        lines: {
                            show: true
                        },
                        points: {
                            radius: 3,
                            fill: true,
                            show: true
                        }
                    },
                    
                    xaxes : [ {
                        mode : "time",
                        tickSize : [ 2, $form.find('select[name="groupBy"]').val()],
                        axisLabelUseCanvas: true,
                        tickLength: 0,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabel: "时间线",
                        axisLabelPadding: 10
                    } ],
                    yaxes : [{
                        axisLabel: "订单数量",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 3
                        
                    }, {
                        axisLabel: "订单金额",
                        position: "right",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 3
                        
                    }]

                }
            });
            $chart.UseTooltip();
        }
    });
});
var previousPoint = null, previousLabel = null;
$.fn.UseTooltip = function () {
    $(this).bind("plothover", function (event, pos, item) {
        if (item) {
            if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                previousPoint = item.dataIndex;
                previousLabel = item.series.label;
                $("#tooltip").remove();

                var x = item.datapoint[0];
                var y = item.datapoint[1];

                var color = item.series.color;
                if (item.seriesIndex == 0) {
                    showTooltip(item.pageX,
                    item.pageY,
                    color,
                    "<strong>" + item.series.label + "</strong>"+ " : <strong>" + y + "</strong><br>" + dateFormat(x) );
                } else {
                    showTooltip(item.pageX,
                    item.pageY,
                    color,
                    "<strong>" + item.series.label + "</strong>"+ " : <strong>￥" + y + "</strong><br>" + dateFormat(x) );
                }
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
};

function showTooltip(x, y, color, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y - 30,
        left: x - 120,
        border: '2px solid ' + color,
        padding: '3px',
        'font-size': '9px',
        'border-radius': '5px',
        'background-color': '#fff',
        'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        opacity: 0.9
    }).appendTo("body").fadeIn(200);
}
function dateFormat(date){
    var d=new Date(date);
    if(d.getMonth()<9){
        return d.getFullYear()+"-0"+(d.getMonth()+1)+"-"+d.getDate();  
    }
    return d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
}
</script>
<%-- <script src="${base}/myt/chart/box-order-chart-count.js" /> --%>
<%@ include file="/common/ajax-footer.jsp"%>