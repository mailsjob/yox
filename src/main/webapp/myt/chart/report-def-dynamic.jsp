<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-chart-report-def-dynamic"
	action="#" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<div class="form-actions">
			<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
				<i class="fa fa-check"></i> 保存
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
		
	</div>
	<div class="form-body control-label-sm">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">模板类型</label>
					<div class="controls">
					<s:select name="fileName" list="#{'FINANCE_COUNT':'财务报表','COMMODITY_SALE_COUNT':'商品销售报表'}"  requiredLabel="true"/>
					</div>
				</div>
			</div>
			</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">模板号</label>
					<div class="controls">
						<input type="text" name="searchDate"
							class="form-control input-medium input-daterangepicker grid-param-data" placeholder="查询日期" pattern="" required="true" >
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/chart/report-def-dynamic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
