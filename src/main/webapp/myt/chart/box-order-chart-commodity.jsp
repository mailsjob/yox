<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form method="get" class="form-inline form-validation form-search-init form-chart-myt-box-order-chart-commodity" action="#">
	<div class="row search-form-default">
		<div class="col-md-12">
			<div class="form-group ">
				<div class="input-group input-medium">
					<span class="input-group-addon">开始日期</span>
					<s3:datetextfield name="dateFrom" offset="-30d" />
				</div>
			</div>
			<div class="form-group">
				<div class="input-group input-medium">
					<span class="input-group-addon">截止日期</span>
					<s3:datetextfield name="dateTo" current="true" />
				</div>
			</div>
			
			<div class="form-group">
				<div class="btn-group">
					<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
						来源 <i class="fa fa-angle-down"></i>
					</button>
					<div class="dropdown-menu hold-on-click dropdown-checkboxes">
						<s:checkboxlist name="orderFrom" list="#application.enums.orderFromEnum"
							value="#application.enums.orderFromEnum.keys" />
					</div>
				</div>
			</div>
			<button class="btn green focus" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			
		</div>
	</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i>商品销量排名
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					
				</div>
			</div>
			<div class="portlet-body">
				<div class="chart chart-plot chart-myt-box-order-chart-commdity-detail" ></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i>商品下单量排名
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="chart chart-plot chart-myt-box-order-chart-commdity-order" ></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i>商品浏览量排名
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="chart chart-plot chart-myt-box-order-chart-commdity-visit" style="height: 400px"></div>
			</div>
		</div>
	</div>
</div>
</form>
<script src="${base}/myt/chart/box-order-chart-commodity.js" />

<%@ include file="/common/ajax-footer.jsp"%>