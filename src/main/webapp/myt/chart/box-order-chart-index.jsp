<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/chart/box-order-chart!count">销售订单统计</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/chart/box-order-chart!forward?_to_=commodity">销售商品统计</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
</div>
<%@ include file="/common/ajax-footer.jsp"%>