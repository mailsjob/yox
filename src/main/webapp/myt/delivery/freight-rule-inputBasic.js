$(function() {
    $(".form-myt-delivery-freight-rule-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
        }
    });
    $(".table-ft-rule-regions").data("dynamicTableOptions", {
        afterAdd : function($tr) {
            $tr.find("input[name$='region.display']").data('data-dropdownselect-callback', function(json) {
                $tr.find("[name$='region.display']").val(json.display);
                $tr.find("[name$='region.id']").val(json.id);
            });
        }
    });
});