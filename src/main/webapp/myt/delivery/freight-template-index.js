$(function() {
    $(".grid-myt-delivery-freight-template-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/delivery/freight-template!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '模板名称',
            name : 'templateName',
            width : 255,
            editable : true,
            align : 'left'
        }, {
            label : '宝贝地址',
            name : 'region.display',
            index : 'region',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '计价方式',
            name : 'valuationType',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('valuationTypeEnum')
            },
            width : 80,
            editable : true,
            align : 'center'
        } ],
        postData : {
            "search['FETCH_region']" : "LEFT"
        },
        editurl : WEB_ROOT + '/myt/delivery/freight-template!doSave',
        delurl : WEB_ROOT + '/myt/delivery/freight-template!doDelete',
        fullediturl : WEB_ROOT + '/myt/delivery/freight-template!inputTabs'
    });
});
