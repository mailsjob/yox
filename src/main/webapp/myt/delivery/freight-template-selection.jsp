<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
    <div class="col-md-12">
        <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-myt-freight-template-selection">
            <div class="form-group">
                <label class="sr-only">运费模板</label> <input type="text" name="search['CN_templateName']" class="form-control input-xlarge" placeholder="模板名称..." />
            </div>
            <button class="btn default" type="reset">
                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
            </button>
            <button class="btn green" type="submmit">
                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
            </button>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="grid-myt-freight-template-selection"></table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-freight-template-selection").data("gridOptions", {
            url : WEB_ROOT + '/myt/delivery/freight-template!findByPage',
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true
            }, {
                label : '模板名称',
                name : 'templateName',
                width : 255,
                editable : true,
                align : 'left'
            }, {
                label : '地址',
                name : 'region.display',
                index : 'region',
                width : 200,
                editable : true,
                align : 'left'
            }, {
                label : '计价方式',
                name : 'valuationType',
                formatter : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('valuationTypeEnum')
                },
                width : 80,
                editable : true,
                align : 'center'
            } ],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>