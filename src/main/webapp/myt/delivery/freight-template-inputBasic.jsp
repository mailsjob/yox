<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-delivery-freight-template-inputBasic" action="${base}/myt/delivery/freight-template!doSave?commodity=<s:property value='#parameters.commodity'/>" method="post" data-editrulesurl="${base}/myt/delivery/freight-template!buildValidateRules?commodity=<s:property value='#parameters.commodity'/>">
    <s:hidden name="id" />
    <s:hidden name="commodity" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-delivery-freight-template-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板名称</label>
                    <div class="controls">
                        <s:textfield name="templateName" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">宝贝地址</label>
                    <div class="controls">
                        <s:textfield name="region.display" data-toggle="dropdownselect" data-url="${base}/myt/md/region!forward?_to_=selection" data-selected="%{parent.id}" />
                        <s:hidden name="region.id" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">计价方式</label>
                    <div class="controls">
                        <s:radio name="valuationType" list="#application.enums.valuationTypeEnum" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">运送方式</label>
                    <div class="controls">
                        <table class="table table-template-rules" data-dynamic-table="true">
                            <thead>
                                <tr>
                                    <td>运送到</td>
                                    <td>首重（kg）</td>
                                    <td>首费（元）</td>
                                    <td>续重（kg）</td>
                                    <td>续费（元）</td>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator value="templateRules" var="item" status="status">
                                    <tr>
                                        <s:hidden name="%{'templateRules['+#status.index+'].extraAttributes.operation'}" value="update" />
                                        <s:hidden name="%{'templateRules['+#status.index+'].template.id'}" />
                                        <td><s:textfield name="%{'templateRules['+#status.index+'].rule.regions'}" data-minWidth="800px"
                                                data-toggle="dropdownselect" data-url="${base}/myt/md/region!forward?_to_=proSelection"
                                                data-selected="%{templateRules[#status.index].rule.regions}" />
                                            <s:hidden name="%{'templateRules['+#status.index+'].rule.regionsId'}" /></td>
                                        <td><s:textfield name="%{'templateRules['+#status.index+'].rule.startWeight'}" /></td>
                                        <td><s:textfield name="%{'templateRules['+#status.index+'].rule.startFee'}" /></td>
                                        <td><s:textfield name="%{'templateRules['+#status.index+'].rule.addWeight'}" /></td>
                                        <td><s:textfield name="%{'templateRules['+#status.index+'].rule.addFee'}" /></td>
                                    </tr>
                                </s:iterator>
                                <tr class="dynamic-table-row-template">
                                    <s:hidden name="templateRules[X].extraAttributes.operation" value="create" />
                                    <s:hidden name="templateRules[X].template.id" value="%{id}" />
                                    <td><s:textfield name="%{'templateRules[X].rule.regions'}" data-toggle="dropdownselect" data-minWidth="800px"
                                            data-url="${base}/myt/md/region!forward?_to_=proSelection" data-selected="%{'templateRules[X].rule.regionsId'}" />
                                        <s:hidden name="%{'templateRules[X].rule.regionsId'}" /></td>
                                    <td><s:textfield name="%{'templateRules[X].rule.startWeight'}" required="true"/></td>
                                    <td><s:textfield name="%{'templateRules[X].rule.startFee'}" required="true"/></td>
                                    <td><s:textfield name="%{'templateRules[X].rule.addWeight'}" required="true"/></td>
                                    <td><s:textfield name="%{'templateRules[X].rule.addFee'}" required="true"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-delivery-freight-template-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script type="text/javascript">
$(function() {
    // 运送到
    $(".table-template-rules").data("dynamicTableOptions", {
        afterAdd : function($tr) {
            $tr.find("input[name$='rule.regions']").data('data-dropdownselect-callback', function(json) {
                $tr.find("[name$='rule.regions']").val(json.display);
                $tr.find("[name$='rule.regionsId']").val(json.id);
            });
        }
    });
    $(".form-myt-delivery-freight-template-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            // 宝贝地址
            $form.find("input[name='region.display']").data('data-dropdownselect-callback', function(json) {
                $form.find("input[name='region.display']").val(json.display);
                $form.find("input[name='region.id']").val(json.id);
            });
        }
    });
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>