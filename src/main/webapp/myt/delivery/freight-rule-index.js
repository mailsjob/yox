$(function() {
    $(".grid-myt-delivery-freight-rule-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/delivery/freight-rule!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '区域',
            name : 'regions',
            width : 60,
            align : 'right'
        }, {
            label : '首重',
            name : 'startWeight',
            width : 60,
            editable : true,
            align : 'right'
        }, {
            label : '首费',
            name : 'startFee',
            width : 60,
            formatter : 'number',
            editable : true,
            align : 'right'
        }, {
            label : '续重',
            name : 'addWeight',
            width : 60,
            editable : true,
            align : 'right'
        }, {
            label : '续费',
            name : 'addFee',
            width : 60,
            formatter : 'number',
            editable : true,
            align : 'right'
        } ],
        editurl : WEB_ROOT + '/myt/delivery/freight-rule!doSave',
        delurl : WEB_ROOT + '/myt/delivery/freight-rule!doDelete',
        fullediturl : WEB_ROOT + '/myt/delivery/freight-rule!inputTabs'
    });
});
