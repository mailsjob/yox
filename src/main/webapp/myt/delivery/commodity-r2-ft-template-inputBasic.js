$(function() {
    $(".form-myt-delivery-commodity-r2-ft-template-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            // 双击选取 运费模板
            $form.find(".fa-select-freightTemplate").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/delivery/freight-template!forward?_to_=selection',
                    title : '选取运费模板',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'freightTemplate.id' : rowdata.id,
                            'freightTemplate.templateName' : rowdata.templateName
                        });
                    }
                })
            });
            // 双击选取 商品
            $form.find(".fa-select-commodity").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                    title : '选取商品',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'commodity.id' : rowdata.id,
                            'commodity.display' : rowdata.display
                        });
                    }
                })
            });
        }
    });
});