$(function() {
    $(".grid-myt-delivery-commodity-r2-ft-template-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/delivery/commodity-r2-ft-template!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '运费模板',
            name : 'freightTemplate.templateName',
            index : 'freightTemplate',
            editable : true,
            editoptions : {
                placeholder : '请双击选取',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    // Callback
                    var select = function(sid, templateName) {
                        $grid.jqGrid("setEditingRowdata", {
                            'freightTemplate.id' : sid,
                            'freightTemplate.templateName' : templateName
                        });
                    }
                    $elem.dblclick(function() {
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/delivery/freight-template!forward?_to_=selection',
                            title : '选取运费模板',
                            callback : function(item) {
                                select(item.id, item.templateName);
                            }
                        })
                    });
                }
            },
            align : 'left'
        }, {
            label : '商品',
            name : 'commodity.display',
            index : 'commodity',
            editable : true,
            editoptions : {
                placeholder : '请双击选取',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    // Callback
                    var select = function(sid, display) {
                        $grid.jqGrid("setEditingRowdata", {
                            'commodity.id' : sid,
                            'commodity.display' : display
                        });
                    }
                    $elem.dblclick(function() {
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                            title : '选取商品',
                            callback : function(item) {
                                select(item.id, item.display);
                            }
                        })
                    });
                }
            },
            align : 'left'
        } ],
        postData : {
            "search['FETCH_commodity']" : "LEFT",
            "search['FETCH_freightTemplate']" : "LEFT"
        },
        editurl : WEB_ROOT + '/myt/delivery/commodity-r2-ft-template!doSave',
        delurl : WEB_ROOT + '/myt/delivery/commodity-r2-ft-template!doDelete',
        fullediturl : WEB_ROOT + '/myt/delivery/commodity-r2-ft-template!inputTabs'
    });
});
