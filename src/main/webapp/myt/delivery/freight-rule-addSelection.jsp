<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
    <div class="col-md-12">
        <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-myt-freight-rule-addSelection">
            <button class="btn default" type="reset">
                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
            </button>
            <button class="btn green" type="submmit">
                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
            </button>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="grid-myt-freight-rule-addSelection"></table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-freight-rule-addSelection").data("gridOptions", {
            url : "${base}/myt/delivery/freight-rule!findByPage",
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true
            }, {
                label : '区域',
                name : 'regions',
                width : 60,
                align : 'right'
            }, {
                label : '续重',
                name : 'addWeight',
                width : 60,
                editable : true,
                align : 'right'
            }, {
                label : '首重',
                name : 'startWeight',
                width : 60,
                editable : true,
                align : 'right'
            }, {
                label : '首费',
                name : 'startFee',
                width : 60,
                formatter : 'number',
                editable : true,
                align : 'right'
            }, {
                label : '续费',
                name : 'addFee',
                width : 60,
                formatter : 'number',
                editable : true,
                align : 'right'
            } ],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            },
            editurl : WEB_ROOT + '/myt/delivery/freight-rule!doSave',
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>