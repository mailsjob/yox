<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-delivery-freight-rule-inputBasic" action="${base}/myt/delivery/freight-rule!doSave" method="post" data-editrulesurl="${base}/myt/delivery/freight-rule!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-delivery-freight-rule-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">首重</label>
                    <div class="controls">
                        <s:textfield name="startWeight" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">首费</label>
                    <div class="controls">
                        <s:textfield name="startFee" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">续重</label>
                    <div class="controls">
                        <s:textfield name="addWeight" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">续费</label>
                    <div class="controls">
                        <s:textfield name="addFee" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">地区（多选）</label>
                    <div class="controls">
                        <table class="table table-ft-rule-regions" data-dynamic-table="true">
                            <thead>
                                <tr>
                                    <th>区域<span class="required">*</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator value="ftRuleR2Regions" var="item" status="status">
                                    <tr>
                                        <s:hidden name="%{'ftRuleR2Regions['+#status.index+'].extraAttributes.operation'}" value="update" />
                                        <s:hidden name="%{'ftRuleR2Regions['+#status.index+'].rule.id'}" />
                                        <td><s:textfield name="%{'ftRuleR2Regions['+#status.index+'].region.display'}" data-toggle="dropdownselect" data-url="${base}/myt/md/region!forward?_to_=proSelection" data-selected="%{ftRuleR2Regions[#status.index].region.display}" /> <s:hidden name="%{'ftRuleR2Regions['+#status.index+'].region.id'}" /></td>
                                    </tr>
                                </s:iterator>
                                <tr class="dynamic-table-row-template">
                                    <s:hidden name="ftRuleR2Regions[X].extraAttributes.operation" value="create" />
                                    <s:hidden name="ftRuleR2Regions[X].rule.id" value="%{id}" />
                                    <td><s:textfield name="%{'ftRuleR2Regions[X].region.display'}" data-toggle="dropdownselect" data-url="${base}/myt/md/region!forward?_to_=proSelection" data-selected="%{parent.id}" /> <s:hidden name="%{'ftRuleR2Regions[X].region.id'}" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-delivery-freight-rule-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/delivery/freight-rule-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>