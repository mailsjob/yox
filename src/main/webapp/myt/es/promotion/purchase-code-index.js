$(function() {
    $(".grid-myt-partner-purchase-code").data("gridOptions", {
        url : WEB_ROOT + '/myt/es/promotion/purchase-code!findByPage',
        colModel : [ {
            label : '消费码号',
            name : 'code',
            width : 100,
            align : 'center'
        }, {
            label : '商品/服务',
            name : 'commodity.title',
            align : 'left'
        }, {
            label : '消费码状态',
            name : 'codeStatus',
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('purchaseCodeStatusEnum')
            },

        }, {
            label : '使用时间',
            name : 'usedDatetime',
            width : 120,
            sorttype : 'date',
            align : 'center'
        }, {
            label : '过期时间',
            name : 'expiredDatetime',
            width : 120,

            sorttype : 'date',
            align : 'center'
        }, {
            label : '客户',
            name : 'boxOrder.receivePerson',
            width : 100,
            align : 'center'
        } ],
        operations : function(itemArray) {
            var url = WEB_ROOT + "/myt/es/promotion/purchase-code!useCode";

            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;" data-url="' + url + '"><i class="fa fa-dot-circle-o "></i>使用此消费码</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                var id = $grid.getOnlyOneSelectedItem();
                if (id) {
                    url = Util.AddOrReplaceUrlParameter(url, "id", id);
                    $(this).ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认使用此消费码吗？"
                    })
                }
            });
            itemArray.push($select);
        }
    });
});
