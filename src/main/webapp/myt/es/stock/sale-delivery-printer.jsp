<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-es-stock-sale-delivery-printer"
	action="${base}/myt/es/stock/sale-delivery!doSave?prepare=true" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<button class="btn blue" type="submit">
				<i class="fa fa-check"></i> 保存并打印
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">发货方式</label>
					<div class="controls">
						<s:textfield name="deliveryMode" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">提货人单位</label>
					<div class="controls">
						<s:textfield name="pickUpCompany" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">提货人</label>
					<div class="controls">
						<s:textfield name="pickUpPerson" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">提货人电话</label>
					<div class="controls">
						<s:textfield name="pickUpMobile" />
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">审批人</label>
					<div class="controls">
						<s:textfield name="pickUpAuditBy" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">提货日期</label>
					<div class="controls">
						<s3:datetextfield format="date" name="pickUpDate" current="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收货地址</label>
					<div class="controls">
						<s:textfield name="pickUpDeliveryAddr" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存并打印
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/es/stock/sale-delivery-printer.js" />
<%@ include file="/common/ajax-footer.jsp"%>
