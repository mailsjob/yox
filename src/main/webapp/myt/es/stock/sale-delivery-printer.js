$(function() {
    $(".form-myt-es-stock-sale-delivery-printer").data("formOptions", {
        successCallback : function(response, submitButton) {
            var url = WEB_ROOT + "/rpt/jasper-report!preview";
            url += "?report=PARTNER_SALE_DELIVERY_COMMODITY_LIST";
            url += "&reportParameters['SALE_DELIVERY_IDS']=" + response.userdata.voucher;
            window.open(url, "_blank");
        }
    });
});