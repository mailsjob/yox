$(function() {
    $(".grid-myt-partner-partner-sale-delivery").data("gridOptions", {
        url : WEB_ROOT + '/myt/es/stock/sale-delivery!findByPage',
        colModel : [ {
            label : '销售单凭证号',
            name : 'voucher',
            align : 'center',
            width : 120
        }, {
            label : '销售单凭证日期',
            name : 'voucherDate',
            stype : 'date',
            width : 100
        }, {
            label : '发货方式',
            name : 'deliveryMode',
            width : 100
        }, {
            label : '提货人',
            name : 'pickUpPerson',
            width : 100
        }, {
            label : '提货人电话',
            name : 'pickUpMobile',
            width : 100
        }, {
            label : '收货地址',
            name : 'pickUpDeliveryAddr',
            width : 250
        }, {
            label : '物流公司',
            name : 'logistics.id',
            align : 'center',
            stype : 'select',
            editoptions : {

                value : Biz.getLogisticsDatas()
            },
            width : 200
        }, {
            label : '快递单号',
            name : 'logisticsNo',
            width : 150
        }, {
            label : '物流费用',
            name : 'logisticsAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '备注',
            name : 'memo',
            width : 400
        } ],
        postData : {
            "search['NN_auditDate']" : true
        },
        editcol : 'voucher',
        footerrow : true,
        footerLocalDataColumn : [ 'logisticsAmount' ],
        addable : false,
        inlineNav : {
            add : false
        },
        editurl : WEB_ROOT + "/myt/es/stock/sale-delivery!doSave",
        fullediturl : WEB_ROOT + "/myt/es/stock/sale-delivery!inputTabs",
        operations : function(itemArray) {
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 打印</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                var url = WEB_ROOT + "/rpt/jasper-report!preview?format=XLS&report=PARTNER_SALE_DELIVERY_COMMODITY_LIST&contentDisposition=attachment";
                var rowDatas = $grid.jqGrid("getSelectedRowdatas");
                for (i = 0; i < rowDatas.length; i++) {
                    var rowData = rowDatas[i];
                    url += "&reportParameters['SALE_DELIVERY_IDS']=" + rowData['voucher'];
                }
                window.open(url, "_blank");
            });
            itemArray.push($select);
        },

        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/es/stock/sale-delivery!saleDeliveryDetails?id=" + row_id,
                colModel : [ {
                    label : '商品编码',
                    name : 'commodity.sku',
                    align : 'center',
                    width : 100
                }, {
                    label : '商品名称',
                    name : 'commodity.title',
                    align : 'left',
                    width : 300
                }, {
                    label : '规格型号',
                    name : 'commodity.spec',
                    width : 100
                }, {
                    label : '批号',
                    name : 'batchNo',
                    width : 100
                }, {
                    label : '到期日',
                    sorttype : 'date',
                    name : 'expireDate',
                    width : 100
                }, {
                    label : '单位',
                    name : 'measureUnit',
                    width : 60
                }, {
                    label : '数量',
                    name : 'quantity',
                    width : 50,
                    align : 'center',
                }, {
                    label : '发货仓库',
                    name : 'storageLocation.id',
                    width : 80,
                    stype : 'select',
                    formatter : 'select',
                    searchoptions : {
                        value : Biz.getStockDatas()
                    }
                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
});
