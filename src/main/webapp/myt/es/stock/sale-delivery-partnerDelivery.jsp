<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form
	class="form-horizontal form-bordered form-label-stripped form-validation form-myt-es-stock-sale-delivery-partnerDelivery"
	action="${base}/myt/es/stock/sale-delivery!deliverySave?prepare=true" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<button class="btn blue" type="submit">
				<i class="fa fa-check"></i> 保存
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="row">
			<div class="col-md-6" id="logistics">
				<div class="form-group">
					<label class="control-label">快递公司</label>
					<div class="controls">
						<s:select name="logistics.id" list="logisticsNameMap" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">快递单号</label>
					<div class="controls">
						<s:textfield name="logisticsNo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">快递费用</label>
					<div class="controls">
						<s:textfield name="logisticsAmount" />
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
