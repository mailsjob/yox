<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">销售出库单</a></li>
		<li><a class="tab-default" data-toggle="tab" href="#tab-auto">移库出库单</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-myt-partner-partner-sale-delivery" action="#">
						<div class="form-group">
							<input type="text"
								name="search['CN_voucher_OR_receivePerson_OR_mobilePhone_OR_logisticsNo_OR_logistics.code_OR_logistics.abbr']"
								class="form-control input-large" placeholder="凭证号、收货人、电话、物流单号、物流公司...">
						</div>
						<div class="form-group">
							<input type="text" name="search['BT_createdDate']" class="form-control input-medium input-daterangepicker"
								placeholder="销售单凭证日期">
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-partner-partner-sale-delivery"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/es/stock/sale-delivery-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
