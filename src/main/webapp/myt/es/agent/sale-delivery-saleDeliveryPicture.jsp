<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-floor"
	action="${base}/myt/es/agent/sale-delivery!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload="">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">图片 </label>
					<div class="controls">
						<s:iterator var="item" status="status" value="saleDeliveryPics">
							<s:hidden name="%{'saleDeliveryPics['+#status.index+'].picUrl'}" data-multiimage="edit" data-pk="%{#item.id}"
								data-index-val="%{#item.picIndex}" data-index-prop="picIndex" />
						</s:iterator>
						<s:hidden name="saleDeliveryPics[X].picUrl" data-multiimage="btn" data-index-prop="picIndex" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload="">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>