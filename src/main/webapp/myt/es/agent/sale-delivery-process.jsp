<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<s:if test="%{deliveryTime!=null}">
<div class="row">
	<div class="col-md-6">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i> 提示信息
				</div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				
					<p>此销售单已发货</p>
			</div>
		</div>
	</div>
</div>
</s:if>
<s:else>
<div class="row">
	<div class="col-md-6">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet box yellow">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i> 确认接单
				</div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<form class="form-horizontal form-bordered form-label-stripped form-validation"
					action="${base}/myt/es/agent/sale-delivery!doAccept" method="post" data-editrulesurl="false">
					<s:hidden name="id" />
					<s:hidden name="version" />
					<p>当前销售单已指派到代理/经销商，如果确认没有问题请点击“确认接单”：</p>
					<div class="form-actions form-inline">
						<button class="btn btn-primary" type="submit">确认接单</button>
						<button class="btn default btn-cancel" type="button">取消</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet box red">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i> 下级指派
				</div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<form
					class="form-horizontal form-bordered form-label-stripped form-validation form-myt-es-agent-sale-delivery-process"
					action="${base}/myt/es/agent/sale-delivery!doAssign" method="post" data-editrulesurl="false" data-pk="<s:property value='#parameters.agentPartnerId'/>">
					<s:hidden name="id" /> 
					<s:hidden name="version" />
					<div class="form-group">
						<label class="control-label">代理分销商</label>
						<div class="controls">
							<s:textfield name="agentPartner.display" />
							<s:hidden name="agentPartner.id" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">指派生效时间</label>
						<div class="controls">
							<s3:datetextfield name="agentAssignDate" current="true" data-timepicker="true" format="timestamp" value="" />
						</div>
					</div>
					<div class="form-actions form-inline">
						<button class="btn btn-primary" type="submit" >确认指派</button>
						<button class="btn default btn-cancel" type="button">取消</button>
					</div>
				</form>
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>
<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet box grey">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i> 退回上级
				</div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<form class="form-horizontal form-bordered form-label-stripped form-validation"
					action="${base}/myt/es/agent/sale-delivery!doReject" method="post" data-editrulesurl="false">
					<s:hidden name="id" />
					<s:hidden name="version" />
					<p>当前销售单已指派到代理/经销商，如果您觉得当前及所辖下级都无法处理本单，请退回上级：</p>
					<p>注意：此处指的上级是指您当前所在级别的上级，不是指当前销售单所在层级的上级。</p>
					<div class="form-group">
						<label class="control-label">退回说明</label>
						<div class="controls">
							<s:textarea name="agentRejectExplain" requiredLabel="true" rows="4" />
						</div>
					</div>
					<div class="form-actions form-inline">
						<button class="btn btn-primary" type="submit">退回上级</button>
						<button class="btn default btn-cancel" type="button">取消</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</s:else>
<script src="${base}/myt/es/agent/sale-delivery-process.js" />
<%@ include file="/common/ajax-footer.jsp"%>