<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation"
	action="${base}/myt/es/agent/sale-return-apply!doReceive" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 确认收货
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body control-label-sm">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">退货联系人</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="sellerName" />
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">退货电话</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="sellerPhone" />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">退货地址</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="sellerDetailAddr" />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">退货地址</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="sellerDetailAddr" />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
				<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">快递单号</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="logisticsNo" />
						</p>
					</div>
				</div>
				</div>
				<div class="col-md-8" >
					<div class="form-group">
						<label class="control-label">快递公司</label>
						<div class="controls">
						<p class="form-control-static">
							<s:property value="logistics.abbr" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">备注</label>
					<div class="controls">
						<s:textarea name="agentReceiveMemo" rows="3"></s:textarea>
					</div>
				</div>
			</div>
		</div>
		</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 确认收货
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
