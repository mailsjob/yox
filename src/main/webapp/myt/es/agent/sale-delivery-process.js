$(function() {
    $(".form-myt-es-agent-sale-delivery-process").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);

            $form.find('input[name="agentPartner.display"]').treeselect({
                url : WEB_ROOT + "/myt/partner/partner!treeDatas",
                callback : {
                    onSingleClick : function(event, treeId, treeNode) {
                        $form.setFormDatas({
                            'agentPartner.id' : Util.startWith(treeNode.id, "-") ? "" : treeNode.id,
                            'agentPartner.display' : Util.startWith(treeNode.id, "-") ? "" : treeNode.name
                        }, true);
                    },
                    onClear : function(event) {
                        $form.setFormDatas({
                            'agentPartner.id' : '',
                            'agentPartner.display' : ''
                        }, true);
                    }
                }
            });
        },
        preValidate : function() {
            var $form = $(this);
            var selAgentPartnerId = $form.find("input[name='agentPartner.id']").val();
            var agentPartnerId=$form.attr("data-pk");
            if(selAgentPartnerId==agentPartnerId){
                bootbox.alert("销售单已经指派给了此分销商，请重新选择");
                return false;
            }
           
        }
    });
});
