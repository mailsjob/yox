$(function() {
    $(".grid-myt-es-agent-sale-delivery-sumByCustomerProfile").data("gridOptions", {
        url : WEB_ROOT + '/myt/es/agent/sale-delivery!findByGroupCustomerOrder',
        colModel : [ {
            label : '客户ID',
            name : 'customerProfile.id',
            align : 'center',
            hidden : true,
            width : 60
        }, {
            label : '客户昵称',
            name : 'customerProfile.nickName',
            align : 'center',
            width : 120
        }, {
            label : '真实姓名',
            name : 'customerProfile.trueName',
            align : 'center',
            width : 80
        }, {
            label : '消费总金额',
            name : 'sum(totalAmount)',
            formatter : 'currency',
            width : 80
        }, {
            label : '联系电话',
            name : 'customerProfile.mobilePhone',
            align : 'center',
            width : 100
        }, {
            label : '详细地址',
            name : 'customerProfile.deliveryAddr',
            width : 250
        }, {
            label : '订单数',
            name : 'count(customerProfile.id)',
            formatter : 'integer',
            width : 60
        } ],
        sortname : 'count(customerProfile.id)',
        sortorder : "desc",
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            var $grid = $(this);
            var rowdata = $grid.getRowData(row_id);
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/es/agent/sale-delivery!findByPage",
                postData : {
                    "search['EQ_customerProfile.id']" : rowdata["customerProfile.id"],
                },
                colModel : [ {
                    label : '凭证号',
                    name : 'voucher',
                    align : 'center',
                    width : 100
                }, {
                    label : '经办人',
                    name : 'voucherUser.display',
                    hidden : true,
                    index : 'voucherUser.signinid',
                    align : 'center',
                    width : 60
                }, {
                    label : '发货人模板',
                    name : 'expressDataTemplate.id',
                    hidden : true,
                    align : 'center',
                    stype : 'select',
                    editoptions : {
                        optionsurl : WEB_ROOT + "/myt/es/agent/express-data-template!list",
                    },
                    width : 120
                }, {
                    label : '发货类型',
                    stype : 'select',
                    name : 'deliveryType',
                    hidden : true,
                    align : 'center',
                    searchoptions : {
                        value : Util.getCacheEnumsByType('deliveryTypeEnum')
                    },
                    cellattr : function(rowId, cellValue, rawObject, cm, rowdata) {
                        if (rowdata['deliveryType'] == 'READY') {
                            return "class='badge-warning'"
                        }
                    },
                    width : 60
                }, {
                    label : '分配时间',
                    name : 'agentAssignDate',
                    formatter : 'timestamp'
                }, {
                    label : '接单时间',
                    name : 'agentAcceptDate',
                    cellattr : function(rowId, cellValue, rowdata, cm) {
                        if (rowdata['agentAcceptDate']) {
                            return "class='badge-success'"
                        } else {
                            var hours = rowdata['agentAssignDatePassDuration'];
                            if (hours > 24) {
                                return "class='badge-danger'"
                            } else if (hours > 12) {
                                return "class='badge-warning'"
                            } else if (hours > 4) {
                                return "class='badge-info'"
                            }
                        }
                    },
                    width : 140,
                    formatter : function(cellValue, options, rowdata, action) {
                        if (rowdata['agentAcceptDate']) {
                            return rowdata['agentAcceptDate'];
                        } else {
                            var hours = rowdata['agentAssignDatePassDuration'];
                            return "已等待时间：" + hours + " 小时";
                        }
                    }
                }, {
                    label : '分配代理商',
                    name : 'agentPartner.display',
                    index : 'agentPartner.code_OR_agentPartner.abbr',
                    width : 130
                }, {
                    label : '最近操作',
                    name : 'lastOperationSummary',
                    hidden : true,
                    width : 120
                }, {
                    label : '凭证日期',
                    name : 'voucherDate',
                    hidden : true,
                    stype : 'date',
                    width : 100
                }, {
                    label : '拣货人',
                    name : 'pickUser.display',
                    hidden : true,
                    width : 100
                }, {
                    label : '拣货时间',
                    name : 'pickTime',
                    hidden : true,
                    width : 100
                }, {
                    label : '收取客户运费',
                    name : 'chargeLogisticsAmount',
                    editable : true,
                    hidden : true,
                    width : 100
                }, {
                    label : '整单金额',
                    name : 'totalAmount',
                    formatter : 'currency',
                    width : 100
                }, {
                    label : '已付金额',
                    name : 'payedAmount',
                    formatter : 'currency',
                    hidden : true,
                    width : 100
                }, {
                    label : '客户',
                    name : 'customerProfile.display',
                    index : 'customerProfile.nickName_OR_customerProfile.trueName',
                    formatter : 'showlink',
                    formatoptions : {
                        title : '查看客户信息',
                        idValue : 'customerProfile.id',
                        baseLinkUrl : WEB_ROOT + '/myt/customer/customer-profile!view'
                    },
                    width : 100
                }, {
                    label : '收货人',
                    name : 'receivePerson',
                    width : 100
                }, {
                    label : '物流公司',
                    name : 'logistics.display',
                    hidden : true,
                    width : 100
                }, {
                    label : '快递单号',
                    name : 'logisticsNo',
                    width : 100
                }, {
                    label : '电话',
                    name : 'mobilePhone',
                    width : 100
                }, {
                    label : '收货地址',
                    name : 'deliveryAddr',
                    width : 250
                }, {
                    label : '实际运费',
                    name : 'logisticsAmount',
                    hidden : true,
                    width : 100
                } ],
                editcol : 'voucher',
                fullediturl : WEB_ROOT + "/myt/es/agent/sale-delivery!inputTabs",
                addable : false,
                operations : function(itemArray) {
                    var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 发货清单</a></li>');
                    $select.children("a").bind("click", function(e) {
                        e.preventDefault();
                        var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                        var url = WEB_ROOT + "/rpt/jasper-report!preview?format=XLS&report=SALE_DELIVERY_COMMODITY_LIST&contentDisposition=attachment";
                        var rowDatas = $grid.jqGrid("getSelectedRowdatas");
                        for (i = 0; i < rowDatas.length; i++) {
                            var rowData = rowDatas[i];
                            url += "&reportParameters['SALE_DELIVERY_IDS']=" + rowData['voucher'];
                        }
                        window.open(url, "_blank");
                    });
                    itemArray.push($select);
                    var $select2 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 全峰</a></li>');
                    $select2.children("a").bind("click", function(e) {
                        e.preventDefault();
                        var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                        var url = WEB_ROOT + "/rpt/jasper-report!preview?format=XLS&report=EXPRESS_QUANFENG&contentDisposition=attachment";
                        var rowDatas = $grid.jqGrid("getSelectedRowdatas");
                        for (i = 0; i < rowDatas.length; i++) {
                            var rowData = rowDatas[i];
                            url += "&reportParameters['SALE_DELIVERY_IDS']=" + rowData['voucher'];
                        }
                        window.open(url, "_blank");
                    });
                    itemArray.push($select2);
                    var $select3 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 申通</a></li>');
                    $select3.children("a").bind("click", function(e) {
                        e.preventDefault();
                        var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                        var url = WEB_ROOT + "/rpt/jasper-report!generate?format=XLS&report=EXPRESS_SHENTONG&contentDisposition=attachment";
                        var rowDatas = $grid.jqGrid("getSelectedRowdatas");
                        for (i = 0; i < rowDatas.length; i++) {
                            var rowData = rowDatas[i];
                            url += "&reportParameters['SALE_DELIVERY_IDS']=" + rowData['voucher'];

                        }
                        window.open(url, "_blank");
                    });
                    itemArray.push($select3);

                },
                subGrid : true,
                subGridRowExpanded : function(subgrid_id, row_id) {
                    Grid.initSubGrid(subgrid_id, row_id, {
                        url : WEB_ROOT + "/myt/es/agent/sale-delivery!saleDeliveryDetails?id=" + row_id,
                        colModel : [ {
                            label : '行项号',
                            name : 'subVoucher',
                            align : 'center',
                            width : 50
                        }, {
                            label : '销售（发货）商品',
                            name : 'commodity.display',
                            align : 'left'
                        }, {
                            label : '单位',
                            name : 'measureUnit',
                            editable : true,
                            width : 60
                        }, {
                            label : '数量',
                            name : 'quantity',
                            width : 50,
                            formatter : 'number'
                        }, {
                            label : '销售单价',
                            name : 'price',
                            width : 60,
                            formatter : 'currency'

                        }, {
                            label : '是否赠品',
                            name : 'gift',
                            width : 50,
                            edittype : 'checkbox'

                        }, {
                            label : '原价金额',
                            name : 'originalAmount',
                            width : 60,
                            formatter : 'currency'

                        }, {
                            label : '折扣率(%)',
                            name : 'discountRate',
                            width : 50,
                            formatter : 'number'
                        }, {
                            label : '折扣额',
                            name : 'discountAmount',
                            width : 60,
                            formatter : 'currency'
                        }, {
                            label : '折后金额',
                            name : 'amount',
                            width : 60,
                            formatter : 'currency'
                        }, {
                            label : '税率(%)',
                            name : 'taxRate',
                            width : 50,
                            hidden : true,
                            formatter : 'number'

                        }, {
                            label : '税额',
                            name : 'taxAmount',
                            width : 60,
                            hidden : true,
                            formatter : 'currency'
                        }, {
                            label : '含税总金额',
                            name : 'commodityAndTaxAmount',
                            width : 80,
                            formatter : 'currency'
                        }, {
                            label : '发货仓库',
                            name : 'storageLocation.id',
                            width : 80,
                            stype : 'select',
                            formatter : 'select',
                            searchoptions : {
                                value : Biz.getStockDatas()
                            }
                        } ],
                        loadonce : true,
                        multiselect : false
                    });
                }
            });
        }
    });
});
