<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">列表查询</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-myt-es-agent-distribute-delivery" action="#">
						<div class="form-group">
							<input type="text" name="search['CN_voucher_OR_logisticsNo']" class="form-control input-large"
								placeholder="配货单号、物流单号...">
						</div>
						<div class="form-group">
								<div class="btn-group">
									<b>隐藏‘已收货’的数据 <b><s:checkbox name="search['EQ_distributeDeliveryStatus']" 
									fieldValue="S40DELIVERIED" value="true"/>
								</div>
						</div> 
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-es-agent-distribute-delivery"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/es/agent/distribute-delivery-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
