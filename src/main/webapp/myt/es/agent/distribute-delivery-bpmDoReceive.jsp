<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation"
      action="${base}/myt/es/agent/distribute-delivery!doReceive" method="post">
      <s:hidden name="taskId" value="%{#parameters.taskId}" />
      <s:hidden name="id" />
      <s:hidden name="version" />
      <s:token />
      <div class="form-actions right">
            <button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
                  <i class="fa fa-check"></i> 确认收货
            </button>
            <button class="btn default btn-cancel" type="button">取消</button>
      </div>
</form>
<div class="distribute-delivery-content ajaxify"
      data-url="${base}/myt/es/agent/distribute-delivery!view?id=<s:property value='#parameters.id'/>"></div>
<%@ include file="/common/ajax-footer.jsp"%>