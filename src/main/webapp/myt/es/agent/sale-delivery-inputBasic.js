$(function() {
    $(".form-myt-es-agent-sale-delivery-inputBasic").data("formOptions", {
       bindEvents : function() {
            var $form = $(this);
            var $grid = $form.find(".grid-myt-es-agent-sale-delivery-inputBasic");
            //打印快递单
            $form.find(".btn_express_print_QF").click(function() {
                if (confirm("确认调用票据打印程序打印当前快递单吗?")) {
                    var url = WEB_ROOT + "/rpt/jasper-report!generate?format=XLS&report=EXPRESS_QUANFENG&contentDisposition=attachment";
                    url += "&reportParameters['SALE_DELIVERY_IDS']=" + $form.find("input[name='voucher']").val();
                    window.open(url, "_blank");
                }
            });
            $form.find(".btn_express_print_ST").click(function() {
                if (confirm("确认调用票据打印程序打印当前快递单吗?")) {
                    var url = WEB_ROOT + "/rpt/jasper-report!generate?format=XLS&report=EXPRESS_SHENTONG&contentDisposition=attachment";
                    url += "&reportParameters['SALE_DELIVERY_IDS']=" + $form.find("input[name='voucher']").val();
                    window.open(url, "_blank");
                }
            });
            //打印发货单
            $form.find(".btn_delivery_print").click(function() {
                if (confirm("确认打印发货清单?")) {
                    var url = WEB_ROOT + "/rpt/jasper-report!generate?format=XLS&report=SALE_DELIVERY_COMMODITY_LIST&contentDisposition=attachment";
                    url += "&reportParameters['SALE_DELIVERY_IDS']=" + $form.find("input[name='voucher']").val();
                    window.open(url, "_blank");
                }
            });
        },
        
    });

    $(".grid-myt-es-agent-sale-delivery-inputBasic").data("gridOptions", {
       url : function() {
            var pk = $(this).attr("data-pk");
            if (pk) {
                return WEB_ROOT + "/myt/es/agent/sale-delivery!saleDeliveryDetails?id=" + pk + "&clone=" + $(this).attr("data-clone");
            }
        },
        colModel : [ {
            label : '行项号',
            name : 'subVoucher',
            hidden:true,
            width : 50
        }, {
            label : '销售（发货）商品',
            name : 'commodity.display',
            width : 200
        }, {
            label : '单位',
            name : 'measureUnit',
            width : 80
        }, {
            label : '数量',
            name : 'quantity',
            width : 80
        }, {
            label : '销售单价',
            name : 'price',
            width : 80,
            formatter : 'currency',
            summaryType : 'sum'
        }, {
            label : '是否赠品',
            name : 'gift',
            width : 80,
            edittype : 'checkbox',
            editable : true,
            align : 'center',
            responsive : 'sm'
        }, {
            label : '商品金额',
            name : 'originalAmount',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : '折扣率(%)',
            name : 'discountRate',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : '折扣额',
            name : 'discountAmount',
            width : 80,
            align : 'right',
            responsive : 'sm'
        }, {
            label : '折后金额',
            name : 'amount',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : '税率(%)',
            name : 'taxRate',
            width : 80,
            formatter : 'number',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '税额',
            name : 'taxAmount',
            width : 80,
            formatter : 'currency',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '含税总金额',
            name : 'commodityAndTaxAmount',
            width : 80,
            formatter : 'currency',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '发货仓库',
            name : 'storageLocation.id',
            width : 150,
            editable : true,
            hidden:true,
            editrules : {
                required : true
            },
            stype : 'select',
            searchoptions : {
                value : Biz.getStockDatas()
            }
        } ],
    });

});
