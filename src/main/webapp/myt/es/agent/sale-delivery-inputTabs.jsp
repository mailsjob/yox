<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-custom tabbable-secondary">
	<ul class="nav nav-tabs">
		<li class="tools pull-right"><a href="javascript:;" class="btn default reload"><i class="fa fa-refresh"></i></a></li>
		<li class="active"><a data-toggle="tab"
			href="${base}/myt/es/agent/sale-delivery!edit?id=<s:property value='#parameters.id'/>&clone=<s:property value='#parameters.clone'/>">基本信息</a>
		<li><a data-toggle="tab"
			href="${base}/myt/es/agent/sale-delivery!forward?_to_=process&id=<s:property value='#parameters.id'/>&agentPartnerId=<s:property value='%{model.agentPartner.id}'/>">接单处理</a>
		<li><a data-toggle="tab"
			href="${base}/myt/es/agent/sale-delivery!forward?_to_=delivery&id=<s:property value='#parameters.id'/>">发货管理</a>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/es/agent/sale-delivery!forward?_to_=saleDeliveryPicture&id=<s:property value='#parameters.id'/>">发货图片管理</a></li>
	</ul>
</div>
<%@ include file="/common/ajax-footer.jsp"%>
