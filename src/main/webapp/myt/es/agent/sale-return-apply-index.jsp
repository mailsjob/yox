<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">列表查询</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">

			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-myt-es-agent-sale-return-apply"
						action="#">
						<div class="form-group">
							<input type="text" name="search['CN_boxOrderDetailCommodity.boxOrder.orderSeq_OR_boxOrderDetailCommodity.commodity.sku_OR_boxOrderDetailCommodity.commodity.title_OR_logisticsNo']" class="form-control input-xlarge"
								placeholder="订单号、退货商品、退货物流单号...">
						</div>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-es-agent-sale-return-apply"  data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/es/agent/sale-return-apply-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
