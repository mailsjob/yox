$(function() {
    $(".grid-myt-es-agent-distribute-delivery").data("gridOptions", {
        url : WEB_ROOT + '/myt/es/agent/distribute-delivery!findByPage',
        colModel : [ {
            label : '状态',
            name : 'distributeDeliveryStatus',
            align : 'center',
            stype : 'select',
            searchoptions : {
                //value : Util.getCacheEnumsByType('distributeDeliveryStatusEnum')
                value : {'':'','S40DELIVERIED':'已发货','S50RECEIVED':'已收货'}
            },
            width : 80
        }, {
            label : '配货单号',
            name : 'voucher',
            align : 'center',
            width : 120
        }, {
            label : '摘要',
            name : 'title',
            width : 200
        }, {
            label : '审核日期',
            hidden : true,
            name : 'auditDate',
            formatter : 'timestamp'
        }, {
            label : '发货日期',
            name : 'deliveryTime',
            formatter : 'date'
        }, {
            label : '经办人',
            name : 'voucherUser.display',
            hidden:true,
            index : 'voucherUser.signinid',
            align : 'center',
            width : 60
        }, {
            label : '发货类型',
            hidden : true,
            stype : 'select',
            name : 'deliveryType',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('deliveryTypeEnum')
            },
            width : 60
        }, {
            label : '商品含税总金额',
            name : 'commodityAndTaxAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '运费',
            name : 'chargeLogisticsAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '整单金额',
            name : 'totalAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '物流公司',
            name : 'logistics.display',
            index : 'logistics.abbr',
            width : 100
        }, {
            label : '物流单号',
            name : 'logisticsNo',
            width : 110
        }, {
            label : '收货人',
            name : 'receivePerson',
            align : 'center',
            width : 100
        }, {
            label : '收货地址',
            name : 'deliveryAddr',
            width : 280
        } ],
        editcol : 'voucher',
        multiselect : false,
        addable : false,
        subGrid : true,
        postData : {
            "search['IN_distributeDeliveryStatus']" : "S40DELIVERIED,S50RECEIVED"
        },
        footerrow : true,
        footerLocalDataColumn : ['commodityAndTaxAmount', 'totalAmount'],
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/es/agent/distribute-delivery!details?id=" + row_id,
                colModel : [{
                    label : '商品',
                    name : 'commodity.display',
                    align : 'left'
                }, {
                    label : '单位',
                    name : 'measureUnit',
                    editable : true,
                    width : 60
                }, {
                    label : '数量',
                    name : 'quantity',
                    align:'center',
                    width : 50
                }, {
                    label : '单价',
                    name : 'price',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '商品金额',
                    name : 'originalAmount',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '折扣率(%)',
                    name : 'discountRate',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '折扣额',
                    name : 'discountAmount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '折后金额',
                    name : 'amount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '税率(%)',
                    name : 'taxRate',
                    width : 50,
                    formatter : 'number'

                }, {
                    label : '税额',
                    name : 'taxAmount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '含税总金额',
                    name : 'commodityAndTaxAmount',
                    width : 80,
                    formatter : 'currency'
                }],
                loadonce : true,
                multiselect : false
            });
        },
    });
});
