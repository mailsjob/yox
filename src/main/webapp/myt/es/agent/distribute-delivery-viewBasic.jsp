<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="form-horizontal form-bordered form-label-stripped">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">物流单号</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="logisticsNo" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">物流公司</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="logistics.display" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">配货单号</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="voucher" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">所属代理商</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="agentPartner.display" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">标题</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="title" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">备注说明</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="memo" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-myt-es-agent-distribute-delivery-viewBasic" data-grid="table" data-pk="<s:property value='id'/>" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="note note-success">
				<h4 class="block">
					订单总金额：
					<s:property value="totalAmount" />
					(商品总金额：
					<s:property value="commodityAmount" />
					+税额：
					<s:property value="totalTaxAmount" />
					+收取运费：
					<s:property value="chargeLogisticsAmount" />
					) ，已预付金额：
					<s:property value="payedAmount" />
				</h4>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/es/agent/distribute-delivery-viewBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
