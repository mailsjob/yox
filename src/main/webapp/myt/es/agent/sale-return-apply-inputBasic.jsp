<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-sale-return-inputBasic"
	action="${base}/myt/es/agent/sale-return-apply!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<h4>
					<i class="fa fa-reorder"></i>基本信息
				</h4>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">商品</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="boxOrderDetailCommodity.commodity.display" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label">申请数量</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="quantity" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label">服务类型</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.processTypeEnum[processType]" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">有无发票</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.booleanLabel[hasInvoice]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">有无检测报告</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.booleanLabel[hasReport]" />
								</p>
							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">问题描述</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="description" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">图片</label>
							<div class="controls">
								<div style="float: left; margin-right: 5px;">
									<img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic1' />" />
								</div>
								<div style="float: left; margin-right: 5px;">
									<img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic2' />" />
								</div>
								<div style="float: left; margin-right: 5px;">
									<img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic3' />" />
								</div>
								<div style="float: left; margin-right: 5px;">
									<img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic4' />" />
								</div>
								<div style="float: left; margin-right: 5px;">
									<img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic5' />" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">收货人名</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="saleReturnName" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">收货人电话</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="saleReturnPhone" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">收货人所在地</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="saleReturnCity.parent.name" />
									&nbsp;&nbsp;
									<s:property value="saleReturnCity.name" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">收货详细地址</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="saleReturnDetailAddr" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">处理结果</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.processResultEnum[processResult]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">处理结果类型</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.processTypeEnum[saleReturnType]" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">处理结果说明</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="processComment" />
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</form>
<%@ include file="/common/ajax-footer.jsp"%>
