<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation"
	action="${base}/myt/es/agent/sale-delivery!doAgentDelivery" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		
		<s3:button disabled="%{disallowAgentDelivery!=null}" type="submit" cssClass="btn blue"
			data-grid-reload=".grid-myt-sale-sale-delivery">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowAgentDelivery!=null?disallowAgentDelivery:'保存'}" />
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body control-label-sm">
		<div class="row">
			<div class="col-md-6" id="logistics">
				<div class="form-group">
					<label class="control-label">快递公司</label>
					<div class="controls">
						<s:select name="logistics.id" list="logisticsNameMap" disabled="%{deliveryTime!=null}"
							placeholder="一旦提交后将不可修改，请仔细选择" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">快递单号</label>
					<div class="controls">
						<s:textfield name="logisticsNo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">快递费用</label>
					<div class="controls">
						<s:textfield name="logisticsAmount" disabled="%{deliveryTime!=null}" placeholder="一旦提交后将不可修改，请仔细填写" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6" id="logistics">
				<div class="form-group">
					<label class="control-label">发货时间</label>
					<div class="controls">
						<s3:datetextfield name="deliveryTime" current="true" format="timestamp" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<s3:button disabled="%{disallowAgentDelivery!=null}" type="submit" cssClass="btn blue"
			data-grid-reload=".grid-myt-sale-sale-delivery">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowAgentDelivery!=null?disallowAgentDelivery:'保存'}" />
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
