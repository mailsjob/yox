$(function() {
    $(".grid-myt-es-agent-sale-return-apply").data("gridOptions", {
        url : WEB_ROOT + '/myt/es/agent/sale-return-apply!findByPage',
        colModel : [ {
            label : '订单号',
            name : 'boxOrderDetailCommodity.boxOrder.orderSeq',
            hidden:true,
            align : 'center',
            width : 180
        }, {
            label : '订单状态',
            name : 'boxOrderDetailCommodity.boxOrder.orderStatus',
            hidden:true,
            align : 'center',
            stype:'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('boxOrderStatusEnum')
            },
            width : 100
        }, {
            label : '商品名称',
            name : 'boxOrderDetailCommodity.commodity.display',
            index : 'boxOrderDetailCommodity.commodity.sku_OR_boxOrderDetailCommodity.commodity.title',
            width : 200
        }, {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName_OR_customerProfile.trueName',
            hidden:true,
            width : 100
        }, {
            label : '单据类型',
            name : 'processType',
            align : 'center',
            stype:'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('processTypeEnum')
            },
            width : 100
        }, {
            label : '数量',
            name : 'quantity',
            align:'center',
            width : 50
        }, {
            label : '有无发票',
            name : 'hasInvoice',
            align:'center',
            stype:'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('booleanLabel')
            },
            width : 100
        }, {
            label : '有无检测报告',
            name : 'hasReport',
            align:'center',
            stype:'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('booleanLabel')
            },
            width : 100
        }, {
            label : '处理结果',
            name : 'processResult',
            stype:'select',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('postAuditResultEnum')
            },
            width : 100
        }, {
            label : '处理结果类型',
            name : 'saleReturnType',
            align : 'center',
            stype:'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('processTypeEnum')
            },
            width : 100
        }, {
            label : '客户寄回的物流公司',
            name : 'logistics.id',
            align : 'center',
            stype : 'select',
            editoptions : {

                value : Biz.getLogisticsDatas()
            },
            width : 200
        }, {
            label : '客户寄回的快递单号',
            name : 'logisticsNo',
            width : 150
        }],
        addable:false,
        editcol : 'boxOrderDetailCommodity.commodity.display',
        fullediturl : WEB_ROOT + "/myt/es/agent/sale-return-apply!inputTabs"
   });
});
