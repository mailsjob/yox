$(function() {
    $(".form-sale-delivery-delivery").data("formOptions", {
        submitHandler : function(form) {
            var $form = $(form);
            var $voucher = $form.find("input[name='voucher']");
            var voucher = $voucher.val();
            if (voucher != '') {
                var url = WEB_ROOT + "/myt/sale/sale-delivery!deliveryDetails?voucher=" + voucher;
                $(".div-sale-delivery-delivery-details").ajaxGetUrl(url)
            }
            $voucher.focus();
            $voucher.select();
        },
        bindEvents : function() {
            var $form = $(this);
            //扫描枪输入处理
            $form.find("input[name='voucher']").barcodeScanSupport({
                onEnter : function() {
                    $form.submit();
                }
            });
        }
    });
});