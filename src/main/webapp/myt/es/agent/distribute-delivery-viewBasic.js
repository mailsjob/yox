$(function() {
    $(".grid-myt-es-agent-distribute-delivery-viewBasic").data("gridOptions", {
        url : function() {
            var pk = $(this).attr("data-pk");
            if (pk) {
                return WEB_ROOT + "/myt/es/agent/distribute-delivery!details?id=" + pk;
            }
        },
        colModel : [ {
            label : '商品',
            name : 'commodity.display',
            width : 200,
            align : 'left'
        }, {
            label : '单位',
            name : 'measureUnit',
            width : 60
        }, {
            label : '数量',
            name : 'quantity',
            width : 50
        }, {
            label : '单价',
            name : 'price',
            width : 60,
            formatter : 'currency'
        }, {
            label : '商品金额',
            name : 'originalAmount',
            width : 60,
            formatter : 'currency'
        }, {
            label : '折扣率(%)',
            name : 'discountRate',
            width : 50,
            hidden : true,
            formatter : 'number'
        }, {
            label : '折扣额',
            name : 'discountAmount',
            width : 60,
            formatter : 'currency'
        }, {
            label : '折后金额',
            name : 'amount',
            width : 60,
            formatter : 'currency'
        }, {
            label : '税率(%)',
            name : 'taxRate',
            width : 50,
            formatter : 'number'
        }, {
            label : '税额',
            name : 'taxAmount',
            width : 60,
            formatter : 'currency'
        }, {
            label : '含税总金额',
            name : 'commodityAndTaxAmount',
            width : 80,
            formatter : 'currency'
        }],
        multiselect : false,
        filterToolbar : false,
        height : 'auto',
        pager : false,
        footerrow : true,
        footerLocalDataColumn : [ 'profitAmount' ]
    });
});
