 $(".grid-md-floor-menu").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/floor-menu!findByPage',
        colModel : [{
            label : '楼层主键',
            name : 'floor.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '楼层',
            name : 'floor.displayLabel',
            index:'floor.id_OR_floor.monthText_OR_floor.siteFlag',
            width : 100,
            align : 'left'
        }, {
            label : '楼层文本',
            hidden : true,
            name : 'floor.monthText',
            width : 100,
            align : 'left'
        }, {
            label : '名称',
            name : 'name',
            editable : true,
            width : 100,
            align : 'left'
        },{
            label : '排序号',
            name : 'orderIndex',
            width : 80,
            editable : true,
            align : 'right'
        }],
        editcol : 'name',
        sortorder : "asc",
        sortname : "orderIndex",
        editurl : WEB_ROOT + "/myt/md/floor-menu!doSave",
        delurl : WEB_ROOT + "/myt/md/floor-menu!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/floor-menu!inputTabs",
    });