<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-custom tabbable-secondary">
	<ul class="nav nav-tabs">
		<li class="tools pull-right"><a href="javascript:;" class="btn default reload"><i class="fa fa-refresh"></i></a></li>
		<li class="active"><a data-toggle="tab"
			href="${base}/myt/md/gift-paper!edit?id=<s:property value='#parameters.id'/>&clone=<s:property value='#parameters.clone'/>
			&paperType=<s:property value='#parameters.paperType'/>">基本信息</a></li>
		<s:if test='%{#parameters.paperType[0]=="G"}'>	
		<li><a data-toggle="tab"  data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/gift-paper!forward?_to_=objects&id=<s:property value='#parameters.id'/>">优惠券使用范围</a></li>
		</s:if>
	</ul>
</div>
<%@ include file="/common/ajax-footer.jsp"%>