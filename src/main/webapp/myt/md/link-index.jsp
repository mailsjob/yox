<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">友情链接列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-md-link" action="#">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_name_OR_url']" class="form-control" placeholder="名称/链接">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-link"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-link").data("gridOptions", {
            url : "${base}/myt/md/link!findByPage",
            colModel : [ {
                label:'启用',
                name : 'enable',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            },{
                label : '时间',
                name : 'createdDate',
                width : 120,
                align : 'center'
            }, {
                label:'名称',
                name : 'name',
                editable : true,
                width : 150,
                align : 'left'
            }, {
                label:'链接',
                name : 'url',
                editable : true,
                width : 150,
                align : 'left'
            },{
                label:'排序号',
                name : 'orderIndex',
                width : 60,
                editable : true,
                align : 'center'
            }],
            editcol : 'name',
            editurl : "${base}/myt/md/link!doSave",
            delurl : "${base}/myt/md/link!doDelete",
            fullediturl : "${base}/myt/md/link!edit"
          });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>