<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-bind-commodity"
	action="${base}/myt/md/bind-commodity!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload="#grid-bind-commodity-list">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-4">名称</label>
					<div class="col-md-8">
						<s:textfield name="name" />
					</div>
				</div>

			</div>

		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">SKU</label>
					<div class="col-md-8">
						<s:textfield name="sku" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">来源</label>
					<div class="col-md-8">
						<s:select name="comeFrom" list="#application.enums.comeFromEnum" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">商品</label>
					<div class="col-md-10">
						<div class="input-icon right">
							<i class="fa fa-search fa-search-commodity"></i> <i class="fa fa-times fa-times-commodity input-icon-secondary"></i>
							<s:textfield name="commodity.display" readonly="true" />
							<s:hidden name="commodity.id" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-md-4">备注</label>
				<div class="col-md-8">
					<s:textarea name="memo" rows="2" />
				</div>
			</div>

		</div>

	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {
        $(".form-edit-bind-commodity").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                $form.find("i.fa-search-commodity").click(function() {
                    $(this).popupDialog({
                        url : '${base}/myt/md/commodity!forward?_to_=selection',
                        title : '选取商品',
                        callback : function(json) {
                            $form.find("[name$='commodity.display']").val(json.sku + " " + json.title);
                            $form.find("[name$='commodity.id']").val(json.id);
                        }
                    })
                });

                $form.find("i.fa-times-commodity").click(function() {
                    $form.find("[name$='commodity.display']").val("");
                    $form.find("[name$='commodity.id']").val("");
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
