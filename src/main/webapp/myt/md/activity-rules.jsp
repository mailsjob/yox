<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-myt-md-activity-rules" data-grid="table"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-activity-rules").data("gridOptions", {
            url : "${base}/myt/md/activity!activityR2Rules?id=<s:property value='#parameters.id'/>",
            colModel : [ {
                label : '规则主键',
                name : 'ruleId',
                editable : true,
                hidden : true
            }, {
                label : '规则名称',
                name : 'ruleTitle',
                align : 'left',
                editable : true,
                editoptions : {
                    dataInit : function(elem) {
                        var $grid = $(this);
                        var $elem = $(elem);
                        $elem.wrap('<div class="input-icon right"/>');
                        $elem.before('<i class="fa fa-ellipsis-horizontal fa-select-object"></i>');
                        $elem.attr("readonly", true);
                        var selectObject = function(item) {
                            $grid.jqGrid("setEditingRowdata", {
                                'ruleId' : item.id,
                                'ruleTitle' : item.ruleTitle
                            });
                        }
                        $grid.find(".fa-select-object").click(function() {
                            var rowdata = $grid.jqGrid("getEditingRowdata");
                            $(this).popupDialog({
                                url : WEB_ROOT + '/myt/activity/activity-rules!forward?_to_=selection',
                                title : '选取规则',
                                callback : selectObject
                            })
                        });
                    }
                }
            }, {
                label : '是否可用',
                name : 'isEnable',
                width : 80,
                edittype : "checkbox",
                align : 'center'
            } ],
            editurl : "${base}/myt/activity/activity-rules!doChooseRules?activity.id=<s:property value='#parameters.id'/>",
            delurl : "${base}/myt/activity/activity-rules!doDelRules"
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>