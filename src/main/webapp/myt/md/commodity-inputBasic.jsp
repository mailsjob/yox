<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<style>
    .controls-categoryTimelines label.checkbox-inline {
        width: 120px;
        margin-left: 10px;
    }

    .controls-categoryLabels label.checkbox-inline {
        width: 150px;
        margin-left: 10px;
    }
</style>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-commodity"
      action="${base}/myt/md/commodity!doSave" method="post">
    <s:hidden name="id"/>
    <s:hidden name="version"/>
    <s:token/>
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-md-commodity-normal">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
        <button class="btn blue pull-right" style="margin-left: 5px" type="submit"
                data-grid-reload=".grid-myt-sale-sale-delivery" data-form-action="${base}/myt/md/commodity!putOff"
                data-confirm="确认下架？">下架
        </button>
        <button class="btn blue pull-right" style="margin-left: 5px" type="submit"
                data-grid-reload=".grid-myt-sale-sale-delivery"
                data-form-action="${base}/myt/md/commodity!putOnAndRecFalse" data-confirm="确认上架但忽略首页推荐？">上架但忽略首页推荐
        </button>
        <button class="btn blue pull-right" style="margin-left: 5px" type="submit"
                data-grid-reload=".grid-myt-sale-sale-delivery"
                data-form-action="${base}/myt/md/commodity!putOnAndRecTrue" data-confirm="确认上架并恢复首页推荐？">上架并恢复首页推荐
        </button>
    </div>

    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">编码</label>
                    <div class="controls">
                        <s:textfield name="sku" readonly="true"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">状态</label>
                    <div class="controls">
                        <p class="form-control-static">
                            <s:property value="#application.enums.commodityStatusEnum[commodityStatus]"/>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">哎呦价</label>
                    <div class="controls">
                        <s:textfield name="price"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">实物条码</label>
                    <div class="controls">
                        <s:textfield name="barcode"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">市场价</label>
                    <div class="controls">
                        <s:textfield name="marketPrice"/>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">成本价</label>
                    <div class="controls">
                        <s:textfield name="costPrice"/>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">商品名称</label>
                    <div class="controls">
                        <s:textfield name="title"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">供应商</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-provider"></i>
                            <s:textfield name="provider.abbr" />
                            <s:hidden name="provider.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">副标题</label>
                    <div class="controls">
                        <s:textfield name="subTitle"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label"> 橱窗图 </label>
                    <div class="controls">
                        <s:iterator var="item" status="status" value="commodityPics">
                            <s:hidden name="%{'commodityPics['+#status.index+'].bigPicUrl'}" data-multiimage="edit"
                                      data-pk="%{#item.id}" data-index-val="%{#item.picIndex}"
                                      data-index-prop="picIndex"/>
                        </s:iterator>
                        <s:hidden name="commodityPics[X].bigPicUrl" data-multiimage="btn" data-index-prop="picIndex"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label"> 横幅图 </label>
                    <div class="controls">
                        <p class="form-control-static" style="padding: 20px 0 4px 25px;">
                            <s:hidden name="bannerPic" data-singleimage="true"/>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">商品类别</label>
                    <div class="controls">
                        <s:textfield name="category.display" data-toggle="treeselect"
                                     data-url="%{#attr.base+'/myt/md/category!categoryList'}"/>
                        <s:hidden name="category.id"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">品牌</label>
                    <div class="controls">
                        <s:select name="brand.id" list="brandsMap"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">搜索关键字</label>
                    <div class="controls">
                        <s:textfield name="keywords" onclick="$(this).closest('div.row').next('div.row').toggle()"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="display: none">
            <div class="col-md-12">
                <div class="form-group form-group-selectionKeyword">
                    <label class="control-label">候选关键字<input type="checkbox"/></label>
                    <div class="controls controls-categoryLabels">
                        <s:checkboxlist list="categoryLabelsMap" name="selectionKeyword"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">时间主题</label>
                    <div class="controls">
                        <s:textfield name="timelines" onclick="$(this).closest('div.row').next('div.row').toggle()"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="display: none">
            <div class="col-md-12">
                <div class="form-group form-group-selectionTimeline">
                    <label class="control-label">候选时间主题 <input type="checkbox"/></label>
                    <div class="controls controls-categoryTimelines">
                        <s:checkboxlist list="categoryTimelinesMap" name="selectionTimeline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">排序号</label>
                    <div class="controls">
                        <s:textfield name="recommendRank"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">商品类型</label>
                    <div class="controls">
                        <s:select name="commodityType" list="#application.enums.commodityTypeEnum"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">ERP商品ID</label>
                    <div class="controls">
                        <s:textfield name="erpProductId" requiredLabel="true"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">立即可购买</label>
                    <div class="controls">
                        <s:radio name="canBuyNow" list="#application.enums.booleanLabel"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">优选商品</label>
                    <div class="controls">
                        <s:radio name="preferred" list="#application.enums.booleanLabel"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">生成消费码</label>
                    <div class="controls">
                        <s:radio name="genPurchaseCodeFlag" list="#application.enums.booleanLabel"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">消费码过期规则</label>
                    <div class="controls">
                        <s:select name="purchaseCodeExpireRule" list="#application.enums.purchaseCodeExpireRuleEnum"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">过期日期</label>
                    <div class="controls">
                        <s3:datetextfield name="purchaseCodeExpiredDatetime" format="date"></s3:datetextfield>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">过期顺延天数</label>
                    <div class="controls">
                        <s:textfield name="purchaseCodeExpiredDays"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">每日可售库存</label>
                    <div class="controls">
                        <s:textfield name="canSaleCount"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">每用户单日限购量</label>
                    <div class="controls">
                        <s:textfield name="dailyUserCanBuy"/>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">总计实际库存</label>
                    <div class="controls">
                        <s:textfield name="stockTotalQuantity"/>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">总计冻结库存</label>
                    <div class="controls">
                        <s:textfield name="totalFreezedStockCount"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">当日已售数量</label>
                    <div class="controls">
                        <s:textfield name="dailySoldCount"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">是否可超卖</label>
                    <div class="controls">
                        <s:radio name="allowOversold" list="#application.enums.booleanLabel"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">缺省库存地</label>
                    <div class="controls">
                        <s:select name="defaultStorageLocation.id" list="storageLocationMap"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">库存报警阀值</label>
                    <div class="controls">
                        <s:textfield name="stockThresholdQuantity"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">商品重量（kg）</label>
                    <div class="controls">
                        <s:textfield name="weight"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">单位</label>
                    <div class="controls">
                        <s:textfield name="measureUnit"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">规格</label>
                    <div class="controls">
                        <s:textfield name="spec"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">erp规格</label>
                    <div class="controls">
                        <s:textfield name="erpSpec"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">喜爱数</label>
                    <div class="controls">
                        <s:textfield name="loveCount"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">购买人数</label>
                    <div class="controls">
                        <s:textfield name="boughtCount"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">浏览次数</label>
                    <div class="controls">
                        <s:textfield name="viewCount"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">商品温馨提示</label>
                    <div class="controls">
                        <s:textarea name="prompt" rows="3"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">比价偏移比率</label>
                    <div class="controls">
                        <s:textfield name="priceCompareRate"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">商品过滤</label>
                    <div class="controls">
                        <s:radio name="solrFilterType" list="#application.enums.solrFilterTypeEnum"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">商城首页显示</label>
                    <div class="controls">
                        <s:radio name="coinPageShow" list="#application.enums.booleanLabel"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">积分抵扣值</label>
                    <div class="controls">
                        <s:textfield name="coins"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">返还积分</label>
                    <div class="controls">
                        <s:textfield name="backCoins"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">比价链接</label>
                    <div class="controls">
                        <s:textarea name="priceCompareUrl" rows="3"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">销售地区</label>
                    <div class="controls">
                        <s:textfield name="regions" data-toggle="dropdownselect"
                                     data-url="${base}/myt/md/region!forward?_to_=proSelection"
                                     data-selected="%{regions}"/>
                        <s:hidden name="regionsId"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-md-commodity-normal">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script type="text/javascript">
    $(function () {
        $(".form-edit-commodity").data("formOptions", {
            bindEvents: function () {
                var $form = $(this);
                var clickSelection = function (source, target) {
                    var keywords = $(target).val().split(" ");
                    $(source).each(function () {
                        var keyword = $(this).val();
                        var checked = $(this).attr("checked");
                        var exists = false;
                        $.each(keywords, function (i, item) {
                            if (keyword == $.trim(item)) {
                                exists = true;
                                return false;
                            }
                        });
                        if (!checked && exists) {
                            keywords.splice(keywords.indexOf(keyword), 1);
                        }
                        if (checked && !exists) {
                            keywords.push(keyword);
                        }
                    })
                    $(target).val($.trim(keywords.join(" ")));
                }

                $form.on('click', '.form-group-selectionKeyword .controls :checkbox', function (e) {
                    clickSelection($(this), $form.find("input[name='keywords']"));
                });

                $form.on('click', '.form-group-selectionKeyword .control-label :checkbox', function (e) {
                    var chks = $form.find(".form-group-selectionKeyword .controls :checkbox");
                    chks.attr("checked", this.checked);
                    clickSelection(chks, $form.find("input[name='keywords']"));
                });

                $form.on('click', '.form-group-selectionTimeline .controls :checkbox', function (e) {
                    clickSelection($(this), $form.find("input[name='timelines']"));
                });

                $form.on('click', '.form-group-selectionTimeline .control-label :checkbox', function (e) {
                    var chks = $form.find(".form-group-selectionTimeline .controls :checkbox");
                    chks.attr("checked", this.checked);
                    clickSelection(chks, $form.find("input[name='timelines']"));
                });

                $form.find("input[name='regions']").data('data-dropdownselect-callback', function (json) {
                    $form.find("input[name='regions']").val(json.display);
                    $form.find("input[name='regionsId']").val(json.id);
                });

                $form.find(".fa-select-provider").click(function() {
                    $(this).popupDialog({
                        url : WEB_ROOT + '/myt/partner/partner!forward?_to_=selection',
                        title : '选取供应商',
                        callback : function(rowdata) {
                            $form.setFormDatas({
                                'provider.id' : rowdata.id,
                                'provider.abbr' : rowdata.abbr
                            });
                        }
                    })
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp" %>