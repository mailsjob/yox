$(".grid-md-recommend-commodity").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/recommend-commodity!findByPage',
        colModel : [{
            label : '楼层主键',
            name : 'floor.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '楼层',
            name : 'floor.displayLabel',
            index:'floor.id_OR_floor.monthText_OR_floor.siteFlag',
            width : 100,
            align : 'left'
        }, {
            label : '楼层文本',
            hidden : true,
            name : 'floor.monthText',
            width : 100,
            align : 'left'
        }, {
            label : '菜单主键',
            name : 'floorMenu.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '菜单名称',
            name : 'floorMenu.name',
            width : 100,
            align : 'left'
        },{
            label : '时间线主键',
            name : 'timeline.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '时间线',
            name : 'timeline.name',
            width : 100
        }, {
            label : '推荐类型',
            name : 'promotionType',
            width : 80,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('promotionTypeEnum')
            },
            align : 'center'
        }, {
            label : '商品主键',
            name : 'commodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '购买的商品',
            name : 'commodity.display',
            index : 'commodity.sku_OR_commodity.title',
            width : 200,
            editrules : {
                required : true
            },
            editoptions : Biz.getGridCommodityEditOptions(),
            align : 'left'
        },{
            label : '主题清单主键',
            name : 'them.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '主题清单',
            name : 'them.themeTitle',
            width : 100
        }, {
            label : '链接',
            name : 'url',
            width : 150
        }, {
            label : '促销标题',
            name : 'promotionTitle',
            width : 150,
            editable : true
        },{
            label : '图片',
            name : 'promotionPic',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        },{
            label : '排序号',
            name : 'orderRank',
            width : 80,
            editable : true,
            sorttype : 'number',
            align : 'right'
        },{
            label:'是否固定',
            name : 'fixed',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        },{
            label:'是否可用',
            name : 'enable',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        }],
        editcol : 'promotionTitle',
        sortorder : "asc",
        sortname : "orderRank",
       /* grouping : true,
        groupingView : {
            groupField : [ 'floor.displayLabel'],
            groupOrder : [ 'asc' ],
            groupCollapse : false
        },*/
        inlineNav : {
            add : false
        },
        editurl : WEB_ROOT + "/myt/md/recommend-commodity!doSave",
        delurl : WEB_ROOT + "/myt/md/recommend-commodity!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/recommend-commodity!inputTabs",
    });