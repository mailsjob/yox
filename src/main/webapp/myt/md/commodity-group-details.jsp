<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-commodity-group-details" action="${base}/myt/md/commodity-group!saveDetails" method="post">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body control-label-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-commodity-group-details">
                        <tr>
                            <th><s:property value="#request.cgDetails1D[0].propLineValX.dynaPropLine.parent.name" /></th>
                            <s:iterator value="#request.cgDetails1D" status="item">
                                <s:if test="%{#request.cgDetails1D[#item.index].propLineValX!=null}">
                                    <th><s:property value="#request.cgDetails1D[#item.index].propLineValX.dynaPropLine.name" />
                                </s:if>
                            </s:iterator>
                        </tr>
                        <tr>
                            <th><span class="require_star">*</span>商品</th>
                            <s:iterator value="#request.cgDetails1D" status="i">
                                <s:if test="%{#request.cgDetails1D[#i.index].propLineValX!=null}">
                                    <td>
                                        <div>
                                            <s:textfield name="%{'cgDetails1D['+#i.index+'].commodity.id'}" value="%{#request.cgDetails1D[#i.index].commodity.id}" />
                                            <!-- <s:hidden name="%{'cgDetails1D['+#i.index+'].commodity.id'}" value="%{#request.cgDetails1D[#i.index].commodity.id}" /> -->
                                        </div>
                                    </td>
                                </s:if>
                            </s:iterator>
                        </tr>

                    </table>
                    <!--  <s:if test="%{#request.cgDetails2D[0][0]!=null}">
              
                <s:checkbox cssClass="edit_checkbox" name="show" />
                &nbsp;&nbsp;<font color="red">显示二维属性</font>
               </s:if>
               
                <table id="twoDimensionTable" class="table table-condensed table-bordered table-edit">
                    <tr>
                        <th><s:property
                                value="#request.cgDetails2D[0][0].propLineValY.dynaPropLine.parent.name" />\<s:property
                                value="#request.cgDetails2D[0][0].propLineValX.dynaPropLine.parent.name" />
                        </th>
                        <s:iterator value="#request.cgDetails2D[0]" status="item">
                            <s:if test="%{#request.cgDetails2D[0][#item.index].propLineValX!=null}">
                                <th><s:property
                                        value="#request.cgDetails2D[0][#item.index].propLineValX.dynaPropLine.name" />
                                </td>
                            </s:if>
                        </s:iterator>
                    </tr>
                    <s:iterator value="#request.cgDetails2D" status="it">
                        <s:if test="%{#request.cgDetails2D[#it.index][0].propLineValY!=null}">
                            <tr>

                                <th ><s:property
                                        value="#request.cgDetails2D[#it.index][0].propLineValY.dynaPropLine.name" />
                                </th>
								 <s:iterator value="#request.cgDetails2D[0]" status="i">
                                    <s:if test="%{#request.cgDetails2D[0][#i.index].propLineValX!=null}">
                                        <td>
                                            <div>
                                            	<s:textfield name="cgDetails2D[#i.index].commodity.id" value="%{#request.cgDetails2D[#i.index].commodity.id}" />
												<s:hidden name="cgDetails2D['+#i.index+'].commodity.id" value="%{#request.cgDetails2D[#i.index].commodity.id}" />
                                             </div>
                                        </td>
                                  </s:if>
                                </s:iterator>
                            </tr>
                        </s:if>
                    </s:iterator>
                </table> -->
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
<script type="text/javascript">
    $(function() {
        $(".form-edit-commodity-group-details").data("formOptions", {
            bindEvents : function() {
                $("#twoDimensionTable").toggle();
                $("#show").change(function() {
                    $("#twoDimensionTable").toggle();
                });
            }
        })
    });
</script>