$(function() {
    $(".grid-md-search-key-word").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/search-key-word!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id'
        },{
            label : '关键词',
            name : 'keyword',
            align : 'left',
            width : 200,
            editable : true
        }, {
            label : '排序号',
            name : 'orderRank',
            editable : true,
            align : 'center',
            width : 80
        }, {
            label : '记录数',
            name : 'count',
            editable : true,
            width : 80,
            align : 'right'
        }, {
            label : '最后查询记录数',
            name : 'lastRecordCount',
            editable : true,
            width : 80,
            align : 'right'
        }, {
            label : '年月',
            name : 'yearMonth',
            width : 100,
            editable : true,
            align : 'center'
        }, {
            label : '状态',
            name : 'searchKeyWordType',
            editable : true,
            align : 'center',
            width : 80,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('searchTypeEnum')
            }
        }],
        editurl : WEB_ROOT + "/myt/md/search-key-word!doSave",
        delurl : WEB_ROOT + "/myt/md/search-key-word!doDelete",
     });
   
});