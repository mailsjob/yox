<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
    <ul class="nav nav-pills">
        <li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">优惠券</a></li>
        <li><a class="tab-default" data-toggle="tab" href="#tab-auto">优惠券模板</a></li>
        <li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row search-form-default">
                <div class="col-md-12">
                    <form method="get" class="form-inline form-validation form-search" data-grid-search=".grid-md-gift-paper" action="#">
                        <div class="form-group">
                            <input type="text" name="search['CN_password']" class="form-control input-small" placeholder="密码..." />
                            <input type="text" name="search['EQ_customerProfile.id']" class="form-control input-small" placeholder="客户ID..." />
                            <input type="text" name="search['CN_customerProfile.nickName_OR_customerProfile.trueName']" class="form-control input-small" placeholder="客户昵称、姓名..." />
                        </div>
                        <button class="btn default hidden-inline-xs" type="reset">
                            <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
                        </button>
                        <button class="btn green" type="submmit">
                            <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
                        </button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-md-gift-paper" data-grid="table"></table>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="row search-form-default">
                <div class="col-md-12">
                    <form method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-md-gift-paper-template" action="#">
                        <div class="form-group">
                            <input type="text" name="search['CN_paperTemplateCode_OR_code_OR_title']" class="form-control input-xlarge" placeholder="模板号、优惠券代码、优惠券名称..." />
                        </div>
                        <button class="btn default hidden-inline-xs" type="reset">
                            <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
                        </button>
                        <button class="btn green" type="submmit">
                            <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
                        </button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-md-gift-paper-template"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${base}/myt/md/gift-paper-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>