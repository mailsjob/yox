<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation" action="${base}/myt/md/activity!doSave"
	method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload="#grid-brand-list">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		 <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">代码</label>
					<div class="controls">
		                <s:textfield name="activityCode" />
					</div>
				</div>
            </div>
             <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">名称</label>
					<div class="controls">
		                <s:textfield name="activityName" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">活动图片</label>
                    <div class="col-md-8">
                        <s:hidden name="img" data-singleimage="true" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">开始时间</label>
					<div class="controls">
						<s3:datetextfield name="beginTime" format="timestamp" data-timepicker="true"></s3:datetextfield>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">结束时间</label>
					<div class="controls">
						<s3:datetextfield name="endTime" format="timestamp" data-timepicker="true"></s3:datetextfield>
						<%-- <s3:datetextfield name="endTime" format="date"></s3:datetextfield> --%>
					</div>
				</div>
			</div>
		</div>
		 <div class="row">
				<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">活动HTML</label>
					<div class="controls">
						<s:textarea name="activityHtml" data-htmleditor="kindeditor" data-height="600px" />
					</div>
				</div>
			</div>
    </div>
			
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
