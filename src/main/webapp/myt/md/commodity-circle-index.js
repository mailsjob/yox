$(".grid-md-commodity-circle").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/commodity!findByPage?search['EQ_circle']=true",
        colModel : [ {
            label : '流水号',
            name : 'id',
            responsive : 'sm'
        }, {
            label : '主图',
            name : 'smallPic',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '商品编码',
            name : 'sku',
            align : 'left',
            width : 80
        }, {
            label : '商品名称',
            name : 'title',
            sortable : false,
            width : 200,
            align : 'left'
        }, {
            label : '基准价格',
            name : 'price',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : '启用',
            name : 'circle',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        }, {
            label : '最小购买量',
            name : 'circleMinQuantity',
            width : 80,
            editable : true,
            align : 'right'
        }, {
            label : '3月价',
            name : 'commodityVaryPrice.m3CirclePrice',
            formatter : 'currency',
            editable : true,
            align : 'right',
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var rowid = $elem.closest("tr.jqgrow").attr("id");
                    var rowdata = $grid.jqGrid("getRowData", rowid);
                    if ($elem.val() == '') {
                        $elem.val((rowdata['price'] * 0.95).toFixed(0));
                    }
                }
            }
        }, {
            label : '4月价',
            name : 'commodityVaryPrice.m4CirclePrice',
            formatter : 'currency',
            editable : true,
            align : 'right',
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var rowid = $elem.closest("tr.jqgrow").attr("id");
                    var rowdata = $grid.jqGrid("getRowData", rowid);
                    if ($elem.val() == '') {
                        $elem.val((rowdata['price'] * 0.9).toFixed(0));
                    }

                }
            }
        }, {
            label : '5月价',
            name : 'commodityVaryPrice.m5CirclePrice',
            formatter : 'currency',
            editable : true,
            align : 'right',
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var rowid = $elem.closest("tr.jqgrow").attr("id");
                    var rowdata = $grid.jqGrid("getRowData", rowid);
                    if ($elem.val() == '') {
                        $elem.val((rowdata['price'] * 0.85).toFixed(0));
                    }
                }
            }
        }, {
            label : '6月价',
            name : 'commodityVaryPrice.m6CirclePrice',
            formatter : 'currency',
            editable : true,
            align : 'right',
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var rowid = $elem.closest("tr.jqgrow").attr("id");
                    var rowdata = $grid.jqGrid("getRowData", rowid);
                    if ($elem.val() == '') {
                        $elem.val((rowdata['price'] * 0.80).toFixed(0));
                    }
                }
            }
        }, {
            label : '标题',
            name : 'circleTitle',
            align : 'left',
            editable : true,
            width : 200
        }, {
            label : '状态',
            name : 'commodityStatus',
            align : 'center',
            width : 50,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('commodityStatusEnum')
            },
            responsive : 'sm'
        } ],
        postData : {
            "search['FETCH_brand']" : "LEFT",
            "search['FETCH_partner']" : "LEFT",
            "search['FETCH_category']" : "LEFT",
            "search['FETCH_commodityVaryPrice']" : "LEFT"
        },
        inlineNav : {
            add : false
        },
        editcol : 'sku',
        editurl : WEB_ROOT + "/myt/md/commodity!doSave",
        delurl : WEB_ROOT + "/myt/md/commodity!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/commodity!inputTabs",
        operations : function(itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>批量下架</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/md/commodity!putOff';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  下架？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select);
            
        },
        setGroupHeaders : {
            groupHeaders : [ {
                startColumnName : 'circle',
                numberOfColumns : 7,
                titleText : '周期购'
            } ]
        }
    });