<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">品牌列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-md-brand-early-warning" action="#">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_title']" class="form-control" placeholder="品牌名称">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-brand-early-warning" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-brand-early-warning").data("gridOptions", {
            url : "${base}/myt/md/brand!getBrandEndDateWarningList",
            colNames : [ '周期购', '排序号', '标题', '同义词', '标语', '图片', '品牌链接' ,'授权开始日期 ', '授权截止日期'  ],
            colModel : [ {
                name : 'circle',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            },{
                name : 'orderIndex',
                width : 60,
                editable : true,
                align : 'center'
            }, {
                name : 'title',
                editable : true,
                width : 150,
                align : 'left'
            }, {
                name : 'synonyms',
                editable : true,
                width : 150,
                align : 'left'
            }, {
                name : 'slogan',
                editable : true,
                width : 150,
                align : 'left'
            }, {
                name : 'smallPic',
                formatter : Biz.md5CodeImgViewFormatter,
                width : 80,
                align : 'left'
            }, {
                name : 'brandUrl',
                align : 'left',
                editable : true,
                formatter : function(cellValue, options, rowdata, action) {
                    if (cellValue) {
                        if (rowdata.brandUrl.startsWith("http:")) {
                            return '<a href="'+rowdata.brandUrl+'" target="_blank">' + rowdata.brandUrl + '</a>';
                        } else {
                            return '<a href="http://'+rowdata.brandUrl+'" target="_blank">' + rowdata.brandUrl + '</a>';
                        }
                    } else {
                        return "";
                    }

                }
            }, {
                name : 'startDate',
                width : 120,
                editable : true,
                sorttype : 'date',
                align : 'center'
            }, {
                name : 'endDate',
                width : 120,
                editable : true,
                sorttype : 'date',
                align : 'center'
            }  ],
            editcol : 'title',
            editurl : "${base}/myt/md/brand!doSave",
            delurl : "${base}/myt/md/brand!doDelete",
            fullediturl : "${base}/myt/md/brand!inputTabs",
         });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>