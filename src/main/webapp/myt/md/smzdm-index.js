$(function() {
    $(".grid-myt-md-smzdm-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/smzdm!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '图片',
            name : 'img',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            editable: false,
            responsive : 'sm'
        }, {
            label : '商品',
            name : 'commodity.display',
            index : 'commodity',
            width : 200,
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '爆料价',
            name : 'price',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '适合人群',
            name : 'fitPeople',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }, {
            label : '主题',
            name : 'title',
            width : 64,
            editable: true,
            align : 'left'
        }, {
            label : '副标题',
            name : 'subTitle',
            width : 64,
            editable: true,
            align : 'left'
        }, {
            label : '介绍',
            name : 'introduction',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }, {
            label : '链接',
            name : 'zdmLink',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }, {
            label : '适合集合',
            name : 'fitUnit',
            width : 60,
            editable: true,
            align : 'right'
        }, {
            label : '标签',
            name : 'labels',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }, {
            label : '联系邮箱',
            name : 'contactEmail',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }, {
            label : '适合阶段',
            name : 'fitStage',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('fitStageEnum')
            },
            width : 80,
            editable: true,
            align : 'center'
        }, {
            label : '起始',
            name : 'fitStart',
            width : 60,
            editable: true,
            align : 'right',
            hidden : true
        }, {
            label : '概述',
            name : 'summary',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }, {
            label : '开始时间',
            name : 'beginTime',
            width : 100,
            formatter: 'date',
            editable: true,
            align : 'center'
        },{
            label : '结束时间',
            name : 'endTime',
            width : 100,
            formatter: 'date',
            editable: true,
            align : 'center'
        }, {
            label : '评论数',
            name : 'commentCount',
            width : 45,
            editable: true,
            align : 'center'
        }, {
            label : '记录时间',
            name : 'recordTime',
            width : 150,
            formatter: 'date',
            editable: true,
            align : 'center',
            hidden : true
        }, {
            label : '是否可用',
            name : 'enable',
            width : 50,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }],
        postData: {
           "search['FETCH_commodity']" : "LEFT"
        },
        delurl : WEB_ROOT + '/myt/md/smzdm!doDelete',
        fullediturl : WEB_ROOT + '/myt/md/smzdm!inputTabs'
    });
});
