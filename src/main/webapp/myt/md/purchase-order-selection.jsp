<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row">
	<table class="grid-myt-purchase-order-selection" data-grid="table"></table>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-purchase-order-selection").data("gridOptions", {
            url : '${base}/myt/purchase/purchase-order!findByPage',
            colModel : [ {
                label : '编号',
                name : 'sn',
                width : 100,
                align : 'center'
            } , {
                label : '供货商主键',
                name : 'supplier.id',
                width : 100,
                align : 'center',
                hidden:true
            }, {
                label : '供货商',
                name : 'supplier.display',
                width : 100,
                align : 'center'
            }, {
                label : '默认库存地主键',
                name : 'storageLocation.id',
                hidden:true
             }, {
                label : '默认库存地',
                name : 'storageLocation.display',
                sortable : false,
                align : 'center'
            }, {
                label : '付款类型',
                name : 'payMode',
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : ""
                }
            }, {
                label : '单位',
                name : 'measureUnit',
                align : 'left'
            }, {
                label : '成本价',
                name : 'costPrice',
               	align : 'right'
            }, {
                name : 'display',
                hidden:true,
                align : 'left'
            }, {
                label : '排序号',
                name : 'recommendRank',
                width : 60,
                editable : true,
                align : 'center'
            }, {
                name : 'display',
                hidden : true
            } ],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            },
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : "${base}/myt/purchase/purchase-order!purchaseOrderDetails?id=" + row_id,
                    colNames : [ '商品', '采购单价', '销售数量' ],
                    colModel : [ {
                        name : 'commodity.display',
                        width : 100
                    }, {
                        name : 'price',
                        width : 100
                    }, {
                        name : 'quantity',
                        width : 100
                        
                            }]
        });
            }
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
