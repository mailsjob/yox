<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-commodity-comments" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-theme-comments" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-theme-comments").data("gridOptions", {
            url : "${base}/myt/md/theme-comment!findByPage",
            colModel : [{
                name : 'senderProfile.id',
                hidden : true,
                hidedlg : true,
                editable : true
            }, {
                label : '客户',
                name : 'senderProfile.display',
                editable : true,
                editrules : {
                    required : true
                },
                editoptions : {
                    dataInit : function(elem) {
                        var $grid = $(this);
                        var $elem = $(elem);

                        $elem.wrap('<div class="input-icon right"/>');
                        $elem.before('<i class="fa fa-ellipsis-horizontal fa-select-customer-profile"></i>');
						 var selectCustomerProfile = function(item) {
                            // 强制覆盖已有值
                            $grid.jqGrid("setEditingRowdata", {
                                'senderProfile.id' : item.id,
                                'senderProfile.display' : item.display,
                             });
                        }

                        $grid.find(".fa-select-customer-profile").click(function() {
                            $(this).popupDialog({
                                url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                                title : '选取客户',
                                callback : selectCustomerProfile
                            })
                        });
                     }
                },
                width : 150,
                align : 'left'
            },{
                label:'内容',
                name : 'commentContent',
                editable : true,
                width : 150
            }, {
                label:'禁用',
                name : 'enable',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            } ],
           
            postData : {
                "search['EQ_theme.id']" : "<s:property value='#parameters.id'/>",
                "search['NU_parent.id']" : true
            },
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initRecursiveSubGrid(subgrid_id, row_id, "parent.id",true);
            },
            inlineNav : {
                add : true
            },
            editurl : "${base}/myt/md/theme-comment!doSave?theme.id=<s:property value='#parameters.id'/>",
            delurl : "${base}/myt/md/theme-comment!doDelete"
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>