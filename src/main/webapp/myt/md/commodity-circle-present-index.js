$(function() {
    $(".grid-myt-md-commodity-circle-present").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/commodity-circle-present!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id'
        },{
            label : '时间',
            name : 'createdDate',
            width : 120,
            align : 'center'
        }, {
            label : '商品主键',
            name : 'commodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '周期购商品',
            name : 'commodity.display',
            index : 'commodity.sku_OR_commodity.title',
            editable : true,
            width : 200,
            editrules : {
                required : true
            },
            editoptions : Biz.getGridCommodityEditOptions(),
            align : 'left'
        }, {
            label : '周期购期数',
            name : 'circleMonthCount',
            editable : true,
            width : 60,
            sorttype : 'number',
            align : 'center'
        }, {
            label : '购买总金额',
            name : 'amount',
            editable : true,
            width : 60,
            formatter : 'currency'
        }, {
            label : '送出第期数',
            name : 'sendByCircleIndex',
            editable : true,
            width : 60,
            sorttype : 'number',
            align : 'center',
            editoptions : {
                defaultValue : 1
            }
        }, {
            label : '赠品主键',
            name : 'presentCommodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '赠品商品',
            name : 'presentCommodity.display',
            index : 'presentCommodity.sku_OR_presentCommodity.title',
            editable : true,
            width : 200,
            editrules : {
                required : true
            },
            editoptions : Biz.getGridCommodityEditOptions(),
            align : 'left'
        }, {
            label : '赠品数',
            name : 'quantity',
            editable : true,
            width : 60,
            sorttype : 'number',
            align : 'right',
            editoptions : {
                defaultValue : 1
            }
        }, {
            label : '开始日期',
            name : 'beginDate',
            width : 150,
            formatter: 'date',
            editable: true,
            align : 'center'
        }, {
            label : '结束日期',
            name : 'endDate',
            width : 150,
            formatter: 'date',
            editable: true,
            align : 'center'
        }, {
            label : '启用',
            name : 'enable',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        }],
        /*grouping : true,
        groupingView : {
            groupField : [ 'commodity.sid' ],
            groupOrder : [ 'desc' ],
            groupCollapse : false
        },*/
        sortorder : "desc",
        sortname : "id",
        editurl : WEB_ROOT + '/myt/md/commodity-circle-present!doSave',
        delurl : WEB_ROOT + '/myt/md/commodity-circle-present!doDelete'
    });
});