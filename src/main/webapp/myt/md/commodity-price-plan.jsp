<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-commodity-price" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-commodity-price-plan" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="div-md-commodity-price-input-batch"></div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-commodity-price-plan").data("gridOptions", {
            url : "${base}/myt/md/commodity!getCommodityPricePlans?id=<s:property value='#parameters.id'/>",
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true                          
            }, {
                label : '价格',
                name : 'price',
                width : 60,
                formatter: 'number',
                editable: true,
                align : 'right'
            }, {
                label : '时间类型',
                name : 'timeType',
                formatter : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('timeTypeEnum')
                },
                width : 80,
                editable: true,
                align : 'center',
                editoptions : {
                    dataInit : function(elem) {
                        var $grid = $(this);
                        var $elem = $(elem);
                        var rowid = $elem.closest("tr.jqgrow").attr("id");
                        var rowdata = $grid.jqGrid("getRowData", rowid);
                        if ($elem.val() == '') {
                            $elem.val('M');
                        }
                    }
                }
            }, {
                label : '时间偏移量',
                name : 'timeOffset',
                width : 60,
                editable: true,
                align : 'center'
            } ],
            editurl : "${base}/myt/md/commodity-price-plan!doSave?commodity.id=<s:property value='#parameters.id'/>",
            delurl : "${base}/myt/md/commodity-price-plan!doDelete"
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>