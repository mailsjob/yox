<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-activity-block"
	action="${base}/myt/md/activity-block!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-activity-block">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">选择活动</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-activity"></i>
							<s:textfield name="activity.display" />
							<s:hidden name="activity.id" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">位置代码</label>
					<div class="controls">
						<s:textfield name="locationCode" readonly="%{notNew}" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">排序号</label>
					<div class="controls">
						<s:textfield name="orderIndex" />
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">名称</label>
					<div class="controls">
						<s:textfield name="blockTitle" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">样式</label>
					<div class="controls">
						<s:select name="blockTemplate" list="#application.enums.blockTemplateEnum"></s:select>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {
        $(".form-edit-activity-block").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                $form.find(".fa-select-activity").click(function() {
                    $(this).popupDialog({
                        url : '${base}/myt/md/activity!forward?_to_=selection',
                        title : '选取活动',
                        callback : function(rowdata) {
                            $form.setFormDatas({
                                'activity.id' : rowdata.id,
                                'activity.display' : rowdata.display
                            });

                        }
                    })
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
