$(".grid-md-floor-ad").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/floor-ad!findByPage',
        colModel : [{
            label : '楼层主键',
            name : 'floor.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '楼层',
            name : 'floor.displayLabel',
            width : 100,
            align : 'left'
        }, {
            label : '楼层文本',
            hidden : true,
            name : 'floor.monthText',
            width : 100,
            align : 'left'
        }, {
            label : '名称',
            name : 'name',
            editable : true,
            width : 100,
            align : 'left'
        },{
            label : '图片',
            name : 'pic',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        },{
            label : '背景图片',
            name : 'bgPic',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        },{
            label : '排序号',
            name : 'orderIndex',
            width : 80,
            editable : true,
            align : 'right'
        }, {
            label : '位置',
            name : 'position',
            editable : true,
            width : 80,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('floorAdPositionEnum')
            },
            align : 'center'
        }, {
            label : '广告类型',
            name : 'adType',
            editable : true,
            index : 'position',
            width : 80,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('floorAdTypeEnum')
            },
            align : 'center'
        },{
            label:'跳转至',
            name : 'target',
            editable : true
        } ,{
            label:'是否可用',
            name : 'enable',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        }],
        editcol : 'name',
       
        editurl : WEB_ROOT + "/myt/md/floor-ad!doSave",
        delurl : WEB_ROOT + "/myt/md/floor-ad!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/floor-ad!inputTabs",
    });