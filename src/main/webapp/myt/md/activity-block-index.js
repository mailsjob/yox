$(function() {
    $(".grid-md-activity-block").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/activity-block!findByPage",
        colModel : [ {
            label : '活动主键',
            name : 'activity.id',
            editable : true,
            hidden : true,
            hidedlg : true,
            width : 50
        }, {
            label : '活动',
            name : 'activity.display',
            index : 'activity.activityCode_OR_activity.activityName',
            editable : true,
            width : 120,
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);

                    $elem.wrap('<div class="input-icon right"/>');
                    $elem.before('<i class="fa fa-ellipsis-horizontal fa-select-activity"></i>');
                    var selectAdObject = function(item) {
                        // 强制覆盖已有值
                        $grid.jqGrid("setEditingRowdata", {
                            'activity.id' : item.id,
                            'activity.display' : item.display
                        });

                    }
                    $grid.find(".fa-select-activity").click(function() {
                        var rowdata = $grid.jqGrid("getEditingRowdata");
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/activity!forward?_to_=selection',
                            title : '选取活动',
                            callback : selectAdObject
                        })
                    });
                }
            },
        }, {
            label : '位置代码',
            name : 'locationCode',
            editable : true,
            width : 120,
            editoptions : {
                updatable : false
            }
        }, {
            label : '名称',
            name : 'blockTitle',
            width : 120,
            editable : true,
            align : 'left'
        }, {
            label : '块样式',
            stype : 'select',
            name : 'blockTemplate',
            editable : true,
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('blockTemplateEnum')
            },
            width : 80
        }, {
            label : '排序号',
            name : 'orderIndex',
            width : 80,
            editable : true,
            align : 'center'
        } ],

        editcol : 'locationCode',
        editurl : WEB_ROOT + "/myt/md/activity-block!doSave",
        delurl : WEB_ROOT + "/myt/md/activity-block!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/activity-block!inputTabs",
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + '/myt/md/activity-block!activityR2Objects?id=' + row_id,
                colModel : [ {
                    label : '可用',
                    name : 'enable',
                    edittype : 'checkbox',
                    editable : true,
                    align : 'center',
                    width : 80
                }, {
                    label : '图片',
                    name : 'pic',
                    sortable : false,
                    search : false,
                    width : 80,
                    align : 'center',
                    formatter : Biz.md5CodeImgViewFormatter,
                    frozen : true,
                    responsive : 'sm'
                }, {
                    label : '对象类型',
                    stype : 'select',
                    editable : true,
                    name : 'objectType',
                    align : 'center',
                    searchoptions : {
                        value : Util.getCacheEnumsByType('blockObjectTypeEnum')
                    },
                    width : 100
                }, {
                    label : '对象主键',
                    name : 'objectSid',
                    editable : true,
                    hidden : true,
                    hidedlg : true,
                    width : '100'
                }, {
                    label : '对象名',
                    name : 'objectDisplay',
                    editable : true,
                    width : 200,
                    align : 'left',
                    editoptions : {
                        dataInit : function(elem) {
                            var $grid = $(this);
                            var $elem = $(elem);

                            $elem.wrap('<div class="input-icon right"/>');
                            $elem.before('<i class="fa fa-ellipsis-horizontal fa-select-object"></i>');
                            var selectAdObject = function(item) {
                                // 强制覆盖已有值
                                $grid.jqGrid("setEditingRowdata", {
                                    'objectSid' : item.id,
                                    'objectDisplay' : item.display
                                });

                            }
                            $grid.find(".fa-select-object").click(function() {

                                var rowdata = $grid.jqGrid("getEditingRowdata");
                                if (rowdata['objectType'] == '') {
                                    alert("请先选择对象类型");
                                    return false;
                                }
                                if (rowdata['objectType'] == 'COMMODITY') {
                                    $(this).popupDialog({
                                        url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                                        title : '选取商品',
                                        callback : selectAdObject
                                    })
                                } else if (rowdata['objectType'] == 'THEME') {
                                    $(this).popupDialog({
                                        url : WEB_ROOT + '/myt/md/theme!forward?_to_=selection',
                                        title : '选取主题',
                                        callback : selectAdObject
                                    })
                                } else if (rowdata['objectType'] == 'ACTIVITY') {
                                    $(this).popupDialog({
                                        url : WEB_ROOT + '/myt/md/activity!forward?_to_=selection',
                                        title : '选取活动',
                                        callback : selectAdObject
                                    })
                                }

                            });
                        }
                    },
                } ],
                cmTemplate : {
                    sortable : false
                },
                editurl : WEB_ROOT + '/myt/md/activity-block-r2-object!doSave?activityBlock.id=' + row_id,
                delurl : WEB_ROOT + "/myt/md/activity-block-r2-object!doDelete",
                rowNum : -1,
            });
        }
    });
});