<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row search-form-default">
		<div class="col-md-12">
			<form method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-md-activity-block-selection"
				action="#">
				<div class="form-group">
					<input type="text" name="search['CN_activity.activityCode_OR_activity.activityName_OR_locationCode_OR_blockTitle']" class="form-control input-xlarge"
						placeholder="活动、代码、名称..." />
				</div>
				
				<button class="btn default hidden-inline-xs" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-md-activity-block-selection"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-activity-block-selection").data("gridOptions", {
            url : "${base}/myt/md/activity-block!findByPage",
            colModel : [{
                label:'活动代码',
                name : 'activity.display',
                index:'activity.activityCode_OR_activity.activityName',
                width : 120
            },{
                label:'代码',
                name : 'locationCode',
                width : 120
            },{
                label:'名称',
                name : 'blockTitle',
                width : 120,
                align : 'left'
            },{
                label:'活动主键',
                name : 'display',
                hidden:true,
                width : 50
            }, {
                label : '区域样式',
                stype : 'select',
                name : 'blockTemplate',
                align : 'center',
                searchoptions : {
                    value : Util.getCacheEnumsByType('blockTemplateEnum')
                },
                width : 80
            },{
                label:'排序号',
                name : 'orderIndex',
                width : 80,
                align : 'center'
            }],
            
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }

            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
