 $(function() {
        $(".grid-md-activity-block-r2-object").data("gridOptions", {
            url : WEB_ROOT + "/myt/md/activity-block-r2-object!findByPage",
            colModel : [ {
                label:'活动块',
                name : 'activityBlock.display',
                hidden:true,
                index:'activityBlock.locationCode_OR_activityBlock.blockTitle',
                width : 120
            },{
                label : '可用',
                name : 'enable',
                edittype : 'checkbox',
                editable : true,
                align : 'center',
                width : 80
            },{
                label : '图片',
                name : 'pic',
                sortable : false,
                search : false,
                width : 80,
                align : 'center',
                formatter : Biz.md5CodeImgViewFormatter,
                frozen : true,
                responsive : 'sm'
            }, {
                label : '对象类型',
                stype : 'select',
                name : 'objectType',
                align : 'center',
                searchoptions : {
                    value : Util.getCacheEnumsByType('blockObjectTypeEnum')
                },
                width : 80
            }, {
                label : '对象名',
                name : 'objectDisplay',
                width : 150
            }],
            inlineNav:{
                add:false
            },
            /*grouping : true,
            groupingView : {
                groupField : [ 'activityBlock.display' ],
                groupOrder : [ 'asc' ],
                groupCollapse : false
            },*/
            editcol : 'objectDisplay',
            editurl : WEB_ROOT + "/myt/md/activity-block-r2-object!doSave",
            delurl : WEB_ROOT + "/myt/md/activity-block-r2-object!doDelete",
            fullediturl : WEB_ROOT + "/myt/md/activity-block-r2-object!inputTabs",
         });
    });