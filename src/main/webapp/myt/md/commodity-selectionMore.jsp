<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row search-form-default">
		<div class="col-md-12">
			<form method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-commodity-selection"
				action="#">
				<div class="form-group">
					<s:textfield name="search['CN_barcode_OR_sku_OR_title']" cssClass="form-control input-xlarge"
						placeholder="编码/条码/名称..." value="%{#parameters.keyword}" />
				</div>
				<div class="form-group">
					<div class="btn-group">
						<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-angle-down"></i>
						</button>
						<div class="dropdown-menu hold-on-click dropdown-checkboxes">
							<s:checkbox name="search['EQ_commodityStatus']" fieldValue="S30ONSALE" label="只查询在售" value="true" />
						</div>
					</div>
				</div>
				<button class="btn default hidden-inline-xs" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-commodity-selection"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-commodity-selection").data("gridOptions", {
            url : "${base}/myt/md/commodity!findByPage",
            postData : {
                "search['FETCH_brand']" : "LEFT"
            },
            colModel : [ {
                label : '已选择数量',
                name : 'selectedQuantity',
                width : 40,
                align : 'center'
            },{
                label : '流水号',
                name : 'id',
                width : 50,
                align : 'center'
            }, {
                label : '主图',
                name : 'smallPic',
                width : 50,
                align : 'center',
                formatter : Biz.md5CodeImgViewFormatter
            },{
                label : '主图',
                name : 'smallPic',
                width : 50,
                align : 'center'
               
            }, {
                label : '商品编码',
                name : 'sku',
                width : 80,
                align : 'center'
            }, {
                label : '品牌',
                name : 'brand.title',
                width : 100,
                align : 'left',
                responsive : 'sm'
            }, {
                label : '商品名称',
                name : 'title',
                width : 250,
                sortable : false,
                align : 'left'
            }, {
                label : '哎呦价',
                name : 'price',
                formatter : 'currency',
                width : 50
            }, {
                label : '单位',
                name : 'measureUnit',
                width : 50,
                align : 'left'
            }, {
                label : '成本价',
                name : 'costPrice',
                hidden : true,
                align : 'right'
            }, {
                name : 'display',
                hidden : true,
                align : 'left'
            }, {
                label : '排序号',
                name : 'recommendRank',
                width : 60,
                hidden : true,
                align : 'center'
            }, {
                label : '状态',
                name : 'commodityStatus',
                align : 'center',
                width : 50,
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('commodityStatusEnum')
                },
                responsive : 'sm'
            }, {
                name : 'display',
                hidden : true,
                hidedlg : true
            }, {
                name : 'defaultStorageLocation.id',
                hidden : true,
                hidedlg : true
            }, {
                name : 'defaultStorageLocation.display',
                hidden : true,
                hidedlg : true
            }, /* {
                label : "库存模式",
                name : 'defaultStorageLocation.storageMode',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('storageModeEnum')
                }
            }, */ {
                name : 'barcode',
                hidden : true,
                hidedlg : true
            } ],
            /* postData: {
                "search['EQ_defaultStorageLocation.storageMode']" : "<s:property value='#parameters.storageMode'/>"
             }, */
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                // $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    rowdata.id = id;
                    $grid.jqGrid('setRowData', id, {
                        'selectedQuantity' : MathUtil.add(rowdata['selectedQuantity'], 1)
                    });
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
