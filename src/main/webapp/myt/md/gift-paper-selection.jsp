<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
    <div class="col-md-12">
        <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-md-gift-paper-selection">
            <div class="form-group">
                <label class="sr-only">客户</label> <input type="text" name="search['CN_nickName_OR_trueName_OR_mobilePhone_OR_email']" class="form-control input-xlarge" placeholder="昵称、姓名、手机号码、邮件地址..." />
            </div>
            <button class="btn default" type="reset">
                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
            </button>
            <button class="btn green" type="submmit">
                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
            </button>

        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="grid-md-gift-paper-selection"></table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-gift-paper-selection").data("gridOptions", {
            url : "${base}/myt/md/gift-paper!findByPage?search['EQ_paperMode']=G",
            colModel : [ {
                label : '模板号',
                name : 'paperTemplateCode',
                width : 250,
                align : 'center'
            }, {
                label : '优惠券代码',
                name : 'code',
                align : 'left',
                width : 150,
            }, {
                label : '优惠券名称',
                name : 'title',
                align : 'left',
                width : 200,
            }, {
                label : '开始时间',
                name : 'beginTime',
                sorttype : 'date',
                align : 'center',
                width : 100
            }, {
                label : '过期时间',
                name : 'expireTime',
                sorttype : 'date',
                align : 'center',
                width : 100
            }, {
                label : '类型',
                name : 'paperMode',
                width : 80,
                align : 'center'
            }, {
                label : '状态',
                name : 'paperStatus',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('giftPaperStatusEnum')
                }
            }, {
                label : '面额',
                name : 'faceValue',
                align : 'right',
                width : 100
            }, {
                label : '来源',
                name : 'paperSource',
                width : 100
            }, {
                label : 'funcCode',
                name : 'funcCode',
                width : 100
            }, {
                label : 'funcParameter',
                name : 'funcParameter',
                width : 300
            }],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>