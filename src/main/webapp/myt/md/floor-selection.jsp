<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row">
		<div class="col-md-12">
			<table class="grid-md-floor-selection" data-grid="table"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-floor-selection").data("gridOptions", {
            url : "${base}/myt/md/floor!findByPage",
            colModel : [{
                label : '流水号',
                name : 'id',
                width : 50,
                align : 'center'
            }, {
                label : '排序号',
                name : 'orderIndex',
                width : 80,
                
                sorttype : 'number',
                align : 'right'
            }, {
                label : '适用起始月',
                name : 'fitStart',
                width : 80,
                
                sorttype : 'number',
                align : 'right'
            }, {
                label : '适用截止月',
                name : 'fitEnd',
                width : 80,
                
                sorttype : 'number',
                align : 'right'
            }, {
                label : '适用阶段',
                name : 'fitStage',
                
                align : 'left'
            }, {
                label : '月份文本',
                name : 'monthText',
                
                align : 'left'
            }, {
                label : '楼层类型',
                name : 'floorType',
                index : 'floorType',
                width : 80,
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('floorTypeEnum')
                },
                align : 'center'
            }, {
                label : '楼层适用类型',
                name : 'siteFlag',
                
                index : 'siteFlag',
                width : 80,
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('siteFlagEnum')
                },
                align : 'center'
            }, {
                label : 'display',
                name : 'display',
                hidden:true,
                align : 'left'
            }, {
                label : 'displayLabel',
                name : 'displayLabel',
                hidden:true,
                align : 'left'
            } ],
            sortorder : "asc",
            sortname : "orderIndex",
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
