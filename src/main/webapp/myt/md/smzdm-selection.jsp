<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row search-form-default">
		<div class="col-md-12">
			<form method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-md-smzdm-selection"
				action="#">
				<div class="form-group">
					<input type="text" name="search['CN_commodity.sku_OR_commodity.title_OR_title_OR_subTitle_OR_zdmLink_OR_contactEmail']" class="form-control input-xlarge"
						placeholder="商品、标题、副标题、链接、邮箱..." />
				</div>
				
				<button class="btn default hidden-inline-xs" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-md-smzdm-selection"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-smzdm-selection").data("gridOptions", {
            url : "${base}/myt/md/smzdm!findByPage",
            colModel : [{
                label : '流水号',
                name:'id'
            },{
                label : '图片',
                name : 'img',
                width : 50,
                align : 'center',
                formatter : Biz.md5CodeImgViewFormatter
            },{
                label : '标题',
                name : 'display',
                width : 100,
                align : 'left',
                responsive : 'sm'
            },{
                label : '商品主键',
                name : 'commodity.id',
                hidden : true,
                hidedlg : true,
                width:'100'
            }, {
                label : '商品',
                name : 'commodity.display',
                index : 'commodity.sku_OR_commodity.title',
                editoptions : Biz.getGridCommodityEditOptions(),
                width:'150'
            }, {
                label : '爆料价',
                name : 'price',
                align : 'right',
                width : 50
            }, {
                label : '评论数',
                name : 'commentCount',
                width : 50,
                align : 'left'
            }, {
                label : '开始时间',
                sorttype:'date',
                name : 'beginTime',
               	align : 'center'
            }, {
                label : '结束时间',
                name : 'endTime',
                width : 60,
                sorttype:'date',
                align : 'center'
            }, {
                label : '推荐',
                name : 'introduction',
                width : 100,
                align : 'left',
                hidden:true,
                responsive : 'sm'
            }, {
                label : '标签',
                name : 'labels',
                width : 100,
                align : 'left',
                responsive : 'sm'
            }, {
                label : '链接',
                name : 'zdmLink',
                width : 100
            }, {
                label : '联系邮箱',
                name : 'contactEmail',
                width : 100
            }, {
                label : '适应阶段',
                name : 'fitStage',
                align : 'center',
                width : 80,
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('fitStageEnum')
                },
                responsive : 'sm'
            }, {
                label : '起始',
                name : 'fitStart',
                width : 100,
                align : 'left',
                responsive : 'sm'
            }, {
                label : '适合集合',
                name : 'fitUnit',
                width : 100,
                align : 'left',
                responsive : 'sm'
            }, {
                label : '适应人群',
                name : 'fitPeople',
                width : 100,
                align : 'left',
                responsive : 'sm'
            }],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }

            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
