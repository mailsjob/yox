<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<style>
.controls-categoryTimelines label.checkbox-inline {
	width: 120px;
	margin-left: 10px;
}

.controls-categoryLabels label.checkbox-inline {
	width: 150px;
	margin-left: 10px;
}
</style>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-category"
	action="${base}/myt/md/category!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-category-normal">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">编码</label>
					<div class="controls">
						<s:textfield name="categoryCode" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">名称</label>
					<div class="controls">
						<s:textfield name="name" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">首付比率</label>
					<div class="controls">
						<s:textfield name="payFirstRate" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否覆盖子分类</label>
					<div class="controls">
						<s:radio name="payFirstRateOverwriteChildren" list="#application.enums.booleanLabel" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">排序号</label>
					<div class="controls">
						<s:textfield name="orderIndex" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否周期购</label>
					<div class="controls">
						<s:radio name="circle" list="#application.enums.booleanLabel" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">等级</label>
					<div class="controls">
						<s:textfield name="categoryLevel" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">分类类型</label>
					<div class="controls">
						<s:select name="categoryType" list="#application.enums.categoryEnum" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">手机图片</label>
					<div class="controls">
						<s:hidden name="mobileIcon" data-singleimage="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">常规图片</label>
					<div class="controls">
						<s:hidden name="icon" data-singleimage="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">鼠标悬停图片</label>
					<div class="controls">
						<s:hidden name="hoverIcon" data-singleimage="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">分类描述(手机)</label>
					<div class="controls">
						<s:textarea name="mobileDesc" rows="2" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">关键字</label>
					<div class="controls">
						<s:textarea name="keywords" rows="4" onclick="$(this).closest('div.row').next('div.row').toggle()" />
					</div>
				</div>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">站点类型</label>
                    <div class="controls">
                        <s:radio name="siteType" list="#application.enums.siteTypeEnum" />
                    </div>
                </div>
            </div>
        </div>
		<div class="row" style="display: none">
			<div class="col-md-12">
				<div class="form-group form-group-selectionKeyword">
					<label class="control-label">候选关键字<input type="checkbox" /></label>
					<div class="controls controls-categoryLabels">
						<s:checkboxlist list="categoryLabelsMap" name="selectionKeyword" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-category-normal">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {
        var $form = $(".form-edit-category");
        $(".form-edit-category").data("formOptions", {
            bindEvents : function() {

                var clickSelection = function(source, target) {
                    var keywords = $(target).val().split(" ");
                    $(source).each(function() {
                        var keyword = $(this).val();
                        var checked = $(this).attr("checked");
                        var exists = false;
                        $.each(keywords, function(i, item) {
                            if (keyword == $.trim(item)) {
                                exists = true;
                                return false;
                            }
                        });
                        if (!checked && exists) {
                            keywords.splice(keywords.indexOf(keyword), 1);
                        }
                        if (checked && !exists) {
                            keywords.push(keyword);
                        }
                    });
                    $(target).val($.trim(keywords.join(" ")));
                }

                $form.on('click', '.form-group-selectionKeyword .controls :checkbox', function(e) {
                    clickSelection($(this), $form.find("input [name='keywords']"));
                });

                $form.on('click', '.form-group-selectionKeyword .control-label :checkbox', function(e) {
                    var chks = $form.find(".form-group-selectionKeyword .controls :checkbox");
                    chks.attr("checked", this.checked);
                    clickSelection(chks, $form.find("input [name='keywords']"));
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>