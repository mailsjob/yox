$(function() {
    $(".grid-md-money-paper").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/money-paper!findByPage",
        colModel : [  {
            label : '代码',
            name : 'code',
            width : 100
        }, {
            label : '名称',
            name : 'title',
            align : 'left',
            width : 100,
        }, {
            label : '客户',
            name : 'customerProfile.display',
            index:'customerProfile.trueName_OR_customerProfile.nickName',
            width : 100
        }, {
            label : '开始时间',
            name : 'beginTime',
            sorttype:'date',
             align : 'center',
            width : 100
        }, {
            label : '过期时间',
            name : 'expireTime',
            sorttype:'date',
            align : 'center',
            width : 100
        }, {
            label : '面额',
            name : 'faceValue',
            formatter : 'currency',
            width : 100
        }, {
            label : '余额',
            name : 'restAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '模式',
            name : 'paperMode',
            width : 80,
            align : 'center'
        }, {
            label : '状态',
            name : 'paperStatus',
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('paperStatusEnum')
            }
        }, {
            label : '分类',
            name : 'paperCategory',
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('paperCategoryEnum')
            }
        }],
       delurl : WEB_ROOT + "/myt/md/money-paper!doDelete",
       editurl : WEB_ROOT + "/myt/md/money-paper!doSave",
       fullediturl : WEB_ROOT + "/myt/md/money-paper!inputTabs",
       subGrid : true,
       subGridRowExpanded : function(subgrid_id, row_id) {
           Grid.initSubGrid(subgrid_id, row_id, {
               url : WEB_ROOT + "/myt/md/money-paper-history!findByPage?search['EQ_moneyPaper.id']=" + row_id,
               colModel : [  {
                   label : '使用金额',
                   name : 'amount',
                   formatter : 'currency',
                   width : 100
               }, {
                   label : '时间',
                   name : 'recordDatetime',
                   width : 150,
                   align : 'center'
               }, {
                   label : '说明',
                   name : 'reason',
                   width : 200
               }, {
                   label : '订单号',
                   name : 'boxOrder.orderSeq',
                   width : 100
               }],
               loadonce : true,
               multiselect : false
           });
       }
    });  
});