<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">全部商品列表</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/md/commodity-circle">周期购商品列表</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/md/commodity-vip">vip商品</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/md/commodity-erp-updateBasic.jsp">erp商品同步</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search" data-grid-search=".grid-md-commodity-normal"
						action="#">
						<div class="form-group">
							<input type="text" name="search['CN_barcode_OR_sku_OR_title_OR_erpProductId_OR_sourceUrl']" class="form-control input-large"
								placeholder="编码/条码/名称/erp商品编码..." />
						</div>
						<div data-toggle="buttons" class="btn-group">
							<label class="btn default"> <input type="checkbox" class="toggle" name="search['EQ_circle']"
								value="true"> 只显示周期购
							</label> <label class="btn default"> <input type="checkbox" class="toggle"
								name="search['GT_totalRealStockCount']" value="0"> 只显示有库存
							</label> 
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-commodity-normal" data-grid="table"></table>
				</div>
			</div>
		</div>
		
	</div>
</div>
<script src="${base}/myt/md/commodity-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>