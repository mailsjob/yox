<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation" action="${base}/myt/md/brand!doSave"
	method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-brand">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-4">
					<div class="form-group">
						<label class="control-label">图片</label>
						<div class="controls">
							<s:hidden name="smallPic" data-singleimage="true" />
						</div>
					</div>
				</div>
			<div class="col-md-8">
				<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">名称</label>
						<div class="controls">
							<s:textfield name="title" />
						</div>
					</div>
				</div>
				</div>
				<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">排序号</label>
						<div class="controls">
							<s:textfield name="orderIndex" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">周期购</label>
						<div class="controls">
							<s:radio name="circle" list="#application.enums.booleanLabel"></s:radio>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">标语</label>
						<div class="controls">
							<s:textfield name="slogan" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">同义词</label>
						<div class="controls">
							<s:textfield name="synonyms" />
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">授权开始日期</label>
						<div class="controls">
						<s3:datetextfield name="startDate" format="date"></s3:datetextfield>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">授权截止日期</label>
						<div class="controls">
							<s3:datetextfield name="endDate" format="date"></s3:datetextfield>
						</div>
					</div>
				</div>
			</div>
		<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">品牌链接</label>
						<div class="controls">
							<s:textfield name="brandUrl" />
						</div>
					</div>
				</div>
			</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">概要说明</label>
					<div class="controls">
						<s:textarea name="summary" data-htmleditor="kindeditor" data-height="150px" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">HTML文本</label>
					<div class="controls">
						<s:textarea name="htmlContent" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
