<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-md-gift-paper-inputBasic"
      action="${base}/myt/md/gift-paper!doSave" method="post">
    <s:hidden name="id"/>
    <s:hidden name="version"/>

    <s:token/>
    <div class="form-actions">
        <button class="btn blue" type="submit">
            <i class="fa fa-check"></i>保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body control-label-sm">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">代码</label>
                    <div class="controls">
                        <s:textfield name="code" readonly="true"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">密码</label>
                    <div class="controls">
                        <s:textfield name="password"/>
                    </div>
                </div>
            </div>
        </div>
        <s:if test='%{#parameters.paperType[0]=="P"}'>
            <s:hidden name="paperMode" value="%{#parameters.paperType}"/>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">销售客户</label>
                        <div class="controls">
                            <div class="input-icon right">
                                <i class="fa fa-ellipsis-horizontal fa-select-customer-profile"></i>
                                <s:textfield name="customerProfile.display"/>
                                <s:hidden name="customerProfile.id"/>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">选择模板</label>
                        <div class="controls">
                            <div class="input-icon right">
                                <i class="fa fa-ellipsis-horizontal fa-select-paper-template"></i>
                                <s:textfield name="paperTemplateCode"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </s:if>
        <s:else>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">模板类型</label>
                        <div class="controls">
                            <s:radio name="paperMode" list="#{'G':'通用','C':'循环'}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">模板号</label>
                        <div class="controls">
                            <s:textfield name="paperTemplateCode"/>
                        </div>
                    </div>
                </div>
            </div>
        </s:else>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">名称</label>
                    <div class="controls">
                        <s:textfield name="title" placeholder="适用范围,名称(范围和名称使用英文‘,’分隔)..."/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">来源</label>
                    <div class="controls">
                        <s:textfield name="paperSource"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">生效时间</label>
                    <div class="controls">
                        <s3:datetextfield name="beginTime" format="timestamp" data-timepicker="true"></s3:datetextfield>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">过期时间</label>
                    <div class="controls">
                        <s3:datetextfield name="expireTime" format="timestamp"
                                          data-timepicker="true"></s3:datetextfield>
                    </div>
                </div>
            </div>
        </div>
        <div id="choose" class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">优惠券类型</label>
                    <div class="controls">
                        <select name="paperStyle"
                                onchange="choosePater(this.options[this.options.selectedIndex].value)">
                            <option value="face">立减</option>
                            <option value="func">满减</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">状态</label>
                    <div class="controls">
                        <s:select name="paperStatus" list="#application.enums.giftPaperStatusEnum"/>
                    </div>
                </div>
            </div>
        </div>

        <div id="face" class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">面额</label>
                    <div class="controls">
                        <s:textfield name="faceValue" id="faceValue" placeholder="立减金额..."/>
                    </div>
                </div>
            </div>
        </div>
        <div id="func" class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">功能代码</label>
                    <div class="controls">
                        <s:textfield name="funcCode" id="funcCode"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label color-red">DIS_BY_PAY_AMOUNT</label>
                </div>
            </div>
        </div>
        <div id="funcPar" class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">功能参数</label>
                    <div class="controls">
                        <s:textarea id="textarea" name="funcParameter" rows="1"></s:textarea>
                        <label class="control-label color-red">{"QUANTITY":0,"ORDER_PAY_AMOUNT":0,"UNIT_PRICE":0,"DISCOUNT":0,"DIS_RATE":0}</label>
                    </div>
                </div>
            </div>
        </div>
        <s:if test='%{#parameters.paperType[0]!="P"}'>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">生成优惠券</label>
                        <div class="controls">
                            <s:textfield name="counts"/>
                        </div>
                    </div>
                </div>
                <input type="button" class="btn" style="margin-top:5px" onclick="createGpBatch()" value="点击执行">
            </div>
        </s:if>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-ajaxify-reload="_closest-ajax-container">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/md/gift-paper-inputBasic.js"/>

<%@ include file="/common/ajax-footer.jsp" %>