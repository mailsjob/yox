<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row">
		<div class="col-md-12">
			<table class="grid-md-floor-menu" data-grid="table"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-floor-menu").data("gridOptions", {
            url : "${base}/myt/md/floor-menu!findByPage?search['EQ_floor.id']="+<s:property value='#parameters.floorId'/>,
            colModel : [{
                label : '流水号',
                name : 'id',
                width : 50,
                align : 'center'
            }, {
                label : '楼层文本',
                name : 'floor.monthText',
                width : 200,
                align : 'left'
            }, {
                label : '名称',
                name : 'name',
                editable : true,
                width : 200,
                align : 'left'
            },{
                label : '排序号',
                name : 'orderIndex',
                width : 80,
                editable : true,
                align : 'right'
            }],
            sortorder : "asc",
            sortname : "orderIndex",
            postData : {
                "search['FETCH_floor']" : "INNER"
            },
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
