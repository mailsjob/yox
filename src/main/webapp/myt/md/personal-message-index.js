$(function() {
    $(".grid-myt-md-personal-message-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/personal-message!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '发布用户',
            name : 'publishUser.id',
            index : 'publishUser.signinid',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheDatas(WEB_ROOT + "/myt/md/personal-message!users")
            },
            width : 100,
            editable: true,
            align : 'left'
        }, {
            label : '标题 ',
            name : 'title',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '发布时间',
            name : 'publishTime',
            width : 150,
            stype : 'date',
            align : 'center'
        }, {
            label : '通知内容',
            name : 'contents',
            width : 400,
            editable: true,
            align : 'left'
        }, {
            label : '目标用户',
            name : 'targetUsers',
            width : 400,
            editable: true,
            align : 'left'
        } ],
        postData: {
           "search['FETCH_publishUser']" : "LEFT"
        },operations : function(itemArray) {

            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-plus"></i> 发布消息</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/md/personal-message!publishPage',
                    title : '发布消息'
                });
            });
            itemArray.push($select);
        },
        multiselect : false,
        // editurl : WEB_ROOT + '/myt/md/personal-message!doSave',
        // delurl : WEB_ROOT + '/myt/md/personal-message!doDelete',
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/md/personal-message!items?id=" + row_id,
                colModel : [ {
                    label : '流水号',
                    name : 'id',
                    hidden : true                          
                }, {
                    name : 'personalMessage.id',
                    formatter : function(){
                      return row_id;  
                    },
                    editable: true,
                    hidden : true                          
                }, {
                    label : '目标用户',
                    stype : 'select',
                    searchoptions : {
                        value : Util.getCacheDatas(WEB_ROOT + "/myt/md/personal-message!users")
                    },
                    name : 'targetUser.id',
                    index : 'targetUser.signinid',
                    width : 200,
                    editable: true,
                    align : 'left'
                }, {
                    label : '首次查看时间',
                    name : 'firstReadTime',
                    width : 150,
                    formatter: 'date',
                    align : 'center'
                }, {
                    label : '最后查看时间',
                    name : 'lastReadTime',
                    width : 150,
                    formatter: 'date',
                    align : 'center'
                }, {
                    label : '查看次数',
                    name : 'totalReadTimes',
                    width : 50,
                    align : 'center'
                }, {
                    label : '是否可用',
                    name : 'enable',
                    width : 50,
                    formatter : 'checkbox',
                    editable: true,
                    align : 'center'
                }],
                loadonce : true,
                multiselect : false
                // editurl : WEB_ROOT + '/myt/md/personal-message-item!doSave',
                // delurl : WEB_ROOT + '/myt/md/personal-message-item!doDelete',
            });
        }
    });
});
