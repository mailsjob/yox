$(function () {
    $(".grid-md-floor").data("gridOptions", {
        url: WEB_ROOT + '/myt/md/floor!findByPage',
        colModel: [/* {
         label : '流水号',
         name : 'id'
         }, */{
            label: '排序号',
            name: 'orderIndex',
            width: 80,
            editable: true,
            sorttype: 'number',
            align: 'right'
        }, {
            label: '地区',
            name: 'region.display',
            width: 80,
            editable: true,
            align: 'center'
        }, {
            label: '适用起始月',
            name: 'fitStart',
            width: 80,
            editable: true,
            sorttype: 'number',
            align: 'right'
        }, {
            label: '适用截止月',
            name: 'fitEnd',
            width: 80,
            editable: true,
            sorttype: 'number',
            align: 'right'
        }, {
            label: '适用阶段',
            name: 'fitStage',
            editable: true,
            align: 'left'
        }, {
            label: '楼层图片',
            name: 'floorImg',
            sortable: false,
            search: false,
            width: 80,
            align: 'center',
            formatter: Biz.md5CodeImgViewFormatter,
            frozen: true,
            responsive: 'sm'
        }, {
            label: '月份文本',
            name: 'monthText',
            editable: true,
            align: 'left'
        }, {
            label: '楼层类型',
            name: 'floorType',
            editable: true,
            index: 'floorType',
            width: 80,
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('floorTypeEnum')
            },
            align: 'center'
        }, {
            label: '楼层适用类型',
            name: 'siteFlag',
            editable: true,
            index: 'siteFlag',
            width: 80,
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('siteFlagEnum')
            },
            align: 'center'
        }],
        postData: {
            "search['FETCH_region']": "LEFT"
        },
        editcol: 'monthText',
        sortorder: "asc",
        sortname: "orderIndex",
        editurl: WEB_ROOT + "/myt/md/floor!doSave",
        delurl: WEB_ROOT + "/myt/md/floor!doDelete",
        fullediturl: WEB_ROOT + "/myt/md/floor!edit",
    });

});