<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-menu">商家分类列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div id="tab-menu" class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search"
						data-grid-search=".grid-partner-category-list">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_name']" class="form-control" placeholder="名称...">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-partner-category-list" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-partner-category-list").data("gridOptions", {
            url : "${base}/myt/md/partner-category!findByPage",
            colNames : [ '名称', '排序号' ],
            colModel : [ {
                name : 'name',
                editable : true
            }, {
                name : 'orderIndex',
                width : 100,
                editable : true
            } ],
           	sortorder : "desc",
            sortname : 'orderIndex',
          	subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initRecursiveSubGrid(subgrid_id, row_id, "parent.id");
            },
           	inlineNav : {
                add : true
            },
            editurl : WEB_ROOT + "/myt/md/partner-category!doSave",
            delurl : WEB_ROOT + "/myt/md/partner-category!doDelete",
         });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
