<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-md-commodity-importErpProduct"
	action="${base}/myt/md/commodity!importErpProduct" method="post" 
	enctype="multipart/form-data" >
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-commodity-importErpProduct">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
        <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-3">Excel</label>
					<div class="col-md-4">
						<input type="file" class="default" name="file"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-3">Excel模板</label>
					<div class="col-md-4">
						<p class="form-control-static">
							<a href="${base}/myt/md/import-commodity-mapping.xlsx">import-commodity-mapping.xlsx</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-commodity-importErpProduct">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>