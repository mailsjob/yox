<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation" action="${base}/myt/md/news!doSave"
	method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-news">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">标题</label>
						<div class="controls">
							<s:textfield name="newsTitle" />
						</div>
					</div>
				</div>
				</div>
				<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">摘要</label>
						<div class="controls">
							<s:textarea name="newsInfo" rows="2"></s:textarea>
						</div>
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">新闻来源</label>
								<div class="controls">
									<s:textfield name="newsFrom" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">新闻来源URL</label>
								<div class="controls">
									<s:textfield name="newsFromUrl" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">发布日期</label>
								<div class="controls">
								<s3:datetextfield name="newsDate" format="timestamp"  current="true" data-timepicker="true"></s3:datetextfield>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">相对排序号</label>
								<div class="controls">
									<s:textfield name="newsRank" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">下架</label>
							<div class="controls">
								<s:radio name="hiddenFlag" list="#application.enums.booleanLabel"></s:radio>
							</div>
					</div>
					</div>
					
				</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label">图片</label>
						<div class="controls">
							<s:hidden name="newsImg" data-singleimage="true" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">新闻内容</label>
					<div class="controls">
						<s:textarea name="newsInfoHtml" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
			</div>
		</div>
		</div>
		
		

	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
