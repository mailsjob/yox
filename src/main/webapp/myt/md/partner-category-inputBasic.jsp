<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-partner-category"
	action="${base}/myt/md/partner-category!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label class="control-label">所属父节点</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-angle-double-down btn-toggle"></i> <i class="fa fa-times"
								onclick="$(this).parent().find('input').val('')"></i>
							<s:textfield name="parent.name" data-toggle="dropdownselect"
								data-url="${base}/myt/md/partner-category!forward?_to_=selection" data-selected="%{parent.id}" />
							<s:hidden name="parent.id" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">名称</label>
					<div class="controls">
						<s:textfield name="name" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label tooltipster" title="排序号越大显示越靠上">排序号</label>
					<div class="controls">
						<s:textfield name="orderIndex" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {
        $(".form-edit-partner-category").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
               
                $form.find("input[name='parent.name']").data('data-dropdownselect-callback', function(json) {
                    $form.find("[name='parent.name']").val(json.name);
                    $form.find("[name='parent.id']").val(json.id);
                });

             }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
