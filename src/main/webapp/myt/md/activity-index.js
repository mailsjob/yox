 $(function() {
        $(".grid-md-activity").data("gridOptions", {
            url : WEB_ROOT + "/myt/md/activity!findByPage",
            colModel : [{
                label:'代码',
                name : 'activityCode',
                editable : true,
                width : 120,
                align : 'left',
                editoptions : {
                    dataInit : function(elem) {
                        var $grid = $(this);
                        var $elem = $(elem);
                        var rowdata = $grid.jqGrid('getSelectedRowdata');
                        if (rowdata && rowdata.id) {
                            $elem.attr("disabled", true);
                        }
                    }
                }
            },{
                label:'名称',
                name : 'activityName',
                width : 120,
                editable : true,
                align : 'left'
            },{
                label:'活动图片',
                name : 'img',
                sortable : false,
                search : false,
                width : 80,
                align : 'center',
                formatter : Biz.md5CodeImgViewFormatter,
                frozen : true,
                responsive : 'sm'
            }, {
                label:'开始时间',
                name : 'beginTime',
                editable : true,
                width : 180,
                sorttype : 'date',
                align : 'center'
            }, {
                label:'结束时间',
                name : 'endTime',
                editable : true,
                width : 180,
                sorttype : 'date',
                align : 'center'
            } ],
            editcol : 'activityName',
            editurl : WEB_ROOT + "/myt/md/activity!doSave",
            delurl : WEB_ROOT + "/myt/md/activity!doDelete",
            fullediturl : WEB_ROOT + "/myt/md/activity!inputTabs"
         });
    });