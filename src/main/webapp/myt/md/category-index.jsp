<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">商品分类</a></li>
		<li><a class="tab-default" data-toggle="tab" href="#tab-auto">标签</a></li>
		<li><a class="tab-default" data-toggle="tab" href="#tab-auto">时间轴</a></li>
		<li><a class="tab-default" data-toggle="tab" href="#tab-auto">主题标签</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search" data-grid-search=".grid-md-category-normal"
						action="#">
						<div class="form-group">
							<input type="text" name="search['CN_categoryCode_OR_name']" class="form-control input-large"
								placeholder="名称、分类号..." />
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-category-normal" data-grid="table"></table>
				</div>
			</div>
		</div>
		<div class="tab-pane">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-md-category-label" action="#">
						<div class="form-group">
							<input type="text" name="search['CN_categoryCode_OR_name']" class="form-control input-large"
								placeholder="名称、分类号..." />
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-category-label"></table>
				</div>
			</div>
		</div>
		<div class="tab-pane">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-md-category-timeline" action="#">
						<div class="form-group">
							<input type="text" name="search['CN_categoryCode_OR_name']" class="form-control input-large"
								placeholder="名称、分类号..." />
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-category-timeline"></table>
				</div>
			</div>
		</div>
		<div class="tab-pane">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-md-category-themelabel" action="#">
						<div class="form-group">
							<input type="text" name="search['CN_categoryCode_OR_name']" class="form-control input-large"
								placeholder="名称、分类号..." />
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-category-themelabel"></table>
				</div>
			</div>
		</div>
		
	</div>
</div>
<script src="${base}/myt/md/category-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>