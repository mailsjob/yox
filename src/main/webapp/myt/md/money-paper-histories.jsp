<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-commodity-comments" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-money-papery-histories" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-money-papery-histories").data("gridOptions", {
            url : "${base}/myt/md/money-papery-history!findByPage?search['EQ_moneyPaper.id']=id=<s:property value='#parameters.moneyPaperId'/>",
            colModel : [ {
                label:'订单号',
                name : 'boxOrder.orderSeq',
                align : 'center'
            }, {
                label:'订单客户',
                name : 'boxOrder.customerProfile.display',
                index:'boxOrder.customerProfile.trueName_OR_boxOrder.customerProfile.nickName',
                align : 'center'
            },{
                label:'使用金额',
                name : 'amount',
                width : 150
            }, {
                label:'说明',
                name : 'reason',
                width : 150
            }, {
                label : '时间',
                name : 'recordDatetime',
                align : 'center',
                stype : 'date'
            } ],
           	inlineNav : {
                add : true
            },
            multiselect : false
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>