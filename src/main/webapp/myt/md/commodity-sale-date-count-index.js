$(function() {
    $(".grid-md-commodity-sale-date-count").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/commodity-sale-date-count!findByPage",
        colModel : [{
            label : '商品主键',
            name : 'commodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '购买的商品',
            name : 'commodity.display',
            index : 'commodity.sku_OR_commodity.title',
            editable : true,
            width : 200,
            editrules : {
                required : true
            },
            editoptions : Biz.getGridCommodityEditOptions(),
            align : 'left'
        }, {
            label : '单个用户每日可购买量',
            name : 'dailyCanBuyCount',
            editable : true,
            width : 80,
            sorttype : 'number',
            align : 'right'
        }, {
            label : '日期',
            name : 'saleDate',
            editable : true,
            width : 200,
            sorttype : 'date',
            align : 'right'
        } ],
        grouping : true,
        groupingView : {
            groupField : [ 'commodity.display' ],
            groupOrder : [ 'asc' ],
            groupCollapse : false
        },
        editurl : WEB_ROOT + "/myt/md/commodity-sale-date-count!doSave",
        delurl : WEB_ROOT + "/myt/md/commodity-sale-date-count!doDelete",
    });   
});