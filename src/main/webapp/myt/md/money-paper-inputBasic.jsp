<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-md-money-paper-inputBasic"
	action="${base}/myt/md/money-paper!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i>保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body control-label-sm">
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">代码</label>
					<div class="controls">
						<s:textfield name="code" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">密码</label>
					<div class="controls">
						<s:textfield name="password"  />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否开发票</label>
					<div class="controls">
						<s:radio name="receiptedInvoice" list="#application.enums.booleanLabel" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">状态</label>
					<div class="controls">
					<s:select name="paperStatus" list="#application.enums.paperStatusEnum" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">名称</label>
					<div class="controls">
						<s:textfield name="title" />
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">销售客户</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-customer-profile"></i>
							<s:textfield name="customerProfile.display" />
							<s:hidden name="customerProfile.id"  />
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">分类</label>
					<div class="controls">
					<s:select name="paperCategory" list="#application.enums.paperCategoryEnum" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">模式</label>
					<div class="controls">
					<s:radio name="paperMode" list="#{'P':'个人'}" />
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">面额</label>
					<div class="controls">
						<s:textfield name="faceValue" readonly="%{notNew}"/>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">余额</label>
					<div class="controls">
						<s:textfield name="restAmount" readonly="%{notNew}"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">生效时间</label>
					<div class="controls">
					<s3:datetextfield name="beginTime"></s3:datetextfield>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">过期时间</label>
					<div class="controls">
						<s3:datetextfield name="expireTime"></s3:datetextfield>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-ajaxify-reload="_closest-ajax-container">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
$(function() {
     $(".form-md-money-paper-inputBasic").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                Biz.setupCustomerProfileSelect($form);
            }
    });
});
</script>

<%@ include file="/common/ajax-footer.jsp"%>