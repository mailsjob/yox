<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-commodity-recommend-fittings" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-commodity-recommend-fittings").data("gridOptions", {
            url : "${base}/myt/md/commodity!recommendFittings?id=<s:property value='#parameters.id'/>",
           	colModel : [ {
                name : 'dest.id',
                hidden : true,
                hidedlg : true,
                editable : true
            }, {
                label : '推荐配件',
                name : 'dest.display',
                editable : true,
                editrules : {
                    required : true
                },
                editoptions : Biz.getGridCommodityEditOptions(),
                align : 'left'
            }, {
                label : '排序号',
                name : 'orderIndex',
                editable : true,
                width : 200
            }],
        	editurl : "${base}/myt/md/recommend-fitting!doSave?source.id=<s:property value='#parameters.id'/>",
            delurl : "${base}/myt/md/recommend-fitting!doDelete"
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>