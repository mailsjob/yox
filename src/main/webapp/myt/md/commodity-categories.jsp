<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-commodity-price" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-commodity-categories" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-commodity-categories").data("gridOptions", {
            url : "${base}/myt/md/commodity!getCategories?id=<s:property value='#parameters.id'/>",
            colNames : [ '名称', 'id','勾选'],
            colModel : [ {
                name : 'name',
                align : 'left'
            }, {
                name : 'id',
                hidden : true
            } ,{
                name : 'operation',
                align : 'center',
                edittype : 'checkbox',
                editable : true
            }],
            cmTemplate : {
                sortable : false
            },
            height : 400,
            pager : false,
            toppager : false,
            treeGrid : true,
            treeGridModel : 'adjacency',
            'cellEdit': true, 
            'cellsubmit': 'remote',
             cellurl: '<c:url value="/finalgrades/create/"/>' + id
        });
    });
  
</script>
<%@ include file="/common/ajax-footer.jsp"%>