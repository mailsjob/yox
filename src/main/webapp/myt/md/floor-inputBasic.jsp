<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-floor"
      action="${base}/myt/md/floor!doSave" method="post">
    <s:hidden name="id"/>
    <s:hidden name="version"/>
    <s:token/>
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-cb-lottery-award-list">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">排序号</label>
                    <div class="col-md-8">
                        <s:textfield name="orderIndex"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">适用阶段</label>
                    <div class="col-md-8">
                        <s:textfield name="fitStage"/>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">适用起始月</label>
                    <div class="col-md-8">
                        <s:textfield name="fitStart"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">适用截止月</label>
                    <div class="col-md-8">
                        <s:textfield name="fitEnd"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">楼层类型</label>
                    <div class="col-md-8">
                        <s:select name="floorType" list="#application.enums.floorTypeEnum"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">楼层适用类型</label>
                    <div class="col-md-8">
                        <s:select name="siteFlag" list="#application.enums.siteFlagEnum"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2">LinkCms</label>
                    <div class="col-md-10">
                        <s:textarea name="linkCms" rows="3"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">楼层图片</label>
                    <div class="col-md-8">
                        <s:hidden name="floorImg" data-singleimage="true"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">背景图片</label>
                    <div class="col-md-8">
                        <s:hidden name="bgPic" data-singleimage="true"/>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">楼层颜色代码</label>
                    <div class="col-md-8">
                        <s:textfield name="floorColor"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">地区</label>
                    <div class="controls region">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-region"></i>
                            <s:textfield name="region.display"/>
                            <s:hidden name="region.id"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    </div>

    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-cb-lottery-win-list">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script type="text/javascript">
    var form;
    $(function () {
        form = $("form .form-edit-floor");
        form.picsEditor = KindEditor.editor({
            allowFileManager: false,
            uploadJson: '<s:property value="kindEditorImageUploadUrl"/>'
        });

    });

    function uploadPic(btn) {
        var picTR = $(btn).closest("tr");
        form.picsEditor.loadPlugin('image', function () {
            form.picsEditor.plugin.imageDialog({
                clickFn: function (url, title, width, height, border, align) {
                    var code = url.split('=')[1]
                    picTR.find("input[name$='pic']").val(code);
                    form.picsEditor.hideDialog();
                }
            })

        })
    }
    function uploadIcon(btn) {
        var picTR = $(btn).closest("tr");
        form.picsEditor.loadPlugin('image', function () {
            form.picsEditor.plugin.imageDialog({
                clickFn: function (url, title, width, height, border, align) {
                    var code = url.split('=')[1]
                    picTR.find("input[name$='icon']").val(code);
                    form.picsEditor.hideDialog();
                }
            })

        })
    }

    $(".form-edit-floor").data("formOptions", {
        bindEvents: function () {
            var $inputform = $(this);
            $inputform.find(".fa-select-region").click(function () {
                $inputform.popupDialog({
                    url: WEB_ROOT + '/myt/md/region!forward?_to_=proSingleSelection',
                    title: '选取地区',
                    callback: function (result) {
                        $inputform.setFormDatas({
                            'region.id': result.id,
                            'region.display': result.display
                        });
                    }
                })
            });
        }
    });
</script>
<%@ include file="/common/ajax-footer.jsp" %>