 $(function() {
        $(".grid-md-ad").data("gridOptions", {
            url : WEB_ROOT + "/myt/md/ad!findByPage",
            colModel : [{
                label:'位置代码',
                name : 'adPositionCode',
                editable : true,
                width : 80,
                align : 'left',
                editoptions : {
                    dataInit : function(elem) {
                        var $grid = $(this);
                        var $elem = $(elem);
                        var rowdata = $grid.jqGrid('getSelectedRowdata');
                        if (rowdata && rowdata.id) {
                            $elem.attr("disabled", true);
                        }
                    }
                }
            },{
                label:'广告名称',
                name : 'adName',
                width : 120,
                editable : true,
                align : 'left'
            },{
                label : '图片',
                name : 'adBannerPic',
                sortable : false,
                search : false,
                width : 80,
                align : 'center',
                formatter : Biz.md5CodeImgViewFormatter,
                frozen : true,
                responsive : 'sm'
            },{
                label : '背景图片',
                name : 'bgImgUrl',
                sortable : false,
                search : false,
                width : 80,
                align : 'center',
                formatter : Biz.md5CodeImgViewFormatter,
                frozen : true,
                responsive : 'sm'
            },{
                label:'排序号',
                name : 'orderIndex',
                width : 120,
                editable : true,
                align : 'left'
            }, {
                label : '类型',
                name : 'adObjectType',
                width : 120,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('adObjectTypeEnum')
                }
            },{
                label:'链接',
                name : 'url',
                width : 120,
                align : 'left'
            },{
                label : '对象主键',
                name : 'adObjectSid',
                /*hidden : true,
                hidedlg : true,*/
                width:'100'
            }, {
                label : '对象名',
                name : 'adObjectDisplay',
                align : 'left',
                editoptions : {
                     dataInit : function(elem) {
                        var $grid = $(this);
                        var $elem = $(elem);
                       
                        $elem.wrap('<div class="input-icon right"/>');
                        $elem.before('<i class="fa fa-ellipsis-horizontal fa-select-adObject"></i>');
                        var selectAdObject = function(item) {
                            // 强制覆盖已有值
                            $grid.jqGrid("setEditingRowdata", {
                                'adObjectSid' : item.id,
                                'adObjectDisplay' : item.display
                            });

                        }
                        $grid.find(".fa-select-adObject").click(function() {
                            var rowdata = $grid.jqGrid("getEditingRowdata");
                          
                            if(rowdata['adObjectType']=='COMMODITY'){
                                $(this).popupDialog({
                                    url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                                    title : '选取商品',
                                    callback : selectAdObject
                                })
                            }else if(rowdata['adObjectType']=='THEME'){
                                $(this).popupDialog({
                                    url : WEB_ROOT + '/myt/md/theme!forward?_to_=selection',
                                    title : '选取主题',
                                    callback : selectAdObject
                                })
                            }else if(rowdata['adObjectType']=='SMZDM'){
                                $(this).popupDialog({
                                    url : WEB_ROOT + '/myt/md/smzdm!forward?_to_=selection',
                                    title : '选取每日推荐',
                                    callback : selectAdObject
                                })
                            }else if(rowdata['adObjectType']=='ACTIVITY'){
                                $(this).popupDialog({
                                    url : WEB_ROOT + '/myt/md/activity!forward?_to_=selection',
                                    title : '选取活动',
                                    callback : selectAdObject
                                })
                            }
                            
                        });
                    }
                },
            }, {
                label : '是否必填',
                name : 'enable',
                editable : true,
                edittype : "checkbox"
            }],
            editcol : 'adName',
            editurl : WEB_ROOT + "/myt/md/ad!doSave",
            delurl : WEB_ROOT + "/myt/md/ad!doDelete",
            fullediturl : WEB_ROOT + "/myt/md/ad!inputTabs"
         });
    });