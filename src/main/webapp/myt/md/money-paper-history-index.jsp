<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="row search-form-default">
	<div class="col-md-12">
		<form action="#" method="get" class="form-inline form-validation  form-search-init"
			data-grid-search=".grid-md-money-paper-history">
			<div class="input-group">
				<div class="input-cont">
					<input type="text"
						name="search['CN_moneyPaper.code_OR_moneyPaper.title_OR_boxOrder.orderSeq']"
						class="form-control" placeholder="礼品卡代码、名称，订单号...">
				</div>
				<span class="input-group-btn">
					<button class="btn green" type="submmit">
						<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
					</button>
					<button class="btn default hidden-inline-xs" type="reset">
						<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
					</button>
				</span>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-md-money-paper-history"></table>
	</div>
</div>
<script src="${base}/myt/md/money-paper-history-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
