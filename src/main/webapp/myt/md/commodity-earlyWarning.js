$(function() {
    $(".grid-md-commodity-early-warning").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/commodity!getContainSensitiveCommodityList",
        colModel : [ {
            label : '主图',
            name : 'smallPic',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '商品编码',
            name : 'sku',
            align : 'left',
            width : 80
        }, {
            label : '实物条码',
            name : 'barcode',
            editable : true,
            align : 'center',
            width : 100
        }, {
            label : '商品名称',
            name : 'title',
            sortable : false,
            editable : true,
            width : 200,
            edittype : 'textarea',
            align : 'left'
        }, {
            label : '品牌',
            name : 'brand.id',
            width : 100,
            align : 'left',
            formatter : "select",
            editable : true,
            edittype : 'select',
            editoptions : {
                value : Biz.getBrandDatas()
            },
            responsive : 'sm'
        }, {
            label : '状态',
            name : 'commodityStatus',
            align : 'center',
            width : 50,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('commodityStatusEnum')
            },
            responsive : 'sm'
        }, {
            label : '价格',
            name : 'price',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : '分类主键',
            name : 'category.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '分类',
            name : 'category.display',
            index : 'category.categoryCode_OR_category.categoryCode',
            width : 120,
            sortable : false,
            editable : true,
            align : 'left',
            editrules : {
            //required : true
            },
            editoptions : {
                dataInit : function(elem, opt) {
                    $(elem).treeselect({
                        url : WEB_ROOT + "/myt/md/category!categoryList"
                    });
                }
            },
            responsive : 'sm'
        }, {
            label : '排序号',
            name : 'recommendRank',
            editable : true,
            width : 80,
            align : 'center',
            responsive : 'sm'
        }, {
            label : '周期购标识',
            name : 'circle',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        }, {
            label : '商家',
            name : 'partner.display',
            index : 'partner',
            hidden : true,
            align : 'left',
            responsive : 'sm'
        }, {
            label : '商品来源',
            name : 'source',
            index : 'source_OR_sourceUrl',
            align : 'left',
            hidden : true,
            width : 100,
            formatter : function(cellValue, options, rowdata, action) {
                return (rowdata.source ? "<a href='" + rowdata.sourceUrl + "' target='_blank'>" + rowdata.source + "</a>" : "");
            },
            responsive : 'lg'
        } ],
        postData : {
            "search['FETCH_brand']" : "LEFT",
            "search['FETCH_partner']" : "LEFT",
            "search['FETCH_category']" : "LEFT"
        },
        inlineNav : {
            add : false
        },
        editcol : 'sku',
        editurl : WEB_ROOT + "/myt/md/commodity!doSave",
        delurl : WEB_ROOT + "/myt/md/commodity!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/commodity!inputTabs"
    });
});