<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row search-form-default">
		<div class="col-md-12">
			<form method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-md-brand-selection"
				action="#">
				<div class="form-group">
					<s:textfield name="search['CN_title']" cssClass="form-control input-xlarge" placeholder="品牌名称"
						value="%{#parameters.keyword}" />
				</div>
				<div class="form-group">
					<div class="btn-group">
						<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-angle-down"></i>
						</button>
					</div>
				</div>
				<button class="btn default hidden-inline-xs" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-md-brand-selection"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-brand-selection").data("gridOptions", {
            url : "${base}/myt/md/brand!findByPage",
            colNames : [ '周期购', '排序号', '标题', '同义词', '标语', '图片', '品牌链接', '授权开始日期 ', '授权截止日期', '显示' ],
            colModel : [ {
                name : 'circle',
                edittype : 'checkbox',
                align : 'center'
            }, {
                name : 'orderIndex',
                width : 60,
                editable : true,
                align : 'center'
            }, {
                name : 'title',
                width : 150,
                align : 'left'
            }, {
                name : 'synonyms',
                width : 150,
                align : 'left'
            }, {
                name : 'slogan',
                width : 150,
                align : 'left'
            }, {
                name : 'smallPic',
                formatter : Biz.md5CodeImgViewFormatter,
                width : 80,
                align : 'left'
            }, {
                name : 'brandUrl',
                align : 'left',
                formatter : function(cellValue, options, rowdata, action) {
                    if (cellValue) {
                        if (rowdata.brandUrl.startsWith("http:")) {
                            return '<a href="'+rowdata.brandUrl+'" target="_blank">' + rowdata.brandUrl + '</a>';
                        } else {
                            return '<a href="http://'+rowdata.brandUrl+'" target="_blank">' + rowdata.brandUrl + '</a>';
                        }
                    } else {
                        return "";
                    }

                }
            }, {
                name : 'startDate',
                width : 120,
                sorttype : 'date',
                align : 'center'
            }, {
                name : 'endTime',
                width : 120,
                sorttype : 'date',
                align : 'center'
            }, {
                name : 'display',
                hidden : true,
                hidedlg : true
            } ],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>