<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable">
	<ul class="nav nav-tabs ">
		<li class="active"><a data-toggle="tab" href="#tab-auto" class="tab-province">省/直辖市</a></li>
		<li class="fade"><a data-toggle="tab" href="#tab-auto" class="tab-city"></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active tab-pane-province"></div>
		<div class="tab-pane tab-pane-city"></div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        var regionDatas = Biz.getRegionDatas();
        $(".tab-pane-province:empty").each(function() {

            var $tabbable = $(this).closest(".tab-content").parent();

            var $province = $(this);
            var html = [];
            $.each(regionDatas, function(i, item) {
                var name = item.name;
                if (item.pinyinShort) {
                    name = item.pinyinShort.substring(0, 1) + "." + name;
                }
                html.push('<div class="col-md-2"><button id="'+item.id+'" class="btn btn-link" data-toggle="button" type="button" data-name="'+item.name+'">' + name + '</button></div>');
            });
            $province.html(html.join(""));
            $province.on('click', 'button', function() {

                var $button = $(this);
                $province.find(".btn").removeClass("default");
                $button.addClass("default");

                var curId = $(this).attr("id");
                var provinceName = $button.attr("data-name");
                var $containerCallback = $button.closest(".container-callback");
                var callback = $containerCallback.data("callback");
                if (callback) {
                    callback.call($button, {
                        id : curId,
                        display : provinceName
                    });
                }

                $tabbable.find(".nav .tab-province").html(provinceName);
                var $tabPaneCity = $(this).closest(".tab-content").find(".tab-pane-city");

                var cityRegionDatas = null;
                $.each(regionDatas, function(i, item) {
                    if (item.id == curId) {
                        cityRegionDatas = item.children;
                        return false;
                    }
                })
                var cityHtml = [];
                $.each(cityRegionDatas, function(i, item) {
                    var name = item.name;
                    if (item.pinyinShort) {
                        name = item.pinyinShort.substring(0, 1) + "." + name;
                    }
                    cityHtml.push('<div class="col-md-3"><button id="'+item.id+'" class="btn btn-link" type="button" data-name="'+item.name+'">' + name + '</button></div>');
                });
                $tabPaneCity.html(cityHtml.join(""));
                var $tabCity = $(this).closest(".tab-content").parent().find(".nav .tab-city");
                $tabCity.html("市");
                $tabCity.parent("li").removeClass("fade");
                $tabCity.click();

                $tabPaneCity.on('click', 'button', function(close) {
                    var cityName = $(this).attr("data-name");
                    var cityId = $(this).attr("id");
                    if (callback) {
                        callback.call($button, {
                            id : cityId,
                            display : provinceName + " " + cityName
                        });
                        if ($(this).attr("init-select")) {
                            $(this).removeAttr("init-select");
                        } else {
                            $containerCallback.hide();
                        }
                    }

                    $tabCity.html(cityName);
                    $tabPaneCity.find(".btn").removeClass("default");
                    $(this).addClass("default");
                })
            });

            var selected = "<s:property value='#parameters.selected'/>";
            if (selected) {
                var splits = selected.split(" ");

                $.each(splits, function(i, item) {
                    var name = $.trim(item);
                    var $li = $tabbable.find(".nav > li").eq(i);
                    $li.removeClass("fade");
                    $li.children("a").html(name);
                    setTimeout(function() {
                        var $btn = $tabbable.find(".tab-content > div.tab-pane").eq(i).find(".btn[data-name='" + name + "']");
                        $btn.attr("init-select", true);
                        $btn.click();
                    }, 1000);
                });

            }

        });

    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>