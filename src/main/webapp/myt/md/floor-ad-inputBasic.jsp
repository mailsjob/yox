<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-floor-ad"
	action="${base}/myt/md/floor-ad!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-floor-ad">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">选择楼层</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-floor"></i>
							<s:textfield name="floor.displayLabel" />
							<s:hidden name="floor.id" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否可用</label>
					<div class="controls">
						<s:radio name="enable" list="#application.enums.booleanLabel"></s:radio>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">图片</label>
					<div class="col-md-8">
						<s:hidden name="pic" data-singleimage="true" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">排序号</label>
							<div class="col-md-8">
								<s:textfield name="name" />
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">排序号</label>
							<div class="col-md-8">
								<s:textfield name="orderIndex" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">楼层下广告方位</label>
							<div class="col-md-8">
								<s:select name="position" list="#application.enums.floorAdPositionEnum" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">楼层适用类型</label>
							<div class="col-md-8">
								<s:select name="adType" list="#application.enums.floorAdTypeEnum" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">背景图片</label>
					<div class="col-md-8">
						<s:hidden name="bgPic" data-singleimage="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">跳转目标</label>
							<div class="col-md-8">
								<s:textfield name="target" />
							</div>
						</div>
					</div>
				</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-floor-ad">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
$(function() {
     $(".form-edit-floor-ad").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                $form.find(".fa-select-floor").click(function() {
                    $(this).popupDialog({
                        url :'${base}/myt/md/floor!forward?_to_=selection',
                        title : '选取楼层',
                        callback : function(rowdata) {
                            $form.setFormDatas({
                                'floor.id' : rowdata.id,
                                'floor.displayLabel' : rowdata.displayLabel
                            });

                        }
                    })
                });
            }
    });
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>