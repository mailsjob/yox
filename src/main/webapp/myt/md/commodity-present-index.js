$(function() {
    $(".grid-myt-md-commodity-present").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/commodity-present!findByPage',
        colModel : [ {
            label : '商品主键',
            name : 'commodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '购买的商品',
            name : 'commodity.display',
            index : 'commodity.sku_OR_commodity.title',
            editable : true,
            width : 200,
            editrules : {
                required : true
            },
            editoptions : Biz.getGridCommodityEditOptions(),
            align : 'left'
        }, {
            label : '赠品主键',
            name : 'presentCommodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '赠品商品',
            name : 'presentCommodity.display',
            index : 'presentCommodity.sku_OR_presentCommodity.title',
            editable : true,
            width : 200,
            editrules : {
                required : true
            },
            editoptions : Biz.getGridCommodityEditOptions(),
            align : 'left'
        }, {
            label : '赠品数',
            name : 'quantity',
            editable : true,
            width : 60,
            sorttype : 'number',
            align : 'right'
        }, {
            label : '开始日期',
            editable : true,
            name : 'beginDate',
            sorttype:'date',
            width : 100
        }, {
            label : '结束日期',
            editable : true,
            name : 'endDate',
            sorttype:'date',
            width : 100
        }, {
            label : '是否叠加赠送',
            name : 'canStacked',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        }, {
            label : '支持类型',
            stype : 'select',
            name : 'givenType',
            editable : true,
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('givenTypeEnum')
            },
            width : 80
        }, {
            label : '启用',
            name : 'enable',
            edittype : 'checkbox',
            editable : true,
            align : 'center',
            editoptions : {
                dataInit : function(elem) {
                    var $elem = $(elem);
                    $elem.attr("checked","checked");
                }
            },
        } ],
        grouping : true,
        groupingView : {
            groupField : [ 'commodity.display' ],
            groupOrder : [ 'asc' ],
            groupCollapse : false
        },
        editurl : WEB_ROOT + '/myt/md/commodity-present!doSave',
        delurl : WEB_ROOT + '/myt/md/commodity-present!doDelete'
    });
});