<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-myt-md-single-goods-selection">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_commodity.title_OR_commodity.sku_OR_title']" 
								class="form-control" placeholder="标题、SKU...">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default hidden-inline-xs" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-single-goods-selection"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function() {
    $(".grid-myt-md-single-goods-selection").data("gridOptions", {
        url : '${base}/myt/md/single-goods!findByPage',
        colModel : [ {
            label : '橱窗图',
            name : 'pic',
            width : 80,
            editable: false,
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '流水号',
            name : 'id'                          
        }, {
            label : '关联商品',
            name : 'commodity.display',
            index : 'commodity.title_OR_commodity.sku',
            width : 200,
            editable: false,
            align : 'left'
        }, {
            label : '商品标题',
            name : 'title',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '单价',
            name : 'price',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '已售数量',
            name : 'soldQuantity',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '最多购买数量',
            name : 'userBoughtMaxQuatity',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '单日限购数',
            name : 'dailyUserCanBuyQuatity',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '最大可售数量',
            name : 'maxSellQuantity',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '起售日期',
            name : 'sellBeginTime',
            width : 150,
            formatter: 'date',
            editable: true,
            align : 'center'
        }, {
            label : '停售日期',
            name : 'sellEndTime',
            width : 150,
            formatter: 'date',
            editable: true,
            align : 'center'
        }, {
            label : '单品描述',
            name : 'htmlContent',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }],
        rowNum : 10,
        multiselect : false,
        toppager : false,
        onSelectRow : function(id) {
            var $grid = $(this);
            var $dialog = $grid.closest(".modal");
            $dialog.modal("hide");
            var callback = $dialog.data("callback");
            if (callback) {
                var rowdata = $grid.jqGrid("getRowData", id);
                rowdata.id = id;
                callback.call($grid, rowdata);
            }
        }
    });
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>
