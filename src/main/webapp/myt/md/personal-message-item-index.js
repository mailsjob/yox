 $(".grid-md-personal-message-item").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/personal-message-item!findByPage',
        colModel : [ {
            label : '接收者',
            name : 'targetUser.display',
            index:'targetUser.signinid',
            width : 80,
            align : 'left'
        },{
            label : '时间',
            name : 'createdDate',
            width : 120,
            align : 'center'
        },{
            label : '消息标题',
            name : 'personalMessage.title',
            width : 120,
            align : 'left'
        },{
            label : '消息内容',
            name : 'personalMessage.contents',
            width : 200,
            align : 'left'
        }, {
            label : '首次查看时间',
            name : 'firstReadTime',
            width : 150,
            formatter: 'date',
            align : 'center'
        }, {
            label : '最后查看时间',
            name : 'lastReadTime',
            width : 150,
            formatter: 'date',
            align : 'center'
        }, {
            label : '查看次数',
            name : 'totalReadTimes',
            width : 50,
            align : 'center'
        }, {
            label : '是否可用',
            name : 'enable',
            width : 50,
            formatter : 'checkbox',
            align : 'center'
        }],
        sortorder : "desc",
        sortname : "createdDate",
        multiselect:true,
        delurl : WEB_ROOT + '/myt/md/personal-message-item!doDelete'
       
    });