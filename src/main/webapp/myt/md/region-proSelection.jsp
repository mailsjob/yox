<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable">
    <ul class="nav nav-tabs ">
        <li class="active"><a data-toggle="tab" href="#tab-auto" class="tab-province">省/直辖市</a></li>
        <input type="button" class="btn" style="margin-top:5px" onclick="callPros()" value="确认">
        <input type="button" class="btn" style="margin-top:5px" onclick="choseAll()" value="全选">
    </div>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active tab-pane-province"></div>
    </div>
</div>
<script type="text/javascript">
function callPros(){
    // 选中区域
    var $check = $("input[name='box']:checked");
    var arrayId= new Array();
    var arrayDis= new Array();
    $check.each(function(){
        arrayId.push($(this).attr("id"));
        arrayDis.push($(this).attr("data-name"));
        $(this).attr("checked",false);
        });
    var $containerCallback = $check.closest(".container-callback");
    var callback = $containerCallback.data("callback");
    if (callback) {
        callback.call($check, {
            id : arrayId.toString(),
            display : arrayDis.toString()
        });
        if ($(this).attr("init-select")) {
            $(this).removeAttr("init-select");
        } else {
            $containerCallback.hide();
        }
    }
}
var checked = false;
function choseAll(){
    // 选中区域
    var $checkBox = $("input[name='box']");
    if (checked) {
    $checkBox.each(function(){
        $(this).attr("checked",false);
        });
        checked = false;
    } else {
        $checkBox.each(function(){
            $(this).attr("checked",true);
            });
        checked = true;
    }
}
$(function() {
var regionDatas = Biz.getRegionDatas();
$(".tab-pane-province:empty").each(
    function() {
        var $tabbable = $(this).closest(".tab-content").parent();
        var $province = $(this);
        var html = [];
        $.each(regionDatas, function(i, item) {
            var name = item.name;
            if (name == "香港" || name == "澳门" || name == "新疆" || name == "西藏" || name == "台湾" ) {
            } else{
                if (item.pinyinShort) {
                    name = item.pinyinShort.substring(0, 1) + "." + name;
                }
                html.push('<div class="col-md-3"><input name="box" id="' +
                        item.id + '"type="checkbox" data-name="' +
                        item.name + '">' + name + '</input></div>');
            }
        });
        $province.html(html.join(""));

        var selected = "<s:property value='#parameters.selected'/>";
        if (selected) {
            var splits = selected.split(",");

            $.each(splits, function(i, item) {
                var name = $.trim(item);
                var $checbox = $tabbable.find(".tab-content > div.tab-pane").find("[data-name='" + name + "']");
                $checbox.attr("checked", true);
            });

        }
    });
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>