<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<style>

.controls-userLabels label.checkbox-inline {
	width: 150px;
	margin-left: 10px;
}
</style>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-md-personal-message-inputBasic"
	action="${base}/myt/md/personal-message!doSave" method="post" 
	data-editrulesurl="${base}/myt/md/personal-message!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-personal-message-index">
			<i class="fa fa-check"></i> 立即发布
		</button>
	</div>
	<div class="form-body">
        
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">发布用户</label>
					<div class="controls">
		                <s:select name="publishUser.id" list="usersMap" />
					</div>
				</div>
            </div>
        </div> 
        <%-- <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">发布时间</label>
					<div class="controls">
		                <s3:datetextfield name="publishTime" format="date"/>                  
					</div>
				</div>
            </div>
        </div>
        --%>
        <div class="row">
            <div class="col-md-8">
				<div class="form-group">
					<label class="control-label">标题 </label>
					<div class="controls">
		                <s:textfield name="title" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
				<div class="form-group">
					<label class="control-label">通知内容</label>
					<div class="controls">
		                <s:textarea name="contents" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">目标用户</label>
					<div class="controls">
						<s:textfield name="targetUsers" onclick="$(this).closest('div.row').next('div.row').toggle()" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="display: none">
			<div class="col-md-12">
				<div class="form-group form-group-selectionTargetUsers">
					<label class="control-label">候选目标用户<input type="checkbox" /></label>
					<div class="controls controls-userLabels">
						<s:checkboxlist list="targetUsersMap" name="selectionTargetUsers" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-personal-message-index">
			<i class="fa fa-check"></i> 立即发布
		</button>
	</div>
</form>
<script src="${base}/myt/md/personal-message-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>