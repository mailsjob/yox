$(function () {
    $(".form-md-gift-paper-inputBasic").data("formOptions", {
        bindEvents: function () {
            if ($("#faceValue").val() != "") {
                $("#face").show();
                $("#choose").hide();
                $("#func").hide();
                $("#funcPar").hide();
                $("select[name='paperStyle']").val("face");
            } else if ($("input[name='funcCode']").val() != "") {
                $("#face").hide();
                $("#choose").hide();
                $("#func").show();
                $("#funcPar").show();
                $("select[name='paperStyle']").val("func");
            } else {
                $("#face").hide();
                $("#func").hide();
                $("#funcPar").hide();
            }
            var $form = $(this);
            Biz.setupCustomerProfileSelect($form);
            $form.find(".fa-select-paper-template").click(function () {
                $(this).popupDialog({
                    url: WEB_ROOT + '/myt/md/gift-paper!forward?_to_=selection',
                    title: '选取模板',
                    callback: function (rowdata) {
                        $form.setFormDatas({
                            'title': rowdata.title,
                            'paperTemplateCode': rowdata.paperTemplateCode,
                            'faceValue': rowdata.faceValue,
                            'beginTime': rowdata.beginTime,
                            'expireTime': rowdata.expireTime,
                            'paperSource': rowdata.paperSource,
                            'funcCode': rowdata.funcCode,
                            'funcParameter': rowdata.funcParameter,
                        });
                    }
                })
            });
        }
    });
});

function createGpBatch() {
    if ($.isEmptyObject($("#counts")) || $("#counts").val() == '') {
        alert("请输入生成优惠券件数");
        return;
    }
    jQuery.ajax({
        type: "post",
        data: {
            paperTemplateCode: $("#paperTemplateCode").val(),
            title: $("#title").val(),
            faceValue: $("#faceValue").val(),
            beginTime: $("#beginTime").val(),
            expireTime: $("#expireTime").val(),
            paperStatus: $("#paperStatus").val(),
            paperSource: $("#paperSource").val(),
            funcCode: $("#funcCode").val(),
            funcParameter: $("#textarea").val(),
            counts: $("#counts").val()
        },
        url: WEB_ROOT + "/myt/md/gift-paper!creatGpBatch"
    });
}

function choosePater(paperStyle) {
    if (paperStyle == "face") {
        $("#face").show();
        $("#func").hide();
        $("#funcPar").hide();
    } else if (paperStyle == "func") {
        $("#face").hide();
        $("#func").show();
        $("#funcPar").show();
    }
}