<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-activity-block-r2-object"
	action="${base}/myt/md/activity-block-r2-object!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-activity-block-r2-object">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">选择活动块</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-activityBlock"></i>
							<s:textfield name="activityBlock.display" />
							<s:hidden name="activityBlock.id" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否可用</label>
					<div class="controls">
						<s:radio name="enable" list="#application.enums.booleanLabel"></s:radio>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">对象类型</label>
					<div class="controls">
						<s:select name="objectType" list="#application.enums.blockObjectTypeEnum" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">关联对象</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-object"></i>
							<s:textfield name="objectDisplay" />
							<s:hidden name="objectSid" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">图片</label>
					<div class="col-md-8">
						<s:hidden name="pic" data-singleimage="true" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {
        $(".form-edit-activity-block-r2-object").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                $form.find(".fa-select-activityBlock").click(function() {
                    $(this).popupDialog({
                        url : '${base}/myt/md/activity-block!forward?_to_=selection',
                        title : '选取活动块',
                        callback : function(rowdata) {
                            $form.setFormDatas({
                                'activityBlock.id' : rowdata.id,
                                'activityBlock.display' : rowdata.display
                            });

                        }
                    })
                });

                var $objectType = $(".form-edit-activity-block-r2-object select[name='objectType']");
                $objectType.change(function() {
                    var objectType = $objectType.val();
                    $form.find("input[name='objectSid']").val("");
                    $form.find("input[name='objectDisplay']").val("");
                })
                $form.find(".fa-select-object").click(function() {
                    var objectType = $objectType.val();
                    if (objectType == 'COMMODITY') {
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                            title : '选取商品',
                            callback : function(rowdata) {
                                $form.setFormDatas({
                                    'objectSid' : rowdata.id,
                                    'objectDisplay' : rowdata.display
                                });

                            }
                        })
                    } else if (objectType == 'THEME') {
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/theme!forward?_to_=selection',
                            title : '选取主题',
                            callback : function(rowdata) {
                                $form.setFormDatas({
                                    'objectSid' : rowdata.id,
                                    'objectDisplay' : rowdata.display
                                });

                            }
                        })
                    } else if (objectType == 'ACTIVITY') {
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/activity!forward?_to_=selection',
                            title : '选取活动',
                            callback : function(rowdata) {
                                $form.setFormDatas({
                                    'objectSid' : rowdata.id,
                                    'objectDisplay' : rowdata.display
                                });

                            }
                        })
                    }
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
