<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-commodity-price" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-commodity-price" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="div-md-commodity-price-input-batch"></div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-commodity-price").data("gridOptions", {
            url : "${base}/myt/md/commodity!getCommodityPrices?id=<s:property value='#parameters.id'/>",
            colNames : [ '计划年月', '当月价格', '+1月', '+2月', '+3月', '+4月', '+5月', '+6月', '+7月', '+8月', '+9月', '+10月', '+11月', '最低价', '是否默认' ],
            colModel : [  {
                name : 'priceMonthYear',
                editable : true,
                sorttype : 'date',
                dateformat:'yyyy-mm',
               	width : 80,
                align : 'center'
            }, {
                name : 'm0Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm1Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm2Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm3Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm4Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm5Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm6Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm7Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm8Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm9Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm10Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'm11Price',
                editable : true,
                width : 60,
                align : 'center'
            }, {
                name : 'lowestPrice',
                width : 60,
                align : 'center'
            }, {
                name : 'defaultPrice',
                edittype : 'checkbox',
                editable : true,
                align : 'center',
            } ],
           /*  operations : function(itemArray) {
                var $grid = $(this);
             	var $inputBatch = $('<li data-position="single"><a href="javascript:;"><i class="fa fa-copy"></i> 批量新增</a></li>');
                $inputBatch.children("a").bind("click", function(e) {
                    e.preventDefault();
                    var url="${base}/myt/md/commodity-price!forward?_to_=input-batch&commoditySid=<s:property value='#parameters.id'/>";
                    $(".div-md-commodity-price-input-batch").ajaxGetUrl(url);
                    $("#commodityPriceRuleType").focus();
                });
                itemArray.push($inputBatch);
            }, */
            editurl : "${base}/myt/md/commodity-price!doSave?commodity.id=<s:property value='#parameters.id'/>",
            delurl : "${base}/myt/md/commodity-price!doDelete"
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>