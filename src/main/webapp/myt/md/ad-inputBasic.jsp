<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-md-ad-inputBasic" action="${base}/myt/md/ad!doSave"
	method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-ad">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		 <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否可用</label>
					<div class="controls">
						<s:radio name="enable" list="#application.enums.booleanLabel" ></s:radio>
		             </div>
				</div>
            </div>
             <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">排序号</label>
					<div class="controls">
		                <s:textfield name="orderIndex" />
					</div>
				</div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">代码</label>
					<div class="controls">
						<s:textfield name="adPositionCode" />
		             </div>
				</div>
            </div>
             <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">名称</label>
					<div class="controls">
		                <s:textfield name="adName" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">广告图片</label>
					<div class="col-md-8">
						<s:hidden name="adBannerPic" data-singleimage="true" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">广告背景图片</label>
					<div class="col-md-8">
						<s:hidden name="bgImgUrl" data-singleimage="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
        	<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">邮件图片</label>
					<div class="col-md-8">
						<s:hidden name="emailPic" data-singleimage="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">背景色</label>
					<div class="controls">
		                <s:textfield name="bgColor" />
					</div>
				</div>
            </div>
            </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">对象类型</label>
					<div class="controls">
					<s:select name="adObjectType" list="#application.enums.adObjectTypeEnum" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">关联对象</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-ad-object"></i>
							<s:textfield name="adObjectDisplay" />
							<s:hidden name="adObjectSid" />
						</div>
					</div>
				</div>
			</div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">链接</label>
					<div class="controls">
						<s:textfield name="url" />
		             </div>
				</div>
            </div>
         </div>
     </div>
     
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
$(function() {
     $(".form-md-ad-inputBasic").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                var $adObjectType=$(".form-md-ad-inputBasic select[name='adObjectType']");
                $adObjectType.change(function() {
                    var adObjectType=$adObjectType.val();
                    if(adObjectType!='URL'){
                        $form.find("input[name='url']").val("");
                    }
                    $form.find("input[name='adObjectSid']").val("");
                    $form.find("input[name='adObjectDisplay']").val("");
                })
                $form.find(".fa-select-ad-object").click(function() {
                    var adObjectType=$adObjectType.val();
                    if(adObjectType=='COMMODITY'){
                         $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                            title : '选取商品',
                            callback : function(rowdata) {
                                $form.setFormDatas({
                                    'adObjectSid' : rowdata.id,
                                    'adObjectDisplay' : rowdata.display
                                });
                                
                            }
                        })
                    }else if(adObjectType=='THEME'){
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/theme!forward?_to_=selection',
                            title : '选取主题',
                            callback : function(rowdata) {
                                $form.setFormDatas({
                                    'adObjectSid' : rowdata.id,
                                    'adObjectDisplay' : rowdata.display
                                });
                                
                            }
                        })
                    }else if(adObjectType=='SMZDM'){
                         $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/smzdm!forward?_to_=selection',
                            title : '选取每日推荐',
                            callback : function(rowdata) {
                                $form.setFormDatas({
                                    'adObjectSid' : rowdata.id,
                                    'adObjectDisplay' : rowdata.display
                                });
                                
                            }
                        })
                    }else if(adObjectType=='ACTIVITY'){
                         $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/activity!forward?_to_=selection',
                            title : '选取活动',
                            callback : function(rowdata) {
                                $form.setFormDatas({
                                    'adObjectSid' : rowdata.id,
                                    'adObjectDisplay' : rowdata.display
                                });
                                
                            }
                        })
                    }else if(adObjectType=='URL'){
                        alert("对象类型为超链接，请维护‘链接’");
                    }
                 });
               
            }  
    });
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>
