<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>

<div class="tabbable tabbable-primary">
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-myt-md-activity-objects" data-grid="table"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $(".grid-myt-md-activity-objects").data("gridOptions", {
            url: "${base}/myt/md/gift-paper!giftPaperR2Objects?id=<s:property value='#parameters.id'/>",
            colModel: [{
                label: '白名单或黑名单',
                name: 'whiteOrBlack',
                align: 'center',
                width: 80,
                stype: 'select',
                editable: true,
                searchoptions: {
                    value: Util.getCacheEnumsByType('whiteOrBlackEnum')
                }
            }, {
                label: '是否可用',
                name: 'enable',
                editable: true,
                width: 80,
                edittype: "checkbox",
                align: 'center'
            }, {
                label: '对象类型',
                name: 'objectType',
                width: 120,
                align: 'center',
                editable: true,
                stype: 'select',
                searchoptions: {
                    value: Util.getCacheEnumsByType('giftPaperTypeEnum')
                }
            }, {
                label: '对象主键',
                name: 'objectSid',
                width: '100',
                editable: true,
                editoptions: {
                    dataInit: function (elem) {
                        var $elem = $(elem);
                        $elem.attr("readonly", true);
                    }
                }
            }, {
                label: '对象名',
                name: 'objectDisplay',
                align: 'left',
                editable: true,
                editoptions: {
                    dataInit: function (elem) {
                        var $grid = $(this);
                        var $elem = $(elem);

                        $elem.wrap('<div class="input-icon right"/>');
                        $elem.before('<i class="fa fa-ellipsis-horizontal fa-select-object"></i>');
                        var selectObject = function (item) {
                            // 强制覆盖已有值
                            $grid.jqGrid("setEditingRowdata", {
                                'objectSid': item.id,
                                'objectDisplay': item.display
                            });
                        }

                        $grid.find("select[name='objectType']").change(function () {
                            $grid.jqGrid("setEditingRowdata", {
                                'objectSid': '',
                                'objectDisplay': ''
                            });
                        })
                        $grid.find(".fa-select-object").click(function () {
                            var rowdata = $grid.jqGrid("getEditingRowdata");
                            if (rowdata['objectType'] == 'COMMODITY') {
                                $(this).popupDialog({
                                    url: WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                                    callback: selectObject
                                })
                            } else if (rowdata['objectType'] == 'CATEGORY') {
                                $(this).popupDialog({
                                    url: WEB_ROOT + '/myt/md/category!forward?_to_=selection',
                                    callback: selectObject
                                })
                            } else if (rowdata['objectType'] == 'GROUP') {
                                $(this).popupDialog({
                                    url: WEB_ROOT + '/myt/md/commodity-group!forward?_to_=selection',
                                    callback: selectObject
                                })
                            } else if (rowdata['objectType'] == 'COLLECTION') {
                                $(this).popupDialog({
                                    url: WEB_ROOT + '/myt/md/collection!forward?_to_=selection',
                                    callback: selectObject
                                })
                            }
                        });
                    }
                }
            }],
            editurl: "${base}/myt/md/gift-paper-object!doSave?giftPaper.id=<s:property value='#parameters.id'/>",
            delurl: "${base}/myt/md/gift-paper-object!doDelete"
        });
    });

</script>
<%@ include file="/common/ajax-footer.jsp" %>