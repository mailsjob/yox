<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row search-form-default">
		<div class="col-md-12">
			<form method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-md-theme-selection"
				action="#">
				<div class="form-group">
					<input type="text" name="search['CN_customerProfile.nickName_OR_customerProfile.trueName_OR_themeTitle']" class="form-control input-xlarge"
						placeholder="客户、主题标题..." />
				</div>
				<button class="btn default hidden-inline-xs" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-md-theme-selection"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-theme-selection").data("gridOptions", {
            url : "${base}/myt/md/theme!findByPage",
            colModel : [ {
                label : '流水号',
                name:'id'
            },{
                label : '图片',
                name : 'themePic',
                width : 30,
                align : 'center',
                formatter : Biz.md5CodeImgViewFormatter
            }, {
                label : '客户',
                name : 'customerProfile.display',
                index:'customerProfile.nickName_OR_customerProfile.trueName',
                width : 80
            }, {
                label : '主题',
                name : 'display',
                width : 100,
                align : 'left',
                responsive : 'sm'
            }, {
                label : '排序号',
                name : 'orderIndex',
                width : 50,
                align : 'left'
            }, {
                label : '关注数',
                name : 'focusCount',
                width : 50
            }, {
                label : '评论数',
                name : 'commentCount',
                width : 50,
                align : 'left'
            }, {
                label : '标签',
                name : 'allLabels',
                hidden : true,
                align : 'right'
            }, {
                label : '排序号',
                name : 'payedScore',
                width : 60,
                hidden : true,
                align : 'center'
            }, {
                label : '状态',
                name : 'themeStatus',
                align : 'center',
                width : 50,
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('themeStatusEnum')
                },
                responsive : 'sm'
            }],
            
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }

            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
