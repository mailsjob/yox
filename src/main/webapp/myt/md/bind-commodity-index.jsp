<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">外部商品绑定内部商品列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search" data-grid-search="#grid-bind-commodity-list" action="#">
						
							<div class="form-group">
								<input class="form-control input-xlarge" type="text" name="search['CN_name_OR_sku_OR_commodity.sku_OR_commodity.title']" placeholder="名称、sku、商品编码、商品名称……">
							</div>
							<div class="form-group">
								<input type="checkbox" name="search['NU_commodity']"  />待绑定
							</div>
							<div class="form-group">
								<input type="checkbox" name="search['NN_commodity']"  />已绑定
							</div>
							<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
								
							
						
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table id="grid-bind-commodity-list" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script src="${base}/biz.js?_=${buildVersion}"></script>
<script type="text/javascript">
    $(function() {
        $("#grid-bind-commodity-list").data("gridOptions", {
            url : "${base}/myt/md/bind-commodity!findByPage",
            colNames : [   '外部商品名','SKU', '内部商品编码', '内部商品名', '备注', '来源'],
            colModel : [  {
                name : 'name',
              	align : 'center'
            }, {
                name : 'sku',
                editable : true,
                width:150,
                align : 'center'
            }, {
                name : 'commodity.sku',
                width:150,
                align : 'center'
            }, {
                name : 'commodity.title',
               	align : 'left'
            }, {
                name : 'memo',
                editable : true,
                align : 'left'
            }, {
                name : 'comeFrom',
                width:50,
               	align : 'center'
            } ],
            editurl : "${base}/myt/md/bind-commodity!doUpdate",
            delurl : "${base}/myt/md/bind-commodity!doDelete",
            fullediturl : "${base}/myt/md/bind-commodity!inputTabs",
            viewurl : "${base}/myt/md/bind-commodity!viewTabs"
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>