$(".grid-md-commodity-vip").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/commodity!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            responsive : 'sm'
        }, {
            label : '周期购标识',
            name : 'circle',
            edittype : 'checkbox',
            align : 'center'
        }, {
            label : '现货',
            name : 'canBuyNow',
            edittype : 'checkbox',
            align : 'center'
        }, {
            label : '商品编码',
            name : 'sku',
            align : 'left',
            width : 100
        }, {
            label : '商品名称',
            name : 'title',
            sortable : false,
            width : 300,
            align : 'left'
        }, {
            label : '状态',
            name : 'commodityStatus',
            align : 'center',
            width : 50,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('commodityStatusEnum')
            },
            responsive : 'sm'
        }, {
            label : '商品价格',
            name : 'price',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : '商品成本价',
            name : 'costPrice',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : 'vip返利',
            name : 'vipCommodity.backMoney',
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : 'vip积分',
            name : 'vipCommodity.score',
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : '启用',
            name : 'vipCommodity.enable',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        }],
        postData : {
            "search['EQ_solrFilterType']" : "MYT"
        },
        inlineNav : {
            add : false
        },
        addable:false,
        editcol : 'sku',
        editurl : WEB_ROOT + "/myt/md/commodity!doSave",
        fullediturl : WEB_ROOT + "/myt/md/commodity!inputTabs",
        operations : function(itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>批量启用/停用</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/md/commodity!vipCommodityEnable';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  启用/禁用？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select);
            
        },
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/vip/vip-commodity-rate!findByPage?search['EQ_commodity.id']=" + row_id,
                colModel : [ {
                    label : 'VIP等级',
                    hidden:true,
                    name : 'vipLevel.levelIndex',
                    width : 100
                },{
                    label : 'VIP等级',
                    name : 'vipLevel.name',
                    width : 100
                },{
                    label : '返利比率（%）',
                    name : 'levelRate',
                    editable : true,
                    align : 'center',
                    width : 100
                }],
                cmTemplate : {
                    sortable : false
                },
                sortorder : "asc",
                sortname : "vipLevel.levelIndex",
                inlineNav : {
                    add : false
                },
                editurl : WEB_ROOT + '/myt/vip/vip-commodity-rate!doSave'

            });
        }
        
     });
