<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-md-smzdm-inputBasic"
	action="${base}/myt/md/smzdm!doSave" method="post" 
	data-editrulesurl="${base}/myt/md/smzdm!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-smzdm-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
            <div class="col-md-6">
            	<div class="form-group">
					<label class="control-label col-md-4">图片</label>
					<div class="col-md-8">
						<s:hidden name="img" data-singleimage="true" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
	            <div class="row">
	            	<div class="col-md-12">
		            	<div class="form-group">
							<label class="control-label col-md-2">商品</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-search fa-search-commodity"></i>
									<i class="fa fa-times fa-clear-commodity"></i>
									<s:textfield name="commodity.display"/>
									<s:hidden name="commodity.id"/>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">爆料价</label>
							<div class="controls">
				                <s:textfield name="price" />
							</div>
						</div>
		            </div>
		            <div class="col-md-12">
						<div class="form-group">
							<label class="control-label">适合阶段</label>
							<div class="controls">
				                <s:select name="fitStage" list="#application.enums.fitStageEnum"/>
							</div>
						</div>
		            </div>
		            <div class="col-md-12">
						<div class="form-group">
							<label class="control-label">是否可用</label>
							<div class="controls">
				                <s:radio name="enable" list="#application.enums.booleanLabel"/>
							</div>
						</div>
		            </div>
	        	</div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">主题</label>
					<div class="controls">
		                <s:textfield name="title" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">副标题</label>
					<div class="controls">
		                <s:textfield name="subTitle" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
       		<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">适合人群</label>
					<div class="controls">
		                <s:textfield name="fitPeople" />
					</div>
				</div>
            </div>
        	<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">适合集合</label>
					<div class="controls">
		                <s:textfield name="fitUnit" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">标签</label>
					<div class="controls">
		                <s:textfield name="labels" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">链接</label>
					<div class="controls">
		                <s:textfield name="zdmLink" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">联系邮箱</label>
					<div class="controls">
		                <s:textfield name="contactEmail" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">记录时间</label>
					<div class="controls">
		                <s3:datetextfield name="recordTime"  format="timestamp" data-timepicker="true" />                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">起始</label>
					<div class="controls">
		                <s:textfield name="fitStart" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">评论数量</label>
					<div class="controls">
		                <s:textfield name="commentCount" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">开始时间</label>
					<div class="controls">
		                <s3:datetextfield name="beginTime" format="timestamp" data-timepicker="true"/>                  
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">结束时间</label>
					<div class="controls">
		                <s3:datetextfield name="endTime" format="timestamp" data-timepicker="true"/>                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">概述</label>
					<div class="controls">
						<s:textarea name="summary" data-htmleditor="kindeditor" data-height="200px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">介绍</label>
					<div class="controls">
						<s:textarea name="introduction" data-htmleditor="kindeditor" data-height="550px" />
					</div>
				</div>
            </div>
        </div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-smzdm-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/md/smzdm-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>