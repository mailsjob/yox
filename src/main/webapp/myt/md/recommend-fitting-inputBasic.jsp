<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-recommend-fitting"
	action="${base}/myt/md/recommend-fitting!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-cb-recommend-fitting-list">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>	
	 <div class="form-body">	
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label class="control-label col-md-4">原商品</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa fa-search fa-search-source"></i>
							<s:textfield name="source.display" readonly="true" />
							<s:hidden name="source.id"/>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row">
		<div class="col-md-10">
			<div class="form-group">
					<label class="control-label col-md-4">目标商品</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa fa-search fa-search-dest"></i>
							<s:textfield name="dest.display" readonly="true" />
							<s:hidden name="dest.id"/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">排序号</label>
					<div class="col-md-8">
						<s:textfield name="orderIndex" />
					</div>
				</div>

			</div>
		</div>
		
	
		<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-cb-lottery-award-list">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {
        $(".form-edit-recommend-fitting").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                $form.find("i.fa-search-source").click(function() {
                   $(this).popupDialog({
                            url : '${base}/myt/md/commodity!forward?_to_=selection',
                            title : '选取商品',
                            callback : function(json) {
                               $form.find("[name$='source.display']").val(json.sku + " " + json.title);
                                $form.find("[name$='source.id']").val(json.id);
                            }
                        })
                   
                });
                $form.find("i.fa-search-dest").click(function() {
                    $(this).popupDialog({
                             url : '${base}/myt/md/commodity!forward?_to_=selection',
                             title : '选取商品',
                             callback : function(json) {
                                 $form.find("[name$='dest.display']").val(json.sku + " " + json.title);
                                 $form.find("[name$='dest.id']").val(json.id);
                             }
                         })
                    
                 });

              }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
