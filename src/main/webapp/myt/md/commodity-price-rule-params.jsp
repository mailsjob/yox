<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<form class="form-horizontal form-bordered form-label-stripped form-validation form-commodity-price-rule-params"
		action="${base}/myt/md/commodity-price!doRuleParamsRefresh" method="post">
		<s:hidden name="commoditySid" value="%{#parameters.commoditySid}" />
		<s:hidden name="ruleType" value="%{#parameters.ruleType}" />
		<div class="form-actions">
		<button class="btn blue" type="submit" >
			<i class="fa fa-check"></i> 刷新价格计划参数
		</button>
		<button class="btn default btn-reset" type="button" onclick="javascript:$('.form-commodity-price-rule-params').reset()">重置</button>
		<s:checkbox cssClass="edit_checkbox" name="coverPrice" value="false" />&nbsp;&nbsp;<font color="red">覆盖已有价格计划</font>
	
	</div>
	</div>
	<div class="form-body">
	<s:if test="#parameters.ruleType[0]=='FREE_INPUT'">
	
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">起始年月</label>
					<div class="controls">
						<s3:datetextfield name="startDate" format="date"/>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">截止年月</label>
					<div class="controls">
						<s3:datetextfield name="endDate" format="date"/>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">参考引用价格</label>
					<div class="controls">
						<s3:textfield name="m0Price" label="当月"/>
					</div>
				</div>
			</div>
		</div>
		
	</s:if>
	<s:elseif test="#parameters.ruleType[0]=='DECREASE_BY_MONTH_RATE'">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">基准价</label>
					<div class="controls">
						<s3:textfield name="basePrice"/>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">最低限价</label>
					<div class="controls">
						<s3:textfield name="lowestPrice" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">递减折扣(小数)</label>
					<div class="controls">
						<s3:textfield name="decreaseRate"/>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">递减月份数</label>
					<div class="controls">
						<s3:textfield name="decreaseMonths"  />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">起始年月</label>
					<div class="controls">
						<s3:datetextfield name="startDate" format="date"/>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">截止年月</label>
					<div class="controls">
						<s3:datetextfield name="endDate" format="date"/>
					</div>
				</div>
			</div>
		</div>
	</s:elseif>
	<s:else>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">基准价</label>
					<div class="controls">
						<s3:textfield name="basePrice"/>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">最低限价</label>
					<div class="controls">
						<s3:textfield name="lowestPrice" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">递减月份数</label>
					<div class="controls">
						<s3:textfield name="decreaseMonths"  />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">起始年月</label>
					<div class="controls">
						<%-- <s3:datetextfield name="startDate" format="date"/> --%>
						<s3:textfield name="startDate"  />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">截止年月</label>
					<div class="controls">
						<%-- <s3:datetextfield name="endDate" format="date"/> --%>
						<s3:textfield name="endDate"  />
					</div>
				</div>
			</div>
		</div>
	</s:else>
	</div>
	</form>	
<div class="div-md-commodity-price-input-line"></div>
<script type="text/javascript">
    $().ready(function() {
        $(".form-commodity-price-rule-params").submit({
            success : function(response) {
                $(".div-md-commodity-price-input-line").html(response);
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
