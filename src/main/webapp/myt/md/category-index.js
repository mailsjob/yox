$(function() {
    $(".grid-md-category-normal").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/category!findByPage?search['EQ_categoryType']=CATEGORY",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '编号',
            name : 'categoryCode',
            width : 80,
            editable : true,
            align : 'center'
        }, {
            label : '名称',
            name : 'name',
            editable : true,
            align : 'left',
            width : 80
        }, {
            label : '排序号',
            name : 'orderIndex',
            editable : true,
            align : 'center',
            width : 100
        }, {
            label : '层级',
            name : 'categoryLevel',
            editable : true,
            align : 'center',
            width : 100
        }, {
            label : '类型',
            name : 'categoryType',
            editable : true,
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('categoryEnum')
            }

        }, {
            label : '周期购标识',
            name : 'circle',
            edittype : 'checkbox',
            align : 'center'
        }, {
            label : '备注',
            name : 'memo',
            edittype : 'textarea',
            editable : true
        } ],
        editcol:'name',
        subGrid : true,
        gridDnD : true,
        sortorder : "asc",
        sortname : "categoryCode",
        postData : {
            "search['NU_parent']" : "true",
        },
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initRecursiveSubGrid(subgrid_id, row_id, "parent.id", true);
        },
        editurl : WEB_ROOT + "/myt/md/category!doSave",
        delurl : WEB_ROOT + "/myt/md/category!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/category!inputTabs"
    });

    $(".grid-md-category-label").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/category!findByPage?search['EQ_categoryType']=LABEL",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '编号',
            name : 'categoryCode',
            editable : true,
            align : 'center'
        }, {
            label : '名称',
            name : 'name',
            editable : true,
            align : 'left'
        }, {
            label : '排序号',
            name : 'orderIndex',
            editable : true,
            align : 'center',
            width : 80
        }, {
            label : '类型',
            name : 'categoryType',
            align : 'center',
            editable : true,
            width : 80,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('categoryEnum')
            }
        } ],
        editurl : WEB_ROOT + "/myt/md/category!doSave?categoryLevel=1",
        delurl : WEB_ROOT + "/myt/md/category!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/category!edit"
    });
    $(".grid-md-category-timeline").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/category!findByPage?search['EQ_categoryType']=TIMELINE",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '编号',
            name : 'categoryCode',
            editable : true,
            width : 100,
            align : 'center'
        }, {
            label : '名称',
            name : 'name',
            editable : true,
            align : 'left',
            width : 120
        }, {
            label : '排序号',
            name : 'orderIndex',
            editable : true,
            align : 'center',
            width : 100
        }, {
            label : '类型',
            name : 'categoryType',
            align : 'center',
            editable : true,
            width : 100,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('categoryEnum')
            },
            responsive : 'sm'
        } ],
        editurl : WEB_ROOT + "/myt/md/category!doSave?categoryLevel=1",
        delurl : WEB_ROOT + "/myt/md/category!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/category!edit"
    });
    $(".grid-md-category-themelabel").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/category!findByPage?search['EQ_categoryType']=THEMELABEL",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '编号',
            name : 'categoryCode',
            editable : true,
            width : 100,
            align : 'center'
        }, {
            label : '名称',
            name : 'name',
            align : 'left',
            editable : true,
            width : 120
        }, {
            label : '排序号',
            name : 'orderIndex',
            editable : true,
            align : 'center',
            width : 100
        }, {
            label : '类型',
            name : 'categoryType',
            align : 'center',
            editable : true,
            width : 100,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('categoryEnum')
            },
            responsive : 'sm'
        } ],
        editurl : WEB_ROOT + "/myt/md/category!doSave?categoryLevel=1",
        delurl : WEB_ROOT + "/myt/md/category!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/category!edit"
    });
});