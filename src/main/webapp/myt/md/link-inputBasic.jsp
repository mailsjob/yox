<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-md-link-inputBasic"
	action="${base}/myt/md/link!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i>保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body control-label-sm">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">名称</label>
					<div class="controls">
						<s:textfield name="name"></s:textfield>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">url</label>
					<div class="controls">
						<s:textfield name="url"></s:textfield>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">代码</label>
					<div class="controls">
						<s:textarea name="xmlStr2" rows="40"></s:textarea>
					</div>
				</div>
			</div>
			
		</div>
		
	
</form>
<script type="text/javascript">

<%@ include file="/common/ajax-footer.jsp"%>