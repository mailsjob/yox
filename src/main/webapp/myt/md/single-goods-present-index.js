$(function() {
    $(".grid-myt-md-single-goods-present-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/single-goods-present!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '单品主键',
            name : 'singleGoods.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '关联单品',
            name : 'singleGoods.display',
            index : 'singleGoods.title',
            width : 300,
            editable: true,
            editoptions : {
                placeholder : '双击选取',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    // 弹出框的选取回调函数
                    var selectCommodity = function(item) {
                    	console.info(item);
                        $grid.jqGrid('setEditingRowdata', {
                        	'singleGoods.id':item['id'],
                        	'singleGoods.display':item['title']
                        });
                    }
                    $elem.dblclick(function() {
                        var rowdata = $grid.jqGrid("getEditingRowdata");
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/single-goods!forward?_to_=selection',
                            postData : {
                                sku : rowdata['commodity.sku']
                            },
                            title : '选取单品商品',
                            callback : function(item) {
                                selectCommodity(item);
                            }
                        })
                    });
                }
            },
            align : 'left'
        }, {
            label : '赠品主键',
            name : 'presentCommodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
        	label : '赠品商品',
            name : 'presentCommodity.display',
            index : 'presentCommodity.sku_OR_presentCommodity.title',
            editable : true,
            width : 200,
            editrules : {
                required : true
            },
            editoptions : Biz.getGridCommodityEditOptions(),
            align : 'left'
        }, {
            label : '数量',
            name : 'quantity',
            width : 80,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '开始日期',
            name : 'beginDate',
            width : 150,
            formatter: 'date',
            editable: true,
            align : 'center'
        }, {
            label : '结束日期',
            name : 'endDate',
            width : 150,
            formatter: 'date',
            editable: true,
            align : 'center'
        }, {
            label : '支持类型',
            stype : 'select',
            name : 'givenType',
            editable : true,
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('givenTypeEnum')
            },
            width : 80
        }, {
            label : '是否叠加赠送',
            name : 'canStacked',
            width : 130,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }, {
            label : '启用',
            name : 'isEnable',
            width : 100,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }],
        editurl : WEB_ROOT + '/myt/md/single-goods-present!doSave',
        delurl : WEB_ROOT + '/myt/md/single-goods-present!doDelete'
    });
});
