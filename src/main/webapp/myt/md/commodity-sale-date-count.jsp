<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-commodity-sale-date-count" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-commodity-sale-date-count").data("gridOptions", {
            url : "${base}/myt/md/commodity!commoditySaleDateCounts?id=<s:property value='#parameters.id'/>",
           	colModel : [  {
                label : '日期',
                name : 'saleDate',
                editable : true,
                width : 200,
                sorttype : 'date',
                align : 'center'
            }, {
                label : '单个用户每日可购买量',
                name : 'dailyCanBuyCount',
                editable : true,
                width : 200,
                sorttype : 'number',
                align : 'right'
            }],
            sortorder : "asc",
            sortname : "saleDate",
           	editurl : "${base}/myt/md/commodity-sale-date-count!doSave?commodity.id=<s:property value='#parameters.id'/>",
            delurl : "${base}/myt/md/commodity-sale-date-count!doDelete"
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>