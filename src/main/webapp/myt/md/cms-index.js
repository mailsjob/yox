$(function() {
    $(".grid-myt-md-cms-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/cms!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id'                            
        }, {
            label : '代码',
            name : 'staticId',
            width : 120,
            editable: true,
            align : 'left'
        }, {
            label : '分类',
            name : 'category',
            width : 100,
            editable: true,
            align : 'left'
        }, {
            label : '标题',
            name : 'title',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '页面关键词',
            name : 'keyWords',
            width : 200,
            editable: true,
            align : 'left'
        } ],
        editurl : WEB_ROOT + '/myt/md/cms!doSave',
       
        delurl : WEB_ROOT + '/myt/md/cms!doDelete',
        fullediturl : WEB_ROOT + '/myt/md/cms!inputTabs'
    });
});
