<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-brand-commodities" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="div-md-commodity-price-input-batch"></div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-brand-commodities").data("gridOptions", {
            url : "${base}/myt/md/commodity!findByPage?search['EQ_brand.id']=<s:property value='#parameters.id'/>",
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true,
                responsive : 'sm'
            }, {
                label : '主图',
                name : 'smallPic',
                sortable : false,
                search : false,
                width : 80,
                align : 'center',
                formatter : Biz.md5CodeImgViewFormatter,
                frozen : true,
                responsive : 'sm'
            }, {
                label : '商品编码',
                name : 'sku',
                align : 'left',
                width : 80
            }, {
                label : '商品名称',
                name : 'title',
                sortable : false,
                width : 200,
                align : 'left'
            }, {
                label : '基准价格',
                name : 'price',
                width : 80,
                formatter : 'currency',
                responsive : 'sm'
            }, {
                label : '是否周期购',
                name : 'circle',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            }, {
                label : '状态',
                name : 'commodityStatus',
                align : 'center',
                width : 50,
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('commodityStatusEnum')
                },
                responsive : 'sm'
            }, {
                label : '分类主键',
                name : 'category.id',
                hidden : true,
                hidedlg : true,
                editable : true
            }, {
                label : '分类',
                name : 'category.display',
                index : 'category.categoryCode_OR_category.categoryCode',
                width : 150,
                sortable : false,
                editable : true,
                align : 'left',
                editoptions : {
                    dataInit : function(elem, opt) {
                        var $grid = $(this);
                        var $elem = $(elem);
                        $elem.treeselect({
                            url : WEB_ROOT + "/myt/md/category!categoryList",
                            callback : {
                                onSingleClick : function(event, treeId, treeNode) {
                                    $grid.jqGrid("setEditingRowdata", {
                                        'category.id' : Util.startWith(treeNode.id, "-") ? "" : treeNode.id,
                                        'category.display' : Util.startWith(treeNode.id, "-") ? "" : treeNode.name
                                    });
                                },
                                onClear : function(event) {
                                    $grid.jqGrid("setEditingRowdata", {
                                        'category.id' : '',
                                        'category.display' : ''
                                    });
                                }
                            }
                        });
                    }
                },
                responsive : 'sm'
            } ],
            inlineNav : {
                add : false
            },
            editurl : "${base}/myt/md/commodity!doSave",
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>