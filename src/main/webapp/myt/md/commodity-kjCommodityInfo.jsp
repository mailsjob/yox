<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation"
	action="${base}/myt/md/commodity!doSave" method="post" >
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" >
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
		
		<button class="btn red pull-right" style="margin-left: 5px" type="submit"
			data-form-action="${base}/myt/md/commodity!kjPause"
			data-confirm="确认暂停备案？">暂停备案</button>
			
		<button class="btn yellow pull-right" style="margin-left: 5px" type="submit"
			data-form-action="${base}/myt/md/commodity!kjEdit"
			data-confirm="确认变更备案？">变更备案</button>
			
		<button class="btn green pull-right" style="margin-left: 5px" type="submit"
			data-form-action="${base}/myt/md/commodity!kjNew"
			data-confirm="确认提交备案？">提交备案</button>
		
	</div>
	<div class="form-body">
		<div class="row">
        	<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">申报计量单位</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.declareUnit" required="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">规格型号</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.goodesSpec" required="true"/>
					</div>
				</div>
            </div>
        </div>
		<div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">法定计量单位</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.legalUnit" required="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">法定计量单位折算数量</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.convLegalUnitNum" required="true"/>
					</div>
				</div>
            </div>
        </div>
        
        <div class="row">
        	<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否试点商品</label>
					<div class="controls">
		                <s:radio name="kjCommodityInfo.isExperimentGoods" list="#application.enums.booleanLabel" required="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">行邮税号</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.postTaxNO" required="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品HS编码</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.hsCode" required="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">入区计量单位</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.inAreaUnit" required="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">入区计量单位折算数量</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.convInAreaUnitNum" required="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">外部系统编号</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.externalNo" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">国外生产企业是否在中国注册备案（食药监局、国家认监委）</label>
					<div class="controls">
		                <s:radio name="kjCommodityInfo.isCncaPor" list="#application.enums.booleanLabel"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">供应商名称</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.supplierName" />
					</div>
				</div>
            </div>
            <%-- <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品名称</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.goodsName" />
					</div>
				</div>
            </div> --%>
            <%-- <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品货号</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.sku" />
					</div>
				</div>
            </div> --%>
        </div>
        
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">国家地区代码</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.originCountryCode" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">施检机构的代码</label>
					<div class="controls">
		                <s:textfield name="kjCommodityInfo.checkOrgCode" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">跨境备注</label>
					<div class="controls">
						<p class="form-control-static">
		                <s:property value="kjCommodityInfo.kjMemo"/>
		                </p>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">审批状态</label>
					<div class="controls">
						<p class="form-control-static">
		                <s:property value="#application.enums.kjSkuStatusCodeEnum[kjCommodityInfo.skuStatusCode]" />
		                </p>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">合法采购证明（国外进货发票或小票）</label>
					<div class="controls">
		                <s:textarea name="kjCommodityInfo.legalTicket" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">原产地证书</label>
					<div class="controls">
		                <s:textarea name="kjCommodityInfo.originPlaceCert" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">食药监局、国家认监委备案附件</label>
					<div class="controls">
		                <s:textarea name="kjCommodityInfo.cncaPorDoc" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">外文标签的中文翻译件</label>
					<div class="controls">
		                <s:textarea name="kjCommodityInfo.markExchange" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">境外官方及第三方机构的检测报告</label>
					<div class="controls">
		                <s:textarea name="kjCommodityInfo.testReport" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" >
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/kuajing/myt-kj-sku-info-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>