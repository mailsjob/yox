<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">楼层</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/md/recommend-commodity">首页商品</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/md/floor-menu">楼层-菜单</a></li>
		<li><a class="tab-default" data-toggle="tab" href="${base}/myt/md/floor-ad">楼层-广告</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
		<div class="tab-content">
			<div class="tab-pane fade active in">
				<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search" data-grid-search=".grid-md-floor"
						action="#">
						<div class="form-group">
							<input type="text" name="search['CN_fitStage_OR_monthText']" class="form-control input-large"
								placeholder="适用阶段、月份文本..." />
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="grid-md-floor"  data-grid="table"></table>
					</div>
				</div>
			</div>
	</div>	
</div>
<script src="${base}/myt/md/floor-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>