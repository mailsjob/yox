<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-md-single-goods-inputBasic" 
    action="${base}/myt/md/single-goods!doSave" method="post" data-editrulesurl="${base}/myt/md/single-goods!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-single-goods-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">橱窗图</label>
                    <div class="controls">
                        <s:hidden name="pic" data-singleimage="true" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">商品</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <i class="fa fa-search fa-search-commodity"></i> <i class="fa fa-times fa-clear-commodity"></i>
                                    <s:textfield name="commodity.display" />
                                    <s:hidden name="commodity.id" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">单价</label>
                            <div class="controls">
                                <s:textfield name="price" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">最大可售数量</label>
                            <div class="controls">
                                <s:textfield name="maxSellQuantity" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">已售数量</label>
                            <div class="controls">
                                <s:textfield name="soldQuantity" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">商品标题</label>
                    <div class="controls">
                        <s:textfield name="title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">单品类型</label>
                    <div class="controls">
                        <s:select name="type" list="#application.enums.singleGoodsTypeEnum" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">闪购位置</label>
                    <div class="controls">
                        <s:select name="siteType" list="#application.enums.mYTSiteTypeEnum" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">单日限购数</label>
                    <div class="controls">
                        <s:textfield name="dailyUserCanBuyQuatity" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">用户最多购买数量</label>
                    <div class="controls">
                        <s:textfield name="userBoughtMaxQuatity" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">起售日期</label>
                    <div class="controls">
                        <s3:datetextfield name="sellBeginTime" format="timestamp" data-timepicker="true"></s3:datetextfield>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">停售日期</label>
                    <div class="controls">
                        <s3:datetextfield name="sellEndTime" format="timestamp" data-timepicker="true"></s3:datetextfield>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">单品描述</label>
                    <div class="controls">
                        <s:textarea name="htmlContent" data-htmleditor="kindeditor" data-height="300px" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-single-goods-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/md/single-goods-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>