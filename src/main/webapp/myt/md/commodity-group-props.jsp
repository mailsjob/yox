<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-commodity-group-props" action="${base}/myt/md/commodity-group!saveProps" method="post">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:hidden name="objName" value="%{#parameters.objName}" />
    <s:hidden name="objSid" value="%{#parameters.objSid}" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body control-label-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-commodity-group-props">
                        <thead>
                            <tr>
                                <th class="input-medium" rowspan="2">代码</th>
                                <th class="input-small" rowspan="2">策略</th>
                                <th colspan="2">排序号</th>
                                <th colspan="3">主参数</th>
                                <th colspan="3">扩展参数1</th>
                                <th colspan="3">扩展参数2</th>
                            </tr>
                            <tr>
                                <th class="input-mini">默认值</th>
                                <th class="input-mini">设定值</th>
                                <th class="input-mini">名称</th>
                                <th class="input-small">默认值</th>
                                <th class="input-small">设定值</th>
                                <th class="input-mini">名称</th>
                                <th class="input-mini">默认值</th>
                                <th class="input-mini">设定值</th>
                                <th class="input-mini">名称</th>
                                <th class="input-mini">默认值</th>
                                <th class="input-mini">设定值</th>
                            </tr>
                        </thead>
                        <tbody>
                            <s:iterator value="displayDynaPropLineVals" status="status" id="item">
                                <s:set var="iteratorId" value="%{'item_' + #status.index}" scope="page" />
                                <tr rowid='<s:property value="dynaPropLine.id"/>' rowparentid='<s:property value="dynaPropLine.parent.id"/>'>
                                    <td><s:set var="inheritLevelWidth" value="%{(dynaPropLine.inheritLevel-#request.dynaPropLineRoot.inheritLevel-1)*30+''}" /> <span
                                        style="display:inline-block;width:<s:property value='inheritLevelWidth'/>px"></span> <s:hidden
                                            name="%{'displayDynaPropLineVals['+#status.index+'].dynaPropLine.id'}" value="%{displayDynaPropLineVals[#status.index].dynaPropLine.id}" />
                                        <s:checkbox name="%{'displayDynaPropLineVals['+#status.index+'].dynaPropLine.extraAttributes.checked'}" value="%{id!=null?true:false}" />&nbsp;
                                        <s:property value="dynaPropLine.code" /></td>
                                    <td style="text-align: center"><s:select list="#application.enums.dynaPropValFetchStrategyEnum"
                                            name="%{'displayDynaPropLineVals['+#status.index+'].propValFetchStrategy'}" /></td>
                                    <td><s:property value="dynaPropLine.orderRank" /></td>
                                    <td style="text-align: center"><s:textfield name="%{'displayDynaPropLineVals['+#status.index+'].orderRank'}"
                                            disabled="%{id==null?true:false}" /></td>
                                    <td><s:property value="dynaPropLine.name" /></td>
                                    <td><s:property value="dynaPropLine.defaultValue" /></td>
                                    <td><s:if test="dynaPropLine.dynaPropExt1==null">N/A</s:if> <s:else>
                                            <s:property value="dynaPropLine.dynaPropExt1.name" />
                                        </s:else></td>
                                    <td><s:if test="dynaPropLine.dynaPropExt1==null">N/A</s:if> <s:else>
                                            <s:property value="dynaPropLine.dynaPropExt1.defaultValue" />
                                        </s:else></td>
                                    <td><s:if test="dynaPropLine.dynaPropExt1==null">N/A</s:if></td>
                                    <td><s:if test="dynaPropLine.dynaPropExt1==null">N/A</s:if> <s:else>
                                            <s:property value="dynaPropLine.dynaPropExt2.name" />
                                        </s:else></td>
                                    <td><s:if test="dynaPropLine.dynaPropExt2==null">N/A</s:if> <s:else>
                                            <s:property value="dynaPropLine.dynaPropExt2.defaultValue" />
                                        </s:else></td>
                                    <td><s:if test="dynaPropLine.dynaPropExt2==null">N/A</s:if></td>
                                </tr>
                            </s:iterator>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
<script type="text/javascript">
    function loopCheckLine(tr) {
        tr.find("input[name$='dynaPropLine.extraAttributes.checked']").attr("checked", true);
        tr.find("input,select,textarea").filter(":visible").attr("disabled", false);
        tr.find("input[name$='dynaPropLine.extraAttributes.checked']").attr("disabled", false);
        var parentid = tr.attr("rowparentid");
        if (parentid) {
            var parenttr = $(".table-commodity-group-props").find("tr[rowid=" + parentid + "]");
            if (parenttr && parenttr.length == 1) {
                loopCheckLine(parenttr);
            }
        }
    }

    function loopUnCheckLine(tr) {
        tr.find("input[name$='dynaPropLine.extraAttributes.checked']").attr("checked", false);
        tr.find("input,select,textarea").filter(":visible").attr("disabled", true);
        tr.find("input[name$='dynaPropLine.extraAttributes.checked']").attr("disabled", false);
        var rowid = tr.attr("rowid");
        $(".table-commodity-group-props").find("tr[rowparentid=" + rowid + "]").each(function() {
            loopUnCheckLine($(this));
        });
    }
    $(function() {
        $(".form-edit-commodity-group-props").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                $(".table-commodity-group-props tbody tr").each(function() {
                    var tr = $(this);
                    tr.find("input[name$='dynaPropLine.extraAttributes.checked']").click(function() {
                        if ($(this).is(":checked")) {
                            loopCheckLine(tr);
                        } else {
                            loopUnCheckLine(tr);
                        }
                    });
                });
            }
        })
    });
</script>