<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row search-form-default">
		<div class="col-md-12">
			<form method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-md-activity-selection"
				action="#">
				<div class="form-group">
					<input type="text" name="search['CN_activityCode_OR_activityName']" class="form-control input-xlarge"
						placeholder="代码、名称..." />
				</div>
				
				<button class="btn default hidden-inline-xs" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-md-activity-selection"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-md-activity-selection").data("gridOptions", {
            url : "${base}/myt/md/activity!findByPage",
            colModel : [{
                label : '流水号',
                name:'id'
            },{
                label:'代码 名称',
                name : 'display',
              	 width : 120
            }, {
                label:'开始时间',
                name : 'beginTime',
                width : 180,
                sorttype : 'date',
                align : 'center'
            }, {
                label:'结束时间',
                name : 'endTime',
                width : 180,
                sorttype : 'date',
                align : 'center'
            }],
            
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }

            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
