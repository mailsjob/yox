<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">商品分类</a></li>
		<li><a class="tab-default" data-toggle="tab" href="#tab-auto">标签</a></li>
		<li><a class="tab-default" data-toggle="tab" href="#tab-auto">时间轴</a></li>
		<li><a class="tab-default" data-toggle="tab" href="#tab-auto">主题标签</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search" data-grid-search=".grid-md-category-normal"
						action="#">
						<div class="form-group">
							<input type="text" name="search['CN_categoryCode_OR_name']" class="form-control input-large"
								placeholder="名称、分类号..." />
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-category-normal" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/common/ajax-footer.jsp"%>
<script>
$(function() {
    $(".grid-md-category-normal").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/category!findByPage?search['EQ_categoryType']=CATEGORY",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true,
            responsive : 'sm'
        }, {
            name : 'display',
            hidden : true
        }, {
            label : '编号',
            name : 'categoryCode',
            width : 80,
            editable : true,
            align : 'center'
        }, {
            label : '名称',
            name : 'name',
            editable : true,
            align : 'left',
            width : 80
        }, {
            name : 'display',
            hidden : true,
            align : 'left'
        }, {
            label : '排序号',
            name : 'orderIndex',
            editable : true,
            align : 'center',
            width : 100
        }, {
            label : '层级',
            name : 'categoryLevel',
            editable : true,
            align : 'center',
            width : 100
        }, {
            label : '类型',
            name : 'categoryType',
            editable : true,
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('categoryEnum')
            }
        }, {
            label : '周期购标识',
            name : 'circle',
            edittype : 'checkbox',
            align : 'center'
        } ],
        editcol:'name',
        subGrid : true,
        gridDnD : true,
        sortorder : "asc",
        sortname : "categoryCode",
        postData : {
            "search['NU_parent']" : "true",
        },
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initRecursiveSubGrid(subgrid_id, row_id, "parent.id", true);
        },
        onSelectRow : function(id) {
            var $grid = $(this);
            var $dialog = $grid.closest(".modal");
            $dialog.modal("hide");
            var callback = $dialog.data("callback");
            if (callback) {
                var rowdata = $grid.jqGrid("getRowData", id);
                rowdata.id = id;
                callback.call($grid, rowdata);
            }
        }
    });
});
</script>