$(function() {
    $(".grid-myt-md-collection-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/collection!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '代码',
            name : 'code',
            width : 80,
            editable: true,
            align : 'left'
        }, {
            label : '标题',
            name : 'title',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '是否可用',
            name : 'enable',
            width : 50,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }, {
            label : '备注',
            name : 'memo',
            width : 300,
            editable: true,
            align : 'left'
        }],
        editurl : WEB_ROOT + '/myt/md/collection!doSave',
        delurl : WEB_ROOT + '/myt/md/collection!doDelete',
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/md/collection!collectionObjects?id=" + row_id,
                colModel : [ {
                    label : '流水号',
                    name : 'id',
                    hidden : true
                }, {
                    label : '对象类型',
                    name : 'objectType',
                    formatter : 'select',
                    searchoptions : {
                        value : Util.getCacheEnumsByType('collectionObjectType')
                    },
                    width : 80,
                    editable: true,
                    align : 'center'
                }, {
                    label : '对象SID',
                    name : 'objectSid',
                    width : 60,
                    hidden : true,
                    editable: true,
                    align : 'center'
                }, {
                    label : '对象',
                    name : 'objectDisplay',
                    width : 200,
                    editable: true,
                    editoptions : {
                        placeholder : '请双击选取',
                        dataInit : function(elem) {
                            var $grid = $(this);
                            var $elem = $(elem);
                            // Callback
                            var select = function(sid, display) {
                                $grid.jqGrid("setEditingRowdata",{
                                	objectSid : sid,
                                	objectDisplay : display
                                });
                            }
                            $elem.dblclick(function() {
                                var rowdata = $grid.jqGrid("getEditingRowdata");
                                
                                if(rowdata.objectType == 'COMMODITY'){// ! 商品选取
                                	$(this).popupDialog({
                                        url : WEB_ROOT + '/myt/stock/commodity-stock!forward?_to_=selectionSingle',
                                        title : '选取库存商品',
                                        callback : function(item){
                                        	select(item.id, item['commodity.display']);
                                        }
                                    })
                                } else if(rowdata.objectType == 'CATEGORY'){// ! 分类选取
                                	$(this).popupDialog({
                                        url : WEB_ROOT + '/myt/md/category!forward?_to_=selection',
                                        title : '选取分类',
                                        callback : function(item){
                                        	select(item.id, item.display);
                                        }
                                    })
                                } else if(rowdata.objectType == 'CUSTOMER'){// ! 客户选取
                                	$(this).popupDialog({
                                        url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                                        title : '选取客户',
                                        callback : function(item){
                                        	select(item.id, item.display);
                                        }
                                    })
                                } else if(rowdata.objectType == 'BRAND'){// ! 品牌选取
                                    $(this).popupDialog({
                                        url : WEB_ROOT + '/myt/md/brand!forward?_to_=selection',
                                        title : '选取品牌',
                                        callback : function(item){
                                            select(item.id, item.display);
                                        }
                                    })
                                } else if(rowdata.objectType == 'GP'){// ! 优惠券选取
                                    $(this).popupDialog({
                                        url : WEB_ROOT + '/myt/md/gift-paper!forward?_to_=selection',
                                        title : '选取优惠券',
                                        callback : function(item){
                                            select(item.id, item.title);
                                        }
                                    })
                                } else {
                                	alert("请先选择对象类型");
                                }
                            });
                        }
                    },
                    align : 'left'
                }, {
                    label : '是否可用',
                    name : 'enable',
                    width : 200,
                    formatter : 'checkbox',
                    editable: true,
                    align : 'center'
                } ],
                loadonce : true,
                multiselect : false,
                editurl : WEB_ROOT + '/myt/md/collection-object!doSave?collection.id='+row_id,
                delurl : WEB_ROOT + '/myt/md/collection-object!doDelete'
            });
        }
    });
});