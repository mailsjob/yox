<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-commodity-group-props2" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-commodity-group-props2").data("gridOptions", {
            url : "${base}/myt/md/commodity-group!props2?id=<s:property value='#parameters.id'/>",
            colModel : [ {
                label : '勾选',
                name : 'extraAttributes.checked',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            }, {
                label : '代码',
                name : 'code',
                width : 150,
                editable : true
            }, {
                label : '名称',
                name : 'name',
                editable : true,
                width : 100,
                editoptions : {
                    placeholder : '创建可留空自动生成'
                }
            }, {
                label : '排序号',
                name : 'orderRank',
                editable : true,
                width : 60
            }, {
                label : '级别',
                name : 'inheritLevel',
                editable : true,
                editrules : {
                    required : true,
                    number : true
                },
                align : 'center',
                width : 60
            } ],
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : WEB_ROOT + "/myt/md/commodity-group!propChildren?id=<s:property value='#parameters.id'/>&dynaPropLineId=" + row_id,
                    colModel : [ {
                        label : '勾选',
                        name : 'extraAttributes.checked',
                        edittype : 'checkbox',
                        editable : true,
                        align : 'center'
                    }, {
                        label : '代码',
                        name : 'code',
                        width : 150,
                        editable : true
                    }, {
                        label : '名称',
                        name : 'name',
                        editable : true,
                        width : 100,
                        editoptions : {
                            placeholder : '创建可留空自动生成'
                        }
                    }, {
                        label : '排序号',
                        name : 'orderRank',
                        editable : true,
                        width : 60
                    }, {
                        label : '级别',
                        name : 'inheritLevel',
                        editable : true,
                        editrules : {
                            required : true,
                            number : true
                        },
                        align : 'center',
                        width : 60
                    } ],
                    loadonce : true,
                    multiselect : false
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>