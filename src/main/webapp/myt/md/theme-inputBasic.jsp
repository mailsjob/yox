<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation" action="${base}/myt/md/theme!doSave"
	method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-theme">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">标题</label>
					<div class="controls">
						<s:textfield name="themeTitle" readonly="true"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">摘要</label>
					<div class="controls">
						<s:textarea name="allLabels" rows="5" readonly="true"></s:textarea>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">关注数</label>
							<div class="controls">
								<s:textfield name="focusCount" readonly="true"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">评论数</label>
							<div class="controls">
								<s:textfield name="commentCount" readonly="true" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">获得积分</label>
							<div class="controls">
								<s:textfield name="payedScore" readonly="true" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">客户</label>
							<div class="controls">
								<div class="input-icon right">
									<i class="fa fa-ellipsis-horizontal fa-select-customer-profile"></i>
									<s:textfield name="customerProfile.display" disabled="true" readonly="true" />
									<s:hidden name="customerProfile.id" disabled="true" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">图片</label>
					<div class="controls">
						<s:hidden name="themePic" data-singleimage="true" disabled="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">状态</label>
					<div class="controls">
						<s:radio name="themeStatus" list="#application.enums.themeStatusEnum" />
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
