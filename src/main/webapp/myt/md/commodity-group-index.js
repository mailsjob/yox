$(function() {
    $(".grid-md-commodity-group").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/commodity-group!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            width : 50
        }, {
            label : '代码',
            name : 'code',
            align : 'left',
            editable : true,
            width : 80
        }, {
            label : '名称',
            name : 'name',
            editable : true,
            width : 100
        } ],
        editcol : 'name',
        editurl : WEB_ROOT + "/myt/md/commodity-group!doSave",
        delurl : WEB_ROOT + "/myt/md/commodity-group!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/commodity-group!inputTabs",
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/md/commodity-group!commodityGroupDetails?id=" + row_id,
                colModel : [ {
                    label : '一维属性',
                    name : 'propLineValX.dynaPropLine.name',
                    width : '100',
                    align : 'center'
                }, {
                    label : '二维属性',
                    name : 'propLineValY.dynaPropLine.name',
                    width : '100',
                    align : 'center'
                }, {
                    label : '商品主键',
                    name : 'commodity.id',
                    hidden : true,
                    hidedlg : true,
                    width : '100',
                    editable : true
                }, {
                    label : '商品',
                    name : 'commodity.display',
                    index : 'commodity.sku_OR_commodity.title',
                    editable : true,
                    editoptions : Biz.getGridCommodityEditOptions(),
                    align : 'left'
                } ],
                loadonce : true,
                addable : false,
                editurl : WEB_ROOT + "/myt/md/commodity-group-detail!doSave",
            });
        }
    });
});