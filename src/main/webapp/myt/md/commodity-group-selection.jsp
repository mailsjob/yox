<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row search-form-default">
		<div class="col-md-12">
			<form method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-commodity-group-selection"
				action="#">
				<div class="form-group">
					<s:textfield name="search['CN_barcode_OR_sku_OR_title']" cssClass="form-control input-xlarge"
						placeholder="编码/条码/名称..." value="%{#parameters.keyword}" />
				</div>
				
				<button class="btn default hidden-inline-xs" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-commodity-group-selection"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-commodity-group-selection").data("gridOptions", {
            url : "${base}/myt/md/commodity-group!findByPage",
            colModel : [ {
                label : '流水号',
                name : 'id',
                width : 50,
                align : 'center'
            },{
                label : '编码',
                name : 'code',
                width : 80,
                align : 'center'
            }, {
                label : '名称',
                name : 'name',
                width : 100,
                sortable : false,
                align : 'left'
            }, {
                name : 'display',
                hidden : true,
                align : 'left'
            }],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
