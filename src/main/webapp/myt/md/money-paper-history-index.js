 $(".grid-md-money-paper-history").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/money-paper-history!findByPage',
        colModel : [  {
            label : '礼品卡代码',
            name : 'moneyPaper.code',
            width : 100
        }, {
            label : '礼品卡名称',
            name : 'moneyPaper.title',
            width : 100
        }, {
            label : '客户',
            name : 'moneyPaper.customerProfile.display',
            inde:'moneyPaper.customerProfile.trueName_OR_moneyPaper.customerProfile.nickName',
            width : 100
        }, {
            label : '面额',
            name : 'moneyPaper.faceValue',
            formatter : 'currency',
            width : 100
        },{
            label : '使用金额',
            name : 'amount',
            formatter : 'currency',
            width : 100
        }, {
            label : '时间',
            name : 'recordDatetime',
            sorttype:'date',
            width : 150,
            align : 'center'
        }, {
            label : '说明',
            formatter : 'textarea',
            name : 'reason',
            width : 200
        }, {
            label : '订单号',
            name : 'boxOrder.orderSeq',
            width : 100
        }],
        sortorder : "desc",
        sortname : "recordDatetime",
        multiselect : false
    });