<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="form-body">
<div class="row">
			<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-4">操作类型</label>
							<div class="col-md-8">
								<s3:select name="ruleType" id="commodityPriceRuleType" list="#{'DECREASE_BY_MONTH_RATE':'逐月折扣递减',
								'FREE_INPUT':'自由录入','DECREASE_BY_GRADE':'差额递减'}"/>
							</div>
						</div>
					</div>
				</div>
				</div>
<div class="div-md-commodity-price-rule-params"></div>
<script type="text/javascript">
    $().ready(function() {
        $("#commodityPriceRuleType").change(function() {
            var ruleType = $(this).val();
            if (ruleType != '') {
                if ($(".div-md-commodity-price-rule-params").html() == "" || confirm("更新规则类型将刷新页面，清除当前页面选项下方已配置数据，是否继续？")) {
                    $(".div-md-commodity-price-rule-params").ajaxGetUrl("${base}/myt/md/commodity-price!forward?_to_=rule-params&commoditySid=<s:property value='#parameters.commoditySid'/>&ruleType=" + ruleType);
                    
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>