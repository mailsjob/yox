<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<style>
.controls-categoryTimelines label.radio-inline {
	width: 120px;
	margin-left: 10px;
}

</style>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-md-recommend-commodity-inputBasic"
	action="${base}/myt/md/recommend-commodity!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-md-recommend-commodity">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">选择楼层</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-floor"></i>
							<s:textfield name="floor.displayLabel" readonly="true"/>
							<s:hidden name="floor.id" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">选择楼层菜单</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-floor-menu"></i>
							<i class="fa fa-times fa-clear-floor-menu"></i>
							<s:textfield name="floorMenu.display" readonly="true"/>
							<s:hidden name="floorMenu.id" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">时间线</label>
					<div class="controls controls-categoryTimelines">
						<s:radio name="timeline.id" list="categoryTimelinesMap" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">推荐类型</label>
					<div class="controls">
						<s:select name="promotionType" list="#application.enums.promotionTypeEnum" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">关联商品</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-commodity"></i>
							<i class="fa fa-times fa-clear-commodity"></i>
							<s:textfield name="commodity.display" />
							<s:hidden name="commodity.id" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">关联主题清单</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-theme"></i>
							<s:textfield name="them.display" />
							<s:hidden name="them.id" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">链接</label>
					<div class="controls">
						<s:textfield name="url" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">标题</label>
				<div class="controls">
					<s:textfield name="promotionTitle" />
				</div>
			</div>
		</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-4">图片</label>
							<div class="col-md-8">
								<s:hidden name="promotionPic" data-singleimage="true" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">是否可用</label>
							<div class="controls">
								<s:radio name="enable" list="#application.enums.booleanLabel"></s:radio>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">是否固定</label>
							<div class="controls">
								<s:radio name="fixed" list="#application.enums.booleanLabel"></s:radio>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">排序号</label>
							<div class="controls">
								<s:textfield name="orderRank" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	</div>
	</div>

	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-cb-lottery-win-list">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
    $(function() {
       $(".form-md-recommend-commodity-inputBasic").data(
                "formOptions",
                {
                    bindEvents : function() {
                        var $form = $(this);
                        $form.find(".fa-select-commodity").click(function() {
                            $(this).popupDialog({
                                url :  '${base}/myt/md/commodity!forward?_to_=selection',
                                title : '选取商品',
                                callback : function(rowdata) {
                                    $form.setFormDatas({
                                        'commodity.id' : rowdata.id,
                                        'commodity.display' : rowdata.display,
                                        'promotionPic' : rowdata.smallPic,
                                        'promotionTitle' : rowdata.title

                                    });
								}
                            })
                        });
                     
                        $form.find(".fa-clear-commodity").click(function() {
                            $form.setFormDatas({
                                'commodity.id' : '',
                                'commodity.display' : ''
                            });
                        });
                        $form.find(".fa-select-theme").click(function() {
                            $(this).popupDialog({
                                url : '${base}/myt/md/theme!forward?_to_=selection',
                                title : '选取主题清单',
                                callback : function(rowdata) {
                                    $form.setFormDatas({
                                        'them.id' : rowdata.id,
                                        'them.display' : rowdata.display
                                    });

                                }
                            })
                        });
                        $form.find(".fa-select-floor").click(function() {
                            $(this).popupDialog({
                                url :'${base}/myt/md/floor!forward?_to_=selection',
                                title : '选取楼层',
                                callback : function(rowdata) {
                                    $form.setFormDatas({
                                        'floor.id' : rowdata.id,
                                        'floor.displayLabel' : rowdata.displayLabel
                                    });

                                }
                            })
                        });
                        
                        $form.find(".fa-select-floor-menu").click(function() {
                            var floorId=$form.find("input[name='floor.id']").val();
                            $(this).popupDialog({
                                url : '${base}/myt/md/floor-menu!forward?_to_=selection&floorId='+floorId,
                                title : '选取楼层菜单',
                                callback : function(rowdata) {
                                    $form.setFormDatas({
                                        'floorMenu.id' : rowdata.id,
                                        'floorMenu.display' : rowdata.name
                                    });

                                }
                            })
                        });
                        $form.find(".fa-clear-floor-menu").click(function() {
                            $form.setFormDatas({
                                'floorMenu.id' : '',
                                'floorMenu.display' : ''
                            });
                        });

                    }
                });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>