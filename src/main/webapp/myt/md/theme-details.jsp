<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-theme-details" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-theme-details").data("gridOptions", {
            url : "${base}/myt/md/theme!themeDetails?id=<s:property value='#parameters.id'/>",
            colModel : [{
                label:'商品',
                name : 'commodity.display',
                index:'commodity.sku_OR_commodity.title',
                width : 150
            }, {
                label:'数量',
                name : 'commodityQuantity',
                width : 80
            }, {
                label:'月',
                name : 'themeMonth',
                width : 80
            }, {
                label:'年份偏移',
                name : 'yearOffset',
                width : 80
            }, {
                label:'排序号',
                name : 'orderIndex',
                width : 80,
                align : 'center'
            }, {
                label : '商品来源',
                stype : 'select',
                name : 'commodityFrom',
                align : 'center',
                searchoptions : {
                    value : Util.getCacheEnumsByType('commodityFromEnum')
                },
                 width : 80
            } ],
            inlineNav : {
                add : true
            }
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>