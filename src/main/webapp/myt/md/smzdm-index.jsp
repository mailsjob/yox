<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">列表查询</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-myt-md-smzdm-index">
						<div class="input-group">
							<div class="form-group" style="width: 500px;">
								<input type="text" name="search['CN_title_OR_commodity.title_OR_commodity.sku']" class="form-control" placeholder="标题、商品名、商品编号...">
							</div>
							<div class="form-group">
								<div class="btn-group">
									<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
										适用阶段 <i class="fa fa-angle-down"></i>
									</button>
									<div class="dropdown-menu hold-on-click dropdown-checkboxes">
										<s:checkboxlist name="search['IN_fitStage']" list="#application.enums.fitStageEnum"
											value="#application.enums.fitStageEnum.keys" />
									</div>
								</div>
							</div>
							<button class="btn green" type="submmit">
								<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
							</button>
							<button class="btn default hidden-inline-xs" type="reset">
								<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
							</button>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-smzdm-index"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/md/smzdm-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
