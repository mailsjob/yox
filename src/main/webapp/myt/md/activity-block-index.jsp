<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form action="#" method="get" class="form-inline form-validation  form-search-init"
			data-grid-search=".grid-md-activity-block">
			<div class="input-group">
				<div class="input-cont">
					<input type="text"
						name="search['CN_activity.activityCode_OR_activity.activityName_OR_locationCode_OR_blockTitle']"
						class="form-control" placeholder="活动、代码、名称...">
				</div>
				<span class="input-group-btn">
					<button class="btn green" type="submmit">
						<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
					</button>
					<button class="btn default hidden-inline-xs" type="reset">
						<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
					</button>
				</span>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-md-activity-block"></table>
	</div>
</div>
<script src="${base}/myt/md/activity-block-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>