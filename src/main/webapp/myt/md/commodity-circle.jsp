<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation" action="${base}/myt/md/commodity!doSave"
	method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">周期购标识</label>
					<div class="controls">
						<s:radio name="circle" list="#application.enums.booleanLabel" />
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">周期购标题</label>
					<div class="controls">
						<s:textfield name="circleTitle" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">3月周期价</label>
					<div class="controls">
						<s:textfield name="commodityVaryPrice.m3CirclePrice" />
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">4月周期价</label>
					<div class="controls">
						<s:textfield name="commodityVaryPrice.m4CirclePrice" />
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">5月周期价</label>
					<div class="controls">
						<s:textfield name="commodityVaryPrice.m5CirclePrice" />
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">6月周期价</label>
					<div class="controls">
						<s:textfield name="commodityVaryPrice.m6CirclePrice" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">1级代理商销售价格</label>
					<div class="controls">
						<s:textfield name="commodityVaryPrice.level1AgentSalePrice" />
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">2级代理商销售价格</label>
					<div class="controls">
						<s:textfield name="commodityVaryPrice.level2AgentSalePrice" />
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">3级代理商销售价格</label>
					<div class="controls">
						<s:textfield name="commodityVaryPrice.level3AgentSalePrice" />
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">&nbsp;</label>
					<div class="controls">
						&nbsp;
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">周期购描述</label>
					<div class="controls">
						<s:textarea name="circleSpecification" data-htmleditor="kindeditor" data-height="500px" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
