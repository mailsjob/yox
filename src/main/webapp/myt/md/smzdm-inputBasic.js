$(function() {
    $(".form-myt-md-smzdm-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find("i.fa-search-commodity").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT+'/myt/md/commodity!forward?_to_=selection',
                    title : '选取商品',
                    callback : function(json) {
                        $form.find("[name$='commodity.display']").val(json.sku + " " + json.title);
                        $form.find("[name$='commodity.id']").val(json.id);
                    }
                })
            });
            $form.find(".fa-clear-commodity").click(function() {
                $form.setFormDatas({
                    'commodity.id' : '',
                    'commodity.display' : ''
                });
            });
        }
    });
});