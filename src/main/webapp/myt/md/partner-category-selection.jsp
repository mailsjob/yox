<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row">
	<div class="col-md-12">
		<table class="grid-partner-category-select-list" data-grid="table" data-selected="<s:property value='#parameters.selected'/>"></table>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-partner-category-select-list").data("gridOptions", {
            url : "${base}/myt/md/partner-category!treeGridData",
            colNames : [ '名称', '选取值' ],
            colModel : [ {
                name : 'name',
                align : 'left'
            }, {
                name : 'name',
                hidden : true
            } ],
            cmTemplate : {
                sortable : false
            },
            height : 400,
            pager : false,
            toppager : false,
            treeGrid : true,
            treeGridModel : 'adjacency',
            onSelectRow : function(id) {
                var $grid = $(this);
                var $containerCallback = $grid.closest(".container-callback");
                $containerCallback.hide();
                var callback = $containerCallback.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);

                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }

            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>