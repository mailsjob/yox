$(function() {
    $(".grid-md-news").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/news!findByPage",
        colModel : [ {
            label : '新闻标题',
            name : 'newsTitle',
            align : 'left',
            width : 200
        },{
            label : '新闻图片',
            name : 'newsImg',
            sortable : false,
            search : false,
            width : 80,
            hidden:true,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '发布日期',
            name : 'newsDate',
            sorttype:'date',
            align : 'center',
            width : 150
        }, {
            label : '相对排序号',
            name : 'newsRank',
            editable : true,
            width : 80,
            align : 'center'
        }, {
            label : '新闻来源',
            name : 'newsFrom',
            editable : true,
            width : 80,
            align : 'center'
        }, {
            label : '新闻来源链接',
            name : 'newsFromUrl',
            editable : true,
            width : 150,
            align : 'right'
        }, {
            label : '下架标记',
            name : 'hiddenFlag',
            editable : true,
            width : 80,
            align : 'right'
        }],
        editcol:'newsTitle',
        editurl : WEB_ROOT + "/myt/md/news!doSave",
        delurl : WEB_ROOT + "/myt/md/news!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/news!inputTabs",
    });
   
});