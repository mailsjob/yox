$(function() {
    $(".grid-md-gift-paper").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/gift-paper!findByPage?search['EQ_paperMode']=P",
        colModel : [ {
            label : '流水号',
            name : 'id',
            width : 70
        }, {
            label : '时间',
            name : 'createdDate',
            width : 120,
            align : 'center'
        }, {
            label : '模板号',
            name : 'paperTemplateCode',
            width : 150
        }, {
            label : '优惠券代码',
            name : 'code',
            align : 'left',
            width : 150,
        }, {
            label : '优惠券名称',
            name : 'title',
            align : 'left',
            width : 200,
        }, {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName_OR_customerProfile.trueName',
            align : 'left',
            width : 100,
        }, {
            label : '开始时间',
            name : 'beginTime',
            sorttype : 'date',
            align : 'center',
            width : 100
        }, {
            label : '过期时间',
            name : 'expireTime',
            sorttype : 'date',
            align : 'center',
            width : 100
        }, {
            label : '类型',
            name : 'paperMode',
            width : 80,
            align : 'center'
        }, {
            label : '状态',
            name : 'paperStatus',
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('giftPaperStatusEnum')
            }
        }, {
            label : '面额',
            name : 'faceValue',
            align : 'right',
            width : 100
        }, {
            label : '来源',
            name : 'paperSource',
            width : 100
        } ],
        sortorder : "desc",
        sortname : "createdDate",
        fullediturl : WEB_ROOT + "/myt/md/gift-paper!inputTabs?paperType=P"
    });

    $(".grid-md-gift-paper-template").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/gift-paper!findByPage?search['EQ_paperMode']=G",
        colModel : [ {
            label : '流水号',
            name : 'id',
            width : 50
        }, {
            label : '时间',
            name : 'createdDate',
            width : 120,
            align : 'center'
        }, {
            label : '模板号',
            name : 'paperTemplateCode',
            width : 250
        }, {
            label : '优惠券代码',
            name : 'code',
            align : 'left',
            width : 150,
        }, {
            label : '优惠券名称',
            name : 'title',
            align : 'left',
            width : 200,
        }, {
            label : '开始时间',
            name : 'beginTime',
            sorttype : 'date',
            align : 'center',
            width : 100
        }, {
            label : '过期时间',
            name : 'expireTime',
            sorttype : 'date',
            align : 'center',
            width : 100
        }, {
            label : '类型',
            name : 'paperMode',
            width : 80,
            align : 'center'
        }, {
            label : '状态',
            name : 'paperStatus',
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('giftPaperStatusEnum')
            }
        }, {
            label : '面额',
            name : 'faceValue',
            align : 'right',
            width : 100
        }, {
            label : '来源',
            name : 'paperSource',
            width : 100
        } ],
        sortorder : "desc",
        sortname : "createdDate",
        editurl : WEB_ROOT + "/myt/md/gift-paper!doSave",
        delurl : WEB_ROOT + "/myt/md/gift-paper!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/gift-paper!inputTabs?paperType=G"
    });
});