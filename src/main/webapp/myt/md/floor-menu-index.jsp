<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="row search-form-default">
	<div class="col-md-12">
		<form action="#" method="get" class="form-inline form-validation  form-search-init"
			data-grid-search=".grid-md-floor-menu">
			<div class="input-group">
				<div class="input-cont">
					<input type="text"
						name="search['CN_name_OR_floor.monthText']"
						class="form-control" placeholder="名称、楼层...">
				</div>
				<span class="input-group-btn">
					<button class="btn green" type="submmit">
						<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
					</button>
					<button class="btn default hidden-inline-xs" type="reset">
						<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
					</button>
				</span>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-md-floor-menu"></table>
	</div>
</div>
<script src="${base}/myt/md/floor-menu-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
