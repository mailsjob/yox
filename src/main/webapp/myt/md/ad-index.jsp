<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">广告</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-md-ad" action="#">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_adPositionCode_OR_adName_OR_url']"
								 class="form-control" placeholder="位置代码、广告名称、链接……">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-ad" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script src="${base}/myt/md/ad-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>