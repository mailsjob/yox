<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-commodity-sku-relation" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-commodity-sku-relation" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="div-md-commodity-price-input-batch"></div>
<script type="text/javascript">
$(function() {
    $(".grid-myt-md-commodity-sku-relation").data("gridOptions", {
        url : "${base}/myt/md/commodity!getCommoditySkuRelation?id=<s:property value='#parameters.id'/>",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '外部商品标题',
            name : 'outsideTitle',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '外部商品SKU',
            name : 'outsideSku',
            width : 255,
            editable: true,
            align : 'left'
        }, {
            label : '商品来源',
            name : 'source',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('outsideCommoditySourceTypeEnum')
            },
            width : 80,
            editable: true,
            align : 'center'
        }],
        editurl : '${base}/myt/md/commodity-sku-relation!doSave?commodity.id='+<s:property value='#parameters.id'/>,
        delurl : '${base}/myt/md/commodity-sku-relation!doDelete',
    });
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>
