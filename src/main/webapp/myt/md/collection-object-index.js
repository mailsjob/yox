$(function() {
    $(".grid-myt-md-collection-object-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/collection-object!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '对应集合',
            name : 'collection.display',
            index : 'collection.title',
            width : 200,
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '对象SID',
            name : 'objectSid',
            width : 60,
            editable: true,
            align : 'center',
            hidden : true
        }, {
            label : '对象名',
            name : 'objectDisplay',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '对象类型',
            name : 'objectType',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('collectionObjectType')
            },
            width : 80,
            editable: true,
            align : 'center'
        }, {
            label : '是否可用',
            name : 'enable',
            width : 50,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }],
        postData: {
           "search['FETCH_collection']" : "INNER"
        },
        editurl : WEB_ROOT + '/myt/md/collection-object!doSave',
        delurl : WEB_ROOT + '/myt/md/collection-object!doDelete',
        fullediturl : WEB_ROOT + '/myt/md/collection-object!inputTabs'
    });
});
