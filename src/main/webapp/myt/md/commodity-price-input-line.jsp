<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<form class="form-horizontal form-bordered form-label-stripped form-validation form-commodity-price-input-line"
		action="${base}/myt/md/commodity-price!doBatchLine" method="post">
		<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-md-commodity-price">
			<i class="fa fa-check"></i> 保存价格计划
		</button>
		<s:radio  name="defaultPriceStrategy"
        list="#{'NONE':'不做自动处理','FORCE_LAST':'始终自动把最后一个月设置为缺省','LAST_IF_UNSET':'如果当前没有设置默认则自动取最后一个月，如果当前已设置则保持不变'}"></s:radio>
	
	</div>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-bordered table-commodity-price-input-line" data-dynamic-table="true">
						<thead>
							<tr>
								<th >计划年</th>
								<th style="width: 40px">当月</th>
								<th style="width: 40px">+1月</th>
								<th style="width: 40px">+2月</th>
								<th style="width: 40px">+3月</th>
								<th style="width: 40px">+4月</th>
								<th style="width: 40px">+5月</th>
								<th style="width: 40px">+6月</th>
								<th style="width: 40px">+7月</th>
								<th style="width: 40px">+8月</th>
								<th style="width: 40px">+9月</th>
								<th style="width: 40px">+10月</th>
								<th style="width: 40px">+11月</th>
							</tr>
						</thead>
						<tbody>
							 <s:iterator value="commodityPrices" status="status" var="item" >
							 	 <s:if test="#item.sid==null">
				               	 <tr class="dynamicRowItem">
				                </s:if>
				                <s:else>
				               		 <tr class="dynamicRowItem" style="background: #BBFFBB">
				                </s:else>
									<s:hidden name="%{'commodityPrices['+#status.index+'].id'}"/>
		                    		<s:hidden name="%{'commodityPrices['+#status.index+'].source.id'}"/>
		                    		<s:hidden name="%{'commodityPrices['+#status.index+'].extraAttributes.operation'}" value="update" />
		                    		<td><s:property value="%{#item.priceYear}"/>-<s:property value="%{#item.priceMonth}"/></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m0Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m1Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m2Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m3Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m4Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m5Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m6Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m7Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m8Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m9Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m10Price'}"   /></td>
		                    		<td><s3:textfield name="%{'commodityPrices['+#status.index+'].m11Price'}"   /></td>
		                    	</tr>
							</s:iterator>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	</form>

<%@ include file="/common/ajax-footer.jsp"%>
