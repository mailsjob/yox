<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<div class="tabbable">
    <ul class="nav nav-tabs ">
        <li class="active"><a data-toggle="tab" href="#tab-auto" class="tab-province">省/直辖市</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active tab-pane-province"></div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        var regionDatas = Biz.getRegionDatas();
        $(".tab-pane-province").each(function () {
            var $tabbable = $(this).closest(".tab-content").parent();
            var $province = $(this);
            var html = [];
            $.each(regionDatas, function (i, item) {
                var name = item.name;
                if (item.pinyinShort) {
                    name = item.pinyinShort.substring(0, 1) + "." + name;
                }
                html.push('<div class="col-md-2"><button id="' + item.id + '" class="btn btn-link" data-toggle="button" type="button" data-name="' + item.name + '">' + name + '</button></div>');
            });
            $province.html(html.join(""));
            $province.on('click', 'button', function () {
                var $button = $(this);
                $province.find(".btn").removeClass("default");
                $button.addClass("default");
                var curId = $button.attr("id");
                var provinceName = $button.attr("data-name");
                var $dialog = $button.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    callback.call($button, {
                        id: curId,
                        display: provinceName
                    });
                    if ($button.attr("init-select")) {
                        $button.removeAttr("init-select");
                    } else {
                        $dialog.hide();
                    }
                    $province.find(".btn").removeClass("default");
                    $button.addClass("default");
                }
            });

            var selected = "<s:property value='#parameters.selected'/>";
            if (selected) {
                var splits = selected.split(",");
                $.each(splits, function (i, item) {
                    var name = $.trim(item);
                    var $checbox = $tabbable.find(".tab-content > div.tab-pane").find("[data-name='" + name + "']");
                    $checbox.attr("checked", true);
                });

            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp" %>