<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">推荐配件列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search" data-grid-search=".grid-md-recommend-fitting-index" action="#">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_sourceDisplay_OR_destDisplay']" class="form-control" placeholder="源对象、目标对象">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-md-recommend-fitting-index" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(function() {
        $(".grid-md-recommend-fitting-index").data("gridOptions", {
            url : "${base}/myt/md/recommend-fitting!findByPage",
            colModel : [ {
                label : '排序号',
                name : 'orderIndex',
                editable : true,
                width:150,
                align : 'center'
            },{
                label : '源商品键',
                name : 'sourceSid',
                hidden : true,
                hidedlg : true,
                editable : true
            }, {
                label : '源类型',
                name : 'sourceType',
                width : 120,
                editable : true,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('sourceTypeEnum')
                }
            }, {
                label : '源对象',
                name : 'sourceDisplay',
               	editable : true,
                width : 200,
                editrules : {
                    required : true
                },
                editoptions : {
                    dataInit : function(elem) {
                       var $grid = $(this);
                       var $elem = $(elem);
                     	$grid.find("select[name='sourceType']").change(function() {
                         	if($(this).val()=='CO'){
                           	var selectSource = function(item) {
      	                             // 强制覆盖已有值
      	                             $grid.jqGrid("setEditingRowdata", {
      	                                 'sourceSid' : item.id,
      	                                 'sourceDisplay' : item.display
      	                             });
      	  						}
        	                    $elem.wrap('<div class="input-icon right"/>');
        	                    $elem.before('<i class="fa fa-ellipsis-horizontal fa-select-source"></i>');
        	                    $grid.find(".fa-select-source").click(function() {
        	                       $(this).popupDialog({
                                     url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                                     title : '选取商品',
                                     callback : selectSource
                                 })
        	                   });
                             }else if($(this).val()=='CA'){
                                     $elem.treeselect({
                                         url : WEB_ROOT + "/myt/md/category!categoryList",
                                         callback : {
                                             onSingleClick : function(event, treeId, treeNode) {
                                                 $grid.jqGrid("setEditingRowdata", {
                                                     'sourceSid' : Util.startWith(treeNode.id, "-") ? "" : treeNode.id,
                                                     'sourceDisplay' : Util.startWith(treeNode.id, "-") ? "" : treeNode.name
                                                 });
                                             },
                                             onClear : function(event) {
                                                 $grid.jqGrid("setEditingRowdata", {
                                                     'sourceSid' : '',
                                                     'sourceDisplay' : ''
                                                 });
                                             }
                                         }
                                     });
                                 } 
                       })
                    }
                }
            }, {
                label : '赠品主键',
                name : 'destSid',
                hidden : true,
                hidedlg : true,
                editable : true
            }, {
                label : '源类型',
                name : 'destType',
                width : 120,
                editable : true,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('sourceTypeEnum')
                }
            }, {
                label : '目标对象',
                name : 'destDisplay',
                editable : true,
                width : 200,
                editrules : {
                    required : true
                },
                editoptions : {
                    dataInit : function(elem) {
                       var $grid = $(this);
                       var $elem = $(elem);
                     	$grid.find("select[name='destType']").change(function() {
                         	if($(this).val()=='CO'){
                           	var selectDest = function(item) {
      	                             // 强制覆盖已有值
      	                             $grid.jqGrid("setEditingRowdata", {
      	                                 'destSid' : item.id,
      	                                 'destDisplay' : item.display
      	                             });
      	  						}
        	                    $elem.wrap('<div class="input-icon right"/>');
        	                    $elem.before('<i class="fa fa-ellipsis-horizontal fa-select-dest"></i>');
        	                    $grid.find(".fa-select-dest").click(function() {
        	                       $(this).popupDialog({
                                     url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                                     title : '选取商品',
                                     callback : selectDest
                                 })
        	                   });
                             }else if($(this).val()=='CA'){
                                     $elem.treeselect({
                                         url : WEB_ROOT + "/myt/md/category!categoryList",
                                         callback : {
                                             onSingleClick : function(event, treeId, treeNode) {
                                                 $grid.jqGrid("setEditingRowdata", {
                                                     'destSid' : Util.startWith(treeNode.id, "-") ? "" : treeNode.id,
                                                     'destDisplay' : Util.startWith(treeNode.id, "-") ? "" : treeNode.name
                                                 });
                                             },
                                             onClear : function(event) {
                                                 $grid.jqGrid("setEditingRowdata", {
                                                     'destSid' : '',
                                                     'destDisplay' : ''
                                                 });
                                             }
                                         }
                                     });
                                 } 
                       })
                    }
                }
            }],
            editurl : "${base}/myt/md/recommend-fitting!doSave",
            delurl : "${base}/myt/md/recommend-fitting!doDelete",
            fullediturl : "${base}/myt/md/recommend-fitting!inputTabs",
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>