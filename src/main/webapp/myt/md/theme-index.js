$(function() {
    $(".grid-md-theme").data("gridOptions", {
        url : WEB_ROOT + '/myt/md/theme!findByPage',
        colModel : [ {
            label : '下单客户',
            name : 'customerProfile.display',
            index:'customerProfile.trueName_OR_customerProfile.nickName',
            align : 'left',
            width : 80,
            formatter : function(cellValue, options, rowdata, action) {
                var url = "${base}/myt/customer/customer-profile!view?id=" + rowdata.customerProfile.id;
                return '<a href="' + url + '" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
            }
        },{
            label : '图片',
            name : 'themePic',
            sortable : false,
            search : false,
            width : 80,
            hidden:true,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '主题标题',
            name : 'themeTitle',
            width : 180
        }, {
            label : '状态',
            name : 'themeStatus',
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('themeStatusEnum')
            }
        }, {
            label : '排序号',
            name : 'orderIndex',
            editable : true,
            width : 80,
            align : 'center'
        }, {
            label : '关注数',
            name : 'focusCount',
            width : 150,
            align : 'right'
        }, {
            label : '评论数',
            name : 'commentCount',
            width : 80,
            align : 'right'
        }, {
            label : '标签',
            name : 'allLabels',
            hidden:true,
            width : 180
        }, {
            label : '获得积分',
            name : 'payedScore',
            width : 80,
            align : 'right'
        }],
        addable:false,
        inlineNav:{
            add:false
        },
        editurl : WEB_ROOT + "/myt/md/theme!doSave",
        fullediturl : WEB_ROOT + "/myt/md/theme!inputTabs",
    });
   
});