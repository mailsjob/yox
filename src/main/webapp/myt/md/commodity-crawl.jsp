<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation" action="${base}/myt/md/commodity!doCrawl"
	method="post">
	<s:hidden name="id" />
	<s:hidden name="version" value="0" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i>抓取
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-2">抓取链接<br>（一行一个URL）</label>
						<div class="col-md-10">
							<s:textarea name="url" rows="10"/>
						</div>
					</div>
				</div>
		</div>
		<div class="row">
		<div class="col-md-12">
				<p>支持URL语法及示例：</p>
				<ul>
					<li>京东单品：http://item.jd.com/(\d*).html，如：http://item.jd.com/1060185211.html</li>
					<li>京东列表：
						<ul>
							<li>单个列表页面：http://mall.jd.com/view_search-171887-0-0-1-24-1.html</li>
							<li>区间列表页面：http://mall.jd.com/view_search-171887-0-0-1-24-[10-20].html</li>
							<li>区间列表页面：http://mall.jd.com/view_search-171887-0-0-1-24-[10-*].html</li>
						</ul>
					</li>
					<li>天猫单品：http://detail.tmall.*item.htm\?.*id=.*</li>
					<li>天猫列表：http://.*.tmall.com/category.htm.*pageNo=.*</li>
					<li>淘宝单品：http://item.taobao.com/item.htm?.*id=.*</li>
					<li>淘宝列表：http://.*.taobao.com/.*pageNo=.*</li>	
					<li>苏宁单品：http://product.suning.com/(\d*).html</li>	
				</ul>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 抓取
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
