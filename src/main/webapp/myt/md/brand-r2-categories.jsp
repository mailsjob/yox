<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-md-brand-r2-categories" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-md-brand-r2-categories").data("gridOptions", {
            url : "${base}/myt/md/brand-r2-category!findByPage?search['EQ_brand.id']=<s:property value='#parameters.id'/>",
           	colModel : [  {
                label : '分类主键',
                name : 'category.id',
                hidden : true,
                hidedlg : true,
                editable : true
            }, {
                label : '分类',
                name : 'category.display',
                index : 'category.categoryCode_OR_category.categoryCode',
                width : 150,
                sortable : false,
                editable : true,
                align : 'left',
                editoptions : {
                    dataInit : function(elem, opt) {
                        var $grid = $(this);
                        var $elem = $(elem);
                        $elem.treeselect({
                            url : WEB_ROOT + "/myt/md/category!categoryList",
                            callback : {
                                onSingleClick : function(event, treeId, treeNode) {
                                    $grid.jqGrid("setEditingRowdata", {
                                        'category.id' : Util.startWith(treeNode.id, "-") ? "" : treeNode.id,
                                        'category.display' : Util.startWith(treeNode.id, "-") ? "" : treeNode.name
                                    });
                                },
                                onClear : function(event) {
                                    $grid.jqGrid("setEditingRowdata", {
                                        'category.id' : '',
                                        'category.display' : ''
                                    });
                                }
                            }
                        });
                    }
                },
                responsive : 'sm'
            }, {
                label : '排序号',
                name : 'orderIndex',
                editable : true,
                width : 100,
                sorttype : 'number',
                align : 'right'
            },{
                label : '是否隐藏',
                name : 'hiddenFlag',
                edittype : 'checkbox',
                editable : true,
                align : 'center'
            }],
            sortorder : "desc",
            sortname : "orderIndex",
           	editurl : "${base}/myt/md/brand-r2-category!doSave?brand.id=<s:property value='#parameters.id'/>",
            delurl : "${base}/myt/md/brand-r2-category!doDelete"
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>