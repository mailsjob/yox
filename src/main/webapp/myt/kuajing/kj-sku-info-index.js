$(function() {
    $(".grid-myt-kuajing-kj-sku-info-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/kuajing/kj-sku-info!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
        	label : '申报类型',
            name : 'actionType',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('kjActionTypeEnum')
            },
            width : 80,
            editable: true,
            align : 'center'
        }, {
            label : '商品货号',
            name : 'sku',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '入区计量单位',
            name : 'inAreaUnit',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '商品名称',
            name : 'goodsName',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '行邮税号',
            name : 'postTaxNO',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '入区计量单位折算数量',
            name : 'convInAreaUnitNum',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '外部系统编号',
            name : 'externalNo',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '法定计量单位折算数量',
            name : 'convLegalUnitNum',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '申报计量单位',
            name : 'declareUnit',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '电商企业名称',
            name : 'eshopEntName',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }, {
            label : '商品HS编码',
            name : 'hsCode',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '施检机构的代码',
            name : 'checkOrgCode',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }, {
            label : '法定计量单位',
            name : 'legalUnit',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '规格型号',
            name : 'goodesSpec',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '试点商品',
            name : 'isExperimentGoods',
            width : 200,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }, {
            /*label : '外企在中国注册备案（食药监局、国家认监委）',*/
        	label : '外企备案',
            name : 'isCncaPor',
            width : 200,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }, {
            label : '供应商名称',
            name : 'supplierName',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }, {
            label : '国家地区代码',
            name : 'originCountryCode',
            width : 200,
            editable: true,
            align : 'left',
            hidden : true
        }],
        addable : false,
        fullediturl : WEB_ROOT + '/myt/kuajing/kj-sku-info!inputTabs'
    });
});
