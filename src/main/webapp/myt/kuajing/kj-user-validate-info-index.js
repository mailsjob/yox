$(function() {
    $(".grid-myt-kuajing-kj-user-validate-info-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/kuajing/kj-user-validate-info!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '真实姓名',
            name : 'trueName',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '身份证号',
            name : 'idNumber',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '电话',
            name : 'mobilePhone',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '备注',
            name : 'memo',
            width : 200,
            editable: true,
            align : 'left'
        } ],
        editurl : WEB_ROOT + '/myt/kuajing/kj-user-validate-info!doSave'
    });
});
