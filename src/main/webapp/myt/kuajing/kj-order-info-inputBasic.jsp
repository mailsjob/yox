<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-kuajing-kj-order-info-inputBasic"
	data-editrulesurl="${base}/myt/kuajing/kj-order-info!buildValidateRules">
	<div class="form-body">
		<div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">申报海关代码</label>
					<div class="controls">
		                <s:textfield name="customersCode" disabled="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">业务类型</label>
					<div class="controls">
		                <s:textfield name="bizTypeCode" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">电商企业代码</label>
					<div class="controls">
		                <s:textfield name="eshopEntCode" disabled="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">电商企业名称</label>
					<div class="controls">
		                <s:textfield name="eshopEntName" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">原始单号</label>
					<div class="controls">
		                <s:textfield name="originalOrderNo" disabled="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">货款总额</label>
					<div class="controls">
		                <s:textfield name="goodsFee" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">税金总额</label>
					<div class="controls">
		                <s:textfield name="taxFee" disabled="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">毛重</label>
					<div class="controls">
		                <s:textfield name="grossWeight" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">收货人姓名</label>
					<div class="controls">
		                <s:textfield name="receiverName" disabled="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">收货人身份证号码</label>
					<div class="controls">
		                <s:textfield name="receiverIdNo" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">收货人电话</label>
					<div class="controls">
		                <s:textfield name="receiverTel" disabled="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">收货人地址</label>
					<div class="controls">
		                <s:textfield name="receiverAddress" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">起运国</label>
					<div class="controls">
		                <s:textfield name="despArriCountryCode" disabled="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">运输方式</label>
					<div class="controls">
		                <s:textfield name="shipToolCode" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">代理企业名称</label>
					<div class="controls">
		                <s:textfield name="proxyEntName" disabled="true"/>
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">代理企业代码</label>
					<div class="controls">
		                <s:textfield name="proxyEntCode" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">分拣线ID</label>
					<div class="controls">
		                <s:textfield name="sortlingId" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">xmlStr</label>
					<div class="controls">
		                <s:textarea name="xmlStr" disabled="true" rows="10"/>
					</div>
				</div>
            </div>
        </div>
	</div>
</form>
<script src="${base}/myt/kuajing/kj-order-info-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>