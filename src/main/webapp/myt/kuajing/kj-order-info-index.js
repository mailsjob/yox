$(function() {
    $(".grid-myt-kuajing-kj-order-info-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/kuajing/kj-order-info!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        },{
            label : '时间',
            name : 'createdDate',
            width : 120,
            align : 'center'
        }, {
            label : '原始单号',
            name : 'originalOrderNo',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '货款总额',
            name : 'goodsFee',
            width : 80,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '税金总额',
            name : 'taxFee',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '毛重',
            name : 'grossWeight',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '收货人身份证号码',
            name : 'receiverIdNo',
            width : 200,
            editable: true,
            align : 'center'
        }, {
            label : '收货人姓名',
            name : 'receiverName',
            width : 100,
            editable: true,
            align : 'center'
        }, {
            label : '收货人电话',
            name : 'receiverTel',
            width : 150,
            editable: true,
            align : 'center'
        }, {
            label : '收货人地址',
            name : 'receiverAddress',
            width : 250,
            editable: true,
            align : 'left'
        }, {
            label : '起运国',
            name : 'despArriCountryCode',
            width : 60,
            editable: true,
            align : 'left'
        } ],
        inlineNav : {
            add : false
        },
        addable : false,
        fullediturl : WEB_ROOT + '/myt/kuajing/kj-order-info!inputTabs'
    });
});
