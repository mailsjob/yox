$(function() {
    $(".grid-myt-kuajing-myt-kj-sku-info-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/kuajing/myt-kj-sku-info!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '法定计量单位',
            name : 'legalUnit',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '商品名称',
            name : 'goodsName',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '入区计量单位',
            name : 'inAreaUnit',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '规格型号',
            name : 'goodesSpec',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '行邮税号',
            name : 'postTaxNO',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '入区计量单位折算数量',
            name : 'convInAreaUnitNum',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '外部系统编号',
            name : 'externalNo',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '商品货号',
            name : 'sku',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '法定计量单位折算数量',
            name : 'convLegalUnitNum',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '商品HS编码',
            name : 'hsCode',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '电商企业代码',
            name : 'eshopEntCode',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '电商企业名称',
            name : 'eshopEntName',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '申报计量单位',
            name : 'declareUnit',
            width : 200,
            editable: true,
            align : 'left'
        } ],
        editurl : WEB_ROOT + '/myt/kuajing/myt-kj-sku-info!doSave',
        fullediturl : WEB_ROOT + '/myt/kuajing/myt-kj-sku-info!inputTabs',
        operations : function(itemArray) {

            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 新增</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                alert("新增");
            });
            itemArray.push($select);
            
            var $select2 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 变更</a></li>');
            $select2.children("a").bind("click", function(e) {
                e.preventDefault();
                alert("变更");
            });
            itemArray.push($select2);
            
            var $select3 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 暂停</a></li>');
            $select3.children("a").bind("click", function(e) {
                e.preventDefault();
                alert("暂停");
            });
            itemArray.push($select3);

        }
    });
});
