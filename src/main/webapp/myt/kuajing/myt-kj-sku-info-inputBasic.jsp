<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-kuajing-myt-kj-sku-info-inputBasic"
	action="${base}/myt/kuajing/myt-kj-sku-info!doSave" method="post" 
	data-editrulesurl="${base}/myt/kuajing/myt-kj-sku-info!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-kuajing-myt-kj-sku-info-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">电商企业代码</label>
					<div class="controls">
		                <s:textfield name="eshopEntCode" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">电商企业名称</label>
					<div class="controls">
		                <s:textfield name="eshopEntName" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">外部系统编号</label>
					<div class="controls">
		                <s:textfield name="externalNo" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品名称</label>
					<div class="controls">
		                <s:textfield name="goodsName" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品货号</label>
					<div class="controls">
		                <s:textfield name="sku" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">规格型号</label>
					<div class="controls">
		                <s:textfield name="goodesSpec" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">法定计量单位</label>
					<div class="controls">
		                <s:textfield name="legalUnit" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">国外生产企业是否在中国注册备案（食药监局、国家认监委）</label>
					<div class="controls">
		                <s:radio name="isCncaPor" list="#application.enums.booleanLabel"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">供应商名称</label>
					<div class="controls">
		                <s:textfield name="supplierName" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">国家地区代码</label>
					<div class="controls">
		                <s:textfield name="originCountryCode" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">入区计量单位</label>
					<div class="controls">
		                <s:textfield name="inAreaUnit" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">行邮税号</label>
					<div class="controls">
		                <s:textfield name="postTaxNO" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">入区计量单位折算数量</label>
					<div class="controls">
		                <s:textfield name="convInAreaUnitNum" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">是否试点商品</label>
					<div class="controls">
		                <s:radio name="isExperimentGoods" list="#application.enums.booleanLabel"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">法定计量单位折算数量</label>
					<div class="controls">
		                <s:textfield name="convLegalUnitNum" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品HS编码</label>
					<div class="controls">
		                <s:textfield name="hsCode" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">施检机构的代码</label>
					<div class="controls">
		                <s:textfield name="checkOrgCode" />
					</div>
				</div>
            </div>
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">申报计量单位</label>
					<div class="controls">
		                <s:textfield name="declareUnit" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">XML</label>
					<div class="controls">
		                <s:textarea name="xmlStr" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">合法采购证明（国外进货发票或小票）</label>
					<div class="controls">
		                <s:textarea name="legalTicket" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">原产地证书</label>
					<div class="controls">
		                <s:textarea name="originPlaceCert" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">食药监局、国家认监委备案附件</label>
					<div class="controls">
		                <s:textarea name="cncaPorDoc" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">外文标签的中文翻译件</label>
					<div class="controls">
		                <s:textarea name="markExchange" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label class="control-label">境外官方及第三方机构的检测报告</label>
					<div class="controls">
		                <s:textarea name="testReport" data-htmleditor="kindeditor" data-height="400px" />
					</div>
				</div>
            </div>
        </div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-kuajing-myt-kj-sku-info-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/kuajing/myt-kj-sku-info-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>