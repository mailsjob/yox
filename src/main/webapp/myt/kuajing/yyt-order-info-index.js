$(function() {
    $(".grid-myt-kuajing-yyt-order-info-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/kuajing/yyt-order-info!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        },{
            label : '提交时间',
            name : 'createdDate',
            align : 'center',
            stype : 'date',
            width : 120
        },{
            label : '订单号',
            name : 'pocode',
            width : 120,
            align : 'center'
        }, {
            label : '订单日期',
            name : 'podate',
            width : 100,
            align : 'center'
        }, {
            label : '收货人名',
            name : 'shiptoname',
            width : 80,
            align : 'center'
        }, {
            label : '手机',
            name : 'mobile',
            width : 100,
            align : 'right'
        }, {
            label : '城市',
            name : 'city',
            width : 60,
            align : 'right'
        }, {
            label : '地址',
            name : 'address',
            width : 200
        }, {
            label : '备注',
            hidden:true,
            name : 'remark1',
            width : 200
        }, {
            label : '买方ID',
            name : 'buyerid',
            width : 100,
            align : 'center'
        }, {
            label : '海关通关号',
            name : 'customcode',
            width : 150,
            align : 'left'
        }],
        inlineNav : {
            add : false
        },
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + '/myt/kuajing/yyt-order-info!getYytOrderInfoRsp?id=' + row_id,
                colModel : [ {
                    label : '状态',
                    name : 'status',
                    align : 'center',
                    stype : 'select',
                    searchoptions : {
                        value : Util.getCacheEnumsByType('yytOrderRspEnum')
                    },
                    width : 80
                },{
                    label : '备注',
                    name : 'message',
                    width : 80
                }],
                cmTemplate : {
                    sortable : false
                },
                rowNum : -1,
                toppager : false,
                filterToolbar : false,
                multiselect : false,
                subGrid : true,
                subGridRowExpanded : function(subgrid_id, row_id) {
                    Grid.initSubGrid(subgrid_id, row_id, {
                        url : WEB_ROOT + '/myt/kuajing/yyt-order-info!yytOrderInfoRspDetails?orderRspId=' + row_id,
                        colModel : [ {
                            label : '状态',
                            name : 'sts',
                            align : 'center',
                            stype : 'select',
                            searchoptions : {
                                value : Util.getCacheEnumsByType('yytOrderRspDetailEnum')
                            },
                            width : 80
                        },{
                            label : '单号',
                            name : 'pocode',
                            width : 80
                        },{
                            label : '备注',
                            name : 'message',
                            width : 80
                        }],
                        cmTemplate : {
                            sortable : false
                        },
                        rowNum : -1,
                        toppager : false,
                        filterToolbar : false,
                        multiselect : false
                    });
                }
            });
        }
        /*subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + '/myt/kuajing/yyt-order-info!orderDetailInfos?id=' + row_id,
                colModel : [ {
                    label : '商品',
                    name : 'goodsId',
                    width : 80
                }, {
                    label : '商品国际码',
                    name : 'barcode',
                    width : 80
                }, {
                    label : '商品名称',
                    name : 'goodsName',
                    width : 120
                }, {
                    label : '数量',
                    name : 'qty',
                    formatter : 'number',
                    width : 80
                }, {
                    label : '单价',
                    name : 'price',
                    width : 100
                }, {
                    label : '单位',
                    name : 'unitName',
                    width : 100
                }  ],
                cmTemplate : {
                    sortable : false
                },
                rowNum : -1,
                toppager : false,
                filterToolbar : false,
                multiselect : false
            });
        }*/
        
    });
});
