$(function() {
    $(".grid-myt-yryd-yryd-store").data("gridOptions", {
        url : WEB_ROOT + "/myt/yryd/yryd-store!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '客户名',
            width : 50,
            name : 'customerProfile.display',
            index:'customerProfile.trueName_OR_customerProfile.nickName'
        }, {
            label : '店铺名',
            width : 50,
            name : 'storeName'
        }, {
            label : '店铺状态',
            name : 'storeStatus',
            width : 80,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('storeStatusEnum')
            }
        }, {
            label : '申请时间',
            width : 120,
            name : 'submitTime',
            sorttype : 'date'
        }, {
            label : '开店时间',
            width : 120,
            name : 'openTime',
            sorttype : 'date'
        } , {
            label : '客服QQ',
            width : 100,
            name : 'serviceQq'
        }, {
            label : '客服电话',
            width : 100,
            name : 'servicePhone'
        } ],
        addable:false,
        inlineNav : {
            add : false
        },
        multiselect:false

    });
});