<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-wlf-user"
	action="${base}/myt/yryd/yryd-user!doSave" method="post">

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">登录账号资料</h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">登录账号</label>
						<div class="controls">
							<s:textfield name="loginid" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">您的基本资料</h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">学生证照片</label>
						<div class="controls">
							<s:hidden name="studentCardPhoto" data-singleimage="true" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">身份证照片</label>
						<div class="controls">
							<s:hidden name="idCardPhoto" data-singleimage="true" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">中文姓名</label>
						<div class="controls">
							<s:textfield name="name" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">拼音/英文姓名</label>
						<div class="controls">
							<s:textfield name="nickName" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">身份证号码</label>
						<div class="controls">
							<s:textfield name="idCard" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">证件有效期至</label>
						<div class="controls">
							<s3:datetextfield name="idCardExpiredTime" format="date"></s3:datetextfield>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">性 别</label>
						<div class="controls">
							<s:radio name="sex" list="#application.enums.genericSexEnum" />
						</div>
					</div>

				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">出生日期</label>
						<div class="controls">
							<s3:datetextfield name="bornTime" format="date"></s3:datetextfield>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">学校全称</label>
						<div class="controls">
							<s:textfield name="schoolName" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">所在院系</label>
						<div class="controls">
							<s:textfield name="departName" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">受教育阶段</label>
						<div class="controls">
							<s:radio name="eduLevel" list="#application.enums.eduLevelEnum" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">入 学</label>
						<div class="controls">
							<s3:datetextfield name="enterTime" format="date"></s3:datetextfield>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">毕 业</label>
						<div class="controls">
							<s3:datetextfield name="leaveTime" format="date"></s3:datetextfield>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">区域</label>
						<div class="controls">
							<div class="input-icon right">
								<i class="fa fa-angle-double-down btn-toggle"></i>
								<s:textfield name="region.display" data-toggle="dropdownselect"
									data-url="%{#attr.base+'/pub/region!forward?_to_=selection'}" data-selected="%{region.id}"
									data-minWidth="700px" />
								<s:hidden name="region.id" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">邮政编码</label>
						<div class="controls">
							<s:textfield name="postCode" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">详细地址</label>
						<div class="controls">
							<s:textfield name="addr" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">手 机</label>
						<div class="controls">
							<s:textfield name="mobilePhone" placeholder="可用于接收通知短信" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">电子邮件</label>
						<div class="controls">
							<s:textfield name="email" placeholder="可用于接收通知邮件" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">院系电话</label>
						<div class="controls">
							<s:textfield name="departPhone" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">宿舍电话</label>
						<div class="controls">
							<s:textfield name="dormPhone" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">在本校期间是否获得过奖学金，如有请列明：</label>
						<div class="controls">
							<s:textarea name="awardMemo" rows="3" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">是否现任学校党、团、学生会或班级干部，如有请列明：</label>
						<div class="controls">
							<s:textarea name="jobMemo" rows="3" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">是否献血，或参加过志愿者活动：</label>
						<div class="controls">
							<s:textarea name="communityMemo" rows="3" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">是否为党员：</label>
						<div class="controls">
							<s:radio name="partyMember" list="#application.enums.booleanLabel" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">家庭联系人资料</h3>
		</div>
		<div class="panel-body">
			<h4 class="form-section">联系人1</h4>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">姓名</label>
						<div class="controls">
							<s:textfield name="r1Name" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">手机</label>
						<div class="controls">
							<s:textfield name="r1MobilePhone" />
						</div>
					</div>
				</div>
			</div>
			<%-- <div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">与申请人关系：</label>
						<div class="controls">
							<s:radio name="r1Relation" list="#application.enums.relationEnum" />
						</div>
					</div>
				</div>
			</div> --%>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">单位名称</label>
						<div class="controls">
							<s:textfield name="r1Company" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">单位电话</label>
						<div class="controls">
							<s:textfield name="r1CompanyPhone" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">住宅地址</label>
						<div class="controls">
							<s:textfield name="r1HomeAddr" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">家庭电话</label>
						<div class="controls">
							<s:textfield name="r1HomePhone" />
						</div>
					</div>
				</div>
			</div>
			<h4 class="form-section">联系人2</h4>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">姓名</label>
						<div class="controls">
							<s:textfield name="r2Name" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">手机</label>
						<div class="controls">
							<s:textfield name="r2MobilePhone" />
						</div>
					</div>
				</div>
			</div>
			<%-- <div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">与申请人关系：</label>
						<div class="controls">
							<s:radio name="r2Relation" list="#application.enums.relationEnum" />
						</div>
					</div>
				</div>
			</div> --%>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">单位名称</label>
						<div class="controls">
							<s:textfield name="r2Company" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">单位电话</label>
						<div class="controls">
							<s:textfield name="r2CompanyPhone" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">住宅地址</label>
						<div class="controls">
							<s:textfield name="r2HomeAddr" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">家庭电话</label>
						<div class="controls">
							<s:textfield name="r2HomePhone" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<<%-- div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">您的其他资料</h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">每月的对账单寄往</label>
						<div class="controls">
							<s:radio name="billAddr" list="#application.enums.billAddrEnum" />
							<span class="help-block">※请再次确认地址是否填写清晰，以确保相关信件和对账单能准确寄送，如果未作任何选择，对账单将寄往通信地址。</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">请勾选卡片领取方式</label>
						<div class="controls">
							<s:radio name="receiveCardType" list="#application.enums.receiveCardTypeEnum" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> --%>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">证明人资料</h3>
		</div>
		<div class="panel-body">
			<h4 class="form-section">证明人1</h4>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">中文姓名</label>
						<div class="controls">
							<s:textfield name="v1Name" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">拼音/英文姓名</label>
						<div class="controls">
							<s:textfield name="v1NickName" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">身份证号码</label>
						<div class="controls">
							<s:textfield name="v1IdCard" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">电话</label>
						<div class="controls">
							<s:textfield name="v1MobilePhone" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">邮件</label>
						<div class="controls">
							<s:textfield name="v1Email" />
						</div>
					</div>
				</div>
			</div>
			<h4 class="form-section">证明人2</h4>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">中文姓名</label>
						<div class="controls">
							<s:textfield name="v2Name" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">拼音/英文姓名</label>
						<div class="controls">
							<s:textfield name="v2NickName" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">身份证号码</label>
						<div class="controls">
							<s:textfield name="v2IdCard" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">电话</label>
						<div class="controls">
							<s:textfield name="v2MobilePhone" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">邮件</label>
						<div class="controls">
							<s:textfield name="v2Email" />
						</div>
					</div>
				</div>
			</div>
			<div class="note note-info">
				<p>※证明人仅是证明申请人填写的资料真实，不承担任何担保责任。</p>
				<p>※证明人需为申请人的同一院系、同一界别的同学。</p>
				<p>※每个申请人应有两个证明人。</p>
			</div>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">推荐人信息</h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">推荐人姓名</label>
						<div class="controls">
							<s:textfield name="referrerName" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">推荐人未来付卡号</label>
						<div class="controls">
							<s:textfield name="referrerCard" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>