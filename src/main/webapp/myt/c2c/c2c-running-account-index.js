$(function() {
    $(".grid-myt-c2c-c2c-running-account-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/c2c/c2c-running-account!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '卖家',
            name : 'c2cFinanceAccount.shop.shopName',
            index : 'c2cFinanceAccount',
            width : 120,
            align : 'left'
        }, {
            label : '发生日期',
            name : 'occureTime',
            width : 120,
            formatter : 'date',
            align : 'center'
        }, {
            label : '订单号',
            name : 'bizNum',
            width : 120,
            align : 'right'
        }, {
            label : '账户类型',
            name : 'c2cFinanceAccount.accountTypeCode',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('accountTypeCodeEnum')
            },
            width : 80,
            align : 'right'
        }, {
            label : '流水金额',
            name : 'amount',
            formatter : 'currency',
            width : 80,
            align : 'right'
        }, {
            label : '业务备注',
            name : 'bizMemo',
            width : 150,
            align : 'right'
        }, {
            label : '是否结算',
            name : 'isSettled',
            formatter : 'select',
            searchoptions : {
                value : {
                    '' : '',
                    'false' : '未结算',
                    'true' : '已结算'
                }
            },
            width : 60,
            align : 'center'
        } ],
        multiselect : false,
        postData : {
            "search['FETCH_c2cFinanceAccount.shop']" : "LEFT.LEFT"
        },
    });
});
