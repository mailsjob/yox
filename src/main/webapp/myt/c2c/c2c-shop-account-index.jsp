<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
    <ul class="nav nav-pills">
        <li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">列表查询</a></li>
        <li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row search-form-default">
                <div class="col-md-12">
                    <form action="#" method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-myt-c2c-c2c-shop-account-index">
                        <div class="input-group">
                            <div class="input-cont">
                                <input type="text" name="shopName" class="form-control" placeholder="店铺名称...">
                            </div>
                            <span class="input-group-btn">
                                <button class="btn green" type="submmit">
                                    <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
                                </button>
                                <button class="btn default hidden-inline-xs" type="reset">
                                    <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-myt-c2c-c2c-shop-account-index"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-c2c-c2c-shop-account-index").data("gridOptions", {
            url : WEB_ROOT + "/myt/c2c/c2c-shop-account!findByShop",
            colModel : [ {
                label : '店铺名',
                name : 'shopName',
                editable : false,
                sortable : false,
                width : 60
            }, {
                label : '总账',
                name : 'total',
                formatter : 'currency',
                editable : false,
                sortable : false,
                width : 60,
                align : 'right'
            }, {
                label : '主账户余额',
                name : 'overage',
                formatter : 'currency',
                editable : false,
                sortable : false,
                width : 60,
                align : 'right'
            }, {
                label : '补贴',
                name : 'subsidy',
                formatter : 'currency',
                editable : false,
                sortable : false,
                width : 60,
                align : 'right'
            }, {
                label : '保证金账户',
                name : 'bail',
                formatter : 'currency',
                editable : true,
                sortable : false,
                width : 60,
                align : 'right'
            } ],
            multiselect : false
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
