$(function() {
    $(".grid-myt-c2c-c2c-shop-info-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/c2c/c2c-shop-info!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '店铺名',
            name : 'shopName',
            width : 150,
            editable : true,
            formatter : function(cellValue, options, rowdata, action) {
                var url = "http://intranet.meiyuetao.com/Shop/ShopHome?ShopSid=" + rowdata.id;
                return '<a href=' + url + ' target="_blank">' + cellValue + '</a>';
            },
            align : 'left'
        }, {
            label : '店铺地址',
            name : 'shopAddr',
            width : 250,
            editable : true,
            align : 'left'
        }, {
            label : '手机',
            name : 'mobilePhone',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '微信',
            name : 'weixin',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '旺旺',
            name : 'wangwang',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : 'QQ',
            name : 'qq',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '补贴额',
            name : 'subsidy',
            width : 200,
            formatter : 'currency',
            editable : false,
            align : 'right'
        }, {
            label : '先行赔付额',
            name : 'advancePayouts',
            width : 200,
            formatter : 'currency',
            editable : false,
            align : 'right'
        }, {
            label : '补贴上限',
            name : 'subsidyCap',
            width : 200,
            formatter : 'currency',
            editable : false,
            align : 'right'
        }, {
            label : '保证金',
            name : 'cautionMoney',
            width : 200,
            formatter : 'currency',
            editable : false,
            align : 'right'
        } ],
        editurl : WEB_ROOT + '/myt/c2c/c2c-shop-info!doSave',
        delurl : WEB_ROOT + '/myt/c2c/c2c-shop-info!doDelete',
        fullediturl : WEB_ROOT + '/myt/c2c/c2c-shop-info!inputTabs'
    });
});
