<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-c2c-c2c-finance-account-inputBasic" action="${base}/myt/c2c/c2c-finance-account!doSave" method="post" data-editrulesurl="${base}/myt/c2c/c2c-finance-account!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-c2c-c2c-finance-account-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">账户类型</label>
                    <div class="controls">
                        <s:select name="accountTypeCode" list="#application.enums.accountTypeCodeEnum" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">主账户</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-financeParent"></i>
                            <s:textfield name="parent.accountTypeCode" />
                            <s:hidden name="parent.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">店铺</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-c2cShopInfo"></i>
                            <s:textfield name="shop.shopName" />
                            <s:hidden name="shop.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">备注</label>
                    <div class="controls">
                        <s:textfield name="memo" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">金额</label>
                    <div class="controls">
                        <s:textfield name="amount" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">冻结金额</label>
                    <div class="controls">
                        <s:textfield name="frozenAmount" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-c2c-c2c-finance-account-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/c2c/c2c-finance-account-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>