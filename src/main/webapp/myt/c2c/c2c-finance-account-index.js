$(function() {
    $(".grid-myt-c2c-c2c-finance-account-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/c2c/c2c-finance-account!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '店铺',
            name : 'shop.shopName',
            index : 'shop',
            width : 60,
            editable : false,
            align : 'left'
        }, {
            label : '账户类型代码',
            name : 'accountTypeCode',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('accountTypeCodeEnum')
            },
            width : 60,
            editable : false,
            align : 'center'
        }, {
            label : '金额',
            name : 'amount',
            width : 60,
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : '冻结金额',
            name : 'frozenAmount',
            width : 60,
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : '备注',
            name : 'memo',
            width : 60,
            editable : false,
            align : 'left'
        } ],
        postData : {
            "search['FETCH_parent']" : "LEFT",
            "search['FETCH_shop']" : "LEFT"
        },
        sortname : 'shop.shopName',
        multiselect : false,
        editurl : WEB_ROOT + '/myt/c2c/c2c-finance-account!doSave',
        delurl : WEB_ROOT + '/myt/c2c/c2c-finance-account!doDelete',
        fullediturl : WEB_ROOT + '/myt/c2c/c2c-finance-account!inputTabs'
    });
});
