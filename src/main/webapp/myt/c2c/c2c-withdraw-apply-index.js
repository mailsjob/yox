$(function() {
    $(".grid-myt-c2c-c2c-withdraw-apply-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/c2c/c2c-withdraw-apply!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '店铺',
            name : 'shop.shopName',
            index : 'shop',
            width : 150,
            editable : true,
            align : 'left'
        }, {
            label : '申请时间',
            name : 'applyTime',
            width : 150,
            formatter : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '联系方式',
            name : 'contact',
            width : 150,
            editable : true,
            align : 'left'
        }, {
            label : '外部账户类型',
            name : 'outAccountType',
            width : 150,
            editable : true,
            align : 'left'
        }, {
            label : '外部帐户编号',
            name : 'outAccountCode',
            width : 150,
            editable : true,
            align : 'left'
        }, {
            label : '金额',
            name : 'amount',
            width : 150,
            formatter : 'number',
            editable : true,
            align : 'right'
        }, {
            label : '备注',
            name : 'memo',
            width : 150,
            editable : true,
            align : 'left'
        }, {
            label : '提现状态',
            name : 'applyStatus',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('applyStatusEnum')
            },
            width : 150,
            editable : true,
            align : 'center'
        } ],
        editurl : WEB_ROOT + '/myt/c2c/c2c-withdraw-apply!doSave',
        delurl : WEB_ROOT + '/myt/c2c/c2c-withdraw-apply!doDelete',
        fullediturl : WEB_ROOT + '/myt/c2c/c2c-withdraw-apply!inputTabs'
    });
});
