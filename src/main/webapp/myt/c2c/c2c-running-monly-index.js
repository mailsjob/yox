$(function() {
    var $form = $(this);
    $form.find(".fa-select-c2cShopInfo").click(function() {
        $(this).popupDialog({
            url : WEB_ROOT + '/myt/c2c/c2c-shop-info!forward?_to_=selection',
            title : '选取集市店铺',
            callback : function(rowdata) {
                $form.setFormDatas({
                    'shop.id' : rowdata.id,
                    'c2cFinanceAccount.shop.shopName' : rowdata.shopName
                });
            }
        })
    });
    $(".grid-c2c-c2c-running-monly-normal").data("gridOptions", {
        url : WEB_ROOT + '/myt/c2c/c2c-running-monly!findByMonly',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '月份',
            name : 'occureYearMon',
            width : 120,
            align : 'center'
        }, {
            label : '账户类型',
            name : 'c2cFinanceAccount.accountTypeCode',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('accountTypeCodeEnum')
            },
            width : 80,
            align : 'center'
        }, {
            label : '订单数',
            name : 'count(bizNum)',
            width : 100,
            align : 'center'
        }, {
            label : '业务类型',
            name : 'bizNumType',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('bizNumTypeEnum')
            },
            width : 100,
            align : 'right'
        }, {
            label : '金额',
            name : 'sum(amount)',
            formatter : 'currency',
            width : 100,
            align : 'right'
        } ],
        multiselect : false
    });
});
