<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
    <div class="col-md-12">
        <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-myt-c2c-c2c-shop-info-selection">
            <div class="form-group">
                <label class="sr-only">集市店铺</label> <input type="text" name="search['CN_shopName']" class="form-control input-xlarge" placeholder="店铺名称..." />
            </div>
            <button class="btn default" type="reset">
                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
            </button>
            <button class="btn green" type="submmit">
                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
            </button>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="grid-myt-c2c-c2c-shop-info-selection"></table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-c2c-c2c-shop-info-selection").data("gridOptions", {
            url : "${base}/myt/c2c/c2c-shop-info!findByPage",
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true
            }, {
                label : '店铺代码',
                name : 'shopCode',
                width : 32,
                editable : true,
                align : 'left'
            }, {
                label : '旺旺',
                name : 'wangwang',
                width : 250,
                editable : true,
                align : 'left'
            }, {
                label : '先行赔付额',
                name : 'advancePayouts',
                width : 60,
                formatter : 'number',
                editable : true,
                align : 'right'
            }, {
                label : '店铺名',
                name : 'shopName',
                width : 200,
                editable : true,
                align : 'left'
            }, {
                label : '手机',
                name : 'mobilePhone',
                width : 32,
                editable : true,
                align : 'left'
            }, {
                label : '微信',
                name : 'weixin',
                width : 250,
                editable : true,
                align : 'left'
            }, {
                label : '补贴上限',
                name : 'subsidyCap',
                width : 60,
                formatter : 'number',
                editable : true,
                align : 'right'
            }, {
                label : 'QQ',
                name : 'qq',
                width : 250,
                editable : true,
                align : 'left'
            }, {
                label : '关联客户',
                name : 'customerProfile.display',
                index : 'customerProfile',
                width : 200,
                width : 200,
                editable : true,
                align : 'left'
            }, {
                label : '店铺地址',
                name : 'shopAddr',
                width : 250,
                editable : true,
                align : 'left'
            }, {
                label : '店铺图片',
                name : 'shopPic',
                width : 32,
                editable : true,
                align : 'left'
            } ],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            },
            editurl : WEB_ROOT + '/myt/c2c/c2c-shop-info!doSave',
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>