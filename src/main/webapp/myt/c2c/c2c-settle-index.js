$(function() {
    $(".grid-myt-c2c-c2c-settle-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/c2c/c2c-settle!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '店铺',
            name : 'financeAccount.shop.shopName',
            index : 'financeAccount',
            width : 100,
            editable : true,
            align : 'left'
        }, {
            label : '结算时间',
            name : 'settleTime',
            width : 100,
            formatter : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '结算类型',
            name : 'settleType',
            width : 100,
            editable : true,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('settleTypeEnum')
            },
            align : 'left'
        }, {
            label : '结算前金额',
            name : 'beforeSettleAmount',
            width : 100,
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : '结算金额',
            name : 'settleAmount',
            width : 100,
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : '结算后金额',
            name : 'afterSettleAmount',
            width : 100,
            formatter : 'currency',
            editable : true,
            align : 'right'
        } ],
        postData : {
            "search['FETCH_financeAccount.shop']" : "LEFT.LEFT"
        },
        multiselect : false
    // editurl : WEB_ROOT + '/myt/c2c/c2c-settle!doSave',
    // delurl : WEB_ROOT + '/myt/c2c/c2c-settle!doDelete',
    // fullediturl : WEB_ROOT + '/myt/c2c/c2c-settle!inputTabs'
    });
});
