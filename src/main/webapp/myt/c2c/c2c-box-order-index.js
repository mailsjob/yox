/**
 * 美月淘运营 销售管理 销售订单列表 订单管理<br>
 */
$(function() {
    $(".grid-myt-c2c-c2c-box-order").data("gridOptions", {
        calcRowAmount : function(rowdata, src) {
            if (src == 'actualAmount') {
                rowdata['discountAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['actualAmount']);
            } else {
                rowdata['actualAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['discountAmount']);
            }
        },
        updateRowAmount : function(src) {
            var $grid = $(this);
            var rowdata = $grid.jqGrid("getEditingRowdata");
            $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
            $grid.jqGrid("setEditingRowdata", rowdata);
        },
        url : WEB_ROOT + '/myt/sale/box-order!findByPage',
        colModel : [ {
            name : 'id',
            hidden : true
        }, {
            label : '下单时间',
            name : 'orderTime',
            align : 'left',
            sorttype : 'date',
            width : 80
        }, {
            label : '付款类型',
            name : 'payMode',
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Biz.getPayMode()
            },
            width : 80
        }, {
            label : '店铺名称',
            name : 'c2cShopInfo.shopName',
            align : 'left',
            width : 150
        }, {
            label : '状态',
            name : 'orderStatus',
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('boxOrderStatusEnum')
            },
            width : 120
        }, {
            label : '订单号',
            name : 'orderSeq',
            width : 200,
            align : 'left',
            formatter : function(cellValue, options, rowdata, action) {
                var html = cellValue;
                if (rowdata.circle) {
                    html += '<span class="badge badge-info">周</span>'
                }
                if (rowdata.splitPayMode != 'NO') {
                    html += '<span class="badge badge-success">分</span>'
                }
                return html;
            }
        }, {
            label : '下单客户',
            name : 'customerProfile.display',
            index : 'customerProfile.trueName_OR_customerProfile.nickName',
            align : 'left',
            width : 100,
            formatter : function(cellValue, options, rowdata, action) {
                var url = WEB_ROOT + "/myt/customer/customer-profile!view?id=" + rowdata.customerProfile.id;
                return '<a href="' + url + '" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
            }
        }, {
            label : '客户电话',
            name : 'customerProfile.mobilePhone',
            width : 170,
            align : 'left'
        }, {
            label : '收货人',
            name : 'receivePerson',
            width : 80,
            align : 'left'
        }, {
            label : '收货人电话',
            name : 'mobilePhone',
            width : 170,
            align : 'left'
        }, {
            label : '原始金额',
            name : 'originalAmount',
            formatter : 'currency',
            editable : true,
            editoptions : {
                dataInit : function(elem) {
                    var $elem = $(elem);
                    $elem.attr("readonly", true);
                }
            },
            width : 120
        }, {
            label : '折扣金额',
            name : 'discountAmount',
            editable : true,
            formatter : 'currency',
            editrules : {
                required : true,
            },
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function() {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            },
            width : 120
        }, {
            label : '应付金额',
            name : 'actualAmount',
            editable : true,
            formatter : 'currency',
            editrules : {
                required : true,
            },
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function() {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            },
            width : 120
        }, {
            label : '已付总金额',
            name : 'actualPayedAmount',
            formatter : 'currency',
            width : 120
        }, {
            label : '改价备注',
            name : 'modifierMemo',
            editable : true,
            editrules : {
                required : true
            },
            width : 120
        } ],
        postData : {
            "search['FETCH_customerProfile']" : "INNER",
            "search['FETCH_intermediaryCustomerProfile']" : "LEFT",
            "search['NN_c2cShopInfo']" : true
        },
        addable : false,
        inlineNav : {
            add : false
        },
        editurl : WEB_ROOT + '/myt/sale/box-order!modifyPrice',
        fullediturl : WEB_ROOT + '/myt/sale/box-order!inputTabs',
        footerrow : true,
        footerLocalDataColumn : [ 'actualAmount', 'actualPayedAmount' ],
        operations : function(itemArray) {
            var loading = false;
            var host = document.domain; // 获取域名 判断是 美月淘平台还是360跨境平台
            if ("open.360kuajing.com" == host || "opentest.360kuajing.com" == host || "localhost" == host) {
                var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 提交至保税平台</a></li>');
                $select.children("a").bind("click", function(e) {
                    if (loading) {
                        alert("正在提交，请稍候...");
                        return;
                    }
                    loading = true;
                    e.preventDefault();
                    var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                    var url = WEB_ROOT + "/myt/sale/box-order!kjPost.json?_=0";
                    var rowDatas = $grid.jqGrid("getSelectedRowdatas");
                    for (i = 0; i < rowDatas.length; i++) {
                        var rowData = rowDatas[i];
                        url += "&orderSid=" + rowData['id'];
                    }

                    $.post(url, "", function(data) {
                        loading = false;
                        alert(data.message);
                    });
                });
                itemArray.push($select);
                var $grid = $(this);
                var $select1 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>提交至怡亚通</a></li>');
                $select1.children("a").bind("click", function(e) {
                    e.preventDefault();
                    var ids = $grid.getAtLeastOneSelectedItem();
                    if (ids) {
                        var url = WEB_ROOT + '/myt/kuajing/yyt-order-info!buildYytOrder';
                        $grid.ajaxPostURL({
                            url : url,
                            success : function() {
                                $grid.refresh();
                            },
                            confirmMsg : "确认  提交？",
                            data : {
                                ids : ids.join(",")
                            }
                        })
                    }
                });
                itemArray.push($select1);
            }
            if ("open.meiyuetao.com" == host || "opentest.meiyuetao.com" == host || "localhost" == host) {
                var $select2 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>更新跨境订单物流</a></li>');
                $select2.children("a").bind("click", function(e) {
                    e.preventDefault();
                    var ids = $grid.getAtLeastOneSelectedItem();
                    if (ids) {
                        var url = WEB_ROOT + '/myt/kuajing/yyt-order-info!updateYytOrderStatu';
                        $grid.ajaxPostURL({
                            url : url,
                            success : function() {
                                $grid.refresh();
                            },
                            confirmMsg : "确认  提交？",
                            data : {
                                ids : ids.join(",")
                            }
                        })
                    }
                });
                itemArray.push($select2);
            }
        },
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                calcRowAmount : function(rowdata, src) {
                    if (src == 'actualAmount') {
                        rowdata['discountAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['actualAmount']);

                    } else {
                        rowdata['actualAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['discountAmount']);
                    }
                },
                updateRowAmount : function(src) {
                    var $grid = $(this);
                    var rowdata = $grid.jqGrid("getEditingRowdata");
                    $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
                    $grid.jqGrid("setEditingRowdata", rowdata);
                },
                url : WEB_ROOT + '/myt/sale/box-order!boxOrderDetails?id=' + row_id,
                colModel : [ {
                    label : '行项号',
                    name : 'sn',
                    align : 'center',
                    width : 60
                }, {
                    label : '预约发货日期',
                    name : 'reserveDeliveryTime',
                    editable : true,
                    align : 'center',
                    stype : 'date'
                }, {
                    label : '行项状态',
                    name : 'orderDetailStatus',
                    width : 80,
                    align : 'center',
                    stype : 'select',
                    searchoptions : {
                        value : Util.getCacheEnumsByType('boxOrderDetailStatusEnum')
                    }
                }, {
                    label : '原始金额',
                    name : 'originalAmount',
                    formatter : 'currency',
                    editable : true,
                    editoptions : {
                        dataInit : function(elem) {
                            var $elem = $(elem);
                            $elem.attr("readonly", true);
                        }
                    }
                }, {
                    label : '折扣金额',
                    name : 'discountAmount',
                    editable : true,
                    formatter : 'currency',
                    editrules : {
                        required : true,
                    },
                    editoptions : {
                        dataInit : function(elem) {
                            var $grid = $(this);
                            var $elem = $(elem);
                            $elem.keyup(function() {
                                $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                            });
                        }
                    }
                }, {
                    label : '应付金额',
                    name : 'actualAmount',
                    editable : true,
                    formatter : 'currency',
                    editrules : {
                        required : true,
                    },
                    editoptions : {
                        dataInit : function(elem) {
                            var $grid = $(this);
                            var $elem = $(elem);
                            $elem.keyup(function() {
                                $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                            });
                        }
                    }
                }, {
                    label : '已付金额',
                    name : 'payAmount',
                    formatter : 'currency'
                }, {
                    label : '改价备注',
                    name : 'modifierMemo',
                    editable : true,
                    editrules : {
                        required : true,
                    },
                    width : 200
                } ],
                cmTemplate : {
                    sortable : false
                },
                rowNum : -1,
                loadonce : true,
                multiselect : false,
                filterToolbar : false,
                editurl : WEB_ROOT + '/myt/sale/box-order-detail!modifyPrice',
                fullediturl : WEB_ROOT + '/myt/sale/box-order-detail!inputTabs',
                toppager : false,
                subGrid : true,
                subGridRowExpanded : function(subgrid_id, row_id) {
                    Grid.initSubGrid(subgrid_id, row_id, {
                        calcRowAmount : function(rowdata, src) {
                            if (src == 'originalAmount') {
                                rowdata['price'] = MathUtil.div(rowdata['originalAmount'], rowdata['quantity']);
                            } else {
                                rowdata['originalAmount'] = MathUtil.mul(rowdata['price'], rowdata['quantity']);
                            }
                            if (src == 'actualAmount') {
                                rowdata['discountAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['actualAmount']);
                            } else {
                                rowdata['actualAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['discountAmount']);
                            }
                        },
                        updateRowAmount : function(src) {
                            var $grid = $(this);
                            var rowdata = $grid.jqGrid("getEditingRowdata");
                            $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
                            $grid.jqGrid("setEditingRowdata", rowdata);
                        },
                        url : WEB_ROOT + '/myt/sale/box-order-detail!boxOrderDetailCommodities?id=' + row_id,
                        colModel : [ {
                            label : '商品',
                            name : 'commodity.display',
                            width : 250
                        }, {
                            label : '销售单价',
                            name : 'price',
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            formatter : 'currency',
                            width : 100
                        }, {
                            label : '销售数量',
                            name : 'quantity',
                            formatter : 'number',
                            width : 100,
                            editrules : {
                                number : true
                            },
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid);
                                    });
                                }
                            }
                        }, {
                            label : '原始金额',
                            name : 'originalAmount',
                            formatter : 'currency',
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            width : 100
                        }, {
                            label : '折扣金额',
                            name : 'discountAmount',
                            editable : true,
                            editrules : {
                                required : true
                            },
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            formatter : 'currency'
                        }, {
                            label : '金额小计',
                            name : 'actualAmount',
                            editable : true,
                            editrules : {
                                required : true
                            },
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            formatter : 'currency'
                        }, {
                            label : '已发货数量',
                            name : 'deliveriedQuantity',
                            formatter : 'number',
                            width : 80
                        }, {
                            label : '发货仓库',
                            name : 'storageLocation.id',
                            width : 120,
                            stype : 'select',
                            searchoptions : {
                                value : Biz.getStockDatas()
                            }
                        }, {
                            label : '锁定库存量',
                            name : 'salingLockedQuantity',
                            formatter : 'number',
                            width : 80
                        }, {
                            label : '改价备注',
                            name : 'modifierMemo',
                            editable : true,
                            editrules : {
                                required : true,
                            },
                            width : 200
                        } ],
                        cmTemplate : {
                            sortable : false
                        },
                        editurl : WEB_ROOT + '/myt/sale/box-order-detail-commodity!modifyPrice',
                        rowNum : -1,
                        toppager : false,
                        filterToolbar : false,
                        multiselect : false
                    });
                }
            });
        }
    });

});
