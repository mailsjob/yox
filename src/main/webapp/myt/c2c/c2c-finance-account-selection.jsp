<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
    <div class="col-md-12">
        <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-myt-c2c-c2c-finance-account-selection">
            <div class="form-group">
                <label class="sr-only">集市账户</label> <input type="text" name="search['CN_accountTypeCode']" class="form-control input-xlarge" placeholder="账户名称..." />
            </div>
            <button class="btn default" type="reset">
                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
            </button>
            <button class="btn green" type="submmit">
                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
            </button>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="grid-myt-c2c-c2c-finance-account-selection"></table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-c2c-c2c-finance-account-selection").data("gridOptions", {
            url : "${base}/myt/c2c/c2c-finance-account!findByPage",
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true
            }, {
                label : '金额',
                name : 'amount',
                width : 60,
                formatter : 'number',
                editable : true,
                align : 'right'
            }, {
                label : '冻结金额',
                name : 'frozenAmount',
                width : 60,
                formatter : 'number',
                editable : true,
                align : 'right'
            }, {
                label : '店铺',
                name : 'shop.shopName',
                index : 'shop',
                width : 200,
                width : 200,
                editable : true,
                align : 'left'
            }, {
                label : '备注',
                name : 'memo',
                width : 128,
                editable : true,
                align : 'left'
            }, {
                label : '账户类型代码',
                name : 'accountTypeCode',
                formatter : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('accountTypeCodeEnum')
                },
                width : 80,
                editable : true,
                align : 'center'
            } ],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            },
            editurl : WEB_ROOT + '/myt/c2c/c2c-finance-account!doSave',
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>