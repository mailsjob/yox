<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-c2c-c2c-settle-inputBasic" action="${base}/myt/c2c/c2c-settle!doSave" method="post" data-editrulesurl="${base}/myt/c2c/c2c-settle!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-c2c-c2c-settle-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">结算时间</label>
                    <div class="controls">
                        <s3:datetextfield name="settleTime" format="date" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">财务账户</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-financeAccount"></i>
                            <s:textfield name="financeAccount.accountTypeCode" />
                            <s:hidden name="financeAccount.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">结算金额</label>
                    <div class="controls">
                        <s:textfield name="settleAmount" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">结算后金额</label>
                    <div class="controls">
                        <s:textfield name="afterSettleAmount" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">结算类型</label>
                    <div class="controls">
                        <s:select name="settleType" list="#application.enums.settleTypeEnum" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">结算前金额</label>
                    <div class="controls">
                        <s:textfield name="beforeSettleAmount" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-c2c-c2c-settle-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/c2c/c2c-settle-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>