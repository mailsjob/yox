$(function() {
    $(".form-myt-c2c-c2c-settle-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-financeAccount").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/c2c/c2c-finance-account!forward?_to_=selection',
                    title : '选取集市店铺',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'financeAccount.id' : rowdata.id,
                            'financeAccount.accountTypeCode' : rowdata.accountTypeCode
                        });
                    }
                })
            });
        }
    });
});