<%-- ===================================== --%>
<%-- 美月淘运营 销售管理 销售订单列表 订单管理   --%>
<%-- ===================================== --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
    <ul class="nav nav-pills">
        <li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">订单管理</a></li>
        <%-- <li><a class="tab-default" data-toggle="tab" href="${base}/myt/sale/box-order-detail">订单行项</a></li>
        <li><a class="tab-default" data-toggle="tab" href="${base}/myt/sale/box-order-detail-commodity">行项商品</a></li>
        <li><a class="tab-default" data-toggle="tab" href="${base}/myt/sale/box-order-detail!forward?_to_=sumByReserveDeliveryMonth">按预约日期汇总商品量</a></li>
        <li><a class="tab-default" data-toggle="tab" href="${base}/myt/sale/tmall-order-file!upLoadNew">导入Tmall订单</a></li>
        <li><a class="tab-default" data-toggle="tab" href="${base}/myt/kuajing/yyt-order-info">怡亚通订单</a></li> --%>
        <li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row search-form-default">
                <div class="col-md-12">
                    <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-myt-c2c-c2c-box-order">
                        <div class="form-group">
                            <label class="sr-only">订单号、客户</label> <input type="text" name="search['CN_orderSeq_OR_customerProfile.nickName_OR_receivePerson_OR_mobilePhone_OR_customerProfile.mobilePhone']" class="form-control input-xlarge" placeholder="订单号、客户、收货人、收件人手机号、下单人手机号..." />
                        </div>
                        <%-- <div data-toggle="buttons" class="btn-group">
                            <label class="btn default"> <s:checkbox name="search['EQ_circle']" /> 只显示周期购
                            </label>
                        </div> --%>
                        <div class="form-group">
                            <div class="btn-group">
                                <button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">状态 <i class="fa fa-angle-down"></i>
                                </button>
                                <div class="dropdown-menu hold-on-click dropdown-checkboxes">
                                    <s:checkboxlist name="search['IN_orderStatus']" list="#application.enums.boxOrderStatusEnum" value="#application.enums.boxOrderStatusEnum.keys.{? #this.name()!='S90CANCLE'}" />
                                </div>
                            </div>
                        </div>
                        <button class="btn green" type="submmit">
                            <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
                        </button>
                        <button class="btn default" type="reset">
                            <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
                        </button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-myt-c2c-c2c-box-order"></table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="${base}/myt/c2c/c2c-box-order-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
