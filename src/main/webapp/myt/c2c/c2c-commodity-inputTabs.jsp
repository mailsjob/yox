<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-custom tabbable-secondary">
	<ul class="nav nav-tabs">
		<li class="tools pull-right"><a href="javascript:;" class="btn default reload"><i class="fa fa-refresh"></i></a></li>
		<li class="active"><a data-toggle="tab"
			href="${base}/myt/md/commodity!edit?id=<s:property value='#parameters.id'/>&clone=<s:property value='#parameters.clone'/>">基本信息</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=kjCommodityInfo&id=<s:property value='#parameters.id'/>">360跨境商品备案</a></li>
		<li><a data-toggle="tab"  data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=sale-date-count&id=<s:property value='#parameters.id'/>">用户单日可购买量</a></li>	
		<li><a data-toggle="tab"  data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=categories&id=<s:property value='#parameters.id'/>">关联分类</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=cmsSpecification&id=<s:property value='#parameters.id'/>">规格说明</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=price-plan&id=<s:property value='#parameters.id'/>">价格表</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=price-index&id=<s:property value='#parameters.id'/>">价格计划</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=recommend-fittings&id=<s:property value='#parameters.id'/>">推荐配件</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=clone&id=<s:property value='#parameters.id'/>">克隆商品</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=circle&id=<s:property value='#parameters.id'/>">周期购属性</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=limited&id=<s:property value='#parameters.id'/>">高级属性</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=comments&id=<s:property value='#parameters.id'/>">商品评论</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/md/commodity!forward?_to_=sku-relation&id=<s:property value='#parameters.id'/>">关联外部商品</a></li>
		<li><a data-toggle="tab"  data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/wlf/wlf-commodity-r2-part-time?commodityId=<s:property value='#parameters.id'/>">兼职信息</a></li>
		<li><a data-toggle="tab"
			href="${base}/myt/md/commodity!revisionIndex?id=<s:property value='#parameters.id'/>">操作记录</a></li>
			
		
	</ul>
</div>
<%@ include file="/common/ajax-footer.jsp"%>