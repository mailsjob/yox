<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-c2c-c2c-withdraw-apply-inputBasic" action="${base}/myt/c2c/c2c-withdraw-apply!doSave" method="post" data-editrulesurl="${base}/myt/c2c/c2c-withdraw-apply!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-c2c-c2c-withdraw-apply-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">店铺</label>
                    <div class="controls">
                        <s:textfield name="shop" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">申请时间</label>
                    <div class="controls">
                        <s3:datetextfield name="applyTime" format="date" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">联系方式</label>
                    <div class="controls">
                        <s:textfield name="contact" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">提现状态</label>
                    <div class="controls">
                        <s:select name="applyStatus" list="#application.enums.applyStatusEnum" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">外部账户类型</label>
                    <div class="controls">
                        <s:textfield name="outAccountType" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">外部帐户编号</label>
                    <div class="controls">
                        <s:textfield name="outAccountCode" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">金额</label>
                    <div class="controls">
                        <s:textfield name="amount" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">备注</label>
                    <div class="controls">
                        <s:textfield name="memo" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-c2c-c2c-withdraw-apply-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/c2c/c2c-withdraw-apply-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>