<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
    <ul class="nav nav-pills">
        <li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">列表查询</a></li>
        <li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row search-form-default">
                <form action="#" method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-myt-c2c-c2c-running-account-index">
                    <div class="col-md-12" style="padding-bottom: 10px;">
                        <div class="form-group">
                            <input type="text" style="width: 180px !important;" name="date" class="form-control input-medium input-daterangepicker grid-param-data" placeholder="日期区间" pattern="">
                        </div>
                        <div class="form-group">
                            <input type="text" style="width: 180px !important;" name="search['CN_c2cFinanceAccount.shop.shopName']" class="form-control input-medium" placeholder="店铺名称">
                        </div>
                        <div class="form-group">
                            <input type="text" style="width: 180px !important;" name="search['CN_bizNum']" class="form-control input-medium" placeholder="订单号码">
                        </div>
                        <div class="form-group input-medium" style="width: 180px !important;">
                            <select name="search['EQ_isSettled']" placeholder="是否结算">
                                <option value="false">未结算</option>
                                <option value="true">已结算</option>
                            </select>
                        </div>
                        <div style="float: right">
                            <button class="btn green" type="submmit">
                                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
                            </button>
                            <button class="btn default hidden-inline-xs" type="reset">
                                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
                            </button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <table class="grid-myt-c2c-c2c-running-account-index"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="${base}/myt/c2c/c2c-running-account-index.js" />
    <%@ include file="/common/ajax-footer.jsp"%>