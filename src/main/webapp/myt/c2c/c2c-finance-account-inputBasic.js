$(function() {
    $(".form-myt-c2c-c2c-finance-account-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-financeParent").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/c2c/c2c-finance-account!forward?_to_=selection',
                    title : '选取主账户',
                    callback : function(rowdata) {
                        if ($form.find("[name=id]").val() == rowdata.id) {
                            alert("不能选取自身账户做主账户");
                        } else {
                            $form.setFormDatas({
                                'parent.id' : rowdata.id,
                                'parent.accountTypeCode' : rowdata.accountTypeCode
                            });
                        }
                    }
                })
            });

            $form.find(".fa-select-c2cShopInfo").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/c2c/c2c-shop-info!forward?_to_=selection',
                    title : '选取集市店铺',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'shop.id' : rowdata.id,
                            'shop.shopName' : rowdata.shopName
                        });
                    }
                })
            });
        }
    });
});