<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
    <ul class="nav nav-pills">
        <li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">日报详情</a></li>
        <li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row search-form-default">
                <form method="get" class="form-inline form-validation form-search-init" data-grid-search=".grid-c2c-c2c-running-daily-normal" action="#">
                    <div class="col-md-12" style="padding-bottom: 10px;">
                        <div class="form-group">
                            <input type="text" style="width: 180px !important;" name="date" class="form-control input-medium input-daterangepicker grid-param-data" placeholder="日期区间" pattern="">
                        </div>
                        <div class="form-group">
                            <div class="input-icon right">
                                <i class="fa fa-ellipsis-horizontal fa-select-c2cShopInfo"></i>
                                <s:textfield name="c2cFinanceAccount.shop.shopName" placeholder="店铺名称" />
                                <s:hidden name="shop.id" />
                            </div>
                        </div>
                        <div class="form-group input-medium" style="width: 180px !important;">
                            <s:select name="c2cFinanceAccount.accountTypeCode" placeholder="账户类型" list="#application.enums.accountTypeCodeEnum" />
                        </div>
                        <div class="form-group input-medium" style="width: 180px !important;">
                            <select name="isSettled" placeholder="是否结算">
                                <option value="false">未结算</option>
                                <option value="true">已结算</option>
                            </select>
                        </div>
                        <div style="float: right">
                            <button class="btn green" type="submmit">
                                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
                            </button>
                            <button class="btn default hidden-inline-xs" type="reset">
                                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
                            </button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <table class="grid-c2c-c2c-running-daily-normal"></table>
                    </div>
                </div>
            </div>
        </div>
        <script src="${base}/myt/c2c/c2c-running-daily-index.js" />
        <%@ include file="/common/ajax-footer.jsp"%>