$(function() {
    $(".grid-c2c-c2c-commodity-normal").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/commodity!findByPage",
        colModel : [ {
            label : '图片',
            name : 'smallPic',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '商品编码',
            name : 'sku',
            align : 'left',
            formatter : function(cellValue, options, rowdata, action) {
                var url = "http://www.meiyuetao.com/product/01-" + rowdata.id + ".html";
                return '<a href=' + url + ' target="_blank">' + cellValue + '</a>';
            },
            width : 80
        }, {
            label : '店铺名称',
            name : 'c2cShopInfo.shopName',
            align : 'left',
            width : 100
        }, {
            label : '商品名称',
            name : 'title',
            sortable : false,
            editable : true,
            width : 130,
            edittype : 'textarea',
            align : 'left'
        }, {
            label : '品牌',
            name : 'brand.id',
            width : 100,
            align : 'left',
            formatter : "select",
            editable : true,
            edittype : 'select',
            editoptions : {
                value : Biz.getBrandDatas()
            },
            responsive : 'sm'
        }, {
            label : '合作伙伴',
            name : 'partner.display',
            index : 'partner.code_OR_partner.abbr',
            width : 100,

            align : 'left'
        }, {
            label : '状态',
            name : 'commodityStatus',
            align : 'center',
            width : 50,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('commodityStatusEnum')
            },
            responsive : 'sm'
        }, {
            label : '价格',
            name : 'price',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : '分类主键',
            name : 'category.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '分类',
            name : 'category.display',
            index : 'category.categoryCode_OR_category.categoryCode',
            width : 120,
            sortable : false,
            editable : true,
            align : 'left',
            editrules : {
            // required : true
            },
            editoptions : {
                dataInit : function(elem, opt) {
                    $(elem).treeselect({
                        url : WEB_ROOT + "/myt/md/category!categoryList"
                    });
                }
            },
            responsive : 'sm'
        }, {
            label : '排序号',
            name : 'recommendRank',
            editable : true,
            width : 80,
            align : 'center',
            responsive : 'sm'
        }, {
            label : '审核标识',
            name : 'commodityAuditStatus',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('commodityAuditStatusEnum')
            },
            editable : true,
            align : 'center'
        }, {
            label : '备注',
            name : 'memo',
            sortable : false,
            editable : true,
            width : 130,
            align : 'left'
        } ],
        postData : {
            "search['FETCH_brand']" : "LEFT",
            "search['FETCH_partner']" : "LEFT",
            "search['FETCH_category']" : "LEFT",
            "search['NN_c2cShopInfo']" : true,
        },
        inlineNav : {
            add : false
        },
        operations : function(itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>批量下架</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/md/commodity!putOff';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  下架？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select);
            var $select2 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>批量上架</a></li>');
            $select2.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/md/commodity!putOnAndRecFalse';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认 上架？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select2);

            // var $select3 = $('<li data-position="multi"
            // data-toolbar="show"><a href="javascript:;"><i class="fa
            // fa-print"></i>批量上架并恢复首页推荐</a></li>');
            // $select3.children("a").bind("click", function(e) {
            // e.preventDefault();
            // var ids = $grid.getAtLeastOneSelectedItem();
            // if (ids) {
            // var url = WEB_ROOT + '/myt/md/commodity!putOnAndRecTrue';
            // $grid.ajaxPostURL({
            // url : url,
            // success : function() {
            // $grid.refresh();
            // },
            // confirmMsg : "确认 上架并恢复首页推荐？",
            // data : {
            // ids : ids.join(",")
            // }
            // })
            // }
            // });
            // itemArray.push($select3);
        },
        editcol : 'sku',
        sortname : "lastModifiedDate",
        sorttype : "desc",
        editurl : WEB_ROOT + "/myt/md/commodity!doSave",
        delurl : WEB_ROOT + "/myt/md/commodity!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/commodity!inputTabs"
    });

    $(".grid-md-commodity-circle").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/commodity!findByPage?search['EQ_circle']=true",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '主图',
            name : 'smallPic',
            sortable : false,
            search : false,
            width : 80,
            align : 'center',
            formatter : Biz.md5CodeImgViewFormatter,
            frozen : true,
            responsive : 'sm'
        }, {
            label : '商品编码',
            name : 'sku',
            align : 'left',
            width : 80
        }, {
            label : '商品名称',
            name : 'title',
            sortable : false,
            width : 200,
            align : 'left'
        }, {
            label : '基准价格',
            name : 'price',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : '启用',
            name : 'circle',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        }, {
            label : '最小购买量',
            name : 'circleMinQuantity',
            width : 80,
            editable : true,
            align : 'right'
        }, {
            label : '3月价',
            name : 'commodityVaryPrice.m3CirclePrice',
            formatter : 'currency',
            editable : true,
            align : 'right',
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var rowid = $elem.closest("tr.jqgrow").attr("id");
                    var rowdata = $grid.jqGrid("getRowData", rowid);
                    if ($elem.val() == '') {
                        $elem.val((rowdata['price'] * 0.95).toFixed(0));
                    }
                }
            }
        }, {
            label : '4月价',
            name : 'commodityVaryPrice.m4CirclePrice',
            formatter : 'currency',
            editable : true,
            align : 'right',
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var rowid = $elem.closest("tr.jqgrow").attr("id");
                    var rowdata = $grid.jqGrid("getRowData", rowid);
                    if ($elem.val() == '') {
                        $elem.val((rowdata['price'] * 0.9).toFixed(0));
                    }

                }
            }
        }, {
            label : '5月价',
            name : 'commodityVaryPrice.m5CirclePrice',
            formatter : 'currency',
            editable : true,
            align : 'right',
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var rowid = $elem.closest("tr.jqgrow").attr("id");
                    var rowdata = $grid.jqGrid("getRowData", rowid);
                    if ($elem.val() == '') {
                        $elem.val((rowdata['price'] * 0.85).toFixed(0));
                    }
                }
            }
        }, {
            label : '6月价',
            name : 'commodityVaryPrice.m6CirclePrice',
            formatter : 'currency',
            editable : true,
            align : 'right',
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var rowid = $elem.closest("tr.jqgrow").attr("id");
                    var rowdata = $grid.jqGrid("getRowData", rowid);
                    if ($elem.val() == '') {
                        $elem.val((rowdata['price'] * 0.80).toFixed(0));
                    }
                }
            }
        }, {
            label : '标题',
            name : 'circleTitle',
            align : 'left',
            editable : true,
            width : 200
        }, {
            label : '状态',
            name : 'commodityStatus',
            align : 'center',
            width : 50,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('commodityStatusEnum')
            },
            responsive : 'sm'
        } ],
        postData : {
            "search['FETCH_brand']" : "LEFT",
            "search['FETCH_partner']" : "LEFT",
            "search['FETCH_category']" : "LEFT",
            "search['FETCH_commodityVaryPrice']" : "LEFT"
        },
        inlineNav : {
            add : false
        },
        editcol : 'sku',
        editurl : WEB_ROOT + "/myt/md/commodity!doSave",
        delurl : WEB_ROOT + "/myt/md/commodity!doDelete",
        fullediturl : WEB_ROOT + "/myt/md/commodity!inputTabs",
        operations : function(itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>批量下架</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/md/commodity!putOff';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  下架？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select);
        },
        setGroupHeaders : {
            groupHeaders : [ {
                startColumnName : 'circle',
                numberOfColumns : 7,
                titleText : '周期购'
            } ]
        }
    });

    $(".grid-md-vip-commodity").data("gridOptions", {
        url : WEB_ROOT + "/myt/md/commodity!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            responsive : 'sm'
        }, {
            label : '周期购标识',
            name : 'circle',
            edittype : 'checkbox',
            align : 'center'
        }, {
            label : '现货',
            name : 'canBuyNow',
            edittype : 'checkbox',
            align : 'center'
        }, {
            label : '商品编码',
            name : 'sku',
            align : 'left',
            width : 100
        }, {
            label : '商品名称',
            name : 'title',
            sortable : false,
            width : 300,
            align : 'left'
        }, {
            label : '状态',
            name : 'commodityStatus',
            align : 'center',
            width : 50,
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('commodityStatusEnum')
            },
            responsive : 'sm'
        }, {
            label : '商品价格',
            name : 'price',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : '商品成本价',
            name : 'costPrice',
            width : 80,
            formatter : 'currency',
            responsive : 'sm'
        }, {
            label : 'vip返利',
            name : 'vipCommodity.backMoney',
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : 'vip积分',
            name : 'vipCommodity.score',
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : '启用',
            name : 'vipCommodity.enable',
            edittype : 'checkbox',
            editable : true,
            align : 'center'
        } ],
        inlineNav : {
            add : false
        },
        addable : false,
        editcol : 'sku',
        editurl : WEB_ROOT + "/myt/md/commodity!doSave",
        fullediturl : WEB_ROOT + "/myt/md/commodity!inputTabs",
        operations : function(itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>批量启用/停用</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/md/commodity!vipCommodityEnable';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  启用/禁用？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select);

        },
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/vip/vip-commodity-rate!findByPage?search['EQ_commodity.id']=" + row_id,
                colModel : [ {
                    label : 'VIP等级',
                    hidden : true,
                    name : 'vipLevel.levelIndex',
                    width : 100
                }, {
                    label : 'VIP等级',
                    name : 'vipLevel.name',
                    width : 100
                }, {
                    label : '返利比率（%）',
                    name : 'levelRate',
                    editable : true,
                    align : 'center',
                    width : 100
                } ],
                cmTemplate : {
                    sortable : false
                },
                sortorder : "asc",
                sortname : "vipLevel.levelIndex",
                inlineNav : {
                    add : false
                },
                editurl : WEB_ROOT + '/myt/vip/vip-commodity-rate!doSave'
            });
        }
    });
});