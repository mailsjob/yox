$(function() {
    $(".form-myt-c2c-c2c-shop-info-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-customerProfile").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                    title : '选取账户',
                    callback : function(rowdata) {
                        jQuery.ajax({
                            type : "get",
                            url : WEB_ROOT + "/myt/c2c/c2c-shop-info!findByCustomer?customer=" + rowdata.id,
                            dataType : "json",
                            success : function(isData) {
                                if (isData.length >= 1) {
                                    var $errorAlert = $('<div class="alert alert-danger display-hide" style="display: block;"/>');
                                    $errorAlert.append('<button class="close" data-close="alert" type="button"></button>');
                                    $errorAlert.append('该账户已有店铺选作主账户！请选择其他账户作为主账户！');
                                    $errorAlert.prependTo($form);
                                    return;
                                } else {
                                    $form.setFormDatas({
                                        'customerProfile.id' : rowdata.id,
                                        'customerProfile.display' : rowdata.display
                                    });
                                }
                            }
                        });
                    }
                })
            });
        }
    });
});