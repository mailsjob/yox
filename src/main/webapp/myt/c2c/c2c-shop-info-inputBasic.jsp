<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-c2c-c2c-shop-info-inputBasic" action="${base}/myt/c2c/c2c-shop-info!doSave" method="post" data-editrulesurl="${base}/myt/c2c/c2c-shop-info!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-c2c-c2c-shop-info-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">店铺名</label>
                    <div class="controls">
                        <s:textfield name="shopName" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">店铺地址</label>
                    <div class="controls">
                        <s:textfield name="shopAddr" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">手机</label>
                    <div class="controls">
                        <s:textfield name="mobilePhone" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">微信</label>
                    <div class="controls">
                        <s:textfield name="weixin" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">旺旺</label>
                    <div class="controls">
                        <s:textfield name="wangwang" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">QQ</label>
                    <div class="controls">
                        <s:textfield name="qq" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">店铺图片</label>
                    <div class="controls">
                        <s:hidden name="shopPic" data-singleimage="true" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">保证金</label>
                    <div class="controls">
                        <s:textfield name="cautionMoney" disabled="true" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">先行赔付额</label>
                    <div class="controls">
                        <s:textfield name="advancePayouts" disabled="true" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">补贴额</label>
                    <div class="controls">
                        <s:textfield name="subsidy" disabled="true" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">补贴上限</label>
                    <div class="controls">
                        <s:textfield name="subsidyCap" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-c2c-c2c-shop-info-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/c2c/c2c-shop-info-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>