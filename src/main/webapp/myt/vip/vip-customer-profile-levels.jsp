<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-vip-customer-profile-levels" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-vip-customer-profile-levels" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-vip-customer-profile-levels").data("gridOptions", {
            url : "${base}/myt/vip/vip-customer-level-history!findByPage",
            colModel : [ {
                label : '流水号',
                name : 'id'                       
            },{
                label : '时间',
                name : 'createdDate',
                width : 150,
                align : 'center'
            }, {
                label : '等级提升积分',
                name : 'levelUpScore',
                width : 120,
                formatter: 'number',
                align : 'right'
            }, {
                label:'备注',
                name : 'memo',
                width : 150
            }, {
                label : '订单',
                name : 'boxOrder.display',
                index : 'boxOrder.orderSeq_OR_boxOrder.title',
                width : 200,
                align : 'left'
            } ],
           	postData : {
                "search['EQ_customerProfile.id']" : "<s:property value='#parameters.id'/>"
            },
            inlineNav : {
                add : false
            }
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>