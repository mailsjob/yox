$(function() {
    $(".form-myt-vip-vip-wx-qrcode-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-customerProfile").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/vip/vip-customer-profile!forward?_to_=selection',
                    title : '选取VIP客户',
                    callback : function(rowdata) {
                        jQuery.ajax({
                            type : "get",
                            url : WEB_ROOT + "/myt/vip/vip-wx-qrcode!findByVipCustomer?vipCustomer=" + rowdata.id,
                            dataType : "json",
                            success : function(isData) {
                                if (isData.length >= 1) {
                                    var $errorAlert = $('<div class="alert alert-danger display-hide" style="display: block;"/>');
                                    $errorAlert.append('<button class="close" data-close="alert" type="button"></button>');
                                    $errorAlert.append('该VIP客户已有二维码！请选择其VIP客户！');
                                    $errorAlert.prependTo($form);
                                    return;
                                } else {
                                    $form.setFormDatas({
                                        'vipCustomerProfile.id' : rowdata.id,
                                        'vipCustomerProfile.display' : rowdata.display
                                    });
                                }
                            }
                        });
                    }
                })
            });
        }
    });
});