<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">vip客户</a></li>
		<li><a class="tab-default" data-toggle="tab" href="#tab-auto">vip返利</a></li>
		<li><a class="tab-default" data-toggle="tab" href="#tab-auto">vip积分</a></li>
		
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-myt-vip-customer-profile-index">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_customerProfile.trueName_OR_customerProfile.trueName_OR_vipLevel.name']" 
								class="form-control" placeholder="客户姓名、vip等级...">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default hidden-inline-xs" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-vip-customer-profile-index"></table>
				</div>
			</div>
		</div>
		<div class="tab-pane">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-myt-vip-bak-money-history-index" action="#">
						<div class="form-group">
							<input type="text" name="search['CN_customerProfile.trueName_OR_customerProfile.nickName_OR_boxOrder.orderSeq_OR_boxOrder.title']" class="form-control input-large"
								placeholder="客户、订单..." />
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-vip-bak-money-history-index"></table>
				</div>
			</div>
		</div>
		<div class="tab-pane">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-myt-vip-score-history-index" action="#">
						<div class="form-group">
							<input type="text" name="search['CN_customerProfile.trueName_OR_customerProfile.nickName_OR_boxOrder.orderSeq_OR_boxOrder.title']" class="form-control input-large"
								placeholder="客户、订单..." />
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-vip-score-history-index"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/vip/vip-customer-profile-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
