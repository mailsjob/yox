$(function() {
    $(".grid-myt-vip-customer-profile-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/vip/vip-customer-profile!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
        }, {
            label : '时间',
            name : 'createdDate',
            width : 120,
            align : 'center'
        }, {
            label : '客户昵称',
            name : 'customerProfile.nickName',
            width : 100,
            align : 'left'
        }, {
            label : '真实姓名',
            name : 'customerProfile.trueName',
            width : 100,
            align : 'left'
        }, {
            label : '客户手机号',
            name : 'customerProfile.mobilePhone',
            width : 100,
            align : 'left'
        }, {
            label : 'vip等级',
            name : 'vipLevel.name',
            width : 100,
            align : 'left'
        }, {
            label : 'vip积分',
            editable : true,
            name : 'score',
            width : 60,
            align : 'right'
        }, {
            label : 'vip返利',
            name : 'backMoney',
            editable : true,
            formatter : 'currency',
            width : 60,
            align : 'right'
        } ],
        subGrid : true,
        gridDnD : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            // Grid.initRecursiveSubGrid(subgrid_id, row_id, "upstream.id");
            Grid.initRecursiveParentSubGrid(subgrid_id, WEB_ROOT + '/myt/vip/vip-customer-profile!findParent', row_id);
        },
        addable : false,
        inlineNav : {
            add : false
        },
        operations: function (itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>客户关系图</a></li>');
            $select.children("a").bind("click", function (e) {
                e.preventDefault();
                var id = $grid.getOnlyOneSelectedItem();
                $select.popupDialog({
                    url: WEB_ROOT + '/myt/customer/coin-customer-profile!forward?_to_=getXiaxian',
                    title: '下线展示图',
                    postData: {ids: id},
                })
            });
            itemArray.push($select);
        },
        editurl : WEB_ROOT + "/myt/vip/vip-customer-profile!doSave",
        fullediturl : WEB_ROOT + "/myt/vip/vip-customer-profile!inputTabs"
    });
    $(".grid-myt-vip-bak-money-history-index").data("gridOptions", {
        url : WEB_ROOT + "/myt/vip/vip-bak-money-history!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '时间',
            name : 'createdDate',
            width : 150,
            align : 'center'
        }, {
            label : '客户主键',
            name : 'customerProfile.id',
            width : 200,
            align : 'left'
        }, {
            label : '客户信息',
            name : 'customerProfile.display',
            index : 'customerProfile.trueName_OR_customerProfile.nickName',
            width : 200,
            align : 'left'
        }, {
            label : '订单',
            name : 'boxOrder.display',
            index : 'boxOrder.orderSeq_OR_boxOrder.title',
            width : 200,
            align : 'left'
        }, {
            label : '返利金额',
            name : 'backMoney',
            width : 100,
            formatter : 'currency',
            align : 'right'
        }, {
            label : '状态',
            name : 'status',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('vipBakMoneyHistoryStatusEnum')
            },
            width : 100,
            align : 'center'
        } ],
        footerrow : true,
        footerLocalDataColumn : [ 'backMoney' ],
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/vip/vip-bak-money-history!vipBakMoneyHistoryDetails?id=" + row_id,
                colModel : [ {
                    label : 'vip等级',
                    name : 'level.name',
                    align : 'center',
                    width : 100
                }, {
                    label : '返利金额',
                    name : 'backMoney',
                    width : 100,
                    formatter : 'currency'

                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
    $(".grid-myt-vip-score-history-index").data("gridOptions", {
        url : WEB_ROOT + "/myt/vip/vip-score-history!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '时间',
            name : 'createdDate',
            width : 150,
            align : 'center'
        }, {
            label : '客户主键',
            name : 'customerProfile.id',
            width : 100,
            align : 'center'
        }, {
            label : '客户信息',
            name : 'customerProfile.display',
            index : 'customerProfile',
            width : 100,
            align : 'left'
        }, {
            label : '订单',
            name : 'boxOrder.display',
            index : 'boxOrder.orderSeq_OR_boxOrder.title',
            width : 200,
            align : 'left'
        }, {
            label : '积分',
            name : 'score',
            width : 60,
            formatter : 'number',
            align : 'right'
        }, {
            label : '状态',
            name : 'status',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('vipScoreHistoryStatusEnum')
            },
            width : 80,
            align : 'center'
        } ],
        footerrow : true,
        footerLocalDataColumn : [ 'score' ],
    });
});
