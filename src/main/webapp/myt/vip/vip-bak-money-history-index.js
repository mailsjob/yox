$(function() {
    $(".grid-myt-vip-vip-bak-money-history-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/vip/vip-bak-money-history!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '客户信息',
            name : 'customerProfile.display',
            index : 'customerProfile.trueName',
            width : 200,
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '订单',
            name : 'boxOrder.display',
            index : 'boxOrder',
            width : 200,
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '返利金额',
            name : 'backMoney',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '状态',
            name : 'status',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('vipBakMoneyHistoryStatusEnum')
            },
            width : 80,
            editable: true,
            align : 'center'
        }]
    	/*,
        editurl : WEB_ROOT + '/myt/vip/vip-bak-money-history!doSave',
        delurl : WEB_ROOT + '/myt/vip/vip-bak-money-history!doDelete',
        fullediturl : WEB_ROOT + '/myt/vip/vip-bak-money-history!inputTabs'*/
    });
});
