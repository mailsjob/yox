<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-vip-customer-profile-inputBasic"
	action="${base}/myt/vip/vip-customer-profile!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<button type="submit" class="btn blue">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
	<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">等级</label>
					<div class="controls">
						<s:select name="vipLevel.id" list="vipLevelsMap" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">直接上线</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-search fa-search-customerProfile"></i> <i class="fa fa-times fa-times-customerProfile input-icon-secondary"></i>
									<s:textfield name="upstream.display"  readonly="true" />
									<s:hidden name="upstream.id" />
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button type="submit" class="btn blue">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
$(function() {

    $(".form-myt-vip-customer-profile-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);

            $form.find("i.fa-search-customerProfile").click(function() {
                $(this).popupDialog({
                    url : '${base}/myt/vip/vip-customer-profile!forward?_to_=selection',
                    title : '选取客户',
                    callback : function(json) {
                        $form.find("[name$='upstream.display']").val(json.display);
                        $form.find("[name$='upstream.id']").val(json.id);
                    }
                })
            });
			$form.find("i.fa-times-customerProfile").click(function() {
                $form.find("[name$='upstream.display']").val("");
                $form.find("[name$='upstream.id']").val("");
            });

        }

    });
    
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>
