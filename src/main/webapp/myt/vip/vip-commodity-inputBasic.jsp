<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-vip-vip-commodity-inputBasic" action="${base}/myt/vip/vip-commodity!doSave" method="post" data-editrulesurl="${base}/myt/vip/vip-commodity!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-vip-vip-commodity-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">小标题</label>
                    <div class="controls">
                        <s:textfield name="smallTitle" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">描述</label>
                    <div class="controls">
                        <s:textfield name="description" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">商品</label>
                    <div class="controls">
                        <s:textfield name="commodity.display" disabled="true" />
                        <s:hidden name="commodity.id" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">是否启用</label>
                    <div class="controls">
                        <s:radio name="enable" list="#application.enums.booleanLabel" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">图</label>
                    <div class="controls">
                        <s:hidden name="pic" data-singleimage="true" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">聚焦图</label>
                    <div class="controls">
                        <s:hidden name="focusSmallPic" data-singleimage="true" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">背景图</label>
                    <div class="controls">
                        <s:hidden name="backgroundPic" data-singleimage="true" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">缩略图</label>
                    <div class="controls">
                        <s:hidden name="smallPic" data-singleimage="true" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-vip-vip-commodity-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/vip/vip-commodity-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>