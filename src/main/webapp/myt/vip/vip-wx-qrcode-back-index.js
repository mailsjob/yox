$(function () {
    $(".grid-myt-vip-vip-wx-qrcode-back-index").data("gridOptions", {
        url: WEB_ROOT + '/myt/vip/vip-wx-qrcode-back!findByPage',
        colModel: [{
            label: '流水号',
            name: 'id',
            hidden: true
        }, {
            label: '关联二维码',
            name: 'wxQrcode',
            formatter: function (cellValue, options, rowdata, action) {
                // ajax获取关联二维码
                var pic = "未关联二维码";
                jQuery.ajax({
                    type: "get",
                    async: false,
                    url: WEB_ROOT + "/yox/yx-customer-bind-info!getQrcode?customerProfile=" + rowdata.vipCustomerProfile.id,
                    dataType: "json",
                    success: function (result) {
                        if (result != null) {
                            pic = Biz.md5CodeImgViewFormatter(result.picMd5);
                        }
                    }
                });
                return pic;
            },
            frozen: true,
            align: 'left'
        }, {
            label: '最近扫码时间',
            name: 'createdDate',
            sorttype: 'date',
            align: 'center'
        }, {
            label: '用户编号',
            name: 'vipCustomerProfile.id',
            index: 'vipCustomerProfile',
            align: 'left'
        }, {
            label: '用户昵称',
            name: 'vipCustomerProfile.customerProfile.nickName',
            index: 'vipCustomerProfile',
            align: 'left'
        }, {
            label: '上级编号',
            name: 'vipCustomerProfile.upstream.id',
            index: 'vipCustomerProfile',
            align: 'left'
        }, {
            label: '上级昵称',
            name: 'vipCustomerProfile.upstream.customerProfile.nickName',
            index: 'vipCustomerProfile',
            align: 'left'
        }, {
            label: 'openId',
            name: 'weixin',
            editable: true,
            align: 'left'
        }, {
            label: 'uninId',
            name: 'uninId',
            editable: true,
            align: 'left'
        }],
        postData: {
            "search['FETCH_vipCustomerProfile']": "LEFT"
        },
        sortname: 'createdDate'
    });
});
