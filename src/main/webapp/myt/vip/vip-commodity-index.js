$(function() {
    $(".grid-myt-vip-vip-commodity-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/vip/vip-commodity!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id'
        }, {
            label : '商品',
            name : 'display',
            align : 'left'
        }, {
            label : '小标题',
            name : 'smallTitle',
            editable : true,
            align : 'left'
        }, {
            label : '图',
            name : 'pic',
            formatter : Biz.md5CodeImgViewFormatter,
            align : 'left'
        }, {
            label : '聚焦图',
            name : 'focusSmallPic',
            formatter : Biz.md5CodeImgViewFormatter,
            align : 'left'
        }, {
            label : '背景图',
            name : 'backgroundPic',
            formatter : Biz.md5CodeImgViewFormatter,
            align : 'left'
        }, {
            label : '缩略图',
            name : 'smallPic',
            formatter : Biz.md5CodeImgViewFormatter,
            align : 'left'
        }, {
            label : '描述',
            name : 'description',
            editable : true,
            align : 'left'
        }, {
            label : '是否启用',
            name : 'enable',
            formatter : 'checkbox',
            editable : true,
            align : 'center'
        } ],
        inlineNav : {
            add : false,
            del : false
        },
        addable : false,
        postData : {
            "search['EQ_commodity.solrFilterType']" : "LM"
        },
        editurl : WEB_ROOT + '/myt/vip/vip-commodity!doSave',
        delurl : WEB_ROOT + '/myt/vip/vip-commodity!doDelete',
        fullediturl : WEB_ROOT + '/myt/vip/vip-commodity!inputTabs',
        operations : function(itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>批量启用/停用</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/md/commodity!vipCommodityEnable';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  启用/禁用？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select);
        }
    });
});
