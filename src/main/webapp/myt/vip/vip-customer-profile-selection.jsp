<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
			data-grid-search=".grid-vip-customerProfile-selection">
			<div class="form-group">
				<label class="sr-only">vip客户</label> <input type="text"
					name="search['CN_customerProfile.nickName_OR_customerProfile.trueName_OR_customerProfile.mobilePhone_OR_customerProfile.email_OR_vipLevel.name']"
					 class="form-control input-xlarge"
					placeholder="昵称、真实姓名、手机号码、邮件地址、等级名称..." />
			</div>
			<button class="btn default" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>

		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-vip-customerProfile-selection"></table>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-vip-customerProfile-selection").data("gridOptions", {
            url : '${base}/myt/vip/vip-customer-profile!findByPage',
            colModel : [  {
                label : '流水号',
                name : 'id',                          
            }, {
                label : 'vip客户信息',
                name : 'display',
                width : 100,
                align : 'left'
            },{
                label : '昵称',
                name : 'customerProfile.nickName',
                width : 100
            }, {
                label : '真实姓名',
                name : 'customerProfile.trueName',
                width : 100
            }, {
                label : '电话',
                name : 'customerProfile.mobilePhone',
                width : 150
            }, {
                label : '邮件',
                name : 'customerProfile.email',
                width : 150
            }, {
                label : 'vip等级',
                name : 'vipLevel.name',
                width : 100,
                align : 'left'
            }, {
                label : 'vip积分',
                name : 'score',
                width : 60,
                formatter: 'number',
                align : 'right'
            }, {
                label : 'vip返利',
                name : 'backMoney',
                width : 60,
                align : 'right'
            }],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
