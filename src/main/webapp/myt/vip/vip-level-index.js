$(function() {
    $(".grid-myt-vip-vip-level-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/vip/vip-level!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '代码',
            name : 'code',
            width : 100,
            editable : true,
            editoptions : {
                updatable : false
            },
            align : 'left'
        }, {
            label : '名称',
            name : 'name',
            width : 100,
            editable : true,
            align : 'left'
        }, {
            label : '积分',
            name : 'score',
            width : 100,
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : '返利比率',
            name : 'backMoneyRate',
            width : 100,
            formatter : 'currency',
            editable : true,
            align : 'right'
        }, {
            label : '排序号',
            name : 'levelIndex',
            width : 100,
            editable : true,
            align : 'right'
        } ],
        sortorder : "asc",
        sortname : "levelIndex",
        addable : false,
        editurl : WEB_ROOT + '/myt/vip/vip-level!doSave'
    });
});
