<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-vip-vip-wx-qrcode-inputBasic" action="${base}/myt/vip/vip-wx-qrcode!doSave" method="post" data-editrulesurl="${base}/myt/vip/vip-wx-qrcode!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-vip-vip-wx-qrcode-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">VIP客户信息</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-customerProfile"></i>
                            <s:textfield name="vipCustomerProfile.display" />
                            <s:hidden name="vipCustomerProfile.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-vip-vip-wx-qrcode-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/vip/vip-wx-qrcode-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>