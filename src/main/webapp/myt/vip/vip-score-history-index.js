$(function() {
    $(".grid-myt-vip-vip-score-history-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/vip/vip-score-history!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '客户信息',
            name : 'customerProfile.display',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '订单',
            name : 'boxOrder.display',
            index : 'boxOrder',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '积分',
            name : 'score',
            width : 60,
            formatter: 'number',
            editable: true,
            align : 'right'
        }, {
            label : '状态',
            name : 'status',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('vipScoreHistoryStatusEnum')
            },
            width : 80,
            editable: true,
            align : 'center'
        } ],    
      
        editurl : WEB_ROOT + '/myt/vip/vip-score-history!doSave',
        delurl : WEB_ROOT + '/myt/vip/vip-score-history!doDelete',
        fullediturl : WEB_ROOT + '/myt/vip/vip-score-history!inputTabs'
    });
});
