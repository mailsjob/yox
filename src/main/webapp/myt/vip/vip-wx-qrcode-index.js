$(function () {
    $(".grid-myt-vip-vip-wx-qrcode-index").data("gridOptions", {
        url: WEB_ROOT + '/myt/vip/vip-wx-qrcode!findByPage',
        colModel: [{
            label: '流水号',
            name: 'id',
            hidden: true
        }, {
            label: '二维码图片',
            name: 'picMd5',
            width: 20,
            formatter: Biz.md5CodeImgViewFormatter,
            align: 'left'
        }, {
            label: 'VIP客户信息',
            name: 'vipCustomerProfile.display',
            index: 'vipCustomerProfile',
            align: 'left'
        }, {
            label: 'VIP客户编号',
            name: 'vipCustomerProfile.id',
            index: 'vipCustomerProfile',
            hidden: true
        }],
        postData: {
            "search['FETCH_vipCustomerProfile']": "LEFT"
        }
    });
});
