$(function() {
    $(".grid-myt-pairui-delivery-sendout-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/pairui/delivery-sendout!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '订单号',
            name : 'billno',
            editable : true,
            align : 'left'
        }, {
            label : '快递单号',
            name : 'deliveryno',
            editable : true,
            align : 'left'
        }, {
            label : '快递名称',
            name : 'express',
            editable : true,
            align : 'left'
        }, {
            label : '订单状态',
            name : 'state',
            editable : true,
            align : 'left'
        }, {
            label : '仓库接收时间',
            name : 'receivetime',
            formatter : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '化零时间',
            name : 'hltime',
            formatter : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '开始拣选时间',
            name : 'pickingtime',
            formatter : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '等待过分拣线时间',
            name : 'checkingtime',
            formatter : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '过完分拣线时间',
            name : 'finishtime',
            formatter : 'date',
            editable : true,
            align : 'center'
        } ],
        editurl : WEB_ROOT + '/myt/pairui/delivery-sendout!doSave',
        delurl : WEB_ROOT + '/myt/pairui/delivery-sendout!doDelete',
        fullediturl : WEB_ROOT + '/myt/pairui/delivery-sendout!inputTabs'
    });
});
