<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-pairui-order-file-inputBasic"
	action="${base}/myt/pairui/pairui-order-file!upload" method="post" 
	enctype="multipart/form-data" 
	data-editrulesurl="${base}/myt/pairui/pairui-order-file!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-pairui-order-file-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
        <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-3">订单文件(.xlsx)</label>
					<div class="col-md-4">
						<input type="file" class="default" name="file" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-pairui-order-file-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script type="text/javascript">
$(function() {
    $(".form-myt-pairui-order-file-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
        }
    });
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>