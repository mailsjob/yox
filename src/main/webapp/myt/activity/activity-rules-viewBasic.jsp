<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="container-fluid data-view">
    <div class="well form-horizontal">
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="#application.enums.booleanLabel[canUseGiftpaper]" label="能否使用优惠券" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="ruleTemplate" label="选择规则模板" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="#application.enums.booleanLabel[isOver]" label="是否可以叠加" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="ruleIndex" label="规则排序" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="activity" label="选择活动" />
            </div>
        </div>

        <div class="row-fluid">
            <div class="span6">
                <s2:property value="fitModule" label="适宜模块" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data1Value" label="规则一" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data2Value" label="规则二" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data3Value" label="规则三" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data4Value" label="规则四" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data5Value" label="规则五" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data6Value" label="规则六" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data7Value" label="规则七" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data8Value" label="规则八" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data9Value" label="规则九" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data10Value" label="规则十" />
            </div>
        </div>
    </div>
</div>