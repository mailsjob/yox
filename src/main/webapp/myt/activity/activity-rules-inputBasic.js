$(function() {
    $(".form-myt-activity-activity-rules-inputBasic").data("formOptions", {
        bindEvents : function() {
            // 选取活动
            $form = $(this);
            var ruleTemplateId = $form.find("input[name='ruleTemplate.id']").val();
            jQuery.ajax({
                type : "get",
                url : WEB_ROOT + "/myt/activity/rule-template!findById?id=" + ruleTemplateId,
                dataType : "json",
                success : function(rowdata) {
                    showTitle(rowdata, $form);
                }
            });
            $form.find(".fa-select-activity").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/md/activity!forward?_to_=selection',
                    title : '选取活动',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'activity.id' : rowdata.id,
                            'activity.display' : rowdata.display
                        });

                    }
                })
            });
            // 双击选取 活动规则模板
            $form.find(".fa-select-ruleTemplate").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/activity/rule-template!forward?_to_=selection',
                    title : '选取规则模板',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'ruleTemplate.id' : rowdata.id,
                            'ruleTemplate.title' : rowdata.title
                        });
                        showTitle(rowdata, $form);
                    }
                })
            });
        }
    });
});
function showTitle(rowdata, $form) {
    // 活动模板标题一动态显示
    if (!$.isEmptyObject(rowdata.data1Title) && (rowdata.data1Title != '')) {
        var $divRule1 = $form.find("#divRule1");
        $divRule1.removeAttr("hidden");
        var $lblTitle1 = $form.find("#lblTitle1");
        $lblTitle1.text(rowdata.data1Title);
    }
    // 活动模板标题二动态显示
    if (!$.isEmptyObject(rowdata.data2Title) && (rowdata.data2Title != '')) {
        var $divRule2 = $form.find("#divRule2");
        $divRule2.removeAttr("hidden");
        var $lblTitle2 = $form.find("#lblTitle2");
        $lblTitle2.text(rowdata.data2Title);
    }
    // 活动模板标题三动态显示
    if (!$.isEmptyObject(rowdata.data3Title) && (rowdata.data3Title != '')) {
        var $divRule3 = $form.find("#divRule3");
        $divRule3.removeAttr("hidden");
        var $lblTitle3 = $form.find("#lblTitle3");
        $lblTitle3.text(rowdata.data3Title);
    }
    // 活动模板标题四动态显示
    if (!$.isEmptyObject(rowdata.data4Title) && (rowdata.data4Title != '')) {
        var $divRule4 = $form.find("#divRule4");
        $divRule4.removeAttr("hidden");
        var $lblTitle4 = $form.find("#lblTitle4");
        $lblTitle4.text(rowdata.data4Title);
    }
    // 活动模板标题五动态显示
    if (!$.isEmptyObject(rowdata.data5Title) && (rowdata.data5Title != '')) {
        var $divRule5 = $form.find("#divRule5");
        $divRule5.removeAttr("hidden");
        var $lblTitle5 = $form.find("#lblTitle5");
        $lblTitle5.text(rowdata.data5Title);
    }
    // 活动模板标题六动态显示
    if (!$.isEmptyObject(rowdata.data6Title) && (rowdata.data6Title != '')) {
        var $divRule6 = $form.find("#divRule6");
        $divRule6.removeAttr("hidden");
        var $lblTitle6 = $form.find("#lblTitle6");
        $lblTitle6.text(rowdata.data6Title);
    }
    // 活动模板标题七动态显示
    if (!$.isEmptyObject(rowdata.data7Title) && (rowdata.data7Title != '')) {
        var $divRule7 = $form.find("#divRule7");
        $divRule7.removeAttr("hidden");
        var $lblTitle7 = $form.find("#lblTitle7");
        $lblTitle7.text(rowdata.data7Title);
    }
    // 活动模板标题八动态显示
    if (!$.isEmptyObject(rowdata.data8Title) && (rowdata.data8Title != '')) {
        var $divRule8 = $form.find("#divRule8");
        $divRule8.removeAttr("hidden");
        var $lblTitle8 = $form.find("#lblTitle8");
        $lblTitle8.text(rowdata.data8Title);
    }
    // 活动模板标题九动态显示
    if (!$.isEmptyObject(rowdata.data9Title) && (rowdata.data9Title != '')) {
        var $divRule9 = $form.find("#divRule9");
        $divRule9.removeAttr("hidden");
        var $lblTitle9 = $form.find("#lblTitle9");
        $lblTitle9.text(rowdata.data9Title);
    }
    // 活动模板标题十动态显示
    if (!$.isEmptyObject(rowdata.data10Title) && (rowdata.data10Title != '')) {
        var $divRule10 = $form.find("#divRule10");
        $divRule10.removeAttr("hidden");
        var $lblTitle10 = $form.find("#lblTitle10");
        $lblTitle10.text(rowdata.data10Title);
    }
}