<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-myt-activity-activity-rules-r2-collection" data-grid="table"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-activity-activity-rules-r2-collection").data("gridOptions", {
            url : "${base}/myt/activity/activity-rules-r2-collection!findByPage?search['EQ_activityRules.id']=<s:property value='#parameters.id'/>",
            colModel : [ {
                label : '集合主键',
                name : 'collection.id',
                hidden : true,
                hidedlg : true,
                editable : true
            }, {
                label : '集合',
                name : 'collection.title',
                width : 200,
                editable : true,
                editoptions : {
                    placeholder : '请双击选取',
                    dataInit : function(elem) {
                        var $grid = $(this);
                        var $elem = $(elem);
                        // Callback
                        var select = function(sid, display) {
                            $grid.jqGrid("setEditingRowdata", {
                                'collection.id' : sid,
                                'collection.title' : display
                            });
                        }
                        $elem.dblclick(function() {
                            $(this).popupDialog({
                                url : WEB_ROOT + '/myt/md/collection!forward?_to_=selection',
                                title : '选取集合',
                                callback : function(item) {
                                    select(item.id, item.title);
                                }
                            })
                        });
                    }
                },
                align : 'left'
            }, {
                label : '白名单或黑名单',
                name : 'whiteOrBlack',
                align : 'center',
                width : 80,
                stype : 'select',
                editable : true,
                searchoptions : {
                    value : Util.getCacheEnumsByType('whiteOrBlackEnum')
                }
            } ],
            editurl : "${base}/myt/activity/activity-rules-r2-collection!doSave?activityRules.id=<s:property value='#parameters.id'/>",
            delurl : "${base}/myt/activity/activity-rules-r2-collection!doDelete"
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
