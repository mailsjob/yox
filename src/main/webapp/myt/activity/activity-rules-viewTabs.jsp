<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<s2:tabbedpanel>
    <ul>
        <li>
            <a href="activity-rules!view?id=<s:property value='#parameters.id'/>"> <span>基本信息</span>
            </a>
        </li>
        <li>
            <a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
               href="activity-rules!forward?_to_=collection&id=<s:property value='#parameters.id'/>"> <span>集合关联</span>
            </a>
        </li>
    </ul>
</s2:tabbedpanel>