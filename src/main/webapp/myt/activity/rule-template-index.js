$(function() {
    $(".grid-myt-activity-rule-template-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/activity/rule-template!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '模板代码',
            name : 'templateCode',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '模板名称',
            name : 'title',
            width : 200,
            editable : true,
            align : 'left'
        } ],
        editurl : WEB_ROOT + '/myt/activity/rule-template!doSave',
        delurl : WEB_ROOT + '/myt/activity/rule-template!doDelete',
        fullediturl : WEB_ROOT + '/myt/activity/rule-template!inputTabs'
    });
});
