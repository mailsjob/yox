<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="container-fluid data-view">
    <div class="well form-horizontal">
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="templateCode" label="模板代码" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="title" label="模板名称" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data1Title" label="模板一" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data2Title" label="模板二" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data3Title" label="模板三" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data4Title" label="模板四" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data5Title" label="模板五" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data6Title" label="模板六" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data7Title" label="模板七" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data8Title" label="模板八" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data9Title" label="模板九" />
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <s2:property value="data10Title" label="模板十" />
            </div>
        </div>
    </div>
</div>