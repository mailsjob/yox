<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
    <div class="col-md-12">
        <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-myt-activity-activity-rules-selection">
            <div class="form-group">
                <label class="sr-only">规则</label> <input type="text" name="search['CN_title']" class="form-control input-xlarge" placeholder="规则名称..." />
            </div>
            <button class="btn default" type="reset">
                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
            </button>
            <button class="btn green" type="submmit">
                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
            </button>

        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="grid-myt-activity-activity-rules-selection"></table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-activity-activity-rules-selection").data("gridOptions", {
            url : "${base}/myt/activity/activity-rules!findByPage",
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true
            }, {
                label : '规则名称',
                name : 'ruleTitle',
                width : 70,
                editable : true,
                align : 'left'
            }, {
                label : '规则排序',
                name : 'ruleIndex',
                width : 70,
                editable : true,
                align : 'right'
            }, {
                label : '适用的活动',
                name : 'activity.display',
                width : 70,
                editable : true,
                align : 'left'
            }, {
                label : '使用的模板',
                name : 'ruleTemplate.title',
                width : 70,
                editable : true,
                align : 'left'
            }, {
                label : '适宜模块',
                name : 'fitModule',
                width : 80,
                editable : true,
                align : 'left',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('fitModuleEnum')
                }
            }, {
                label : '是否可以叠加',
                name : 'isOver',
                width : 70,
                formatter : 'checkbox',
                edittype : "checkbox",
                editable : true,
                align : 'center'
            }, {
                label : '能否使用优惠券',
                name : 'canUseGiftpaper',
                width : 80,
                formatter : 'checkbox',
                edittype : "checkbox",
                editable : true,
                align : 'center'
            }, {
                label : '是否可用  ',
                name : 'isEnable',
                width : 80,
                formatter : 'checkbox',
                edittype : "checkbox",
                editable : true,
                align : 'center'
            } ],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>