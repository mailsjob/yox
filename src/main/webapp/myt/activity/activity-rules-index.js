$(function() {
    $(".grid-myt-activity-activity-rules-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/activity/activity-rules!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '规则名称',
            name : 'ruleTitle',
            width : 70,
            editable : true,
            align : 'left'
        }, {
            label : '规则排序',
            name : 'ruleIndex',
            width : 70,
            editable : true,
            align : 'right'
        }, {
            label : '适用的活动',
            name : 'activity.display',
            width : 70,
            editable : true,
            align : 'left'
        }, {
            label : '使用的模板',
            name : 'ruleTemplate.title',
            width : 70,
            editable : true,
            align : 'left'
        }, {
            label : '适宜模块',
            name : 'fitModule',
            width : 80,
            editable : true,
            align : 'left',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('fitModuleEnum')
            }
        }, {
            label : '是否可以叠加',
            name : 'isOver',
            width : 70,
            formatter : 'checkbox',
            edittype : "checkbox",
            editable : true,
            align : 'center'
        }, {
            label : '能否使用优惠券',
            name : 'canUseGiftpaper',
            width : 80,
            formatter : 'checkbox',
            edittype : "checkbox",
            editable : true,
            align : 'center'
        }, {
            label : '是否可用  ',
            name : 'isEnable',
            width : 80,
            formatter : 'checkbox',
            edittype : "checkbox",
            editable : true,
            align : 'center'
        } ],
        sortname : "isOver",
        sortorder : "desc",
        editurl : WEB_ROOT + '/myt/activity/activity-rules!doSave',
        delurl : WEB_ROOT + '/myt/activity/activity-rules!doDelete',
        fullediturl : WEB_ROOT + '/myt/activity/activity-rules!inputTabs'
    });
});
