<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-activity-rule-template-inputBasic" action="${base}/myt/activity/rule-template!doSave" method="post" data-editrulesurl="${base}/myt/activity/rule-template!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-activity-rule-template-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板代码</label>
                    <div class="controls">
                        <s:textfield name="templateCode" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板名称</label>
                    <div class="controls">
                        <s:textfield name="title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板一</label>
                    <div class="controls">
                        <s:textfield name="data1Title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板二</label>
                    <div class="controls">
                        <s:textfield name="data2Title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板三</label>
                    <div class="controls">
                        <s:textfield name="data3Title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板四</label>
                    <div class="controls">
                        <s:textfield name="data4Title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板五</label>
                    <div class="controls">
                        <s:textfield name="data5Title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板六</label>
                    <div class="controls">
                        <s:textfield name="data6Title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板七</label>
                    <div class="controls">
                        <s:textfield name="data7Title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板八</label>
                    <div class="controls">
                        <s:textfield name="data8Title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板九</label>
                    <div class="controls">
                        <s:textfield name="data9Title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">模板十</label>
                    <div class="controls">
                        <s:textfield name="data10Title" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-activity-rule-template-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/activity/rule-template-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>