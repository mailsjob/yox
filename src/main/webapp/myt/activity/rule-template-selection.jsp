<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
    <div class="col-md-12">
        <form action="#" method="get" class="form-inline form-validation form-search form-search-init" data-grid-search=".grid-myt-activity-rule-template-selection">
            <div class="form-group">
                <label class="sr-only">规则模板</label> <input type="text" name="search['CN_title']" class="form-control input-xlarge" placeholder="模板名称..." />
            </div>
            <button class="btn default" type="reset">
                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
            </button>
            <button class="btn green" type="submmit">
                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
            </button>

        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="grid-myt-activity-rule-template-selection"></table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-activity-rule-template-selection").data("gridOptions", {
            url : "${base}/myt/activity/rule-template!findByPage",
            colModel : [ {
                label : '流水号',
                name : 'id',
                hidden : true
            }, {
                label : '模板代码',
                name : 'templateCode',
                width : 200,
                editable : true,
                align : 'left'
            }, {
                label : '模板名称',
                name : 'title',
                width : 200,
                editable : true,
                align : 'left'
            } , {
                label : '模板抬头一',
                name : 'data1Title',
                hidden : true
            } , {
                label : '模板抬头二',
                name : 'data2Title',
                hidden : true
            } , {
                label : '模板抬头三',
                name : 'data3Title',
                hidden : true
            } , {
                label : '模板抬头四',
                name : 'data4Title',
                hidden : true
            } , {
                label : '模板抬头五',
                name : 'data5Title',
                hidden : true
            } , {
                label : '模板抬头六',
                name : 'data6Title',
                hidden : true
            } , {
                label : '模板抬头七',
                name : 'data7Title',
                hidden : true
            } , {
                label : '模板抬头八',
                name : 'data8Title',
                hidden : true
            } , {
                label : '模板抬头九',
                name : 'data9Title',
                hidden : true
            } , {
                label : '模板抬头十',
                name : 'data10Title',
                hidden : true
            } ],
            rowNum : 10,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>