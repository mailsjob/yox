<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-activity-activity-rules-inputBasic" action="${base}/myt/activity/activity-rules!doSave" method="post" data-editrulesurl="${base}/myt/activity/activity-rules!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-activity-activity-rules-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">规则名称</label>
                    <div class="controls">
                        <s:textfield name="ruleTitle" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">规则排序</label>
                    <div class="controls">
                        <s:textfield name="ruleIndex" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">选择规则模板</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-ruleTemplate"></i>
                            <s:textfield name="ruleTemplate.title" />
                            <s:hidden name="ruleTemplate.id" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">选择活动</label>
                    <div class="controls">
                        <div class="input-icon right">
                            <i class="fa fa-ellipsis-horizontal fa-select-activity"></i>
                            <s:textfield name="activity.display" />
                            <s:hidden name="activity.id" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">是否可用</label>
                    <div class="controls">
                        <s:radio name="isEnable" list="#application.enums.booleanLabel" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">能否使用优惠券</label>
                    <div class="controls">
                        <s:radio name="canUseGiftpaper" list="#application.enums.booleanLabel" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">是否可以叠加</label>
                    <div class="controls">
                        <s:radio name="isOver" list="#application.enums.booleanLabel" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">适宜模块</label>
                    <div class="controls">
                        <s:select name="fitModule" list="#application.enums.fitModuleEnum" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">开始时间</label>
                    <div class="controls">
                        <s3:datetextfield name="startTime" format="timestamp" data-timepicker="true"></s3:datetextfield>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">结束时间</label>
                    <div class="controls">
                        <s3:datetextfield name="endTime" format="timestamp" data-timepicker="true"></s3:datetextfield>
                    </div>
                </div>
            </div>
        </div>
        <div id="divRule1" class="row" hidden="true">
            <div class="col-md-6">
                <div class="form-group">
                    <label id="lblTitle1" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data1Value" />
                    </div>
                </div>
            </div>
            <div id="divRule2" class="col-md-6" hidden="true">
                <div class="form-group">
                    <label id="lblTitle2" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data2Value" />
                    </div>
                </div>
            </div>
        </div>
        <div id="divRule3" class="row" hidden="true">
            <div class="col-md-6">
                <div class="form-group">
                    <label id="lblTitle3" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data3Value" />
                    </div>
                </div>
            </div>
            <div id="divRule4" class="col-md-6" hidden="true">
                <div class="form-group">
                    <label id="lblTitle4" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data4Value" />
                    </div>
                </div>
            </div>
        </div>
        <div id="divRule5" class="row" hidden="true">
            <div class="col-md-6">
                <div class="form-group">
                    <label id="lblTitle5" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data5Value" />
                    </div>
                </div>
            </div>
            <div id="divRule6" class="col-md-6" hidden="true">
                <div class="form-group">
                    <label id="lblTitle6" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data6Value" />
                    </div>
                </div>
            </div>
        </div>
        <div id="divRule7" class="row" hidden="true">
            <div class="col-md-6">
                <div class="form-group">
                    <label id="lblTitle7" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data7Value" />
                    </div>
                </div>
            </div>
            <div id="divRule8" class="col-md-6" hidden="true">
                <div class="form-group">
                    <label id="lblTitle8" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data8Value" />
                    </div>
                </div>
            </div>
        </div>
        <div id="divRule9" class="row" hidden="true">
            <div class="col-md-6">
                <div class="form-group">
                    <label id="lblTitle9" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data9Value" />
                    </div>
                </div>
            </div>
            <div id="divRule10" class="col-md-6" hidden="true">
                <div class="form-group">
                    <label id="lblTitle10" class="control-label"></label>
                    <div class="controls">
                        <s:textfield name="data10Value" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-activity-activity-rules-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/activity/activity-rules-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>