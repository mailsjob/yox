<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-distribute-delivery-inputBasic"
	action="${base}/myt/partner/distribute-delivery!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
			data-grid-reload=".grid-myt-distribute-delivery">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
		<s3:button disabled="%{disallowChargeAgainst!=null}" type="submit" cssClass="btn red pull-right"
			data-grid-reload=".grid-myt-distribute-delivery" data-form-action="${base}/myt/partner/distribute-delivery!chargeAgainst"
			data-confirm="确认冲销当前销售单？">
			<s:property value="%{disallowChargeAgainst!=null?disallowChargeAgainst:'红冲'}" />
		</s3:button>
	</div>

	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">凭证状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.voucherStateEnum[voucherState]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 ">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:textfield name="voucher" readonly="%{notNew}" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<s3:datetextfield name="voucherDate" format="date" current="true" readonly="true" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:select name="voucherUser.id" list="usersMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:select name="voucherDepartment.id" list="departmentsMap" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">代理分销商</label>
					<div class="controls">
						<s:textfield name="agentPartner.display" data-toggle="treeselect"
							data-url="%{#attr.base+'/myt/partner/partner!treeDatas?partnerType=AGENT'}" />
						<s:hidden name="agentPartner.id" />
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">发货类型</label>
					<div class="controls">
						<s:radio name="deliveryType" list="#application.enums.deliveryTypeEnum"
							data-profile-param="default_distribute_delivery_type" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">销售单名称</label>
					<div class="controls">
						<s:textfield name="title" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人</label>
					<div class="controls">
						<s:textfield name="receivePerson" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">电话</label>
					<div class="controls">
						<s:textfield name="mobilePhone" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">邮编</label>
					<div class="controls">
						<s:textfield name="postCode" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收货地址</label>
					<div class="controls">
						<div class="input-group">
							<s:textfield name="deliveryAddr" />
							<div class="input-group-btn">
								<button tabindex="-1" class="btn default btn_delivery_print" type="button">打印发货清单</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">代发货库房</label>
					<div class="controls">
                        <s:textfield name="stockPartner.display" data-toggle="treeselect"
                            data-url="%{#attr.base+'/myt/partner/partner!treeDatas?partnerType=STOCK'}" />
                        <s:hidden name="stockPartner.id" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">备注说明</label>
					<div class="controls">
						<s:textfield name="memo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-distribute-delivery-inputBasic" data-grid="items" data-readonly="true"
					data-pk='<s:property value="#parameters.id"/>'></table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">整单折扣额</label>
					<div class="controls">
						<s:textfield name="discountAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">总计原始金额</label>
					<div class="controls">
						<s:textfield name="totalOriginalAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">折后金额</label>
					<div class="controls">
						<s:textfield name="commodityAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">交税总金额</label>
					<div class="controls">
						<s:textfield name="totalTaxAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">含税总金额</label>
					<div class="controls">
						<s:textfield name="commodityAndTaxAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收取运费</label>
					<div class="controls">
						<s:textfield name="chargeLogisticsAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">应收总金额</label>
					<div class="controls">
						<s:textfield name="totalAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">已收总金额</label>
					<div class="controls">
						<s:textfield name="payedAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收款账户</label>
					<div class="controls">
						<s:select name="accountSubject.id" list="paymentAccountSubjects" disabled="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收款备注</label>
					<div class="controls">
						<s:textarea name="paymentReference" rows="3" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<s:if test="redwordDate==null">
		<div class="form-actions right">
			<button type="submit" class="btn blue" data-grid-reload=".grid-myt-distribute-delivery">
				<i class="fa fa-check"></i> 保存
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<s:if test="%{deliveryTime==null}">
				<button class="btn red pull-left" type="submit" data-grid-reload=".grid-myt-distribute-delivery"
					data-form-action="${base}/myt/partner/distribute-delivery!chargeAgainst" data-confirm="确认冲销当前销售单？">红冲</button>
			</s:if>
		</div>
	</s:if>
</form>
<script src="${base}/myt/partner/distribute-delivery-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
