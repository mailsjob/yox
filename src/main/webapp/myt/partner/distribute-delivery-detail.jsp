<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-distribute-delivery-inputBasic"
	action="${base}/myt/partner/distribute-delivery!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	

	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">凭证状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.voucherStateEnum[voucherState]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 ">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:property value="voucher" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<%--<s3:datetextfield name="voucherDate" format="date" current="true" readonly="true" /> --%>
								<s:property value="voucherDate" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:property value="voucherUser.display" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:property value="voucherDepartment.display" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">代理分销商</label>
					<div class="controls">
						<s:property value="agentPartner.display" />
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">发货类型</label>
					<div class="controls">
						<s:radio name="deliveryType" list="#application.enums.deliveryTypeEnum"
							data-profile-param="default_sale_delivery_type" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">销售单名称</label>
					<div class="controls">
						<s:property value="title" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人</label>
					<div class="controls">
						<s:property value="receivePerson" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">电话</label>
					<div class="controls">
						<s:property value="mobilePhone" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">邮编</label>
					<div class="controls">
						<s:property value="postCode" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收货地址</label>
					<div class="controls">
						<div class="input-group">
							<s:property value="deliveryAddr" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">代发货库房</label>
					<div class="controls">
						<s:property value="stockPartner.display" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">备注说明</label>
					<div class="controls">
						<s:property value="memo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-distribute-delivery-detail" data-grid="items" data-readonly="true" 
					data-pk='<s:property value="#parameters.id"/>'></table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">整单折扣额</label>
					<div class="controls">
						<s:property value="discountAmount" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">总计原始金额</label>
					<div class="controls">
						<s:property value="totalOriginalAmount" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">折后金额</label>
					<div class="controls">
						<s:property value="commodityAmount" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">交税总金额</label>
					<div class="controls">
						<s:property value="totalTaxAmount" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">含税总金额</label>
					<div class="controls">
						<s:property value="commodityAndTaxAmount" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收取运费</label>
					<div class="controls">
						<s:property value="chargeLogisticsAmount" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">应收总金额</label>
					<div class="controls">
						<s:property value="totalAmount" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">已收总金额</label>
					<div class="controls">
						<s:property value="payedAmount" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收款账户</label>
					<div class="controls">
						<s:property value="accountSubject.display" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收款备注</label>
					<div class="controls">
						<s:property value="paymentReference" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<s:if test="redwordDate==null">
		<div class="form-actions right">
			<button type="submit" class="btn blue" data-grid-reload=".grid-myt-distribute-delivery">
				<i class="fa fa-check"></i> 保存
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<s:if test="%{deliveryTime==null}">
				<button class="btn red pull-left" type="submit" data-grid-reload=".grid-myt-distribute-delivery"
					data-form-action="${base}/myt/partner/distribute-delivery!chargeAgainst" data-confirm="确认冲销当前销售单？">红冲</button>
			</s:if>
		</div>
	</s:if>
</form>
<script src="${base}/myt/partner/distribute-delivery-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
