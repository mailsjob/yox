<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-partner-partner-inputBasic"
	action="${base}/myt/partner/partner!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="portlet box yellow">
				<div class="portlet-title">
					<div class="caption">关联品牌</div>
					<div class="tools">
						<a class="collapse" href="javascript:;"></a>
					</div>
				</div>
				<div class="portlet-body" style="padding: 0">
					<table class="grid-myt-partner-agentConfig-brand" data-grid="items" data-pk='<s:property value="id"/>'></table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="portlet box grey">
				<div class="portlet-title">
					<div class="caption">关联商品</div>
					<div class="tools">
						<a class="collapse" href="javascript:;"></a>
					</div>
				</div>
				<div class="portlet-body" style="padding: 0">
					<table class="grid-myt-partner-agentConfig-commodity" data-grid="items" data-pk='<s:property value="id"/>'></table>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<hr />
<div class="col-md-6">
	<div class="portlet box yellow">
		<div class="portlet-title">
			<div class="caption">所有已关联品牌</div>
			<div class="tools">
				<a class="collapse" href="javascript:;"></a>
			</div>
		</div>
		<div class="portlet-body" style="padding: 0">
			<table class="grid-myt-partner-agentConfig-brand-display" data-grid="items" data-pk='<s:property value="id"/>' data-readonly="true"></table>
		</div>
	</div>
</div>
<div class="col-md-6">
	<div class="portlet box grey">
		<div class="portlet-title">
			<div class="caption">所有已关联商品</div>
			<div class="tools">
				<a class="collapse" href="javascript:;"></a>
			</div>
		</div>
		<div class="portlet-body" style="padding: 0">
			<table class="grid-myt-partner-agentConfig-commodity-display" data-grid="items" data-pk='<s:property value="id"/>' data-readonly="true"></table>
		</div>
	</div>
</div>
<script src="${base}/myt/partner/partner-agentConfig.js" />
<%@ include file="/common/ajax-footer.jsp"%>