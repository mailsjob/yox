$(function() {
    $(".form-myt-partner-partner-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);

            var $abbr = $form.find('input[name="abbr"]');
            $abbr.change(function() {
                var val = $abbr.val();
                if (val != '') {
                    var $signinid = $form.find('input[name="signinid"]');
                    if ($signinid) {
                        $signinid.val(Pinyin.getCamelChars(val).toLowerCase() + "01");
                    }
                }
            });

            $form.find('input[name="parent.display"]').treeselect({
                url : WEB_ROOT + "/myt/partner/partner!treeDatas",
                callback : {
                    onSingleClick : function(event, treeId, treeNode) {
                        $form.setFormDatas({
                            'parent.id' : Util.startWith(treeNode.id, "-") ? "" : treeNode.id,
                            'parent.display' : Util.startWith(treeNode.id, "-") ? "" : treeNode.name
                        }, true);
                    },
                    onClear : function(event) {
                        $form.setFormDatas({
                            'parent.id' : '',
                            'parent.display' : ''
                        }, true);
                    }
                }
            });

            $form.find("input[name='region.display']").data('data-dropdownselect-callback', function(json) {
                $form.find("input[name='region.display']").val(json.display);
                $form.find("input[name='region.id']").val(json.id);
            });

            $form.find(".btn-map-locate").on("click", function(mapData) {
                var address = $form.find("input[name='region.display']").val();
                address += $form.find("input[name='address']").val();
                if (address != "") {
                    var $map = $form.find(".gmaps-baidu-partner-inputBasic");
                    $map.trigger("mapLocate", address);
                    $map.click();
                }
            });

            $form.find(".gmaps-baidu-partner-inputBasic").on("mapClick", function(event, mapData) {
                $form.find("input[name='address']").val(mapData.fullAddress);
                $form.find("input[name='latLon']").val(mapData.point.lat+","+mapData.point.lng);
            });
        }

    });
});