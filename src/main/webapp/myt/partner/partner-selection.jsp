<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row">
		<div class="col-md-12">
			<table class="grid-myt-partner-partner-selection" data-grid="table"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-partner-partner-selection").data("gridOptions", {
            url : "${base}/myt/partner/partner!findByPage",
            colModel : [{
                label : '流水号',
                name : 'id',
                width : 50,
                align : 'center'
            }, {
                label : '代码',
                name : 'code',
                width : 100,
                align : 'left'
            }, {
                label : '名称',
                name : 'abbr',
                width : 100,
                align : 'left'
            }, {
                label : '名称',
                name : 'display',
                width : 100,
                hidden:true,
                align : 'left'
            }, {
                label : '联系人',
                name : 'contactPerson',
                width : 100,
                align : 'left'
            },{
                label : '联系电话',
                name : 'contactPhone',
                width : 180
            },{
                label : '排序号',
                name : 'address',
                width : 180
            }],
           	rowNum : 15,
            multiselect : false,
            toppager : false,
            onSelectRow : function(id) {
                var $grid = $(this);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
