$(function() {

    $(".grid-myt-partner-agentConfig-brand").data("gridOptions", {
        url : function() {
            pk = $(this).attr("data-pk");
            return WEB_ROOT + "/myt/partner/partner!partnerR2Brands?id=" + pk;
        },
        batchEntitiesPrefix : "partnerR2Brands",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            name : 'partner.id',
            hidden : true,
            hidedlg : true,
            editable : true,
            formatter : function() {
                return $(this).attr("data-pk")
            }
        }, {
            label : '品牌',
            name : 'brand.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '品牌',
            name : 'brand.display',
            width : 200,
            width : 200,
            editable : true,
            align : 'left',
            editoptions : {
                placeholder : '双击选取品牌..',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var selectCommodity = function(item) {
                        var $curRow = $elem.closest("tr.jqgrow");
                        // 强制覆盖已有值
                        $grid.jqGrid("setEditingRowdata", {
                            'brand.id' : item['id'],
                            'brand.display' : item['display'],
                        }, true);
                    }
                    $elem.dblclick(function(event) {
                        var rowdata = $grid.jqGrid("getEditingRowdata");
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/brand!forward?_to_=selection',
                            title : '选取品牌',
                            callback : function(item) {
                                selectCommodity(item);
                            }
                        })
                        event.stopPropagation();
                    })
                }
            }
        } ]
    });

    $(".grid-myt-partner-agentConfig-commodity").data("gridOptions", {
        url : function() {
            var pk = $(this).attr("data-pk");
            return WEB_ROOT + "/myt/partner/partner!partnerR2Commodities?id=" + pk;
        },
        batchEntitiesPrefix : "partnerR2Commodities",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '商品',
            name : 'commodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            name : 'partner.id',
            hidden : true,
            hidedlg : true,
            editable : true,
            formatter : function() {
                return $(this).attr("data-pk")
            }
        }, {
            label : '商品',
            name : 'commodity.display',
            width : 200,
            width : 200,
            editable : true,
            editoptions : {
                placeholder : '双击选取商品...',
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    var selectCommodity = function(item) {
                        var $curRow = $elem.closest("tr.jqgrow");
                        // 强制覆盖已有值
                        $grid.jqGrid("setEditingRowdata", {
                            'commodity.id' : item['id'],
                            'commodity.display' : item['display']
                        }, true);
                    }
                    $elem.dblclick(function(event) {
                        var rowdata = $grid.jqGrid("getEditingRowdata");
                        $(this).popupDialog({
                            url : WEB_ROOT + '/myt/md/commodity!forward?_to_=selection',
                            title : '选取品牌',
                            callback : function(item) {
                                selectCommodity(item);
                            }
                        })
                        event.stopPropagation();
                    })
                }
            },
            align : 'left'
        } ]
    });
    
    // display
    $(".grid-myt-partner-agentConfig-brand-display").data("gridOptions", {
        url : function() {
            pk = $(this).attr("data-pk");
            return WEB_ROOT + "/myt/partner/partner!relationedBrands?id=" + pk;
        },
        colModel : [{
            label : '品牌',
            name : 'display',
            width : 200,
            width : 200,
            editable : true,
            align : 'left'
        } ]
    });

    $(".grid-myt-partner-agentConfig-commodity-display").data("gridOptions", {
        url : function() {
            var pk = $(this).attr("data-pk");
            return WEB_ROOT + "/myt/partner/partner!relationedCommodities?id=" + pk;
        },
        colModel : [ {
            label : '商品',
            name : 'display',
            align : 'left'
        } ]
    });
});