<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-partner-partner-inputBasic"
	action="${base}/myt/partner/partner!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-partner-partner-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">类型</label>
							<div class="controls">
								<s:select list="#application.enums.partnerTypeEnum" name="partnerType" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">上级节点</label>
							<div class="controls">
								<s:textfield name="parent.display" />
								<s:hidden name="parent.id" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">简称</label>
							<div class="controls">
								<s:textfield name="abbr" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">代码</label>
							<div class="controls">
								<s:textfield name="code" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">联系人姓名</label>
							<div class="controls">
								<s:textfield name="contactPerson" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">联系人性别</label>
							<div class="controls">
								<s:radio name="contactSex" list="#application.enums.genericSexEnum" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">电子邮件</label>
							<div class="controls">
								<s:textfield name="contactEmail" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">联系电话</label>
							<div class="controls">
								<s:textfield name="contactPhone" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">邮编</label>
							<div class="controls">
								<s:textfield name="postCode" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">行政区域</label>
							<div class="controls">
								<s:textfield name="region.display" data-toggle="dropdownselect" data-minWidth="800px"
									data-url="${base}/myt/md/region!forward?_to_=selection" data-selected="%{parent.id}" />
								<s:hidden name="region.id" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">详细地址</label>
							<div class="controls">
								<div class="input-group">
									<s:textfield name="address" />
									<span class="input-group-btn">
										<button type="button" class="btn blue btn-map-locate">定位</button>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">经纬度</label>
							<div class="controls">
								<s:textfield name="latLon" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="gmaps gmaps-baidu gmaps-baidu-partner-inputBasic" data-init-location="<s:property value='address'/>"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">全称</label>
					<div class="controls">
						<s:textfield name="companyName" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">关联订单来源集合</label>
					<div class="controls">
						<s:checkboxlist name="grantedOrderFroms" list="orderFroms" value="%{selectedGrantedOrderFroms}" />
					</div>
				</div>
			</div>
		</div>
		<s:if test="%{!notNew}">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">初始化登录帐号</label>
						<div class="controls ">
							<s:textfield name="signinid" requiredLabel="true" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">设置密码</label>
						<div class="controls">
							<s:password name="newpassword" requiredLabel="true" autocomplete="off" data-rule-minlength="3" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">确认密码</label>
						<div class="controls">
							<s:password name="cfmpassword" requiredLabel="true" autocomplete="off" data-rule-equalToByName="newpassword"
								data-rule-minlength="3" />
						</div>
					</div>
				</div>
			</div>

		</s:if>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-partner-partner-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/partner/partner-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>