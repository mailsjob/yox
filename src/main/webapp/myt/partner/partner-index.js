$(function() {
    $(".grid-myt-partner-partner-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/partner/partner!findByPage',
        colModel : [ {
            label : '编码',
            name : 'code',
            editable : true,
            width : 100,
            align : 'center'
        }, {
            label : '简称',
            name : 'abbr',
            editable : true,
            align : 'left'
        },{
            label : '类型',
            stype : 'select',
            name : 'partnerType',
            editable : true,
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('partnerTypeEnum')
            },
            width : 60
        }, {
            label : '地址',
            name : 'address',
            editable : true,
            align : 'left'
        }, {
            label : '授权订单类型',
            name : 'selectedGrantedOrderFroms',
            editable : false,
            align : 'left'
        } ],
        subGrid : true,
        gridDnD : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initRecursiveSubGrid(subgrid_id, row_id, "parent.id");
        },
        editcol : 'code',
        inlineNav : {
            add : false
        },
        editurl : WEB_ROOT + "/myt/partner/partner!doSave",
        delurl : WEB_ROOT + "/myt/partner/partner!doDelete",
        fullediturl : WEB_ROOT + "/myt/partner/partner!inputTabs"
    });
});
