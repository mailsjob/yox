<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">销售订单列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".myt-partner-view-box-order-index">
						<div class="form-group">
							<label class="sr-only">订单号、客户</label> <input type="text" name="search['CN_orderSeq_OR_customerProfile.nickName']"
								class="form-control input-xlarge" placeholder="订单号、客户..." />
						</div>
						<div class="form-group">
							<div class="btn-group">
								<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
									状态 <i class="fa fa-angle-down"></i>
								</button>
								<div class="dropdown-menu hold-on-click dropdown-checkboxes">
									<s:checkboxlist name="search['IN_orderStatus']" list="#application.enums.boxOrderStatusEnum"
										value="#application.enums.boxOrderStatusEnum.keys.{? #this.name()!='S90CANCLE'}" />
								</div>
							</div>
						</div>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="myt-partner-view-box-order-index"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".myt-partner-view-box-order-index").data("gridOptions", {
            url : '${base}/myt/partner/view-box-order!findByPage',
            colNames : [ '流水号', '状态', '付款模式', '订单号', '订单名称', '下单客户', '收货人', '订单总金额', '已付总金额' ],
            colModel : [ {
                name : 'id'
            }, {
                name : 'orderStatus',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('boxOrderStatusEnum')
                }

            }, {
                name : 'splitPayMode',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('boxOrderSplitPayModeEnum')
                }

            }, {
                name : 'orderSeq',
                width : 180,
                align : 'left'
            }, {
                name : 'title',
                align : 'left'
            }, {
                name : 'customerProfile.display',
                index : 'customerProfile.nickName',
                width : 100,
                align : 'left',
                formatter : function(cellValue, options, rowdata, action) {
                    var url = "${base}/myt/customer/customer-profile!view?id=" + rowdata.customerProfile.id;
                    return '<a href="'+url+'" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
                }
            }, {
                name : 'receivePerson',
                width : 80,
                align : 'left'
            }, {
                name : 'actualAmount',
                formatter : 'currency'
            }, {
                name : 'actualPayedAmount',
                formatter : 'currency'
            } ],
            postData : {
                "search['FETCH_customerProfile']" : "INNER",
                "search['FETCH_intermediaryCustomerProfile']" : "LEFT"
            },
            footerrow : true,
            footerLocalDataColumn : [ 'actualAmount', 'actualPayedAmount' ],
            multiselect : false,
            contextMenu : false,
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : '${base}/myt/partner/view-box-order!getBoxOrderDetails?id=' + row_id,
                    colNames : [ '预约发货日期', '行项状态' ],
                    colModel : [ {
                        name : 'reserveDeliveryTime',
                        sorttype : 'date',
                        width : 200
                    }, {
                        name : 'orderDetailStatus',
                        align : 'center',
                        stype : 'select',
                        searchoptions : {
                            value : Util.getCacheEnumsByType('boxOrderDetailStatusEnum')
                        }

                    } ],
                    cmTemplate : {
                        sortable : false
                    },
                    rowNum : -1,
                    multiselect : false,
                    filterToolbar : false,
                    contextMenu : false,
                    pager : false,
                    subGrid : true,
                    subGridRowExpanded : function(subgrid_id, row_id) {
                        Grid.initSubGrid(subgrid_id, row_id, {
                            url : "${base}/myt/partner/view-box-order!getBoxOrderDetailCommodities?detailSid=" + row_id,
                            colNames : [ '商品编码', '商品名称', '价格', '数量' ],
                            colModel : [ {
                                name : 'commodity.sku',
                                align : 'center',
                                width : 100
                            }, {
                                name : 'commodity.title'
                            }, {
                                name : 'finalPrice',
                                align : 'right',
                                formatter : 'currency',
                                width : 100
                            }, {
                                name : 'quantity',
                                sorttype : 'number',
                                align : 'right',
                                width : 100
                            } ],
                            cmTemplate : {
                                sortable : false
                            },
                            rowNum : -1,
                            pager : false,
                            filterToolbar : false,
                            contextMenu : false,
                            multiselect : false
                        });
                    }
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
