<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">列表查询</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-myt-distribute-delivery" action="#">
						<div class="form-group">
							<input type="text" name="search['CN_voucher_OR_receivePerson']" class="form-control input-large"
								placeholder="配货单号、收货人...">
						</div>
						<div class="form-group">
							<input type="text" name="search['BT_createdDate']" class="form-control input-medium input-daterangepicker"
								placeholder="创建日期">	
						</div>
						<div class="form-group">
							<label> <s:checkbox name="search['EQ_voucherUser.signinid']" fieldValue="%{signinUsername}" />只显示我的
							</label>
						</div>
						<div class="form-group">
							<div class="btn-group">
								<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
									配货状态 <i class="fa fa-angle-down"></i>
								</button>
								<div class="dropdown-menu hold-on-click dropdown-checkboxes">
									<s:checkboxlist name="search['IN_distributeDeliveryStatus']" list="#application.enums.distributeDeliveryStatusEnum"
										value="#application.enums.distributeDeliveryStatusEnum.keys.{? #this.name()!='S40DELIVERIED' and #this.name()!='S50RECEIVED'}" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="btn-group">
								<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
									凭证状态 <i class="fa fa-angle-down"></i>
								</button>
								<div class="dropdown-menu hold-on-click dropdown-checkboxes">
									<s:checkboxlist name="search['IN_voucherState']" list="#application.enums.voucherStateEnum"
										value="#application.enums.voucherStateEnum.keys.{? #this.name()!='REDW'}" />
								</div>
							</div>
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-distribute-delivery"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/partner/distribute-delivery-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
