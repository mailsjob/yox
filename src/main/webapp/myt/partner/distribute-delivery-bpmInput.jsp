<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-distribute-delivery-bpmInput"
	action="${base}/myt/partner/distribute-delivery!bpmSave?prepare=true" method="post">
	<s:hidden name="taskId" value="%{#parameters.taskId}" />
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<s:hidden name="boxOrderDetail.id" />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
				<i class="fa fa-check"></i> 保存
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
		</div>
		<div class="pull-right">
			<div class="form-group input-medium">
			</div>
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">凭证状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.voucherStateEnum[voucherState]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 ">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:textfield name="voucher" readonly="%{notNew}" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<s3:datetextfield name="voucherDate" format="date" current="true" />
							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:select name="voucherUser.id" list="usersMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:select name="voucherDepartment.id" list="departmentsMap" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">代理分销商</label>
					<div class="controls">
						<s:textfield name="agentPartner.display" data-toggle="treeselect"
							data-url="%{#attr.base+'/myt/partner/partner!treeDatas?partnerType=AGENT'}" />
						<s:hidden name="agentPartner.id" />
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">发货类型</label>
					<div class="controls">
						<s:radio name="deliveryType" list="#application.enums.deliveryTypeEnum"
							data-profile-param="default_sale_delivery_type" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">销售单名称</label>
					<div class="controls">
						<s:textfield name="title"  placeholder="留空会自动基于商品信息生成标题摘要" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人</label>
					<div class="controls">
						<s:textfield name="receivePerson" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">电话</label>
					<div class="controls">
						<s:textfield name="mobilePhone" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">邮编</label>
					<div class="controls">
						<s:textfield name="postCode" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收货地址</label>
					<div class="controls">
						<div class="input-group">
							<s:textfield name="deliveryAddr" />
							<div class="input-group-btn">
								<button tabindex="-1" class="btn default btn_delivery_print" type="button">打印发货清单</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">代发货库房</label>
					<div class="controls">
						<s:textfield name="stockPartner.display" data-toggle="treeselect"
							data-url="%{#attr.base+'/myt/partner/partner!treeDatas?partnerType=STOCK'}" />
						<s:hidden name="stockPartner.id" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">备注说明</label>
					<div class="controls">
						<s:textfield name="memo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-distribute-delivery-bpmInput" data-grid="items"
					data-readonly="<s:property value="%{voucherState.name()=='DRAFT'?false:true}"/>" data-agentLevel=""
					data-pk='<s:property value="#parameters.id"/>' data-clone='<s:property value="#parameters.clone"/>'></table>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">整单折扣额</label>
							<div class="controls">
								<div class="input-group">
									<s:textfield name="discountAmount" readonly="%{'DRAFT'!=voucherState.name()}" />
									<div class="input-group-btn">
										<button tabindex="-1" class="btn default btn-discount-by-amount" type="button">按金额自动分摊</button>
										<button tabindex="-1" data-toggle="dropdown" class="btn default dropdown-toggle" type="button">
											<i class="fa fa-angle-down"></i>
										</button>
										<ul role="menu" class="dropdown-menu pull-right">
											<li><a class="btn-discount-by-quantity">按数量自动分摊</a></li>
											<li><a>平均分摊</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">总计原始金额</label>
							<div class="controls">
								<s:textfield name="totalOriginalAmount" readonly="true" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">折后金额</label>
							<div class="controls">
								<s:textfield name="commodityAmount" readonly="true" />
							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">交税总金额</label>
							<div class="controls">
								<s:textfield name="totalTaxAmount" readonly="true" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">含税总金额</label>
							<div class="controls">
								<s:textfield name="commodityAndTaxAmount" readonly="true" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">收取运费</label>
							<div class="controls">
								<s:textfield name="chargeLogisticsAmount" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">应收总金额</label>
							<div class="controls">
								<s:textfield name="totalAmount" readonly="true" />
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">已收总金额</label>
							<div class="controls">
								<s:textfield name="payedAmount" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">收款账户</label>
							<div class="controls">
								<s:select name="accountSubject.id" list="paymentAccountSubjects"
									data-profile-param="default_sale_delivery_account_subject_id" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">收款备注</label>
							<div class="controls">
								<s:textarea name="paymentReference" rows="3" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">提交审批</label>
					<div class="controls text-warning">
						<s:checkbox name="submitToAudit" disabled="%{submitDate!=null}" label="勾选此项将会立即提交审批流程，在此期间不可修改数据！" value="true" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-actions right">
		<div class="clearfix pull-left">
		</div>
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/partner/distribute-delivery-bpmInput.js" />
<%@ include file="/common/ajax-footer.jsp"%>
