<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="form-horizontal form-bordered form-label-stripped">
	<div class="row">
		<s:if test="#attr.taskVariablesVar['auditLevel1Time']!=null">
			<div class="col-md-6">
				<div class="portlet gren">
					<div class="portlet-title">
						<div class="caption">一线审核</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<label class="control-label">通过:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.booleanLabel[#attr.taskVariablesVar['auditLevel1Pass']]" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审核人员:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#attr.taskVariablesVar['auditLevel1User']" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审核时间:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:date name="#attr.taskVariablesVar['auditLevel1Time']" format="yyyy-MM-dd HH:mm:ss" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审核意见:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#attr.taskVariablesVar['auditLevel1Explain']" />
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</s:if>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">经办人</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="voucherUser.display" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">部门</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="voucherDepartment.display" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">销售单号</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="voucher" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">代理商</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="agentPartner.display" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">标题</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="title" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">备注说明</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="memo" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-myt-sale-sale-delivery-viewBasic" data-grid="table" data-pk="<s:property value='id'/>" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="note note-success">
				<h4 class="block">
					订单总金额：
					<s:property value="totalAmount" />
					(商品总金额：
					<s:property value="commodityAmount" />
					+收取运费：
					<s:property value="chargeLogisticsAmount" />
					) ，已预付金额：
					<s:property value="payedAmount" />
				</h4>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/partner/distribute-delivery-viewBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
