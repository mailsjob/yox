$(function() {
    $(".grid-myt-distribute-delivery").data("gridOptions", {
        url : WEB_ROOT + '/myt/partner/distribute-delivery!findByPage',
        colModel : [ {
            label : '状态',
            name : 'distributeDeliveryStatus',
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('distributeDeliveryStatusEnum')
            },
            width : 80
        }, {
            label : '凭证号',
            name : 'voucher',
            align : 'center',
            width : 120
        }, {
            label : '经办人',
            name : 'voucherUser.display',
            index : 'voucherUser.signinid',
            align : 'center',
            width : 60
        }, {
            label : '发货类型',
            stype : 'select',
            name : 'deliveryType',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('deliveryTypeEnum')
            },
            width : 60
        }, {
            label : '提交时间',
            name : 'submitDate',
            stype : 'date',
            hidden : true,
            width : 100
        }, {
            label : '凭证日期',
            name : 'voucherDate',
            hidden : true,
            stype : 'date',
            width : 100
        }, {
            label : '整单金额',
            name : 'totalAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '已付金额',
            name : 'payedAmount',
            formatter : 'currency',
            hidden : true,
            width : 100
        }, {
            label : '经办部门',
            name : 'voucherDepartment.display',
            index : 'voucherDepartment.code_OR_voucherDepartment.title',
            hidden : true,
            width : 100
        }, {
            label : '标题',
            name : 'title',
            hidden : true,
            width : 100
        }, {
            label : '收货人',
            name : 'receivePerson',
            width : 100
        }, {
            label : '物流公司',
            name : 'logistics.display',
            index : 'logistics.abbr',
            width : 100
        }, {
            label : '快递单号',
            name : 'logisticsNo',
            width : 100
        }, {
            label : '电话',
            name : 'mobilePhone',
            width : 100
        }, {
            label : '收货地址',
            name : 'deliveryAddr',
            width : 250
        }, {
            label : '实际运费',
            name : 'logisticsAmount',
            hidden : true,
            width : 100
        } ],
        editcol : 'voucher',
        fullediturl : WEB_ROOT + "/myt/partner/distribute-delivery!inputTabs",
        addable : false,
        operations : function(itemArray) {

            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 发货清单</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                var url = WEB_ROOT + "/rpt/jasper-report!preview?format=XLS&report=DISTRIBUTE_DELIVERY_COMMODITY_LIST&contentDisposition=attachment";
                var rowDatas = $grid.jqGrid("getSelectedRowdatas");
                for (i = 0; i < rowDatas.length; i++) {
                    var rowData = rowDatas[i];
                    url += "&reportParameters['DISTRIBUTE_DELIVERY_IDS']=" + rowData['voucher'];
                    alert(rowData['voucher']);
                }
                window.open(url, "_blank");
            });
            itemArray.push($select);
        },
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/partner/distribute-delivery!details?id=" + row_id,
                colModel : [ {
                    label : '行项号',
                    name : 'subVoucher',
                    align : 'center',
                    width : 50
                }, {
                    label : '商品',
                    name : 'commodity.display',
                    align : 'left'
                }, {
                    label : '单位',
                    name : 'measureUnit',
                    editable : true,
                    width : 60
                }, {
                    label : '数量',
                    name : 'quantity',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '销售单价',
                    name : 'price',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '是否赠品',
                    name : 'gift',
                    width : 50,
                    edittype : 'checkbox'

                }, {
                    label : '原价金额',
                    name : 'originalAmount',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '折扣率(%)',
                    name : 'discountRate',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '折扣额',
                    name : 'discountAmount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '折后金额',
                    name : 'amount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '税率(%)',
                    name : 'taxRate',
                    width : 50,
                    hidden : true,
                    formatter : 'number'

                }, {
                    label : '税额',
                    name : 'taxAmount',
                    width : 60,
                    hidden : true,
                    formatter : 'currency'
                }, {
                    label : '含税总金额',
                    name : 'commodityAndTaxAmount',
                    width : 80,
                    formatter : 'currency'
                }, {
                    label : '发货仓库',
                    name : 'storageLocation.id',
                    width : 80,
                    stype : 'select',
                    formatter : 'select',
                    searchoptions : {
                        value : Biz.getStockDatas()
                    }
                }, {
                    label : '批次号',
                    name : 'batchNo',
                    width : 80
                }, {
                    label : '批次过期日期',
                    name : 'expireDate',
                    width : 80
                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
});
