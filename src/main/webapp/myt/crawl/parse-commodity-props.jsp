<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div id="tab-parse-commodity-props" class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-crawl-parse-commodity-props" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-crawl-parse-commodity-props").data("gridOptions", {
            url : "${base}/myt/crawl/parse-commodity!getParsePropTracks?id=<s:property value='#parameters.id'/>",
          
            colModel : [ {
                label:'prop_group',
                name : 'propGroup',
              	width : 150
            } ,{
                label:'prop_key',
                name : 'propKey',
              	width : 200
            }, {
                label:'prop_sub_key',
                name : 'propSubKey',
              	width : 100
            }, {
                label:'prop_label',
                align:'center',
                name : 'propLabel',
              	width : 100
            }, {
                label:'prop_value',
                name : 'propValue',
              	width : 200
            }, {
                label:'fetch_time',
                name : 'fetchTimeLabel',
              	width : 100
            }],
            multiselect : false
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>