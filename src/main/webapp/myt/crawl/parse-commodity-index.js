$(function() {
    $(".grid-myt-crawl-parse-commodity").data("gridOptions", {
        url : WEB_ROOT + "/myt/crawl/parse-commodity!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '来源',
            name : 'sourceType',
            align : 'left',
            width : 50
        }, {
            label : '来源链接',
            name : 'baseUrl',
            align : 'left',
            width : 400
        }, {
            label : '窗图数量',
            name : 'winImgNum',
            align : 'center',
            width : 50
        }, {
            label : '备注',
            name : 'sysMemo',
            width : 50,
            align : 'center',
            formatter : function(cellValue, options, rowdata, action) {
                if (cellValue) {
                    return '<span class="badge">有</span>';
                }
                return '<span class="badge badge-success">无</span>';

            }
        }, {
            label : '名称',
            name : 'title',
            width : 250,
            align : 'left'

        }, {
            label : '基准价',
            name : 'salePrice',
            formatter : 'currency',
            width : 80
        }, {
            label : '导出价',
            name : 'targetSalePrice',
            formatter : 'currency',
            editable : true,
            width : 80,
            editoptions : {
                placeholder : '留空则取基准价'
            }
        }, {
            label : '计算品牌',
            name : 'targetBrand.id',
            width : 100,
            align : 'left',
            formatter : "select",
            editable : true,
            edittype : 'select',
            editoptions : {
                value : Biz.getBrandDatas()
            },
            responsive : 'sm'
        }, {
            label : '计算分类主键',
            name : 'targetCategory.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '计算分类',
            name : 'targetCategory.display',
            index : 'targetCategory.categoryCode_OR_targetCategory.categoryCode',
            width : 120,
            sortable : false,
            editable : true,
            align : 'left',
            editoptions : {
                dataInit : function(elem, opt) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.treeselect({
                        url : WEB_ROOT + "/myt/md/category!categoryList",
                        callback : {
                            onSingleClick : function(event, treeId, treeNode) {
                                $grid.jqGrid("setEditingRowdata", {
                                    'targetCategory.id' : Util.startWith(treeNode.id, "-") ? "" : treeNode.id,
                                    'targetCategory.display' : Util.startWith(treeNode.id, "-") ? "" : treeNode.name
                                });
                            },
                            onClear : function(event) {
                                $grid.jqGrid("setEditingRowdata", {
                                    'targetCategory.id' : '',
                                    'targetCategory.display' : ''
                                });
                            }
                        }
                    });
                }
            },
            responsive : 'sm'
        }, {
            label : '计算查重分值',
            name : 'matchMaxScore',
            align : 'center',
            width : 50
        }, {
            label : '来源分类',
            name : 'categoryPath',
            width : 200
        }, {
            label : '平均评价',
            name : 'averageReview',
            width : 50,
            align : 'center'

        }, {
            label : '库存状态',
            name : 'stockState',
            width : 50

        }, {
            label : '最近抓取时间',
            name : 'lastFetchTimeLabel',
            width : 120
        }, {
            label : '最近导入时间',
            name : 'lastImportTime',
            width : 100

        } ],
        postData : {
            "search['FETCH_targetBrand']" : "LEFT",
            "search['FETCH_targetCategory']" : "LEFT"
        },
        addable : false,
        editcol : 'baseUrl',
        editurl : WEB_ROOT + "/myt/crawl/parse-commodity!doSave",
        delurl : WEB_ROOT + "/myt/crawl/parse-commodity!doDelete",
        fullediturl : WEB_ROOT + "/myt/crawl/parse-commodity!inputTabs",
        operations : function(itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>爬虫抓取</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var url = WEB_ROOT + "/myt/md/commodity!forward?_to_=crawl";
                var ah = $grid.closest(".tabbable").find(" > .nav");
                Global.addOrActiveTab(ah, {
                    title : "爬虫抓取",
                    url : url
                })

            });
            itemArray.push($select);
            var $select2 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>批量导入</a></li>');
            $select2.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/crawl/parse-commodity!doImport';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  导入到商品？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select2);
            var $select3 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>智能计算品牌和分类</a></li>');
            $select3.children("a").bind("click", function(e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/myt/crawl/parse-commodity!doAIInit';
                    $grid.ajaxPostURL({
                        url : url,
                        success : function() {
                            $grid.refresh();
                        },
                        confirmMsg : "确认  导入到商品？",
                        data : {
                            ids : ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select3);
        },
    });

});