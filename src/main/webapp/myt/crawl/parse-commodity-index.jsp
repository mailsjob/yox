<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">爬虫列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search"
						data-grid-search=".grid-myt-crawl-parse-commodity" action="#">
						<div class="form-group">
								<input type="text" name="search['CN_sourceType_OR_baseUrl_OR_title']" class="form-control input-xlarge"
								 placeholder="来源、链接、名称">
							</div>
						
							<div class="form-group">
								<div class="btn-group">
									<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
										问题备注 <i class="fa fa-angle-down"></i>
									</button>
									<div class="dropdown-menu hold-on-click dropdown-checkboxes">
										<s:radio name="search['NU_sysMemo']" list="#{'true':'无','false':'有'}" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<span class="input-group-btn">
									<button class="btn green" type="submmit">
										<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
									</button>
									<button class="btn default" type="reset">
										<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
									</button>
								</span>
							</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-crawl-parse-commodity" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/crawl/parse-commodity-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>