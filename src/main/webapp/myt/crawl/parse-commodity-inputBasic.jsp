<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-edit-parse-commodity"
	action="${base}/myt/crawl/parse-commodity!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-crawl-parse-commodity">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">导入商品名称</label>
					<div class="controls">
						<s:textfield name="targetTitle" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">计划基准价格：</label>
					<div class="controls">
						<s:textfield name="targetSalePrice" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">导入商品分类</label>
					<div class="controls">
						<s:textfield name="targetCategory.display" />
						<s:hidden name="targetCategory.id" />
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">分类匹配说明</label>
					<div class="controls">
					<p class="form-control-static">
						<s:property value='targetCategoryExplain' />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">导入商品品牌</label>
					<div class="controls">
						<s:select name="targetBrand.id" list="brandsMap" />
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">品牌匹配分类</label>
					<div class="controls">
					<p class="form-control-static">
						<s:property value='targetBrandExplain' />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="form-actions right">
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
		</div>
		<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<span style="color: #FF83FA">爬虫商品信息</span>
					</div>
					<div class="tools">
						<a class="collapse" href="javascript:;"></a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">来源链接:</label>
								<div class="controls">
								<p class="form-control-static">
									<a href='<s:property value="baseUrl" />' target="_blank"><s:property
									value="baseUrl" /></a>
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">备注说明</label>
								<div class="controls">
								<p class="form-control-static">
									<s:property value="sysMemo" />
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">名称</label>
								<div class="controls">
								<p class="form-control-static">
									<s:property value="title" />
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">分类</label>
								<div class="controls">
								<p class="form-control-static">
									<s:property value="categoryPath" />
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">售价</label>
								<div class="controls">
								<p class="form-control-static">
									<s:property value="salePrice" />
								</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">市场价/参考价</label>
								<div class="controls">
								<p class="form-control-static">
									<s:property value="marketPrice" />
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">平均评价</label>
								<div class="controls">
								<p class="form-control-static">
									<s:property value="averageReview" />
								</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">销售库存状态</label>
								<div class="controls">
								<p class="form-control-static">
									<s:property value="stockState" />
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">促销信息</label>
								<div class="controls">
								<p class="form-control-static">
									<s:property value="salePrompt" />
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">智能匹配信息</label>
								<div class="controls">
								<p class="form-control-static">
									<s:property value="matchDocs" />
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">创建日期</label>
								<div class="controls">
								<p class="form-control-static">
									<s:date name="firstFetchTimeLabel"/>
								</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">最后更新日期</label>
								<div class="controls">
								<p class="form-control-static">
									<s:date name="lastFetchTimeLabel"/>
								</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">商品描述：<br /><font color="red">只做显示修改无效</font></label>
								<div class="controls">
								<s:textarea name="description" data-htmleditor="kindeditor" data-height="800px" readonly="true" />
								</div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
	</div>
	
</form>
<%@ include file="/common/ajax-footer.jsp"%>
<script type="text/javascript">
 $(function() {
        $(".form-edit-parse-commodity").data("formOptions", {
            bindEvents : function() {
                var $form = $(this);
                $form.find('input[name="targetCategory.display"]').treeselect({
                    url : WEB_ROOT + "/myt/md/category!categoryList",
                    callback : {
                        onSingleClick : function(event, treeId, treeNode) {
                            $form.setFormDatas({
                                'targetCategory.id' : Util.startWith(treeNode.id, "-") ? "" : treeNode.id,
                                'targetCategory.display' : Util.startWith(treeNode.id, "-") ? "" : treeNode.name
                            }, true);
                        },
                        onClear : function(event) {
                            $form.setFormDatas({
                                'targetCategory.id' : '',
                                'targetCategory.display' : ''
                            }, true);
                        }
                    }
                });
           }
        })
 })
</script>