$(function () {
    $(".grid-order-cost-count-index").data("gridOptions", {
        url: WEB_ROOT + '/myt/sale/order-cost-count!count',
        colModel: [{
            label: '流水号',
            name: 'id',
            hidden: true
        }, {
            label: '日期',
            name: 'orderTime',
            formatter: 'date',
            align: 'left'
        }, {
            label: '订单数',
            name: 'total',
            align: 'right'
        }, {
            label: '订单商品金额',
            name: 'commodityPrice',
            align: 'right'
        }, {
            label: '邮费',
            name: 'postage',
            align: 'right'
        }, {
            label: '支付佣金金额',
            name: 'payAcc',
            align: 'right'
        }, {
            label: '货品成本',
            name: 'commodityCost',
            align: 'right'
        }, {
            label: '退货退款',
            name: 'backFee',
            align: 'right'
        }],
        footerrow: true,
        footerLocalDataColumn: ['total', 'commodityPrice', 'postage', 'payAcc', 'commodityCost', 'backFee'],
    });
});
