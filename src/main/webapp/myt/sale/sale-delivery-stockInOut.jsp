<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-sale-sale-delivery-stockInOut" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-sale-sale-delivery-stockInOut").data(
                "gridOptions",
                {
                    url : "${base}/myt/sale/sale-delivery!stockInOuts?id=<s:property value='#parameters.id'/>",
                    colModel : [
                            {
                                label : '时间',
                                name : 'createdDate',
                                width : 140,
                                align : 'center'
                            },
                            {
                                label : '商品',
                                name : 'commodityStock.commodity.display',
                                index : 'commodity.sku_OR_commodity.title',
                                width : 250
                            },
                            {
                                label : '库存地',
                                name : 'commodityStock.storageLocation.id',
                                width : 140,
                                editrules : {
                                    required : true
                                },
                                stype : 'select',
                                searchoptions : {
                                    value : Biz.getStockDatas()
                                },
                                align : 'left'
                            },
                            {
                                label : '实物量',
                                name : 'quantity',
                                width : 60,
                                sorttype : 'currency',
                                formatter : function(cellValue, options, rowdata, action) {
                                    if (rowdata.diffQuantity < 0) {
                                        return rowdata.quantity + "(" + rowdata.originalQuantity + "-" + (-rowdata.diffQuantity)
                                                + ")";
                                    } else {
                                        return rowdata.quantity + "(" + rowdata.originalQuantity + "+" + (rowdata.diffQuantity)
                                                + ")";
                                    }
                                },
                                align : 'right'
                            },
                            {
                                label : '锁定量',
                                name : 'originalSalingQuantity',
                                width : 60,
                                sorttype : 'currency',
                                formatter : function(cellValue, options, rowdata, action) {
                                    if (rowdata.diffSalingQuantity < 0) {
                                        return rowdata.salingQuantity + "(" + rowdata.originalSalingQuantity + "-"
                                                + (-rowdata.diffSalingQuantity) + ")";
                                    } else {
                                        return rowdata.salingQuantity + "(" + rowdata.originalSalingQuantity + "+"
                                                + (rowdata.diffSalingQuantity) + ")";
                                    }
                                },
                                align : 'right'
                            },
                            {
                                label : '在途量',
                                name : 'originalPurchasingQuantity',
                                width : 60,
                                sorttype : 'currency',
                                formatter : function(cellValue, options, rowdata, action) {
                                    if (rowdata.diffPurchasingQuantity < 0) {
                                        return rowdata.purchasingQuantity + "(" + rowdata.originalPurchasingQuantity + "-"
                                                + (-rowdata.diffPurchasingQuantity) + ")";
                                    } else {
                                        return rowdata.purchasingQuantity + "(" + rowdata.originalPurchasingQuantity + "+"
                                                + (rowdata.diffPurchasingQuantity) + ")";
                                    }
                                },
                                align : 'right'
                            }, {
                                label : '凭证号',
                                name : 'voucher',
                                width : 140,
                                align : 'center'
                            }, {
                                label : '类型',
                                name : 'voucherType',
                                width : 80,
                                align : 'center',
                                stype : 'select',
                                searchoptions : {
                                    value : Util.getCacheEnumsByType('voucherTypeEnum')
                                }
                            }, {
                                label : '操作人',
                                name : 'createdBy',
                                align : 'center',
                                width : 50
                            }, {
                                label : '操作摘要',
                                name : 'operationSummary',
                                width : 150
                            } ],
                   
                    sortorder : "desc",
                    sortname : "createdDate",
                    multiselect : false,
                    postData : {
                        "search['FETCH_commodityStock.commodity']" : "INNER.INNER",
                        "search['FETCH_commodityStock.storageLocation']" : "INNER.INNER"
                    }
                });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>