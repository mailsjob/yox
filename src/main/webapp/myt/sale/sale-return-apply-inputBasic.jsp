<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-sale-return-apply-inputBasic"
	action="${base}/myt/sale/sale-return-apply!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
				data-grid-reload=".grid-myt-sale-sale-return">
				<i class="fa fa-check"></i>
				<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
			</s3:button>
			<button class="btn default btn-cancel" type="button">取消</button>
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">订单号</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="boxOrderDetailCommodity.boxOrder.orderSeq" />
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">订单行项号</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="boxOrderDetailCommodity.boxOrderDetail.sn" />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">商品</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="boxOrderDetailCommodity.commodity.display" />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">客户</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="customerProfile.display" />
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">单据类型</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="#application.enums.processTypeEnum[processType]" />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">问题描述</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="description" />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">图片</label>
					<div class="controls" >
						<div style="float: left; margin-right: 5px;"><img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic1' />"
									 /></div>
						<div style="float: left; margin-right: 5px;"><img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic2' />"
									 /></div>
						<div style="float: left; margin-right: 5px;"><img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic3' />"/></div>
						<div style="float: left; margin-right: 5px;"><img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic4' />"/></div>
						<div style="float: left; margin-right: 5px;"><img width="150px" height="150px" src="<s:property value='imgUrlPrefix'/><s:property value='pic5' />"/></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">有无发票</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="#application.enums.booleanLabel[hasInvoice]" />
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">有无检测报告</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="#application.enums.booleanLabel[hasReport]" />
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">申请数量</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="quantity" />
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人名</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="saleReturnName" />
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人电话</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="saleReturnPhone" />
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人所在地</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="saleReturnCity.name" />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收货详细地址</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="saleReturnDetailAddr" />
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">审核通过</label>
					<div class="controls">
						<s:radio name="auditPass" list="#application.enums.booleanLabel" value="true" />
						
					</div>
				</div>
			</div>       
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">处理结果类型</label>
					<div class="controls">
						<s:radio name="saleReturnType" list="#application.enums.processTypeEnum"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">处理结果说明</label>
					<div class="controls">
						<s:textarea name="processComment" rows="3"></s:textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">代理分销商</label>
					<div class="controls">
						<s:textfield name="agentPartner.display" />
							<s:hidden name="agentPartner.id" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">退货商家联系人</label>
					<div class="controls">
						<s:textfield name="sellerName" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">退货商家电话</label>
					<div class="controls">
						<s:textfield name="sellerPhone" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">退货商家邮编</label>
					<div class="controls">
						<s:textfield name="sellerPostCode" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">退货商家地址</label>
					<div class="controls">
						<s:textfield name="sellerDetailAddr" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">客户寄回的快递公司</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="logistics.display" />
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">客户寄回的快递单号</label>
					<div class="controls">
						<p class="form-control-static">
							<s:property value="logisticsNo" />
						</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<div class="form-actions right">
		<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
			data-grid-reload=".grid-myt-sale-sale-delivery">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/sale/sale-return-apply-inputBasic.js"/>
<%@ include file="/common/ajax-footer.jsp"%>
