<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<div class="tabbable tabbable-primary">
    <ul class="nav nav-pills">
        <li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">订单统计</a></li>
        <li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in">
            <div class="row search-form-default">
                <div class="col-md-12">
                    <form action="#" method="get" class="form-inline form-validation form-search-init"
                          data-grid-search=".grid-order-cost-count-index">
                        <div class="form-group">
                            <input type="text" name="orderTime"
                                   class="form-control input-medium input-daterangepicker grid-param-data"
                                   placeholder="日期区间" pattern="">
                            <span class="input-group-addon form-control input-small">供应商</span>
                            <select name="provider" class="form-control input-small" >
                                <option value="">不限</option>
                                <option value="0000000000">总部</option>
                                <option value="HANZ">韩总供应商</option>
                            </select>
                            <button class="btn green" type="submmit">
                                <i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
                            </button>
                            <button class="btn default hidden-inline-xs" type="reset">
                                <i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="grid-order-cost-count-index"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${base}/myt/sale/order-cost-count-index.js"/>
<%@ include file="/common/ajax-footer.jsp" %>
