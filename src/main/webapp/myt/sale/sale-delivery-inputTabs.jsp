<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-custom tabbable-secondary">
	<ul class="nav nav-tabs">
		<li class="tools pull-right"><a href="javascript:;" class="btn default reload"><i class="fa fa-refresh"></i></a></li>
		<li class="active"><a data-toggle="tab"
			href="${base}/myt/sale/sale-delivery!edit?id=<s:property value='#parameters.id'/>&clone=<s:property value='#parameters.clone'/>">基本信息</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/sale/sale-delivery!forward?_to_=stockInOut&id=<s:property value='#parameters.id'/>">库存记录</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/sale/sale-delivery!forward?_to_=accountInOut&id=<s:property value='#parameters.id'/>">财会记录</a></li>
		<li><a data-toggle="tab" data-tab-disabled="<s:property value='%{!persistentedModel}'/>"
			href="${base}/myt/sale/sale-delivery!forward?_to_=saleDeliveryPicture&id=<s:property value='#parameters.id'/>">发货图片管理</a></li>
		<li><a data-toggle="tab"
			href="${base}/myt/sale/sale-delivery!revisionIndex?id=<s:property value='#parameters.id'/>">变更记录</a></li>

	</ul>
</div>
<%@ include file="/common/ajax-footer.jsp"%>
