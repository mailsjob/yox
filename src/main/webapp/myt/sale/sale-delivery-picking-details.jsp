<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<s:if test="%{model==null}">
	<div class="alert alert-warning">
		<strong>提示：</strong> 输入销售单号未找到匹配数据，请检查输入！
	</div>
</s:if>
 <s:elseif test="%{pickTime!=null}">
	<div class="alert alert-warning">
		<strong>提示：</strong> 输入的销售单<font color="red">已经拣货完毕</font>！
	</div>
</s:elseif>
<s:elseif test="%{voucherState.name()=='REDW'}">
	<div class="alert alert-warning">
		<strong>提示：</strong> 此销售单<font color="red">已冲销</font>
	</div>
</s:elseif>
<s:elseif test="%{auditDate==null}">
	<div class="alert alert-warning">
		<strong>提示：</strong> 此销售单<font color="red">请先审核，再拣货</font>
	</div>
</s:elseif>  
<s:else>
	<form class="form-horizontal form-bordered form-label-stripped form-validation form-sale-delivery-picking-details"
		action="${base}/myt/sale/sale-delivery!pickSave" method="post">
		<s:hidden name="id" />
		<s:hidden name="version" />
		<s:token />
		<div class="form-actions">
			<button class="btn blue" type="submit" data-ajaxify-reload="_closest-ajax-container">
				<i class="fa fa-check"></i> 拣货
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<div class="clearfix pull-right" style="margin-left: 5px">
				<div class="input-group">
					<span class="input-group-addon">商品条码 <i class="fa fa-barcode"></i></span><input type="text" name="barcode"
						placeholder="条码枪扫描输入..." class="form-control">
				</div>
			</div>
		</div>
		<div class="form-body control-label-sm">
			<div class="portlet">
				<div class="portlet-title">
					<div class="tools">
						<a class="expand" href="javascript:;"></a>
					</div>
				</div>
				<div class="portlet-body display-hide">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">凭证编号</label>
								<div class="controls">
									<p class="form-control-static">
										<s:property value="voucher" />
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">经办人</label>
								<div class="controls">
									<div class="input-icon right">
										<p class="form-control-static">
											<s:property value="voucherUser.display" />
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">部门</label>
								<div class="controls">
									<div class="input-icon right">
										<p class="form-control-static">
											<s:property value="voucherDepartment.display" />
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">记账日期</label>
								<div class="controls">
									<p class="form-control-static">
										<s:date name="voucherDate" format="yyyy-MM-dd" />
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">销售客户</label>
								<div class="controls">
									<div class="input-icon right">
										<p class="form-control-static">
											<s:property value="customerProfile.display" />
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">关联订单号</label>
								<div class="controls">
									<p class="form-control-static">
										<s:property value="referenceVoucher" />
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">关联订单来源</label>
								<div class="controls">
									<p class="form-control-static">
										<s:property value="referenceSource" />
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">收货人</label>
								<div class="controls">
									<p class="form-control-static">
										<s:property value="receivePerson" />
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">电话</label>
								<div class="controls">
									<p class="form-control-static">
										<s:property value="mobilePhone" />
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">邮编</label>
								<div class="controls">
									<p class="form-control-static">
										<s:property value="postCode" />
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">收货地址</label>
								<div class="controls">
									<p class="form-control-static">
										<s:property value="deliveryAddr" />
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">快递单号</label>
						<div class="controls">
							<s:textfield name="logisticsNo" readonly="%{deliveryTime}"/>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6" id="logistics">
					<div class="form-group">
						<label class="control-label">快递公司</label>
						<div class="controls">
							<s:select name="logistics.id" list="logisticsNameMap" disabled="%{deliveryTime}" data-profile-param="default_sale_delivery_delivery_logistics"/>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">预约发货日期</label>
						<div class="controls">
						<p class="form-control-static">
								<span class="badge-info">&nbsp;<s:date name="reserveDeliveryTime" format="yyyy-MM-dd"/>&nbsp;</span>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">备注说明</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="memo" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row margin-2" />
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-sale-sale-delivery-picking-details" data-grid="items" data-pk='<s:property value="id"/>'></table>
				</div>
			</div>
		</div>
		<div class="form-actions right">
			<button class="btn blue" type="submit" data-ajaxify-reload="_closest-ajax-container">
				<i class="fa fa-check"></i> 拣货
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
		</div>
	</form>
	<script src="${base}/myt/sale/sale-delivery-picking-details.js" />
</s:else>
<%@ include file="/common/ajax-footer.jsp"%>
