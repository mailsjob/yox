<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-sale-delivery-notify"
	action="${base}/myt/sale/sale-delivery!deliveryNotify" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<%-- <s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
			data-grid-reload=".grid-myt-sale-sale-delivery">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
		</s3:button> --%>
		<button class="btn blue" type="submit">
			<i class="fa fa-check"></i> 发送
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>

	</div>
	<div class="form-body control-label-sm">
		<div class="row hide">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">凭证号</label>
					<div class="controls">
						<s:textfield name="voucher" readonly="true" />
					</div>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">关联订单号</label>
					<div class="controls">
						<s:textfield name="boxOrderDetail.boxOrder.orderSeq" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">客户</label>
					<div class="controls">
						<s:textfield name="customerProfile.display" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">客户电话</label>
					<div class="controls">
						<s:textfield name="customerProfile.mobilePhone" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">客户邮件</label>
					<div class="controls">
						<s:textfield name="customerProfile.email" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人电话</label>
					<div class="controls">
						<s:textfield name="mobilePhone" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人邮件</label>
					<div class="controls">
						<s:textfield name="email" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><font color="red">短信消息</font>&nbsp;<s:checkbox name="booleanSendSms" value="true" /></div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label">电话</label>
							<div class="controls">
								<s:textfield name="preMobile" value="%{#request.preMobile}"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label">内容</label>
							<div class="controls">
								<s:textarea name="preSmsContent" rows="5" value="%{#request.preSmsContent}"></s:textarea>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><font color="red">邮件消息</font>&nbsp;<s:checkbox name="booleanSendEmail" value="true" /></div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">地址</label>
							<div class="controls">
								<s:textfield name="preEmail" value="%{#request.preEmail}"/>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label">主题</label>
							<div class="controls">
								<s:textfield name="preSub" value="%{#request.preSub}"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">内容</label>
							<div class="controls">
								<s:textarea name="preEmailContent" data-htmleditor="kindeditor" value="%{#request.preEmailContent}" data-height="600px" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</form>
<script src="${base}/myt/sale/sale-delivery-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
