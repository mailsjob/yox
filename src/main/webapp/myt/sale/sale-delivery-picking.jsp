<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-sale-delivery-picking" action="#" method="post">
	<div class="form-actions">
		<div class="row">
			<div class="col-md-6">
				<div class="input-group">
					<span class="input-group-addon">销售单号 <i class="fa fa-barcode"></i></span><input type="text" name="voucher"
						placeholder="条码枪扫描输入..." class="form-control"><span class="input-group-btn">
						<button type="submit" class="btn blue">查询</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</form>
<div class="div-sale-delivery-picking-details"></div>
<script src="${base}/myt/sale/sale-delivery-picking.js" />
<%@ include file="/common/ajax-footer.jsp"%>