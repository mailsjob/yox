<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="form-horizontal form-bordered form-label-stripped">
	<div class="row">
		<s:if test="#attr.taskVariablesVar['auditPostTime']!=null">
			<div class="col-md-6">
				<div class="portlet gren">
					<div class="portlet-title">
						<div class="caption">一线审核</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<label class="control-label">通过:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.booleanLabel[#attr.taskVariablesVar['auditPostPass']]" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审核人员:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#attr.taskVariablesVar['auditPostUser']" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审核时间:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:date name="#attr.taskVariablesVar['auditPostTime']" format="yyyy-MM-dd HH:mm:ss" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审核意见:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#attr.taskVariablesVar['auditPostExplain']" />
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</s:if>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">凭证号</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="voucher" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">退货客户</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="customerProfile.display" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">退货类型</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="#application.enums.processTypeEnum[processType]" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">退货商品</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="commodity.display" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">退货数量</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="quantity" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">商品单价</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="boxOrderDetailCommodity.commodity.price" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">客户退回快递公司</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="logistics.display" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">客户退回快递单号</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="logisticsNo" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">用户备注</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="description" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">系统备注</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="memo" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<%-- <div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th align="center" style="width: 20px">#</th>
							<th align="center" style="width: 400px">商品</th>
							<th align="center" style="width: 100px">数量</th>
							<th align="center" style="width: 100px">单价</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><s:property value="1" /></td>
							<td><s:property value="boxOrderDetailCommodity.commodity.display" /></td>
							<td align="center"><s:property value="boxOrderDetailCommodity.commodity.quantity" /></td>
							<td align="right"><s:property value="boxOrderDetailCommodity.commodity.price" /></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div> --%>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">用户收货城市</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="saleReturnCity.display" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">用户收货电话</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="saleReturnPhone" />
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">收货人</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="saleReturnName" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">用户收货详细地址</label>
				<div class="controls">
					<div class="input-group">
						<p class="form-control-static">
							<s:property value="saleReturnDetailAddr" />
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%-- <div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">商品图片</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="commodityImg" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">商品标题</label>
				<div class="controls">
					<p class="form-control-static">
						<s:property value="commodityTitle" />
					</p>
				</div>
			</div>
		</div>
	</div> --%>
	
	<div class="row">
		<div class="col-md-12">
			<div class="note note-success">
				<h4 class="block">
					商品总金额：
					<s:property value="quantity * boxOrderDetailCommodity.commodity.price" />
				</h4>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/sale/sale-return-apply-viewBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>