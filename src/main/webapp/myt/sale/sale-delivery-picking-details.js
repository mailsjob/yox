$(function() {

    $(".form-sale-delivery-picking-details").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            var $grid = $form.find(".grid-myt-sale-sale-delivery-picking-details");
           /* //默认申通
            $("#logistics option").each(function() {
                var $option = $(this);
                var storageDisplay = $option.text();
                if (storageDisplay.indexOf('申通快递') > -1) {
                    $option.parent("select").val($option.val()).select2();
                 }
            });*/
            //扫描枪输入处理
            $form.find("input[name='barcode']").barcodeScanSupport({
                onEnter : function() {
                    var code = $(this).val();
                    if (code != '') {
                        var ids = $grid.jqGrid('getDataIDs');
                        var targetRowdata = null;
                        var targetRowid = null;
                        $.each(ids, function(i, id) {
                            var item = $grid.jqGrid('getRowData', id);
                            if (item['commodity.barcode'] == code) {
                                if (item['gift'] != 'true') {
                                    targetRowid = id;
                                    targetRowdata = item;
                                    return false;
                                }
                            }
                        });
                        if (targetRowdata) {
                            var pickQuantity = parseFloat(targetRowdata.pickQuantity) + 1;
                            targetRowdata.pickQuantity = pickQuantity;
                            if (pickQuantity > parseFloat(targetRowdata.quantity)) {
                                alert('拣货数量不能大于销售数量');
                                return;
                            }
                            $grid.jqGrid('setRowData', targetRowid, targetRowdata);
                        }
                        $(this).focus();
                    }
                }
            });
        },
        preValidate : function() {
            var $form = $(this);
            var $grid = $form.find(".grid-myt-sale-sale-delivery-picking-details");

            var rowdatas = $grid.jqGrid("getRowData");
            var pass = true;
            $.each(rowdatas, function(i, rowdata) {
                var pickQuantity = parseFloat(rowdata.pickQuantity);
                var quantity = parseFloat(rowdata.quantity);
                if (pickQuantity != quantity) {
                    pass = false;
                    return false;
                }
            });
            if (!pass) {
                alert("所有行项的拣货数量必须等于销售数量");
            }
            return pass;
        }
    });
    $(".grid-myt-sale-sale-delivery-picking-details").data("gridOptions", {
        batchEntitiesPrefix : "saleDeliveryDetails",
        url : function() {
            var pk = $(this).attr("data-pk");
            if (pk) {
                return WEB_ROOT + "/myt/sale/sale-delivery!saleDeliveryDetails?id=" + pk;
            }
        },
        colModel : [ {
            label : '所属销售单主键',
            name : 'saleDelivery.id',
            hidden : true,
            hidedlg : true,
            editable : true,
            formatter : function(cellValue, options, rowdata, action) {
                var pk = $(this).attr("data-pk");
                return pk ? pk : "";
            }
        }, {
            label : '销售（发货）商品',
            name : 'commodity.display',
            width : 300
        }, {
            name : 'commodity.barcode',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '单位',
            name : 'measureUnit',
            width : 80
        }, {
            label : '数量',
            name : 'quantity',
            width : 80,
            formatter : 'number',
            editable : true,
            editoptions : {
                readonly : true
            },
            summaryType : 'sum'
        }, {
            label : '拣货数量',
            name : 'pickQuantity',
            width : 80,
            align : 'center',
            formatter : 'number',
            editable : true,
            formatter : function(cellValue, options, rowdata, action) {
                var state = null;
                if (cellValue == undefined) {
                    cellValue = 0;
                }
                cellValue = parseFloat(cellValue);
                var quantity = parseFloat(rowdata.quantity);
                if (cellValue == 0 || cellValue > quantity) {
                    state = 'badge-danger';
                } else if (cellValue == quantity) {
                    state = 'badge-success';
                } else {
                    state = 'badge-warning';
                }
                return '<span class="badge ' + state + '">' + cellValue + '</span>';
            },
            unformat : function(cellValue, options, cell) {
                return $('span', cell).text();
            },
            editrules : {
                number : true
            },
            summaryType : 'sum'
        }, {
            label : '发货仓库',
            name : 'storageLocation.id',
            width : 150,
            stype : 'select',
            searchoptions : {
                value : Biz.getStockDatas()
            }
        }, {
            label : '销售单价',
            name : 'price',
            width : 80,
            formatter : 'currency'
        }, {
            label : '是否赠品',
            name : 'gift',
            width : 80,
            edittype : 'checkbox',
            align : 'center'
        }, {
            label : '批次号',
            name : 'batchNo',
            width : 80
        }, {
            label : '批次过期日期',
            name : 'expireDate',
            width : 80
        }  ],
        footerLocalDataColumn : [ 'quantity' ],
        inlineNav : {
            add : false,
            del : false
        },
        footerrow : true,
        userDataOnFooter : true
    });
});