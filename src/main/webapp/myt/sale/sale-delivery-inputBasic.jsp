<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-sale-delivery-inputBasic"
	action="${base}/myt/sale/sale-delivery!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		
		<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
			data-grid-reload=".grid-myt-sale-sale-delivery">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
		</s3:button>
		<button class="btn default btn-cancel"  type="button">取消</button>
		<button class="btn green" type="button" data-toggle="panel"
				data-url="${base}/myt/finance/payment-apply!bpmNew?bizVoucher=<s:property value='voucher'/>
				&bizVoucherType=XS&bizTradeUnitId=<s:property value='customerProfile.bizTradeUnitId'/>
				&bizTradeUnitName=<s:property value='customerProfile.display'/>">补偿退付款</button>
		<s3:button disabled="%{disallowChargeAgainst!=null}" type="submit" cssClass="btn red pull-right"
			data-grid-reload=".grid-myt-sale-sale-delivery" data-form-action="${base}/myt/sale/sale-delivery!chargeAgainst"
			data-confirm="确认冲销当前销售单？">
			<s:property value="%{disallowChargeAgainst!=null?disallowChargeAgainst:'红冲'}" />
		</s3:button>
		<a class="btn yellow btn-send-delivery-notify" href="javascript:;"><i class="fa fa-indent"> 发货通知邮件和短信</i></a>
	</div>

	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">凭证状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.voucherStateEnum[voucherState]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 ">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:textfield name="voucher" readonly="%{notNew}" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<s3:datetextfield name="voucherDate" format="date" current="true" readonly="true" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:select name="voucherUser.id" list="usersMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:select name="voucherDepartment.id" list="departmentsMap" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">销售客户</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-customer-profile"></i>
							<s:textfield name="customerProfile.display" disabled="%{'DRAFT'!=voucherState.name()}" readonly="true" />
							<s:hidden name="customerProfile.id" disabled="%{'DRAFT'!=voucherState.name()}" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">关联订单号</label>
					<div class="controls">
						<s:textfield name="referenceVoucher" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">关联订单来源</label>
					<div class="controls">
						<s:select name="referenceSource" list="referenceSourceMap"
							data-profile-param="default_sale_delivery_reference_source" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">销售单名称</label>
					<div class="controls">
						<s:textfield name="title" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人</label>
					<div class="controls">
						<s:textfield name="receivePerson" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">电话</label>
					<div class="controls">
						<s:textfield name="mobilePhone" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">邮编</label>
					<div class="controls">
						<s:textfield name="postCode" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收货地址</label>
					<div class="controls">
						<div class="input-group">
							<s:textfield name="deliveryAddr" />
							<div class="input-group-btn">
								<button tabindex="-1" class="btn default btn_delivery_print" type="button">打印发货清单</button>
								&nbsp;&nbsp;
								<button tabindex="-1" class="btn default btn_express_print_QF" type="button">打印快递单（全峰快递）</button>
								<button tabindex="-1" data-toggle="dropdown" class="btn default dropdown-toggle" type="button">
									<i class="fa fa-angle-down"></i>
								</button>
								<ul role="menu" class="dropdown-menu pull-right">
									<li><a class="btn_express_print_ST">申通快递</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">发货人模板</label>
					<div class="controls">
						<s:select name="expressDataTemplate.id" list="expressDataTemplatesMap"
							data-profile-param="default_sale_delivery_express_template" />
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">发货类型</label>
					<div class="controls">
						<s:radio name="deliveryType" list="#{'INNER_EXPRESS':'普邮','EXPRESS':'直邮特快','POST':'直邮普邮','DHL':'直邮DHL','TNT':'直邮TNT'}"
							value="'INNER_EXPRESS'" data-profile-param="default_sale_delivery_type" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">代理分销商</label>
					<div class="controls">
						<s:textfield name="agentPartner.display" />
						<s:hidden name="agentPartner.id" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">代发货库房</label>
					<div class="controls">
						<s:select name="stockPartner.id" list="partnersMap" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">预约发货日期</label>
					<div class="controls">
						<s3:datetextfield format="date" name="reserveDeliveryTime"  />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label">库存模式</label>
					<div class="controls">
						<s:radio name="storageMode" list="#application.enums.storageModeEnum" disabled="true"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">备注说明</label>
					<div class="controls">
						<s:textfield name="memo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-sale-sale-delivery-inputBasic" data-grid="items" data-readonly="true"
					data-pk='<s:property value="#parameters.id"/>'></table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">整单折扣额</label>
					<div class="controls">
						<s:textfield name="discountAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">总计原始金额</label>
					<div class="controls">
						<s:textfield name="totalOriginalAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">折后金额</label>
					<div class="controls">
						<s:textfield name="commodityAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">交税总金额</label>
					<div class="controls">
						<s:textfield name="totalTaxAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">含税总金额</label>
					<div class="controls">
						<s:textfield name="commodityAndTaxAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收取运费</label>
					<div class="controls">
						<s:textfield name="chargeLogisticsAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">应收总金额</label>
					<div class="controls">
						<s:textfield name="totalAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">已收总金额</label>
					<div class="controls">
						<s:textfield name="payedAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收款账户</label>
					<div class="controls">
						<s:select name="accountSubject.id" list="paymentAccountSubjects" disabled="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收款备注</label>
					<div class="controls">
						<s:textarea name="paymentReference" rows="3" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<s:if test="redwordDate==null">
		<div class="form-actions right">
			<button type="submit" class="btn blue" data-grid-reload=".grid-myt-sale-sale-delivery">
				<i class="fa fa-check"></i> 保存
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<s:if test="%{deliveryTime==null}">
				<button class="btn red pull-left" type="submit" data-grid-reload=".grid-myt-sale-sale-delivery"
					data-form-action="${base}/myt/sale/sale-delivery!chargeAgainst" data-confirm="确认冲销当前销售单？">红冲</button>
			</s:if>
		</div>
	</s:if>
</form>
<script src="${base}/myt/sale/sale-delivery-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
