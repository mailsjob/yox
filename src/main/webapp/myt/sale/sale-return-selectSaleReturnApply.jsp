<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form method="get" class="form-inline form-validation form-search-init"
			data-grid-search=".grid-sale-sale-return-select-apply" action="#">
			<div class="form-group">
				<s:textfield name="search['CN_boxOrderDetailCommodity.boxOrder.orderSeq_OR_commodity.sku_OR_commodity.title_OR_saleReturnNamee_OR_saleReturnPhone']"
					cssClass="form-control input-xlarge" placeholder="订单编码/商品/退换货收货人/退换货收货电话..." />
			</div>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
		</form>
	</div>
</div>
<div class="row">
	<table class="grid-sale-sale-return-select-apply" data-grid="table"></table>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-sale-sale-return-select-apply").data("gridOptions", {
            url : WEB_ROOT + '/myt/sale/sale-return-apply!findByPage',
            colModel : [{
                label : '流水号',
                name : 'id',
                hidden:true
            }, {
                label : '订单号',
                name : 'boxOrderDetailCommodity.boxOrder.orderSeq',
                align : 'center',
                width : 180
            }, {
                label : '行项号',
                name : 'boxOrderDetailCommodity.boxOrderDetail.sn',
                stype : 'date',
                width : 100
            }, {
                label : '行项状态',
                name : 'boxOrderDetailCommodity.boxOrderDetail.orderDetailStatus',
                align : 'center',
                stype:'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('boxOrderDetailStatusEnum')
                },
                width : 100
            }, {
                label : '商品名称',
                name : 'boxOrderDetailCommodity.commodity.display',
                index : 'commodity.sku_OR_commodity.title',
                width : 200
            }, {
                label : '商品名称',
                name : 'boxOrderDetailCommodity.commodity.id',
                hidden:true,
                width : 100
            }, {
                label : '商品sku',
                name : 'boxOrderDetailCommodity.commodity.sku',
                hidden:true,
                width : 100
            }, {
                label : '商品名称',
                name : 'boxOrderDetailCommodity.commodity.title',
                hidden:true,
                width : 100
            }, {
                label : '商品名称',
                name : 'boxOrderDetailCommodity.commodity.id',
                hidden:true,
                width : 100
            }, {
                label : '行项商品售价',
                name : 'boxOrderDetailCommodity.price',
                width : 100
            }, {
                label : '原价金额',
                hidden:true,
                name : 'boxOrderDetailCommodity.originalAmount',
                width : 100
            }, {
                label : '折扣额',
                hidden:true,
                name : 'boxOrderDetailCommodity.discountAmount',
                width : 100
            }, {
                label : '折后金额',
                hidden:true,
                name : 'boxOrderDetailCommodity.actualAmount',
                width : 100
            }, {
                label : '成本价',
                hidden:true,
                name : 'boxOrderDetailCommodity.costPrice',
                width : 100
            }, {
                label : '商品金额',
                hidden:true,
                name : 'boxOrderDetailCommodity.commodityAmount',
                width : 100
            }, {
                label : '客户',
                name : 'customerProfile.id',
                hidden:true,
                width : 100
            }, {
                label : '客户',
                name : 'customerProfile.display',
                index : 'customerProfile.nickName_OR_customerProfile.trueName',
                width : 100
            }, {
                label : '服务类型',
                name : 'processType',
                align : 'center',
                stype:'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('processTypeEnum')
                },
                width : 100
            }, {
                label : '数量',
                name : 'quantity',
                align:'center',
                width : 50
            }, {
                label : '处理结果',
                name : 'processResult',
                stype:'select',
                align : 'center',
                searchoptions : {
                    value : Util.getCacheEnumsByType('postAuditResultEnum')
                },
                width : 100
            }, {
                label : '处理结果类型',
                name : 'saleReturnType',
                align : 'center',
                stype:'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('processTypeEnum')
                },
                width : 100
            }, {
                label : '客户寄回的物流公司',
                name : 'logistics.id',
                align : 'center',
                stype : 'select',
                editoptions : {

                    value : Biz.getLogisticsDatas()
                },
                width : 200
            }, {
                label : '客户寄回的快递单号',
                name : 'logisticsNo',
                width : 150
            },/* {
                label : '商家收到退货时间',
                name : 'agentPartnerReceiveTime',
                sorttype:'date',
                width : 120
            },*/ {
                label : '收货人',
                name : 'saleReturnName',
                width : 100
            }, {
                label : '收货电话',
                name : 'saleReturnPhone',
                width : 100
            }, {
                label : '收货地址',
                name : 'saleReturnDetailAddr',
                width : 100
            }],
            rowNum : 10,
            multiselect : false,
            /* postData : {
               "search['NU_deliveryTime']" : false
            }, */
            setGroupHeaders : {
                groupHeaders : [ {
                    startColumnName : 'saleReturnName',
                    numberOfColumns : 3,
                    titleText : '客户填写的收货信息'
                } ]
            },
            onSelectRow : function(id) {
                var $grid = $(this);
                var rowdata = $grid.jqGrid("getRowData", id);
                var $dialog = $grid.closest(".modal");
                $dialog.modal("hide");
                var callback = $dialog.data("callback");
                if (callback) {
                    rowdata.id = id;
                    callback.call($grid, rowdata);
                }
			 }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
