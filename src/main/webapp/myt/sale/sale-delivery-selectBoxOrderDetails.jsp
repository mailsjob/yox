<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row search-form-default">
		<div class="col-md-12">
			<form method="get" class="form-inline form-validation form-search form-search-init"
				data-grid-search=".grid-sale-sale-delivery-select-order" action="#">
				<div class="form-group">
					<input type="text"
						name="search['CN_boxOrder.orderSeq_OR_boxOrder.receivePerson_OR_boxOrder.customerProfile.trueName_OR_boxOrder.customerProfile.nickName']"
						class="form-control input-large" placeholder="订单号、客户、收货人..." />
				</div>
				<div class="form-group">
					<div class="input-group input-medium">
						<input type="text" name="search['BT_reserveDeliveryTime']"
							class="form-control input-medium input-daterangepicker grid-param-data" placeholder="预约发货日期">
					</div>
				</div>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
				<button class="btn default hidden-inline-xs" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-sale-sale-delivery-select-order" data-grid="table"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-sale-sale-delivery-select-order").data("gridOptions", {
            url : '${base}/myt/sale/box-order-detail!findByPage',
            colModel : [ {
                label : '订单号',
                name : 'boxOrder.orderSeq',
                width : 180,
                align : 'left',
                formatter : function(cellValue, options, rowdata, action) {
                    var html = cellValue;
                    if (rowdata.boxOrder.circle) {
                        html += '<span class="badge badge-info">周</span>'
                    }
                    if (rowdata.boxOrder.splitPayMode != 'NO') {
                        html += '<span class="badge badge-success">分</span>'
                    }
                    return html;
                },
                unformat : function(cellValue, options, cell) {
                    return Util.getTextWithoutChildren(cell);
                }
            }, {
                label : '分期模式',
                name : 'boxOrder.splitPayMode',
                width : 50,
                align : 'center'
            }, {
                label : '名称',
                name : 'boxOrder.title',
                align : 'left',
                width : 120
            }, {
                label : '行项号',
                name : 'sn',
                align : 'center',
                width : 60
            }, {
                label : '预约发货日期',
                name : 'reserveDeliveryTime',
                editable : true,
                width : 100,
                align : 'center',
                stype : 'date'
            }, {
                label : '行项状态',
                name : 'orderDetailStatus',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('boxOrderDetailStatusEnum')
                }
            }, {
                label : '行项应付金额',
                name : 'amount',
                width : 70,
                formatter : 'currency'
            }, {
                label : '行项已付金额',
                name : 'payAmount',
                width : 70,
                formatter : 'currency'
            }, {
                label : '订单应付金额',
                width : 70,
                name : 'boxOrder.actualAmount',
                formatter : 'currency'
            }, {
                label : '订单已付金额',
                width : 70,
                name : 'boxOrder.actualPayedAmount',
                formatter : 'currency'
            }, {
                name : 'boxOrder.customerProfile.id',
                hidden : true,
                width : 80
            }, {
                label : '下单客户',
                name : 'boxOrder.customerProfile.display',
                align : 'left',
                width : 80,
                formatter : function(cellValue, options, rowdata, action) {
                    var url = WEB_ROOT + "/myt/customer/customer-profile!view?id=" + rowdata.boxOrder.customerProfile.id;
                    return '<a href="'+url+'" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
                },
                unformat : function(cellValue, options, cell) {
                    return $('a', cell).html();
                }
            }, {
                label : '收货人',
                width : 70,
                align : 'center',
                name : 'boxOrder.receivePerson'
            }, {
                label : '收货人电话',
                width : 70,
                name : 'boxOrder.mobilePhone'
            }, {
                label : '收货人地址',
                width : 200,
                name : 'boxOrder.deliveryAddr'
            }, {
                label : '邮编',
                width : 70,
                hidden : true,
                name : 'boxOrder.postCode'
            }, {
                label : '客户备注',
                width : 100,
                hidden : true,
                name : 'boxOrder.customerMemo'
            } ],
            rowNum : 10,
            multiselect : false,
            postData : {
                "search['FETCH_boxOrder']" : "INNER",
                "search['IN_orderDetailStatus']" : "S10O,S15O,S20PYD,S25PYD,S30C,S35C,S40DP"
            },
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : "${base}/myt/sale/box-order-detail!boxOrderDetailCommodities?id=" + row_id,
                    colModel : [ {
                        name : 'id',
                        hidden : true,
                        responsive : 'sm'
                    }, {
                        name : 'commodity.id',
                        hidden : true,
                        width : 50
                    }, {
                        label : '赠品',
                        name : 'gift',
                        edittype : 'checkbox',
                        align : 'center',
                        width : 50
                    }, {
                        label : '商品编码',
                        name : 'commodity.sku',
                        align : 'center',
                        width : 80
                    }, {
                        label : '商品标题',
                        name : 'commodity.title',
                        width : 250
                    }, {
                        label : '商品',
                        name : 'commodity.display',
                        hidden : true,
                        width : 250
                    }, {
                        label : '发货库存地',
                        name : 'storageLocation.display',
                        width : 80
                    }, {
                        name : 'storageLocation.id',
                        hidden : true,
                        width : 80
                    }, {
                        label : '商品库存地',
                        name : 'commodity.defaultStorageLocation.display',
                        hidden : true,
                        width : 80
                    }, {
                        name : 'commodity.defaultStorageLocation.id',
                        hidden : true,
                        width : 80
                    }, {
                        label : '销售单价',
                        name : 'price',
                        formatter : 'currency',
                        width : 100
                    }, {
                        label : '销售数量',
                        name : 'quantity',
                        formatter : 'number',
                        width : 100
                    }, {
                        label : '商品单位',
                        name : 'commodity.measureUnit',
                        width : 80
                    }, {
                        label : '原始金额',
                        name : 'originalAmount',
                        formatter : 'currency',
                        width : 100
                    }, {
                        label : '折后金额',
                        name : 'amount',
                        formatter : 'currency',
                        width : 100
                    }, {
                        label : '已发货数量',
                        name : 'deliveriedQuantity',
                        formatter : 'number',
                        width : 80
                    }, {
                        label : '可发货数量',
                        name : 'toDeliveryQuantity',
                        formatter : 'number',
                        width : 80
                    }, {
                        label : '锁定库存量',
                        name : 'salingLockedQuantity',
                        formatter : 'number',
                        width : 80
                    } ],
                    rowNum : -1,
                    loadonce : true,
                    multiselect : true,
                    operations : function(itemArray) {
                        var $grid = $(this);
                        var $select = $('<li><a href="javascript:;"><i class="fa fa-check-square-o"></i> 选取</a></li>');
                        $select.children("a").bind("click", function(e) {
                            e.preventDefault();
                            var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                            var $dialog = $grid.closest(".modal");
                            $dialog.modal("hide");
                            var callback = $dialog.data("callback");
                            if (callback) {
                                var $prow = $grid.closest('.ui-subgrid').prev();
                                var $pgrid = $prow.closest('.ui-jqgrid-btable');
                                var prowid = $prow.attr("id");
                                var data = {
                                    master : $pgrid.jqGrid("getRowData", prowid),
                                    rows : $grid.jqGrid("getSelectedRowdatas")
                                };
                                //alert(data.master['boxOrder.receivePerson']);
                                callback.call($grid, data);
                            }
                        });
                        itemArray.push($select);
                    }
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
