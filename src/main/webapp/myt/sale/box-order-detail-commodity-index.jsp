<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form action="#" method="get" class="form-inline form-validation  form-search-init"
			data-grid-search=".grid-myt-sale-box-order-detail-commodity">
			<div class="input-group">
				<div class="form-group">
				<input type="text" name="search['CN_commodity.sku_OR_commodity.title_OR_boxOrder.orderSeq_OR_boxOrder.receivePerson_OR_boxOrder.customerProfile.trueName_OR_boxOrder.customerProfile.nickName']"
							 class="form-control input-xlarge"
								placeholder="商品编码、商品名称、订单号、客户，收货人..." />
				</div>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
				<button class="btn default" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-myt-sale-box-order-detail-commodity"></table>
	</div>
</div>
<script src="${base}/myt/sale/box-order-detail-commodity-index.js" />
<%@ include file="/common/ajax-footer.jsp"%>
