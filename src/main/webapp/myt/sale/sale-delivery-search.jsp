<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab" href="#tab-auto">发货列表</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-myt-sale-sale-delivery-search" action="#">
						<div class="form-group">
							<input type="text" name="search['CN_voucher_OR_referenceVoucher_OR_receivePerson_OR_logisticsNo_OR_mobilePhone_OR_deliveryAddr']"
							 class="form-control input-xlarge"
								placeholder="发货凭证号，销售订单号，物流单号，收货人，电话，地址...">
						</div>
						<div class="form-group">
							<input type="text" name="search['BT_deliveryTime']" class="form-control input-medium input-daterangepicker"
								placeholder="发货时间">
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-sale-sale-delivery-search"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/sale/sale-delivery-search.js" />
<%@ include file="/common/ajax-footer.jsp"%>
