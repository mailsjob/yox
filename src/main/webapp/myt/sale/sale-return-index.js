$(function() {
    $(".grid-myt-sale-sale-return").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/sale-return!findByPage',
        colModel : [ {
            label : '凭证号',
            name : 'voucher',
            width : 140
        }, {
            label : '凭证日期',
            name : 'voucherDate',
            stype : 'date',
            width : 100
        }, {
            label : '凭证状态',
            name : 'voucherState',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('voucherStateEnum')
            },
            formatter : function(cellValue, options, rowdata, action) {
                if (cellValue == 'REDW') {
                    return '<span class="badge">红冲</span>'
                } else if (cellValue == 'DRAFT') {
                    return '<span class="badge badge-warning">草稿</span>'
                } else if (cellValue == 'POST') {
                    return '<span class="badge badge-success">提交</span>'
                }
                return cellValue;
            },
            width : 100
        }, {
            label : '退换货类型',
            name : 'saleReturnType',
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('saleReturnTypeEnum')
            },
            width : 100
        }, {
            label : '提交时间',
            name : 'submitDate',
            stype : 'date',
            hidden : true,
            width : 100
        }, {
            label : '最近操作',
            name : 'lastOperationSummary',
            width : 120
        }, {
            label : '经办人',
            name : 'voucherUser.display',
            index : 'voucherUser.signinid',
            width : 120
        }, {
            label : '经办部门',
            name : 'voucherDepartment.display',
            index : 'voucherDepartment.code_OR_voucherDepartment.title',
            width : 100
        }, {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName_OR_customerProfile.trueName',
            width : 100
        }, {
            label : '商品售额',
            name : 'totalSaleAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '负担买家运费',
            name : 'deliveryAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '退款金额',
            name : 'totalReturnAmount',
            formatter : 'currency',
            width : 100
        } ],
        editcol : 'voucher',
        editurl : WEB_ROOT + "/myt/sale/sale-return!doSave",
        fullediturl : WEB_ROOT + "/myt/sale/sale-return!inputTabs",
        subGrid : true,
        addable : false,
        inlineNav : {
            add : false
        },
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/sale/sale-return!saleReturnDetails?id=" + row_id,
                colModel : [ {
                    label : '退货商品',
                    name : 'commodity.display',
                    align : 'left'
                }, {
                    label : '发货仓库',
                    name : 'storageLocation.id',
                    width : 80,
                    stype : 'select',
                    formatter : 'select',
                    searchoptions : {
                        value : Biz.getStockDatas()
                    }
                }, {
                    label : '批次号',
                    name : 'batchNo',
                    width : 80
                }, {
                    label : '过期日期',
                    name : 'expireDate',
                    width : 80
                }, {
                    label : '单位',
                    name : 'measureUnit',
                    editable : true,
                    width : 60
                }, {
                    label : '数量',
                    name : 'quantity',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '单价',
                    name : 'price',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '商品售额',
                    name : 'amount',
                    width : 80,
                    formatter : 'currency'
                }, {
                    label : '售出时成本额',
                    name : 'commodityAmount',
                    width : 80,
                    formatter : 'currency'
                }, {
                    label : '是否报废',
                    name : 'scrapped',
                    width : 80,
                    edittype : 'checkbox',
                    editable : true
                }, {
                    label : '折损额',
                    name : 'lossAmount',
                    width : 80,
                    formatter : 'currency'
                }, {
                    label : '运费',
                    name : 'deliveryAmount',
                    width : 80,
                    formatter : 'currency'
                }, {
                    label : '入库总成本',
                    name : 'costAmount',
                    width : 80,
                    formatter : 'currency'
                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
});
