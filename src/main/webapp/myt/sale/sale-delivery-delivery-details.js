$(function() {
    $(".form-sale-delivery-delivery-details").data("formOptions", {
       
        bindEvents : function() {
            var $form = $(this);
            // 扫描枪输入处理
            $form.find("input[name='logisticsNo']").barcodeScanSupport({
                onEnter : function() {
                    var code = $(this).val();
                    var strCode = code.substr(0, 3);
                    var logisticsLabel = '';
                    if (strCode == '768') {
                        logisticsLabel = "申通快递";
                    } else if (strCode == '300') {
                        logisticsLabel = "全峰快递";
                    }
                    if (logisticsLabel != '') {
                        $("#logistics option").each(function() {
                            var $option = $(this);
                            var logisticsDisplay = $option.text();
                            if (logisticsDisplay.indexOf(logisticsLabel) > -1) {
                                $option.parent("select").val($option.val()).select2();
                                $form.find("input[name='logisticsAmount']").focus();
                                 return false;
                            }
                        });
                    }
                }
            });
            $form.find(".btn-send-delivery-notify").click(function() {
                var id=$(this).attr("data-pk");
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/sale/sale-delivery!preDeliveryNotify?_to_=notify&id='+id,
                    title:'发货通知邮件和短信'
                 }) 
            });
            //默认申通
           /* var $el = $form.find("select[name='logistics.id']");
            var orgVal = $el.val();
            if(orgVal == undefined || orgVal == ''){
                $("#logistics option").each(function() {
                    var $option = $(this);
                    var logisticsText = $option.text();
                    if (logisticsText.indexOf('申通快递') > -1) {
                        $option.parent("select").val($option.val()).select2();
                     }
                }); 
            }*/
            
            
            /**
             * 由于快递发货 实际是编辑一个已存在的销售单，
             * 使其无法使用通用的默认值处理（通用的默认值处理需要表单为新建状态，即不能有ID。见page.js 398）
             * 所以这里单独做 快递发货的 默认值处理
             */
            var $el = $form.find("select[name='logistics.id']");
            var code = $el.attr("data-profile-param");
            var orgVal = $el.val();
            if (orgVal == undefined || orgVal == '') {
                //从缓存取当前用户的配置参数
                var initVal = Global.findUserProfileParams(code);
                if (initVal) {
                    if ($el.is('[type="radio"]')) {
                        $controls.find('[type="radio"][value="' + initVal + '"]').attr("checked", true);
                    } else {
                        $el.val(initVal);
                        if ($el.is("select")) {
                            $el.select2();
                        }
                    }
                }
            }
            /** **/
        },
        
    });
});