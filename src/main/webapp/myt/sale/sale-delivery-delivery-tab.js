$(function() {
    $(".grid-myt-sale-sale-delivery-delivery-tab").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/sale-delivery!findByPage',
        colModel : [ {
            label : '凭证号',
            name : 'voucher',
            width : 80
        }, {
            label : '发货类型',
            stype : 'select',
            name : 'deliveryType',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('deliveryTypeEnum')
            },
            width : 60
        }, {
            label : '下单时间',
            name : 'createdDate',
            stype : 'date',
            width : 100
        }, {
            label : '预约发货日期',
            name : 'reserveDeliveryTime',
            stype : 'date',
            width : 120,
            cellattr : function(rowId, cellValue, rowdata, cm) {
                if (rowdata.reserveDeliveryTime) {
                    var today = new Date();
                    var month;
                    if (today.getMonth() < 9) {
                        month = "0" + (today.getMonth() + 1);

                    } else {
                        month = today.getMonth() + 1;
                    }
                    var todayLabel = today.getFullYear() + "-" + month + "-" + today.getDate();
                    if (cellValue == todayLabel) {
                        return "class='badge-warning'"
                    } else if (cellValue > todayLabel) {
                        return "class='badge-success'"
                    } else {
                        return "class='badge-danger'"
                    }
                }
                return;
            }
        }, {
            label : '最近操作',
            name : 'lastOperationSummary',
            width : 120
        }, {
            label : '经办人',
            name : 'voucherUser.display',
            index : 'voucherUser.signinid',
            width : 80
        }, {
            label : '经办部门',
            name : 'voucherDepartment.display',
            index : 'voucherDepartment.code_OR_voucherDepartment.title',
            hidden : true,
            width : 100
        }, {
            label : '拣货人',
            name : 'pickUser.display',
            hidden : true,
            width : 100
        }, {
            label : '拣货时间',
            name : 'pickTime',
            hidden : true,
            width : 100
        }, {
            label : '收货人',
            name : 'receivePerson',
            width : 60
        }, {
            label : '收货人电话',
            name : 'mobilePhone',
            width : 80
        }, {
            label : '收货地址',
            name : 'deliveryAddr',
            width : 200
        }, {
            label : '收取客户运费',
            name : 'chargeLogisticsAmount',
            editable : true,
            hidden : true,
            width : 100
        }, {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName_OR_customerProfile.trueName',
            width : 100
        }, {
            label : '参考来源',
            name : 'referenceSource',
            hidden : true,
            width : 100
        }, {
            label : '参考凭证号',
            name : 'referenceVoucher',
            hidden : true,
            width : 100
        }, {
            label : '标题',
            name : 'title',
            hidden : true,
            width : 100
        }, {
            label : '整单金额',
            name : 'totalAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '已付金额',
            name : 'payedAmount',
            formatter : 'currency',
            width : 100
        },  {
            label : '物流公司',
            editable:true,
            name : 'logistics.id',
            align : 'center',
            stype : 'select',
            editoptions : {
                value : Biz.getLogisticsDatas()
            },
            width : 200
        }, {
            label : '快递单号',
            name : 'logisticsNo',
            editable : true,
            width : 100
        }, {
            label : '实际运费',
            name : 'logisticsAmount',
            editable : true,
            width : 100
        } ],
        editcol : 'voucher',
        addable : false,
        editurl:WEB_ROOT + "/myt/sale/sale-delivery!doSave",
        fullediturl : WEB_ROOT + "/myt/sale/sale-delivery!viewDelivery",
        subGrid : true,
        sortname: 'reserveDeliveryTime',
        sortorder: 'asc',
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/sale/sale-delivery!saleDeliveryDetails?id=" + row_id,
                colModel : [ {
                    label : '销售（发货）商品',
                    name : 'commodity.display',
                    align : 'left'
                }, {
                    label : '单位',
                    name : 'measureUnit',
                    editable : true,
                    width : 60
                }, {
                    label : '数量',
                    name : 'quantity',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '销售单价',
                    name : 'price',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '是否赠品',
                    name : 'gift',
                    width : 50,
                    edittype : 'checkbox'

                }, {
                    label : '原价金额',
                    name : 'originalAmount',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '折扣率(%)',
                    name : 'discountRate',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '折扣额',
                    name : 'discountAmount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '折后金额',
                    name : 'amount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '税率(%)',
                    name : 'taxRate',
                    width : 50,
                    hidden : true,
                    formatter : 'number'

                }, {
                    label : '税额',
                    name : 'taxAmount',
                    width : 60,
                    hidden : true,
                    formatter : 'currency'
                }, {
                    label : '含税总金额',
                    name : 'commodityAndTaxAmount',
                    width : 80,
                    formatter : 'currency'
                }, {
                    label : '发货仓库',
                    name : 'storageLocation.id',
                    width : 80,
                    stype : 'select',
                    formatter : 'select',
                    searchoptions : {
                        value : Biz.getStockDatas()
                    }
                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
});
