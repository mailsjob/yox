$(function() {
    $(".form-myt-sale-sale-return-apply-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);

            $form.find('input[name="agentPartner.display"]').treeselect({
                url : WEB_ROOT + "/myt/partner/partner!treeDatas",
                callback : {
                    onSingleClick : function(event, treeId, treeNode) {
                        $form.setFormDatas({
                            'agentPartner.id' : Util.startWith(treeNode.id, "-") ? "" : treeNode.id,
                            'agentPartner.display' : Util.startWith(treeNode.id, "-") ? "" : treeNode.name,
                            'sellerName' : treeNode.contactPerson,
                            'sellerDetailAddr' : treeNode.address,
                            'sellerPhone' : treeNode.contactPhone        
                        }, true);
                    },
                    onClear : function(event) {
                        $form.setFormDatas({
                            'agentPartner.id' : '',
                            'agentPartner.display' : ''
                        }, true);
                    }
                }
            });
        }
    });
});
