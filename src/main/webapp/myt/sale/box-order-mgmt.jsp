<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-box-order-inputBasic"
	action="${base}/myt/sale/box-order!cancelOrder" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<button class="btn default btn-cancel" type="button">取消</button>
		<button type="submit" class="btn red">
			<i class="fa fa-check"></i> 订单取消
		</button>
		<s3:button  type="submit" cssClass="btn green" data-form-action="${base}/myt/sale/box-order!doDelivery"
			 data-confirm="确认订单发货操作？">
			<s:property value="'订单发货'" />
		</s3:button>
		<s3:button  type="submit" cssClass="btn blue" data-form-action="${base}/myt/sale/box-order!recvConfirm"
			 data-confirm="确认订单完成收货？">
			<s:property value="'订单完成收货'" />
		</s3:button>
		<s3:button  type="submit" cssClass="btn green" data-form-action="${base}/myt/sale/box-order!successClose"
			 data-confirm="确认订单完结操作？">
			<s:property value="'订单完结'" />
		</s3:button>
        <s3:button  type="submit" cssClass="btn green" data-form-action="${base}/myt/sale/box-order!free"
             data-confirm="确认免单操作？">
            <s:property value="'免单'" />
        </s3:button>
        <s3:button  type="submit" cssClass="btn green" data-form-action="${base}/myt/sale/box-order!freeCancle"
             data-confirm="确认取消免单操作？">
            <s:property value="'取消免单'" />
        </s3:button>
        <s3:button  type="submit" cssClass="btn green" data-form-action="${base}/myt/sale/box-order!backCommodity"
             data-confirm="确认订单退款操作？">
            <s:property value="'订单退款'" />
        </s3:button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">订单操作汇总</label>
					<div class="controls">
						<s:textarea name="operationEvent" rows="3" readonly="true"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</form>

<script src="${base}/myt/es/agent/sale-delivery-process.js" />
<%@ include file="/common/ajax-footer.jsp"%>