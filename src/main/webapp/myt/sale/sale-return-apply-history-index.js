$(function() {
    $(".grid-myt-sale-sale-return-apply-history-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/sale-return-apply-history!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '关联退换货申请凭证',
            name : 'saleReturnApply.voucher',
            index : 'saleReturnApply.voucher',
            width : 200,
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '处理时间',
            name : 'processTime',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '操作人',
            name : 'operater',
            width : 100,
            editable: true,
            align : 'center'
        }, {
            label : '审核状态',
            name : 'processResult',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('postAuditResultEnum')
            },
            width : 80,
            editable: true,
            align : 'center'
        }, {
            label : '当前步奏',
            name : 'applyHistoryStatus',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('returnApplyStatusEnum')
            },
            width : 80,
            editable: true,
            align : 'center'
        }, {
            label : '操作备注',
            name : 'processComment',
            width : 400,
            editable: true,
            align : 'left'
        }],
        postData: {
           "search['FETCH_saleReturnApply']" : "LEFT"
        }
    	/*editurl : WEB_ROOT + '/myt/sale/sale-return-apply-history!doSave',
        delurl : WEB_ROOT + '/myt/sale/sale-return-apply-history!doDelete',
        fullediturl : WEB_ROOT + '/myt/sale/sale-return-apply-history!inputTabs'*/
    });
});