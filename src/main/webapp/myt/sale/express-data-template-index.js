$(function() {
    $(".grid-myt-sale-express-data-template").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/express-data-template!findByPage',
        colModel : [ {
            label : '模板名称（唯一）',
            name : 'templateName',
            editable:true,
            width : 100
        }, {
            label : '寄件人姓名',
            name : 'senderName',
            editable:true,
            width : 100
        }, {
            label : '寄件公司',
            name : 'senderCompany',
            editable:true,
            width : 100
        }, {
            label : '始发地',
            name : 'senderOrigin',
            editable:true,
            width : 100
        }, {
            label : '寄件人电话',
            name : 'senderMobile',
            editable:true,
            align : 'center',
          width : 100
        }, {
            label : '寄件人地址',
            name : 'senderAddr',
            editable:true,
            width : 140
        }],
        
        editcol : 'templateName',
        editurl : WEB_ROOT + "/myt/sale/express-data-template!doSave",
        delurl : WEB_ROOT + "/myt/sale/express-data-template!doDelete",
        fullediturl : WEB_ROOT + "/myt/sale/express-data-template!inputTabs",
    });
});
