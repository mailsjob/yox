<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-myt-sale-sale-delivery-saleAndReturn" action="#">
						<div class="form-group">
							<input type="text" name="kw" class="form-control input-large"
								placeholder="凭证号、商品SKU、商品标题...">
						</div>
						<div class="form-group">
							<input type="text" name="date" class="form-control input-medium input-daterangepicker"
								placeholder="销售日期">
						</div>
						<div class="form-group">
							<div class="btn-group">
								<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
									单据选择 <i class="fa fa-angle-down"></i>
								</button>
								<div class="dropdown-menu hold-on-click dropdown-checkboxes">
									<s:checkboxlist name="voucherType" list="#{'XS':'销售单','TH':'退换货单'}"
										value="{'XS','TH'}" />
								</div>
							</div>
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-sale-sale-delivery-saleAndReturn"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${base}/myt/sale/sale-delivery-saleAndReturn.js" />
<%@ include file="/common/ajax-footer.jsp"%>
