<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search-init"
						data-grid-search=".grid-myt-sale-selectSaleDelivery" action="#">
						<div class="form-group">
							<input type="text" name="search['CN_voucher_OR_receivePerson']" class="form-control input-large"
								placeholder="销售单号、收货人...">
						</div>
						<div class="form-group">
							<input type="text" name="search['BT_createdDate']" class="form-control input-medium input-daterangepicker"
								placeholder="创建日期">
						</div>
						
						<div class="form-group">
							<label> <s:checkbox name="search['EQ_voucherUser.signinid']" fieldValue="%{signinUsername}" />只显示我的
							</label>
						</div>
						<div class="form-group">
							<div class="btn-group">
								<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
									凭证状态 <i class="fa fa-angle-down"></i>
								</button>
								<div class="dropdown-menu hold-on-click dropdown-checkboxes">
									<s:checkboxlist name="search['IN_voucherState']" list="#application.enums.voucherStateEnum"
										value="#application.enums.voucherStateEnum.keys.{? #this.name()!='REDW'}" />
								</div>
							</div>
						</div>
						<button class="btn default hidden-inline-xs" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-sale-selectSaleDelivery"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function() {
    $(".grid-myt-sale-selectSaleDelivery").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/sale-delivery!findByPage',
        colModel : [ {
            label : '凭证号',
            name : 'voucher',
            align : 'center',
            width : 120
        }, {
            label : '经办人',
            name : 'voucherUser.display',
            index : 'voucherUser.signinid',
            align : 'center',
            width : 60
        }, {
            label : '发货类型',
            stype : 'select',
            name : 'deliveryType',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('deliveryTypeEnum')
            },
            cellattr : function(rowId, cellValue, rawObject, cm, rowdata) {
                if (rowdata['deliveryType'] == 'READY') {
                    return "class='badge-warning'"
                }
            },
            width : 60
        }, {
            label : '库存模式',
            stype : 'select',
            name : 'storageMode',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('storageModeEnum')
            },
           	width : 60
        }, {
            label : '预约发货日期',
            name : 'reserveDeliveryTime',
            stype : 'date',
            editable:true,
            width : 120,
            cellattr : function(rowId, cellValue, rowdata, cm) {
               if(rowdata.reserveDeliveryTime){
                   var today=new Date();
                   var month;
                   if(today.getMonth()<9){
                        month="0"+(today.getMonth()+1);
                      
                   }else{
                       month=today.getMonth()+1;
                   }
                   var todayLabel=today.getFullYear()+"-"+month+"-"+today.getDate();
                   if (cellValue==todayLabel) {
                       return "class='badge-warning'"
                   } else if (cellValue > todayLabel) {
                       return "class='badge-success'"
                   } else{
                       return "class='badge-danger'"
                   }
               }
               return ;
            }
        }, {
            label : '提交时间',
            name : 'submitDate',
            stype : 'date',
            hidden : true,
            width : 100
        }, {
            label : '最近操作',
            name : 'lastOperationSummary',
            width : 120
        }, {
            label : '凭证日期',
            name : 'voucherDate',
            hidden : true,
            stype : 'date',
            width : 100
        }, {
            label : '拣货人',
            name : 'pickUser.display',
            hidden : true,
            width : 100
        }, {
            label : '拣货时间',
            name : 'pickTime',
            hidden : true,
            width : 100
        }, {
            label : '收取客户运费',
            name : 'chargeLogisticsAmount',
            editable : true,
            hidden : true,
            width : 100
        }, {
            label : '整单金额',
            name : 'totalAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '已付金额',
            name : 'payedAmount',
            formatter : 'currency',
            hidden : true,
            width : 100
        }, {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName_OR_customerProfile.trueName',
            width : 100
        }, {
            label : '经办部门',
            name : 'voucherDepartment.display',
            index : 'voucherDepartment.code_OR_voucherDepartment.title',
            hidden : true,
            width : 100
        }, {
            label : '参考来源',
            name : 'referenceSource',
            hidden : true,
            width : 100
        }, {
            label : '参考凭证号',
            name : 'referenceVoucher',
            hidden : true,
            width : 100
        }, {
            label : '标题',
            name : 'title',
            hidden : true,
            width : 100
        }, {
            label : '收货人',
            name : 'receivePerson',
            width : 100
        }, {
            label : '物流公司',
            name : 'logistics.id',
            align : 'center',
            stype : 'select',
            editoptions : {
                value : Biz.getLogisticsDatas()
            },
            width : 200
        }, {
            label : '快递单号',
            name : 'logisticsNo',
            width : 100
        }, {
            label : '电话',
            name : 'mobilePhone',
            width : 100
        }, {
            label : '收货地址',
            name : 'deliveryAddr',
            width : 250
        }, {
            label : '实际运费',
            name : 'logisticsAmount',
            hidden : true,
            width : 100
        } ],
        editcol : 'voucher',
        addable : false,
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/sale/sale-delivery!saleDeliveryDetails?id=" + row_id,
                colModel : [ {
                    label : '行项号',
                    name : 'subVoucher',
                    align : 'center',
                    width : 50
                }, {
                    name : 'commodity.sku',
                    hidden : true
                }, {
                    name : 'commodity.id',
                    hidden : true
                }, {
                    name : 'commodity.barcode',
                    hidden : true
                }, {
                    label : '销售（发货）商品',
                    name : 'commodity.display',
                    align : 'left'
                }, {
                    label : '单位',
                    name : 'measureUnit',
                    editable : true,
                    width : 60
                }, {
                    label : '数量',
                    name : 'quantity',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '销售单价',
                    name : 'price',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '是否赠品',
                    name : 'gift',
                    width : 50,
                    edittype : 'checkbox'

                }, {
                    label : '原价金额',
                    name : 'originalAmount',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '折扣率(%)',
                    name : 'discountRate',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '折扣额',
                    name : 'discountAmount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '折后金额',
                    name : 'amount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '税率(%)',
                    name : 'taxRate',
                    width : 50,
                    hidden : true,
                    formatter : 'number'

                }, {
                    label : '税额',
                    name : 'taxAmount',
                    width : 60,
                    hidden : true,
                    formatter : 'currency'
                }, {
                    label : '含税总金额',
                    name : 'commodityAndTaxAmount',
                    width : 80,
                    formatter : 'currency'
                }, {
                    label : '发货仓库',
                    name : 'storageLocation.id',
                    width : 80,
                    stype : 'select',
                    formatter : 'select',
                    searchoptions : {
                        value : Biz.getStockDatas()
                    }
                }, {
                    label : '批次号',
                    name : 'batchNo',
                    width : 80
                }, {
                    label : '批次过期日期',
                    name : 'expireDate',
                    width : 80
                } ],
                loadonce : true,
                multiselect : true,
                operations : function(itemArray) {
                    var $grid = $(this);
                    var $select = $('<li><a href="javascript:;"><i class="fa fa-check-square-o"></i> 选取</a></li>');
                    $select.children("a").bind("click", function(e) {
                        e.preventDefault();
                        var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                        var $dialog = $grid.closest(".modal");
                        $dialog.modal("hide");
                        var callback = $dialog.data("callback");
                        if (callback) {
                            var $prow = $grid.closest('.ui-subgrid').prev();
                            var $pgrid = $prow.closest('.ui-jqgrid-btable');
                            var prowid = $prow.attr("id");
                            var data = {
                                master : $pgrid.jqGrid("getRowData", prowid),
                                rows : $grid.jqGrid("getSelectedRowdatas")
                            };
                            //alert(data.master['boxOrder.receivePerson']);
                            console.info(data);
                            callback.call($grid, data);
                        }
                    });
                    itemArray.push($select);
                }
            });
        }
    });
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>