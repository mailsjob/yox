<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form action="#" method="get" class="form-inline form-validation  form-search-init"
			data-grid-search=".grid-myt-sale-box-order-detail-early-warning">
			<div class="input-group">
				<div class="form-group">
					<input type="text"
						name="search['CN_boxOrder.orderSeq_OR_boxOrder.receivePerson_OR_boxOrder.customerProfile.trueName_OR_boxOrder.customerProfile.nickName']"
						class="form-control input-medium" placeholder="订单号、客户、收货人..." />
				</div>
				<div class="form-group">
					<div class="btn-group">
						<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
							行项状态 <i class="fa fa-angle-down"></i>
						</button>
						<div class="dropdown-menu hold-on-click dropdown-checkboxes">
							<s:checkboxlist name="search['IN_orderDetailStatus']" list="#application.enums.boxOrderDetailStatusEnum"
								value="#application.enums.boxOrderDetailStatusEnum.keys.{? #this.name()!='S90CANCLE'
										&&#this.name()!='S70CLS'&&#this.name()!='S60R'&&#this.name()!='S50DF'}" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group input-medium">
						<input type="text" name="search['BT_reserveDeliveryTime']"
							class="form-control input-medium input-daterangepicker grid-param-data" placeholder="预约发货日期" pattern="">
					</div>
				</div>
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
				<button class="btn default" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-myt-sale-box-order-detail-early-warning"></table>
	</div>
</div>
<script src="${base}/myt/sale/box-order-detail-earlyWarning.js" />
<%@ include file="/common/ajax-footer.jsp"%>
