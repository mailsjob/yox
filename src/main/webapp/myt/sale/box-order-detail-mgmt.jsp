<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-box-order-detail-mgmt"
	action="${base}/myt/sale/box-order-detail!recvConfirm" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<button class="btn default btn-cancel" type="button">取消</button>
		<button type="submit" class="btn red">
			<i class="fa fa-check"></i> 行项完成收货
		</button>
		<s3:button  type="submit" cssClass="btn green" data-form-action="${base}/myt/sale/box-order-detail!successClose"
			 data-confirm="确认行项完结操作？">
			<s:property value="'行项完结'"/>
		</s3:button>
	</div>
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">行项操作汇总</label>
					<div class="controls">
						<s:textarea name="operationEvent" rows="3" readonly="true"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</form>

<script src="${base}/myt/es/agent/sale-delivery-process.js" />
<%@ include file="/common/ajax-footer.jsp"%>