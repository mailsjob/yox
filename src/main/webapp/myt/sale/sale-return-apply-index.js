$(function() {
    $(".grid-myt-sale-sale-return-apply-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/sale-return-apply!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true                          
        }, {
            label : '唯一凭证号',
            name : 'voucher',
            width : 128,
            editable: true,
            align : 'left'
        }, /*{
            label : '商品标题',
            name : 'commodityTitle',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '退货数量',
            name : 'quantity',
            width : 160,
            formatter: 'number',
            editable: true,
            align : 'right'
        },*/ {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName',
            width : 200,
            width : 200,
            editable: true,
            align : 'center'
        }, {
            label : '退货类型',
            name : 'processType',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('processTypeEnum')
            },
            width : 150,
            editable: true,
            align : 'center'
        }, {
            label : '收货人',
            name : 'saleReturnName',
            width : 200,
            editable: true,
            align : 'center'
        }, {
            label : '提交审核状态',
            name : 'processResult',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('postAuditResultEnum')
            },
            width : 150,
            editable: true,
            align : 'center'
        }, {
            label : '用户备注',
            name : 'description',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '最后审核结果',
            name : 'lastAuditResult',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('lastAuditResultEnum')
            },
            width : 150,
            editable: true,
            align : 'center'
        }, {
            label : '最近操作摘要',
            name : 'lastOperationSummary',
            width : 200,
            editable: true,
            align : 'left'
        }, {
            label : '流程当前任务节点',
            name : 'activeTaskName',
            width : 150,
            editable: true,
            align : 'left'
        }, {
            label : '有检测报告',
            name : 'hasReport',
            width : 150,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }, {
            label : '有发票',
            name : 'hasInvoice',
            width : 100,
            formatter : 'checkbox',
            editable: true,
            align : 'center'
        }],
        editurl : WEB_ROOT + '/myt/sale/sale-return-apply!doSave',
        delurl : WEB_ROOT + '/myt/sale/sale-return-apply!doDelete',
        fullediturl : WEB_ROOT + '/myt/sale/sale-return-apply!inputTabs',
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
               
                url : WEB_ROOT + '/myt/sale/sale-return-apply!boxOrderDetailCommodities?id=' + row_id,
                colModel : [ {
                    label : '退换货商品',
                    name : 'commodity.display',
                    width : 250
                }, {
                    label : '销售单价',
                    name : 'price',
                    editable : true,
                    formatter : 'currency',
                    width : 100
                }, {
                    label : '销售数量',
                    name : 'quantity',
                    editable : true,
                    formatter : 'number',
                    width : 100
                }, {
                    label : '原始金额',
                    name : 'originalAmount',
                    formatter : 'currency',
                    width : 100
                }, {
                    label : '折扣金额',
                    name : 'discountAmount',
                    formatter : 'currency'
                }, {
                    label : '金额小计',
                    name : 'actualAmount',
                    formatter : 'currency'
                }, /*
                     * { label : '已发货数量', name : 'deliveriedQuantity', formatter :
                     * 'number', width : 80 },
                     */{
                    label : '发货仓库',
                    name : 'storageLocation.id',
                    width : 120,
                    stype : 'select',
                    searchoptions : {
                        value : Biz.getStockDatas()
                    }
                }, /*{
                    label : '锁定库存量',
                    name : 'salingLockedQuantity',
                    formatter : 'number',
                    width : 80
                }, */{
                    label : '改价备注',
                    name : 'modifierMemo',
                    width : 200
                } ],
                cmTemplate : {
                    sortable : false
                },
                rowNum : -1,
                toppager : false,
                filterToolbar : false,
                multiselect : false
            });
        }
    });
});
