<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form action="#" method="get" class="form-inline form-validation form-search form-search-init"
						data-grid-search=".grid-myt-sale-box-order">
						<div class="form-group">
							<label class="sr-only">订单号、客户</label> <input type="text" name="search['CN_orderSeq_OR_customerProfile.nickName_OR_receivePerson']"
								class="form-control input-xlarge" placeholder="订单号、客户、收货人..." />
						</div>
						<div class="form-group">
							<div class="btn-group">
								<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown">
									状态 <i class="fa fa-angle-down"></i>
								</button>
								<div class="dropdown-menu hold-on-click dropdown-checkboxes">
									<s:checkboxlist name="search['IN_orderStatus']" list="#application.enums.boxOrderStatusEnum"
										value="#application.enums.boxOrderStatusEnum.keys.{? #this.name()!='S90CANCLE'}" />
								</div>
							</div>
						</div>
						<button class="btn green" type="submmit">
							<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
						</button>
						<button class="btn default" type="reset">
							<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
						</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-sale-box-order"></table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function() {
    $(".grid-myt-sale-box-order").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/box-order!findByPage',
        colModel : [ {
            name : 'id',
            hidden : true
        }, {
            label : '下单时间',
            name : 'orderTime',
            align : 'left',
            sorttype : 'date'
        }, {
            label : '状态',
            name : 'orderStatus',
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('boxOrderStatusEnum')
            },
            width : 80
        }, {
            label : '订单号',
            name : 'orderSeq',
            width : 200,
            align : 'left',
            formatter : function(cellValue, options, rowdata, action) {
                var html = cellValue;
                if (rowdata.circle) {
                    html += '<span class="badge badge-info">周</span>'
                }
                if (rowdata.splitPayMode != 'NO') {
                    html += '<span class="badge badge-success">分</span>'
                }
                return html;
            }
        }, {
            label : '下单客户',
            name : 'customerProfile.display',
            index : 'customerProfile.trueName_OR_customerProfile.nickName',
            align : 'left',
            width : 80,
            formatter : function(cellValue, options, rowdata, action) {
                var url = "${base}/myt/customer/customer-profile!view?id=" + rowdata.customerProfile.id;
                return '<a href="' + url + '" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
            }
        }, {
            label : '收货人',
            name : 'receivePerson',
            width : 80,
            align : 'left'
        }, {
            label : '原始金额',
            name : 'originalAmount',
            width : 80,
            formatter : 'currency',
            editable : true,
            editoptions : {
                dataInit : function(elem) {
                    var $elem = $(elem);
                    $elem.attr("readonly", true);
                }
            }
        }, {
            label : '折扣金额',
            name : 'discountAmount',
            editable : true,
            formatter : 'currency',
            editrules : {
                required : true,
            },
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function() {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            }
        }, {
            label : '应付金额',
            name : 'actualAmount',
            editable : true,
            formatter : 'currency',
            editrules : {
                required : true,
            },
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function() {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            }
        }, {
            label : '已付总金额',
            width : 70,
            name : 'actualPayedAmount',
            formatter : 'currency'
        }, {
            label : '改价备注',
            name : 'modifierMemo',
            editable : true,
            editrules : {
                required : true
            },
            width : 200
        }],
        postData : {
            "search['FETCH_customerProfile']" : "INNER",
            "search['FETCH_intermediaryCustomerProfile']" : "LEFT"
        },
        addable : false,
        inlineNav : {
            add : false
        },
        multiselect : false,
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + '/myt/sale/box-order!boxOrderDetails?id=' + row_id,
                colModel : [ {
                    label : '行项号',
                    name : 'sn',
                    align : 'center',
                    width : 60
                }, {
                    label : '预约发货日期',
                    name : 'reserveDeliveryTime',
                    editable : true,
                    align : 'center',
                    stype : 'date'
                }, {
                    label : '行项状态',
                    name : 'orderDetailStatus',
                    width : 80,
                    align : 'center',
                    stype : 'select',
                    searchoptions : {
                        value : Util.getCacheEnumsByType('boxOrderDetailStatusEnum')
                    }
                }, {
                    label : '原始金额',
                    name : 'originalAmount',
                    formatter : 'currency',
                    editable : true,
                    editoptions : {
                        dataInit : function(elem) {
                            var $elem = $(elem);
                            $elem.attr("readonly", true);
                        }
                    }
                }, {
                    label : '折扣金额',
                    name : 'discountAmount',
                    editable : true,
                    formatter : 'currency',
                    editrules : {
                        required : true,
                    },
                    editoptions : {
                        dataInit : function(elem) {
                            var $grid = $(this);
                            var $elem = $(elem);
                            $elem.keyup(function() {
                                $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                            });
                        }
                    }
                }, {
                    label : '应付金额',
                    name : 'actualAmount',
                    editable : true,
                    formatter : 'currency',
                    editrules : {
                        required : true,
                    },
                    editoptions : {
                        dataInit : function(elem) {
                            var $grid = $(this);
                            var $elem = $(elem);
                            $elem.keyup(function() {
                                $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                            });
                        }
                    }
                }, {
                    label : '已付金额',
                    name : 'payAmount',
                    formatter : 'currency'
                }, {
                    label : '改价备注',
                    name : 'modifierMemo',
                    editable : true,
                    editrules : {
                        required : true,
                    },
                    width : 200
                } ],
                cmTemplate : {
                    sortable : false
                },
                rowNum : -1,
                multiselect : false,
                toppager : false,
                subGrid : true,
                subGridRowExpanded : function(subgrid_id, row_id) {
                    Grid.initSubGrid(subgrid_id, row_id, {
                        url : WEB_ROOT + '/myt/sale/box-order-detail!boxOrderDetailCommodities?id=' + row_id,
                        colModel : [ {
                            label : '商品',
                            name : 'commodity.display',
                            width : 250
                        }, {
                            label : '销售单价',
                            name : 'price',
                            editable : true,
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            formatter : 'currency',
                            width : 100
                        }, {
                            label : '销售数量',
                            name : 'quantity',
                            editable : true,
                            formatter : 'number',
                            width : 100,
                            editrules : {
                                number : true
                            },
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid);
                                    });
                                }
                            }
                        }, {
                            label : '原始金额',
                            name : 'originalAmount',
                            formatter : 'currency',
                            editable : true,
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            width : 100
                        }, {
                            label : '折扣金额',
                            name : 'discountAmount',
                            editable : true,
                            editrules : {
                                required : true
                            },
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            formatter : 'currency'
                        }, {
                            label : '金额小计',
                            name : 'actualAmount',
                            editable : true,
                            editrules : {
                                required : true
                            },
                            editoptions : {
                                dataInit : function(elem) {
                                    var $grid = $(this);
                                    var $elem = $(elem);
                                    $elem.keyup(function() {
                                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                                    });
                                }
                            },
                            formatter : 'currency'
                        }, {
                            label : '已发货数量',
                            name : 'deliveriedQuantity',
                            formatter : 'number',
                            width : 80
                        }, {
                            label : '发货仓库',
                            name : 'storageLocation.id',
                            width : 120,
                            stype : 'select',
                            searchoptions : {
                                value : Biz.getStockDatas()
                            }
                        }, {
                            label : '锁定库存量',
                            name : 'salingLockedQuantity',
                            formatter : 'number',
                            width : 80
                        }, {
                            label : '改价备注',
                            name : 'modifierMemo',
                            editable : true,
                            editrules : {
                                required : true,
                            },
                            width : 200
                        } ],
                        cmTemplate : {
                            sortable : false
                        },
                        rowNum : -1,
                        toppager : false,
                        multiselect : false
                    });
                }
            });
        },
        onSelectRow : function(id) {
            var $grid = $(this);
            var $dialog = $grid.closest(".modal");
            $dialog.modal("hide");
            var callback = $dialog.data("callback");
            if (callback) {
                var rowdata = $grid.jqGrid("getRowData", id);
                rowdata.id = id;
                callback.call($grid, rowdata);
            }
        }
    });
});
</script>
<%@ include file="/common/ajax-footer.jsp"%>