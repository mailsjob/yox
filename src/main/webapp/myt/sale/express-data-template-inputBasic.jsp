<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-express-data-template-inputBasic"
	action="${base}/myt/sale/express-data-template!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<button class="btn blue" type="submit" data-grid-reload=".grid-myt-sale-express-data-template">
				<i class="fa fa-check"></i> 保存
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
		</div>
		
	</div>
	<div class="form-body control-label-sm">
		<div class="row">
						<div class="col-md-8 ">
							<div class="form-group">
								<label class="control-label">模板名称</label>
								<div class="controls">
									<s:textfield name="templateName" readonly="%{notNew}" />名称唯一，一经创建不能更改
								</div>
							</div>
						</div>
					</div>
			<div class="row">
				<div class="col-md-6 ">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">寄件人姓名</label>
								<div class="controls">
									<s:textfield name="senderName"/>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">始发地</label>
								<div class="controls">
									<s:textfield name="senderOrigin"/>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label class="control-label">寄件公司</label>
								<div class="controls">
								
									<s:textfield name="senderCompany" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label class="control-label">寄件人地址</label>
								<div class="controls">
								<s:textarea name="senderAddr" rows="2"></s:textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">寄件人电话</label>
								<div class="controls">
									<s:textfield name="senderMobile"/>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">寄件人邮编</label>
								<div class="controls">
									<s:textfield name="senderPostcode"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 ">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">收件人姓名</label>
								<div class="controls">
									<s:textfield name="receiveName"/>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">目的地</label>
								<div class="controls">
									<s:textfield name="receiveOrigin"/>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label class="control-label">收件公司</label>
								<div class="controls">
									<s:textfield name="receiveCompany"/>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 ">
							<div class="form-group">
								<label class="control-label">收件人地址</label>
								<div class="controls">
									<s:textarea name="receiveAddr" rows="2" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">收件人电话</label>
								<div class="controls">
									<s:textfield name="receiveMobile"/>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">收件人邮编</label>
								<div class="controls">
									<s:textfield name="receivePostcode"/>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
		
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-sale-express-data-template">
				<i class="fa fa-check"></i> 保存
			</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>

</form>
<script src="${base}/myt/sale/sale-delivery-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
