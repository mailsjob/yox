<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-sale-return-inputBasic"
	action="${base}/myt/sale/sale-return!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
				data-grid-reload=".grid-myt-sale-sale-return">
				<i class="fa fa-check"></i>
				<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
			</s3:button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<button class="btn green" type="button" data-toggle="panel"
				data-url="${base}/myt/finance/payment-apply!bpmNew?bizVoucher=<s:property value='voucher'/>
				&bizVoucherType=TH&bizTradeUnitId=<s:property value='customerProfile.bizTradeUnitId'/>
				&bizTradeUnitName=<s:property value='customerProfile.display'/>">创建退款单</button>
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">凭证状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.voucherStateEnum[voucherState]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 ">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:textfield name="voucher" readonly="%{notNew}" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:select name="voucherUser.id" list="usersMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:select name="voucherDepartment.id" list="departmentsMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<s3:datetextfield name="voucherDate" format="date" current="true" readonly="true"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">销售客户</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-customer-profile"></i>
							<s:textfield name="customerProfile.display" disabled="%{'DRAFT'!=voucherState.name()}" />
							<s:hidden name="customerProfile.id" disabled="%{'DRAFT'!=voucherState.name()}" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">关联订单号</label>
					<div class="controls">
						<s:textfield name="referenceVoucher" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">关联订单来源</label>
					<div class="controls">
						<s:select name="referenceSource" list="referenceSourceMap" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">销售单名称</label>
					<div class="controls">
						<s:textfield name="title" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">备注说明</label>
					<div class="controls">
						<s:textfield name="memo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-sale-sale-return-inputBasic" data-grid="items"
					data-readonly="<s:property value="%{voucherState.name()=='DRAFT'?false:true}"/>"
					data-pk='<s:property value="#parameters.id"/>' data-clone='<s:property value="#parameters.clone"/>'></table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">立即提交</label>
					<div class="controls text-warning">
						<s:checkbox name="voucherState" fieldValue="POST" disabled="%{'DRAFT'!=voucherState.name()}" label="提交" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">商品总成本</label>
					<div class="controls">
						<s:textfield name="totalCommodityAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">折损额</label>
					<div class="controls">
						<s:textfield name="totalLossAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">入库成本</label>
					<div class="controls">
						<s:textfield name="totalCostAmount" readonly="true" />
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品成本</label>
					<div class="controls">
						<s:textfield name="commodityAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品售额</label>
					<div class="controls">
						<s:textfield name="totalSaleAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">补偿客户运费</label>
					<div class="controls">
						<s:textfield name="deliveryAmount" readonly="true"/>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">应退客户费用</label>
					<div class="controls">
						<s:textfield name="totalReturnAmount" readonly="true"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">退货成本</label>
					<div class="controls">
						<s:textfield name="totalCostAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<s3:button disabled="%{disallowUpdate!=null}" type="submit" cssClass="btn blue"
			data-grid-reload=".grid-myt-sale-sale-delivery">
			<i class="fa fa-check"></i>
			<s:property value="%{disallowUpdate!=null?disallowUpdate:'保存'}" />
		</s3:button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/sale/sale-return-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>
