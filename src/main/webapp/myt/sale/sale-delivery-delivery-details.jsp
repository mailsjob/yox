<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<s:if test="%{model==null}">
	<div class="alert alert-warning">
		<strong>提示：</strong> 输入销售单号未找到匹配数据，请检查输入！
	</div>
</s:if>
<s:elseif test="%{voucherState.name()=='REDW'}">
	<div class="alert alert-warning">
		<strong>提示：</strong> 此销售单<font color="red">已冲销</font>
	</div>
</s:elseif>
<s:elseif test="%{auditDate==null}">
	<div class="alert alert-warning">
		<strong>提示：</strong> 此销售单<font color="red">请先审核，再拣货</font>
	</div>
</s:elseif>
<s:elseif test="%{pickTime==null}">
	<div class="alert alert-warning">
		<strong>提示：</strong> 输入的销售单<font color="red">还未拣货，请先拣货</font>！
	</div>
</s:elseif>
<%-- <s:elseif test="%{deliveryTime!=null}">
	<div class="alert alert-warning">
		<strong>提示：</strong> 输入的销售单<font color="red">已经发货完毕</font>！
	</div>
</s:elseif> --%>
<s:else>
	<form class="form-horizontal form-bordered form-label-stripped form-validation form-sale-delivery-delivery-details"
		action="${base}/myt/sale/sale-delivery!deliverySave" method="post">
		<s:hidden name="id"/>
		<s:hidden name="version" />
		<s:token />
		<div class="form-actions">
			<button class="btn blue"  type="submit"  data-ajaxify-reload="div-sale-delivery-delivery-details">
				<i class="fa fa-check"></i> 发货
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<a data-pk="<s:property value='id'/>" class="btn yellow btn-send-delivery-notify" href="javascript:;"><i class="fa fa-indent"> 发货通知邮件和短信</i></a>
		</div>
		<div class="form-body control-label-sm">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">快递单号</label>
						<div class="controls">
							<s:textfield name="logisticsNo" readonly="%{deliveryTime}"/>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">快递费用</label>
						<div class="controls">
							<s:textfield name="logisticsAmount"/>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6" id="logistics">
					<div class="form-group">
						<label class="control-label">快递公司</label>
						<div class="controls">
							<s:select name="logistics.id" list="logisticsNameMap" disabled="%{deliveryTime}" 
								data-profile-param="default_sale_delivery_delivery_logistics" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">预约发货日期</label>
						<div class="controls">
							<p class="form-control-static">
								<span class="badge-info">&nbsp;<s:date name="reserveDeliveryTime" format="yyyy-MM-dd" />&nbsp;
								</span>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">拣货时间</label>
						<div class="controls">
							<p class="form-control-static">
								<s:date name="pickTime" format="yyyy-MM-dd HH:mm:ss" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">拣货人</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="pickUser.display" />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">发货时间</label>
						<div class="controls">
							<p class="form-control-static">
								<s:property value="deliveryTime"/>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-actions left">
			<button class="btn blue"   type="submit">
				<i class="fa fa-check" data-ajaxify-reload="_closest-ajax-container"></i> 发货
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
			<a data-pk="<s:property value='id'/>" class="btn yellow btn-send-delivery-notify" href="javascript:;"><i class="fa fa-indent"> 发货通知邮件和短信</i></a>
		</div>
	</form>
	<script src="${base}/myt/sale/sale-delivery-delivery-details.js" />
</s:else>
<%@ include file="/common/ajax-footer.jsp"%>
