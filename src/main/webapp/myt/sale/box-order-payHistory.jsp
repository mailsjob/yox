<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-sale-box-order-payHistory" data-grid="table"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-sale-box-order-payHistory").data("gridOptions", {
            url : "${base}/myt/sale/box-order!findPayHistory?id=<s:property value='#parameters.id'/>",
            colModel : [ {
                label : '状态',
                name : 'processStatus',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('processStatusEnum')
                },
                width : 80,
                align : 'center'
            }, {
                label : '付款类型',
                name : 'payType',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('payTypeEnum')
                },
                width : 80,
                align : 'center'
            }, {
                label : '交易金额',
                name : 'totalFee',
                width : 60,
                formatter : 'currency',
                align : 'right'
            }, {
                label : '买家账户',
                name : 'buyerEmail',
                width : 120,
                align : 'center'
            }, {
                label : '交易信息',
                name : 'processInfo',
                index : 'processInfo',
                width : 100,
                align : 'left'
            },{
                label : '付款流水号',
                name : 'paySeq',
                width : 200,
                align : 'left'
            }, {
                label : '上传支付网关唯一号码',
                name : 'outTradeNo',
                width : 200,
                align : 'center'
            }, {
                label : '支付网关交易号',
                name : 'tradeNo',
                width : 200,
                align : 'left'
            }],
            multiselect : false
        });
    });
    
</script>
<%@ include file="/common/ajax-footer.jsp"%>