<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row">
	<table class="grid-sale-sale-return-select-order" data-grid="table"></table>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-sale-sale-return-select-order").data("gridOptions", {
            url : '${base}/myt/sale/sale-delivery!findByPage',
            colModel : [ {
                label : '凭证号',
                name : 'voucher',
                width : 140
            }, {
                label : '记账日期',
                name : 'voucherDate',
                stype : 'date',
                width : 100
            }, {
                label : '拣货人',
                name : 'pickUser.display',
                hidden : true,
                width : 100
            }, {
                label : '拣货时间',
                name : 'pickTime',
                hidden : true,
                width : 100
            }, {
                label : '客户',
                name : 'customerProfile.id',
                hidden : true
            }, {
                label : '客户',
                name : 'customerProfile.display',
                index : 'customerProfile.nickName_OR_customerProfile.trueName',
                width : 100
            }, {
                label : '经办人',
                name : 'voucherUser.display',
                index : 'voucherUser.signinid',
                width : 120
            }, {
                label : '参考来源',
                name : 'referenceSource',
                hidden : true,
                width : 100
            }, {
                label : '参考凭证号',
                name : 'referenceVoucher',
                hidden : true,
                width : 100
            }, {
                label : '标题',
                name : 'title',
                hidden : true,
                width : 100
            }, {
                label : '收货人',
                name : 'receivePerson',
                width : 100
            }, {
                label : '电话',
                name : 'mobilePhone',
                width : 100
            }, {
                label : '收货地址',
                name : 'deliveryAddr',
                width : 250
            } ],
            rowNum : 10,
            multiselect : false,
            postData : {
                "search['NU_deliveryTime']" : false
            },
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : WEB_ROOT + "/myt/sale/sale-delivery!saleDeliveryDetails?id=" + row_id,
                    colModel : [ {
                        label : '商品主键',
                        name : 'commodity.id',
                        hidden : true
                    }, {
                        label : '销售（发货）商品',
                        name : 'commodity.display',
                        align : 'left'
                    }, {
                        label : '发货仓库',
                        name : 'storageLocation.id',
                        width : 80,
                        stype : 'select',
                        formatter : 'select',
                        searchoptions : {
                            value : Biz.getStockDatas()
                        }
                    }, {
                        label : '单位',
                        name : 'measureUnit',

                        width : 60
                    }, {
                        label : '数量',
                        name : 'quantity',
                        width : 50,
                        formatter : 'number'
                    }, {
                        label : '销售单价',
                        name : 'price',
                        width : 60,
                        formatter : 'currency'

                    }, {
                        label : '原价金额',
                        name : 'originalAmount',
                        width : 60,
                        formatter : 'currency'

                    }, {
                        label : '成本价',
                        name : 'costPrice',
                        width : 60,
                        formatter : 'currency'

                    }, {
                        label : '成本金额',
                        name : 'costAmount',
                        width : 60,
                        formatter : 'currency'

                    } ],
                    rowNum : -1,
                    loadonce : true,
                    multiselect : true,
                    operations : function(itemArray) {
                        var $grid = $(this);
                        var $select = $('<li><a href="javascript:;"><i class="fa fa-check-square-o"></i> 选取</a></li>');
                        $select.children("a").bind("click", function(e) {
                            e.preventDefault();
                            var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                            var $dialog = $grid.closest(".modal");
                            $dialog.modal("hide");
                            var callback = $dialog.data("callback");
                            if (callback) {
                                var $prow = $grid.closest('.ui-subgrid').prev();
                                var $pgrid = $prow.closest('.ui-jqgrid-btable');
                                var prowid = $prow.attr("id");
                                var data = {
                                    master : $pgrid.jqGrid("getRowData", prowid),
                                    rows : $grid.jqGrid("getSelectedRowdatas")
                                };

                                callback.call($grid, data);
                            }
                        });
                        itemArray.push($select);
                    }
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
