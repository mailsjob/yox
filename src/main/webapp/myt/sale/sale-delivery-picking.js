$(function() {
    var $form =$(".form-sale-delivery-picking");

    //扫描枪输入处理
    $form.find("input[name='voucher']").barcodeScanSupport({
        onEnter : function() {
            $form.submit();
        }
    })
    //销售单号输入
    $form.submit(function(event) {
        var $voucher = $('input[name="voucher"]', this);
        var voucher = $voucher.val();
        if (voucher != '') {
            var url = WEB_ROOT + "/myt/sale/sale-delivery!pickingDetails?voucher=" + voucher;
            $(".div-sale-delivery-picking-details").ajaxGetUrl(url)
        }
        $voucher.focus();
        $voucher.select();
        event.preventDefault();
        event.stopPropagation();
        return false;
    })
});