$(function() {
    $(".grid-myt-sale-sale-delivery-search").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/sale-delivery!findByPage',
        colModel : [ {
            label : '发货时间',
            name : 'deliveryTime',
            align : 'center',
            width : 150
        }, {
            label : '发货单凭证号',
            name : 'voucher',
            align : 'center',
            width : 120
        }, {
            label : '销售订单号',
            name : 'referenceVoucher',
            width : 150
        }, {
            label : '快递单号',
            name : 'logisticsNo',
            width : 100
        }, {
            label : '物流公司',
            name : 'logistics.id',
            align : 'center',
            stype : 'select',
            editoptions : {

                value : Biz.getLogisticsDatas()
            },
            width : 200
        }, {
            label : '实际运费',
            name : 'logisticsAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '发货类型',
            stype : 'select',
            name : 'deliveryType',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('deliveryTypeEnum')
            },
            cellattr : function(rowId, cellValue, rawObject, cm, rowdata) {
                if (rowdata['deliveryType'] == 'READY') {
                    return "class='badge-warning'"
                }
            },
            width : 60
        }, {
            label : '收货人',
            name : 'receivePerson',
            width : 100
        }, {
            label : '电话',
            name : 'mobilePhone',
            width : 100
        }, {
            label : '收货地址',
            name : 'deliveryAddr',
            width : 250
        } ],
        postData : {
            "search['NU_deliveryTime']" : false
        },
        sortorder : "desc",
        sortname : "deliveryTime",
        addable : false,
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/sale/sale-delivery!saleDeliveryDetails?id=" + row_id,
                colModel : [ {
                    label : '行项号',
                    name : 'subVoucher',
                    align : 'center',
                    width : 50
                }, {
                    label : '销售（发货）商品',
                    name : 'commodity.display',
                    align : 'left'
                }, {
                    label : '数量',
                    name : 'quantity',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '是否赠品',
                    name : 'gift',
                    width : 50,
                    edittype : 'checkbox'

                }, {
                    label : '单位',
                    name : 'measureUnit',
                    editable : true,
                    width : 60
                }, {
                    label : '规格型号',
                    name : 'commodity.spec',
                    width : 80
                }, {
                    label : '批号',
                    name : 'batchNo',
                    width : 80
                }, {
                    label : '到期日',
                    sorttype : 'date',
                    name : 'expireDate',
                    width : 100
                }, {
                    label : '发货仓库',
                    name : 'storageLocation.id',
                    width : 80,
                    stype : 'select',
                    formatter : 'select',
                    searchoptions : {
                        value : Biz.getStockDatas()
                    }
                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
});
