<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="tab-pane fade active in">
	<div class="row search-form-default">
		<div class="col-md-12">
			<form method="get" class="form-inline form-validation form-search form-search-init"
				data-grid-search=".grid-sale-sale-delivery-select-sale-return" action="#">
				<div class="form-group">
					<input type="text"
						name="search['CN_customerProfile.nickName_OR_customerProfile.trueName_OR_voucher_OR_referenceVoucher']"
						class="form-control input-large" placeholder="凭证号、客户、外部单号..." />
				</div>
				
				<button class="btn green" type="submmit">
					<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
				</button>
				<button class="btn default hidden-inline-xs" type="reset">
					<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
				</button>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="grid-sale-sale-delivery-select-sale-return" data-grid="table"></table>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-sale-sale-delivery-select-sale-return").data("gridOptions", {
            url : '${base}/myt/sale/sale-return!findByPage',
            colModel : [{
                label : '凭证号',
                name : 'voucher',
                width : 140
            }, {
                label : '退换货类型',
                name : 'saleReturnType',
                align : 'center',
                stype : 'select',
                searchoptions : {
                    value : Util.getCacheEnumsByType('saleReturnTypeEnum')
                },
                width : 100
            }, {
                label : '外部单号',
                name : 'referenceVoucher',
                width : 100
            }, {
                label : '审核通过',
                name : 'auditDate',
                sorttype:'date',
                width : 150
            }, {
                label : '客户',
                name : 'customerProfile.id',
                hidden:true,
                width : 100
            }, {
                label : '客户',
                name : 'customerProfile.display',
                index : 'customerProfile.nickName_OR_customerProfile.trueName',
                width : 100
            }, {
                label : '收货人',
                hidden:true,
                name : 'receivePerson',
                width : 100
            }, {
                label : '电话',
                name : 'mobilePhone',
                hidden:true,
                width : 100
            }, {
                label : '邮编',
                name : 'postCode',
                hidden:true,
                width : 100
            }, {
                label : '地址',
                hidden:true,
                name : 'deliveryAddr',
                width : 100
            }, {
                label : '备注',
                name : 'memo',
                width : 100
            }],
            rowNum : 10,
            multiselect : false,
            postData : {
                "search['NN_auditDate']" : true,
            },
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                    url : WEB_ROOT + "/myt/sale/sale-return!saleReturnDetails?id=" + row_id,
                        colModel : [ {
                            label : '流水号',
                            name : 'id',
                            hidden:true,
                            align : 'left'
                        },{
                            label : '商品',
                            name : 'commodity.id',
                            hidden:true,
                            align : 'left'
                        }, {
                            label : '商品',
                            name : 'commodity.sku',
                            hidden:true,
                            align : 'left'
                        } ,{
                            label : '商品',
                            name : 'commodity.title',
                            hidden:true,
                            align : 'left'
                        } ,{
                            label : '商品',
                            name : 'commodity.display',
                            align : 'left'
                        }, {
                            label : '数量',
                            name : 'quantity',
                            width : 50,
                            align:'center',
                            formatter : function(cellValue, options, rowdata, action) {
                                if (cellValue) {
                                    return cellValue.split("\.")[0];
                                }
                                return cellValue;
                            },
                            formatter : 'number'
                        }, {
                            label : '仓库',
                            name : 'storageLocation.id',
                            width : 80,
                            stype : 'select',
                            formatter : 'select',
                            searchoptions : {
                                value : Biz.getStockDatas()
                            }
                        }, {
                            label : '仓库',
                            name : 'storageLocation.display',
                            hidden:true,
                            width : 80
                        }, {
                            label : '批次号',
                            name : 'batchNo',
                            width : 80
                        }, {
                            label : '过期日期',
                            name : 'expireDate',
                            width : 80
                        }],
                    rowNum : -1,
                    loadonce : true,
                    multiselect : true,
                    operations : function(itemArray) {
                        var $grid = $(this);
                        var $select = $('<li><a href="javascript:;"><i class="fa fa-check-square-o"></i> 选取</a></li>');
                        $select.children("a").bind("click", function(e) {
                            e.preventDefault();
                            var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                            var $dialog = $grid.closest(".modal");
                            $dialog.modal("hide");
                            var callback = $dialog.data("callback");
                            if (callback) {
                                var $prow = $grid.closest('.ui-subgrid').prev();
                                var $pgrid = $prow.closest('.ui-jqgrid-btable');
                                var prowid = $prow.attr("id");
                                var data = {
                                    master : $pgrid.jqGrid("getRowData", prowid),
                                    rows : $grid.jqGrid("getSelectedRowdatas")
                                };
                                //alert(data.master['boxOrder.receivePerson']);
                                callback.call($grid, data);
                            }
                        });
                        itemArray.push($select);
                    }
                });
            }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>
