<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form method="get"
			class="form-inline form-validation form-search-init"
			data-grid-search=".grid-myt-sale-sale-delivery-delivery-tab"
			action="#">
			<input type="hidden" name="search['NN_auditDate']" value="true">
			<input type="hidden" name="search['NU_redwordDate']" value="true">
			<div class="form-group">
				<input type="text" name="search['CN_voucher_OR_receivePerson']"
					class="form-control input-large" placeholder="销售单号、收货人...">
			</div>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon">状态</span>
					<s:select name="search['NU_deliveryTime']"
						list="#{'true':'未发货','false':'已发货'}" value="true" />
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon">发货日期小于等于</span>
					<s3:datetextfield name="search['LE_reserveDeliveryTime']" current="true" />
				</div>
			</div>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-myt-sale-sale-delivery-delivery-tab"></table>
	</div>
</div>
<script src="${base}/myt/sale/sale-delivery-delivery-tab.js"
	type="text/javascript" />
<%@ include file="/common/ajax-footer.jsp"%>