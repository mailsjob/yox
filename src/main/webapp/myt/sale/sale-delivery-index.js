$(function() {
    $(".grid-myt-sale-sale-delivery").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/sale-delivery!findByPage',
        colModel : [ {
            label : '凭证号',
            name : 'voucher',
            align : 'center',
            width : 120
        },{
            label : '下单时间',
            name : 'createdDate',
            align : 'center',
            stype : 'date',
            width : 120
        },{
            label : '经办人',
            name : 'voucherUser.display',
            index : 'voucherUser.signinid',
            align : 'center',
            width : 60
        }, {
            label : '发货人模板',
            name : 'expressDataTemplate.id',
            editable:true,
            align : 'center',
            stype : 'select',
            editoptions : {
                optionsurl : WEB_ROOT + "/myt/sale/express-data-template!list",
            },
            width : 120
        }, {
            label : '库存模式',
            stype : 'select',
            name : 'storageMode',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('storageModeEnum')
            },
            width : 60
        }, {
            label : '发货类型',
            stype : 'select',
            name : 'deliveryType',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('deliveryTypeEnum')
            },
            cellattr : function(rowId, cellValue, rawObject, cm, rowdata) {
                if (rowdata['deliveryType'] == 'READY') {
                    return "class='badge-warning'"
                }
            },
            width : 60
        }, {
            label : '预约发货日期',
            name : 'reserveDeliveryTime',
            stype : 'date',
            editable:true,
            width : 120,
            cellattr : function(rowId, cellValue, rowdata, cm) {
               if(rowdata.reserveDeliveryTime){
                   var today=new Date();
                   var month;
                   if(today.getMonth()<9){
                        month="0"+(today.getMonth()+1);
                      
                   }else{
                       month=today.getMonth()+1;
                   }
                   var todayLabel=today.getFullYear()+"-"+month+"-"+today.getDate();
                   if (cellValue==todayLabel) {
                       return "class='badge-warning'"
                   } else if (cellValue > todayLabel) {
                       return "class='badge-success'"
                   } else{
                       return "class='badge-danger'"
                   }
               }
               return ;
            }
        }, {
            label : '提交时间',
            name : 'submitDate',
            stype : 'date',
            hidden : true,
            width : 100
        }, {
            label : '外部单号',
            name : 'referenceVoucher',
            width : 120
        }, {
            label : '最近操作',
            name : 'lastOperationSummary',
            width : 120
        }, {
            label : '凭证日期',
            name : 'voucherDate',
            hidden : true,
            stype : 'date',
            width : 100
        }, {
            label : '拣货人',
            name : 'pickUser.display',
            hidden : true,
            width : 100
        }, {
            label : '拣货时间',
            name : 'pickTime',
            hidden : true,
            width : 100
        }, {
            label : '整单金额',
            name : 'totalAmount',
            formatter : 'currency',
            width : 100
        }, {
            label : '收取客户运费',
            name : 'chargeLogisticsAmount',
            width : 100
        }, {
            label : '已付金额',
            name : 'payedAmount',
            formatter : 'currency',
            hidden : true,
            width : 100
        }, {
            label : '客户',
            name : 'customerProfile.display',
            index : 'customerProfile.nickName_OR_customerProfile.trueName',
            width : 100
        }, {
            label : '经办部门',
            name : 'voucherDepartment.display',
            index : 'voucherDepartment.code_OR_voucherDepartment.title',
            hidden : true,
            width : 100
        }, {
            label : '参考来源',
            name : 'referenceSource',
            hidden : true,
            width : 100
        }, {
            label : '参考凭证号',
            name : 'referenceVoucher',
            hidden : true,
            width : 100
        }, {
            label : '标题',
            name : 'title',
            hidden : true,
            width : 100
        }, {
            label : '收货人',
            name : 'receivePerson',
            width : 100
        }, {
            label : '物流公司',
            editable:true,
            name : 'logistics.id',
            align : 'center',
            stype : 'select',
            editoptions : {
                value : Biz.getLogisticsDatas()
            },
            width : 200
        }, {
            label : '快递单号',
            editable:true,
            name : 'logisticsNo',
            width : 100
        }, {
            label : '实际运费',
            editable : true,
            name : 'logisticsAmount',
            width : 100
        }, {
            label : '电话',
            name : 'mobilePhone',
            width : 100
        }, {
            label : '收货地址',
            name : 'deliveryAddr',
            width : 250
        }],
        editcol : 'voucher',
        footerrow : true,
        footerLocalDataColumn : [ 'totalAmount','payedAmount','chargeLogisticsAmount'],
        editurl:WEB_ROOT + "/myt/sale/sale-delivery!doSave",
        fullediturl : WEB_ROOT + "/myt/sale/sale-delivery!inputTabs",
        addable : false,
        operations : function(itemArray) {

            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 发货清单</a></li>');
            $select.children("a").bind("click", function(e) {
                e.preventDefault();
                var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                var url = WEB_ROOT + "/rpt/jasper-report!preview?format=XLS&report=SALE_DELIVERY_COMMODITY_LIST&contentDisposition=attachment";
                var rowDatas = $grid.jqGrid("getSelectedRowdatas");
                for (i = 0; i < rowDatas.length; i++) {
                    var rowData = rowDatas[i];
                    url += "&reportParameters['SALE_DELIVERY_IDS']=" + rowData['voucher'];
                }
                window.open(url, "_blank");
            });
            itemArray.push($select);
            
            var $select3 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i> 申通</a></li>');
            $select3.children("a").bind("click", function(e) {
                e.preventDefault();
                var $grid = $(this).closest(".ui-jqgrid").find(".ui-jqgrid-btable:first");
                var url = WEB_ROOT + "/rpt/jasper-report!preview?format=XLS&report=EXPRESS_SHENTONG&contentDisposition=attachment";
                var rowDatas = $grid.jqGrid("getSelectedRowdatas");
                for (i = 0; i < rowDatas.length; i++) {
                    var rowData = rowDatas[i];
                    url += "&reportParameters['SALE_DELIVERY_IDS']=" + rowData['voucher'];

                }
                window.open(url, "_blank");
            });
            itemArray.push($select3);

        },
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + "/myt/sale/sale-delivery!saleDeliveryDetails?id=" + row_id,
                colModel : [ {
                    label : '行项号',
                    name : 'subVoucher',
                    align : 'center',
                    width : 50
                }, {
                    label : '销售（发货）商品',
                    name : 'commodity.display',
                    align : 'left'
                }, {
                    label : '单位',
                    name : 'measureUnit',
                    editable : true,
                    width : 60
                }, {
                    label : '数量',
                    name : 'quantity',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '销售单价',
                    name : 'price',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '是否赠品',
                    name : 'gift',
                    width : 50,
                    edittype : 'checkbox'

                }, {
                    label : '原价金额',
                    name : 'originalAmount',
                    width : 60,
                    formatter : 'currency'

                }, {
                    label : '折扣率(%)',
                    name : 'discountRate',
                    width : 50,
                    formatter : 'number'
                }, {
                    label : '折扣额',
                    name : 'discountAmount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '折后金额',
                    name : 'amount',
                    width : 60,
                    formatter : 'currency'
                }, {
                    label : '税率(%)',
                    name : 'taxRate',
                    width : 50,
                    hidden : true,
                    formatter : 'number'

                }, {
                    label : '税额',
                    name : 'taxAmount',
                    width : 60,
                    hidden : true,
                    formatter : 'currency'
                }, {
                    label : '含税总金额',
                    name : 'commodityAndTaxAmount',
                    width : 80,
                    formatter : 'currency'
                }, {
                    label : '发货仓库',
                    name : 'storageLocation.id',
                    width : 80,
                    stype : 'select',
                    formatter : 'select',
                    searchoptions : {
                        value : Biz.getStockDatas()
                    }
                }, {
                    label : '批次号',
                    name : 'batchNo',
                    width : 80
                }, {
                    label : '批次过期日期',
                    name : 'expireDate',
                    width : 80
                } ],
                loadonce : true,
                multiselect : false
            });
        }
    });
});
