<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-sale-return-bpmInput"
	action="${base}/myt/sale/sale-return!bpmSave?prepare=true" method="post">
	<s:hidden name="taskId" value="%{#parameters.taskId}" />
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<s:hidden name="saleDelivery.id" />
	 <s:hidden name="saleReturnApply.id" />
	<div class="form-actions form-inline">
		<div class="inline" style="width: 100px">
			<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
				<i class="fa fa-check"></i> 保存
			</button>
			<button class="btn default btn-cancel" type="button">取消</button>
		</div>
		
		<div class="pull-right">
			<!-- <div class="form-group input-medium">
				<div class="input-group ">
					<span class="input-group-addon">商品条码</span> <input type="text" name="barcodeScan" placeholder="条码枪扫描输入..."
						class="form-control"><span class="input-group-addon"><i class="fa fa-barcode"></i></span>
				</div>
			</div> -->
			<a class="btn yellow btn-select-sale-delivery" href="javascript:;"><i class="fa fa-indent"> 从销售单选取</i></a> 
		</div>
	</div>
	<div class="form-body control-label-sm">
		<div class="portlet">
			<div class="portlet-title">
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">凭证状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.voucherStateEnum[voucherState]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 ">
						<div class="form-group">
							<label class="control-label">凭证编号</label>
							<div class="controls">
								<s:textfield name="voucher" readonly="%{notNew}" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">经办人</label>
							<div class="controls">
								<s:select name="voucherUser.id" list="usersMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">部门</label>
							<div class="controls">
								<s:select name="voucherDepartment.id" list="departmentsMap" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">记账日期</label>
							<div class="controls">
								<s3:datetextfield name="voucherDate" format="date" current="true" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">客户</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-customer-profile"></i>
							<s:textfield name="customerProfile.display" disabled="%{'DRAFT'!=voucherState.name()}" />
							<s:hidden name="customerProfile.id" disabled="%{'DRAFT'!=voucherState.name()}" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">关联外部单号</label>
					<div class="controls">
						<s:textfield name="referenceVoucher" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">关联订单来源</label>
					<div class="controls">
						<s:select name="referenceSource" list="referenceSourceMap"
							data-profile-param="default_sale_delivery_reference_source" />
					</div>
				</div>
			</div>
		</div>
		<div class="row hide">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">收货人</label>
					<div class="controls">
						<s:textfield name="receivePerson" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">电话</label>
					<div class="controls">
						<s:textfield name="mobilePhone" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">邮编</label>
					<div class="controls">
						<s:textfield name="postCode" />
					</div>
				</div>
			</div>
		</div>
		<div class="row hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">收货地址</label>
					<div class="controls">

						<s:textfield name="deliveryAddr" />

					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">退货类型</label>
					<div class="controls">
						<s:radio name="saleReturnType" list="#application.enums.saleReturnTypeEnum" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">备注说明</label>
					<div class="controls">
						<s:textfield name="memo" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="col-md-12">
				<table class="grid-myt-sale-sale-return-bpmInput" data-grid="items"
					data-readonly="<s:property value="%{voucherState.name()=='DRAFT'?false:true}"/>"
					data-pk='<s:property value="#parameters.id"/>' data-clone='<s:property value="#parameters.clone"/>'></table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">提交审批</label>
					<div class="controls text-warning">
						<s:checkbox name="submitToAudit" disabled="%{submitDate!=null}" label="勾选此项将会立即提交审批流程，在此期间不可修改数据！" value="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">商品总成本</label>
					<div class="controls">
						<s:textfield name="totalCommodityAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">折损额</label>
					<div class="controls">
						<s:textfield name="totalLossAmount" readonly="true" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">入库成本</label>
					<div class="controls">
						<s:textfield name="totalCostAmount" readonly="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品售额</label>
					<div class="controls">
						<s:textfield name="totalSaleAmount" readonly="true" />
					</div>
				</div>
			</div>

		</div>
		<div class="row">

			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">补偿客户运费</label>
					<div class="controls">
						<div class="input-group">
							<s:textfield name="deliveryAmount" />
							<div class="input-group-btn">
								<button tabindex="-1" class="btn default btn-delivery-by-amount" type="button">按金额自动分摊</button>
								<button tabindex="-1" data-toggle="dropdown" class="btn default dropdown-toggle" type="button">
									<i class="fa fa-angle-down"></i>
								</button>
								<ul role="menu" class="dropdown-menu pull-right">
									<li><a href="javascript:alert('TODO');">按数量自动分摊</a></li>
									<li><a href="javascript:alert('TODO');">平均分摊</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="note note-success">
				<h4 class="block">
					整单金额：<span class="span-total-return-amount"><s:property value="totalReturnAmount" /></span>
					<s:hidden name="totalReturnAmount" value="%{totalReturnAmount}" />
				</h4>
			</div>
		</div>
	</div>
	</div>

	<div class="form-actions right">
		
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/sale/sale-return-bpmInput.js" />
<%@ include file="/common/ajax-footer.jsp"%>
