$(function () {
    $(".grid-myt-sale-box-order-detail-commodity").data("gridOptions", {
        calcRowAmount: function (rowdata, src) {
            if (src == 'discountAmount') {
                rowdata['actualAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['discountAmount']);
                rowdata['modifiedPrice'] = MathUtil.div(rowdata['actualAmount'], rowdata['quantity']);
            } else if (src == 'actualAmount') {
                rowdata['discountAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['actualAmount']);
                rowdata['modifiedPrice'] = MathUtil.div(rowdata['actualAmount'], rowdata['quantity']);
            } else if (src == 'modifiedPrice') {
                rowdata['actualAmount'] = MathUtil.mul(rowdata['quantity'], rowdata['modifiedPrice']);
                rowdata['discountAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['actualAmount']);
            } else {
                rowdata['actualAmount'] = MathUtil.mul(rowdata['modifiedPrice'], rowdata['quantity']);
                rowdata['discountAmount'] = MathUtil.sub(rowdata['originalAmount'], rowdata['actualAmount']);
            }
        },
        updateRowAmount: function (src) {
            var $grid = $(this);
            var rowdata = $grid.jqGrid("getEditingRowdata");
            $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
            $grid.jqGrid("setEditingRowdata", rowdata);
        },
        url: WEB_ROOT + '/myt/sale/box-order-detail-commodity!findByPage',
        colModel: [{
            label: '商品',
            name: 'commodity.display',
            width: 250
        }, {
            label: '商品供应商',
            name: 'commodity.provider.code',
            stype: 'select',
            searchoptions: {
                value: {
                    "": "",
                    "HANZ": "韩总供应商",
                    "0000000000": "美月淘"
                }
            },
            width: 250
        }, {
            label: '销售单价',
            name: 'price',
            editoptions: {
                dataInit: function (elem) {
                    var $elem = $(elem);
                    $elem.attr("readonly", true);
                }
            },
            formatter: 'currency',
            width: 100
        }, {
            label: '销售数量',
            name: 'quantity',
            formatter: 'number',
            width: 100,
            editrules: {
                required: true,
                number: true
            },
            editoptions: {
                dataInit: function (elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function () {
                        $grid.data("gridOptions").updateRowAmount.call($grid);
                    });
                }
            }
        }, {
            label: '原始金额',
            name: 'originalAmount',
            formatter: 'currency',
            editoptions: {
                dataInit: function (elem) {
                    var $elem = $(elem);
                    $elem.attr("readonly", true);
                }
            },
            width: 100
        }, {
            label: '修改单价',
            name: 'modifiedPrice',
            formatter: 'currency',
            editrules: {
                number: true
            },
            editoptions: {
                dataInit: function (elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function () {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            },
            width: 100
        }, {
            label: '折扣金额',
            name: 'discountAmount',
            editrules: {
                required: true
            },
            editoptions: {
                dataInit: function (elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function () {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            },
            formatter: 'currency'
        }, {
            label: '金额小计',
            name: 'actualAmount',
            editrules: {
                required: true
            },
            editoptions: {
                dataInit: function (elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function () {
                        $grid.data("gridOptions").updateRowAmount.call($grid, $elem.attr("name"));
                    });
                }
            },
            formatter: 'currency'
        }, {
            label: '已发货数量',
            name: 'deliveriedQuantity',
            formatter: 'number',
            width: 80
        }, {
            label: '发货仓库',
            name: 'storageLocation.id',
            width: 120,
            editable: true,
            stype: 'select',
            searchoptions: {
                value: Biz.getStockDatas()
            }
        }, {
            label: '锁定库存量',
            name: 'salingLockedQuantity',
            formatter: 'number',
            width: 80,
            editable: true,
            editoptions: {
                dataInit: function (el) {
                    var $el = $(el);
                    var $grid = $(this);
                    var rowid = $el.closest("tr.jqgrow").attr("id");
                    var rowdata = $grid.jqGrid("getRowData", rowid);
                    if ($el.val() == '') {
                        $el.val(rowdata['quantity']);
                    }
                }
            }
        }, {
            label: '订单号',
            name: 'boxOrder.orderSeq',
            width: 200,
            align: 'left',
            formatter: function (cellValue, options, rowdata, action) {
                var html = cellValue;
                if (rowdata.circle) {
                    html += '<span class="badge badge-info">周</span>'
                }
                if (rowdata.splitPayMode != 'NO') {
                    html += '<span class="badge badge-success">分</span>'
                }
                return html;
            }
        }, {
            label: '状态',
            name: 'boxOrder.orderStatus',
            align: 'center',
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('boxOrderStatusEnum')
            },
            width: 80
        }, {
            label: '下单时间',
            name: 'boxOrder.orderTime',
            align: 'left',
            sorttype: 'date'
        }, {
            label: '下单客户',
            name: 'boxOrder.customerProfile.display',
            index: 'boxOrder.customerProfile.trueName_OR_boxOrder.customerProfile.nickName_OR_boxOrder.customerProfile.id',
            align: 'left',
            width: 80,
            formatter: function (cellValue, options, rowdata, action) {
                var url = "${base}/myt/customer/customer-profile!view?id=" + rowdata.boxOrder.customerProfile.id;
                return '<a href="' + url + '" data-toggle="modal-ajaxify" title="客户资料">' + cellValue + '</a>'
            }
        }, {
            label: '收货人',
            name: 'boxOrder.receivePerson',
            width: 50,
            align: 'left'
        }, {
            label: '客户来源',
            name: 'boxOrder.customerProfile.customerFrom',
            stype: 'select',
            searchoptions: {
                value: {
                    "": "",
                    "NONE": "美月淘",
                    "LAMAVC": "优蜜美店",
                    "SD": "圣大"
                }
            },
            align: 'left'
        }, {
            label: '行项号',
            name: 'boxOrderDetail.sn',
            align: 'center',
            width: 60
        }, {
            label: '预约发货日期',
            name: 'boxOrderDetail.reserveDeliveryTime',
            editable: true,
            align: 'center',
            stype: 'date'
        }, {
            label: '行项状态',
            name: 'boxOrderDetail.orderDetailStatus',
            width: 80,
            align: 'center',
            stype: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('boxOrderDetailStatusEnum')
            }
        }],
        inlineNav: {
            add: false
        },
        editurl: WEB_ROOT + '/myt/sale/box-order-detail-commodity!doSave',
        setGroupHeaders: {
            groupHeaders: [{
                startColumnName: 'boxOrder.orderSeq',
                numberOfColumns: 6,
                titleText: '订单信息'
            }, {
                startColumnName: 'boxOrderDetail.sn',
                numberOfColumns: 3,
                titleText: '行项信息'
            }]
        },
        postData: {
            "search['FETCH_boxOrder']": "INNER",
            "search['FETCH_boxOrderDetail']": "INNER",
            "search['FETCH_boxOrder.customerProfile']": "INNER.INNER"
        }
    });
});