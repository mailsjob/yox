<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="row search-form-default">
	<div class="col-md-12">
		<form method="get" class="form-inline form-validation form-search-init"
			data-grid-search=".grid-myt-sale-box-order-detail-sumByReserveDeliveryMonth" action="#">
			<div class="form-group">
				<s:textfield name="search['CN_commodity.sku_OR_commodity.title']"
					cssClass="form-control input-xlarge" placeholder="商品编码/名称..." />
			</div>
			<div class="form-group">
				<div class="input-group input-medium">
							<input type="text" name="search['BT_boxOrderDetail.reserveDeliveryTime']"
								class="form-control input-medium input-daterangepicker grid-param-data" placeholder="预约发货日期" pattern="" >
							</div>
				</div>
			<button class="btn default hidden-inline-xs" type="reset">
				<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
			</button>
			<button class="btn green" type="submmit">
				<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
			</button>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="grid-myt-sale-box-order-detail-sumByReserveDeliveryMonth"></table>
	</div>
</div>
<script src="${base}/myt/sale/box-order-detail-sumByReserveDeliveryMonth.js" />
<%@ include file="/common/ajax-footer.jsp"%>