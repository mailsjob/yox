$(function() {
    $(".grid-myt-sale-sale-delivery-saleAndReturn").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/sale-delivery!saleAndReturnDatas',
        colModel : [{
            label : '单据类型',
            name : 'voucherType',
            width : 100,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : {'':'','XS':'销售单','TH':'退换货单'}
            },
        },{
            label : '凭证号',
            name : 'voucher',
            width : 100
        }, {
            label : '销售日期',
            name : 'createdDate',
            type : 'date',
            width : 100
        }, {
            label : '商品名称',
            name : 'commodityTitle',
            width : 100
        }, {
            label : '数量',
            name : 'quantity',
            width : 50,
            formatter : 'number'
        }, {
            label : '单价',
            name : 'price',
            width : 60,
            formatter : 'currency'
        }, {
            label : '原价金额',
            name : 'originalAmount',
            width : 60,
            formatter : 'currency'

        }, {
            label : '折扣额',
            name : 'discountAmount',
            width : 60,
            formatter : 'currency'
        }, {
            label : '折后金额',
            name : 'amount',
            width : 60,
            formatter : 'currency'
        }, {
            label : '含税总金额',
            name : 'commodityAndTaxAmount',
            width : 80,
            formatter : 'currency'
        }, {
            label : '含税进价',
            name : 'costPrice',
            formatter : 'currency',
            width : 100
        }, {
            label : '库存模式',
            stype : 'select',
            name : 'storageMode',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('storageModeEnum')
            },
            width : 60
        } ],
        loadonce : true,
        footerrow : true,
        footerLocalDataColumn : [ 'quantity', 'originalAmount','discountAmount','amount','commodityAndTaxAmount']
    });
});
