<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-box-order-detail-inputBasic"
	action="${base}/myt/sale/box-order-detail!doSave" method="post" 
	data-editrulesurl="${base}/myt/sale/box-order-detail!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-sale-box-order-detail-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">行项付款状态</label>
					<div class="controls">
		                <s:select name="orderDetailStatus" list="#application.enums.boxOrderDetailStatusEnum" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">发货完成时间(后台在所有订单行项发货完成后更新设置)</label>
					<div class="controls">
		                <s3:datetextfield name="deliveryFinishTime" format="date" disabled="true"/>                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">销售(运费)费用</label>
					<div class="controls">
		                <s:textfield name="deliveryAmount" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">实际收货完成时间(可能是前端用户点击收货完成更新，也可能是定时任务更新)</label>
					<div class="controls">
		                <s3:datetextfield name="actualConfirmTime" format="date" disabled="true"/>                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">实际收货确认操作者(前端用户写入客户唯一标识，后台任务处理写入'SYS')</label>
					<div class="controls">
		                <s:textfield name="actualConfirmOperator" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">行项付款金额</label>
					<div class="controls">
		                <s:textfield name="payAmount" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">行项流水号，100、110、120……</label>
					<div class="controls">
		                <s:textfield name="sn" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">商品金额</label>
					<div class="controls">
		                <s:textfield name="commodityAmount" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">付款凭证</label>
					<div class="controls">
		                <s:textfield name="payVoucher" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">发货数量</label>
					<div class="controls">
		                <s:textfield name="deliveriedQuantity" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">关联盒子</label>
					<div class="controls">
		                <s:textfield name="box" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">实际付款金额</label>
					<div class="controls">
		                <s:textfield name="actualAmount" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">实际订单完结操作者(后台用户写入用户唯一标识，后台任务处理写入'SYS')</label>
					<div class="controls">
		                <s:textfield name="actualSuccessOperator" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">行项关联商品类型</label>
					<div class="controls">
		                <s:select name="commoditiesType" list="#application.enums.commoditiesTypeEnum" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">发货物流单号</label>
					<div class="controls">
		                <s:textfield name="logisticsNo" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">付款模式</label>
					<div class="controls">
		                <s:textfield name="payMode" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">自动收货完成时间(后台用户可修改从而延长收货时间)</label>
					<div class="controls">
		                <s3:datetextfield name="autoConfirmTime" format="date"  disabled="true"/>                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">价格</label>
					<div class="controls">
		                <s:textfield name="price" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">盒子名</label>
					<div class="controls">
		                <s:textfield name="boxTitle" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">当前工作流活动任务</label>
					<div class="controls">
		                <s:textfield name="activeTaskName" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">确认付款时间</label>
					<div class="controls">
		                <s3:datetextfield name="payConfirmTime" format="date"/>                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">销售折扣金额</label>
					<div class="controls">
		                <s:textfield name="discountAmount" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">行项付款时间</label>
					<div class="controls">
		                <s3:datetextfield name="payTime" format="date" disabled="true"/>                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">邮费</label>
					<div class="controls">
		                <s:textfield name="postage" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">上传支付网关号</label>
					<div class="controls">
		                <s:textfield name="detailOutTradeNo" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">预付发货时间</label>
					<div class="controls">
		                <s3:datetextfield name="reserveDeliveryTime" format="date" disabled="true"/>                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">实际完结订单时间 (可能是后台用户点击提取完结，也可能是定时任务更新)</label>
					<div class="controls">
		                <s3:datetextfield name="actualSuccessTime" format="date" disabled="true"/>                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">关联订单</label>
					<div class="controls">
		                <s:textfield name="boxOrder" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">自动完结订单时间(后台用户可修改从而延长完结时间)</label>
					<div class="controls">
		                <s3:datetextfield name="autoSuccessTime" format="date" disabled="true"/>                  
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">发货物流公司</label>
					<div class="controls">
		                <s:textfield name="logisticsName" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">数量</label>
					<div class="controls">
		                <s:textfield name="quantity" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">客户</label>
					<div class="controls">
		                <s:textfield name="customerProfile" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label"></label>
					<div class="controls">
		                <s:textfield name="notifiedCount" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">改价备注</label>
					<div class="controls">
		                <s:textfield name="modifierMemo" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">销售(成本)费用</label>
					<div class="controls">
		                <s:textfield name="costAmount" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">原价金额</label>
					<div class="controls">
		                <s:textfield name="originalAmount" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">行项里商品是否出现过退货</label>
					<div class="controls">
		                <s:radio name="hasReturnLog" list="#application.enums.booleanLabel" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">支付宝接口唯一编号</label>
					<div class="controls">
		                <s:textfield name="orderDetailSeq" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">已退款金额</label>
					<div class="controls">
		                <s:textfield name="refundAmount" disabled="true"/>
					</div>
				</div>
            </div>
        </div>
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-myt-sale-box-order-detail-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/myt/sale/box-order-detail-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>