<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<div class="tabbable tabbable-primary">
	<ul class="nav nav-pills">
		<li class="active"><a class="tab-default" data-toggle="tab">订单列表</a></li>
		<li class="tools pull-right"><a class="btn default reload" href="javascript:;"><i class="fa fa-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in">
			<div class="row search-form-default">
				<div class="col-md-12">
					<form method="get" class="form-inline form-validation form-search" data-grid-search=".grid-myt-sale-view-box-order"
						action="#">
						<div class="input-group">
							<div class="input-cont">
								<input type="text" name="search['CN_awardName_OR_lotteryCode_OR_awardCode']" class="form-control"
									placeholder="奖品名/奖品码/抽奖码">
							</div>
							<span class="input-group-btn">
								<button class="btn green" type="submmit">
									<i class="m-icon-swapright m-icon-white"></i>&nbsp; 查&nbsp;询
								</button>
								<button class="btn default" type="reset">
									<i class="fa fa-undo"></i>&nbsp; 重&nbsp;置
								</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="grid-myt-sale-view-box-order" data-grid="table"></table>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(function() {
        $(".grid-myt-sale-view-box-order").data("gridOptions", {
            url: '${base}/myt/sale/view-box-order!findByPage',
            colNames : [  '状态', '订单号', '订单名称<br>订单组号', '下单客户<br>推荐客户', '收货人','订单总金额', '已付总金额','下单时间', '付款模式' ],
            colModel : [{
                name : 'orderStatus',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : { 
                    value : Util.getCacheEnumsByType('boxOrderStatusEnum') 
               	}
                
            }, {
                name : 'orderSeq',
                width : 180,
                align : 'left'
            }, {
                name : 'orderGroupSeq_OR_title',
                index:'title',
                formatter : function(cellValue, options, rowdata, action) {
                   
                    var html = rowdata.title;
                    if (rowdata.orderGroupSeq) {
                        html += ("<br/>" + rowdata.orderGroupSeq);
                    }
						if(html){
						   return html;
                    }
                    return '';
                },
                align : 'left'
            }, {
                name : 'customerProfile.nickName_OR_customerProfile.trueName',
                formatter : function(cellValue, options, rowdata, action) {
                    var html = rowdata.customerProfile.display;
                    if (rowdata.intermediaryCustomerProfile) {
                        html += ("<br/>" + rowdata.intermediaryCustomerProfile.display);
                    }
                    return html;
                },
                hidden:true,
                width : 150,
                align : 'left'
            }, {
                name : 'receivePerson',
                index : 'receivePerson',
                width : 80,
                align : 'left'
            }, {
                name : 'actualAmount',
                index : 'actualAmount',
                formatter : 'currency'
            }, {
                name : 'actualPayedAmount',
                index : 'actualPayedAmount',
                formatter : 'currency'
            }, {
                name : 'orderTime',
                index : 'orderTime',
                sorttype:'date'
            }, {
                name : 'splitPayMode',
                width : 80,
                align : 'center',
                stype : 'select',
                searchoptions : { 
                    value : Util.getCacheEnumsByType('boxOrderSplitPayModeEnum') 
               	}
             } ],
            postData : {
                "search['FETCH_customerProfile']" : "INNER",
                "search['FETCH_intermediaryCustomerProfile']" : "LEFT"
            },
           
            multiselect : false,
            footerrow : true,
            footerLocalDataColumn : [ 'actualAmount', 'actualPayedAmount' ],
            subGrid : true,
            subGridRowExpanded : function(subgrid_id, row_id) {
                Grid.initSubGrid(subgrid_id, row_id, {
                   	url : '${base}/myt/sale/view-box-order!getBoxOrderDetails?id=' + row_id,       
                   	colNames : [  '状态',  '单价', '预约发货日期'],
                    colModel : [{
                        name : 'orderDetailStatus',
                        align : 'center',
                      	stype : 'select',
                        searchoptions : {
                            value : Util.getCacheEnumsByType('boxOrderDetailStatusEnum')
                        }
                    }, {
                        name : 'price',
                        index : 'price',
                        align : 'right',
                        formatter : 'currency'
                     }, {
                        name : 'reserveDeliveryTime',
                        index : 'reserveDeliveryTime',
                        sorttype:'date'
                      
                    }],
                   
                    
                    rowNum:-1,
                    multiselect : false,
                    pager : false,
                    subGrid : true,
                    subGridRowExpanded : function(subgrid_id, row_id) {
                        Grid.initSubGrid(subgrid_id, row_id, {
                            url : "${base}/myt/sale/view-box-order!getBoxOrderDetailCommodities?detailSid=" + row_id,        
                            colNames : [ '商品条码', '商品名称', '价格', '数量' ],
                            colModel : [ {
                                name : 'commodity.sku',
                               	index : 'commodity.sku',
                                width : 200,
                                formatter : function(cellValue, options, rowdata, action) {
                                    return "<a href='javascript:void(0)' class='operate_link' onclick='viewCommodityToTab(" + rowdata.commoditySid + ")'>" + cellValue + "</a>";
                                }
                            }, {
                                name : 'commodity.title'
                            }, {
                                name : 'finalPrice',
                                align : 'right',
                                formatter : 'currency'
                            }, {
                                name : 'quantity',
                                index : 'quantity',
                                sorttype:'number',
                                align : 'right'
                            }],
                            cmTemplate : {
                                sortable : false
                            },
                            rowNum:-1,
                            pager : false,
                            multiselect : false
                        });
                    }
                });
               }
        });
    });
</script>
<%@ include file="/common/ajax-footer.jsp"%>