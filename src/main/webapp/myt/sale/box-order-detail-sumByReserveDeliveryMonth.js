$(function() {
    $(".grid-myt-sale-box-order-detail-sumByReserveDeliveryMonth").data("gridOptions", {
        url : WEB_ROOT + '/myt/sale/box-order-detail-commodity!findByGroupReserveDeliveryMonth',
        postData : {
            "search['IN_boxOrderDetail.orderDetailStatus']" : 'S15O,S20PYD,S25PYD,S30C,S35C,S40DP'
        },
        colModel : [ {
            label : '预约发货月份',
            name : 'boxOrderDetail.reserveDeliveryMonth',
            stype : 'date',
            align : 'center'
        }, {
            label : '商品主键',
            hidden : true,
            name : 'commodity.id'
        }, {
            label : '商品sku',
            width : 100,
            name : 'commodity.sku',
            align : 'center'
        }, {
            label : '商品名称',
            width : 300,
            name : 'commodity.title'

        }, {
            label : '数量',
            align : 'center',
            width : 100,
            name : 'sum(quantity)'
        } ],
        grouping : true,
        groupingView : {
            groupField : [ 'boxOrderDetail.reserveDeliveryMonth' ],
            groupOrder : [ 'desc' ],
            groupCollapse : false
        },
        sortname : 'sum(quantity)',
        rowNum : 500,
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            var $grid = $(this);
            var rowdata = $grid.getRowData(row_id);
            Grid.initSubGrid(subgrid_id, row_id, {
                url : WEB_ROOT + '/myt/sale/box-order-detail-commodity!findByGroupReserveDeliveryTime',
                postData : {
                    "search['EQ_commodity.id']" : rowdata["commodity.id"],
                    "search['EQ_boxOrderDetail.reserveDeliveryMonth']" : rowdata["boxOrderDetail.reserveDeliveryMonth"],
                    "search['IN_boxOrderDetail.orderDetailStatus']" : 'S15O,S20PYD,S25PYD,S30C,S35C,S40DP'
                },
                colModel : [ {
                    label : '预约发货日期',
                    name : 'boxOrderDetail.reserveDeliveryTime',
                    stype : 'date'
                }, {
                    label : '商品主键',
                    hidden : true,
                    name : 'commodity.id'
                }, {
                    label : '商品sku',
                    name : 'commodity.sku',
                    align : 'center'
                }, {
                    label : '商品名称',
                    name : 'commodity.title'
                }, {
                    label : '数量',
                    name : 'sum(quantity)'
                } ],
                multiselect : false,
                subGrid : true,
                subGridRowExpanded : function(subgrid_id, row_id) {
                    var $grid = $(this);
                    var rowdata = $grid.getRowData(row_id);
                    Grid.initSubGrid(subgrid_id, row_id, {
                        url : WEB_ROOT + '/myt/sale/box-order-detail-commodity!findByPage',
                        postData : {
                            "search['EQ_commodity.id']" : rowdata["commodity.id"],
                            "search['EQ_boxOrderDetail.reserveDeliveryTime']" : rowdata["boxOrderDetail.reserveDeliveryTime"]
                        },
                        colModel : [ {
                            label : '商品sku',
                            name : 'commodity.sku',
                            align : 'center'
                        }, {
                            label : '商品名称',
                            name : 'commodity.title'
                        }, {
                            label : '数量',
                            name : 'quantity'
                        }, {
                            label : '行项',
                            name : 'boxOrderDetail.sn'
                        }, {
                            label : '订单',
                            name : 'boxOrder.orderSeq'
                        } ],

                        multiselect : false
                    })
                }
            })
        }
    });
});