<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-return-apply-bpmStoreReceive"
	action="${base}/myt/sale/sale-return-apply!bpmStoreReceive" method="post" autocomplete="off">
	<s:hidden name="taskId" value="%{#parameters.taskId}" />
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-ajaxify-reload=".ajaxify-tasks">
			<i class="fa fa-check"></i> 确认收货
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">是否入库</label>
				<div class="controls">
					<s:radio name="isInStore" list="#application.enums.booleanLabel" value="true" />
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">入库批次</label>
				<div class="controls">
					<s:textfield name="commodityStoreDisplay" placeholder="双击选择"/>
					<s:hidden name="commodityStoreId" />
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label">收货备注</label>
				<div class="controls">
					<s:textarea name="receiptMemo" rows="3"/>
				</div>
			</div>
		</div>
	</div>
</form>
<script>
$(function() {
	$(".form-myt-sale-return-apply-bpmStoreReceive").data("formOptions", {
       bindEvents : function() {
           var $form = $(this);
           $form.find("input[name='commodityStoreDisplay']").dblclick(function(){
        	   $this = $(this);
        	   $(this).popupDialog({
                   url : WEB_ROOT + '/myt/stock/commodity-stock!forward?_to_=selectionSingle&isGift=false',
                   postData : {
                       sku : <s:property value="boxOrderDetailCommodity.commodity.sku" />
                   },
                   title : '选取库存商品',
                   callback : function(item) {
                	   console.info(item);
                	  $this.val(item['commodity.display']+" | "+item['storageLocation.display'] + " | "+ item.batchNo);
                	  $form.find("input[name='commodityStoreId']").val(item.id);
                   }
               })
           });
       }
 	});
});
</script>
<div class="sale-delivery-content ajaxify"
	data-url="${base}/myt/sale/sale-return-apply!view?id=<s:property value='#parameters.id'/>"></div>
<%@ include file="/common/ajax-footer.jsp"%>
