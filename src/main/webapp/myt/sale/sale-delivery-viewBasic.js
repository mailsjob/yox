$(function() {
    $(".grid-myt-sale-sale-delivery-viewBasic").data("gridOptions", {
        url : WEB_ROOT + "/myt/sale/sale-delivery-detail!findByPage",
        url : function() {
            var pk = $(this).attr("data-pk");
            if (pk) {
                return WEB_ROOT + "/myt/sale/sale-delivery-detail!findByPage?search['EQ_saleDelivery.id']=" + pk;
            }
        },
        colModel : [ {
            label : '销售（发货）商品',
            name : 'commodity.display',
            width : 200,
            align : 'left'
        }, {
            label : '单位',
            name : 'measureUnit',
            width : 60
        }, {
            label : '数量',
            name : 'quantity',
            width : 50,
            formatter : 'number'
        }, {
            label : '成本单价',
            name : 'costPrice',
            width : 60,
            formatter : 'currency'
        }, {
            label : '销售单价',
            name : 'price',
            width : 60,
            formatter : 'currency'
        }, {
            label : '是否赠品',
            name : 'gift',
            width : 50,
            edittype : 'checkbox'
        }, {
            label : '原价金额',
            name : 'originalAmount',
            width : 60,
            formatter : 'currency'
        }, {
            label : '折扣率(%)',
            name : 'discountRate',
            width : 50,
            hidden : true,
            formatter : 'number'
        }, {
            label : '折扣额',
            name : 'discountAmount',
            width : 60,
            formatter : 'currency'
        }, {
            label : '折后金额',
            name : 'amount',
            width : 60,
            formatter : 'currency'
        }, {
            label : '税率(%)',
            name : 'taxRate',
            width : 50,
            hidden : true,
            formatter : 'number'
        }, {
            label : '税额',
            name : 'taxAmount',
            width : 60,
            hidden : true,
            formatter : 'currency'
        }, {
            label : '含税总金额',
            name : 'commodityAndTaxAmount',
            width : 80,
            hidden : true,
            formatter : 'currency'
        }, {
            label : '毛利率',
            name : 'profitRate',
            width : 40,
            formatter : 'percentage'
        }, {
            label : '毛利额',
            name : 'profitAmount',
            width : 40,
            sorttype : 'number',
            align : 'right'
        }, {
            label : '发货仓库',
            name : 'storageLocation.id',
            width : 80,
            stype : 'select',
            formatter : 'select',
            searchoptions : {
                value : Biz.getStockDatas()
            }
        }, {
            label : '批次号',
            name : 'batchNo',
            width : 80
        }, {
            label : '批次过期日期',
            name : 'expireDate',
            width : 80
        }  ],
        multiselect : false,
        filterToolbar : false,
        height : 'auto',
        pager : false,
        footerrow : true,
        footerLocalDataColumn : [ 'amount','profitAmount' ]
    });
});
