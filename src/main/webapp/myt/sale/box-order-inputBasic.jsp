<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sale-box-order-inputBasic"
	action="${base}/myt/sale/box-order!doSave" method="post">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions form-inline">
		<button type="submit" class="btn blue">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">订单信息</div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">订单号</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="orderSeq" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">订单组号</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="orderGroupSeq" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">跨境备注</label>
							<div class="controls">
								<s:property value="kjMemo" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">跨境支付备注</label>
							<div class="controls">
								<s:property value="kjPaymentMemo" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">跨境订单状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.kjOrderStatusCodeEnum[kjOrderStatusCode]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">跨境支付单状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.KjPaymentStatusCodeEnum[kjPaymentStatusCode]" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">订单名称</label>
							<div class="controls">
								<s:textfield name="title" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">订单状态</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.boxOrderStatusEnum[orderStatus]" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">订单备注</label>
							<div class="controls">
								<s:textarea name="memo" rows="5"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">客户备注</label>
							<div class="controls">
							<s:textarea name="customerMemo" rows="5"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">管理备注</label>
							<div class="controls">
								<s:textarea name="adminMemo" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">下单时间</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="orderTime" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">确认时间</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="confirmTime" />
								</p>
							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">支付类型:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="payModeMap[payMode]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">订单类型:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.boxOrderModeEnum[orderMode]" />
								</p>
							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">分期类型:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.boxOrderSplitPayModeEnum[splitPayMode]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">订单来源:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="#application.enums.orderFromEnum[orderFrom]" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">交易渠道:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="tradeTypeMap[tradeType]" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">积分:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="score" />
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">付款信息</div>
					<div class="tools">
						<a class="collapse" href="javascript:;"></a>
					</div>
				</div>
				<div class="portlet-body"></div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">应付定金:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="cautionMoneyNeed" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">已付定金:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="cautionMoneyActual" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">总计原始金额:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="originalAmount" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">销售折扣金额</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="discountAmount" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">运费</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="deliveryAmount" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">总计应付金额</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="actualAmount" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">已付总金额</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="actualPayedAmount" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label ">退款金额</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="refundAmount" />
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">收货信息</div>
					<div class="tools">
						<a class="collapse" href="javascript:;"></a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label ">收货人姓名:</label>
								<div class="controls">
									<s:textfield name="receivePerson" />
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label ">联系电话:</label>
								<div class="controls">
									<s:textfield name="mobilePhone" />
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label ">邮编:</label>
								<div class="controls">
									<s:textfield name="postCode" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label ">收货地址:</label>
								<div class="controls">
									<s:textfield name="deliveryAddr" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label ">送货日期类型:</label>
								<div class="controls">
									<s:select list="#application.enums.deliveryDateModeEnum" name="deliveryDateMode" />
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label ">送货时间类型:</label>
								<div class="controls">
									<s:select list="#application.enums.deliveryTimeModeEnum" name="deliveryTimeMode" />
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label ">邮费:</label>
								<div class="controls">
									<p class="form-control-static">
										<s:property value="postage" />
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">发票信息</div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label ">发票类型:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="invoiceType" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label ">发票公司:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="invoiceCompany" />
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label ">发票抬头:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="invoiceTitle" />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label ">发票内容:</label>
							<div class="controls">
								<p class="form-control-static">
									<s:property value="invoiceContent" />
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">送礼留言</div>
				<div class="tools">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label ">是否送礼:</label>
							<div class="controls">
								<s:radio list="#application.enums.booleanLabel" name="isPresent"></s:radio>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label ">接收人:</label>
							<div class="controls">
								<s:textfield name="presentReceiver" />
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label ">送礼人:</label>
							<div class="controls">
								<s:textfield name="presentSender" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label ">送礼代码:</label>
							<div class="controls">
								<s:textfield name="presentPaperCode" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label ">送礼贺词:</label>
							<div class="controls">
								<s:textarea name="presentWords" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions right">
		<button type="submit" class="btn blue">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<%@ include file="/common/ajax-footer.jsp"%>
