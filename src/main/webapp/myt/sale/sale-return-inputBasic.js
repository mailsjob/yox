$(function() {

    $(".form-myt-sale-sale-return-inputBasic").data("formOptions", {
        updateTotalAmount : function() {
            var $form = $(this);

            var $grid = $form.find(".grid-myt-sale-sale-return-inputBasic");
            var userData = {
                "commodity.display" : "合计："
            };
            // 更新表格汇总统计数据

            userData.quantity = $grid.jqGrid('sumColumn', 'quantity');
            userData.deliveryAmount = $grid.jqGrid('sumColumn', 'deliveryAmount');
            userData.originalAmount = $grid.jqGrid('sumColumn', 'originalAmount');
            userData.commodityAmount= $grid.jqGrid('sumColumn', 'commodityAmount');
            
            $grid.jqGrid("footerData", "set", userData, true);
            var totalCostAmount=userData.commodityAmount+userData.deliveryAmount;
            $form.setFormDatas({
                deliveryAmount : userData.deliveryAmount,
                totalAmount : userData.originalAmount,
                totalCostAmount:totalCostAmount,
                commodityAmount: userData.commodityAmount
                
            });
        },
        bindEvents : function() {
            var $form = $(this);
            var $grid = $form.find(".grid-myt-sale-sale-return-inputBasic");

            $form.find("input[name='chargeLogisticsAmount']").keyup(function() {
                $form.data("formOptions").updateTotalAmount.call($form);
            });

            Biz.setupCustomerProfileSelect($form);

            //扫描枪输入处理
            $form.find("input[name='barcode']").barcodeScanSupport({
                onEnter : function() {
                    var code = $(this).val();
                    if (code != '') {
                        var data = Biz.queryCacheCommodityDatas(code);
                        if (data && data.length > 0) {
                            var rowdata = data[0];
                            var ids = $grid.jqGrid('getDataIDs');
                            var targetRowdata = null;
                            var targetRowid = null;
                            $.each(ids, function(i, id) {
                                var item = $grid.jqGrid('getRowData', id);
                                if (item['commodity.barcode'] == rowdata['barcode']) {
                                    if (item['gift'] != 'true') {
                                        targetRowid = id;
                                        targetRowdata = item;
                                        return false;
                                    }
                                }
                            });

                            if (targetRowdata) {
                                targetRowdata.quantity = Number(targetRowdata.quantity) + 1;
                                $grid.data("gridOptions").calcRowAmount.call($grid, targetRowdata);
                                $grid.jqGrid('setRowData', targetRowid, targetRowdata);
                            } else {

                                var newdata = {
                                    'commodity.id' : rowdata.id,
                                    'commodity.barcode' : rowdata.barcode,
                                    'measureUnit' : rowdata.measureUnit,
                                    'storageLocation.id' : rowdata['defaultStorageLocation.id'],
                                    'commodity.display' : rowdata.display,
                                    'quantity' : 1
                                }
                                newdata['price'] = rowdata['lastSalePrice'];
                                $grid.data("gridOptions").calcRowAmount.call($grid, newdata);
                                $grid.jqGrid('insertNewRowdata', newdata);
                            }
                            $form.data("formOptions").updateTotalAmount.call($form);
                        }
                        $(this).focus();
                    }
                }
            });

            // 从销售订单选取
            $form.find(".btn-select-sale-delivery").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/sale/sale-return!forward?_to_=selectSaleDeliveryDetails',
                    title : '选取销售单',
                    callback : function(data) {
                        var master = data.master;
                        var rows = data.rows;
                        $form.setFormDatas({
                            'customerProfile.id' : master['customerProfile.id'],
                            'customerProfile.display' : master['customerProfile.display'],
                            'referenceVoucher' : master['boxOrder.orderSeq'],
                            'referenceSource' : 'MYT',
                            'memo' : master['memo']
                        }, false);

                        $.each(rows, function(i, row) {
                            row['measureUnit'] = row['commodity.measureUnit'];
                            row['saleDeliveryDetail.id'] = row['id'];
                            row['commodityPrice'] = row['costPrice'];
                            row['commodityAmount'] = row['costAmount'];
                            row['deliveryAmount'] = 0;
                            $grid.data("gridOptions").calcRowAmount.call($grid, row);
                            $grid.jqGrid('insertNewRowdata', row);
                            $form.data("formOptions").updateTotalAmount.call($form);
                        });
                    }
                })
            });
            
          //按金额自动分摊运费
            $form.find(".btn-delivery-by-amount").click(function() {

                if ($grid.jqGrid("isEditingMode", true)) {
                    return false;
                }
                //按照“原价金额”的比率计算更新每个行项的折扣额，注意最后一条记录需要以减法计算以修正小数精度问题
                var totalDeliveryAmount = $form.find("input[name='deliveryAmount']").val();
                var totalCommodityAmount = $grid.jqGrid('sumColumn', 'commodityAmount');
                var perDeliveryAmount = MathUtil.div(totalDeliveryAmount, totalCommodityAmount, 5);
                var lastrowid = null;
                var tempDeliveryAmount = 0;
                var ids = $grid.jqGrid("getDataIDs");
                $.each(ids, function(i, id) {
                    var rowdata = $grid.jqGrid("getRowData", id);
                    if (rowdata['commodity.id'] != '') {
                        rowdata['deliveryAmount'] = MathUtil.mul(perDeliveryAmount, rowdata['commodityAmount']);
                        $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, 'deliveryAmount');
                        $grid.jqGrid('setRowData', id, rowdata);
                        tempDeliveryAmount = MathUtil.add(tempDeliveryAmount, rowdata['deliveryAmount']);
                        lastrowid = id;
                    }
                });

                //最后一条记录需要以减法计算以修正小数精度问题
                if (lastrowid) {
                    var rowdata = $grid.jqGrid("getRowData", lastrowid);
                    rowdata['deliveryAmount'] = MathUtil.sub(totalDeliveryAmount, MathUtil.sub(tempDeliveryAmount, rowdata['deliveryAmount']));
                    $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, 'deliveryAmount');
                    $grid.jqGrid('setRowData', lastrowid, rowdata);
                }

                $form.data("formOptions").updateTotalAmount.call($form);

            });
        },
       
    });

    $(".grid-myt-sale-sale-return-inputBasic").data("gridOptions", {
        calcRowAmount : function(rowdata, src) {
            rowdata['originalAmount'] =MathUtil.mul( rowdata['price'] , rowdata['quantity']);
            rowdata['commodityAmount']=MathUtil.mul(rowdata['commodityPrice'] , rowdata['quantity']);
            rowdata['costAmount'] = MathUtil.add(rowdata['commodityAmount'], rowdata['deliveryAmount']);
            rowdata['costPrice'] = MathUtil.div(rowdata['costAmount'], rowdata['quantity']);
        },
        updateRowAmount : function(src) {
            var $grid = $(this);
            var rowdata = $grid.jqGrid("getEditingRowdata");
            $grid.data("gridOptions").calcRowAmount.call($grid, rowdata, src);
            $grid.jqGrid("setEditingRowdata", rowdata);
        },
        batchEntitiesPrefix : "saleReturnDetails",
        url : function() {
            var pk = $(this).attr("data-pk");
            if (pk) {
                return WEB_ROOT + "/myt/sale/sale-return!saleReturnDetails?id=" + pk + "&clone=" + $(this).attr("data-clone");
            }
        },
        colModel : [ {
            label : '所属销售单主键',
            name : 'saleReturn.id',
            hidden : true,
            hidedlg : true,
            editable : true,
            formatter : function(cellValue, options, rowdata, action) {
                var pk = $(this).attr("data-pk");
                return pk ? pk : "";
            }
        }, {
            name : 'commodity.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            name : 'saleDeliveryDetail.id',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            name : 'commodity.barcode',
            hidden : true,
            hidedlg : true,
            editable : true
        }, {
            label : '商品编码',
            name : 'commodity.sku',
            editable : true,
            editrules : {
                required : true
            },
            editoptions : Biz.getGridCommodityEditOptions(),
            align : 'left'
        }, {
            label : '商品名称',
            name : 'commodity.title',
            editable : true,
            editrules : {
                required : true
            },
            editoptions : Biz.getGridCommodityEditOptions(),
            align : 'left'
        }, {
            label : '发货仓库',
            name : 'storageLocation.id',
            width : 150,
            editable : true,
            editrules : {
                required : true
            },
            stype : 'select',
            searchoptions : {
                value : Biz.getStockDatas()
            }
        }, {
            label : '单位',
            name : 'measureUnit',
            editable : true,
            width : 80
        }, {
            label : '数量',
            name : 'quantity',
            width : 80,
            formatter : 'number',
            editable : true,
            editrules : {
                required : true,
                number : true
            },
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function() {
                        $grid.data("gridOptions").updateRowAmount.call($grid);
                    });
                }
            },
            summaryType : 'sum'
        }, {
            label : '单价',
            name : 'price',
            width : 80,
            formatter : 'currency',
            editable : true,
            editrules : {
                required : true,
                number : true
            },
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    $elem.keyup(function() {
                        $grid.data("gridOptions").updateRowAmount.call($grid);
                    });
                }
            },
            summaryType : 'sum'
        }, {
            label : '原始金额',
            name : 'originalAmount',
            width : 80,
            formatter : 'currency',
            editable : true,
            editoptions : {
                dataInit : function(elem) {
                    var $elem = $(elem);
                    $elem.attr("readonly", true);
                }
            },
            responsive : 'sm'
        }, {
            label : '退货商品单价',
            name : 'commodityPrice',
            width : 80,
            formatter : 'currency',
            editable : true,
            editoptions : {
                readonly : true
            },
            responsive : 'sm'
        }, {
            label : '退货商品金额',
            name : 'commodityAmount',
            width : 80,
            formatter : 'currency',
            editable : true,
            editoptions : {
                dataInit : function(elem) {
                    var $elem = $(elem);
                    $elem.attr("readonly", true);
                }
            },
            responsive : 'sm'
        }, {
            label : '运费',
            name : 'deliveryAmount',
            width : 80,
            formatter : 'currency',
            editable : true,
            editoptions : {
                dataInit : function(elem) {
                    var $grid = $(this);
                    var $elem = $(elem);
                    if ($elem.val() == "") {
                        $elem.val(0)
                    }
                    var $form = $elem.closest("form");
                    $elem.keyup(function() {
                        $grid.data("gridOptions").updateRowAmount.call($grid);
                    });
                }
            },
            responsive : 'sm'
        }, {
            label : '退货成本单价',
            name : 'costPrice',
            width : 80,
            formatter : 'currency',
            editable : true,
            editoptions : {
                readonly : true
            },
            responsive : 'sm'
        }, {
            label : '退货总成本',
            name : 'costAmount',
            width : 80,
            formatter : 'currency',
            editable : true,
            editoptions : {
                dataInit : function(elem) {
                    var $elem = $(elem);
                    $elem.attr("readonly", true);
                }
            },
            responsive : 'sm'
        } ],
        footerrow : true,
        beforeInlineSaveRow : function(rowid) {
            var $grid = $(this);
            $grid.data("gridOptions").updateRowAmount.call($grid);
        },
        afterInlineSaveRow : function(rowid) {
            var $grid = $(this);
            // 整个Form相关金额字段更新
            var $form = $grid.closest("form");
            $form.data("formOptions").updateTotalAmount.call($form);
        },
        afterInlineDeleteRow : function(rowid) {
            var $grid = $(this);
            // 整个Form相关金额字段更新
            var $form = $grid.closest("form");
            $form.data("formOptions").updateTotalAmount.call($form);
        },
        userDataOnFooter : true
    });

});
