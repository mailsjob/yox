$(function() {
    $(".grid-myt-mnf-mnf-user").data("gridOptions", {
        url : WEB_ROOT + "/myt/mnf/mnf-user!findByPage",
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true,
            responsive : 'sm'
        }, {
            label : '关联客户',
            name : 'customerProfile.display',
            name : 'customerProfile.trueName_OR_customerProfile.nickName',
            width : 50
            
        }, {
            label : '账户名',
            width : 50,
            name : 'accountNo'
        }, {
            label : '姓名',
            align:'center',
            width : 50,
            name : 'trueName'
        }, {
            label : '身份证号码',
            align:'center',
            width : 150,
            name : 'idCard'
        }, {
            label : '性别',
            stype : 'select',
            name : 'sex',
            align : 'center',
            searchoptions : {
                value : Util.getCacheEnumsByType('genericSexEnum')
            },
           width : 60
        }, {
            label : '预申请额度',
            width : 50,
            formatter:'currency',
            name : 'mnfMaxAccount'
        }, {
            label : '还款开始年',
            align:'center',
            width : 50,
            name : 'refundYear'
        }, {
            label : '还款开始月',
            align:'center',
            width : 50,
            name : 'refundMonth'
        }, {
            label : '账单日',
            align:'center',
            width : 50,
            name : 'billDay'
        }, {
            label : '还款月数',
            align:'center',
            width : 50,
            name : 'refundMonthCount'
        }]
    });
});