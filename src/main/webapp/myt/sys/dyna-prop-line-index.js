$(function() {
    $(".grid-sys-dyna-prop-line-index").data("gridOptions", {
        url : WEB_ROOT + "/myt/sys/dyna-prop-line!findByPage",
        colModel : [ {
            label : '代码',
            name : 'code',
            width : 200,
            editable : true
        }, {
            label : '名称',
            name : 'name',
            editable : true,
            width : 100,
            editoptions : {
                placeholder : '创建可留空自动生成'
            }
        }, {
            label : '排序号',
            name : 'orderRank',
            editable : true,
            width : 60
        }, {
            label : '级别',
            name : 'inheritLevel',
            editable : true,
            editrules : {
                required : true,
                number : true
            },
            align:'center',
            width : 60
        }, {
            label : '子项数目',
            name : 'childrenSize',
            editable : true,
            editrules : {
                required : true,
                number : true
            },
            align:'center',
            width : 60
        }, {
            label : '类型',
            name : 'type',
            width : 120,
            editable : true,
            align : 'center',
            stype : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('dynamicParameterTypeEnum')
            }
        }, {
            label : '是否必填',
            name : 'requiredFlag',
            editable : true,
            edittype : "checkbox"
        }, {
            label : '是否隐藏',
            name : 'hiddenFlag',
            editable : true,
            edittype : "checkbox"
        }, {
            label : '是否允许多选',
            name : 'multiSelectFlag',
            editable : true,
            edittype : "checkbox"
        }, {
            label : '是否禁用',
            name : 'disabled',
            editable : true,
            edittype : "checkbox"
        }, {
            label : '缺省',
            name : 'defaultValue',
            width : 100,
            editable : true
        }],
        sortorder : "desc",
        sortname : 'orderRank',
        subGrid : true,
        gridDnD : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            Grid.initRecursiveSubGrid(subgrid_id, row_id, "parent.id");
        },
        editurl : WEB_ROOT + "/myt/sys/dyna-prop-line!doSave",
        delurl : WEB_ROOT + "/myt/sys/dyna-prop-line!doDelete",
        editcol : 'code',
        inlineNav : {
            add : true
        }
    });
});