<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-myt-sys-announcement-inputBasic" action="${base}/myt/sys/announcement!doSave" method="post" data-editrulesurl="${base}/myt/sys/announcement!buildValidateRules">
    <s:hidden name="id" />
    <s:hidden name="version" />
    <s:token />
    <div class="form-actions">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-sys-announcement-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">发布位置代码</label>
                    <div class="controls">
                        <s:textfield name="displayPositionCode" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">到期时间</label>
                    <div class="controls">
                        <s3:datetextfield name="expireDate" format="date" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">发布时间</label>
                    <div class="controls">
                        <s3:datetextfield name="publishTime" format="date" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">内容</label>
                    <div class="controls">
                        <s:textfield name="contents" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">标题</label>
                    <div class="controls">
                        <s:textfield name="title" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">群代码</label>
                    <div class="controls">
                        <s:textfield name="toGroupCode" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">有效时间</label>
                    <div class="controls">
                        <s3:datetextfield name="effectiveDate" format="date" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">重要级别</label>
                    <div class="controls">
                        <s:textfield name="priorityLevel" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">状态</label>
                    <div class="controls">
                        <s:textfield name="state" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">发布序列代码</label>
                    <div class="controls">
                        <s:select name="displayScopeType" list="#application.enums.displayScopeTypeEnum" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">发布用户</label>
                    <div class="controls">
                        <s:textfield name="publishUser" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-myt-sys-announcement-index">
            <i class="fa fa-check"></i> 保存
        </button>
        <button class="btn default btn-cancel" type="button">取消</button>
    </div>
</form>
<script src="${base}/myt/sys/announcement-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>