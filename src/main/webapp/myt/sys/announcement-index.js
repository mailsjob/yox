$(function() {
    $(".grid-myt-sys-announcement-index").data("gridOptions", {
        url : WEB_ROOT + '/myt/sys/announcement!findByPage',
        colModel : [ {
            label : '流水号',
            name : 'id',
            hidden : true
        }, {
            label : '标题',
            name : 'title',
            width : 120,
            editable : true,
            align : 'left'
        }, {
            label : '发布位置代码',
            name : 'displayPositionCode',
            width : 120,
            editable : true,
            align : 'left'
        }, {
            label : '到期时间',
            name : 'expireDate',
            width : 100,
            formatter : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '发布时间',
            name : 'publishTime',
            width : 100,
            formatter : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '内容',
            name : 'contents',
            width : 200,
            editable : true,
            align : 'left'
        }, {
            label : '群代码',
            name : 'toGroupCode',
            width : 100,
            editable : true,
            align : 'left'
        }, {
            label : '有效时间',
            name : 'effectiveDate',
            width : 100,
            formatter : 'date',
            editable : true,
            align : 'center'
        }, {
            label : '重要级别',
            name : 'priorityLevel',
            width : 100,
            editable : true,
            align : 'left'
        }, {
            label : '状态',
            name : 'state',
            width : 100,
            editable : true,
            align : 'left'
        }, {
            label : '发布序列代码',
            name : 'displayScopeType',
            formatter : 'select',
            searchoptions : {
                value : Util.getCacheEnumsByType('displayScopeTypeEnum')
            },
            width : 100,
            editable : true,
            align : 'center'
        }, {
            label : '发布用户',
            name : 'publishUser.nick',
            index : 'publishUser',
            width : 120,
            editable : true,
            align : 'left'
        } ],
        postData : {
            "search['FETCH_publishUser']" : "LEFT"
        },
        editurl : WEB_ROOT + '/myt/sys/announcement!doSave',
        delurl : WEB_ROOT + '/myt/sys/announcement!doDelete',
        fullediturl : WEB_ROOT + '/myt/sys/announcement!inputTabs'
    });
});
