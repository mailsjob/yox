<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div class="portlet box red " id="portlet-notify-list">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-bell-o"></i> 预警信息
		</div>
		<div class="tools">
			<a href="" class="reload"></a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="list-group">
			<s:iterator value="#request.notifyDataMap" status="s" var="item">
				<s:if test='%{#item.key=="NOTIFY_TO_PAY_ORDER_DETAILS"}'>
					<a class="list-group-item" href=""
						href="javascript:;"
						rel="address:/myt/sale/box-order-detail!forward?_to_=earlyWarning&tabActiveIndex=1"
						id='<s:property  value="#item.key" />'>一周内预约发货而未付款的订单行项数 <span
						class="badge badge-danger"><s:property value="#item.value" /></span>
					</a>
				</s:if>
				<s:elseif test='%{#item.key=="NOTIFY_TO_DELIVERY_SALE_DETAILS"}'>
					<a class="list-group-item" href="" class="ajaxify"
						href="javascript:;"
						rel="address:/myt/sale/sale-delivery?tabActiveIndex=1"
						id='<s:property  value="#item.key" />'>预约发货日期快到或过期的提醒拣货发货的销售单行项<span
						class="badge badge-danger"><s:property value="#item.value" /></span>
					</a>
				</s:elseif>
				<s:elseif test='%{#item.key=="NOTIFY_TO_COMMODITY"}'>
					<a class="list-group-item" href="" class="ajaxify"
						href="javascript:;"
						rel="address:/myt/md/commodity!forward?_to_=earlyWarning&tabActiveIndex=1"
						id='<s:property  value="#item.key" />'>商品内容敏感信息预警<span
						class="badge badge-danger"><s:property value="#item.value" /></span>
					</a>
				</s:elseif>
				<s:elseif test='%{#item.key=="NOTIFY_TO_BRAND"}'>
					<a class="list-group-item" href="" class="ajaxify"
						href="javascript:;"
						rel="address:/myt/md/brand!forward?_to_=earlyWarning&tabActiveIndex=1"
						id='<s:property  value="#item.key" />'>品牌授权截止日期到期提醒<span
						class="badge badge-danger"><s:property value="#item.value" /></span>
					</a>
				</s:elseif>
				<s:elseif test='%{#item.key=="NOTIFY_TO_PURCHASE_ORDER"}'>
					<a class="list-group-item" href="" class="ajaxify"
						href="javascript:;"
						rel="address:/myt/purchase/purchase-order!forward?_to_=earlyWarning&tabActiveIndex=1"
						id='<s:property  value="#item.key" />'>计划入库时间已到还未入库的采购<span
						class="badge badge-danger"><s:property value="#item.value" /></span>
					</a>
				</s:elseif>
				<s:else>
					<a class="list-group-item" href="#"
						id='<s:property  value="#item.key" />'><s:property
							value="#item.key" /> <span class="badge badge-danger"><s:property
								value="#item.value" /></span> </a>
				</s:else>
			</s:iterator>
		</div>
	</div>
</div>
<%@ include file="/common/ajax-footer.jsp"%>