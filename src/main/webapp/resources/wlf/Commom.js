﻿function getLength(str) {
    var len = $.trim(str).length;
    var reLen = 0;
    for (var i = 0; i < len; i++) {
        if (str.charCodeAt(i) < 27 || str.charCodeAt(i) > 126) {
            // 全角    
            reLen += 2;
        } else {
            reLen++;
        }
    }
    return reLen;
}
function CheckDate(str) {
    var r = str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
    if (r == null) return false;
    var d = new Date(r[1], r[3] - 1, r[4]);
    return (d.getFullYear() == r[1] && (d.getMonth() + 1) == r[3] && d.getDate() == r[4]);
}
function strToDate(str) {
    var arys = new Array();
    arys = str.split('-');
    var newDate = new Date(arys[0], arys[1] - 1, arys[2]);
    return newDate;
}
function IsMobilePhone(phone) {
    if (/^(13[0-9]|15[0-9]|18[0-9]|14[0-9])\d{8}$/.test(phone)) {
        return true;
    }
    else {
        return false;
    }
}
//拼音
function IsPINYIN(value) {
    //alert(value);
    if (/^[A-Za-z]+$/.test(value)) {
        return true;
    }
    else {
        return false;
    }

}
//区号
function IsAreaCode(value) {
    //alert(value);
    if (/^0[0-9][0-9][0-9]?[0-9]?$/.test(value)) {
        return true;
    }
    else {
        return false;
    }
}
//电话号
function IsTel(value) {
    if (/^[0-9]*$/.test(value)) {
        if (value.length == 7 || value.length == 8) {
            return true;
        }
    }
}
//邮编
function IsPost(value) {
    if (/^[0-9]{6}$/.test(value)) {
        return true;
    }
    else {
        return false;
    }
}
//身份证
function CheckIDNumber(idcard) {
    var Errors = new Array(
				"验证通过!",
				"身份证号码位数不正确!",
				"身份证号码出生日期超出范围或含有非法字符!",
				"身份证号码校验错误!",
				"身份证地区非法!"
				);
    var area = { 11: "北京", 12: "天津", 13: "河北", 14: "山西", 15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江", 31: "上海", 32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西", 37: "山东", 41: "河南", 42: "湖北", 43: "湖南", 44: "广东", 45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州", 53: "云南", 54: "西藏", 61: "陕西", 62: "甘肃", 63: "青海", 64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门", 91: "国外" }

    var Y, JYM;
    var S, M;
    var idcard_array = new Array();
    idcard_array = idcard.split("");
    //地区检验
    if (area[parseInt(idcard.substr(0, 2))] == null) {
        alert(Errors[4]);
        return false;
    }
    //身份号码位数及格式检验
    switch (idcard.length) {
        case 15:
            if ((parseInt(idcard.substr(6, 2)) + 1900) % 4 == 0 || ((parseInt(idcard.substr(6, 2)) + 1900) % 100 == 0 && (parseInt(idcard.substr(6, 2)) + 1900) % 4 == 0)) {
                ereg = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$/; //测试出生日期的合法性
            }
            else {
                ereg = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$/; //测试出生日期的合法性
            }
            if (ereg.test(idcard)) {
                return true;
            }
            else {
                alert(Errors[2]);
                return false;
            }
            break;
        case 18:
            //18位身份号码检测
            //出生日期的合法性检查 
            //闰年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))
            //平年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))
            if (parseInt(idcard.substr(6, 4)) % 4 == 0 || (parseInt(idcard.substr(6, 4)) % 100 == 0 && parseInt(idcard.substr(6, 4)) % 4 == 0)) {
                ereg = /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$/; //闰年出生日期的合法性正则表达式
            } else {
                ereg = /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$/; //平年出生日期的合法性正则表达式
            }
            if (ereg.test(idcard)) {//测试出生日期的合法性
                //计算校验位
                S = (parseInt(idcard_array[0]) + parseInt(idcard_array[10])) * 7
				+ (parseInt(idcard_array[1]) + parseInt(idcard_array[11])) * 9
				+ (parseInt(idcard_array[2]) + parseInt(idcard_array[12])) * 10
				+ (parseInt(idcard_array[3]) + parseInt(idcard_array[13])) * 5
				+ (parseInt(idcard_array[4]) + parseInt(idcard_array[14])) * 8
				+ (parseInt(idcard_array[5]) + parseInt(idcard_array[15])) * 4
				+ (parseInt(idcard_array[6]) + parseInt(idcard_array[16])) * 2
				+ parseInt(idcard_array[7]) * 1
				+ parseInt(idcard_array[8]) * 6
				+ parseInt(idcard_array[9]) * 3;
                Y = S % 11;
                M = "F";
                JYM = "10X98765432";
                M = JYM.substr(Y, 1); //判断校验位
                if (M == idcard_array[17]) return true; //检测ID的校验位
                else {
                    alert(Errors[3]);
                    return false;
                }
            }
            else {
                alert(Errors[2]);
                return false;
            }
            break;
        default:
            alert(Errors[1]);
            return false;
            break;
    }
}
/* 
* 处理过长的字符串，截取并添加省略号 
* 注：半角长度为1，全角长度为2 
*  
* pStr:字符串 
* pLen:截取长度 
*  
* return: 截取后的字符串 
*/
function autoAddEllipsis(pStr, pLen) {

    var _ret = cutString(pStr, pLen);
    var _cutFlag = _ret.cutflag;
    var _cutStringn = _ret.cutstring;

    if ("1" == _cutFlag) {
        return _cutStringn;
    } else {
        return _cutStringn;
    }
}

/* 
* 取得指定长度的字符串 
* 注：半角长度为1，全角长度为2 
*  
* pStr:字符串 
* pLen:截取长度 
*  
* return: 截取后的字符串 
*/
function cutString(pStr, pLen) {

    // 原字符串长度  
    var _strLen = pStr.length;

    var _tmpCode;

    var _cutString;

    // 默认情况下，返回的字符串是原字符串的一部分  
    var _cutFlag = "1";

    var _lenCount = 0;

    var _ret = false;

    if (_strLen <= pLen / 2) {
        _cutString = pStr;
        _ret = true;
    }

    if (!_ret) {
        for (var i = 0; i < _strLen; i++) {
            if (isFull(pStr.charAt(i))) {
                _lenCount += 2;
            } else {
                _lenCount += 1;
            }

            if (_lenCount > pLen) {
                _cutString = pStr.substring(0, i);
                _ret = true;
                break;
            } else if (_lenCount == pLen) {
                _cutString = pStr.substring(0, i + 1);
                _ret = true;
                break;
            }
        }
    }

    if (!_ret) {
        _cutString = pStr;
        _ret = true;
    }

    if (_cutString.length == _strLen) {
        _cutFlag = "0";
    }

    return { "cutstring": _cutString, "cutflag": _cutFlag };
}

/* 
* 判断是否为全角 
*  
* pChar:长度为1的字符串 
* return: true:全角 
*          false:半角 
*/
function isFull(pChar) {
    if ((pChar.charCodeAt(0) > 128)) {
        return true;
    } else {
        return false;
    }
}  
var checkLength = function (strTemp) {
    var i, sum;
    sum = 0;
    for (i = 0; i < strTemp.length; i++) {
        if ((strTemp.charCodeAt(i) >= 0) && (strTemp.charCodeAt(i) <= 255)) {
            sum = sum + 1;
        } else {
            sum = sum + 2;
        }
    }
    return sum;
};
function checkstring(str, msg, icount, objs) {
    var charLength = checkLength(str, true);
    if (charLength > icount) {
        alert(msg + "不能输入超过" + icount + "个字符！");
        objs.focus();
        return true;
    }
}

function checkstring2(str1, str2, msg, icount, objs) {
    var charLength = checkLength(str1, true) + checkLength(str2, true);
    if (charLength > icount) {
        alert(msg+"不能输入超过"+icount+"个字符！");
        objs.focus();
        return true;
    }
}
function checkstring3(str1, str2, str3, msg, icount, objs) {
    var charLength = checkLength(str1, true) + checkLength(str2, true) + checkLength(str3, true);
    if (charLength > icount) {
        alert(msg + "不能输入超过" + icount + "个字符！");
        objs.focus();
        return true;
    }
}
function checkstring4(str1, str2, str3, str4, msg, icount, objs) {
    var charLength = checkLength(str1, true) + checkLength(str2, true) + checkLength(str3, true) + checkLength(str4, true);
    if (charLength > icount) {
        alert(msg + "不能输入超过" + icount + "个字符！");
        objs.focus();
        return true;
    }
}
 function seashowtip(tips, flag, iwidth, positionID) {
	var PID = document.getElementById(positionID);
	var my_tips = document.getElementById("mytips");
	//alert(PID.offsetTop);
	if (flag) {
		my_tips.innerHTML = tips;
		my_tips.style.display = "";
		my_tips.style.width = iwidth;
		my_tips.style.left = PID.offsetLeft + 138;
		my_tips.style.top = PID.offsetTop;
	}
	else {
		my_tips.style.display = "none";
	}
}