function GoAjax(url,type,dataType,paramter,ResultCallback){
   var xmlHttp=false;
   try{
     xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
   }catch(e){
     try{
       xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
     }catch(e2){
       xmlHttp=false;
     }
   }

   if(!xmlHttp && typeof XMLHttpRequest!='undefined'){
      xmlHttp=new XMLHttpRequest();
   }


   var data=paramter;
   
   xmlHttp.open(type,url,false);
   xmlHttp.setRequestHeader("CONTENT-TYPE","application/x-www-form-urlencoded");
   xmlHttp.setRequestHeader("Content-Length",data.length);

   xmlHttp.onreadystatechange=function(){
      
      if(xmlHttp.readyState==4){
        if(dataType=="text"){
          ResultCallback(xmlHttp.responseText);
        }
        delete(xmlHttp);
      }
      
   }
   xmlHttp.send(data);
}