<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-yox-yx-customer-bind-info-inputBasic"
	action="${base}/yox/yx-customer-bind-info!doSave" method="post" 
	data-editrulesurl="${base}/yox/yx-customer-bind-info!buildValidateRules">
	<s:hidden name="id" />
	<s:hidden name="version" />
	<s:token />
	<div class="form-actions">
		<button class="btn blue" type="submit" data-grid-reload=".grid-yox-yx-customer-bind-info-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
	<div class="form-body">
        <!--div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">绑定状态</label>
					<div class="controls">
		                <s:select name="bindStatus" list="#application.enums.bindStatusEnum"/>
					</div>
				</div>
            </div>
        </div-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">真实姓名</label>
					<div class="controls">
						<s:textfield name="trueName" />
					</div>
				</div>
			</div>
		</div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">手机号</label>
					<div class="controls">
		                <s:textfield name="mobile" />
					</div>
				</div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">工作单位</label>
					<div class="controls">
						<s:textfield name="hospital" />
					</div>
				</div>
			</div>
		</div>
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">备注</label>
					<div class="controls">
		                <s:textfield name="memo" />
					</div>
				</div>
            </div>
        </div>
        <!--div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label class="control-label">关联用户</label>
					<div class="controls">
						<div class="input-icon right">
							<i class="fa fa-ellipsis-horizontal fa-select-customer"></i>
							<s:textfield name="customerProfile.display" />
							<s:hidden name="customerProfile.id" />
						</div>
					</div>
				</div>
            </div>
        </div-->
	</div>
	<div class="form-actions right">
		<button class="btn blue" type="submit" data-grid-reload=".grid-yox-yx-customer-bind-info-index">
			<i class="fa fa-check"></i> 保存
		</button>
		<button class="btn default btn-cancel" type="button">取消</button>
	</div>
</form>
<script src="${base}/yox/yx-customer-bind-info-inputBasic.js" />
<%@ include file="/common/ajax-footer.jsp"%>