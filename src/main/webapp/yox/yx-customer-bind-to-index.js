$(function () {
    $(".grid-yox-yx-customer-bind-to-index").data("gridOptions", {
        url: WEB_ROOT + '/yox/yx-customer-bind-info!findByPage',
        colModel: [{
            label: '关联二维码',
            name: 'wxQrcode',
            formatter: function (cellValue, options, rowdata, action) {
                // ajax获取关联二维码
                var pic = "未关联二维码";
                jQuery.ajax({
                    type: "get",
                    async: false,
                    url: WEB_ROOT + "/yox/yx-customer-bind-info!getQrcode?customerProfile=" + rowdata.customerProfile.id,
                    dataType: "json",
                    success: function (result) {
                        if (result != null) {
                            pic = Biz.md5CodeImgViewFormatter(result.picMd5);
                        }
                    }
                });
                return pic;
            },
            frozen: true,
            align: 'left'
        }, {
            label: '流水号',
            name: 'id',
            hidden: true
        }, {
            label: '真实姓名',
            name: 'trueName',
            align: 'left'
        }, {
            label: '手机号',
            name: 'mobile',
            align: 'left'
        }, {
            label: '工作单位',
            name: 'hospital',
            align: 'left'
        }, {
            label: '备注',
            name: 'memo',
            align: 'left'
        }, {
            label: '关联用户',
            name: 'customerProfile.display',
            index: 'customerProfile',
            formatter: function (cellValue, options, rowdata, action) {
                return rowdata.customerProfile.id + ' ' + cellValue;
            },
            align: 'left'
        }, {
            label: '状态',
            name: 'bindStatus',
            formatter: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('bindStatusEnum')
            },
            align: 'center'
        }, {
            label: 'openId',
            name: 'customerProfile.openid',
            hidden: true,
            align: 'left'
        }, {
            label: 'weixin',
            name: 'customerProfile.weixinId',
            hidden: true,
            align: 'left'
        }],
        gridComplete: function () {
            $('.blockUI.blockOverlay').css('display', 'none');
        },
        postData: {
            "search['FETCH_customerProfile']": "LEFT",
            "search['EQ_customerProfile.customerFrom']": "ZHONGZI",
            "search['EQ_bindStatus']": "OK"
        },
        sortname: "lastModifiedDate",
        operations: function (itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>解绑并降级</a></li>');
            $select.children("a").bind("click", function (e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/yox/yx-customer-bind-info!rollback';
                    $grid.ajaxPostURL({
                        url: url,
                        success: function () {
                            $grid.refresh();
                        },
                        confirmMsg: "确认  解绑并降级？",
                        data: {
                            ids: ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select);
        },
    });
});
