$(function () {
    $(".grid-yox-yx-customer-bind-from-index").data("gridOptions", {
        url: WEB_ROOT + '/yox/yx-customer-bind-info!findByPage',
        colModel: [{
            label: '流水号',
            name: 'id',
            hidden: true
        }, {
            label: '关联二维码',
            name: 'wxQrcode',
            formatter: function (cellValue, options, rowdata, action) {
                // ajax获取关联二维码
                var pic = "未关联二维码";
                jQuery.ajax({
                    type: "get",
                    async: false,
                    url: WEB_ROOT + "/yox/yx-customer-bind-info!getQrcode?customerProfile=" + rowdata.customerProfile.id,
                    dataType: "json",
                    success: function (result) {
                        if (result != null) {
                            pic = Biz.md5CodeImgViewFormatter(result.picMd5);
                        }
                    }
                });
                return pic;
            },
            frozen: true,
            align: 'left'
        }, {
            label: '编号',
            name: 'customerProfile.id'
        }, {
            label: '真实姓名',
            name: 'trueName',
            align: 'left'
        }, {
            label: '手机号',
            name: 'mobile',
            align: 'left'
        }, {
            label: '工作单位',
            name: 'hospital',
            align: 'left'
        }, {
            label: '备注',
            name: 'memo',
            align: 'left'
        }, {
            label: 'openId',
            name: 'customerProfile.openid',
            hidden: true,
            align: 'left'
        }, {
            label: 'weixin',
            name: 'customerProfile.weixinId',
            hidden: true,
            align: 'left'
        }],
        postData: {
            "search['FETCH_customerProfile']": "LEFT",
            "search['EQ_customerProfile.customerFrom']": "ZHONGZI",
            "search['EQ_bindStatus']": "NO"
        },
        gridComplete: function () {
            $('.blockUI.blockOverlay').css('display', 'none');
        },
        sortname: "createdDate",
        editurl: WEB_ROOT + '/yox/yx-customer-bind-info!doSave',
        delurl: WEB_ROOT + '/yox/yx-customer-bind-info!doDelete',
        fullediturl: WEB_ROOT + '/yox/yx-customer-bind-info!inputTabs'
    });
});
