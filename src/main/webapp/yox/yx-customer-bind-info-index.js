$(function () {
    var ids = new Array();
    $(".grid-yox-yx-customer-bind-info-index").data("gridOptions", {
        url: WEB_ROOT + '/yox/yx-customer-bind-info!findByPage',
        colModel: [{
            label: '流水号',
            name: 'id',
            hidden: true
        }, {
            label: '真实姓名',
            name: 'trueName',
            align: 'left'
        }, {
            label: '手机号',
            name: 'mobile',
            formatter: function (cellValue, options, rowdata, action) {
                // ajax同步判断该用户是否是种子用户，如果是则令该行默认选中
                var $grid = $(this);
                jQuery.ajax({
                    type: "get",
                    async: false,
                    url: WEB_ROOT + "/yox/yx-customer-bind-from!check?mobile=" + cellValue,
                    dataType: "json",
                    success: function (result) {
                        if (result.length > 0) {
                            ids.push(options.rowId);
                            return cellValue = cellValue + "(可升级)";
                        }
                    }
                });
                return cellValue;
            },
            cellattr: function (rowId, cellValue, rowdata, cm) {
                if (ids.indexOf(rowId) > -1) {
                    return "class='badge-success'"
                }
            },
            align: 'left'
        }, {
            label: '工作单位',
            name: 'hospital',
            align: 'left'
        }, {
            label: '备注',
            name: 'memo',
            align: 'left'
        }, {
            label: '关联用户',
            name: 'customerProfile.display',
            index: 'customerProfile',
            formatter: function (cellValue, options, rowdata, action) {
                return rowdata.customerProfile.id + ' ' + cellValue;
            },
            align: 'left'
        }, {
            label: '状态',
            name: 'bindStatus',
            formatter: 'select',
            searchoptions: {
                value: Util.getCacheEnumsByType('bindStatusEnum')
            },
            editable: true,
            align: 'center'
        }, {
            label: 'openId',
            name: 'customerProfile.openid',
            hidden: true,
            align: 'left'
        }, {
            label: 'weixin',
            name: 'customerProfile.weixinId',
            hidden: true,
            align: 'left'
        }],
        postData: {
            "search['FETCH_customerProfile']": "LEFT",
            "search['NE_customerProfile.customerFrom']": "ZHONGZI",
            "search['EQ_bindStatus']": "NO"
        },
        operations: function (itemArray) {
            var $grid = $(this);
            var $select = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>升级</a></li>');
            $select.children("a").bind("click", function (e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/yox/yx-customer-bind-info!merge';
                    $grid.ajaxPostURL({
                        url: url,
                        success: function () {
                            $grid.refresh();
                        },
                        confirmMsg: "确认  升级？",
                        data: {
                            ids: ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select);

            var $select1 = $('<li data-position="multi" data-toolbar="show"><a href="javascript:;"><i class="fa fa-print"></i>解绑</a></li>');
            $select1.children("a").bind("click", function (e) {
                e.preventDefault();
                var ids = $grid.getAtLeastOneSelectedItem();
                if (ids) {
                    var url = WEB_ROOT + '/yox/yx-customer-bind-info!unbunde';
                    $grid.ajaxPostURL({
                        url: url,
                        success: function () {
                            $grid.refresh();
                        },
                        confirmMsg: "确认  解绑？",
                        data: {
                            ids: ids.join(",")
                        }
                    })
                }
            });
            itemArray.push($select1);
        },
        sortname: "createdDate"
        //gridComplete: function () {
        //    for (var i in ids) {
        //        $(this).jqGrid('setSelection', ids[i]);
        //    }
        //}
    });
});
