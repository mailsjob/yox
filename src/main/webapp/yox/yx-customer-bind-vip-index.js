$(function () {
    $(".grid-yox-yx-customer-bind-vip-index").data("gridOptions", {
        url: WEB_ROOT + '/yox/yx-customer-bind-info!findByPage',
        colModel: [{
            label: '头像',
            name: 'customerProfile.headPhoto',
            formatter: Biz.md5CodeImgViewFormatter,
            align: 'left'
        }, {
            label: '最近扫码时间',
            name: 'customerProfile.registrationTime',
            sorttype: 'date',
            align: 'center'
        }, {
            label: '编号',
            name: 'customerProfile.id'
        }, {
            label: '昵称',
            name: 'customerProfile.nickName',
            align: 'left'
        }, {
            label: 'openId',
            name: 'customerProfile.openid',
            hidden: true,
            align: 'left'
        }, {
            label: 'weixin',
            name: 'customerProfile.weixinId',
            hidden: true,
            align: 'left'
        }, {
            label: '上级编号',
            name: 'upstreamId',
            formatter: function (cellValue, options, rowdata, action) {
                // ajax获取上线
                var up = "";
                jQuery.ajax({
                    type: "get",
                    async: false,
                    url: WEB_ROOT + "/yox/yx-customer-bind-info!getUpstream?customerProfile=" + rowdata.customerProfile.id,
                    dataType: "json",
                    success: function (result) {
                        if (result != null) {
                            up = result.customerProfile.id;
                        }
                    }
                });
                return up;
            },
            align: 'left'
        }, {
            label: '上级昵称',
            name: 'upstreamNickName',
            formatter: function (cellValue, options, rowdata, action) {
                // ajax获取上线
                var up = "";
                jQuery.ajax({
                    type: "get",
                    async: false,
                    url: WEB_ROOT + "/yox/yx-customer-bind-info!getUpstream?customerProfile=" + rowdata.customerProfile.id,
                    dataType: "json",
                    success: function (result) {
                        if (result != null) {
                            up = result.customerProfile.nickName;
                        }
                    }
                });
                return up;
            },
            align: 'left'
        }],
        postData: {
            "search['FETCH_customerProfile']": "LEFT",
            "search['NN_customerProfile.registrationTime']": "true"
        },
        sortname: "customerProfile.registrationTime",
        sortorder: "desc",
    });
});
