<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<form class="form-horizontal form-bordered form-label-stripped form-validation form-yox-create-customer"
      action="${base}/yox/yx-customer-bind-info!createCustomer" method="post"
      enctype="multipart/form-data">
    <s:token/>

    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-3">Excel</label>
                    <div class="col-md-4">
                        <input type="file" class="default" name="customerExcel"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-3">Excel模板示例</label>
                    <div class="col-md-4">
                        <p class="form-control-static">
                            <a href="${base}/yox/import-yox-customer.xlsx">用户数据模板</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue" type="submit" data-grid-reload=".grid-yox-create-customer">
            <i class="fa fa-check"></i> 生成
        </button>
    </div>
</form>
<%@ include file="/common/ajax-footer.jsp" %>