$(function() {
    $(".form-yox-yx-customer-bind-info-inputBasic").data("formOptions", {
        bindEvents : function() {
            var $form = $(this);
            $form.find(".fa-select-customer").click(function() {
                $(this).popupDialog({
                    url : WEB_ROOT + '/myt/customer/customer-profile!forward?_to_=selection',
                    title : '选取用户',
                    callback : function(rowdata) {
                        $form.setFormDatas({
                            'customerProfile.id' : rowdata.id,
                            'customerProfile.display' : rowdata.display
                        });
                    }
                })
            });
        }
    });
});