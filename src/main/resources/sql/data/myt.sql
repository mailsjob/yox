INSERT INTO [tbl_sys_menu] ([id], [acl_code], [acl_type], [created_by], [created_dt], [updated_by], [updated_dt], [version], [children_size], [code], [description], [disabled], [inherit_level], [init_open], [order_rank], [style], [title], [type], [url], [parent_id]) VALUES ('402880c54201d09f014201d5e34c0000', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'M489641', '', '0', NULL, '0', 5000, '', '美月淘运营', 'RELC', '', NULL);
GO
INSERT INTO [tbl_sys_menu] ([id], [acl_code], [acl_type], [created_by], [created_dt], [updated_by], [updated_dt], [version], [children_size], [code], [description], [disabled], [inherit_level], [init_open], [order_rank], [style], [title], [type], [url], [parent_id]) VALUES ('402880c54201d09f014201d64d660001', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'M881249', '', '0', NULL, '1', 1000, '', '主数据管理', 'RELC', '', '402880c54201d09f014201d5e34c0000');
GO
INSERT INTO [tbl_sys_menu] ([id], [acl_code], [acl_type], [created_by], [created_dt], [updated_by], [updated_dt], [version], [children_size], [code], [description], [disabled], [inherit_level], [init_open], [order_rank], [style], [title], [type], [url], [parent_id]) VALUES ('402880c54201d09f014201d6e7b30002', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL,'M457573', '', '0', NULL, '1', 100, '', '商品数据管理', 'RELC', '', '402880c54201d09f014201d64d660001');
GO
INSERT INTO [tbl_sys_menu] ([id], [acl_code], [acl_type], [created_by], [created_dt], [updated_by], [updated_dt], [version], [children_size], [code], [description], [disabled], [inherit_level], [init_open], [order_rank], [style], [title], [type], [url], [parent_id]) VALUES ('402880c54201d09f014201d77dd10004', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'M071900', '', '0', NULL, '1', 1000, '', '品牌管理', 'RELC', '/md/brand', '402880c54201d09f014201d6e7b30002');
GO
INSERT INTO [tbl_sys_menu] ([id], [acl_code], [acl_type], [created_by], [created_dt], [updated_by], [updated_dt], [version], [children_size], [code], [description], [disabled], [inherit_level], [init_open], [order_rank], [style], [title], [type], [url], [parent_id]) VALUES ('402880c54201d09f014201d8019d0006', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'M117030', '', '0', NULL, '1', 800, '', '商品管理', 'RELC', '/md/commodity', '402880c54201d09f014201d6e7b30002');
GO
INSERT INTO [tbl_sys_menu] ([id], [acl_code], [acl_type], [created_by], [created_dt], [updated_by], [updated_dt], [version], [children_size], [code], [description], [disabled], [inherit_level], [init_open], [order_rank], [style], [title], [type], [url], [parent_id]) VALUES ('402880c5420d626801420d6f4a220008', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'M794011', '', '0', NULL, '0', 5000, '', '商家分类管理', 'RELC', '/md/partner-category', '402880c54201d09f014201d6e7b30002');






INSERT INTO [tbl_auth_privilege] ([id], [acl_code], [acl_type], [created_by], [created_dt], [updated_by], [updated_dt], [version], [category], [code], [description], [disabled], [order_rank], [title], [type], [url]) VALUES ('402880c54201d09f014201d77dca0003', NULL, NULL, 'QQ#223865F1D22A80490A642BD2CE61E494', '2013-10-29 09:32:48', NULL, NULL, 0, '菜单权限', 'PM071900', '创建菜单自动创建对应权限', '0', 100, '品牌管理', 'MENU', '/md/brand');
GO
INSERT INTO [tbl_auth_privilege] ([id], [acl_code], [acl_type], [created_by], [created_dt], [updated_by], [updated_dt], [version], [category], [code], [description], [disabled], [order_rank], [title], [type], [url]) VALUES ('402880c54201d09f014201d8019b0005', NULL, NULL, 'QQ#223865F1D22A80490A642BD2CE61E494', '2013-10-29 09:33:22', NULL, NULL, 0, '菜单权限', 'PM117030', '创建菜单自动创建对应权限', '0', 100, '商品管理', 'MENU', '/md/commodity');
GO
INSERT INTO [tbl_auth_privilege] ([id], [acl_code], [acl_type], [created_by], [created_dt], [updated_by], [updated_dt], [version], [category], [code], [description], [disabled], [order_rank], [title], [type], [url]) VALUES ('402880c5420d626801420d6f4a170007', NULL, NULL, 'admin', '2013-10-31 15:34:26', NULL, NULL, 0, '菜单权限', 'PM794011', '创建菜单自动创建对应权限', '0', 100, '商家分类管理', 'MENU', '/md/ptcategory');
GO