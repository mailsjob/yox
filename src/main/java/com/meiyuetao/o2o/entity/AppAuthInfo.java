package com.meiyuetao.o2o.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.vip.entity.VipCustomerProfile;

@MetaData("app用户信息")
@Entity
@Table(name = "app_auth_info")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class AppAuthInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("app名")
    private String appName;
    @MetaData("关联用户")
    private VipCustomerProfile customerProfile;
    @MetaData("手机号")
    private String mobile;
    @MetaData("app下载地址")
    private String appDownLoadUrl;
    @MetaData("app包名")
    private String appPackageName;
    @MetaData("备注")
    private String memo;

    @JsonProperty
    @Column(name = "app_name", nullable = false)
    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    @OneToOne
    @JsonProperty
    @JoinColumn(name = "customer_profile_sid", nullable = false)
    public VipCustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(VipCustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @JsonProperty
    @Column(name = "mobile", nullable = false)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonProperty
    @Column(name = "app_down_load_url", nullable = false)
    public String getAppDownLoadUrl() {
        return appDownLoadUrl;
    }

    public void setAppDownLoadUrl(String appDownLoadUrl) {
        this.appDownLoadUrl = appDownLoadUrl;
    }

    @JsonProperty
    @Column(name = "app_package_name", nullable = false)
    public String getAppPackageName() {
        return appPackageName;
    }

    public void setAppPackageName(String appPackageName) {
        this.appPackageName = appPackageName;
    }

    @JsonProperty
    @Column(name = "memo")
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}
