package com.meiyuetao.o2o.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.md.entity.ActivityR2Object.ObjectTypeEnum;
import com.meiyuetao.myt.md.entity.Commodity;

@MetaData("优蜜美店用户晒单")
@Entity
@Table(name = "sns_shared_posts")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class SharedPosts extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("对象类型")
    private ObjectTypeEnum objectType;
    @MetaData("对象商品")
    private Commodity object;
    @MetaData("对象名字")
    private String objectName;
    @MetaData("对象图片")
    private String objectPic;
    @MetaData("对象值")
    private String objectExtValue;
    @MetaData("对象值1")
    private String objectExtValue1;
    @MetaData("晒单内容")
    private String postsContent;
    @MetaData("晒单图1")
    private String postsSharedPic1;
    @MetaData("晒单图2")
    private String postsSharedPic2;
    @MetaData("晒单图3")
    private String postsSharedPic3;
    @MetaData("回复数")
    private Integer postsReplyCount;
    @MetaData("点赞数")
    private Integer postsLoveCount;
    @MetaData("用户")
    private CustomerProfile user;
    @MetaData("审核状态")
    private PostsStatusEnum postsStatus;

    // public enum ObjectTypeEnum {
    // @MetaData("商品")
    // COMMODITY;
    // }

    public enum PostsStatusEnum {
        @MetaData("用户发布")
        PUBLISH,

        @MetaData("通过审核")
        PASS,

        @MetaData("封杀")
        KILL;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "object_type")
    @JsonProperty
    public ObjectTypeEnum getObjectType() {
        return objectType;
    }

    public void setObjectType(ObjectTypeEnum objectType) {
        this.objectType = objectType;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "object_sid")
    @JsonProperty
    public Commodity getObject() {
        return object;
    }

    public void setObject(Commodity object) {
        this.object = object;
    }

    @Column(name = "object_name")
    @JsonProperty
    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    @Column(name = "object_pic")
    @JsonProperty
    public String getObjectPic() {
        return objectPic;
    }

    public void setObjectPic(String objectPic) {
        this.objectPic = objectPic;
    }

    @Column(name = "object_ext_value")
    @JsonProperty
    public String getObjectExtValue() {
        return objectExtValue;
    }

    public void setObjectExtValue(String objectExtValue) {
        this.objectExtValue = objectExtValue;
    }

    @Column(name = "object_ext_value1")
    @JsonProperty
    public String getObjectExtValue1() {
        return objectExtValue1;
    }

    public void setObjectExtValue1(String objectExtValue1) {
        this.objectExtValue1 = objectExtValue1;
    }

    @Column(name = "posts_content")
    @JsonProperty
    public String getPostsContent() {
        return postsContent;
    }

    public void setPostsContent(String postsContent) {
        this.postsContent = postsContent;
    }

    @Column(name = "posts_shared_pic1")
    @JsonProperty
    public String getPostsSharedPic1() {
        return postsSharedPic1;
    }

    public void setPostsSharedPic1(String postsSharedPic1) {
        this.postsSharedPic1 = postsSharedPic1;
    }

    @Column(name = "posts_shared_pic2")
    @JsonProperty
    public String getPostsSharedPic2() {
        return postsSharedPic2;
    }

    public void setPostsSharedPic2(String postsSharedPic2) {
        this.postsSharedPic2 = postsSharedPic2;
    }

    @Column(name = "posts_shared_pic3")
    @JsonProperty
    public String getPostsSharedPic3() {
        return postsSharedPic3;
    }

    public void setPostsSharedPic3(String postsSharedPic3) {
        this.postsSharedPic3 = postsSharedPic3;
    }

    @Column(name = "posts_reply_count")
    @JsonProperty
    public Integer getPostsReplyCount() {
        return postsReplyCount;
    }

    public void setPostsReplyCount(Integer postsReplyCount) {
        this.postsReplyCount = postsReplyCount;
    }

    @Column(name = "posts_love_count")
    @JsonProperty
    public Integer getPostsLoveCount() {
        return postsLoveCount;
    }

    public void setPostsLoveCount(Integer postsLoveCount) {
        this.postsLoveCount = postsLoveCount;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "user_sid")
    @JsonProperty
    public CustomerProfile getUser() {
        return user;
    }

    public void setUser(CustomerProfile user) {
        this.user = user;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "posts_status")
    @JsonProperty
    public PostsStatusEnum getPostsStatus() {
        return postsStatus;
    }

    public void setPostsStatus(PostsStatusEnum postsStatus) {
        this.postsStatus = postsStatus;
    }

    @Transient
    @JsonProperty
    public String getDisplay() {
        if (StringUtils.isNotBlank(this.objectName)) {
            return this.objectName;
        }
        return id.toString();
    }

}
