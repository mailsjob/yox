package com.meiyuetao.o2o.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("美月淘优蜜美店")
@Entity
@Table(name = "o2o_store")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Store extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("老板")
    private CustomerProfile boss;
    @MetaData("收银员")
    private CustomerProfile cashier;
    @MetaData("发货员")
    private CustomerProfile delivery;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "boss_sid")
    @JsonProperty
    public CustomerProfile getBoss() {
        return boss;
    }

    public void setBoss(CustomerProfile boss) {
        this.boss = boss;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "cashier_sid")
    @JsonProperty
    public CustomerProfile getCashier() {
        return cashier;
    }

    public void setCashier(CustomerProfile cashier) {
        this.cashier = cashier;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "delivery_sid")
    @JsonProperty
    public CustomerProfile getDelivery() {
        return delivery;
    }

    public void setDelivery(CustomerProfile delivery) {
        this.delivery = delivery;
    }
}
