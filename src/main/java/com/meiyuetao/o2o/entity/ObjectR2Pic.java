package com.meiyuetao.o2o.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("图片管理")
@Entity
@Table(name = "iyb_object_r2_pic")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ObjectR2Pic extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("图片地址MD5")
    private String picUrl;
    @MetaData("SID")
    private Long objectSid;
    @MetaData("排序号")
    private Integer orderIndex = 100;
    @MetaData("图片类型")
    private ObjectPicTypeEnum objectType;

    public enum ObjectPicTypeEnum {
        @MetaData("店铺头像")
        SHOP_HEAD_PHOTO,

        @MetaData("店铺主页图片")
        SHOP_PAGE_PIC;
    }

    @JsonProperty
    @Column(name = "pic_url", nullable = false)
    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    @JsonProperty
    @Column(name = "object_sid", nullable = false)
    public Long getObjectSid() {
        return objectSid;
    }

    public void setObjectSid(Long objectSid) {
        this.objectSid = objectSid;
    }

    @JsonProperty
    @Enumerated(EnumType.STRING)
    @Column(name = "object_type", nullable = false)
    public ObjectPicTypeEnum getObjectType() {
        return objectType;
    }

    public void setObjectType(ObjectPicTypeEnum objectType) {
        this.objectType = objectType;
    }

    @JsonProperty
    @Column(name = "order_index", nullable = false)
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }
}
