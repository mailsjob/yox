package com.meiyuetao.o2o.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Category.SiteTypeEnum;
import com.meiyuetao.myt.vip.entity.VipCustomerProfile;

@MetaData("app内嵌优蜜")
@Entity
@Table(name = "myt_site_statistics")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class MytSiteStatistics extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联客户")
    private VipCustomerProfile vipCustomerProfile;
    @MetaData("站点类型")
    private SiteTypeEnum siteType;
    @MetaData("操作日期")
    private Date perationDate;
    @MetaData("IP")
    private String ip;
    @MetaData("编号")
    private Integer no;
    @MetaData("URL")
    private String rawUrl;

    @OneToOne
    @JoinColumn(name = "customer_profile_sid", nullable = false)
    public VipCustomerProfile getVipCustomerProfile() {
        return vipCustomerProfile;
    }

    public void setVipCustomerProfile(VipCustomerProfile vipCustomerProfile) {
        this.vipCustomerProfile = vipCustomerProfile;
    }

    @Column(name = "site_type", nullable = false)
    public SiteTypeEnum getSiteType() {
        return siteType;
    }

    public void setSiteType(SiteTypeEnum siteType) {
        this.siteType = siteType;
    }

    @Column(name = "peration_date", nullable = false)
    public Date getPerationDate() {
        return perationDate;
    }

    public void setPerationDate(Date perationDate) {
        this.perationDate = perationDate;
    }

    @Column(name = "ip", nullable = false)
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Column(name = "no", nullable = false)
    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    @Column(name = "raw_url", nullable = false)
    public String getRawUrl() {
        return rawUrl;
    }

    public void setRawUrl(String rawUrl) {
        this.rawUrl = rawUrl;
    }

}
