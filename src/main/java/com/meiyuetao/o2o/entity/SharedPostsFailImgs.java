package com.meiyuetao.o2o.entity;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import lab.s2jh.core.annotation.MetaData;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * Created by zhuhui on 16-1-25.
 */
@MetaData("优蜜美店用户晒单失败图")
@Entity
@Table(name = "sns_shared_posts_fail_imgs")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class SharedPostsFailImgs extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("晒单")
    private SharedPosts post;
    @MetaData("服务")
    private String serverId;
    @MetaData("密令")
    private String token;
    @MetaData("是否可显示")
    private Boolean hasPerformed;
    @MetaData("图片索引")
    private String imgIndex;

    @OneToOne
    @JoinColumn(name = "posts_id")
    public SharedPosts getPost() {
        return post;
    }

    public void setPost(SharedPosts post) {
        this.post = post;
    }

    @Column(name = "server_id")
    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Column(name = "has_performed")
    public Boolean getHasPerformed() {
        return hasPerformed;
    }

    public void setHasPerformed(Boolean hasPerformed) {
        this.hasPerformed = hasPerformed;
    }


    @Column(name = "img_index")
    public String getImgIndex() {
        return imgIndex;
    }

    public void setImgIndex(String imgIndex) {
        this.imgIndex = imgIndex;
    }
}
