package com.meiyuetao.o2o.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("用户评论管理")
@Entity
@Table(name = "sns_shared_posts_reply")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class SharedPostsReply extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("用户晒单")
    private SharedPosts sharedPosts;
    @MetaData("客户")
    private CustomerProfile user;

    // @MetaData("reply")
    // private SharedPostsReply replySid;
    // @MetaData("reply_to_user_id")
    // private WeixinMsgReply replyToUserId;

    @MetaData("评论内容")
    private String replyContent;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "share_posts_sid")
    @JsonProperty
    public SharedPosts getSharedPosts() {
        return sharedPosts;
    }

    public void setSharedPosts(SharedPosts sharedPosts) {
        this.sharedPosts = sharedPosts;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "user_sid")
    @JsonProperty
    public CustomerProfile getUser() {
        return user;
    }

    public void setUser(CustomerProfile user) {
        this.user = user;
    }

    @Column(name = "reply_content", length = 1024)
    @JsonProperty
    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

}
