package com.meiyuetao.o2o.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;
import lab.s2jh.core.annotation.MetaData;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * Created by zhuhui on 16-4-25.
 */

@MetaData("直播相关商品")
@Entity
@Table(name = "live_r2_commodity")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class LiveR2Commodity extends MytBaseEntity {
    @MetaData("直播信息")
    private LiveInfo live;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("商品图")
    private String commodityImg;
    @MetaData("排序号")
    private Integer orderIndex;
    @MetaData("是否可用")
    private Boolean enable;

    @JsonProperty
    @OneToOne
    @JoinColumn(name = "live_sid", nullable = false)
    public LiveInfo getLive() {
        return live;
    }

    public void setLive(LiveInfo live) {
        this.live = live;
    }

    @JsonProperty
    @OneToOne
    @JoinColumn(name = "commodity_sid", nullable = false)
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public String getCommodityImg() {
        return commodityImg;
    }

    public void setCommodityImg(String commodityImg) {
        this.commodityImg = commodityImg;
    }

    @JsonProperty
    @Column(name = "order_index", nullable = false)
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @JsonProperty
    @Column(name = "enable", nullable = false)
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
