package com.meiyuetao.o2o.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.md.entity.Commodity;

@MetaData("店铺商品关联管理")
@Entity
@Table(name = "vip_home_index")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class VipHomeIndex extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("附近店铺")
    private CustomerProfile customerProfile;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("排序")
    private Integer orderIndex;

    @ManyToOne
    @JsonProperty
    @JoinColumn(name = "customer_profile_sid")
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @ManyToOne
    @JsonProperty
    @JoinColumn(name = "commodity_sid")
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }
}
