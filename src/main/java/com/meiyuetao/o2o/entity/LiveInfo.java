package com.meiyuetao.o2o.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by zhuhui on 16-4-22.
 */

@MetaData(value = "直播信息")
@Entity
@Table(name = "live_info")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class LiveInfo extends MytBaseEntity {
    @MetaData("开始时间")
    private Date startTime;
    @MetaData("结束时间")
    private Date endTime;
    @MetaData("直播图片")
    private String livePic;
    @MetaData("直播链接")
    private String liveLink;
    @MetaData("排序号")
    private Integer orderIndex;
    @MetaData("是否可用")
    private Boolean enable;

    @JsonProperty
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @Column(name = "start_time", nullable = false)
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @JsonProperty
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @Column(name = "end_time", nullable = false)
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @JsonProperty
    @Column(name = "live_pic", nullable = false)
    public String getLivePic() {
        return livePic;
    }

    public void setLivePic(String livePic) {
        this.livePic = livePic;
    }

    @JsonProperty
    @Column(name = "live_link", nullable = false)
    public String getLiveLink() {
        return liveLink;
    }

    public void setLiveLink(String liveLink) {
        this.liveLink = liveLink;
    }

    @JsonProperty
    @Column(name = "order_index", nullable = false)
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @JsonProperty
    @Column(name = "enable", nullable = false)
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
