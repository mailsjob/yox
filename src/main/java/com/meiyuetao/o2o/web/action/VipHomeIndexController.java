package com.meiyuetao.o2o.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.o2o.entity.VipHomeIndex;
import com.meiyuetao.o2o.service.VipHomeIndexService;

@MetaData("店铺商品关联管理管理")
public class VipHomeIndexController extends BaseController<VipHomeIndex, Long> {

    @Autowired
    private VipHomeIndexService vipHomeIndexService;

    @Override
    protected BaseService<VipHomeIndex, Long> getEntityService() {
        return vipHomeIndexService;
    }

    @Override
    protected void checkEntityAclPermission(VipHomeIndex entity) {
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}