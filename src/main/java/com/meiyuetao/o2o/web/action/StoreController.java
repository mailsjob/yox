package com.meiyuetao.o2o.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.customer.service.CustomerProfileService;
import com.meiyuetao.o2o.entity.Store;
import com.meiyuetao.o2o.service.StoreService;

@MetaData("美月淘优蜜美店管理")
public class StoreController extends BaseController<Store, Long> {

    @Autowired
    private StoreService storeService;

    @Autowired
    private CustomerProfileService customerProfileService;

    @Override
    protected BaseService<Store, Long> getEntityService() {
        return storeService;
    }

    @Override
    protected void checkEntityAclPermission(Store entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        String boss = getParameter("boss.display");
        if (StringUtils.isNullOrEmpty(boss)) {
            bindingEntity.setBoss(null);
        } else {
            String bossId = getParameter("boss.id");
            bindingEntity.setBoss(customerProfileService.findOne(Long.valueOf(bossId)));
        }
        String cashier = getParameter("cashier.display");
        if (StringUtils.isNullOrEmpty(cashier)) {
            bindingEntity.setCashier(null);
        } else {
            String cashierId = getParameter("cashier.id");
            bindingEntity.setCashier(customerProfileService.findOne(Long.valueOf(cashierId)));
        }
        String delivery = getParameter("delivery.display");
        if (StringUtils.isNullOrEmpty(delivery)) {
            bindingEntity.setDelivery(null);
        } else {
            String deliveryId = getParameter("delivery.id");
            bindingEntity.setDelivery(customerProfileService.findOne(Long.valueOf(deliveryId)));
        }

        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}