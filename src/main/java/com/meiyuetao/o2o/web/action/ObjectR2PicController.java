package com.meiyuetao.o2o.web.action;

import lab.s2jh.core.annotation.MetaData;
import com.meiyuetao.o2o.entity.ObjectR2Pic;
import com.meiyuetao.o2o.service.ObjectR2PicService;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

@MetaData("优蜜美店用户晒单管理")
public class ObjectR2PicController extends BaseController<ObjectR2Pic,Long> {

    @Autowired
    private ObjectR2PicService objectR2PicService;

    @Override
    protected BaseService<ObjectR2Pic, Long> getEntityService() {
        return objectR2PicService;
    }
    
    @Override
    protected void checkEntityAclPermission(ObjectR2Pic entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        //TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }
    
    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }
    
    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}