package com.meiyuetao.o2o.web.action;

import com.meiyuetao.o2o.entity.LiveR2Commodity;
import com.meiyuetao.o2o.service.LiveR2CommodityService;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

@MetaData("直播相关商品管理")
public class LiveR2CommodityController extends BaseController<LiveR2Commodity,Long> {

    @Autowired
    private LiveR2CommodityService liveR2CommodityService;

    @Override
    protected BaseService<LiveR2Commodity, Long> getEntityService() {
        return liveR2CommodityService;
    }
    
    @Override
    protected void checkEntityAclPermission(LiveR2Commodity entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        //TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }
    
    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }
    
    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}