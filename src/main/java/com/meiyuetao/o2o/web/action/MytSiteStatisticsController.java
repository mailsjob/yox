package com.meiyuetao.o2o.web.action;

import java.util.Date;
import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.util.DateUtils;
import lab.s2jh.web.action.BaseController;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.vip.entity.VipCustomerProfile;
import com.meiyuetao.myt.vip.service.VipCustomerProfileService;
import com.meiyuetao.o2o.entity.MytSiteStatistics;
import com.meiyuetao.o2o.service.MytSiteStatisticsService;

@MetaData("app内嵌优蜜管理")
public class MytSiteStatisticsController extends BaseController<MytSiteStatistics, Long> {

    @Autowired
    private MytSiteStatisticsService mytSiteStatisticsService;
    @Autowired
    private VipCustomerProfileService vipCustomerProfileService;

    @Override
    protected BaseService<MytSiteStatistics, Long> getEntityService() {
        return mytSiteStatisticsService;
    }

    @Override
    protected void checkEntityAclPermission(MytSiteStatistics entity) {
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    public void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        String date = this.getParameter("date");
        String[] dates = date.split("～");
        Date dateFrom;
        Date dateTo;
        if (StringUtils.isNotBlank(date) && dates.length >= 2) {
            dateFrom = DateUtils.parseDate(dates[0].trim());
            dateTo = DateUtils.parseDate(dates[1].trim());
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.GE, "createdDate", dateFrom));
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.LT, "createdDate", dateTo));
        } else {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.LT, "createdDate", new Date()));
        }
    }

    private void appendFilterProUps(GroupPropertyFilter groupFilter) {
        String vipCustomerId = this.getParameter("vipCustomerProfile.id");
        if (StringUtils.isNotEmpty(vipCustomerId)) {
            List<VipCustomerProfile> lst = vipCustomerProfileService.findByUpstream(Long.valueOf(vipCustomerId));
            if (CollectionUtils.isNotEmpty(lst)) {
                groupFilter.forceAnd(new PropertyFilter(MatchType.IN, "vipCustomerProfile", lst));
            } else {
                groupFilter.forceAnd(new PropertyFilter(MatchType.LT, "id", 0));
            }
        }
    }

    public HttpHeaders findByDate() {
        GroupPropertyFilter groupFilter = GroupPropertyFilter.buildFromHttpRequest(entityClass, getRequest());
        appendFilterProperty(groupFilter);
        appendFilterProUps(groupFilter);
        setModel(findByGroupAggregate(groupFilter, "count(id)"));
        return buildDefaultHttpHeaders();
    }

}