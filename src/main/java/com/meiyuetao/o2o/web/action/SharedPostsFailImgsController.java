package com.meiyuetao.o2o.web.action;

import lab.s2jh.core.annotation.MetaData;
import com.meiyuetao.o2o.entity.SharedPostsFailImgs;
import com.meiyuetao.o2o.service.SharedPostsFailImgsService;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

@MetaData("优蜜美店用户晒单失败图管理")
public class SharedPostsFailImgsController extends BaseController<SharedPostsFailImgs,Long> {

    @Autowired
    private SharedPostsFailImgsService sharedPostsFailImgsService;

    @Override
    protected BaseService<SharedPostsFailImgs, Long> getEntityService() {
        return sharedPostsFailImgsService;
    }
    
    @Override
    protected void checkEntityAclPermission(SharedPostsFailImgs entity) {
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }
    
    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}