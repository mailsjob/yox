package com.meiyuetao.o2o.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.o2o.entity.SharedPosts;
import com.meiyuetao.o2o.entity.SharedPostsReply;
import com.meiyuetao.o2o.service.SharedPostsReplyService;
import com.meiyuetao.o2o.service.SharedPostsService;

@MetaData("用户评论管理管理")
public class SharedPostsReplyController extends BaseController<SharedPostsReply, Long> {

    @Autowired
    private SharedPostsReplyService sharedPostsReplyService;

    @Autowired
    private SharedPostsService sharedPostsService;

    @Override
    protected BaseService<SharedPostsReply, Long> getEntityService() {
        return sharedPostsReplyService;
    }

    @Override
    protected void checkEntityAclPermission(SharedPostsReply entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        String sharedPostsId = super.getParameter("sharedPosts.id");
        String sharedPostsdis = super.getParameter("sharedPosts.display");
        if (!StringUtils.isNullOrEmpty(sharedPostsdis)) {
            SharedPosts sharedPosts = sharedPostsService.findOne(Long.valueOf(sharedPostsId));
            bindingEntity.setSharedPosts(sharedPosts);
        } else {
            bindingEntity.setSharedPosts(null);
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}