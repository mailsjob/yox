package com.meiyuetao.o2o.web.action;

import com.meiyuetao.o2o.entity.LiveInfo;
import com.meiyuetao.o2o.service.LiveInfoService;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

@MetaData("直播信息管理")
public class LiveInfoController extends BaseController<LiveInfo,Long> {

    @Autowired
    private LiveInfoService liveInfoService;

    @Override
    protected BaseService<LiveInfo, Long> getEntityService() {
        return liveInfoService;
    }
    
    @Override
    protected void checkEntityAclPermission(LiveInfo entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        //TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }
    
    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }
    
    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}