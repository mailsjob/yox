package com.meiyuetao.o2o.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.o2o.entity.AppAuthInfo;
import com.meiyuetao.o2o.service.AppAuthInfoService;

@MetaData("app用户信息管理")
public class AppAuthInfoController extends BaseController<AppAuthInfo, Long> {

    @Autowired
    private AppAuthInfoService appAuthInfoService;

    @Override
    protected BaseService<AppAuthInfo, Long> getEntityService() {
        return appAuthInfoService;
    }

    @Override
    protected void checkEntityAclPermission(AppAuthInfo entity) {
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}