package com.meiyuetao.o2o.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.o2o.entity.Store;

@Repository
public interface StoreDao extends BaseDao<Store, Long> {

}