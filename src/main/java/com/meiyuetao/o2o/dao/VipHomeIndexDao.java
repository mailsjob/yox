package com.meiyuetao.o2o.dao;

import com.meiyuetao.o2o.entity.VipHomeIndex;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface VipHomeIndexDao extends BaseDao<VipHomeIndex, Long> {

}