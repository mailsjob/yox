package com.meiyuetao.o2o.dao;

import com.meiyuetao.o2o.entity.SharedPostsFailImgs;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface SharedPostsFailImgsDao extends BaseDao<SharedPostsFailImgs, Long> {

}