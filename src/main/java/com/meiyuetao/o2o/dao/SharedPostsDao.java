package com.meiyuetao.o2o.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.o2o.entity.SharedPosts;

@Repository
public interface SharedPostsDao extends BaseDao<SharedPosts, Long> {

}