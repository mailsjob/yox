package com.meiyuetao.o2o.dao;

import com.meiyuetao.o2o.entity.AppAuthInfo;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface AppAuthInfoDao extends BaseDao<AppAuthInfo, Long> {

}