package com.meiyuetao.o2o.dao;

import com.meiyuetao.o2o.entity.LiveR2Commodity;
import lab.s2jh.core.dao.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public interface LiveR2CommodityDao extends BaseDao<LiveR2Commodity, Long> {

}