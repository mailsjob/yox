package com.meiyuetao.o2o.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.o2o.entity.SharedPostsReply;

@Repository
public interface SharedPostsReplyDao extends BaseDao<SharedPostsReply, Long> {

}