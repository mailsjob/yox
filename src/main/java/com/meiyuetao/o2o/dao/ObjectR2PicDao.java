package com.meiyuetao.o2o.dao;

import com.meiyuetao.o2o.entity.ObjectR2Pic;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface ObjectR2PicDao extends BaseDao<ObjectR2Pic, Long> {

}