package com.meiyuetao.o2o.service;

import com.meiyuetao.o2o.dao.LiveInfoDao;
import com.meiyuetao.o2o.entity.LiveInfo;
import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LiveInfoService extends BaseService<LiveInfo,Long>{
    
    @Autowired
    private LiveInfoDao liveInfoDao;

    @Override
    protected BaseDao<LiveInfo, Long> getEntityDao() {
        return liveInfoDao;
    }
}
