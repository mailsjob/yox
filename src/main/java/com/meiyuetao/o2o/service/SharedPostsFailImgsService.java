package com.meiyuetao.o2o.service;

import com.meiyuetao.o2o.dao.SharedPostsFailImgsDao;
import com.meiyuetao.o2o.entity.SharedPostsFailImgs;
import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SharedPostsFailImgsService extends BaseService<SharedPostsFailImgs, Long> {

    @Autowired
    private SharedPostsFailImgsDao sharedPostsFailImgsDao;

    @Override
    protected BaseDao<SharedPostsFailImgs, Long> getEntityDao() {
        return sharedPostsFailImgsDao;
    }
}
