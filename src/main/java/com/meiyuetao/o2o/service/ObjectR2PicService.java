package com.meiyuetao.o2o.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import com.meiyuetao.o2o.entity.ObjectR2Pic;
import com.meiyuetao.o2o.dao.ObjectR2PicDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ObjectR2PicService extends BaseService<ObjectR2Pic,Long>{
    
    @Autowired
    private ObjectR2PicDao objectR2PicDao;

    @Override
    protected BaseDao<ObjectR2Pic, Long> getEntityDao() {
        return objectR2PicDao;
    }
}
