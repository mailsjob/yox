package com.meiyuetao.o2o.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.o2o.dao.SharedPostsDao;
import com.meiyuetao.o2o.entity.SharedPosts;

@Service
@Transactional
public class SharedPostsService extends BaseService<SharedPosts, Long> {

    @Autowired
    private SharedPostsDao sharedPostsDao;

    @Override
    protected BaseDao<SharedPosts, Long> getEntityDao() {
        return sharedPostsDao;
    }
}
