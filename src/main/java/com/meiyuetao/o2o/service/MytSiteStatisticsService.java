package com.meiyuetao.o2o.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import com.meiyuetao.o2o.entity.MytSiteStatistics;
import com.meiyuetao.o2o.dao.MytSiteStatisticsDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MytSiteStatisticsService extends BaseService<MytSiteStatistics,Long>{
    
    @Autowired
    private MytSiteStatisticsDao mytSiteStatisticsDao;

    @Override
    protected BaseDao<MytSiteStatistics, Long> getEntityDao() {
        return mytSiteStatisticsDao;
    }
}
