package com.meiyuetao.o2o.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import com.meiyuetao.o2o.entity.VipHomeIndex;
import com.meiyuetao.o2o.dao.VipHomeIndexDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class VipHomeIndexService extends BaseService<VipHomeIndex,Long>{
    
    @Autowired
    private VipHomeIndexDao vipHomeIndexDao;

    @Override
    protected BaseDao<VipHomeIndex, Long> getEntityDao() {
        return vipHomeIndexDao;
    }
}
