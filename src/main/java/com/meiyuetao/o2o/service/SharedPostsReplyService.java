package com.meiyuetao.o2o.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.o2o.dao.SharedPostsReplyDao;
import com.meiyuetao.o2o.entity.SharedPostsReply;

@Service
@Transactional
public class SharedPostsReplyService extends BaseService<SharedPostsReply, Long> {

    @Autowired
    private SharedPostsReplyDao sharedPostsReplyDao;

    @Override
    protected BaseDao<SharedPostsReply, Long> getEntityDao() {
        return sharedPostsReplyDao;
    }
}
