package com.meiyuetao.o2o.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.o2o.dao.StoreDao;
import com.meiyuetao.o2o.entity.Store;

@Service
@Transactional
public class StoreService extends BaseService<Store, Long> {

    @Autowired
    private StoreDao storeDao;

    @Override
    protected BaseDao<Store, Long> getEntityDao() {
        return storeDao;
    }
}
