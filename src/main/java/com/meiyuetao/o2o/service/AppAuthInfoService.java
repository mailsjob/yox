package com.meiyuetao.o2o.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import com.meiyuetao.o2o.entity.AppAuthInfo;
import com.meiyuetao.o2o.dao.AppAuthInfoDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AppAuthInfoService extends BaseService<AppAuthInfo,Long>{
    
    @Autowired
    private AppAuthInfoDao appAuthInfoDao;

    @Override
    protected BaseDao<AppAuthInfo, Long> getEntityDao() {
        return appAuthInfoDao;
    }
}
