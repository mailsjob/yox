package com.meiyuetao.o2o.service;

import com.meiyuetao.o2o.dao.LiveR2CommodityDao;
import com.meiyuetao.o2o.entity.LiveR2Commodity;
import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LiveR2CommodityService extends BaseService<LiveR2Commodity,Long>{
    
    @Autowired
    private LiveR2CommodityDao liveR2CommodityDao;

    @Override
    protected BaseDao<LiveR2Commodity, Long> getEntityDao() {
        return liveR2CommodityDao;
    }
}
