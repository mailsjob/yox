package com.meiyuetao.myt.yryd.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.yryd.entity.YrydStore;
import com.meiyuetao.myt.yryd.service.YrydStoreService;

@MetaData("一人一店管理")
public class YrydStoreController extends MytBaseController<YrydStore, Long> {

    @Autowired
    private YrydStoreService yrydStoreService;

    @Override
    protected BaseService<YrydStore, Long> getEntityService() {
        return yrydStoreService;
    }

    @Override
    protected void checkEntityAclPermission(YrydStore entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}