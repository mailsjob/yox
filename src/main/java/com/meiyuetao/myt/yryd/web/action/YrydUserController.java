package com.meiyuetao.myt.yryd.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.yryd.entity.YrydUser;
import com.meiyuetao.myt.yryd.entity.YrydUser.YrydAuditStatusEnum;
import com.meiyuetao.myt.yryd.service.YrydUserService;

@MetaData("一人一店用户管理")
public class YrydUserController extends MytBaseController<YrydUser, Long> {

    @Autowired
    private YrydUserService yrydUserService;

    @Override
    protected BaseService<YrydUser, Long> getEntityService() {
        return yrydUserService;
    }

    @Override
    protected void checkEntityAclPermission(YrydUser entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    public HttpHeaders doAudit() {
        List<YrydUser> yrydUsers = null;
        Long[] ids = this.getSelectSids();
        if (ids != null && ids.length > 0) {
            yrydUsers = yrydUserService.findAll(ids);
        } else {
            GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
            yrydUsers = yrydUserService.findByFilters(groupPropertyFilter);
        }
        int i = 0;
        for (YrydUser yrydUser : yrydUsers) {
            if (YrydAuditStatusEnum.A10UNSUBMIT.equals(yrydUser.getAuditStatus())) {
                yrydUser.setAuditUser(getLogonUser());
                yrydUserService.doAudit(yrydUser);
                i++;
            }
        }
        setModel(OperationResult.buildSuccessResult("完成审核数据：" + i + "条"));
        return buildDefaultHttpHeaders();
    }

}