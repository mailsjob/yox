package com.meiyuetao.myt.yryd.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.ctx.DynamicConfigService;
import lab.s2jh.ctx.FreemarkerService;
import lab.s2jh.ctx.MailService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.core.service.SolrService;
import com.meiyuetao.myt.customer.service.CustomerProfileService;
import com.meiyuetao.myt.customer.service.SmsDetailLogService;
import com.meiyuetao.myt.job.BusinessNotifyService;
import com.meiyuetao.myt.yryd.dao.YrydUserDao;
import com.meiyuetao.myt.yryd.entity.YrydStore;
import com.meiyuetao.myt.yryd.entity.YrydStore.StoreStatusEnum;
import com.meiyuetao.myt.yryd.entity.YrydUser;
import com.meiyuetao.myt.yryd.entity.YrydUser.YrydAuditStatusEnum;

@Service
@Transactional
public class YrydUserService extends BaseService<YrydUser, Long> {

    @Autowired
    private YrydUserDao yrydUserDao;

    @Autowired
    private BusinessNotifyService businessNotifyService;
    @Autowired
    private DynamicConfigService dynamicConfigService;
    @Autowired
    private SmsDetailLogService smsDetailLogService;
    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private YrydStoreService yrydStoreService;
    @Autowired
    private SolrService solrService;
    @Autowired
    private FreemarkerService freemarkerService;
    @Autowired
    private MailService mailService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    // private static int MAX_TRY = 3;

    @Override
    protected BaseDao<YrydUser, Long> getEntityDao() {
        return yrydUserDao;
    }

    @Async
    public void sendMsg(String mobilePhone, String content) {
        businessNotifyService.sendMsg(mobilePhone, content);
    }

    public void doAudit(YrydUser entity) {
        entity.setAuditTime(new Date());
        entity.setAuditStatus(YrydAuditStatusEnum.A30PASSED);
        yrydUserDao.save(entity);
        YrydStore yrydStore = entity.getYrydStore();
        yrydStore.setStoreStatus(StoreStatusEnum.NORMAL);
        yrydStoreService.save(yrydStore);

        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("yrydUser", entity);
        String emailContent = freemarkerService.processTemplateByFileName("YRYD_USER_AUDIT_EMAIL", dataMap);
        String email = entity.getEmail();
        if (StringUtils.isNotBlank(email)) {
            mailService.sendHtmlMail("店铺申请", emailContent, false, email);
        }
        String content = dynamicConfigService.getString("cfg.sms.wlf.audit", "");
        if (StringUtils.isNotBlank(content) && StringUtils.isNotBlank(entity.getMobilePhone())) {
            sendMsg(entity.getMobilePhone(), content);
        }
    }

}
