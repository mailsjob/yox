package com.meiyuetao.myt.yryd.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.yryd.dao.YrydStoreR2CommodityDao;
import com.meiyuetao.myt.yryd.entity.YrydStoreR2Commodity;

@Service
@Transactional
public class YrydStoreR2CommodityService extends BaseService<YrydStoreR2Commodity, Long> {

    @Autowired
    private YrydStoreR2CommodityDao yrydStoreR2CommodityDao;

    @Override
    protected BaseDao<YrydStoreR2Commodity, Long> getEntityDao() {
        return yrydStoreR2CommodityDao;
    }

}
