package com.meiyuetao.myt.yryd.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.yryd.dao.YrydStoreDao;
import com.meiyuetao.myt.yryd.entity.YrydStore;

@Service
@Transactional
public class YrydStoreService extends BaseService<YrydStore, Long> {

    @Autowired
    private YrydStoreDao yrydStoreDao;

    @Override
    protected BaseDao<YrydStore, Long> getEntityDao() {
        return yrydStoreDao;
    }

}
