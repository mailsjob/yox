package com.meiyuetao.myt.yryd.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;

@MetaData("一人一店关联商品")
@Entity
@Table(name = "yryd_store_r2_commodity")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class YrydStoreR2Commodity extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("店铺")
    private YrydStore store;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("价格")
    private BigDecimal price;
    @MetaData("浏览量")
    private Integer viewCount = 0;
    @MetaData("购买量")
    private Integer boughtCount = 0;

    @ManyToOne
    @JoinColumn(name = "store_sid")
    public YrydStore getStore() {
        return store;
    }

    public void setStore(YrydStore store) {
        this.store = store;
    }

    @ManyToOne
    @JoinColumn(name = "commodity_sid")
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getBoughtCount() {
        return boughtCount;
    }

    public void setBoughtCount(Integer boughtCount) {
        this.boughtCount = boughtCount;
    }

}
