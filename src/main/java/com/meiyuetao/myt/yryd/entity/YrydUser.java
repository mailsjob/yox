package com.meiyuetao.myt.yryd.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.auth.entity.User;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.constant.GenericSexEnum;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Region;
import com.meiyuetao.myt.wlf.entity.WlfStudentInfo;

@MetaData("未来付注册用户")
@Entity
@Table(name = "wlf_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YrydUser extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("真实姓名")
    private String trueName;
    // private String nickName;
    // private String studentCardPhoto;
    // private String idCardPhoto;
    @MetaData("身份证号")
    private String idCard;
    // private Date idCardExpiredTime;
    // private Date bornTime;
    @MetaData("入学时间")
    private Date enterTime;
    // private Date leaveTime;
    @MetaData("性别")
    private GenericSexEnum sex;
    // private String schoolName;
    // private String departName;
    @MetaData("教育层次")
    private YrydEduLevelEnum eduLevel;

    public enum YrydEduLevelEnum {
        @MetaData(value = "专科")
        DZ,

        @MetaData(value = "本科")
        BK,

        @MetaData(value = "研究生")
        SS,

        @MetaData(value = "博士")
        BS;
    }

    // private String addr;
    // private String postCode;
    // private String departPhone;
    // private String dormPhone;
    @MetaData("手机号")
    private String mobilePhone;
    private String email;
    // private String awardMemo;
    // private String jobMemo;
    // private String communityMemo;
    // private Boolean partyMember;
    /*
     * private String r1Name; private String r1MobilePhone; private RelationEnum
     * r1Relation; private String r1Company; private String r1CompanyPhone;
     * private String r1HomeAddr; private String r1HomePhone; private String
     * r2Name; private String r2MobilePhone; private RelationEnum r2Relation;
     * private String r2Company; private String r2CompanyPhone; private String
     * r2HomeAddr; private String r2HomePhone; private String r3Name; private
     * String r3MobilePhone; private RelationEnum r3Relation; private String
     * r3Company; private String r3CompanyPhone; private String r3HomeAddr;
     * private String r3HomePhone; private BillAddrEnum billAddr; private
     * ReceiveCardTypeEnum receiveCardType; private BillAddrEnum cardPostAddr;
     * private String v1Name; private String v1NickName; private String
     * v1IdCard; private String v1MobilePhone; private String v1Email; private
     * String v2Name; private String v2NickName; private String v2IdCard;
     * private String v2MobilePhone; private String v2Email; private String
     * referrerName; private String referrerCard;
     */
    @MetaData("申请时间")
    private Date applyTime;
    @MetaData("用户名")
    private String loginid;
    @MetaData("密码")
    private String password;

    @MetaData("授信额度")
    private BigDecimal weilaifuMaxAccount = BigDecimal.ZERO;

    @MetaData("剩余授信额度")
    private BigDecimal weilaifuAccount = BigDecimal.ZERO;

    @MetaData("审核状态")
    private YrydAuditStatusEnum auditStatus;
    @MetaData("审核备注")
    private String auditMemo;
    @MetaData("审核时间")
    private Date auditTime;
    @MetaData("审核人")
    private User auditUser;

    @MetaData(value = "地区")
    private Region region;
    @MetaData("学生信息")
    private WlfStudentInfo studentInfo;
    @MetaData("一人一店")
    private YrydStore yrydStore;

    public enum YrydAuditStatusEnum {
        @MetaData(value = "未提交")
        NONE,

        @MetaData(value = "未审核")
        A10UNSUBMIT,

        @MetaData(value = "审核驳回")
        A20REJECTED,

        @MetaData(value = "审核通过")
        A30PASSED;
    }

    public enum SexEnum {

        @MetaData(value = "男")
        M, @MetaData(value = "女")
        F;
    }

    @Column(nullable = false, unique = true)
    @JsonProperty
    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    @JsonProperty
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(Date enterTime) {
        this.enterTime = enterTime;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 1)
    @JsonProperty
    public GenericSexEnum getSex() {
        return sex;
    }

    public void setSex(GenericSexEnum sex) {
        this.sex = sex;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public YrydEduLevelEnum getEduLevel() {
        return eduLevel;
    }

    public void setEduLevel(YrydEduLevelEnum eduLevel) {
        this.eduLevel = eduLevel;
    }

    @JsonProperty
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(nullable = false, unique = true)
    @JsonProperty
    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    @JsonProperty
    public BigDecimal getWeilaifuMaxAccount() {
        return weilaifuMaxAccount;
    }

    public void setWeilaifuMaxAccount(BigDecimal weilaifuMaxAccount) {
        this.weilaifuMaxAccount = weilaifuMaxAccount;
    }

    public BigDecimal getWeilaifuAccount() {
        return weilaifuAccount;
    }

    public void setWeilaifuAccount(BigDecimal weilaifuAccount) {
        this.weilaifuAccount = weilaifuAccount;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public YrydAuditStatusEnum getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(YrydAuditStatusEnum auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditMemo() {
        return auditMemo;
    }

    public void setAuditMemo(String auditMemo) {
        this.auditMemo = auditMemo;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    @ManyToOne
    @JoinColumn(name = "audit_user_sid")
    @JsonProperty
    public User getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(User auditUser) {
        this.auditUser = auditUser;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_info_sid")
    @NotAudited
    public WlfStudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(WlfStudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "region_sid")
    @NotAudited
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty
    @Column(name = "name")
    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "store_sid", nullable = true)
    @NotAudited
    @JsonProperty
    public YrydStore getYrydStore() {
        return yrydStore;
    }

    public void setYrydStore(YrydStore yrydStore) {
        this.yrydStore = yrydStore;
    }

}
