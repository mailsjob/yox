package com.meiyuetao.myt.yryd.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("一人一店")
@Entity
@Table(name = "yryd_store")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class YrydStore extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("客户")
    private CustomerProfile customerProfile;
    @MetaData("店铺名")
    private String storeName;
    @MetaData("店铺描述")
    private String storeDescription;
    @MetaData("图片1")
    private String showWindowsPic1;
    @MetaData("图片21")
    private String showWindowsPic21;
    @MetaData("图片22")
    private String showWindowsPic22;
    @MetaData("图片23")
    private String showWindowsPic23;
    @MetaData("图片3")
    private String showWindowsPic3;
    @MetaData("客服QQ")
    private String serviceQq;
    @MetaData("店铺状态")
    private StoreStatusEnum storeStatus;

    public enum StoreStatusEnum {
        @MetaData(value = "待审批")
        WAIT,

        @MetaData(value = "正常")
        NORMAL,

        @MetaData(value = "禁用")
        DISABLE;
    }

    @MetaData("提交时间")
    private Date submitTime;
    @MetaData("开店时间")
    private Date openTime;
    @MetaData("图片1链接")
    private String showWindowsPic1Href;
    @MetaData("图片21链接")
    private String showWindowsPic21Href;
    @MetaData("图片22链接")
    private String showWindowsPic22Href;
    @MetaData("图片23链接")
    private String showWindowsPic23Href;
    @MetaData("图片3链接")
    private String showWindowsPic3Href;
    @MetaData("客服电话")
    private String servicePhone;
    @MetaData("浏览量")
    private Integer viewCount = 0;
    @MetaData("购买量")
    private Integer boughtCount = 0;

    @OneToOne
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Column(length = 32, unique = true, updatable = false, nullable = false)
    @JsonProperty
    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreDescription() {
        return storeDescription;
    }

    public void setStoreDescription(String storeDescription) {
        this.storeDescription = storeDescription;
    }

    @JsonProperty
    @Column(nullable = false)
    public String getShowWindowsPic1() {
        return showWindowsPic1;
    }

    public void setShowWindowsPic1(String showWindowsPic1) {
        this.showWindowsPic1 = showWindowsPic1;
    }

    @JsonProperty
    @Column(nullable = false)
    public String getShowWindowsPic21() {
        return showWindowsPic21;
    }

    public void setShowWindowsPic21(String showWindowsPic21) {
        this.showWindowsPic21 = showWindowsPic21;
    }

    @JsonProperty
    @Column(nullable = false)
    public String getShowWindowsPic22() {
        return showWindowsPic22;
    }

    public void setShowWindowsPic22(String showWindowsPic22) {
        this.showWindowsPic22 = showWindowsPic22;
    }

    @JsonProperty
    @Column(nullable = false)
    public String getShowWindowsPic23() {
        return showWindowsPic23;
    }

    public void setShowWindowsPic23(String showWindowsPic23) {
        this.showWindowsPic23 = showWindowsPic23;
    }

    @JsonProperty
    @Column(nullable = false)
    public String getShowWindowsPic3() {
        return showWindowsPic3;
    }

    public void setShowWindowsPic3(String showWindowsPic3) {
        this.showWindowsPic3 = showWindowsPic3;
    }

    @JsonProperty
    @Column(nullable = false)
    public String getServiceQq() {
        return serviceQq;
    }

    public void setServiceQq(String serviceQq) {
        this.serviceQq = serviceQq;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public StoreStatusEnum getStoreStatus() {
        return storeStatus;
    }

    public void setStoreStatus(StoreStatusEnum storeStatus) {
        this.storeStatus = storeStatus;
    }

    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    @Column(name = "show_windows_pic1_href", nullable = false)
    public String getShowWindowsPic1Href() {
        return showWindowsPic1Href;
    }

    public void setShowWindowsPic1Href(String showWindowsPic1Href) {
        this.showWindowsPic1Href = showWindowsPic1Href;
    }

    @Column(name = "show_windows_pic21_href", nullable = false)
    public String getShowWindowsPic21Href() {
        return showWindowsPic21Href;
    }

    public void setShowWindowsPic21Href(String showWindowsPic21Href) {
        this.showWindowsPic21Href = showWindowsPic21Href;
    }

    @Column(name = "show_windows_pic22_href", nullable = false)
    public String getShowWindowsPic22Href() {
        return showWindowsPic22Href;
    }

    public void setShowWindowsPic22Href(String showWindowsPic22Href) {
        this.showWindowsPic22Href = showWindowsPic22Href;
    }

    @Column(name = "show_windows_pic23_href", nullable = false)
    public String getShowWindowsPic23Href() {
        return showWindowsPic23Href;
    }

    public void setShowWindowsPic23Href(String showWindowsPic23Href) {
        this.showWindowsPic23Href = showWindowsPic23Href;
    }

    @Column(name = "show_windows_pic3_href", nullable = false)
    public String getShowWindowsPic3Href() {
        return showWindowsPic3Href;
    }

    public void setShowWindowsPic3Href(String showWindowsPic3Href) {
        this.showWindowsPic3Href = showWindowsPic3Href;
    }

    @JsonProperty
    @Column(nullable = false)
    public String getServicePhone() {
        return servicePhone;
    }

    public void setServicePhone(String servicePhone) {
        this.servicePhone = servicePhone;
    }

    @JsonProperty
    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    @JsonProperty
    public Integer getBoughtCount() {
        return boughtCount;
    }

    public void setBoughtCount(Integer boughtCount) {
        this.boughtCount = boughtCount;
    }

}
