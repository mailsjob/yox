package com.meiyuetao.myt.yryd.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.yryd.entity.YrydStore;

@Repository
public interface YrydStoreDao extends BaseDao<YrydStore, Long> {

}