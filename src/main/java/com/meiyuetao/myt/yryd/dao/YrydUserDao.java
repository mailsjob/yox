package com.meiyuetao.myt.yryd.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.yryd.entity.YrydUser;

@Repository
public interface YrydUserDao extends BaseDao<YrydUser, Long> {

}