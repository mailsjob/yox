package com.meiyuetao.myt.yryd.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.yryd.entity.YrydStoreR2Commodity;

@Repository
public interface YrydStoreR2CommodityDao extends BaseDao<YrydStoreR2Commodity, Long> {

}