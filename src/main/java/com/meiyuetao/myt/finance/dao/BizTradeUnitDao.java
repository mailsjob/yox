package com.meiyuetao.myt.finance.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.finance.entity.BizTradeUnit;

@Repository
public interface BizTradeUnitDao extends BaseDao<BizTradeUnit, String> {

}