package com.meiyuetao.myt.finance.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.finance.entity.PayNotifyR2SaleDelivery;

@Repository
public interface PayNotifyR2SaleDeliveryDao extends BaseDao<PayNotifyR2SaleDelivery, Long> {

}