package com.meiyuetao.myt.finance.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.core.constant.VoucherTypeEnum;
import com.meiyuetao.myt.finance.entity.AccountInOut;

@Repository
public interface AccountInOutDao extends BaseDao<AccountInOut, Long> {

    List<AccountInOut> findByVoucherAndVoucherType(String voucher, VoucherTypeEnum voucherType);

}