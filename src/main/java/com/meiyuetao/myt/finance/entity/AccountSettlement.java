package com.meiyuetao.myt.finance.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("结存账期")
@Entity
@Table(name = "myt_finance_account_settlement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AccountSettlement extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("开始日期")
    private Date fromDate;
    @MetaData("结束日期")
    private Date toDate;
    @MetaData("标题")
    private String title;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
