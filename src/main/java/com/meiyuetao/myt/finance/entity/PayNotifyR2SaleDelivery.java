package com.meiyuetao.myt.finance.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.sale.entity.SaleDelivery;

@Entity
@MetaData("付款记录关联销售单")
@Table(name = "iyb_pay_notify_r2_sale_delivery")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class PayNotifyR2SaleDelivery extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("付款记录")
    private PayNotifyHistory payNotifyHistory;
    @MetaData("关联销售单")
    private SaleDelivery saleDelivery;

    @OneToOne
    @JoinColumn(name = "pay_notify_history_sid", nullable = false)
    public PayNotifyHistory getPayNotifyHistory() {
        return payNotifyHistory;
    }

    public void setPayNotifyHistory(PayNotifyHistory payNotifyHistory) {
        this.payNotifyHistory = payNotifyHistory;
    }

    @OneToOne
    @JoinColumn(name = "sale_delivery_sid", nullable = false)
    public SaleDelivery getSaleDelivery() {
        return saleDelivery;
    }

    public void setSaleDelivery(SaleDelivery saleDelivery) {
        this.saleDelivery = saleDelivery;
    }

}
