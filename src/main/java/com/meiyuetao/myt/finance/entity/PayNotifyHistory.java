package com.meiyuetao.myt.finance.entity;

import java.math.BigDecimal;

import javax.persistence.*;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("付款记录")
@Entity
@Table(name = "iyb_pay_notify_history")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class PayNotifyHistory extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("上传支付宝交易号")
    private String outTradeNo;
    @MetaData("支付宝交易号")
    private String tradeNo;
    @MetaData("金额")
    private BigDecimal totalFee;
    @MetaData("主题")
    private String subject;
    @MetaData("内容")
    private String body;
    @MetaData("购买者邮件")
    private String buyerEmail;
    @MetaData("付款流水号")
    private String paySeq;
    @MetaData("交易状态")
    private ProcessStatusEnum processStatus;
    @MetaData("交易信息")
    private String processInfo;
    @MetaData("交易数据")
    private String rawData;
    @MetaData("支付类型")
    private PayTypeEnum payType;
    @MetaData(value = "付款方式", comments = "手机支付:CHINA_MOBILE 网银在线:CHINA_BANK 其他:OTHR INR_MONEY-帐户可用资金:ACCOUNT_INR_MONEY INR_BILL-内部财务记帐:INR_BILL 银行支票:CHK 支付宝:ALI")
    private String payMode;
    @MetaData("关联客户")
    private CustomerProfile customerProfile;

    public enum ProcessStatusEnum {

        @MetaData("正常")
        OK,

        @MetaData("异常")
        ERROR;
    }

    public enum PayTypeEnum {
        // 0
        @MetaData("忽略")
        IGNORE,

        //1
        @MetaData("全额付款")
        ALLPAY,

        //2
        @MetaData("支付保证金")
        CAUTIONMONEYPAY,

        //3
        @MetaData("支付每月盒子金额")
        ORDERDETAILPAY,

        //4
        @MetaData("支付剩余的盒子金额")
        ORDERDETAILSURPLUSPAY,

        //5
        @MetaData("支付订单增项的金额")
        ORDERORDETAILINDE,

        //6
        @MetaData("充值到现金帐户")
        RECHARGE,

        //7
        @MetaData("保证金和行项合并支付")
        CAUTIONANDDETAIL,

        //8
        @MetaData("订单合并后付款")
        ORDERGROUPSEQ;
    }

    @JsonProperty
    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    @JsonProperty
    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    @JsonProperty
    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    @JsonProperty
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @JsonProperty
    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    @JsonProperty
    public String getPaySeq() {
        return paySeq;
    }

    public void setPaySeq(String paySeq) {
        this.paySeq = paySeq;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public ProcessStatusEnum getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(ProcessStatusEnum processStatus) {
        this.processStatus = processStatus;
    }

    @JsonProperty
    public String getProcessInfo() {
        return processInfo;
    }

    public void setProcessInfo(String processInfo) {
        this.processInfo = processInfo;
    }

    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(length = 8)
    @JsonProperty
    public PayTypeEnum getPayType() {
        return payType;
    }

    public void setPayType(PayTypeEnum payType) {
        this.payType = payType;
    }

    @JsonProperty
    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    @OneToOne
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }
}
