package com.meiyuetao.myt.finance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.PersistableEntity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@MetaData("往来单位")
@Entity
@Table(name = "myt_biz_trade_unit")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class BizTradeUnit extends PersistableEntity<String> {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "单位名称")
    private String unitName;

    @MetaData(value = "单位类型")
    private BizTradeUnitTypeEnum unitType;

    @MetaData(value = "类型对应实体ID")
    private String typeId;

    private String id;

    public static enum BizTradeUnitTypeEnum {

        @MetaData(value = "客户")
        CUSTOMER,

        @MetaData(value = "供应商")
        SUPPLIER;

    }

    @Id
    @Column(name = "id")
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    @Transient
    public String getDisplay() {
        return unitType + " " + unitName;
    }

    @Column(length = 128, nullable = false)
    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    public BizTradeUnitTypeEnum getUnitType() {
        return unitType;
    }

    public void setUnitType(BizTradeUnitTypeEnum unitType) {
        this.unitType = unitType;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

}
