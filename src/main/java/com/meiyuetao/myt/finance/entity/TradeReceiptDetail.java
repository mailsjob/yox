package com.meiyuetao.myt.finance.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("收付款单行项")
@Entity
@Table(name = "myt_trade_receipt_detail")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class TradeReceiptDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联收付款单")
    private TradeReceipt tradeReceipt;
    @MetaData("关联会计科目")
    private AccountSubject accountSubject;
    @MetaData(value = "记账摘要")
    private String accountSummary;
    @MetaData("费用")
    private BigDecimal amount;
    @MetaData("付款记录关联销售单备注")
    private String memo;
    @MetaData("付款时间")
    private Date payTime;
    @MetaData(value = "关联付款凭证号列表", comments = "逗号分隔，一般用于记录其他三方系统付款的凭证信息")
    private String paymentVouchers;
    @MetaData(value = "付款参考信息", comments = "一般用于记录其他三方系统付款的凭证信息")
    private String paymentReference;

    @ManyToOne
    @JoinColumn(name = "trade_receipt_sid", nullable = false)
    @JsonProperty
    public TradeReceipt getTradeReceipt() {
        return tradeReceipt;
    }

    public void setTradeReceipt(TradeReceipt tradeReceipt) {
        this.tradeReceipt = tradeReceipt;
    }

    @ManyToOne
    @JoinColumn(name = "account_subject_sid", nullable = true)
    @JsonProperty
    public AccountSubject getAccountSubject() {
        return accountSubject;
    }

    public void setAccountSubject(AccountSubject accountSubject) {
        this.accountSubject = accountSubject;
    }

    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    @Lob
    @JsonProperty
    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    @Column(length = 2000)
    @JsonProperty
    public String getPaymentVouchers() {
        return paymentVouchers;
    }

    public void setPaymentVouchers(String paymentVouchers) {
        this.paymentVouchers = paymentVouchers;
    }

    @JsonProperty
    public String getAccountSummary() {
        return accountSummary;
    }

    public void setAccountSummary(String accountSummary) {
        this.accountSummary = accountSummary;
    }

}
