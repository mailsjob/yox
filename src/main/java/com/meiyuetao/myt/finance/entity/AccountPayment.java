package com.meiyuetao.myt.finance.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.PersistableEntity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Formula;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.finance.entity.BizTradeUnit.BizTradeUnitTypeEnum;

@MetaData("应付账款")
@Entity
@Table(name = "myt_biz_trade_unit")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class AccountPayment extends PersistableEntity<String> {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "单位名称")
    private String unitName;

    @MetaData(value = "单位类型")
    private BizTradeUnitTypeEnum unitType;

    @MetaData(value = "类型对应实体ID")
    private String typeId;

    @MetaData("应付账款")
    private BigDecimal accountsPayable;
    @MetaData("应收账款")
    private BigDecimal accountsReceivable;
    @MetaData("结算账款")
    private BigDecimal accountsSettlement;

    private String id;

    @Id
    @Column(name = "id")
    @JsonProperty
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public BizTradeUnitTypeEnum getUnitType() {
        return unitType;
    }

    public void setUnitType(BizTradeUnitTypeEnum unitType) {
        this.unitType = unitType;
    }

    @JsonProperty
    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    @Formula("(select ISNULL(sum(a.amount),0) from myt_finance_account_in_out a where a.biz_trade_unit_id= id and a.account_subject_code='2202')")
    @JsonProperty
    @NotAudited
    public BigDecimal getAccountsPayable() {
        return accountsPayable;
    }

    public void setAccountsPayable(BigDecimal accountsPayable) {
        this.accountsPayable = accountsPayable;
    }

    @Formula("(select ISNULL(sum(a.amount),0)  from myt_finance_account_in_out a where a.biz_trade_unit_id= id and a.account_subject_code in ('1122','1123'))")
    @JsonProperty
    @NotAudited
    public BigDecimal getAccountsReceivable() {
        return accountsReceivable;
    }

    public void setAccountsReceivable(BigDecimal accountsReceivable) {
        this.accountsReceivable = accountsReceivable;
    }

    @Formula("((select ISNULL(sum(a.amount),0) from myt_finance_account_in_out a where a.biz_trade_unit_id= id and a.account_subject_code='2202')-(select ISNULL(sum(a.amount),0)  from myt_finance_account_in_out a where a.biz_trade_unit_id= id and a.account_subject_code in ('1122','1123')))")
    @JsonProperty
    @NotAudited
    public BigDecimal getAccountsSettlement() {
        return accountsSettlement;
    }

    public void setAccountsSettlement(BigDecimal accountsSettlement) {
        this.accountsSettlement = accountsSettlement;
    }

    @Override
    @Transient
    public String getDisplay() {
        return unitName;
    }
}
