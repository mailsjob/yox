package com.meiyuetao.myt.finance.web.action;

import java.util.List;
import java.util.Map;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.annotation.SecurityControlIgnore;

import org.apache.commons.collections.CollectionUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.finance.entity.AccountSubject;
import com.meiyuetao.myt.finance.service.AccountSubjectService;

@MetaData("AccountSubjectController")
public class AccountSubjectController extends MytBaseController<AccountSubject, Long> {
    @Autowired
    private AccountSubjectService accountSubjectService;

    @Override
    protected BaseService<AccountSubject, Long> getEntityService() {
        return accountSubjectService;
    }

    @Override
    protected void checkEntityAclPermission(AccountSubject entity) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        if (groupPropertyFilter.isEmpty()) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "parent.id", true));
        }
        super.appendFilterProperty(groupPropertyFilter);
    }

    @Override
    @MetaData(value = "查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @MetaData(value = "付款会计科目树形数据")
    @SecurityControlIgnore
    public HttpHeaders findPaymentAccountSubjects() {
        List<Map<String, Object>> treeDatas = Lists.newArrayList();
        Iterable<AccountSubject> items = accountSubjectService.findPaymentAccountSubjects();
        for (AccountSubject item : items) {
            loopTreeData(treeDatas, item);
        }
        setModel(treeDatas);
        return buildDefaultHttpHeaders();
    }

    private void loopTreeData(List<Map<String, Object>> treeDatas, AccountSubject item) {
        Map<String, Object> row = Maps.newHashMap();
        treeDatas.add(row);
        row.put("id", item.getId());
        row.put("code", item.getCode());
        row.put("name", item.getName());
        row.put("display", item.getDisplay());
        row.put("open", false);
        List<AccountSubject> children = accountSubjectService.findChildren(item);
        if (!CollectionUtils.isEmpty(children)) {
            List<Map<String, Object>> childrenList = Lists.newArrayList();
            row.put("children", childrenList);
            for (AccountSubject child : children) {
                loopTreeData(childrenList, child);
            }
        }
    }

    @MetaData(value = "付款单出账对应会计科目")
    @SecurityControlIgnore
    public HttpHeaders findPaymentAccountSubjectsForPayable() {
        List<Map<String, Object>> treeDatas = Lists.newArrayList();
        Iterable<AccountSubject> items = accountSubjectService.findLeavesByCodes("1123", "2202");
        for (AccountSubject item : items) {
            loopTreeData(treeDatas, item);
        }
        setModel(treeDatas);
        return buildDefaultHttpHeaders();
    }

    @MetaData(value = "收款单入账对应会计科目")
    @SecurityControlIgnore
    public HttpHeaders findPaymentAccountSubjectsForRecorded() {
        List<Map<String, Object>> treeDatas = Lists.newArrayList();
        Iterable<AccountSubject> items = accountSubjectService.findLeavesByCodes("1122", "2205");
        for (AccountSubject item : items) {
            loopTreeData(treeDatas, item);
        }
        setModel(treeDatas);
        return buildDefaultHttpHeaders();
    }
}