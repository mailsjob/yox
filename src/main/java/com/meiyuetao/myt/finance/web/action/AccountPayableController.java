package com.meiyuetao.myt.finance.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.PersistableController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.finance.entity.AccountPayment;
import com.meiyuetao.myt.finance.service.AccountPaymentService;

@MetaData("应付账款")
public class AccountPayableController extends PersistableController<AccountPayment, String> {

    @Autowired
    private AccountPaymentService accountPaymentService;

    @Override
    protected BaseService<AccountPayment, String> getEntityService() {
        return accountPaymentService;
    }

    @Override
    protected void checkEntityAclPermission(AccountPayment entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}