package com.meiyuetao.myt.finance.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.finance.entity.TradeReceiptDetail;
import com.meiyuetao.myt.finance.service.TradeReceiptDetailService;

@MetaData("付款单明细")
public class PaymentReceiptDetailController extends MytBaseController<TradeReceiptDetail, Long> {
    @Autowired
    private TradeReceiptDetailService tradeReceiptDetailService;

    @Override
    protected BaseService<TradeReceiptDetail, Long> getEntityService() {
        return tradeReceiptDetailService;
    }

    @Override
    protected void checkEntityAclPermission(TradeReceiptDetail entity) {

    }

    @Override
    @MetaData(value = "查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}