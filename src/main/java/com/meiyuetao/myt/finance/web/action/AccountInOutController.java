package com.meiyuetao.myt.finance.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.annotation.SecurityControlIgnore;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.finance.entity.AccountInOut;
import com.meiyuetao.myt.finance.service.AccountInOutService;

@MetaData("AccountInOutController")
public class AccountInOutController extends MytBaseController<AccountInOut, Long> {

    @Autowired
    private AccountInOutService accountInOutService;

    @Override
    protected BaseService<AccountInOut, Long> getEntityService() {
        return accountInOutService;
    }

    @Override
    protected void checkEntityAclPermission(AccountInOut entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData(value = "会计科目总帐")
    @SecurityControlIgnore
    public HttpHeaders findByGroupAccountSubject() {
        setModel(findByGroupAggregate("accountSubject.id", "accountSubject.code", "accountSubject.name", "sum(directionAmount)"));
        return buildDefaultHttpHeaders();
    }
}