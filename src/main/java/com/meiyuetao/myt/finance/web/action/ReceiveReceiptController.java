package com.meiyuetao.myt.finance.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.finance.entity.TradeReceipt;
import com.meiyuetao.myt.finance.service.TradeReceiptService;

@MetaData("收款单")
public class ReceiveReceiptController extends MytBaseController<TradeReceipt, Long> {
    @Autowired
    private TradeReceiptService tradeReceiptService;

    @Override
    protected BaseService<TradeReceipt, Long> getEntityService() {
        return tradeReceiptService;
    }

    @Override
    protected void checkEntityAclPermission(TradeReceipt entity) {

    }

    @Override
    @MetaData(value = "查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @MetaData("行项数据")
    public HttpHeaders tradeReceiptDetails() {
        setModel(buildPageResultFromList(bindingEntity.getTradeReceiptDetails()));
        return buildDefaultHttpHeaders();
    }
}