package com.meiyuetao.myt.finance.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.finance.entity.PayNotifyHistory;
import com.meiyuetao.myt.finance.service.PayNotifyHistoryService;

@MetaData("PayNotifyHistoryController")
public class PayNotifyHistoryController extends MytBaseController<PayNotifyHistory, Long> {
    @Autowired
    private PayNotifyHistoryService payNotifyHistoryService;

    @Override
    protected BaseService<PayNotifyHistory, Long> getEntityService() {
        // TODO Auto-generated method stub
        return payNotifyHistoryService;
    }

    @Override
    protected void checkEntityAclPermission(PayNotifyHistory entity) {
        // TODO Auto-generated method stub

    }

    @Override
    @MetaData(value = "查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

}