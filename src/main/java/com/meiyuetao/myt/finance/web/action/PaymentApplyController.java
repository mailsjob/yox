package com.meiyuetao.myt.finance.web.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.audit.envers.ExtRevisionListener;
import lab.s2jh.core.security.AuthContextHolder;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.util.DateUtils;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.hibernate.envers.RevisionType;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Maps;
import com.meiyuetao.myt.core.constant.VoucherStateEnum;
import com.meiyuetao.myt.core.constant.VoucherTypeEnum;
import com.meiyuetao.myt.core.service.VoucherNumGenerateService;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.finance.entity.TradeReceipt;
import com.meiyuetao.myt.finance.entity.TradeReceiptDetail;
import com.meiyuetao.myt.finance.service.TradeReceiptService;

@MetaData("付款申请单")
public class PaymentApplyController extends MytBaseController<TradeReceipt, Long> {
    @Autowired
    private TradeReceiptService tradeReceiptService;
    @Autowired
    private VoucherNumGenerateService voucherNumGenerateService;

    @Override
    protected BaseService<TradeReceipt, Long> getEntityService() {
        return tradeReceiptService;
    }

    @Override
    protected void checkEntityAclPermission(TradeReceipt entity) {

    }

    @Override
    @MetaData(value = "查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    public void prepareBpmNew() {
        bindingEntity = new TradeReceipt();
        bindingEntity.setVoucher(voucherNumGenerateService.getVoucherNumByType(VoucherTypeEnum.FKD));
        bindingEntity.setVoucherDate(new Date());
        bindingEntity.setVoucherUser(getLogonUser());
        bindingEntity.setVoucherDepartment(getLogonUser().getDepartment());

    }

    @Override
    protected void setupDetachedBindingEntity(Long id) {
        bindingEntity = getEntityService().findDetachedOne(id, "tradeReceiptDetails");
    }

    @MetaData("创建初始化")
    public HttpHeaders bpmNew() {

        return buildDefaultHttpHeaders("bpmInput");
    }

    @MetaData("保存")
    public HttpHeaders bpmSave() {
        Map<String, Object> variables = Maps.newHashMap();
        String submitToAudit = this.getParameter("submitToAudit");
        if (BooleanUtils.toBoolean(submitToAudit)) {
            bindingEntity.setLastOperationSummary("提交:" + DateUtils.formatTime(new Date()));
            bindingEntity.setSubmitTime(new Date());
            bindingEntity.setVoucherState(VoucherStateEnum.POST);
        }
        List<TradeReceiptDetail> tradeReceiptDetails = bindingEntity.getTradeReceiptDetails();
        for (TradeReceiptDetail tradeReceiptDetail : tradeReceiptDetails) {
            tradeReceiptDetail.setTradeReceipt(bindingEntity);
        }
        if (bindingEntity.isNew()) {
            bindingEntity.setReceiptType(VoucherTypeEnum.FKD);
            checkEntityAclPermission(bindingEntity);
            ExtRevisionListener.setOperationEvent(RevisionType.ADD.name());
            tradeReceiptService.bpmCreate(bindingEntity, variables);
            setModel(OperationResult.buildSuccessResult("付款申请创建完成，并同步启动处理流程", bindingEntity));
        } else {
            tradeReceiptService.bpmUpdate(bindingEntity, this.getRequiredParameter("taskId"), variables);
            setModel(OperationResult.buildSuccessResult("付款申请任务提交完成", bindingEntity));
        }
        return buildDefaultHttpHeaders();
    }

    @MetaData("行项数据")
    public HttpHeaders tradeReceiptDetails() {
        setModel(buildPageResultFromList(bindingEntity.getTradeReceiptDetails()));
        return buildDefaultHttpHeaders();
    }

    @MetaData("一线审核")
    public HttpHeaders bpmLevel1Audit() {
        Map<String, Object> variables = Maps.newHashMap();
        variables.put("auditLevel1Time", new Date());
        variables.put("auditLevel1User", AuthContextHolder.getAuthUserPin());
        Boolean auditLevel1Pass = new Boolean(getRequiredParameter("auditLevel1Pass"));
        variables.put("auditLevel1Pass", auditLevel1Pass);
        variables.put("auditLevel1Explain", getParameter("auditLevel1Explain"));
        bindingEntity.setLastOperationSummary(bindingEntity.buildLastOperationSummary("审核"));
        if (!auditLevel1Pass) {
            bindingEntity.setSubmitTime(null);
        }
        bindingEntity.setAuditTime(new Date());
        tradeReceiptService.bpmUpdate(bindingEntity, this.getRequiredParameter("taskId"), variables);
        setModel(OperationResult.buildSuccessResult("付款申请一线审核完成", bindingEntity));
        return buildDefaultHttpHeaders();
    }

    @MetaData("付款")
    public HttpHeaders bpmPay() {
        List<TradeReceiptDetail> tradeReceiptDetails = bindingEntity.getTradeReceiptDetails();
        for (TradeReceiptDetail tradeReceiptDetail : tradeReceiptDetails) {
            tradeReceiptDetail.setTradeReceipt(bindingEntity);
        }
        bindingEntity.setPayTime(new Date());
        tradeReceiptService.bpmPay(bindingEntity, this.getRequiredParameter("taskId"));
        setModel(OperationResult.buildSuccessResult("数据保存成功", bindingEntity));
        return buildDefaultHttpHeaders();
    }

}