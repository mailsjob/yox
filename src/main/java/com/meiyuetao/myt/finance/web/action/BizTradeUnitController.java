package com.meiyuetao.myt.finance.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.PersistableController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.finance.entity.BizTradeUnit;
import com.meiyuetao.myt.finance.service.BizTradeUnitService;

@MetaData("往来单位管理")
public class BizTradeUnitController extends PersistableController<BizTradeUnit, String> {

    @Autowired
    private BizTradeUnitService bizTradeUnitService;

    @Override
    protected BaseService<BizTradeUnit, String> getEntityService() {
        return bizTradeUnitService;
    }

    @Override
    protected void checkEntityAclPermission(BizTradeUnit entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}