package com.meiyuetao.myt.finance.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.finance.dao.TradeReceiptDetailDao;
import com.meiyuetao.myt.finance.entity.TradeReceiptDetail;

@Service
@Transactional
public class TradeReceiptDetailService extends BaseService<TradeReceiptDetail, Long> {

    @Autowired
    private TradeReceiptDetailDao tradeReceiptDetailDao;

    @Override
    protected BaseDao<TradeReceiptDetail, Long> getEntityDao() {
        return tradeReceiptDetailDao;
    }

}
