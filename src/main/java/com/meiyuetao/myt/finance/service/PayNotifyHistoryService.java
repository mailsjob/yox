package com.meiyuetao.myt.finance.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.finance.dao.PayNotifyHistoryDao;
import com.meiyuetao.myt.finance.entity.PayNotifyHistory;

@Service
@Transactional
public class PayNotifyHistoryService extends BaseService<PayNotifyHistory, Long> {

    @Autowired
    private PayNotifyHistoryDao payNotifyHistoryDao;

    @Override
    protected BaseDao<PayNotifyHistory, Long> getEntityDao() {
        return payNotifyHistoryDao;
    }

}
