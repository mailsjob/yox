package com.meiyuetao.myt.finance.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.finance.dao.BizTradeUnitDao;
import com.meiyuetao.myt.finance.entity.BizTradeUnit;

@Service
@Transactional
public class BizTradeUnitService extends BaseService<BizTradeUnit, String> {

    @Autowired
    private BizTradeUnitDao bizTradeUnitDao;

    @Override
    protected BaseDao<BizTradeUnit, String> getEntityDao() {
        return bizTradeUnitDao;
    }
}
