package com.meiyuetao.myt.finance.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.finance.dao.PayNotifyR2SaleDeliveryDao;
import com.meiyuetao.myt.finance.entity.PayNotifyR2SaleDelivery;

@Service
@Transactional
public class PayNotifyR2SaleDeliveryService extends BaseService<PayNotifyR2SaleDelivery, Long> {

    @Autowired
    private PayNotifyR2SaleDeliveryDao payNotifyR2SaleDeliveryDao;

    @Override
    protected BaseDao<PayNotifyR2SaleDelivery, Long> getEntityDao() {
        return payNotifyR2SaleDeliveryDao;
    }

}
