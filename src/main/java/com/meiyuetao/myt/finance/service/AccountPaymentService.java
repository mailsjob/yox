package com.meiyuetao.myt.finance.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.finance.dao.AccountPaymentDao;
import com.meiyuetao.myt.finance.entity.AccountPayment;

@Service
@Transactional
public class AccountPaymentService extends BaseService<AccountPayment, String> {

    @Autowired
    private AccountPaymentDao accountPaymentDao;

    @Override
    protected BaseDao<AccountPayment, String> getEntityDao() {
        return accountPaymentDao;
    }
}
