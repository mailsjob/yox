package com.meiyuetao.myt.ext.web.action;

import java.io.File;
import java.io.IOException;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.SimpleController;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Value;

@MetaData("SsoExtController")
public class SsoExtController extends SimpleController {
    @Value("${sso.login.pic.path}")
    private String path;
    @Value("${sso.login.pic.fileName}")
    private String fileName;
    private File file;

    public HttpHeaders updateLoginPic() {
        File diskFile = new File(path + File.separator + fileName);
        try {
            FileUtils.copyFile(file, diskFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        setModel(OperationResult.buildSuccessResult("更新成功"));
        return buildDefaultHttpHeaders();
    }

    public HttpHeaders edit() {

        return buildDefaultHttpHeaders("inputBasic");

    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

}