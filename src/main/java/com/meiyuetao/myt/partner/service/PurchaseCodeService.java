package com.meiyuetao.myt.partner.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.partner.dao.PurchaseCodeDao;
import com.meiyuetao.myt.partner.entity.PurchaseCode;

@Service
@Transactional
public class PurchaseCodeService extends BaseService<PurchaseCode, Long> {

    @Autowired
    private PurchaseCodeDao purchaseCodeDao;

    @Override
    protected BaseDao<PurchaseCode, Long> getEntityDao() {
        return purchaseCodeDao;
    }
}
