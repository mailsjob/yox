package com.meiyuetao.myt.partner.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;

@Entity
@Table(name = "iyb_partner_r2_Commodity", uniqueConstraints = @UniqueConstraint(columnNames = { "partner_sid", "commodity_sid" }))
@MetaData(value = "代理商商品关联")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PartnerR2Commodity extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("代理商（合作伙伴）")
    private Partner partner;
    @MetaData("关联商品")
    private Commodity commodity;

    @Transient
    public String getDisplay() {
        return partner.getDisplay() + " " + commodity.getDisplay();
    }

    @ManyToOne
    @JoinColumn(name = "partner_sid", nullable = false)
    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

}