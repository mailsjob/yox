package com.meiyuetao.myt.partner.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.stock.entity.StorageLocation;

@Entity
@Table(name = "iyb_partner_r2_storage_location", uniqueConstraints = @UniqueConstraint(columnNames = { "Partner_ID", "Storage_Location_ID" }))
@MetaData(value = "合作伙伴库存地关联")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PartnerR2StorageLocation extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("合作伙伴")
    private Partner partner;
    @MetaData("库存地")
    private StorageLocation storageLocation;

    @Transient
    public String getDisplay() {
        return partner.getDisplay() + " " + storageLocation.getDisplay();
    }

    @ManyToOne
    @JoinColumn(name = "Partner_ID", nullable = false)
    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    @ManyToOne
    @JoinColumn(name = "Storage_Location_ID", nullable = false)
    public StorageLocation getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(StorageLocation storageLocation) {
        this.storageLocation = storageLocation;
    }
}