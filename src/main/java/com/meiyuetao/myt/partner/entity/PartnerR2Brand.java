package com.meiyuetao.myt.partner.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Brand;

@Entity
@Table(name = "iyb_partner_r2_brand", uniqueConstraints = @UniqueConstraint(columnNames = { "partner_sid", "brand_sid" }))
@MetaData(value = "代理商品牌关联")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PartnerR2Brand extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("代理商（合作伙伴）")
    private Partner partner;
    @MetaData("品牌")
    private Brand brand;

    @Transient
    public String getDisplay() {
        return partner.getDisplay() + " " + brand.getDisplay();
    }

    @ManyToOne
    @JoinColumn(name = "partner_sid", nullable = false)
    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "brand_sid", nullable = false)
    @JsonProperty
    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

}