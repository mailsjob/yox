package com.meiyuetao.myt.partner.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;

@Entity
@Table(name = "iyb_purchase_code")
@MetaData(value = "消费码")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class PurchaseCode extends MytBaseEntity {
    private static final long serialVersionUID = 1L;

    @MetaData(value = "消费码")
    private String code;

    @MetaData(value = "生产时间")
    private Date genDatetime;

    @MetaData(value = "使用时间")
    private Date usedDatetime;

    @MetaData(value = "过期时间")
    private Date expiredDatetime;

    @MetaData(value = "第三方合作伙伴")
    private Partner partner;

    @MetaData(value = "状态")
    private PurchaseCodeStatusEnum codeStatus = PurchaseCodeStatusEnum.ENABLE;

    @MetaData(value = "关联订单[冗余属性]")
    private BoxOrder boxOrder;

    @MetaData(value = "关联订单行项商品")
    private BoxOrderDetailCommodity boxOrderDetailCommodity;

    @MetaData(value = " 关联商品[冗余属性]")
    private Commodity commodity;

    @Column(length = 32, unique = true, nullable = false)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getGenDatetime() {
        return genDatetime;
    }

    public void setGenDatetime(Date genDatetime) {
        this.genDatetime = genDatetime;
    }

    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getUsedDatetime() {
        return usedDatetime;
    }

    public void setUsedDatetime(Date usedDatetime) {
        this.usedDatetime = usedDatetime;
    }

    @JsonSerialize(using = DateJsonSerializer.class)
    @JsonProperty
    public Date getExpiredDatetime() {
        return expiredDatetime;
    }

    public void setExpiredDatetime(Date expiredDatetime) {
        this.expiredDatetime = expiredDatetime;
    }

    public enum PurchaseCodeStatusEnum {
        @MetaData("可用")
        ENABLE, @MetaData("已使用")
        USED, @MetaData("已过期")
        OVERDUE, @MetaData("作废")
        DISABLE;

    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8, nullable = false)
    @JsonProperty
    public PurchaseCodeStatusEnum getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(PurchaseCodeStatusEnum codeStatus) {
        this.codeStatus = codeStatus;
    }

    @ManyToOne
    @JoinColumn(name = "Partner_SID", nullable = false)
    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    @ManyToOne
    @JoinColumn(name = "ORDER_SID", nullable = false)
    @JsonProperty
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

    @OneToOne
    @JoinColumn(name = "order_detail_commodity_sid", nullable = false)
    @JsonProperty
    public BoxOrderDetailCommodity getBoxOrderDetailCommodity() {
        return boxOrderDetailCommodity;
    }

    public void setBoxOrderDetailCommodity(BoxOrderDetailCommodity boxOrderDetailCommodity) {
        this.boxOrderDetailCommodity = boxOrderDetailCommodity;
    }

    @ManyToOne
    @JoinColumn(name = "COMMODITY_SID", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

}