package com.meiyuetao.myt.partner.dao;

import java.util.List;
import java.util.Set;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Brand;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.partner.entity.Partner;

@Repository
public interface PartnerDao extends BaseDao<Partner, Long> {

    Partner findByCode(String code);

    @Query("from Partner")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    public List<Partner> findAllCached();

    /**
     * 获取代理商关联的商品
     * 
     * @param partners
     * @return
     */
    /*
     * SELECT c.* FROM iyb_partner_r2_commodity p2c INNER JOIN iyb_partner p ON
     * p.sid = p2c.partner_sid INNER JOIN iyb_commodity c ON c.sid =
     * p2c.commodity_sid WHERE p.sid IN (24);
     */
    @Query("SELECT c FROM Commodity c, Partner p, PartnerR2Commodity p2c " + "WHERE p2c.commodity=c AND p2c.partner=p AND p IN(:partners)")
    List<Commodity> findRelationedCommodities(@Param("partners") Set<Partner> partners);

    /**
     * 获取代理商关联的品牌
     * 
     * @param partners
     * @return
     */
    @Query("SELECT b FROM Brand b, Partner p, PartnerR2Brand p2b " + "WHERE p2b.brand=b AND p2b.partner=p AND p IN(:partners)")
    List<Brand> findRelationedBrands(@Param("partners") Set<Partner> partners);

}