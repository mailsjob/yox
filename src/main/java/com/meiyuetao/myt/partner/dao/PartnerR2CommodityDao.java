package com.meiyuetao.myt.partner.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.partner.entity.PartnerR2Commodity;

@Repository
public interface PartnerR2CommodityDao extends BaseDao<PartnerR2Commodity, Long> {
    List<PartnerR2Commodity> findByCommodity(Commodity commodity);
}