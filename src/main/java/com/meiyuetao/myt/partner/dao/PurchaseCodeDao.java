package com.meiyuetao.myt.partner.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.partner.entity.PurchaseCode;

@Repository
public interface PurchaseCodeDao extends BaseDao<PurchaseCode, Long> {

    PurchaseCode findByCode(String code);

}