package com.meiyuetao.myt.partner.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.partner.entity.DistributeDeliveryDetail;

@Repository
public interface DistributeDeliveryDetailDao extends BaseDao<DistributeDeliveryDetail, Long> {

}