package com.meiyuetao.myt.partner.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Brand;
import com.meiyuetao.myt.partner.entity.PartnerR2Brand;

@Repository
public interface PartnerR2BrandDao extends BaseDao<PartnerR2Brand, Long> {
    List<PartnerR2Brand> findByBrand(Brand brand);
}