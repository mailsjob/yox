package com.meiyuetao.myt.sys.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.sys.entity.DynaPropLineVal;
import com.meiyuetao.myt.sys.service.DynaPropLineService;
import com.meiyuetao.myt.sys.service.DynaPropLineValService;

@MetaData(value = "DynaPropLineValController")
public class DynaPropLineValController extends BaseController<DynaPropLineVal, Long> {

    @Autowired
    private DynaPropLineValService dynaPropLineValService;

    @Autowired
    private DynaPropLineService dynaPropLineService;

    @Override
    protected BaseService<DynaPropLineVal, Long> getEntityService() {
        return dynaPropLineValService;
    }

    @Override
    protected void checkEntityAclPermission(DynaPropLineVal entity) {
        // Do nothing check
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        if (groupPropertyFilter.isEmpty()) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "parent.id", true));
        }
        super.appendFilterProperty(groupPropertyFilter);
    }

    @Override
    @MetaData(value = "查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    @MetaData(value = "保存")
    public HttpHeaders doSave() {

        return super.doSave();
    }

    @Override
    @MetaData(value = "删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

}
