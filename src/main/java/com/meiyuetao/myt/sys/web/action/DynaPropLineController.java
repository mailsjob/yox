package com.meiyuetao.myt.sys.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.service.Validation;
import lab.s2jh.web.action.BaseController;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.sys.entity.DynaPropLine;
import com.meiyuetao.myt.sys.service.DynaPropLineService;

@MetaData(value = "DynaPropLineController")
public class DynaPropLineController extends BaseController<DynaPropLine, Long> {

    @Autowired
    private DynaPropLineService dynaPropLineService;

    @Override
    protected BaseService<DynaPropLine, Long> getEntityService() {
        return dynaPropLineService;
    }

    @Override
    protected void checkEntityAclPermission(DynaPropLine entity) {
        // Do nothing check
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        if (groupPropertyFilter.isEmpty()) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "parent.id", true));
        }
        super.appendFilterProperty(groupPropertyFilter);
    }

    @Override
    @MetaData(value = "查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    @MetaData(value = "保存")
    public HttpHeaders doSave() {
        if (StringUtils.isBlank(bindingEntity.getName())) {
            bindingEntity.setName(bindingEntity.getCode());
        }
        if (bindingEntity.getType() == null) {
            Validation.isTrue(false, "请选择类型");
        }
        return super.doSave();
    }

}
