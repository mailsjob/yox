package com.meiyuetao.myt.sys.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.sys.dao.AnnouncementDao;
import com.meiyuetao.myt.sys.entity.Announcement;

@Service
@Transactional
public class AnnouncementService extends BaseService<Announcement, Long> {

    @Autowired
    private AnnouncementDao announcementDao;

    @Override
    protected BaseDao<Announcement, Long> getEntityDao() {
        return announcementDao;
    }
}
