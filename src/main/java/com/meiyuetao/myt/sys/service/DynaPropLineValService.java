package com.meiyuetao.myt.sys.service;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.sys.dao.DynaPropLineValDao;
import com.meiyuetao.myt.sys.entity.DynaPropLineVal;

@Service
@Transactional
public class DynaPropLineValService extends BaseService<DynaPropLineVal, Long> {

    @Autowired
    private DynaPropLineValDao dynaPropLineValDao;

    @Override
    protected BaseDao<DynaPropLineVal, Long> getEntityDao() {
        return dynaPropLineValDao;
    }

    public List<DynaPropLineVal> findByObjNameAndObjSid(String objName, Long objSid) {
        return dynaPropLineValDao.findByObjNameAndObjSid(objName, objSid);
    }
}
