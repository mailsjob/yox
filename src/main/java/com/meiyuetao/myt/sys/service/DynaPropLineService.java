package com.meiyuetao.myt.sys.service;

import java.util.Collections;
import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.meiyuetao.myt.sys.dao.DynaPropLineDao;
import com.meiyuetao.myt.sys.entity.DynaPropLine;

@Service
@Transactional
public class DynaPropLineService extends BaseService<DynaPropLine, Long> {

    @Autowired
    private DynaPropLineDao dynaPropLineDao;
    @Autowired
    private DynaPropLineValService dynaPropLineValService;

    @Override
    protected BaseDao<DynaPropLine, Long> getEntityDao() {
        return dynaPropLineDao;
    }

    @Transactional(readOnly = true)
    public List<DynaPropLine> findRoots() {
        List<DynaPropLine> roots = Lists.newArrayList();
        Iterable<DynaPropLine> allDynaPropLines = dynaPropLineDao.findAllCached();
        for (DynaPropLine dynaPropLine : allDynaPropLines) {
            if (dynaPropLine.getParent() == null) {
                roots.add(dynaPropLine);
            }
        }
        Collections.sort(roots);

        return roots;
    }

    @Transactional(readOnly = true)
    public List<DynaPropLine> findChildren(DynaPropLine parent) {
        List<DynaPropLine> items = Lists.newArrayList();
        Iterable<DynaPropLine> allDynaPropLines = dynaPropLineDao.findAllCached();
        for (DynaPropLine dynaPropLine : allDynaPropLines) {
            if (dynaPropLine.getParent() == parent) {
                items.add(dynaPropLine);
            }
        }
        Collections.sort(items);
        return items;
    }

}
