package com.meiyuetao.myt.sys.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "T_SYS_DYNA_PROP_EXT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DynaPropExt extends DynaPropDefinition {
    private static final long serialVersionUID = 1L;
}
