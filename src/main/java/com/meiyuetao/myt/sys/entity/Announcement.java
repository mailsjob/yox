package com.meiyuetao.myt.sys.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.auth.entity.User;
import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@Entity
@Table(name = "t_sys_mass_notice")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Announcement extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("内容")
    private String contents;
    @MetaData("到期时间")
    private Date expireDate;
    @MetaData("重要级别")
    private String priorityLevel;
    @MetaData("发布时间")
    private Date publishTime;
    @MetaData("状态")
    private String state;
    @MetaData("标题")
    private String title;
    @MetaData("群代码")
    private String toGroupCode;
    @MetaData("发布用户")
    private User publishUser;
    @MetaData("发布序列代码")
    private DisplayScopeTypeEnum displayScopeType;
    @MetaData("发布位置代码")
    private String displayPositionCode;
    @MetaData("有效时间")
    private Date effectiveDate;

    public enum DisplayScopeTypeEnum {
        @MetaData("后台")
        B,

        @MetaData("前台")
        F,

        @MetaData("前后台")
        BF;
    }

    @Column(name = "contents")
    @JsonProperty
    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    @Column(name = "expire_date", length = 7)
    @JsonProperty
    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    @Column(name = "priority_level", length = 32)
    @JsonProperty
    public String getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(String priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    @Column(name = "publish_time", length = 7)
    @JsonProperty
    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    @Column(name = "state", length = 32)
    @JsonProperty
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name = "title", length = 500)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "to_group_code", length = 16)
    @JsonProperty
    public String getToGroupCode() {
        return toGroupCode;
    }

    public void setToGroupCode(String toGroupCode) {
        this.toGroupCode = toGroupCode;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "display_scope_type", length = 32)
    @JsonProperty
    public DisplayScopeTypeEnum getDisplayScopeType() {
        return displayScopeType;
    }

    public void setDisplayScopeType(DisplayScopeTypeEnum displayScopeType) {
        this.displayScopeType = displayScopeType;
    }

    @Column(name = "display_position_code", length = 128)
    @JsonProperty
    public String getDisplayPositionCode() {
        return displayPositionCode;
    }

    public void setDisplayPositionCode(String displayPositionCode) {
        this.displayPositionCode = displayPositionCode;
    }

    @Column(name = "effective_date", length = 7)
    @JsonProperty
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @OneToOne
    @JoinColumn(name = "publish_user_sid")
    @JsonProperty
    public User getPublishUser() {
        return publishUser;
    }

    public void setPublishUser(User publishUser) {
        this.publishUser = publishUser;
    }

}
