package com.meiyuetao.myt.sys.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import lab.s2jh.core.audit.SaveUpdateAuditListener;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "T_SYS_DYNA_PROP_LINE")
@Cache(usage = CacheConcurrencyStrategy.NONE)
@EntityListeners({ SaveUpdateAuditListener.class })
public class DynaPropLine extends DynaPropDefinition implements Comparable<DynaPropLine> {
    private static final long serialVersionUID = 1L;
    private DynaPropLine parent;
    /** 下属子项集合 */

    private List<DynaPropLine> children = new ArrayList<DynaPropLine>();
    private Integer inheritLevel = 0;

    /** 下属子项数目（冗余字段，便于查询） */
    private Integer childrenSize = 0;

    private Boolean disabled = Boolean.FALSE;

    private DynaPropExt dynaPropExt1;

    private DynaPropExt dynaPropExt2;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "PARENT_SID", nullable = true)
    @JsonIgnore
    public DynaPropLine getParent() {
        return parent;
    }

    public void setParent(DynaPropLine parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("orderRank desc")
    @JsonIgnore
    public List<DynaPropLine> getChildren() {
        return children;
    }

    public void setChildren(List<DynaPropLine> children) {
        this.children = children;
    }

    @JsonProperty
    public Integer getInheritLevel() {
        return inheritLevel;
    }

    public void setInheritLevel(Integer inheritLevel) {
        this.inheritLevel = inheritLevel;
    }

    @JsonProperty
    public Integer getChildrenSize() {
        return childrenSize;
    }

    public void setChildrenSize(Integer childrenSize) {
        this.childrenSize = childrenSize;
    }

    @JsonProperty
    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    @OneToOne(cascade = CascadeType.DETACH, orphanRemoval = true)
    @JoinColumn(name = "PROP_EXT1_SID", nullable = true)
    @JsonProperty
    public DynaPropExt getDynaPropExt1() {
        return dynaPropExt1;
    }

    public void setDynaPropExt1(DynaPropExt dynaPropExt1) {
        this.dynaPropExt1 = dynaPropExt1;
    }

    @OneToOne(cascade = CascadeType.DETACH, orphanRemoval = true)
    @JoinColumn(name = "PROP_EXT2_SID", nullable = true)
    @JsonProperty
    public DynaPropExt getDynaPropExt2() {
        return dynaPropExt2;
    }

    public void setDynaPropExt2(DynaPropExt dynaPropExt2) {
        this.dynaPropExt2 = dynaPropExt2;
    }

    @Override
    public int compareTo(DynaPropLine o) {
        return CompareToBuilder.reflectionCompare(o.getOrderRank(), this.getOrderRank());
    }

}