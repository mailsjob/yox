package com.meiyuetao.myt.sys.entity;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import lab.s2jh.core.entity.BaseEntity;
import lab.s2jh.core.entity.def.DynamicParameterTypeEnum;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

@MappedSuperclass
public class DynaPropDefinition extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
    private Long id;
    /** 参数代码 */
    private String code;
    /** 参数显示名称 */
    private String name = "描述";
    /** 参数用法描述 */
    private String description;
    /** 相对排序号，数值越大越靠前 */
    private Integer orderRank = 100;
    /** 参数类型 */
    private DynamicParameterTypeEnum type = DynamicParameterTypeEnum.STRING;
    /** 前端UI校验规则，如：{required:true,min:0,max:1000} */
    private String validateRules;
    /** 是否必填参数 */
    private Boolean requiredFlag = Boolean.FALSE;
    /** 隐藏类型参数，一般需要设定defaultValue以便固定带入JasperReport参数 */
    private Boolean hiddenFlag = Boolean.FALSE;
    /** 缺省参数值 */
    private String defaultValue;
    /** 对于下拉框类型参数，是否允许多选 */
    private Boolean multiSelectFlag = Boolean.FALSE;
    /** 对于List类型数据的数据源指定，即定义如何提供给用户选取的数据 */
    private String listDataSource;

    @Id
    @Column(name = "sid")
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "native")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Transient
    public String getFullValidateRules() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        if (this.getRequiredFlag()) {
            sb.append("required:true,");
        }
        switch (this.getType()) {
        case DATE:
            sb.append("date:true,");
            break;
        case TIMESTAMP:
            sb.append("timestamp:true,");
            break;
        case FLOAT:
            sb.append("number:true,");
            break;
        case INTEGER:
            sb.append("number:true,digits:true,");
            break;
        }
        if (StringUtils.isNotBlank(this.getValidateRules())) {
            sb.append(this.getValidateRules() + ",");
        }
        return sb.substring(0, sb.length() - 1) + "}";
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = true)
    @JsonProperty
    public DynamicParameterTypeEnum getType() {
        return type;
    }

    public void setType(DynamicParameterTypeEnum type) {
        this.type = type;
    }

    @Column(nullable = true)
    @JsonProperty
    public Boolean getHiddenFlag() {
        return hiddenFlag;
    }

    public void setHiddenFlag(Boolean hiddenFlag) {
        this.hiddenFlag = hiddenFlag;
    }

    @Column(length = 32, nullable = true)
    @JsonProperty
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Column(nullable = true)
    @JsonProperty
    public Boolean getMultiSelectFlag() {
        return multiSelectFlag;
    }

    public void setMultiSelectFlag(Boolean multiSelectFlag) {
        this.multiSelectFlag = multiSelectFlag;
    }

    @Column(length = 256, nullable = true)
    public String getListDataSource() {
        return listDataSource;
    }

    public void setListDataSource(String listDataSource) {
        this.listDataSource = listDataSource;
    }

    @Column(length = 128, nullable = true)
    public String getValidateRules() {
        return validateRules;
    }

    public void setValidateRules(String validateRules) {
        this.validateRules = validateRules;
    }

    @Column(nullable = true)
    @JsonProperty
    public Boolean getRequiredFlag() {
        return requiredFlag;
    }

    public void setRequiredFlag(Boolean requiredFlag) {
        this.requiredFlag = requiredFlag;
    }

    @JsonProperty
    public Integer getOrderRank() {
        return orderRank;
    }

    public void setOrderRank(Integer orderRank) {
        this.orderRank = orderRank;
    }

    @Column(length = 32, nullable = false)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(length = 2000, nullable = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    @Transient
    public String getDisplay() {
        return code + " " + name;
    }
}
