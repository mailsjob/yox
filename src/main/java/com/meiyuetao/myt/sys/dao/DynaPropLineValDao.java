package com.meiyuetao.myt.sys.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.sys.entity.DynaPropLineVal;

@Repository
public interface DynaPropLineValDao extends BaseDao<DynaPropLineVal, Long> {

    List<DynaPropLineVal> findByObjNameAndObjSid(String objName, Long objSid);
}
