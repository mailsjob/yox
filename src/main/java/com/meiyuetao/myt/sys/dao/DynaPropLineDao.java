package com.meiyuetao.myt.sys.dao;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.sys.entity.DynaPropLine;

@Repository
public interface DynaPropLineDao extends BaseDao<DynaPropLine, Long> {
    @Query("from DynaPropLine")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    public Iterable<DynaPropLine> findAllCached();
}
