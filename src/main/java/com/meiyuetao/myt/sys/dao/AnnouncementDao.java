package com.meiyuetao.myt.sys.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.sys.entity.Announcement;

@Repository
public interface AnnouncementDao extends BaseDao<Announcement, Long> {

}