package com.meiyuetao.myt.stock.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.stock.entity.CommodityStockMove;

@Repository
public interface CommodityStockMoveDao extends BaseDao<CommodityStockMove, Long> {

}