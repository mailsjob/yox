package com.meiyuetao.myt.stock.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.stock.entity.CommodityStockMoveDetail;

@Repository
public interface CommodityStockMoveDetailDao extends BaseDao<CommodityStockMoveDetail, Long> {

}