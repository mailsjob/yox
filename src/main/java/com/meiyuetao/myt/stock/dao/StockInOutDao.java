package com.meiyuetao.myt.stock.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.core.constant.VoucherTypeEnum;
import com.meiyuetao.myt.stock.entity.StockInOut;

@Repository
public interface StockInOutDao extends BaseDao<StockInOut, Long> {
    List<StockInOut> findByVoucherAndVoucherType(String voucher, VoucherTypeEnum voucherType);
}