package com.meiyuetao.myt.stock.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.stock.entity.StorageLocation;

@Repository
public interface StorageLocationDao extends BaseDao<StorageLocation, Long> {

    @Query("from StorageLocation order by code ASC")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    public List<StorageLocation> findAllCached();

}