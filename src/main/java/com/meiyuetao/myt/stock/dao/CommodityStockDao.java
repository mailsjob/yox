package com.meiyuetao.myt.stock.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.stock.entity.CommodityStock;
import com.meiyuetao.myt.stock.entity.StorageLocation;

@Repository
public interface CommodityStockDao extends BaseDao<CommodityStock, Long> {

    @Query("from CommodityStock where commodity=:commodity and storageLocation=:storageLocation and (batchNo is null or batchNo='')")
    CommodityStock findByCommodityAndStorageLocation(@Param("commodity") Commodity commodity, @Param("storageLocation") StorageLocation storageLocation);

    CommodityStock findByCommodityAndStorageLocationAndBatchNo(Commodity commodity, StorageLocation storageLocation, String batchNo);

    List<CommodityStock> findByCommodity(Commodity commodity);

    @Query("select distinct cs.commodity  from CommodityStock cs  join fetch cs.commodity.commodityVaryPrice")
    List<Commodity> findSalableCommodities();

}