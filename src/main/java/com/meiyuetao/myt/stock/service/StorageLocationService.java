package com.meiyuetao.myt.stock.service;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.stock.dao.StorageLocationDao;
import com.meiyuetao.myt.stock.entity.StorageLocation;

@Service
@Transactional
public class StorageLocationService extends BaseService<StorageLocation, Long> {
    @Autowired
    private StorageLocationDao storageLocationDao;

    @Override
    protected BaseDao<StorageLocation, Long> getEntityDao() {
        return storageLocationDao;
    }

    public List<StorageLocation> findAllCached() {
        return storageLocationDao.findAllCached();
    }

    /**
     * 先按照简化的hardcode方式处理，根据需要再调整为可配置方式
     * 
     * @return
     */
    public StorageLocation getDefaultStorageLocation() {
        List<StorageLocation> storageLocations = findAllCached();
        for (StorageLocation storageLocation : storageLocations) {
            if (storageLocation.getCode().equals("L140301")) {
                return storageLocation;
            }
        }
        return null;
    }
}
