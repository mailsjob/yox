package com.meiyuetao.myt.stock.service;

import java.math.BigDecimal;
import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.core.constant.VoucherTypeEnum;
import com.meiyuetao.myt.md.dao.CommodityDao;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.stock.dao.CommodityStockDao;
import com.meiyuetao.myt.stock.dao.StockInOutDao;
import com.meiyuetao.myt.stock.entity.CommodityStock;
import com.meiyuetao.myt.stock.entity.StockInOut;

@Service
@Transactional
public class StockInOutService extends BaseService<StockInOut, Long> {

    @Autowired
    private StockInOutDao stockInOutDao;
    @Autowired
    private CommodityStockDao commodityStockDao;
    @Autowired
    private CommodityDao commodityDao;

    @Override
    protected BaseDao<StockInOut, Long> getEntityDao() {
        return stockInOutDao;
    }

    public List<StockInOut> findByVoucherAndVoucherType(String voucher, VoucherTypeEnum voucherType) {

        return stockInOutDao.findByVoucherAndVoucherType(voucher, voucherType);
    }

    public void saveCascade(StockInOut entity) {
        // 更新商品库存量
        CommodityStock commodityStock = entity.getCommodityStock();
        /*
         * entity.setOriginalPurchasingQuantity(commodityStock.
         * getPurchasingTotalQuantity());
         * entity.setOriginalSalingQuantity(commodityStock
         * .getSalingTotalQuantity());
         * entity.setOriginalQuantity(commodityStock.getCurStockQuantity());
         * entity.setOriginalCostPrice(commodityStock.getCostPrice());
         * entity.setOriginalStockAmount(commodityStock.getCurStockAmount());
         */
        if (entity.getDiffSalingQuantity().compareTo(BigDecimal.ZERO) != 0) {
            if (commodityStock.getSalingTotalQuantity() == null) {
                commodityStock.setSalingTotalQuantity(entity.getDiffSalingQuantity());
            } else {
                commodityStock.setSalingTotalQuantity(commodityStock.getSalingTotalQuantity().add(entity.getDiffSalingQuantity()));
            }
        }
        // 变更后锁定量
        entity.setSalingQuantity(commodityStock.getSalingTotalQuantity());

        if (entity.getDiffPurchasingQuantity().compareTo(BigDecimal.ZERO) != 0) {
            if (commodityStock.getPurchasingTotalQuantity() == null) {
                commodityStock.setPurchasingTotalQuantity(entity.getDiffPurchasingQuantity());
            } else {
                commodityStock.setPurchasingTotalQuantity(commodityStock.getPurchasingTotalQuantity().add(entity.getDiffPurchasingQuantity()));
            }
        }
        // 变更后采购在途
        entity.setPurchasingQuantity(commodityStock.getPurchasingTotalQuantity());

        // 实物库存数量 （当前库存数量 ）
        if (entity.getDiffQuantity().compareTo(BigDecimal.ZERO) != 0) {
            if (commodityStock.getCurStockQuantity() == null) {
                // 实物库存数量
                commodityStock.setCurStockQuantity(entity.getDiffQuantity());
            } else {
                // 实物库存数量 = 实物库存数量 + 数量变化
                commodityStock.setCurStockQuantity(commodityStock.getCurStockQuantity().add(entity.getDiffQuantity()));
            }
        }
        // 变更后数量
        entity.setQuantity(commodityStock.getCurStockQuantity());
        if (entity.getDiffStockAmount().compareTo(BigDecimal.ZERO) != 0) {
            if (commodityStock.getCurStockAmount() == null) {
                commodityStock.setCurStockAmount(entity.getDiffStockAmount());
            } else {
                commodityStock.setCurStockAmount(commodityStock.getCurStockAmount().add(entity.getDiffStockAmount()));
            }
            /*
             * commodityStock.setCostPrice(commodityStock.getCurStockAmount().divide
             * (commodityStock.getCurStockQuantity(), 2,
             * BigDecimal.ROUND_HALF_EVEN));
             */
        } /*
           * else {
           * commodityStock.setCurStockAmount(commodityStock.getCurStockQuantity
           * ().multiply( commodityStock.getCostPrice())); }
           */
        if (commodityStock.getCurStockQuantity().compareTo(BigDecimal.ZERO) != 0) {
            commodityStock.setCostPrice(commodityStock.getCurStockAmount().divide(commodityStock.getCurStockQuantity(), 2, BigDecimal.ROUND_HALF_EVEN));
        }
        // 变更后成本价
        entity.setCostPrice(commodityStock.getCostPrice());
        // 变更后库存金额
        entity.setStockAmount(commodityStock.getCurStockAmount());
        commodityStockDao.save(commodityStock);
        stockInOutDao.save(entity);

        // 级联更新商品的汇总实际库存量
        Commodity commodity = commodityDao.findOne(commodityStock.getCommodity().getId());
        BigDecimal commoditySumStockQuantity = BigDecimal.ZERO;
        List<CommodityStock> commodityStocks = commodityStockDao.findByCommodity(commodity);
        for (CommodityStock cs : commodityStocks) {
            commoditySumStockQuantity.add(cs.getCurStockQuantity());
        }
        commodity.setTotalRealStockCount(commoditySumStockQuantity);
        commodityDao.save(commodity);
    }

    public void redword(String voucher, VoucherTypeEnum voucherType, String redwordVoucher) {
        List<StockInOut> stockInOuts = stockInOutDao.findByVoucherAndVoucherType(voucher, voucherType);
        for (StockInOut stockInOut : stockInOuts) {
            entityManager.detach(stockInOut);
            stockInOut.setId(null);
            stockInOut.setVoucher(redwordVoucher);
            if (!stockInOut.getDiffPurchasingQuantity().equals(BigDecimal.ZERO)) {
                stockInOut.setDiffPurchasingQuantity(stockInOut.getDiffPurchasingQuantity().negate());
            }
            if (!stockInOut.getDiffSalingQuantity().equals(BigDecimal.ZERO)) {
                stockInOut.setDiffSalingQuantity(stockInOut.getDiffSalingQuantity().negate());
            }
            if (!stockInOut.getDiffQuantity().equals(BigDecimal.ZERO)) {
                stockInOut.setDiffQuantity(stockInOut.getDiffQuantity().negate());
            }
            saveCascade(stockInOut);
        }
    }

}
