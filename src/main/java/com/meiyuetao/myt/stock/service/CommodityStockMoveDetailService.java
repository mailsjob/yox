package com.meiyuetao.myt.stock.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.stock.dao.CommodityStockMoveDetailDao;
import com.meiyuetao.myt.stock.entity.CommodityStockMoveDetail;

@Service
@Transactional
public class CommodityStockMoveDetailService extends BaseService<CommodityStockMoveDetail, Long> {
    @Autowired
    private CommodityStockMoveDetailDao commodityStockMoveDetailDao;

    @Override
    protected BaseDao<CommodityStockMoveDetail, Long> getEntityDao() {
        return commodityStockMoveDetailDao;
    }

}
