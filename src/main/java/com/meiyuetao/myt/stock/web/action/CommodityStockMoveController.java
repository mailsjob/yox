package com.meiyuetao.myt.stock.web.action;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.service.Validation;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.constant.VoucherStateEnum;
import com.meiyuetao.myt.core.constant.VoucherTypeEnum;
import com.meiyuetao.myt.core.service.VoucherNumGenerateService;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.stock.entity.CommodityStockMove;
import com.meiyuetao.myt.stock.entity.CommodityStockMoveDetail;
import com.meiyuetao.myt.stock.service.CommodityStockMoveService;
import com.meiyuetao.myt.stock.service.StorageLocationService;

@MetaData("CommodityStockMoveController")
public class CommodityStockMoveController extends MytBaseController<CommodityStockMove, Long> {

    @Autowired
    private CommodityStockMoveService commodityStockMoveService;
    @Autowired
    private VoucherNumGenerateService voucherNumGenerateService;
    @Autowired
    private StorageLocationService storageLocationService;

    @Override
    protected BaseService<CommodityStockMove, Long> getEntityService() {
        return commodityStockMoveService;
    }

    @Override
    protected void checkEntityAclPermission(CommodityStockMove entity) {

    }

    @Override
    protected void setupDetachedBindingEntity(Long id) {
        bindingEntity = getEntityService().findDetachedOne(id, "commodityStockMoveDetails");
    }

    @Override
    public void prepareEdit() {
        super.prepareEdit();
        if (bindingEntity.isNew()) {
            bindingEntity.setVoucher(voucherNumGenerateService.getVoucherNumByType(VoucherTypeEnum.DH));
            bindingEntity.setVoucherDate(new Date());
            bindingEntity.setVoucherUser(getLogonUser());
            bindingEntity.setVoucherDepartment(getLogonUser().getDepartment());
        }
    }

    @Override
    public String isDisallowUpdate() {
        if (VoucherStateEnum.REDW.equals(bindingEntity.getVoucherState())) {
            return "已红冲的不能修改";
        }
        return null;
    }

    public String isDisallowChargeAgainst() {
        if (VoucherStateEnum.REDW.equals(bindingEntity.getVoucherState())) {
            return "已红冲";
        }
        Boolean isInStocked = false;
        for (CommodityStockMoveDetail commodityStockMoveDetail : bindingEntity.getCommodityStockMoveDetails()) {
            if (commodityStockMoveDetail.getInStockQuantity().compareTo(BigDecimal.ZERO) > 0) {
                isInStocked = true;
                break;
            }

        }
        if (isInStocked) {
            return "已入库不能红冲";
        }
        return null;
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.isNew()) {
            List<CommodityStockMoveDetail> commodityStockMoveDetails = bindingEntity.getCommodityStockMoveDetails();
            for (CommodityStockMoveDetail commodityStockMoveDetail : commodityStockMoveDetails) {
                commodityStockMoveDetail.setCommodityStockMove(bindingEntity);
            }
        }
        commodityStockMoveService.post(bindingEntity);
        /*
         * if (VoucherStateEnum.DRAFT.equals(bindingEntity.getVoucherState())) {
         * if
         * (VoucherStateEnum.POST.name().equals(getParameter("voucherState"))) {
         * bindingEntity.setVoucherState(VoucherStateEnum.POST);
         * commodityStockMoveService.post(bindingEntity); } else {
         * commodityStockMoveService.save(bindingEntity); } } else {
         * commodityStockMoveService.save(bindingEntity); }
         */
        setModel(OperationResult.buildSuccessResult("数据保存成功", bindingEntity));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("行项数据")
    public HttpHeaders commodityStockMoveDetails() {
        setModel(buildPageResultFromList(bindingEntity.getCommodityStockMoveDetails()));
        return buildDefaultHttpHeaders();
    }

    public HttpHeaders chargeAgainst() {
        if (!bindingEntity.isNew()) {
            Validation.isTrue(!VoucherStateEnum.REDW.equals(bindingEntity.getVoucherState()), "调货单已红冲");
            commodityStockMoveService.chargeAgainst(bindingEntity);
        }
        setModel(OperationResult.buildSuccessResult("红冲完成"));
        return buildDefaultHttpHeaders();
    }

}