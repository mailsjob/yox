package com.meiyuetao.myt.stock.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.service.BaseService;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.stock.entity.CommodityStock;
import com.meiyuetao.myt.stock.service.CommodityStockService;

@MetaData("ViewCommodityStockController")
public class ViewCommodityStockController extends MytBaseController<CommodityStock, Long> {
    @Autowired
    private CommodityStockService commodityStockService;

    @Override
    protected BaseService<CommodityStock, Long> getEntityService() {
        // TODO Auto-generated method stub
        return commodityStockService;
    }

    @Override
    protected void checkEntityAclPermission(CommodityStock entity) {
        // TODO Auto-generated method stub

    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        Pageable pageable = PropertyFilter.buildPageableFromHttpRequest(getRequest());
        String sidx = getRequest().getParameter("sidx");
        String sord = getRequest().getParameter("sord");
        String searchValue = getRequest().getParameter("searchValue");
        String sordSql = "";
        if (StringUtils.isNotBlank(sidx) && StringUtils.isNotBlank(sord) && !sidx.equals("createdDate")) {
            sordSql = "  order by " + sidx + " " + sord;
        }
        String searchSql = "";
        if (StringUtils.isNotBlank(searchValue)) {
            searchSql = "  where tab4.barcode='" + searchValue + "' or tab4.title='" + searchValue + "'";
        }
        String endTime = this.getRequest().getParameter("endTime");
        String storageLocationSid = this.getRequest().getParameter("storageLocationSid");
        String stockSql1 = "";
        String stockSql2 = "";
        if (StringUtils.isNotBlank(storageLocationSid)) {
            stockSql1 = "  where storage_location_sid= " + storageLocationSid;
            stockSql2 = " and bodc.storage_location_sid= " + storageLocationSid;
        }
        String sql = "select tab4.storage_display, tab4.barcode as barcode,tab4.title as title,tab4.cur_stock_quantity as cur_stock_quantity,"
                + "tab4.pre_in_quantity as pre_in_quantity,tab4.pre_out_quantity as pre_out_quantity,"
                + " (tab4.cur_stock_quantity+tab4.pre_in_quantity-tab4.pre_in_quantity) as can_use_quantity, " + " tab4.stock_threshold_quantity as stock_threshold_quantity, "
                + " (tab4.cur_stock_quantity+tab4.pre_in_quantity-tab4.pre_out_quantity-tab4.stock_threshold_quantity) as low_stock_dist"
                + " FROM (select tab2.storage_display,tab2.barcode,tab2.title,tab2.cur_stock_quantity,tab2.pre_in_quantity,"
                + " case when tab3.pre_out_quantity is  null then 0.00 " + " else tab3.pre_out_quantity end  as pre_out_quantity " + " ,tab2.stock_threshold_quantity"
                + " from (select CONVERT(varchar(64),ssl.code+'  '+ssl.title) as storage_display , t.cur_stock_quantity,"
                + " case when t.pre_in_quantity is  null then 0.00 else t.pre_in_quantity end as pre_in_quantity,"
                + " CONVERT(varchar(64),c.title) as title,CONVERT(varchar(20),c.barcode) as barcode,c.stock_threshold_quantity from ( "
                + " select sc.storage_location_sid,sum(cur_stock_quantity) as cur_stock_quantity ,sum(pre_in_quantity) as pre_in_quantity ,commodity_sid  "
                + " from iyb_stock_commodity sc " + stockSql1 + "  group by commodity_sid,storage_location_sid ) t, "
                + " iyb_commodity c,iyb_stock_storage_location ssl where t.commodity_sid=c.sid and ssl.sid=t.storage_location_sid) tab2"
                + " LEFT JOIN (select sum(tab1.quantity) as pre_out_quantity,tab1.barcode from "
                + " (select CONVERT(varchar(20),c.barcode) as barcode ,c.sid,bodc.quantity from iyb_box_order_detail bod , "
                + " iyb_box_order_detail_commodity bodc, iyb_commodity c " + " where bod.order_detail_status in ('S15O','S20PYD','S25PYD','S35C') and bod.reserve_delivery_time<='"
                + endTime + "'" + " and bodc.order_detail_sid=bod.sid " + stockSql2 + " and bodc.commodity_sid=c.sid) tab1 group by tab1.barcode) tab3 "
                + " on tab3.barcode=tab2.barcode" + " ) tab4 ";
        setModel(commodityStockService.findByPageNativeSQL(pageable, sql, searchSql + sordSql));
        return buildDefaultHttpHeaders();
    }
}