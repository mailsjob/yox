package com.meiyuetao.myt.stock.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.stock.entity.StorageLocation;
import com.meiyuetao.myt.stock.service.StorageLocationService;

@MetaData("库存地管理")
public class StorageLocationController extends MytBaseController<StorageLocation, Long> {
    @Autowired
    private StorageLocationService storageLocationService;

    @Override
    protected BaseService<StorageLocation, Long> getEntityService() {
        return storageLocationService;
    }

    @Override
    protected void checkEntityAclPermission(StorageLocation entity) {
        // Do nothing
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}