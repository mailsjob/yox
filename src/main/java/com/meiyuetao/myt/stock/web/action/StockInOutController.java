package com.meiyuetao.myt.stock.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.stock.entity.StockInOut;
import com.meiyuetao.myt.stock.service.StockInOutService;

@MetaData("StockInOutController")
public class StockInOutController extends MytBaseController<StockInOut, Long> {

    @Autowired
    private StockInOutService stockInOutService;

    @Override
    protected BaseService<StockInOut, Long> getEntityService() {
        return stockInOutService;
    }

    @Override
    protected void checkEntityAclPermission(StockInOut entity) {

    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}