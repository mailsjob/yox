package com.meiyuetao.myt.stock.web.action;

import java.math.BigDecimal;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.service.Validation;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.constant.VoucherTypeEnum;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.service.CommodityService;
import com.meiyuetao.myt.stock.entity.CommodityStock;
import com.meiyuetao.myt.stock.entity.StockInOut;
import com.meiyuetao.myt.stock.entity.StorageLocation;
import com.meiyuetao.myt.stock.service.CommodityStockService;
import com.meiyuetao.myt.stock.service.StockInOutService;
import com.meiyuetao.myt.stock.service.StorageLocationService;

@MetaData("商品库存")
public class CommodityStockController extends MytBaseController<CommodityStock, Long> {

    @Autowired
    private CommodityStockService commodityStockService;
    @Autowired
    private CommodityService commodityService;
    @Autowired
    private StorageLocationService storageLocationService;
    @Autowired
    private StockInOutService stockInOutService;

    @Override
    protected BaseService<CommodityStock, Long> getEntityService() {
        return commodityStockService;
    }

    @Override
    protected void checkEntityAclPermission(CommodityStock entity) {
        // TODO Auto-generated method stub

    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.isNotNew()) {
            CommodityStock oldCommodityStock = commodityStockService.findOne(bindingEntity.getId());
            /*
             * if
             * (!oldCommodityStock.getCurStockQuantity().equals(bindingEntity.
             * getCurStockQuantity()) ||
             * !oldCommodityStock.getSalingTotalQuantity
             * ().equals(bindingEntity.getSalingTotalQuantity()) ||
             * !oldCommodityStock.getPurchasingTotalQuantity().equals(
             * bindingEntity.getPurchasingTotalQuantity()) ||
             * oldCommodityStock.getCostPrice
             * ().compareTo(bindingEntity.getCostPrice()) != 0) {
             */
            if (oldCommodityStock.getCurStockQuantity().compareTo(bindingEntity.getCurStockQuantity()) != 0
                    || oldCommodityStock.getSalingTotalQuantity().compareTo(bindingEntity.getSalingTotalQuantity()) != 0
                    || oldCommodityStock.getPurchasingTotalQuantity().compareTo(bindingEntity.getPurchasingTotalQuantity()) != 0
                    || oldCommodityStock.getCostPrice().compareTo(bindingEntity.getCostPrice()) != 0) {
                // StockInOut stockInOut = new StockInOut();
                StockInOut stockInOut = new StockInOut(null, null, null, oldCommodityStock);
                // stockInOut.setCommodityStock(bindingEntity);
                stockInOut.setDiffQuantity(bindingEntity.getCurStockQuantity().subtract(oldCommodityStock.getCurStockQuantity()));
                BigDecimal newStockAmount = bindingEntity.getCostPrice().multiply(bindingEntity.getCurStockQuantity());
                stockInOut.setDiffStockAmount(newStockAmount.subtract(oldCommodityStock.getCurStockAmount()));
                stockInOut.setDiffPurchasingQuantity(bindingEntity.getPurchasingTotalQuantity().subtract(oldCommodityStock.getPurchasingTotalQuantity()));
                stockInOut.setDiffSalingQuantity(bindingEntity.getSalingTotalQuantity().subtract(oldCommodityStock.getSalingTotalQuantity()));
                /*
                 * bindingEntity.setCurStockQuantity(oldCommodityStock.
                 * getCurStockQuantity());
                 * bindingEntity.setPurchasingTotalQuantity
                 * (oldCommodityStock.getPurchasingTotalQuantity());
                 * bindingEntity
                 * .setSalingTotalQuantity(oldCommodityStock.getSalingTotalQuantity
                 * ());
                 */
                stockInOut.setOperationSummary("直接变更库存量数据");
                stockInOutService.saveCascade(stockInOut);
            } else {
                commodityStockService.doSave(bindingEntity);
            }
        } else {
            StockInOut stockInOut = new StockInOut();
            // 数量变化 = 实物库存数量
            stockInOut.setDiffQuantity(bindingEntity.getCurStockQuantity());
            // 商品金额变化 = 实物库存数量 * 当前成本价
            stockInOut.setDiffStockAmount(bindingEntity.getCurStockQuantity().multiply(bindingEntity.getCostPrice()));
            stockInOut.setDiffPurchasingQuantity(bindingEntity.getPurchasingTotalQuantity());
            stockInOut.setDiffSalingQuantity(bindingEntity.getSalingTotalQuantity());
            stockInOut.setOperationSummary("直接初始化库存量数据");
            // change by harvey 2015/4/15 解除注释
            bindingEntity.setCurStockQuantity(BigDecimal.ZERO);
            bindingEntity.setPurchasingTotalQuantity(BigDecimal.ZERO);
            bindingEntity.setSalingTotalQuantity(BigDecimal.ZERO);
            stockInOut.setCommodityStock(bindingEntity);
            stockInOutService.saveCascade(stockInOut);
        }
        setModel(OperationResult.buildSuccessResult("数据保存成功", bindingEntity));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("盘存显示")
    public HttpHeaders inventory() {
        return buildDefaultHttpHeaders("inventory");
    }

    public HttpHeaders findForInventory() {
        String barcode = getRequiredParameter("barcode");
        Commodity commodity = commodityService.findByBarcode(barcode);
        if (commodity == null) {
            setModel(OperationResult.buildFailureResult("未知商品数据: " + barcode));
        } else {
            String batchNo = getParameter("batchNo");
            StorageLocation storageLocation = storageLocationService.findOne(getId("storageLocationId"));
            CommodityStock commodityStock = commodityStockService.findBy(commodity, storageLocation, batchNo);
            Validation.isTrue(commodityStock != null, "无库存数据，请检查录入数据或先初始化库存数据");
            setModel(commodityStock);
        }
        return buildDefaultHttpHeaders();
    }

    @MetaData("盘存")
    public HttpHeaders doInventory() {
        CommodityStock commodityStock = commodityStockService.findBy(bindingEntity.getCommodity(), bindingEntity.getStorageLocation(), bindingEntity.getBatchNo());
        StockInOut stockInOut = new StockInOut();
        stockInOut.setCommodityStock(commodityStock);
        stockInOut.setVoucherType(VoucherTypeEnum.PC);
        stockInOut.setDiffQuantity(bindingEntity.getCurStockQuantity().subtract(commodityStock.getCurStockQuantity()));
        stockInOut.setDiffStockAmount(stockInOut.getDiffQuantity().multiply(commodityStock.getCostPrice()));
        String inventoryExplain = getParameter("inventoryExplain");
        if (StringUtils.isNotBlank(inventoryExplain)) {
            stockInOut.setOperationSummary("移动盘存:" + inventoryExplain);
        } else {
            stockInOut.setOperationSummary("移动盘存: 无变更登记");
        }
        stockInOutService.saveCascade(stockInOut);
        setModel(OperationResult.buildSuccessResult("数据保存成功", bindingEntity));
        return buildDefaultHttpHeaders();
    }

    @MetaData(value = "按库存地汇总库存量")
    public HttpHeaders findByGroupStorageLocation() {
        setModel(findByGroupAggregate("commodity.id", "commodity.sku", "commodity.barcode", "commodity.title", "storageLocation.id", "sum(curStockQuantity)",
                "sum(salingTotalQuantity)", "sum(purchasingTotalQuantity)", "sum(stockThresholdQuantity)",
                "sum(diff(diff(sum(curStockQuantity,purchasingTotalQuantity),salingTotalQuantity),stockThresholdQuantity))"));
        return buildDefaultHttpHeaders();
    }

    @MetaData(value = "按商品汇总库存量")
    public HttpHeaders findByGroupCommodity() {
        setModel(findByGroupAggregate("commodity.id", "commodity.sku", "commodity.barcode", "commodity.title", "sum(curStockQuantity)", "sum(salingTotalQuantity)",
                "sum(purchasingTotalQuantity)", "sum(stockThresholdQuantity)",
                "sum(diff(diff(sum(curStockQuantity,purchasingTotalQuantity),salingTotalQuantity),stockThresholdQuantity))"));
        return buildDefaultHttpHeaders();
    }
}