package com.meiyuetao.myt.stock.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;

@MetaData("库存调拨单明细")
@Entity
@Table(name = "myt_commodity_stock_move_detail")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityStockMoveDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("库存调拨单")
    private CommodityStockMove commodityStockMove;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("批次号")
    private String batchNo;
    @MetaData("单位")
    private String measureUnit;
    @MetaData("商品成本单")
    private BigDecimal price;
    @MetaData("成本金额")
    private BigDecimal amount;
    @MetaData("数量")
    private BigDecimal quantity;
    @MetaData("运费")
    private BigDecimal deliveryAmount = BigDecimal.ZERO;
    @MetaData("入库成本单价")
    private BigDecimal costPrice;
    @MetaData("入库总成本")
    private BigDecimal costAmount = BigDecimal.ZERO;
    @MetaData("已入库数量")
    private BigDecimal inStockQuantity = BigDecimal.ZERO;
    @MetaData(value = "过期时间")
    private Date expireDate;

    @ManyToOne
    @JoinColumn(name = "commodity_stock_move_sid", nullable = false)
    public CommodityStockMove getCommodityStockMove() {
        return commodityStockMove;
    }

    public void setCommodityStockMove(CommodityStockMove commodityStockMove) {
        this.commodityStockMove = commodityStockMove;
    }

    @OneToOne
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getCostAmount() {
        return costAmount;
    }

    public void setCostAmount(BigDecimal costAmount) {
        this.costAmount = costAmount;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(BigDecimal deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getInStockQuantity() {
        return inStockQuantity;
    }

    public void setInStockQuantity(BigDecimal inStockQuantity) {
        this.inStockQuantity = inStockQuantity;
    }

    @Column(length = 128)
    @JsonProperty
    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    @JsonSerialize(using = DateJsonSerializer.class)
    @JsonProperty
    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }
}
