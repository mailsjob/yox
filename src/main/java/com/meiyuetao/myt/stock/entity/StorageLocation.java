package com.meiyuetao.myt.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.constant.StorageModeEnum;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@Entity
@MetaData("库存地信息")
@Table(name = "iyb_stock_storage_location")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class StorageLocation extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("代码")
    private String code;
    @MetaData("名称")
    private String title;
    @MetaData("地址")
    private String addr;
    @MetaData("失效")
    private Boolean disabled;
    @MetaData(value = "库存模式", comments = "用在采购、销售流程，控制入库出库方式")
    private StorageModeEnum storageMode = StorageModeEnum.MU;

    @Column(length = 32, unique = true, nullable = false)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 512)
    @JsonProperty
    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    @JsonProperty
    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    @Transient
    @JsonProperty
    public String getDisplay() {
        return code + " " + title;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public StorageModeEnum getStorageMode() {
        return storageMode;
    }

    public void setStorageMode(StorageModeEnum storageMode) {
        this.storageMode = storageMode;
    }

}