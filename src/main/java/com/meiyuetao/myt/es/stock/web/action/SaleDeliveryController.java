package com.meiyuetao.myt.es.stock.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.exception.DataAccessDeniedException;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.security.AclService;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.partner.entity.Partner;
import com.meiyuetao.myt.sale.entity.SaleDelivery;
import com.meiyuetao.myt.sale.entity.SaleDeliveryDetail;
import com.meiyuetao.myt.sale.service.ExpressDataTemplateService;
import com.meiyuetao.myt.sale.service.SaleDeliveryService;
import com.opensymphony.xwork2.interceptor.ParameterNameAware;

@MetaData("合作库房的销售单管理")
public class SaleDeliveryController extends MytBaseController<SaleDelivery, Long> implements ParameterNameAware {

    @Autowired
    private SaleDeliveryService saleDeliveryService;
    @Autowired
    private ExpressDataTemplateService expressDataTemplateService;
    @Autowired
    private AclService aclService;

    @Override
    protected BaseService<SaleDelivery, Long> getEntityService() {
        return saleDeliveryService;
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    protected void checkEntityAclPermission(SaleDelivery entity) {
        // 限定只能访问登录用户所在商家的数据
        String aclCodePrefix = aclService.getLogonUserAclCodePrefix();
        Partner partner = entity.getStockPartner();
        if (partner != null && StringUtils.isNotBlank(aclCodePrefix)) {
            String partnerCode = partner.getCode();
            if (!partnerCode.startsWith(aclCodePrefix)) {
                throw new DataAccessDeniedException();
            }
        }
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NN, "auditDate", true));
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "redwordDate", true));
        // 限定只能访问登录用户所在商家的数据
        String aclCodePrefix = aclService.getLogonUserAclCodePrefix();
        if (StringUtils.isNotBlank(aclCodePrefix)) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.BW, "stockPartner.code", aclCodePrefix));
        }
    }

    @MetaData("行项数据")
    public HttpHeaders saleDeliveryDetails() {
        List<SaleDeliveryDetail> saleDeliveryDetails = bindingEntity.getSaleDeliveryDetails();
        setModel(buildPageResultFromList(saleDeliveryDetails));
        return buildDefaultHttpHeaders();
    }

    @Override
    public HttpHeaders doSave() {
        return super.doSave();
    }

    public HttpHeaders printSave() {
        saleDeliveryService.doSave(bindingEntity);
        setModel(OperationResult.buildSuccessResult("操作成功"));
        return buildDefaultHttpHeaders();
    }

    public HttpHeaders deliverySave() {
        saleDeliveryService.deliverySave(bindingEntity);
        setModel(OperationResult.buildSuccessResult("发货完成"));
        return buildDefaultHttpHeaders();
    }

    private static String[] acceptableParameterNames = new String[] { "logistics.id", "logisticsNo", "logisticsAmount", "deliveryMode", "pickUpPerson", "pickUpCompany",
            "pickUpMobile", "pickUpDate", "pickUpAuditBy", "pickUpDeliveryAddr" };

    /**
     * 对于需要给外部用户访问的Controller为了避免用户非法篡改数据 以 @see ParameterNameAware
     * 形式设置自动绑定参数白名单
     */
    @Override
    public boolean acceptableParameterName(String parameterName) {
        return acceptableParameterName(acceptableParameterNames, parameterName);
    }

}