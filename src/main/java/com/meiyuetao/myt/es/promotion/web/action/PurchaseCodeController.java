package com.meiyuetao.myt.es.promotion.web.action;

import java.util.Date;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.partner.entity.PurchaseCode;
import com.meiyuetao.myt.partner.entity.PurchaseCode.PurchaseCodeStatusEnum;
import com.meiyuetao.myt.partner.service.PurchaseCodeService;

@MetaData("合作推广商的消费码管理")
public class PurchaseCodeController extends MytBaseController<PurchaseCode, Long> {
    @Autowired
    private PurchaseCodeService purchaseCodeService;

    @Override
    protected BaseService<PurchaseCode, Long> getEntityService() {
        return purchaseCodeService;
    }

    @Override
    protected void checkEntityAclPermission(PurchaseCode entity) {

    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    public HttpHeaders findByPage() {

        return super.findByPage();
    }

    public HttpHeaders useCode() {
        if (PurchaseCodeStatusEnum.ENABLE.equals(bindingEntity.getCodeStatus()) && bindingEntity.getUsedDatetime() == null) {
            bindingEntity.setUsedDatetime(new Date());
            bindingEntity.setCodeStatus(PurchaseCodeStatusEnum.USED);
        } else {
            throw new RuntimeException("此消费码不可使用");
        }
        purchaseCodeService.save(bindingEntity);
        setModel(OperationResult.buildSuccessResult("消费码成功使用"));
        return buildDefaultHttpHeaders();
    }

}