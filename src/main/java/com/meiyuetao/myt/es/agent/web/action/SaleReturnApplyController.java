package com.meiyuetao.myt.es.agent.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.sale.entity.SaleReturnApply;
import com.meiyuetao.myt.sale.service.SaleReturnApplyService;
import com.opensymphony.xwork2.interceptor.ParameterNameAware;

@MetaData("SaleReturnApplyController")
public class SaleReturnApplyController extends MytBaseController<SaleReturnApply, Long> implements ParameterNameAware {

    @Autowired
    private SaleReturnApplyService saleReturnApplyService;

    @Override
    protected BaseService<SaleReturnApply, Long> getEntityService() {
        return saleReturnApplyService;
    }

    @Override
    protected void checkEntityAclPermission(SaleReturnApply entity) {
        // 限定只能访问登录用户所在商家的数据
        /*
         * Partner partner = entity.getAgentPartner(); if (partner != null &&
         * !partner.equals(this.getLogonPartner())) { throw new
         * DataAccessDeniedException(); }
         */
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        // 限定只能访问登录用户所在商家的数据
        // groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ,
        // "agentPartner", this.getLogonPartner()));

    }

    /*
     * @Override public String isDisallowUpdate() { if
     * (bindingEntity.getAgentPartnerReceiveTime() != null) { return "已确认收货"; }
     * return null; }
     */

    /*
     * @MetaData("保存") public HttpHeaders doReceive() {
     * bindingEntity.setAgentPartnerReceiveTime(new Date()); return
     * super.doSave(); }
     */
    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    private static String[] acceptableParameterNames = new String[] { "agentPartnerReceiveTime" };

    /**
     * 对于需要给外部用户访问的Controller为了避免用户非法篡改数据 以 @see ParameterNameAware
     * 形式设置自动绑定参数白名单
     */
    @Override
    public boolean acceptableParameterName(String parameterName) {
        return acceptableParameterName(acceptableParameterNames, parameterName);
    }

}