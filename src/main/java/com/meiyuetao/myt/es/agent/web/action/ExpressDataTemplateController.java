package com.meiyuetao.myt.es.agent.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.annotation.SecurityControlIgnore;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.sale.entity.ExpressDataTemplate;
import com.meiyuetao.myt.sale.service.ExpressDataTemplateService;

@MetaData("ExpressDataTemplateController")
public class ExpressDataTemplateController extends MytBaseController<ExpressDataTemplate, Long> {

    @Autowired
    private ExpressDataTemplateService expressDataTemplateService;

    @Override
    protected BaseService<ExpressDataTemplate, Long> getEntityService() {
        return expressDataTemplateService;
    }

    @Override
    protected void checkEntityAclPermission(ExpressDataTemplate entity) {
        // 限定只能访问登录用户所在商家的数据
        Assert.isTrue(getLogonPartner().equals(entity.getAgentPartner()));
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        // 限定只能访问登录用户所在商家的数据
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "agentPartner", getLogonPartner()));
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        bindingEntity.setAgentPartner(getLogonPartner());
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("列表数据")
    @SecurityControlIgnore
    public HttpHeaders list() {
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildFromHttpRequest(entityClass, getRequest());
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "agentPartner", getLogonPartner()));
        setModel(buildPageResultFromList(getEntityService().findByFilters(groupPropertyFilter)));
        return buildDefaultHttpHeaders();
    }
}