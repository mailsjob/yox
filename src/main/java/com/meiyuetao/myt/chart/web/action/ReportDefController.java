package com.meiyuetao.myt.chart.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.rpt.entity.ReportDef;

import org.apache.struts2.rest.HttpHeaders;

import com.meiyuetao.myt.core.web.MytBaseController;

@MetaData(value = "报表定义管理")
public class ReportDefController extends MytBaseController<ReportDef, String> {

    @Override
    protected BaseService<ReportDef, String> getEntityService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void checkEntityAclPermission(ReportDef entity) {
        // TODO Auto-generated method stub

    }

    @MetaData("创建初始化")
    public HttpHeaders bpmNew() {
        return buildDefaultHttpHeaders("dynamic");
    }

}