package com.meiyuetao.myt.job;

import com.meiyuetao.myt.customer.entity.CustomerCoinsHistory;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.customer.service.CustomerCoinsHistoryService;
import com.meiyuetao.myt.customer.service.CustomerProfileService;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.schedule.BaseQuartzJobBean;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by zhuhui on 16-3-31.
 */
public class AutoRechargeCoinsDailyJob extends BaseQuartzJobBean {
    private final static Logger logger = LoggerFactory.getLogger(AutoRechargeCoinsDailyJob.class);

    @Override
    protected void executeInternalBiz(JobExecutionContext jobExecutionContext) {
        logger.debug("AutoRechargeCoinsDailyJob executing...");
        CustomerProfileService customerProfileService = this.getSpringBean(CustomerProfileService.class);
        CustomerCoinsHistoryService customerCoinsHistoryService = this.getSpringBean(CustomerCoinsHistoryService.class);
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(PropertyFilter.MatchType.EQ, "customerFrom", "SD"));
        groupPropertyFilter.append(new PropertyFilter(PropertyFilter.MatchType.LT, "lastRechargeTime", new Date()));
        List<CustomerProfile> lstCustomerProfile = customerProfileService.findByFilters(groupPropertyFilter);
        if (!CollectionUtils.isEmpty(lstCustomerProfile)) {
            for (CustomerProfile cp : lstCustomerProfile) {
                BigDecimal oldCoin = BigDecimal.ZERO;
                BigDecimal oldBaseScore = BigDecimal.ZERO;
                BigDecimal oldFreezeCoin = BigDecimal.ZERO;
                BigDecimal oldFreezenScore = BigDecimal.ZERO;
                BigDecimal cumuCoin = BigDecimal.ZERO;
                BigDecimal oldGivenScore = BigDecimal.ZERO;
                BigDecimal dailyCumuCoin = BigDecimal.ZERO;
                if (null != cp.getCoins()) {
                    oldCoin = cp.getCoins();
                }
                if (null != cp.getFreezeCoins()) {
                    oldFreezeCoin = cp.getFreezeCoins();
                }
                if (null != cp.getFreezenNewScore()) {
                    oldFreezenScore = cp.getFreezenNewScore();
                }
                if (null != cp.getCumuCoins()) {
                    cumuCoin = cp.getCumuCoins();
                }
                if (null != cp.getBaseNewScore()) {
                    oldBaseScore = cp.getBaseNewScore();
                }
                if (null != cp.getGivenNewScore()) {
                    oldGivenScore = cp.getGivenNewScore();
                }
                // 每日累积积分为（充值积分-冻结积分）*2%
                //dailyCumuCoin = (oldCoin.subtract(oldFreezeCoin)).multiply(BigDecimal.valueOf(0.02));
                dailyCumuCoin= (oldBaseScore.subtract(oldFreezenScore)).multiply(BigDecimal.valueOf(0.02));
                CustomerCoinsHistory cch = new CustomerCoinsHistory();
                cch.setCoins(dailyCumuCoin);
                cch.setGivenNewScore(oldGivenScore);
                cch.setCoinsBefore(oldCoin);
                cch.setCoinsAfter(oldCoin.add(dailyCumuCoin));
                cch.setCustomerProfile(cp);
                cch.setOperationType(CustomerCoinsHistory.OperationTypeEnum.DAILYCHARGE);
                cch.setMemo("自动充值" + dailyCumuCoin + "积分");
                customerCoinsHistoryService.save(cch);
                cp.setCoins(oldCoin.add(dailyCumuCoin));
                cp.setCumuCoins(cumuCoin.add(dailyCumuCoin));
                cp.setGivenNewScore(oldGivenScore.add(dailyCumuCoin));
                customerProfileService.save(cp);
            }
        }
        logger.debug("AutoRechargeCoinsDailyJob executing end.");
    }
}
