package com.meiyuetao.myt.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lab.s2jh.core.util.DateUtils;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.joda.time.DateTime;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jd.open.api.sdk.DefaultJdClient;
import com.jd.open.api.sdk.JdClient;
import com.jd.open.api.sdk.JdException;
import com.jd.open.api.sdk.domain.order.OrderSearchInfo;
import com.jd.open.api.sdk.request.order.OrderSearchRequest;
import com.jd.open.api.sdk.response.order.OrderSearchResponse;
import com.meiyuetao.myt.sale.entity.BoxOrder.BoxOrderStatusEnum;
import com.meiyuetao.myt.sale.service.BoxOrderService;

public class IybAutoImportJdOrderJob extends BaseQuartzJobBean {
    private static Logger logger = LoggerFactory.getLogger(IybAutoImportJdOrderJob.class);
    private static String url = "http://gw.api.360buy.com/routerjson";
    // 正式环境需要设置为:http://gw.api.taobao.com/router/rest
    private static String appkey = "401C75FE6500B58445F431B1FAA4E4CC";
    private static String appSecret = "1633170b75b5420c8621c30dee46ea34";
    private static String access_token = "028d3a78-20d3-472b-9967-a5f46d7c97f3";
    // 查询的页数
    private String page = "1";

    // 每页的条数（最大page_size 100条）
    private String pageSize = "100";
    /*
     * 多订单状态可以用英文逗号隔开。 WAIT_SELLER_STOCK_OUT 等待出库, SEND_TO_DISTRIBUTION_CENER
     * 发往配送中心（只适用于LBP，SOPL商家）, DISTRIBUTION_CENTER_RECEIVED
     * 配送中心已收货（只适用于LBP，SOPL商家）, WAIT_GOODS_RECEIVE_CONFIRM 等待确认收货,
     * RECEIPTS_CONFIRM 收款确认（服务完成）（只适用于LBP，SOPL商家）,
     * WAIT_SELLER_DELIVERY等待发货（只适用于海外购商家，等待境内发货 标签下的订单）, FINISHED_L 完成,
     * TRADE_CANCELED 取消（取消的订单不返回收货人基本信息）, LOCKED 已锁定（锁定的订单不返回收货人基本信息）
     */
    private String orderState = "FINISHED_L,WAIT_SELLER_STOCK_OUT,WAIT_GOODS_RECEIVE_CONFIRM";

    private Map<String, BoxOrderStatusEnum> map = new HashMap<String, BoxOrderStatusEnum>() {
        {
            put("WAIT_SELLER_STOCK_OUT", BoxOrderStatusEnum.S30C);
            put("SEND_TO_DISTRIBUTION_CENER", BoxOrderStatusEnum.S50DF);
            put("DISTRIBUTION_CENTER_RECEIVED", BoxOrderStatusEnum.S50DF);
            put("WAIT_GOODS_RECEIVE_CONFIRM", BoxOrderStatusEnum.S60R);
            put("RECEIPTS_CONFIRM", BoxOrderStatusEnum.S30C);
            put("WAIT_SELLER_DELIVERY", BoxOrderStatusEnum.S30C);
            put("FINISHED_L", BoxOrderStatusEnum.S70CLS);
            put("TRADE_CANCELED", BoxOrderStatusEnum.S90CANCLE);
            put("LOCKED", BoxOrderStatusEnum.S90CANCLE);

        }
    };

    protected void executeInternalBiz(JobExecutionContext context) {
        BoxOrderService boxOrderService = this.getSpringBean(BoxOrderService.class);

        JdClient client = new DefaultJdClient(url, access_token, appkey, appSecret);
        OrderSearchRequest request = new OrderSearchRequest();
        request.setPage(page);
        request.setPageSize(pageSize);
        // order_state=WAIT_SELLER_STOCK_OUT
        // 等待出库，则start_date可以为"否"（开始时间和结束时间均为空，默认返回前一个月的订单）；
        // order_state=其他值，则start_date必须为"是"（开始时间和结束时间，不得相差超过1个月。此时间仅针对订单状态及运单号修改的时间）
        DateTime now = new DateTime();
        DateTime starTime = now.minusMonths(1).plusDays(1);
        request.setStartDate(DateUtils.formatTime(starTime.toDate()));
        request.setEndDate(DateUtils.formatTime(now.toDate()));
        request.setOrderState(orderState);
        OrderSearchResponse response;
        List<OrderSearchInfo> osis = null;
        try {
            response = client.execute(request);
            osis = response.getOrderInfoResult().getOrderInfoList();
        } catch (JdException e1) {
            logger.error(e1.getMessage());
        }
        String result = boxOrderService.importOrderFromJd(osis, map);
        context.setResult(result);
    }
}
