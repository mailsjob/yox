package com.meiyuetao.myt.job;

import lab.s2jh.schedule.BaseQuartzJobBean;

import org.quartz.JobExecutionContext;

import com.meiyuetao.myt.c2c.service.C2cRunningAccountService;

/**
 * 每月5日将上个月的补贴转到卖家美月淘主账户
 * 
 * @author harvey
 * 
 */
public class C2cCloseSubsideMonthJob extends BaseQuartzJobBean {

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        C2cRunningAccountService runningService = this.getSpringBean(C2cRunningAccountService.class);
        runningService.closeSubside();
    }
}