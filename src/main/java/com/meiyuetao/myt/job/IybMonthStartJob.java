package com.meiyuetao.myt.job;

import java.util.List;

import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.quartz.JobExecutionContext;

import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.service.CommodityService;

public class IybMonthStartJob extends BaseQuartzJobBean {

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        CommodityService commodityService = this.getSpringBean(CommodityService.class);

        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        // 商品的canBuyNow不为true时（非现货），才把价格计划更新至哎呦价
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "canBuyNow", Boolean.FALSE));
        List<Commodity> commodities = commodityService.findByFilters(groupPropertyFilter);
        for (Commodity commodity : commodities) {
            commodityService.updatePriceByCommodityPrice(commodity);
        }
    }
}
