package com.meiyuetao.myt.job;

import lab.s2jh.schedule.BaseQuartzJobBean;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestJob extends BaseQuartzJobBean {

    private final static Logger logger = LoggerFactory.getLogger(TestJob.class);

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        logger.debug("Just for quartz cron trigger job testing...");
        context.setResult("TestJob execute success.");
    }

}
