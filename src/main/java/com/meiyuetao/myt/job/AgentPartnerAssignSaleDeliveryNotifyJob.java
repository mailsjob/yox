package com.meiyuetao.myt.job;

import java.util.Date;
import java.util.List;
import java.util.Map;

import lab.s2jh.auth.entity.User;
import lab.s2jh.auth.service.UserService;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.ctx.MailService;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Maps;
import com.meiyuetao.myt.partner.entity.Partner;
import com.meiyuetao.myt.sale.entity.SaleDelivery;
import com.meiyuetao.myt.sale.service.SaleDeliveryService;

public class AgentPartnerAssignSaleDeliveryNotifyJob extends BaseQuartzJobBean {
    private static Logger logger = LoggerFactory.getLogger(AgentPartnerAssignSaleDeliveryNotifyJob.class);

    protected void executeInternalBiz(JobExecutionContext context) {
        SaleDeliveryService saleDeliveryService = this.getSpringBean(SaleDeliveryService.class);
        UserService userService = this.getSpringBean(UserService.class);
        MailService mailService = this.getSpringBean(MailService.class);
        BusinessNotifyService businessNotifyService = this.getSpringBean(BusinessNotifyService.class);

        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        // 审核通过的
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NN, "auditDate", true));
        // 没有红冲的
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "redwordDate", true));
        // 已指派代理商
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NN, "agentPartner", true));
        // 未接单
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "agentAcceptDate", true));
        // agentAssignDate大于当前时间的
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.LT, "agentAssignDate", new Date()));
        List<SaleDelivery> saleDeliverys = saleDeliveryService.findByFilters(groupPropertyFilter);
        if (!CollectionUtils.isEmpty(saleDeliverys)) {
            Map<Partner, Integer> map = Maps.newHashMap();
            // 统计各代理商未接单数
            for (SaleDelivery saleDelivery : saleDeliverys) {
                // 已指派代理商和上次指派不一样
                Partner agentPartner = saleDelivery.getAgentPartner();
                if (!agentPartner.equals(saleDelivery.getAgentPartnerNotified())) {
                    saleDelivery.setAgentPartnerNotified(agentPartner);
                    Integer count = map.get(agentPartner);
                    count = (count == null ? 0 : count);
                    map.put(agentPartner, ++count);
                    saleDeliveryService.save(saleDelivery);
                }
            }
            // 发送邮件通知
            for (Partner partner : map.keySet()) {
                List<User> users = userService.findByAclCode(partner.getCode());
                for (User user : users) {
                    String emall = user.getEmail();
                    if (emall != null && emall.length() > 0) {
                        String emailContent = "您有 " + map.get(partner) + " 个未接收的销售单指派";
                        mailService.sendHtmlMail("代理商派单通知", emailContent, false, emall);
                    }
                    String mobilePhone = user.getMobilePhone();
                    if (mobilePhone != null && mobilePhone.length() >= 11) {
                        String content = "您有 " + map.get(partner) + " 个未接收的销售单指派";
                        businessNotifyService.sendMsgAsync(mobilePhone, content);
                    }
                }
            }
        }
        logger.info("AgentPartnerAssignSaleDeliveryNotifyJob执行完毕。");
    }
}
