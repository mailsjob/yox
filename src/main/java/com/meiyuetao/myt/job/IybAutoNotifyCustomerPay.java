package com.meiyuetao.myt.job;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.apache.commons.collections.CollectionUtils;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meiyuetao.myt.sale.entity.BoxOrderDetail;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail.BoxOrderDetailStatusEnum;
import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;
import com.meiyuetao.myt.sale.service.BoxOrderDetailCommodityService;
import com.meiyuetao.myt.sale.service.BoxOrderDetailService;

public class IybAutoNotifyCustomerPay extends BaseQuartzJobBean {
    // private static int MAX_TRY = 3;
    private final static Logger logger = LoggerFactory.getLogger(BusinessNotifyService.class);

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        BoxOrderDetailService boxOrderDetailService = this.getSpringBean(BoxOrderDetailService.class);
        // JdbcTemplate jdbcTemplate = this.getSpringBean(JdbcTemplate.class);
        // BusinessNotifyService businessNotifyService = this.getSpringBean(BusinessNotifyService.class);
        BoxOrderDetailCommodityService boxOrderDetailCommodityService = this.getSpringBean(BoxOrderDetailCommodityService.class);

        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        Calendar remindDate = Calendar.getInstance();
        remindDate.setTime(new Date());
        remindDate.add(Calendar.DAY_OF_YEAR, 14);
        groupPropertyFilter.append(new PropertyFilter(MatchType.LT, "reserveDeliveryTime", remindDate.getTime()));
        groupPropertyFilter.append(new PropertyFilter(MatchType.IN, "orderDetailStatus", new BoxOrderDetailStatusEnum[] { BoxOrderDetailStatusEnum.S15O,
                BoxOrderDetailStatusEnum.S10O }));
        groupPropertyFilter.append(new PropertyFilter(MatchType.NU, "payTime", true));
        List<BoxOrderDetail> boxOrderDetails = boxOrderDetailService.findByFilters(groupPropertyFilter);
        int i = 0;
        if (!CollectionUtils.isEmpty(boxOrderDetails)) {
            for (BoxOrderDetail boxOrderDetail : boxOrderDetails) {
                // 发送邮件
                if (i < 1) {
                    groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
                    groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "boxOrderDetail", boxOrderDetail));
                    List<BoxOrderDetailCommodity> boxOrderDetailCommodities = boxOrderDetailCommodityService.findByFilters(groupPropertyFilter);
                    boxOrderDetail.setBoxOrderDetailCommodities(boxOrderDetailCommodities);
                    logger.debug("订单号{} - 行项{}", boxOrderDetail.getBoxOrder().getOrderSeq(), boxOrderDetail.getSn());
                    // businessNotifyService.sendPayNotifyEmail(boxOrderDetail);
                    // businessNotifyService.sendPayNotifySms(boxOrderDetail);
                    break;
                }

            }
        }
    }
}
