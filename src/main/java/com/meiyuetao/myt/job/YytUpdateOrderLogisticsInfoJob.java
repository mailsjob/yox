package com.meiyuetao.myt.job;

import java.util.List;

import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionContext;

import com.meiyuetao.myt.kuajing.service.YytOrderInfoService;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrder.YytPostStatusEnum;
import com.meiyuetao.myt.sale.service.BoxOrderService;

public class YytUpdateOrderLogisticsInfoJob extends BaseQuartzJobBean {

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        BoxOrderService boxOrderService = this.getSpringBean(BoxOrderService.class);
        YytOrderInfoService yytOrderInfoService = this.getSpringBean(YytOrderInfoService.class);
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "yytPostStatus", YytPostStatusEnum.Y_1));
        List<BoxOrder> boxOrders = boxOrderService.findByFilters(groupPropertyFilter);
        String[] orderSeqArray = new String[boxOrders.size()];
        int i = 0;
        // /
        for (BoxOrder boxOrder : boxOrders) {
            if (StringUtils.isBlank(boxOrder.getYytLogisticsNo())) {
                orderSeqArray[i++] = boxOrder.getOrderSeq();
            }

        }
        String orderSeqs = StringUtils.join(orderSeqArray, ",");
        String result = yytOrderInfoService.sendFbOutInfo(orderSeqs);
        context.setResult(result);
    }
}
