package com.meiyuetao.myt.job;

import java.util.List;

import lab.s2jh.core.audit.envers.ExtRevisionListener;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.joda.time.LocalDateTime;
import org.quartz.JobExecutionContext;
import org.springframework.util.CollectionUtils;

import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrder.BoxOrderStatusEnum;
import com.meiyuetao.myt.sale.service.BoxOrderService;

public class IybAutoCancelOrderJob extends BaseQuartzJobBean {

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        BoxOrderService boxOrderService = this.getSpringBean(BoxOrderService.class);
        // 自动取消48小时未付款订单
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "orderStatus", BoxOrderStatusEnum.S10O));
        groupPropertyFilter.append(new PropertyFilter(MatchType.LT, "orderTime", new LocalDateTime().minusDays(2).toDate()));
        List<BoxOrder> boxOrders = boxOrderService.findByFilters(groupPropertyFilter);
        if (!CollectionUtils.isEmpty(boxOrders)) {
            // 审计记录业务操作说明
            ExtRevisionListener.setOperationExplain("自动取消48小时未付款订单");
            for (BoxOrder boxOrder : boxOrders) {
                boxOrderService.cancelOrder(boxOrder);
            }
        }
    }
}
