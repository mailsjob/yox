package com.meiyuetao.myt.job;

import lab.s2jh.schedule.BaseQuartzJobBean;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meiyuetao.myt.c2c.service.C2cRunningAccountService;

public class C2cUnfreezeDailyJob extends BaseQuartzJobBean {

    private final static Logger logger = LoggerFactory.getLogger(C2cUnfreezeDailyJob.class);

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        logger.debug("C2cUnfreezeDailyJob executing...");
        C2cRunningAccountService runningService = this.getSpringBean(C2cRunningAccountService.class);
        runningService.unfreeze();
    }
}
