package com.meiyuetao.myt.job;

import lab.s2jh.schedule.BaseQuartzJobBean;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutoImportUserJob extends BaseQuartzJobBean {
    private static Logger logger = LoggerFactory.getLogger(AutoImportUserJob.class);

    /**
     * 每天9~24时 间隔30分钟触发一次
     */
    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        // InactiveUserService inactiveUserService =
        // this.getSpringBean(InactiveUserService.class);
        // String msg = inactiveUserService.autoImportUser();
        // context.setResult(msg);

        // BusinessNotifyService businessNotifyService =
        // this.getSpringBean(BusinessNotifyService.class);
        // businessNotifyService.sendTextSms("13261432567",
        // "测试短信"+System.currentTimeMillis());
        // businessNotifyService.sendMsgAsync("13261432567",
        // "测试短信"+System.currentTimeMillis());
        logger.info("AutoImportUserJob执行完毕。");
    }

}