package com.meiyuetao.myt.job;

import java.util.HashMap;
import java.util.List;

import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meiyuetao.myt.sale.entity.SaleReturnApply;
import com.meiyuetao.myt.sale.service.SaleReturnApplyService;

public class AutoDetectionSaleReturnApply extends BaseQuartzJobBean {
    private static Logger logger = LoggerFactory.getLogger(AutoDetectionSaleReturnApply.class);

    protected void executeInternalBiz(JobExecutionContext context) {
        SaleReturnApplyService saleReturnApplyService = this.getSpringBean(SaleReturnApplyService.class);
        // 自动创建退货流程
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        // 没有启动流程的（即凭证号为NULL）
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "voucher", true));

        List<SaleReturnApply> SaleReturnApplys = saleReturnApplyService.findByFilters(groupPropertyFilter);

        for (SaleReturnApply entity : SaleReturnApplys) {
            // 启动流程
            saleReturnApplyService.bpmCreate(entity, new HashMap<String, Object>());
            // break;
        }
        logger.info("AutoDetectionSaleReturnApply执行完毕。");
    }
}
