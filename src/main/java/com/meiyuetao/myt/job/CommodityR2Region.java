package com.meiyuetao.myt.job;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.apache.commons.collections.CollectionUtils;
import org.quartz.JobExecutionContext;

import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.Commodity.CommodityStatusEnum;
import com.meiyuetao.myt.md.entity.Commodity.SolrFilterTypeEnum;
import com.meiyuetao.myt.md.entity.CommodityDistributRegion;
import com.meiyuetao.myt.md.entity.Region;
import com.meiyuetao.myt.md.service.CommodityDistributRegionService;
import com.meiyuetao.myt.md.service.CommodityService;
import com.meiyuetao.myt.md.service.RegionService;

@MetaData("商品销售区域默认赋值")
public class CommodityR2Region extends BaseQuartzJobBean {

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        CommodityService commodityService = this.getSpringBean(CommodityService.class);
        RegionService regionService = this.getSpringBean(RegionService.class);
        CommodityDistributRegionService commodityDistributRegionService = this.getSpringBean(CommodityDistributRegionService.class);
        String regionIds = "13,1,4,14,20,21,29,24,18,5,10,19,25,17,9,11,15,8,33,31,27,22,16,3,6,28,2,26,12";
        String[] regionsIdArr = regionIds.split(",");
        GroupPropertyFilter comgpf = GroupPropertyFilter.buildDefaultAndGroupFilter();
        comgpf.append(new PropertyFilter(MatchType.EQ, "commodityStatus", CommodityStatusEnum.S30ONSALE));
        comgpf.append(new PropertyFilter(MatchType.EQ, "solrFilterType", SolrFilterTypeEnum.LM));
        List<Commodity> commoditys = commodityService.findByFilters(comgpf);
        if (CollectionUtils.isNotEmpty(commoditys)) {
            for (Commodity commodity : commoditys) {
                if (!commodity.getRegionsId().isEmpty()) {
                    continue;
                }
                for (String regionId : regionsIdArr) {
                    Region region = regionService.findOne(Long.valueOf(regionId));
                    GroupPropertyFilter regpf = GroupPropertyFilter.buildDefaultAndGroupFilter();
                    regpf.append(new PropertyFilter(MatchType.EQ, "region", region));
                    regpf.append(new PropertyFilter(MatchType.EQ, "commodity", commodity));
                    List<CommodityDistributRegion> commodityDistributRegions = commodityDistributRegionService.findByFilters(regpf);
                    if (!CollectionUtils.isEmpty(commodityDistributRegions)) {
                        continue;
                    } else {
                        CommodityDistributRegion commodityDistributRegion = new CommodityDistributRegion();
                        commodityDistributRegion.setCommodity(commodity);
                        commodityDistributRegion.setRegion(region);
                        commodityDistributRegionService.save(commodityDistributRegion);
                    }
                }
                commodity.setRegionsId(regionIds);
                commodity.setRegions("安徽,北京,重庆,福建,广东,甘肃,广西,贵州,湖北,河北,黑龙江,湖南,海南,河南,吉林,江苏,江西,辽宁,内蒙古,宁夏,青海,四川,山东,上海,山西,陕西,天津,云南,浙江");
                commodityService.save(commodity);
            }
        }
    }
}
