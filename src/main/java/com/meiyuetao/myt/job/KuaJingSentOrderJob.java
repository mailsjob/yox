package com.meiyuetao.myt.job;

import java.util.List;

import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.quartz.JobExecutionContext;

import com.meiyuetao.myt.kuajing.service.KjOrderInfoService;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrder.KuaJingStatusEnum;
import com.meiyuetao.myt.sale.service.BoxOrderService;

public class KuaJingSentOrderJob extends BaseQuartzJobBean {

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        // CommodityService commodityService =
        // this.getSpringBean(CommodityService.class);
        BoxOrderService boxOrderService = this.getSpringBean(BoxOrderService.class);
        KjOrderInfoService kjOrderInfoService = this.getSpringBean(KjOrderInfoService.class);

        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "kuaJingStatus", KuaJingStatusEnum.UNSEND));
        List<BoxOrder> boxOrders = boxOrderService.findByFilters(groupPropertyFilter);
        for (BoxOrder boxOrder : boxOrders) {
            kjOrderInfoService.sendToKuaJing(boxOrder);
        }
    }
}
