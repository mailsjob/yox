package com.meiyuetao.myt.pub.web.action;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import lab.s2jh.core.util.DateUtils;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.util.DigestUtils;

public class KuaJingTest {
    private static String url = "http://113.204.136.28/KJClientReceiver/Data.aspx";
    private static String userName = "MYTC102015";
    private static String passWord = "MYTC102015";

    /*
     * private static String userName = "P201500001"; private static String
     * passWord = "P201500001";
     */

    private static String generateOrderReturnInfoXml() {
        String uuid = UUID.randomUUID().toString();
        String md5Pwd = DigestUtils.md5DigestAsHex((uuid + passWord).getBytes());
        String xmlString = "<?xml version='1.0' encoding='utf-8'?><DTC_Message>" + "<MessageHead>" + " <MessageType>RDER_RETURN_INFO</MessageType>" + "<MessageId>" + uuid
                + "</MessageId>" + "<ActionType>1</ActionType>" + "<MessageTime>" + DateUtils.formatTime(new Date()) + "</MessageTime>" + " <SenderId>" + userName + "</SenderId>"
                + " <ReceiverId>CQITC</ReceiverId>" + " <UserNo>" + userName + "</UserNo>" + " <Password>" + md5Pwd + "</Password>" + "</MessageHead>" + " <MessageBody>"
                + " <DTCFlow>" + " <RDER_RETURN_INFO>" + "<ORIGINAL_ORDER_NO>mytdd01220001</ORIGINAL_ORDER_NO>" + "<ESHOP_ENT_CODE>" + userName + "</ESHOP_ENT_CODE>"
                + "<RETURN_REASON>七天无理由退货</RETURN_REASON>" + "<QUALITY_REPORT>质检不合格</QUALITY_REPORT>" + "</RDER_RETURN_INFO>" + "</DTCFlow>" + "</MessageBody>" + "</DTC_Message>";
        return xmlString;
    }

    private static String generateTaxPrepayCommandXml() {
        String uuid = UUID.randomUUID().toString();
        String md5Pwd = DigestUtils.md5DigestAsHex((uuid + passWord).getBytes());
        String xmlString = "<?xml version='1.0' encoding='utf-8'?><DTC_Message>" + "<MessageHead>" + " <MessageType>PAYMENT_INFO</MessageType>" + "<MessageId>" + uuid
                + "</MessageId>" + "<ActionType>1</ActionType>" + "<MessageTime>" + DateUtils.formatTime(new Date()) + "</MessageTime>" + " <SenderId>" + userName + "</SenderId>"
                + " <ReceiverId>CQITC</ReceiverId>" + " <UserNo>" + userName + "</UserNo>" + " <Password>" + md5Pwd + "</Password>" + "</MessageHead>" + " <MessageBody>"
                + " <DTCFlow>" + " <ORDER_HEAD>" + " <PRE_PAY_NO>8013</CUSTOMS_CODE>" + " <ORDER_NO>I20</BIZ_TYPE_CODE>" + "<TAX_FEE>0</TAX_FEE>"
                + "<CURRENCY_CODE>142</CURRENCY_CODE>" + "<BANK_CODE>B0005B250000001</BANK_CODE>" + "<BANK_ACCOUNT>500102022018110061317</BANK_ACCOUNT>" + "<ESHOP_ENT_CODE>"
                + userName + "</ESHOP_ENT_CODE>" + " <ESHOP_ENT_NAME>" + "重庆美月淘电子商务有限公司" + "</ESHOP_ENT_NAME>" + "</ORDER_HEAD>" + "</DTCFlow>" + "</MessageBody>"
                + "</DTC_Message>";
        return xmlString;
    }

    private static String generateTaxReturnCommandXml() {
        String uuid = UUID.randomUUID().toString();
        String md5Pwd = DigestUtils.md5DigestAsHex((uuid + passWord).getBytes());
        String xmlString = "<?xml version='1.0' encoding='utf-8'?><DTC_Message>" + "<MessageHead>" + " <MessageType>TAX_RETURN_COMMAND</MessageType>" + "<MessageId>" + uuid
                + "</MessageId>" + "<ActionType>1</ActionType>" + "<MessageTime>" + DateUtils.formatTime(new Date()) + "</MessageTime>" + " <SenderId>" + userName + "</SenderId>"
                + " <ReceiverId>CQITC</ReceiverId>" + " <UserNo>" + userName + "</UserNo>" + " <Password>" + md5Pwd + "</Password>" + "</MessageHead>" + " <MessageBody>"
                + " <DTCFlow>" + " <TAX_RETURN_COMMAND>" + " <PRE_PAY_NO>8013</CUSTOMS_CODE>" + " <ORDER_NO>BMYT-I150122-0000002</BIZ_TYPE_CODE>" + "<TAX_FEE>1</TAX_FEE>"
                + "<CURRENCY_CODE>142</CURRENCY_CODE>" + "<BANK_CODE>B0005B250000001</BANK_CODE>" + "<BANK_ACCOUNT>500102022018110061317</BANK_ACCOUNT>" + "<ESHOP_ENT_CODE>"
                + userName + "</ESHOP_ENT_CODE>" + " <ESHOP_ENT_NAME>" + "重庆美月淘电子商务有限公司" + "</ESHOP_ENT_NAME>" + "</TAX_RETURN_COMMAND>" + "</DTCFlow>" + "</MessageBody>"
                + "</DTC_Message>";
        return xmlString;
    }

    public static void main(String[] args) {
        String xmlString = generateOrderInfoXml();
        String postData = "";
        try {
            postData = new String(Base64.encodeBase64(xmlString.getBytes()), "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        // postData = new String(Base64.decodeBase64(postData.getBytes()));
        String response = null;
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        CloseableHttpClient client = httpClientBuilder.build();
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
        formparams.add(new BasicNameValuePair("data", postData));
        UrlEncodedFormEntity entity;
        // 设置Post数据
        try {
            entity = new UrlEncodedFormEntity(formparams, "UTF-8");
            httpPost.setEntity(entity);
            HttpResponse httpResponse;
            // post请求
            httpResponse = client.execute(httpPost);
            int status = httpResponse.getStatusLine().getStatusCode();
            if (HttpStatus.SC_OK == status) {
                System.out.println("OK");

            }
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                response = EntityUtils.toString(httpEntity, "UTF-8");
                System.out.println("response:" + response);
            }

            client.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 订单对应分运单号
    private static String generateOrderSetTransportNoXml() {
        String uuid = UUID.randomUUID().toString();
        String md5Pwd = DigestUtils.md5DigestAsHex((uuid + passWord).getBytes());
        String xmlString = "<?xml version='1.0' encoding='utf-8'?><DTC_Message>" + "<MessageHead>" + " <MessageType>ORDER_SET_TRANSPORT_NO</MessageType>" + "<MessageId>" + uuid
                + "</MessageId>" + "<ActionType>1</ActionType>" + "<MessageTime>" + DateUtils.formatTime(new Date()) + "</MessageTime>" + " <SenderId>" + userName + "</SenderId>"
                + " <ReceiverId>CQITC</ReceiverId>" + " <UserNo>" + userName + "</UserNo>" + " <Password>" + md5Pwd + "</Password>" + "</MessageHead>" + " <MessageBody>"
                + " <DTCFlow>" + " <ORDER_SET_TRANSPORT_NO>" + "<ORIGINAL_ORDER_NO>mytdd01230001</ORIGINAL_ORDER_NO>" + "<TRANSPORT_BILL_NO>ydh01230002</TRANSPORT_BILL_NO>"
                + "<ESHOP_ENT_CODE>" + userName + "</ESHOP_ENT_CODE>" + " <ESHOP_ENT_NAME>" + "重庆美月淘电子商务有限公司" + "</ESHOP_ENT_NAME>" + "</ORDER_SET_TRANSPORT_NO>" + "</DTCFlow>"
                + "</MessageBody>" + "</DTC_Message>";
        return xmlString;
    }

    private static String generatePaymentInfoXml() {
        String uuid = UUID.randomUUID().toString();
        String md5Pwd = DigestUtils.md5DigestAsHex((uuid + passWord).getBytes());
        String xmlString = "<?xml version='1.0' encoding='utf-8'?><DTC_Message>" + "<MessageHead>" + " <MessageType>PAYMENT_INFO</MessageType>" + "<MessageId>" + uuid
                + "</MessageId>" + "<ActionType>1</ActionType>" + "<MessageTime>" + DateUtils.formatTime(new Date()) + "</MessageTime>" + " <SenderId>" + userName + "</SenderId>"
                + " <ReceiverId>CQITC</ReceiverId>" + " <UserNo>" + userName + "</UserNo>" + " <Password>" + md5Pwd + "</Password>" + "</MessageHead>" + " <MessageBody>"
                + " <DTCFlow>" + " <PAYMENT_INFO>" + " <CUSTOMS_CODE>8013</CUSTOMS_CODE>" + " <BIZ_TYPE_CODE>I20</BIZ_TYPE_CODE>" + "<ESHOP_ENT_CODE>" + userName
                + "</ESHOP_ENT_CODE>" + " <ESHOP_ENT_NAME>" + "重庆美月淘电子商务有限公司" + "</ESHOP_ENT_NAME>" + "<PAYMENT_ENT_CODE>B0005B250000001</PAYMENT_ENT_CODE>"
                + "<PAYMENT_ENT_NAME>交通银行</PAYMENT_ENT_NAME>" + "<PAYMENT_NO>ZF01230003</PAYMENT_NO>" + "<ORIGINAL_ORDER_NO>mytdd01230003</ORIGINAL_ORDER_NO>"
                + "<PAY_AMOUNT>770</PAY_AMOUNT>" + "<GOODS_FEE>700</GOODS_FEE>" + "<TAX_FEE>70</TAX_FEE>" + "<CURRENCY_CODE>142</CURRENCY_CODE>" + "<MEMO>支付单</MEMO>"
                + "</PAYMENT_INFO>" + "</DTCFlow>" + "</MessageBody>" + "</DTC_Message>";
        return xmlString;
    }

    private static String generateOrderInfoXml() {
        String uuid = UUID.randomUUID().toString();
        String md5Pwd = DigestUtils.md5DigestAsHex((uuid + passWord).getBytes());
        String xmlString = "<?xml version='1.0' encoding='utf-8'?><DTC_Message>" + "<MessageHead>" + " <MessageType>ORDER_INFO</MessageType>" + "<MessageId>" + uuid
                + "</MessageId>" + "<ActionType>1</ActionType>" + "<MessageTime>" + DateUtils.formatTime(new Date()) + "</MessageTime>" + " <SenderId>" + userName + "</SenderId>"
                + " <ReceiverId>CQITC</ReceiverId>" + " <UserNo>" + userName + "</UserNo>" + " <Password>" + md5Pwd + "</Password>" + "</MessageHead>" + " <MessageBody>"
                + " <DTCFlow>" + " <ORDER_HEAD>" + " <CUSTOMS_CODE>8013</CUSTOMS_CODE>" + " <BIZ_TYPE_CODE>I20</BIZ_TYPE_CODE>"
                + " <ORIGINAL_ORDER_NO>mytdd1502031720</ORIGINAL_ORDER_NO>" + "<ESHOP_ENT_CODE>" + userName + "</ESHOP_ENT_CODE>" + " <ESHOP_ENT_NAME>" + "重庆美月淘电子商务有限公司"
                + "</ESHOP_ENT_NAME>" + "<DESP_ARRI_COUNTRY_CODE>324</DESP_ARRI_COUNTRY_CODE>" + "<SHIP_TOOL_CODE>Y</SHIP_TOOL_CODE>"
                + "<RECEIVER_ID_NO>412728198811156034</RECEIVER_ID_NO>" + "<RECEIVER_NAME>郭超</RECEIVER_NAME>" + "<RECEIVER_ADDRESS>北京朝阳望京soho</RECEIVER_ADDRESS>"
                + "<RECEIVER_TEL>18911910015</RECEIVER_TEL>" + "<GOODS_FEE>700</GOODS_FEE>" + "<TAX_FEE>70</TAX_FEE>" + "<GROSS_WEIGHT>2.0</GROSS_WEIGHT>"
                + "<SORTLINE_ID>SORTLINE02</SORTLINE_ID>"

                + "<ORDER_DETAIL>" + "<SKU>myt0119005</SKU>" + "<GOODS_SPEC>XL01</GOODS_SPEC>" + "<CURRENCY_CODE>142</CURRENCY_CODE>" + "<PRICE>100</PRICE>" + "<QTY>3</QTY>"
                + "<GOODS_FEE>300</GOODS_FEE>" + "<TAX_FEE>30</TAX_FEE>" + "</ORDER_DETAIL>"

                + "<ORDER_DETAIL>" + "<SKU>myt0119004</SKU>" + "<GOODS_SPEC>XL01</GOODS_SPEC>" + "<CURRENCY_CODE>142</CURRENCY_CODE>" + "<PRICE>100</PRICE>" + "<QTY>4</QTY>"
                + "<GOODS_FEE>400</GOODS_FEE>" + "<TAX_FEE>40</TAX_FEE>" + "</ORDER_DETAIL>" + "</ORDER_HEAD>" + "</DTCFlow>" + "</MessageBody>" + "</DTC_Message>";
        return xmlString;
    }

    // P201500001300111 P201500001300101
    private static String generateSkuInfoXml() {
        String uuid = UUID.randomUUID().toString();
        String md5Pwd = DigestUtils.md5DigestAsHex((uuid + passWord).getBytes());
        String xmlString = "<?xml version='1.0' encoding='utf-8'?><DTC_Message>" + "<MessageHead>" + " <MessageType>SKU_INFO</MessageType>" + "<MessageId>" + uuid + "</MessageId>"
                + "<ActionType>1</ActionType>" + "<MessageTime>" + DateUtils.formatTime(new Date()) + "</MessageTime>" + " <SenderId>" + userName + "</SenderId>"
                + " <ReceiverId>CQITC</ReceiverId>" + " <UserNo>" + userName + "</UserNo>" + " <Password>" + md5Pwd + "</Password>" + "</MessageHead>" + "<MessageBody>"
                + "<DTCFlow>"

                + "<SKU_INFO>" + "<ESHOP_ENT_CODE>" + userName + "</ESHOP_ENT_CODE>" + "<ESHOP_ENT_NAME>重庆美月淘电子商务有限公司</ESHOP_ENT_NAME>" + "<SKU>myt0126013</SKU>"
                + "<GOODS_NAME>测试商品0119012</GOODS_NAME>" + "<LEGAL_UNIT>035</LEGAL_UNIT>" + "<CONV_LEGAL_UNIT_NUM>1</CONV_LEGAL_UNIT_NUM>" + "<GOODS_SPEC>XL02</GOODS_SPEC>"
                + "<DECLARE_UNIT>035</DECLARE_UNIT>" + "<POST_TAX_NO>01010700</POST_TAX_NO>" + "<IS_EXPERIMENT_GOODS>0</IS_EXPERIMENT_GOODS>" + "<HS_CODE>8419399001</HS_CODE>"
                + "<IN_AREA_UNIT>035</IN_AREA_UNIT>" + "<CONV_IN_AREA_UNIT_NUM>1</CONV_IN_AREA_UNIT_NUM>" + "</SKU_INFO>"

                + "</DTCFlow>" + "</MessageBody>" + "</DTC_Message>";
        return xmlString;
    }

}
