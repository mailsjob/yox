package com.meiyuetao.myt.pub.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.SimpleController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.md.service.RegionService;

@MetaData("RegionController")
public class RegionController extends SimpleController {
    @Autowired
    private RegionService regionService;

    @MetaData(value = "区域数据")
    public HttpHeaders all() {
        setModel(regionService.findTreeDatas());
        return buildDefaultHttpHeaders();
    }
}