package com.meiyuetao.myt.pub.web.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.SimpleController;
import lab.s2jh.core.web.json.HibernateAwareObjectMapper;
import lab.s2jh.crawl.filter.ParseFilter;
import lab.s2jh.crawl.service.CrawlService;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.rest.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import com.meiyuetao.myt.crawl.filter.AbstractCommodityParseFilter;
import com.meiyuetao.myt.crawl.service.ParseCommodityService;

@MetaData("爬虫")
public class CrawlController extends SimpleController {
    @Autowired
    private CrawlService crawlService;
    @Autowired
    private ParseCommodityService parseCommodityService;

    private final Logger logger = LoggerFactory.getLogger(CrawlController.class);

    public HttpHeaders doCrawl() {
        Map<String, Object> jsonMap = Maps.newHashMap();
        ObjectMapper objectMapper = HibernateAwareObjectMapper.getInstance();
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            String crawlUrl = request.getParameter("crawlUrl");
            if (StringUtils.isNotBlank(crawlUrl)) {
                crawlUrl = crawlUrl.trim();
                List<ParseFilter> parseFilters = crawlService.getParseFilters();
                Map<String, Object> simpleData = null;
                for (ParseFilter parseFilter : parseFilters) {
                    if (parseFilter instanceof AbstractCommodityParseFilter) {
                        AbstractCommodityParseFilter abstractCommodityParseFilter = (AbstractCommodityParseFilter) parseFilter;
                        simpleData = abstractCommodityParseFilter.parseSimpleData(crawlUrl);
                        if (simpleData != null) {
                            break;
                        }
                    }
                }
                if (simpleData == null) {
                    jsonMap.put("error", "01");
                    jsonMap.put("message", "Not find any match parse filter");
                    String jsoString = objectMapper.writeValueAsString(jsonMap);
                    setModel(jsoString);
                } else {
                    String jsoString = objectMapper.writeValueAsString(simpleData);
                    setModel(jsoString);
                }
            } else {
                jsonMap.put("error", "11");
                jsonMap.put("message", "miss parameter 'crawlUrl'");
                String jsoString = objectMapper.writeValueAsString(jsonMap);
                setModel(jsoString);
            }

        } catch (Exception e1) {
            logger.error(e1.getMessage());
        }
        return buildDefaultHttpHeaders();

    }
}