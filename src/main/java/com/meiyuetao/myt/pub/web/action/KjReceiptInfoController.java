package com.meiyuetao.myt.pub.web.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.SimpleController;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.rest.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.ext.ExtXStream;
import com.meiyuetao.myt.kuajing.service.KjReceiptInfoService;
import com.meiyuetao.myt.md.entity.Link;
import com.meiyuetao.myt.md.service.LinkService;
import com.meiyuetao.myt.statistics.vo.EsRspVo;
import com.thoughtworks.xstream.XStream;

@MetaData("KjReceiptInfoController")
public class KjReceiptInfoController extends SimpleController {

    private final Logger logger = LoggerFactory.getLogger(KjReceiptInfoController.class);

    @Autowired
    private LinkService linkService;
    @Autowired
    private KjReceiptInfoService kjReceiptInfoService;

    public HttpHeaders execute() {
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            String data = request.getParameter("data");
            if (StringUtils.isBlank(data)) {
                processRspInfo(new EsRspVo("1", "回执参数：'data'不能为空"));
                return buildDefaultHttpHeaders();
            }
            String xmlStr = new String(Base64.decodeBase64(data.getBytes()));
            Link link = new Link();
            link.setName("xmlStr");
            link.setXmlStr2(xmlStr);
            link.setEnable(true);
            linkService.save(link);
            kjReceiptInfoService.parseXmlToObject(xmlStr);

        } catch (Exception e) {
            logger.error("System Errror", e);
            Link link = new Link();
            link.setName("错误");
            link.setEnable(true);
            link.setErrorStr(e.getMessage());
            linkService.save(link);
            processRspInfo(new EsRspVo("1", e.getMessage()));
            return buildDefaultHttpHeaders();
        }
        processRspInfo(new EsRspVo("0", "成功"));
        return buildDefaultHttpHeaders();

    }

    private void setHttpServletResponse(String xmlinfo) {
        HttpServletResponse response = getResponse();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.setCharacterEncoding("utf-8");
        try {
            response.getWriter().print(xmlinfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processRspInfo(EsRspVo esRspVo) {
        XStream xstream = ExtXStream.createXstream(EsRspVo.class);
        xstream.processAnnotations(EsRspVo.class);
        String xmlhead = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        String xmlinfo = xmlhead + xstream.toXML(esRspVo);
        setHttpServletResponse(xmlinfo);
    }

}