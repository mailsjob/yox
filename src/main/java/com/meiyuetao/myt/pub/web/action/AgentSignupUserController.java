package com.meiyuetao.myt.pub.web.action;

import javax.servlet.http.HttpServletRequest;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.captcha.ImageCaptchaServlet;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.auth.entity.AgentSignupUser;
import com.meiyuetao.myt.auth.service.AgentSignupUserService;
import com.meiyuetao.myt.job.BusinessNotifyService;

@MetaData("代理商注册")
public class AgentSignupUserController extends BaseController<AgentSignupUser, Long> {

    @Autowired
    private AgentSignupUserService agentSignupUserService;
    @Autowired
    private BusinessNotifyService businessNotifyService;

    @Override
    protected BaseService<AgentSignupUser, Long> getEntityService() {
        return agentSignupUserService;
    }

    @Override
    protected void checkEntityAclPermission(AgentSignupUser entity) {
        // TODO Add acl check code logic
    }

    public HttpHeaders submit() {
        HttpServletRequest request = ServletActionContext.getRequest();
        String jCaptcha = getRequiredParameter("j_captcha");
        if (!ImageCaptchaServlet.validateResponse(request, jCaptcha)) {
            setModel(OperationResult.buildFailureResult("验证码不正确，请重新输入"));
            return buildDefaultHttpHeaders();
        }

        setModel(OperationResult.buildSuccessResult("账号注册成功"));
        return buildDefaultHttpHeaders();
    }

    public HttpHeaders getSmsCheckCode() {
        HttpServletRequest request = this.getRequest();
        String smsCode = RandomStringUtils.randomNumeric(6);
        request.getSession().setAttribute("SMS_CODE", smsCode);
        String mobilePhone = request.getParameter("mobilePhone");
        request.getSession().setAttribute("mobilePhone", mobilePhone);
        // String content = "代理商家注册校验码：" + smsCode;
        // businessNotifyService.sendMsgAsync(mobilePhone, content);
        setModel(OperationResult.buildSuccessResult("校验码已发送"));
        // setModel(OperationResult.buildSuccessResult(smsCode));
        return buildDefaultHttpHeaders();
    }

}