package com.meiyuetao.myt.customer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("每月目标")
@Entity
@Table(name = "myt_import_user_aim")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ImportUserAim extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("月份")
    private Integer month;

    @MetaData("目标")
    private Integer aimNum;

    @JsonProperty
    @Column(nullable = false)
    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    @JsonProperty
    @Column(nullable = false)
    public Integer getAimNum() {
        return aimNum;
    }

    public void setAimNum(Integer aimNum) {
        this.aimNum = aimNum;
    }
}
