package com.meiyuetao.myt.customer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.auth.entity.User;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("微信消息回复")
@Entity
@Table(name = "iyb_weixin_msg_reply")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class WeixinMsgReply extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("微信消息主对象")
    private WeixinMsg weixinMsg;
    @MetaData("回复内容类型")
    private ReplyTypeEnum replyType;
    @MetaData("回复内容")
    private String replyMsg;
    @MetaData("回复时间")
    private Date replyTime = new Date();
    @MetaData("回复者")
    private User replyUser;
    @MetaData("备注")
    private String memo;

    // text,image,voice,video,location,link
    public enum ReplyTypeEnum {

        @MetaData("文本")
        text,

        @MetaData("图片")
        image,

        @MetaData("语音")
        voice,

        @MetaData("视频")
        video,

        @MetaData("位置")
        location,

        @MetaData("链接")
        link;

    }

    @ManyToOne
    @JoinColumn(name = "weixin_msg_sid", nullable = false)
    public WeixinMsg getWeixinMsg() {
        return weixinMsg;
    }

    public void setWeixinMsg(WeixinMsg weixinMsg) {
        this.weixinMsg = weixinMsg;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    @JsonProperty
    public ReplyTypeEnum getReplyType() {
        return replyType;
    }

    public void setReplyType(ReplyTypeEnum replyType) {
        this.replyType = replyType;
    }

    @OneToOne
    @JoinColumn(name = "reply_user_sid")
    public User getReplyUser() {
        return replyUser;
    }

    public void setReplyUser(User replyUser) {
        this.replyUser = replyUser;
    }

    @Column(nullable = false)
    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(Date replyTime) {
        this.replyTime = replyTime;
    }

    @Column(length = 512, nullable = false)
    @JsonProperty
    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}
