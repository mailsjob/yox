package com.meiyuetao.myt.customer.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Region;

@MetaData(value = "客户收货地址")
@Entity
@Table(name = "iyb_receive_address")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ReceiveAddress extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "客户")
    @EntityAutoCode(search = true)
    private CustomerProfile customerProfile;

    @MetaData(value = "地区")
    @EntityAutoCode(order = 40, search = true)
    private Region region;

    @MetaData(value = "详细地址")
    @EntityAutoCode(order = 20, search = true)
    private String detailAddress;

    @MetaData(value = "收货人")
    @EntityAutoCode(order = 20, search = true)
    private String receivePerson;
    @MetaData(value = "手机号")
    @EntityAutoCode(order = 20, search = true)
    private String mobilePhone;
    @MetaData(value = "邮编")
    @EntityAutoCode(order = 20, search = true)
    private String postCode;

    @ManyToOne
    @JoinColumn(name = "customer_profile_sid")
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @ManyToOne
    @JoinColumn(name = "region_sid")
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getReceivePerson() {
        return receivePerson;
    }

    public void setReceivePerson(String receivePerson) {
        this.receivePerson = receivePerson;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

}
