package com.meiyuetao.myt.customer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("微信账户管理")
@Entity
@Table(name = "iyb_weixin_user")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class WeixinUser extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("客户信息")
    private CustomerProfile customerProfile;
    @MetaData("微信名字")
    private String weixinName;
    @MetaData("用户状态")
    private UserStatusEnum userStatus = UserStatusEnum.FOCUS;
    @MetaData("首次点击时间")
    private Date firstFocusTime;
    @MetaData("最后点击时间")
    private Date lastFocusTime;
    @MetaData("首次绑定时间")
    private Date firstBindingTime;
    @MetaData("最后绑定时间")
    private Date lastBingdingTime;

    public enum UserStatusEnum {
        @MetaData("关注")
        FOCUS,

        @MetaData("取消关注")
        CANCEL;
    }

    @OneToOne
    @JoinColumn(name = "iyb_customer_profile")
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Column(name = "weixin_name", length = 64)
    @JsonProperty
    public String getWeixinName() {
        return weixinName;
    }

    public void setWeixinName(String weixinName) {
        this.weixinName = weixinName;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "user_status", length = 16)
    @JsonProperty
    public UserStatusEnum getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatusEnum userStatus) {
        this.userStatus = userStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getFirstFocusTime() {
        return firstFocusTime;
    }

    public void setFirstFocusTime(Date firstFocusTime) {
        this.firstFocusTime = firstFocusTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getLastFocusTime() {
        return lastFocusTime;
    }

    public void setLastFocusTime(Date lastFocusTime) {
        this.lastFocusTime = lastFocusTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getFirstBindingTime() {
        return firstBindingTime;
    }

    public void setFirstBindingTime(Date firstBindingTime) {
        this.firstBindingTime = firstBindingTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getLastBingdingTime() {
        return lastBingdingTime;
    }

    public void setLastBingdingTime(Date lastBingdingTime) {
        this.lastBingdingTime = lastBingdingTime;
    }

}
