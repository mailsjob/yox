package com.meiyuetao.myt.customer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("客户实名认证")
@Entity
@Table(name = "iyb_name_certificate")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class NameCertificate extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("客户信息")
    private CustomerProfile customerProfile;
    @MetaData("真实姓名")
    private String trueName;
    @MetaData("身份证号")
    private String idCardNo;
    @MetaData("电话号")
    private String mobileNo;
    @MetaData("上传照片1")
    private String photo1;
    @MetaData("上传照片2")
    private String photo2;
    @MetaData("上传照片3")
    private String photo3;
    @MetaData("上传照片4")
    private String photo4;
    @MetaData("说明")
    private String comment;
    @MetaData("认证类型")
    private CertificateTypeEnum certificateType;
    @MetaData("认证状态")
    private CertificateStatusEnum certificateStatus;

    public enum CertificateTypeEnum {

        @MetaData("实名")
        S10SM,

        @MetaData("达人")
        S30DR;

    }

    public enum CertificateStatusEnum {

        @MetaData("新申请")
        S10APPLY, @MetaData("申请未通过")
        S20NOPASS,

        @MetaData("认证完成")
        S30COMPLETE;

    }

    @OneToOne
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @JsonProperty
    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    @JsonProperty
    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    @JsonProperty
    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @JsonProperty
    public String getPhoto1() {
        return photo1;
    }

    public void setPhoto1(String photo1) {
        this.photo1 = photo1;
    }

    @JsonProperty
    public String getPhoto2() {
        return photo2;
    }

    public void setPhoto2(String photo2) {
        this.photo2 = photo2;
    }

    @JsonProperty
    public String getPhoto3() {
        return photo3;
    }

    public void setPhoto3(String photo3) {
        this.photo3 = photo3;
    }

    @JsonProperty
    public String getPhoto4() {
        return photo4;
    }

    public void setPhoto4(String photo4) {
        this.photo4 = photo4;
    }

    @JsonProperty
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public CertificateTypeEnum getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(CertificateTypeEnum certificateType) {
        this.certificateType = certificateType;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public CertificateStatusEnum getCertificateStatus() {
        return certificateStatus;
    }

    public void setCertificateStatus(CertificateStatusEnum certificateStatus) {
        this.certificateStatus = certificateStatus;
    }

}
