package com.meiyuetao.myt.customer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.constant.MessageStateEnum;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("短信发送记录")
@Entity
@Table(name = "iyb_sms_log_detail")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class SmsDetailLog extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联短信主对象")
    private SmsLog smsLog;

    @MetaData(value = "短信内容")
    private String content;

    @MetaData(value = "请求发送号码")
    private String mobile;

    @MetaData(value = "状态")
    private MessageStateEnum state;

    @MetaData(value = "发送时间")
    private Date sendTime;

    @MetaData(value = "信息")
    private String memo;

    @MetaData(value = "短信通道")
    private SmsDetailChannelEnum smsChannel = SmsDetailChannelEnum.QYQQ;

    public enum SmsDetailChannelEnum {
        @MetaData(value = "云片")
        YP,

        @MetaData(value = "云通信")
        YTX,

        @MetaData(value = "三通")
        STONG,

        @MetaData(value = "QYQQ")
        QYQQ;
    }

    @ManyToOne
    @JoinColumn(name = "sms_log_sid", nullable = false)
    public SmsLog getSmsLog() {
        return smsLog;
    }

    public void setSmsLog(SmsLog smsLog) {
        this.smsLog = smsLog;
    }

    @Column(length = 512, nullable = false)
    @JsonProperty
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(nullable = false)
    @JsonProperty
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8, nullable = true)
    @JsonProperty
    public MessageStateEnum getState() {
        return state;
    }

    public void setState(MessageStateEnum state) {
        this.state = state;
    }

    @Column(nullable = false)
    @JsonProperty
    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8, nullable = false)
    @JsonProperty
    public SmsDetailChannelEnum getSmsChannel() {
        return smsChannel;
    }

    public void setSmsChannel(SmsDetailChannelEnum smsChannel) {
        this.smsChannel = smsChannel;
    }

}
