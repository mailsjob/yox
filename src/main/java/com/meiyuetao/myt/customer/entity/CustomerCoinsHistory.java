package com.meiyuetao.myt.customer.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import lab.s2jh.core.annotation.MetaData;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by zhuhui on 16-3-23.
 */
@MetaData("客户金币变更明细")
@Entity
@Table(name = "customer_coins_history")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CustomerCoinsHistory extends MytBaseEntity {
    @MetaData("变更前积分")
    private BigDecimal coinsBefore;
    @MetaData("变更后积分")
    private BigDecimal coinsAfter;
    @MetaData("本次操作变更积分")
    private BigDecimal coins;
    private BigDecimal baseNewScore;
    private BigDecimal givenNewScore;
    @MetaData("操作类型")
    private OperationTypeEnum operationType;
    @MetaData("订单")
    private BoxOrder order;
    @MetaData("客户")
    private CustomerProfile customerProfile;
    @MetaData("备注")
    private String memo;

    @JsonProperty
    public BigDecimal getCoinsBefore() {
        return coinsBefore;
    }

    public void setCoinsBefore(BigDecimal coinsBefore) {
        this.coinsBefore = coinsBefore;
    }

    @JsonProperty
    public BigDecimal getCoinsAfter() {
        return coinsAfter;
    }

    public void setCoinsAfter(BigDecimal coinsAfter) {
        this.coinsAfter = coinsAfter;
    }

    @JsonProperty
    public BigDecimal getCoins() {
        return coins;
    }

    public void setCoins(BigDecimal coins) {
        this.coins = coins;
    }

    @JsonProperty
    public BigDecimal getBaseNewScore() {
        if (baseNewScore == null){
            baseNewScore = BigDecimal.ZERO;
        }
        return baseNewScore;
    }

    public void setBaseNewScore(BigDecimal baseNewScore) {
        this.baseNewScore = baseNewScore;
    }

    @JsonProperty
    public BigDecimal getGivenNewScore() {
        if (givenNewScore == null){
            givenNewScore = BigDecimal.ZERO;
        }
        return givenNewScore;
    }

    public void setGivenNewScore(BigDecimal givenNewScore) {
        this.givenNewScore = givenNewScore;
    }

    @JsonProperty
    @Enumerated(EnumType.STRING)
    public OperationTypeEnum getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationTypeEnum operationType) {
        this.operationType = operationType;
    }

    @JsonProperty
    @OneToOne
    @JoinColumn(name = "order_sid")
    public BoxOrder getOrder() {
        return order;
    }

    public void setOrder(BoxOrder order) {
        this.order = order;
    }

    @JsonProperty
    @OneToOne
    @JoinColumn(name = "customer_profile_sid")
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public enum OperationTypeEnum {
        @MetaData("支付")
        PAY,

        @MetaData("充值")
        CHARGE,

        @MetaData("反充值")
        RECHARGE,

        @MetaData("商品返还积分")
        BACKCOINS,

        @MetaData("取消订单返还积分")
        CANCLECOINS,

        @MetaData("自动充值")
        DAILYCHARGE,

        @MetaData("满1年清除")
        YEARLYCLEAN;
    }
}
