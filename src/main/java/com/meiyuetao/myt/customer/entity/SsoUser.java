package com.meiyuetao.myt.customer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.PersistableEntity;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.constant.GenericSexEnum;

@MetaData("Sso用户")
@Entity
@Table(name = "T_SSO_USER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SsoUser extends PersistableEntity<Long> {
    private static final long serialVersionUID = 1L;
    @MetaData("登录id")
    private String loginid;
    @MetaData("第三方的登录id")
    private String providerUid;
    @MetaData("第三方的分类")
    private String providerType;
    @MetaData("昵称")
    private String nick;
    @MetaData("邮件")
    private String email;
    @MetaData("第三方登录时头像地址")
    private String imgUrl;
    @MetaData("性别")
    private GenericSexEnum sex = GenericSexEnum.U;
    @MetaData("电话号")
    private String mobilePhone;
    @MetaData("密码")
    private String password;
    @MetaData("注册时间")
    private Date registerTime;
    @MetaData("登录次数")
    private Integer totalLogonTimes = 0;
    @MetaData("最近登录时间")
    private Date lastLogonTime;
    @MetaData("是否可用")
    private Boolean enabled = Boolean.TRUE;

    @MetaData("流水号主键")
    protected Long id;
    private String realName;

    @MetaData(value = "最近操作摘要")
    private String lastOperationSummary;

    @MetaData("是否系统导入")
    private Boolean sysImport = Boolean.FALSE;
    @MetaData("是否回访")
    private Boolean returnVisit = Boolean.FALSE;

    @Id
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "native")
    @Column(name = "sid")
    @JsonProperty
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false, unique = true, updatable = false)
    @JsonProperty
    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    @JsonProperty
    public String getProviderUid() {
        return providerUid;
    }

    public void setProviderUid(String providerUid) {
        this.providerUid = providerUid;
    }

    @JsonProperty
    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    @JsonProperty
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @JsonProperty
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @JsonProperty
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    @JsonProperty
    public Integer getTotalLogonTimes() {
        return totalLogonTimes;
    }

    public void setTotalLogonTimes(Integer totalLogonTimes) {
        this.totalLogonTimes = totalLogonTimes;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getLastLogonTime() {
        return lastLogonTime;
    }

    public void setLastLogonTime(Date lastLogonTime) {
        this.lastLogonTime = lastLogonTime;
    }

    @Transient
    @Override
    public String getDisplay() {
        return loginid;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 1)
    @JsonProperty
    public GenericSexEnum getSex() {
        return sex;
    }

    public void setSex(GenericSexEnum sex) {
        this.sex = sex;
    }

    @Column(name = "is_enabled")
    @JsonProperty
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    @JsonProperty
    public String getLastOperationSummary() {
        return lastOperationSummary;
    }

    public void setLastOperationSummary(String lastOperationSummary) {
        this.lastOperationSummary = lastOperationSummary;
    }

    @JsonProperty
    @Column(name = "is_sys_import")
    public Boolean getSysImport() {
        return sysImport;
    }

    public void setSysImport(Boolean sysImport) {
        this.sysImport = sysImport;
    }

    @JsonProperty
    @Column(name = "is_return_visit")
    public Boolean getReturnVisit() {
        return returnVisit;
    }

    public void setReturnVisit(Boolean returnVisit) {
        this.returnVisit = returnVisit;
    }

}
