package com.meiyuetao.myt.customer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.meiyuetao.myt.c2c.entity.C2cShopInfo;
import com.meiyuetao.myt.chinababy.entity.CbLotteryWin;
import com.meiyuetao.myt.core.constant.GenericSexEnum;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.CommodityViewStat;
import com.meiyuetao.myt.partner.entity.Partner;
import com.meiyuetao.myt.vip.entity.VipCustomerProfile;
import com.meiyuetao.myt.vip.entity.VipScoreHistory;
import com.meiyuetao.o2o.entity.ObjectR2Pic;
import lab.s2jh.auth.entity.User;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@MetaData("客户信息")
@Entity
@Table(name = "iyb_customer_profile")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class CustomerProfile extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("客户名")
    private String customerName;
    @MetaData("真实姓名")
    private String trueName;
    @MetaData("昵称")
    private String nickName;
    @MetaData("生日")
    private Date birthday;
    @MetaData("性别")
    private GenericSexEnum sex = GenericSexEnum.U;
    @MetaData("宝宝性别")
    private GenericSexEnum babySex;
    @MetaData("是否有宝宝 HAS-有宝宝;WILL-即将有;NO-没有宝宝")
    private String babyStatus;
    @MetaData("宝宝生日")
    private Date babyBirthday;
    @MetaData("宝宝体重")
    private BigDecimal babyWight;
    @MetaData("宝宝身高")
    private BigDecimal babyHeight;
    // 0,盒子;1,新浪;2,腾讯;3,人人;4,豆瓣;5,淘宝;6,其他待定
    @MetaData("客户来源类型，0,盒子;1,新浪;2,腾讯;3,人人;4,豆瓣;5,淘宝;6,其他待定")
    private String customerNameType;
    @MetaData("头像")
    private String headPhoto;
    @MetaData("邮箱")
    private String email;
    @MetaData("发货地址")
    private String deliveryAddr;
    @MetaData("电话号")
    private String mobilePhone;
    @MetaData("最后一次记录的经度")
    private Float lastLng;
    @MetaData("最后一次记录的纬度")
    private Float lastLat;
    @MetaData("最近登录时间")
    private Date lastLogOnTime;
    @MetaData("token")
    private String token;
    @MetaData("积分")
    private Integer score = 0;
    @MetaData("冻结分数")
    private Integer frozenScore = 0;
    @MetaData("消费总金额 ")
    private BigDecimal spentMoney = BigDecimal.ZERO;
    @MetaData("信息状况")
    private String infoStatus;
    @MetaData("备注")
    private String adminMemo;
    @MetaData("推荐客户")
    private CustomerProfile intermediaryCustomerProfile;
    @MetaData("推荐人帐号")
    private String intermediary;
    @MetaData("推荐类型，0,盒子;1,新浪;2,腾讯;3,人人;4,豆瓣;5,淘宝;6,其他待定")
    private String intermediaryType;
    @MetaData("指向合并后的父帐号")
    private CustomerProfile parentCustomerProfile;
    @MetaData("现金帐户，即用户的余额")
    private BigDecimal currentAccount = BigDecimal.ZERO;
    @MetaData("定金帐户")
    private BigDecimal earnestMoneyAccount = BigDecimal.ZERO;
    @MetaData("冻结金额 ")
    private BigDecimal frozenAccount = BigDecimal.ZERO;
    @MetaData("签到金金额")
    private BigDecimal checkInAccount = BigDecimal.ZERO;
    @MetaData("推荐数量（人数）")
    private Integer recommendCount = 0;
    @MetaData("有效订单数量")
    private Integer validOrderCount = 0;
    @MetaData("签到次数")
    private Integer checkInCount = 0;
    @MetaData("最后签到时间")
    private Date lastCheckInTime;
    @MetaData("客户等级：NORMAL-普通用户；VIP-VIP用户；SVIP-尊享VIP用户")
    private CustomerLevelEnum customerLevel = CustomerLevelEnum.NORMAL;
    @MetaData("用户值，主要用于用户的等级判断，正常情况下等同于spent_money")
    private BigDecimal customerValue = BigDecimal.ZERO;
    @MetaData("提成比率设定值 ")
    private BigDecimal rewardRate;
    @MetaData("提成比率计算模式：VOC=始终以有效订单数计算为准，FIX=始终以设定值为准，MIN=以低值为准，MAX=以高值为准")
    private RewardModeEnum rewardMode = RewardModeEnum.VOC;
    @MetaData("待评价数量")
    private Integer readyCommentCount = 0;
    @MetaData("客户来源")
    private String customerFrom;
    @MetaData("注册用的ip")
    private String ipWhenRegister;
    @MetaData("客户类型")
    private CustomerBuyTypeEnum customerBuyType;
    @MetaData("认证类型")
    private CustomCertificateTypeEnum certificateType;
    @MetaData("可兑换的积分")
    private Integer canXScore = 0;
    @MetaData("节省钱数")
    private BigDecimal savedMoney = BigDecimal.ZERO;
    @MetaData("用户类型,第1位表示是新老用户,第2位表示是否通过注册码验证,第3位表示是否达人,第4位表示是否专家")
    private Integer customerType = 0;
    @MetaData("好友数")
    private Integer friendCount = 0;
    @MetaData("粉丝数")
    private Integer fansCount = 0;
    @MetaData("店铺范围")
    private Integer scope = 0;
    private List<CbLotteryWin> cbLotteryWins = Lists.newArrayList();
    @MetaData("绑定第三方合作伙伴 ")
    private Partner bindPartner;
    @MetaData("绑定系统用户")
    private User bindUser;
    @MetaData("授信额度")
    private BigDecimal weilaifuMaxAccount = BigDecimal.ZERO;
    @MetaData("剩余授信额度")
    private BigDecimal weilaifuAccount = BigDecimal.ZERO;
    @MetaData("兼职薪资")
    private BigDecimal partTimeSalary = BigDecimal.ZERO;
    @MetaData("关联的SSO用户，显示用")
    private SsoUser ssoUser;
    @MetaData("最近浏览的商品记录")
    private List<CommodityViewStat> commodityViewStats = Lists.newArrayList();
    @MetaData("VIP积分信息历史记录")
    private List<VipScoreHistory> vipScoreHistories = Lists.newArrayList();
    @MetaData("附近店铺")
    private Boolean toBusiness = Boolean.FALSE;
    @MetaData("vip客户表")
    private VipCustomerProfile vipCustomerProfile;
    @MetaData("店铺名称")
    private String shopName;
    @MetaData("店铺二维码")
    private String shopCodePic;
    @MetaData("店铺审核名称")
    private String shopNameAudit;
    @MetaData("审核状态")
    private ShopNameAuditStatusEnum shopNameAuditStatus;
    @MetaData("美月淘集市店铺")
    private C2cShopInfo c2cShopInfo;
    @MetaData("微信ID")
    private String weixinId;
    @MetaData("是否触屏")
    private IsTochEnum isToch = IsTochEnum.N;
    @MetaData("店铺描述")
    private String shopDescription;
    @MetaData("请求接口时间（秒）")
    private String ApiWaitTime;
    @MetaData("优蜜用户最近登录时间")
    private Date meivdLastLogOnTime;
    @MetaData("店铺图片")
    private List<ObjectR2Pic> shopPagePics = Lists.newArrayList();
    @MetaData("openid")
    private String openid;
    @MetaData("首次注册时间")
    private Date registrationTime;
    @MetaData("积分")
    private BigDecimal coins;
    private BigDecimal baseNewScore;
    @MetaData("冻结积分")
    private BigDecimal freezeCoins;
    private BigDecimal freezenNewScore;
    @MetaData("每日累积积分")
    private BigDecimal cumuCoins;
    private BigDecimal givenNewScore;
    private BigDecimal consumedNewScore;
    @MetaData("最近充值时间")
    private Date lastRechargeTime;
    @MetaData("积分是否可以自增章")
    private Boolean isGiven;

    @Column(length = 16)
    @JsonProperty
    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    @Temporal(TemporalType.DATE)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 1)
    @JsonProperty
    public GenericSexEnum getSex() {
        return sex;
    }

    public void setSex(GenericSexEnum sex) {
        this.sex = sex;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 1)
    @JsonProperty
    public GenericSexEnum getBabySex() {
        return babySex;
    }

    public void setBabySex(GenericSexEnum babySex) {
        this.babySex = babySex;
    }

    @Column(length = 8)
    public String getBabyStatus() {
        return babyStatus;
    }

    public void setBabyStatus(String babyStatus) {
        this.babyStatus = babyStatus;
    }

    @JsonSerialize(using = DateJsonSerializer.class)
    @Temporal(TemporalType.DATE)
    @JsonProperty
    public Date getBabyBirthday() {
        return babyBirthday;
    }

    public void setBabyBirthday(Date babyBirthday) {
        this.babyBirthday = babyBirthday;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getBabyWight() {
        return babyWight;
    }

    public void setBabyWight(BigDecimal babyWight) {
        this.babyWight = babyWight;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getBabyHeight() {
        return babyHeight;
    }

    public void setBabyHeight(BigDecimal babyHeight) {
        this.babyHeight = babyHeight;
    }

    @Column(length = 128)
    public String getHeadPhoto() {
        return headPhoto;
    }

    public void setHeadPhoto(String headPhoto) {
        this.headPhoto = headPhoto;
    }

    @Column(length = 128)
    @JsonProperty
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(length = 256)
    @JsonProperty
    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    @Column(length = 32)
    @JsonProperty
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public Float getLastLng() {
        return lastLng;
    }

    public void setLastLng(Float lastLng) {
        this.lastLng = lastLng;
    }

    public Float getLastLat() {
        return lastLat;
    }

    public void setLastLat(Float lastLat) {
        this.lastLat = lastLat;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty
    public Date getLastLogOnTime() {
        return lastLogOnTime;
    }

    public void setLastLogOnTime(Date lastLogOnTime) {
        this.lastLogOnTime = lastLogOnTime;
    }

    @Column(length = 1024)
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Column(length = 512)
    public String getAdminMemo() {
        return adminMemo;
    }

    public void setAdminMemo(String adminMemo) {
        this.adminMemo = adminMemo;
    }

    @Column(length = 64)
    public String getIntermediary() {
        return intermediary;
    }

    public void setIntermediary(String intermediary) {
        this.intermediary = intermediary;
    }

    @Column(length = 8)
    public String getIntermediaryType() {
        return intermediaryType;
    }

    public void setIntermediaryType(String intermediaryType) {
        this.intermediaryType = intermediaryType;
    }

    @ManyToOne
    @JoinColumn(name = "PARENT_PROFILE_SID", nullable = true)
    public CustomerProfile getParentCustomerProfile() {
        return parentCustomerProfile;
    }

    public void setParentCustomerProfile(CustomerProfile parentCustomerProfile) {
        this.parentCustomerProfile = parentCustomerProfile;
    }

    @Column(length = 64)
    @JsonProperty
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Column(length = 8)
    @JsonProperty
    public String getCustomerNameType() {
        return customerNameType;
    }

    public void setCustomerNameType(String customerNameType) {
        this.customerNameType = customerNameType;
    }

    @Transient
    @JsonProperty
    public String getDisplay() {
        if (StringUtils.isNotBlank(this.nickName)) {
            return this.nickName;
        }
        if (StringUtils.isNotBlank(this.trueName)) {
            return this.trueName;
        }
        return id.toString();
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getCurrentAccount() {
        return currentAccount;
    }

    public void setCurrentAccount(BigDecimal currentAccount) {
        this.currentAccount = currentAccount;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getEarnestMoneyAccount() {
        return earnestMoneyAccount;
    }

    public void setEarnestMoneyAccount(BigDecimal earnestMoneyAccount) {
        this.earnestMoneyAccount = earnestMoneyAccount;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getFrozenAccount() {
        return frozenAccount;
    }

    public void setFrozenAccount(BigDecimal frozenAccount) {
        this.frozenAccount = frozenAccount;
    }

    public Integer getRecommendCount() {
        return recommendCount;
    }

    public void setRecommendCount(Integer recommendCount) {
        this.recommendCount = recommendCount;
    }

    public Integer getValidOrderCount() {
        return validOrderCount;
    }

    public void setValidOrderCount(Integer validOrderCount) {
        this.validOrderCount = validOrderCount;
    }

    public Integer getFrozenScore() {
        return frozenScore;
    }

    public void setFrozenScore(Integer frozenScore) {
        this.frozenScore = frozenScore;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getSpentMoney() {
        return spentMoney;
    }

    public void setSpentMoney(BigDecimal spentMoney) {
        this.spentMoney = spentMoney;
    }

    public Integer getCheckInCount() {
        return checkInCount;
    }

    public void setCheckInCount(Integer checkInCount) {
        this.checkInCount = checkInCount;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    public CustomerLevelEnum getCustomerLevel() {
        return customerLevel;
    }

    public void setCustomerLevel(CustomerLevelEnum customerLevel) {
        this.customerLevel = customerLevel;
    }

    public Date getLastCheckInTime() {
        return lastCheckInTime;
    }

    public void setLastCheckInTime(Date lastCheckInTime) {
        this.lastCheckInTime = lastCheckInTime;
    }

    public BigDecimal getRewardRate() {
        return rewardRate;
    }

    public void setRewardRate(BigDecimal rewardRate) {
        this.rewardRate = rewardRate;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    public RewardModeEnum getRewardMode() {
        return rewardMode;
    }

    public void setRewardMode(RewardModeEnum rewardMode) {
        this.rewardMode = rewardMode;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getCheckInAccount() {
        return checkInAccount;
    }

    public void setCheckInAccount(BigDecimal checkInAccount) {
        this.checkInAccount = checkInAccount;
    }

    @Column(length = 128)
    @JsonProperty
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @OneToOne
    @JoinColumn(name = "Intermediary_SID", nullable = true)
    public CustomerProfile getIntermediaryCustomerProfile() {
        return intermediaryCustomerProfile;
    }

    public void setIntermediaryCustomerProfile(CustomerProfile intermediaryCustomerProfile) {
        this.intermediaryCustomerProfile = intermediaryCustomerProfile;
    }

    /**
     * @OneToOne
     * @JoinColumn(name = "SERVICE_REPRESENTATIVE_SID", nullable = true)
     * @NotAudited public User getServiceRepresentative() { return serviceRepresentative; }
     * <p>
     * public void setServiceRepresentative(User serviceRepresentative) { this.serviceRepresentative = serviceRepresentative; }
     */

    public BigDecimal getCustomerValue() {
        return customerValue;
    }

    public void setCustomerValue(BigDecimal customerValue) {
        this.customerValue = customerValue;
    }

    public Integer getReadyCommentCount() {
        return readyCommentCount;
    }

    public void setReadyCommentCount(Integer readyCommentCount) {
        this.readyCommentCount = readyCommentCount;
    }

    @JsonProperty
    public String getCustomerFrom() {
        return customerFrom;
    }

    public void setCustomerFrom(String customerFrom) {
        this.customerFrom = customerFrom;
    }

    @Column(length = 32)
    public String getIpWhenRegister() {
        return ipWhenRegister;
    }

    public void setIpWhenRegister(String ipWhenRegister) {
        this.ipWhenRegister = ipWhenRegister;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    public CustomerBuyTypeEnum getCustomerBuyType() {
        return customerBuyType;
    }

    public void setCustomerBuyType(CustomerBuyTypeEnum customerBuyType) {
        this.customerBuyType = customerBuyType;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    public CustomCertificateTypeEnum getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(CustomCertificateTypeEnum certificateType) {
        this.certificateType = certificateType;
    }

    @Column(name = "can_x_score")
    public Integer getCanXScore() {
        return canXScore;
    }

    public void setCanXScore(Integer canXScore) {
        this.canXScore = canXScore;
    }

    public BigDecimal getSavedMoney() {
        return savedMoney;
    }

    public void setSavedMoney(BigDecimal savedMoney) {
        this.savedMoney = savedMoney;
    }

    public Integer getCustomerType() {
        return customerType;
    }

    public void setCustomerType(Integer customerType) {
        this.customerType = customerType;
    }

    public Integer getFriendCount() {
        return friendCount;
    }

    public void setFriendCount(Integer friendCount) {
        this.friendCount = friendCount;
    }

    public Integer getFansCount() {
        return fansCount;
    }

    public void setFansCount(Integer fansCount) {
        this.fansCount = fansCount;
    }

    public Integer getScope() {
        return scope;
    }

    public void setScope(Integer scope) {
        this.scope = scope;
    }

    @Column(length = 8)
    public String getInfoStatus() {
        return infoStatus;
    }

    public void setInfoStatus(String infoStatus) {
        this.infoStatus = infoStatus;
    }

    @OneToMany(mappedBy = "customerProfile")
    @NotAudited
    @JsonIgnore
    public List<CbLotteryWin> getCbLotteryWins() {
        return cbLotteryWins;
    }

    public void setCbLotteryWins(List<CbLotteryWin> cbLotteryWins) {
        this.cbLotteryWins = cbLotteryWins;
    }

    @JsonProperty
    @Transient
    public String getFriendlyName() {
        if (StringUtils.isNotBlank(nickName)) {
            return nickName;
        }
        if (StringUtils.isNotBlank(trueName)) {
            return trueName;
        }
        return "客户";
    }

    @ManyToOne
    @JoinColumn(name = "Bind_Partner_SID", nullable = true)
    public Partner getBindPartner() {
        return bindPartner;
    }

    public void setBindPartner(Partner bindPartner) {
        this.bindPartner = bindPartner;
    }

    @ManyToOne
    @JoinColumn(name = "Bind_User_SID", nullable = true)
    public User getBindUser() {
        return bindUser;
    }

    public void setBindUser(User bindUser) {
        this.bindUser = bindUser;
    }

    public BigDecimal getWeilaifuMaxAccount() {
        return weilaifuMaxAccount;
    }

    public void setWeilaifuMaxAccount(BigDecimal weilaifuMaxAccount) {
        this.weilaifuMaxAccount = weilaifuMaxAccount;
    }

    public BigDecimal getWeilaifuAccount() {
        return weilaifuAccount;
    }

    public void setWeilaifuAccount(BigDecimal weilaifuAccount) {
        this.weilaifuAccount = weilaifuAccount;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getPartTimeSalary() {
        return partTimeSalary;
    }

    public void setPartTimeSalary(BigDecimal partTimeSalary) {
        this.partTimeSalary = partTimeSalary;
    }

    @Transient
    public String getBizTradeUnitId() {
        return "CUSTOMER_" + id;
    }

    @Transient
    @JsonProperty
    public SsoUser getSsoUser() {
        return ssoUser;
    }

    public void setSsoUser(SsoUser ssoUser) {
        this.ssoUser = ssoUser;
    }

    @OneToMany(mappedBy = "customerProfile")
    @NotAudited
    public List<CommodityViewStat> getCommodityViewStats() {
        return commodityViewStats;
    }

    public void setCommodityViewStats(List<CommodityViewStat> commodityViewStats) {
        this.commodityViewStats = commodityViewStats;
    }

    @Column(name = "is_to_b")
    public Boolean getToBusiness() {
        return toBusiness;
    }

    public void setToBusiness(Boolean toBusiness) {
        this.toBusiness = toBusiness;
    }

    @OneToMany(mappedBy = "customerProfile", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    public List<VipScoreHistory> getVipScoreHistories() {
        return vipScoreHistories;
    }

    public void setVipScoreHistories(List<VipScoreHistory> vipScoreHistories) {
        this.vipScoreHistories = vipScoreHistories;
    }

    @PrimaryKeyJoinColumn
    @OneToOne(orphanRemoval = true, optional = false, mappedBy = "customerProfile")
    public VipCustomerProfile getVipCustomerProfile() {
        return vipCustomerProfile;
    }

    public void setVipCustomerProfile(VipCustomerProfile vipCustomerProfile) {
        this.vipCustomerProfile = vipCustomerProfile;
    }

    @JsonProperty
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @JsonProperty
    public String getShopNameAudit() {
        return shopNameAudit;
    }

    public void setShopNameAudit(String shopNameAudit) {
        this.shopNameAudit = shopNameAudit;
    }

    @JsonProperty
    public String getShopCodePic() {
        return shopCodePic;
    }

    public void setShopCodePic(String shopCodePic) {
        this.shopCodePic = shopCodePic;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    @JsonProperty
    public ShopNameAuditStatusEnum getShopNameAuditStatus() {
        return shopNameAuditStatus;
    }

    public void setShopNameAuditStatus(ShopNameAuditStatusEnum shopNameAuditStatus) {
        this.shopNameAuditStatus = shopNameAuditStatus;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "c2c_shop_info_sid")
    @JsonProperty
    public C2cShopInfo getC2cShopInfo() {
        return c2cShopInfo;
    }

    public void setC2cShopInfo(C2cShopInfo c2cShopInfo) {
        this.c2cShopInfo = c2cShopInfo;
    }

    @Column(name = "weixin_id")
    @JsonProperty
    public String getWeixinId() {
        return weixinId;
    }

    public void setWeixinId(String weixinId) {
        this.weixinId = weixinId;
    }

    @Column(name = "is_toch")
    @JsonProperty
    public IsTochEnum getIsToch() {
        return isToch;
    }

    public void setIsToch(IsTochEnum isToch) {
        this.isToch = isToch;
    }

    @Column(name = "shop_description")
    @JsonProperty
    public String getShopDescription() {
        return shopDescription;
    }

    public void setShopDescription(String shopDescription) {
        this.shopDescription = shopDescription;
    }

    @Column(name = "api_wait_time")
    @JsonProperty
    public String getApiWaitTime() {
        return ApiWaitTime;
    }

    public void setApiWaitTime(String apiWaitTime) {
        ApiWaitTime = apiWaitTime;
    }

    @Column(name = "meivd_last_log_on_time")
    @JsonProperty
    public Date getMeivdLastLogOnTime() {
        return meivdLastLogOnTime;
    }

    public void setMeivdLastLogOnTime(Date meivdLastLogOnTime) {
        this.meivdLastLogOnTime = meivdLastLogOnTime;
    }

    @Transient
    @JsonProperty
    public Boolean getIsMeivd() {
        if (this.meivdLastLogOnTime == null) {
            return true;
        } else {
            return false;
        }
    }

    @OneToMany(mappedBy = "objectSid", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("orderIndex desc")
    public List<ObjectR2Pic> getShopPagePics() {
        return shopPagePics;
    }

    public void setShopPagePics(List<ObjectR2Pic> shopPagePics) {
        this.shopPagePics = shopPagePics;
    }


    @JsonProperty
    @Column(name = "openid")
    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    @Column(name = "registration_time")
    @JsonProperty
    public Date getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(Date registrationTime) {
        this.registrationTime = registrationTime;
    }

    @JsonProperty
    public BigDecimal getCoins() {
        return coins;
    }

    public void setCoins(BigDecimal coins) {
        this.coins = coins;
    }

    @JsonProperty
    public BigDecimal getFreezeCoins() {
        return freezeCoins;
    }

    public void setFreezeCoins(BigDecimal freezeCoins) {
        this.freezeCoins = freezeCoins;
    }

    @JsonProperty
    public BigDecimal getFreezenNewScore() {
        return freezenNewScore;
    }

    public void setFreezenNewScore(BigDecimal freezenNewScore) {
        this.freezenNewScore = freezenNewScore;
    }

    @JsonProperty
    public BigDecimal getCumuCoins() {
        return cumuCoins;
    }

    public void setCumuCoins(BigDecimal cumuCoins) {
        this.cumuCoins = cumuCoins;
    }

    @JsonProperty
    public Date getLastRechargeTime() {
        return lastRechargeTime;
    }

    public void setLastRechargeTime(Date lastRechargeTime) {
        this.lastRechargeTime = lastRechargeTime;
    }

    @JsonProperty
    public BigDecimal getBaseNewScore() {
        if (baseNewScore == null) {
            baseNewScore = BigDecimal.ZERO;
        }
        return baseNewScore;
    }

    public void setBaseNewScore(BigDecimal baseNewScore) {
        this.baseNewScore = baseNewScore;
    }

    @JsonProperty
    public BigDecimal getGivenNewScore() {
        if (givenNewScore == null) {
            givenNewScore = BigDecimal.ZERO;
        }
        return givenNewScore;
    }

    public void setGivenNewScore(BigDecimal givenNewScore) {
        this.givenNewScore = givenNewScore;
    }

    @JsonProperty
    public BigDecimal getConsumedNewScore() {
        if (consumedNewScore == null) {
            consumedNewScore = BigDecimal.ZERO;
        }
        return consumedNewScore;
    }

    public void setConsumedNewScore(BigDecimal consumedNewScore) {
        this.consumedNewScore = consumedNewScore;
    }

    @JsonProperty
    public Boolean getGiven() {
        return isGiven;
    }

    public void setGiven(Boolean given) {
        isGiven = given;
    }

    public enum IsTochEnum {
        @MetaData("非触屏")
        N,

        @MetaData("触屏")
        Y;
    }

    public enum ShopNameAuditStatusEnum {
        @MetaData(value = "用户已提交")
        CUSTOMER_SUBMIT,

        @MetaData(value = "审核中")
        REVIEWING,

        @MetaData(value = "失败")
        FAILED,

        @MetaData(value = "成功")
        SUCCESS;
    }

    public enum CustomCertificateTypeEnum {
        @MetaData(value = "实名")
        S10SM,

        @MetaData(value = "达人")
        S30DR,

        @MetaData(value = "专家")
        S50ZJ;
    }

    public enum CustomerLevelEnum {
        @MetaData("普通用户")
        NORMAL,

        @MetaData("VIP用户")
        VIP,

        @MetaData("尊享VIP用户")
        SVIP;
    }

    public enum CustomerBuyTypeEnum {

        @MetaData(value = "个人")
        PERSON,

        @MetaData(value = "分销商")
        DISTRIBUTOR;
    }

    public enum RewardModeEnum {

        @MetaData(value = "始终以有效订单数计算为准")
        VOC,

        @MetaData(value = "始终以设定值为准")
        FIX,

        @MetaData(value = "以低值为准")
        MIN,

        @MetaData(value = "以高值为准")
        MAX;
    }

}
