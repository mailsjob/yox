package com.meiyuetao.myt.customer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("待激活用户，直邮用户待导入MYT")
@Entity
@Table(name = "myt_inactive_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class InactiveUser extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("直邮用户ＩＤ")
    private Long archiveId;
    @MetaData("姓名")
    private String name;
    @MetaData("性别")
    private Boolean sex;
    @MetaData("生日")
    private Date birthday;
    @MetaData("电话")
    private String mobile;
    @MetaData("邮件")
    private String email;
    /*
     * @MetaData("地区") private String region;
     * 
     * @MetaData("地址") private String address;
     */
    @MetaData("教育经历")
    private String educationsBlock;
    @MetaData("工作经历")
    private String experiencesBlock;
    @MetaData("已导入MYT")
    private ImportStateEnum importState = ImportStateEnum.WAIT;

    public enum ImportStateEnum {

        @MetaData(value = "已导入")
        IMPORTED,

        @MetaData(value = "等待导入")
        WAIT,

        @MetaData(value = "不能导入")
        CANNOT;
    }

    public Long getArchiveId() {
        return archiveId;
    }

    public void setArchiveId(Long archiveId) {
        this.archiveId = archiveId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /*
     * public String getRegion() { return region; } public void setRegion(String
     * region) { this.region = region; } public String getAddress() { return
     * address; } public void setAddress(String address) { this.address =
     * address; }
     */

    @Lob
    public String getEducationsBlock() {
        return educationsBlock;
    }

    public void setEducationsBlock(String educationsBlock) {
        this.educationsBlock = educationsBlock;
    }

    @Lob
    public String getExperiencesBlock() {
        return experiencesBlock;
    }

    public void setExperiencesBlock(String experiencesBlock) {
        this.experiencesBlock = experiencesBlock;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public ImportStateEnum getImportState() {
        return importState;
    }

    public void setImportState(ImportStateEnum importState) {
        this.importState = importState;
    }
}
