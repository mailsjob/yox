package com.meiyuetao.myt.customer.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail;

@MetaData("客户收支信息")
@Entity
@Table(name = "iyb_customer_account_history")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CustomerAccountHist extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("交易金额")
    private BigDecimal amount;
    @MetaData("备注")
    private String sysMemo;
    @MetaData("交易时间")
    private Date occurTime;
    @MetaData("交易类型（充值：CHG，提现：WTHDR，提成：DED）")
    private CustomerAccountHistOccurTypeEnum occurType;
    @MetaData("账户类型（ CURRENT-可用金；EARNEST-定金；FROZEN-冻结佣金；CHECKIN-签到金）")
    private CustomerAccountHistAccountTypeEnum accountType;
    @MetaData(" 财务(支付宝)凭证号")
    private String inOutAccountNo;
    @MetaData("身份证号")
    private String idCardNo;
    @MetaData("电话号")
    private String mobileNo;
    @MetaData("关联客户")
    private CustomerProfile customerProfile;
    @MetaData("关联订单")
    private BoxOrder boxOrder;
    @MetaData("关联订单付款行项")
    private BoxOrderDetail boxOrderDetail;

    public enum CustomerAccountHistOccurTypeEnum {
        @MetaData(value = "充值")
        CHG, @MetaData(value = "提现")
        WTHDR, @MetaData(value = "提成")
        DED, @MetaData(value = "积分抵扣款")
        SCOREDED, @MetaData(value = "签到延期一天扣款")
        OVERDED1, @MetaData(value = "签到延期两天扣款")
        OVERDED2, @MetaData(value = "签到延期三天扣款")
        OVERDED3, @MetaData(value = "帐户内转帐")
        INRTRANS, @MetaData(value = "签到金")
        CHECKIN, @MetaData(value = "消费")
        CONS, @MetaData(value = "消费退款")
        REIMB, @MetaData(value = "积分兑换")
        EC;

    }

    public enum CustomerAccountHistAccountTypeEnum {
        @MetaData(value = "可用金")
        CURRENT, @MetaData(value = "定金")
        EARNEST, @MetaData(value = "冻结佣金")
        FROZEN, @MetaData(value = "签到金")
        CHECKIN, @MetaData(value = "消费帐户")
        CONSUME;
    }

    @OneToOne
    @JoinColumn(name = "Customer_Profile_Sid", nullable = false)
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(length = 512)
    @JsonProperty
    public String getSysMemo() {
        return sysMemo;
    }

    public void setSysMemo(String sysMemo) {
        this.sysMemo = sysMemo;
    }

    @JsonProperty
    public Date getOccurTime() {
        return occurTime;
    }

    public void setOccurTime(Date occurTime) {
        this.occurTime = occurTime;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public CustomerAccountHistOccurTypeEnum getOccurType() {
        return occurType;
    }

    public void setOccurType(CustomerAccountHistOccurTypeEnum occurType) {
        this.occurType = occurType;
    }

    @Column(length = 128)
    @JsonProperty
    public String getInOutAccountNo() {
        return inOutAccountNo;
    }

    public void setInOutAccountNo(String inOutAccountNo) {
        this.inOutAccountNo = inOutAccountNo;
    }

    @Column(length = 32)
    @JsonProperty
    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    @Column(length = 32)
    @JsonProperty
    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @ManyToOne
    @JoinColumn(name = "ORDER_SID", nullable = true)
    @JsonProperty
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

    @ManyToOne
    @JoinColumn(name = "ORDER_DETAIL_SID", nullable = true)
    @JsonProperty
    public BoxOrderDetail getBoxOrderDetail() {
        return boxOrderDetail;
    }

    public void setBoxOrderDetail(BoxOrderDetail boxOrderDetail) {
        this.boxOrderDetail = boxOrderDetail;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public CustomerAccountHistAccountTypeEnum getAccountType() {
        return accountType;
    }

    public void setAccountType(CustomerAccountHistAccountTypeEnum accountType) {
        this.accountType = accountType;
    }

}
