package com.meiyuetao.myt.customer.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail;

@MetaData("积分信息")
@Entity
@Table(name = "iyb_box_score")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Score extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("积分事件描述")
    private String eventDescription;
    @MetaData("事件")
    private Long event;
    @MetaData("类型")
    private ScoreEventTypeEnum eventType;

    public enum ScoreEventTypeEnum {
        @MetaData(value = "购买商品积分")
        B, @MetaData(value = "评论积分")
        C, @MetaData(value = "其他原因管理员奖励积分")
        S, @MetaData(value = "积分兑换账户金")
        EC, @MetaData(value = "网站签到")
        CKI
    }

    @MetaData("备注")
    private String memo;

    @MetaData("积分产生时间")
    private Date recordTime;

    @MetaData("分额")
    private Integer scoreAmount;

    @MetaData("客户")
    private CustomerProfile customerProfile;

    @MetaData("积分待定")
    private Boolean scorePending;

    @MetaData("订单")
    private BoxOrder boxOrder;

    @MetaData("订单行项")
    private BoxOrderDetail boxOrderDetail;

    @MetaData("有效时间")
    private Date effectiveTime;

    @MetaData("系统备注")
    private String sysMemo;

    @Column(length = 256)
    @JsonProperty
    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    @Column(name = "event_sid")
    @JsonProperty
    public Long getEvent() {
        return event;
    }

    public void setEvent(Long event) {
        this.event = event;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public ScoreEventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(ScoreEventTypeEnum eventType) {
        this.eventType = eventType;
    }

    @Column(length = 256)
    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty
    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "customer_profile_sid", nullable = true, updatable = false)
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    @Column(length = 32, updatable = false)
    @JsonProperty
    public Integer getScoreAmount() {
        return scoreAmount;
    }

    public void setScoreAmount(Integer scoreAmount) {
        this.scoreAmount = scoreAmount;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @JsonProperty
    public Boolean getScorePending() {
        return scorePending;
    }

    public void setScorePending(Boolean scorePending) {
        this.scorePending = scorePending;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "order_sid")
    @JsonProperty
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "order_detail_sid")
    @JsonProperty
    public BoxOrderDetail getBoxOrderDetail() {
        return boxOrderDetail;
    }

    public void setBoxOrderDetail(BoxOrderDetail boxOrderDetail) {
        this.boxOrderDetail = boxOrderDetail;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty
    public Date getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(Date effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    @Column(length = 256)
    @JsonProperty
    public String getSysMemo() {
        return sysMemo;
    }

    public void setSysMemo(String sysMemo) {
        this.sysMemo = sysMemo;
    }

}