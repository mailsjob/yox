package com.meiyuetao.myt.customer.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("微信消息")
@Entity
@Table(name = "iyb_weixin_msg")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class WeixinMsg extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("微信消息内容")
    private String weixinMsg;
    @MetaData("微信消息发送时间")
    private Date msgCreatedDt;
    @MetaData("微信用户名")
    private String fromWeixinName;
    @MetaData("消息id")
    private Long msgId;
    @MetaData("消息内容类型")
    private MsgTypeEnum msgType;
    private List<WeixinMsgReply> weixinMsgReplies = new ArrayList<WeixinMsgReply>();
    @MetaData("消息操作类型")
    private ActionTypeEnum actionType;

    public enum MsgTypeEnum {

        @MetaData("文本")
        Text,

        @MetaData("图片")
        Image,

        @MetaData("语音")
        Voice,

        @MetaData("视频")
        Video,

        @MetaData("位置")
        Location,

        @MetaData("链接")
        Link;

    }

    public enum ActionTypeEnum {

        @MetaData("优惠券")
        PAPERS,

        @MetaData("签到")
        SIGN,

        @MetaData("绑定")
        BIND,

        @MetaData("其他")
        UNKNOWN;

    }

    @Column(length = 512)
    @JsonProperty
    public String getWeixinMsg() {
        return weixinMsg;
    }

    public void setWeixinMsg(String weixinMsg) {
        this.weixinMsg = weixinMsg;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getMsgCreatedDt() {
        return msgCreatedDt;
    }

    public void setMsgCreatedDt(Date msgCreatedDt) {
        this.msgCreatedDt = msgCreatedDt;
    }

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getFromWeixinName() {
        return fromWeixinName;
    }

    public void setFromWeixinName(String fromWeixinName) {
        this.fromWeixinName = fromWeixinName;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    @JsonProperty
    public MsgTypeEnum getMsgType() {
        return msgType;
    }

    public void setMsgType(MsgTypeEnum msgType) {
        this.msgType = msgType;
    }

    @OneToMany(mappedBy = "weixinMsg", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    public List<WeixinMsgReply> getWeixinMsgReplies() {
        return weixinMsgReplies;
    }

    public void setWeixinMsgReplies(List<WeixinMsgReply> weixinMsgReplies) {
        this.weixinMsgReplies = weixinMsgReplies;
    }

    @Column(nullable = false)
    @JsonProperty
    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, name = "weixin_action_type", nullable = false)
    @JsonProperty
    public ActionTypeEnum getActionType() {
        return actionType;
    }

    public void setActionType(ActionTypeEnum actionType) {
        this.actionType = actionType;
    }

}
