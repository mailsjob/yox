package com.meiyuetao.myt.customer.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;

@MetaData("推送商品配置")
@Entity
@Table(name = "myt_push_commodity_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PushCommodityInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("商品链接")
    private String commodityUrl;

    /*
     * @MetaData("手机商品链接") private String mobileCommodityUrl;
     */

    @MetaData("商品")
    private Commodity commodity;

    /*
     * @MetaData("是否短信推送商品") private Boolean mobilePush;
     * 
     * @MetaData("是否启用") private Boolean enable;
     */

    @JsonProperty
    public String getCommodityUrl() {
        return commodityUrl;
    }

    public void setCommodityUrl(String commodityUrl) {
        this.commodityUrl = commodityUrl;
    }

    @OneToOne(cascade = CascadeType.DETACH, optional = true)
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

}
