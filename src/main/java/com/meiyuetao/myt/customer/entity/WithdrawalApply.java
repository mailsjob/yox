package com.meiyuetao.myt.customer.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提现申请")
@Entity
@Table(name = "iyb_withdrawal_apply")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WithdrawalApply extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联客户")
    private CustomerProfile customerProfile;
    @MetaData("申请时间")
    private Date applyTime;
    @MetaData("申请状态")
    private WithdrawalApplyStautsEnum applyStatus;

    public enum WithdrawalApplyStautsEnum {

        @MetaData("申请中")
        S10APPLY,

        @MetaData("申请取消")
        S20CANCEL,

        @MetaData("进行中")
        S30EXEC,

        @MetaData("提现完成")
        S40COMP;

    }

    @MetaData("提现金额")
    private BigDecimal applyAmount;

    @MetaData("财务(支付宝)凭证号 ")
    private String inOutAccountNo;
    @MetaData("手机号")
    private String applierMobile;
    @MetaData("提现支付宝账号/提现美月淘ID")
    private String applierAlipayAccount;
    @MetaData("身份证号")
    private String applierIdcardNo;
    @MetaData("客户收支记录")
    private CustomerAccountHist customerAccountHist;
    @MetaData("备注")
    private String memo;

    @OneToOne
    @JoinColumn(name = "applier_profile_sid", nullable = false)
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public WithdrawalApplyStautsEnum getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(WithdrawalApplyStautsEnum applyStatus) {
        this.applyStatus = applyStatus;
    }

    @JsonProperty
    public BigDecimal getApplyAmount() {
        return applyAmount;
    }

    public void setApplyAmount(BigDecimal applyAmount) {
        this.applyAmount = applyAmount;
    }

    @JsonProperty
    public String getInOutAccountNo() {
        return inOutAccountNo;
    }

    public void setInOutAccountNo(String inOutAccountNo) {
        this.inOutAccountNo = inOutAccountNo;
    }

    @JsonProperty
    public String getApplierMobile() {
        return applierMobile;
    }

    public void setApplierMobile(String applierMobile) {
        this.applierMobile = applierMobile;
    }

    @JsonProperty
    public String getApplierAlipayAccount() {
        return applierAlipayAccount;
    }

    public void setApplierAlipayAccount(String applierAlipayAccount) {
        this.applierAlipayAccount = applierAlipayAccount;
    }

    @JsonProperty
    public String getApplierIdcardNo() {
        return applierIdcardNo;
    }

    public void setApplierIdcardNo(String applierIdcardNo) {
        this.applierIdcardNo = applierIdcardNo;
    }

    @OneToOne
    @JoinColumn(name = "account_history_sid")
    @JsonProperty
    @NotAudited
    public CustomerAccountHist getCustomerAccountHist() {
        return customerAccountHist;
    }

    public void setCustomerAccountHist(CustomerAccountHist customerAccountHist) {
        this.customerAccountHist = customerAccountHist;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}
