package com.meiyuetao.myt.customer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("短信发送记录")
@Entity
@Table(name = "iyb_sms_log")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class SmsLog extends MytBaseEntity {

    private static final long serialVersionUID = 1L;

    @MetaData(value = "短信内容")
    private String sendContent;

    @MetaData(value = "请求发送号码列表")
    private String sendToList;

    @MetaData(value = "失败发送号码列表")
    private String failureToList;

    @MetaData(value = "发送时间")
    private Date sendTime;

    @MetaData(value = "发送人登录账号")
    private String sendUserUid;

    @MetaData(value = "短信通道")
    private SmsChannelEnum smsChannel = SmsChannelEnum.YTX;

    @MetaData(value = "短信模板")
    private String smsModel;

    public enum SmsChannelEnum {
        @MetaData(value = "云片")
        YP,

        @MetaData(value = "云通信")
        YTX,

        @MetaData(value = "三通")
        STONG,

        @MetaData(value = "QYQQ")
        QYQQ;

    }

    @Column(length = 512, nullable = false)
    @JsonProperty
    public String getSendContent() {
        return sendContent;
    }

    public void setSendContent(String sendContent) {
        this.sendContent = sendContent;
    }

    @Column(nullable = false)
    @Lob
    public String getSendToList() {
        return sendToList;
    }

    public void setSendToList(String sendToList) {
        this.sendToList = sendToList;
    }

    @Column(nullable = true)
    @Lob
    public String getFailureToList() {
        return failureToList;
    }

    public void setFailureToList(String failureToList) {
        this.failureToList = failureToList;
    }

    @JsonProperty
    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    @JsonProperty
    public String getSendUserUid() {
        return sendUserUid;
    }

    public void setSendUserUid(String sendUserUid) {
        this.sendUserUid = sendUserUid;
    }

    @Transient
    @JsonProperty
    public boolean isHasFailure() {
        return StringUtils.isNotBlank(failureToList);
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8, nullable = false)
    @JsonProperty
    public SmsChannelEnum getSmsChannel() {
        return smsChannel;
    }

    public void setSmsChannel(SmsChannelEnum smsChannel) {
        this.smsChannel = smsChannel;
    }

    @Column(name = "sms_model")
    @JsonProperty
    public String getSmsModel() {
        return smsModel;
    }

    public void setSmsModel(String smsModel) {
        this.smsModel = smsModel;
    }

}
