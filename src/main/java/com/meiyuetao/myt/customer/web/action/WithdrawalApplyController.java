package com.meiyuetao.myt.customer.web.action;

import java.util.Date;
import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.service.Validation;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.customer.entity.CustomerAccountHist;
import com.meiyuetao.myt.customer.entity.CustomerAccountHist.CustomerAccountHistAccountTypeEnum;
import com.meiyuetao.myt.customer.entity.CustomerAccountHist.CustomerAccountHistOccurTypeEnum;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.customer.entity.WithdrawalApply;
import com.meiyuetao.myt.customer.entity.WithdrawalApply.WithdrawalApplyStautsEnum;
import com.meiyuetao.myt.customer.service.CustomerAccountHistService;
import com.meiyuetao.myt.customer.service.CustomerProfileService;
import com.meiyuetao.myt.customer.service.WithdrawalApplyService;

@MetaData("WithdrawalApplyController")
public class WithdrawalApplyController extends MytBaseController<WithdrawalApply, Long> {

    @Autowired
    private WithdrawalApplyService withdrawalApplyService;

    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private CustomerAccountHistService customerAccountHistService;

    @Override
    protected BaseService<WithdrawalApply, Long> getEntityService() {
        return withdrawalApplyService;
    }

    @Override
    protected void checkEntityAclPermission(WithdrawalApply entity) {
        // Nothing to do
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("申请提现")
    public HttpHeaders apply() {
        if (StringUtils.isNotBlank(getParameter("ids"))) {
            Long[] ids = this.getSelectSids();
            Validation.isTrue(ids != null && ids.length > 0, "至少选择一条记录");
            List<WithdrawalApply> withdrawalApplies = withdrawalApplyService.findAll(ids);
            int succount = 0;
            for (WithdrawalApply withdrawalApplie : withdrawalApplies) {
                if (WithdrawalApplyStautsEnum.S40COMP.equals(withdrawalApplie.getApplyStatus())) {
                    continue;
                }
                // 提现状态变更
                withdrawalApplie.setApplyStatus(WithdrawalApplyStautsEnum.S40COMP);
                withdrawalApplyService.save(withdrawalApplies);
                // 客户账户金额减少
                CustomerProfile customerProfile = withdrawalApplie.getCustomerProfile();
                customerProfile.setCurrentAccount(customerProfile.getCurrentAccount().subtract(withdrawalApplie.getApplyAmount()));
                customerProfileService.save(customerProfile);
                // 追加收支记录
                CustomerAccountHist customerAccountHist = new CustomerAccountHist();
                customerAccountHist.setAccountType(CustomerAccountHistAccountTypeEnum.CURRENT);
                customerAccountHist.setOccurType(CustomerAccountHistOccurTypeEnum.WTHDR);
                customerAccountHist.setAmount(withdrawalApplie.getApplyAmount());
                customerAccountHist.setCustomerProfile(customerProfile);
                customerAccountHist.setOccurTime(new Date());
                customerAccountHist.setSysMemo("提现获得" + withdrawalApplie.getApplyAmount() + "元签到金");
                customerAccountHist.setInOutAccountNo(withdrawalApplie.getInOutAccountNo());
                customerAccountHist.setIdCardNo(withdrawalApplie.getApplierIdcardNo());
                customerAccountHist.setMobileNo(withdrawalApplie.getApplierMobile());
                customerAccountHistService.save(customerAccountHist);
                succount++;
            }
            if (succount == withdrawalApplies.size()) {
                setModel(OperationResult.buildSuccessResult("提现" + succount + "条数据"));
            } else if (succount == 0) {
                setModel(OperationResult.buildFailureResult("已经提现的部分不能再申请提现！"));
            } else {
                int suc = withdrawalApplies.size() - succount;
                setModel(OperationResult.buildFailureResult("已经提现的部分不能再申请提现！成功提现" + suc + "条数据"));
            }

        }

        return buildDefaultHttpHeaders();
    }
}