package com.meiyuetao.myt.customer.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.customer.entity.Score;
import com.meiyuetao.myt.customer.service.ScoreService;

@MetaData("积分信息管理")
public class ScoreController extends BaseController<Score, Long> {

    @Autowired
    private ScoreService scoreService;

    @Override
    protected BaseService<Score, Long> getEntityService() {
        return scoreService;
    }

    @Override
    protected void checkEntityAclPermission(Score entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}