package com.meiyuetao.myt.customer.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.customer.entity.NameCertificate;
import com.meiyuetao.myt.customer.service.NameCertificateService;

@MetaData("NameCertificateController")
public class NameCertificateController extends MytBaseController<NameCertificate, Long> {

    @Autowired
    private NameCertificateService nameCertificateService;

    @Override
    protected BaseService<NameCertificate, Long> getEntityService() {
        return nameCertificateService;
    }

    @Override
    protected void checkEntityAclPermission(NameCertificate entity) {
        // Nothing to do
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        nameCertificateService.save(bindingEntity);
        setModel(OperationResult.buildSuccessResult("数据保存成功", bindingEntity));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}