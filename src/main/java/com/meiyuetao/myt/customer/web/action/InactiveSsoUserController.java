package com.meiyuetao.myt.customer.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.PersistableController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.customer.entity.SsoUser;
import com.meiyuetao.myt.customer.service.SsoUserService;

@MetaData("SsoUserController")
public class InactiveSsoUserController extends PersistableController<SsoUser, Long> {

    @Autowired
    private SsoUserService ssoUserService;

    @Override
    protected BaseService<SsoUser, Long> getEntityService() {
        return ssoUserService;
    }

    @Override
    protected void checkEntityAclPermission(SsoUser entity) {
        // Nothing
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}