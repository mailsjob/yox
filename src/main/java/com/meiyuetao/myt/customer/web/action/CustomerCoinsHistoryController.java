package com.meiyuetao.myt.customer.web.action;

import lab.s2jh.core.annotation.MetaData;
import com.meiyuetao.myt.customer.entity.CustomerCoinsHistory;
import com.meiyuetao.myt.customer.service.CustomerCoinsHistoryService;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

@MetaData("客户金币变更明细管理")
public class CustomerCoinsHistoryController extends BaseController<CustomerCoinsHistory,Long> {

    @Autowired
    private CustomerCoinsHistoryService customerCoinsHistoryService;

    @Override
    protected BaseService<CustomerCoinsHistory, Long> getEntityService() {
        return customerCoinsHistoryService;
    }
    
    @Override
    protected void checkEntityAclPermission(CustomerCoinsHistory entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        //TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }
    
    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }
    
    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}