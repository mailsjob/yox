package com.meiyuetao.myt.customer.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.customer.entity.CustomerAccountHist;
import com.meiyuetao.myt.customer.service.CustomerAccountHistService;

@MetaData("CustomerAccountHistController")
public class CustomerAccountHistController extends MytBaseController<CustomerAccountHist, Long> {

    @Autowired
    private CustomerAccountHistService customerAccountHistService;

    @Override
    protected BaseService<CustomerAccountHist, Long> getEntityService() {
        return customerAccountHistService;
    }

    @Override
    protected void checkEntityAclPermission(CustomerAccountHist entity) {
        // Nothing to do
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}