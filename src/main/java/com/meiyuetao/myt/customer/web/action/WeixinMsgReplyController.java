package com.meiyuetao.myt.customer.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.customer.entity.WeixinMsgReply;
import com.meiyuetao.myt.customer.service.WeixinMsgReplyService;

@MetaData("WeixinMsgReplyController")
public class WeixinMsgReplyController extends MytBaseController<WeixinMsgReply, Long> {

    @Autowired
    private WeixinMsgReplyService weixinMsgReplyService;

    @Override
    protected BaseService<WeixinMsgReply, Long> getEntityService() {
        return weixinMsgReplyService;
    }

    @Override
    protected void checkEntityAclPermission(WeixinMsgReply entity) {
        // Nothing
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}