package com.meiyuetao.myt.customer.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.customer.entity.CustomerProfile.ShopNameAuditStatusEnum;
import com.meiyuetao.myt.customer.service.CustomerProfileService;

@MetaData("ShopNameAuditController")
public class ShopNameAuditController extends MytBaseController<CustomerProfile, Long> {

    @Autowired
    private CustomerProfileService customerProfileService;

    @Override
    protected BaseService<CustomerProfile, Long> getEntityService() {
        // TODO Auto-generated method stub
        return customerProfileService;
    }

    @Override
    protected void checkEntityAclPermission(CustomerProfile entity) {
        // TODO Auto-generated method stub

    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("保存店铺审核名称到店铺名称")
    public HttpHeaders saveShopName() {
        if (ShopNameAuditStatusEnum.SUCCESS.equals(bindingEntity.getShopNameAuditStatus())) {
            bindingEntity.setShopName(bindingEntity.getShopNameAudit());
        }
        return super.doSave();
    }
}
