package com.meiyuetao.myt.customer.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.customer.entity.SmsDetailLog;
import com.meiyuetao.myt.customer.service.SmsDetailLogService;

@MetaData("SmsDetailLogController")
public class SmsDetailLogController extends MytBaseController<SmsDetailLog, Long> {

    @Autowired
    private SmsDetailLogService smsDetailLogService;

    @Override
    protected BaseService<SmsDetailLog, Long> getEntityService() {
        return smsDetailLogService;
    }

    @Override
    protected void checkEntityAclPermission(SmsDetailLog entity) {
        // Nothing
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}