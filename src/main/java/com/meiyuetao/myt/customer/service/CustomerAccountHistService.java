package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.CustomerAccountHistDao;
import com.meiyuetao.myt.customer.entity.CustomerAccountHist;

@Service
@Transactional
public class CustomerAccountHistService extends BaseService<CustomerAccountHist, Long> {

    @Autowired
    private CustomerAccountHistDao customerAccountHistDao;

    @Override
    protected BaseDao<CustomerAccountHist, Long> getEntityDao() {
        // TODO Auto-generated method stub
        return customerAccountHistDao;
    }

}
