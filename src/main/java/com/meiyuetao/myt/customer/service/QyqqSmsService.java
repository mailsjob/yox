package com.meiyuetao.myt.customer.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lab.s2jh.core.util.DateUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.core.constant.MessageStateEnum;
import com.meiyuetao.myt.customer.dao.SmsDetailLogDao;
import com.meiyuetao.myt.customer.dao.SmsLogDao;
import com.meiyuetao.myt.customer.entity.SmsDetailLog;
import com.meiyuetao.myt.customer.entity.SmsDetailLog.SmsDetailChannelEnum;
import com.meiyuetao.myt.customer.entity.SmsLog;

@Service("qyqqSmsService")
@Transactional
public class QyqqSmsService extends SmsService {
    @Value("${qyqqSms.account}")
    private String account; // 用户名
    @Value("${qyqqSms.password}")
    private String password; // 密码
    @Value("${qyqqSms.userid}")
    private String userid; // 企业id
    // public static String url="http://3tong.net/http/sms/Submit"; //无限通使用地址
    @Value("${qyqqSms.url}")
    private String url;
    private String extno = "";
    @Autowired
    private SmsDetailLogDao smsDetailLogDao;
    @Autowired
    private SmsLogDao smsLogDao;

    @Override
    public Boolean bizSend(String mobilePhone, String content, SmsLog smsLog) {
        String response = null;
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        CloseableHttpClient client = httpClientBuilder.build();
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();

        formparams.add(new BasicNameValuePair("action", "send"));
        formparams.add(new BasicNameValuePair("userid", userid));
        formparams.add(new BasicNameValuePair("account", account));
        formparams.add(new BasicNameValuePair("password", password));
        formparams.add(new BasicNameValuePair("mobile", mobilePhone));
        formparams.add(new BasicNameValuePair("content", content));
        formparams.add(new BasicNameValuePair("sendTime", DateUtils.formatTime(new Date())));
        formparams.add(new BasicNameValuePair("extno", extno));
        UrlEncodedFormEntity entity;
        // 设置Post数据
        try {
            entity = new UrlEncodedFormEntity(formparams, "UTF-8");
            httpPost.setEntity(entity);
            HttpResponse httpResponse;
            // post请求
            httpResponse = client.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                response = EntityUtils.toString(httpEntity, "UTF-8");

            }
            client.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        String returnstatus = StringUtils.substringBetween(response, "<returnstatus>", "</returnstatus>");
        SmsDetailLog smsDetailLog = new SmsDetailLog();
        smsDetailLog.setContent(content);
        smsDetailLog.setMobile(mobilePhone);
        smsDetailLog.setSendTime(new Date());
        smsDetailLog.setSmsLog(smsLog);
        smsDetailLog.setSmsChannel(SmsDetailChannelEnum.QYQQ);
        String message = StringUtils.substringBetween(response, "<message>", "</message>");
        smsLogDao.save(smsLog);
        smsDetailLog.setMemo(message);
        if (returnstatus.equals("Success")) {
            smsDetailLog.setState(MessageStateEnum.OK);
            smsDetailLog.setSmsLog(smsLog);
            smsDetailLogDao.save(smsDetailLog);
            return true;
        } else {
            smsDetailLog.setState(MessageStateEnum.FAIL);
            smsDetailLogDao.save(smsDetailLog);
            return false;
        }

    }

    public String getBalance() {
        String response = null;
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        CloseableHttpClient client = httpClientBuilder.build();
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();

        formparams.add(new BasicNameValuePair("action", "overage"));
        formparams.add(new BasicNameValuePair("userid", userid));
        formparams.add(new BasicNameValuePair("account", account));
        formparams.add(new BasicNameValuePair("password", password));
        UrlEncodedFormEntity entity;
        // 设置Post数据
        try {
            entity = new UrlEncodedFormEntity(formparams, "UTF-8");
            httpPost.setEntity(entity);
            HttpResponse httpResponse;
            // post请求
            httpResponse = client.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                response = EntityUtils.toString(httpEntity, "UTF-8");

            }
            client.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
