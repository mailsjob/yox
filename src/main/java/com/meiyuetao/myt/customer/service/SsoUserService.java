package com.meiyuetao.myt.customer.service;

import java.util.Date;
import java.util.UUID;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.ctx.FreemarkerService;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.SsoUserDao;
import com.meiyuetao.myt.customer.entity.SsoUser;
import com.meiyuetao.myt.job.BusinessNotifyService;

@Service
@Transactional
public class SsoUserService extends BaseService<SsoUser, Long> {

    @Autowired
    private SsoUserDao ssoUserDao;

    @Autowired
    private Md5PasswordEncoder passwordEncoder;

    @Autowired
    private BusinessNotifyService businessNotifyService;
    @Autowired
    private FreemarkerService freemarkerService;
    public static final String TYPE_IYOUBOX_UID = "IYouBoxUID";
    public static final String TYPE_CHINABABY = "CHINABABY";

    @Override
    protected BaseDao<SsoUser, Long> getEntityDao() {
        return ssoUserDao;
    }

    public void resetPassword(SsoUser entity) {
        String phoneNum = entity.getMobilePhone();
        SsoUser dbu = ssoUserDao.findOne(entity.getId());
        String newPassword = RandomStringUtils.randomNumeric(6);
        String encodedPwd = passwordEncoder.encodePassword(newPassword, dbu.getLoginid());
        dbu.setPassword(encodedPwd);
        dbu.setLastOperationSummary(dbu.buildLastOperationSummary("重置密码，并发信至" + phoneNum));
        ssoUserDao.save(dbu);

        // 三通服务商暂时无法使用，发送短信改用云片
        // Map<String, Object> dataMap = new HashMap<String, Object>();
        // dataMap.put("loginid", dbu.getLoginid());
        // dataMap.put("password", newPassword);
        // String content =
        // freemarkerService.processTemplateByFileName("SSO_PWD_RESET_SMS",
        // dataMap);
        // businessNotifyService.sendMsg(phoneNum, content);
        // 重置密码模板号 "833043"
        String content = "#username#=" + dbu.getLoginid() + "&#password#=" + newPassword + "&#tel#=400-898-6221";
        businessNotifyService.sendSmsByYunPian(dbu.getLoginid(), "833043", content);
    }

    public void fastRegisterByMobile(String mobilePhone) {
        String passwordText = RandomStringUtils.randomNumeric(6);
        String encodedPwd = passwordEncoder.encodePassword(passwordText, mobilePhone);
        SsoUser ssoUser = new SsoUser();
        ssoUser.setPassword(encodedPwd);
        ssoUser.setLoginid(mobilePhone);
        ssoUser.setMobilePhone(mobilePhone);
        ssoUser.setEnabled(Boolean.TRUE);
        ssoUser.setProviderType(TYPE_IYOUBOX_UID);
        ssoUser.setProviderUid(passwordEncoder.encodePassword(mobilePhone, UUID.randomUUID().toString()).toUpperCase());
        ssoUser.setRegisterTime(new Date());
        ssoUser.setLastOperationSummary(ssoUser.buildLastOperationSummary("系统快速注册，手机号" + mobilePhone));
        ssoUserDao.save(ssoUser);
        // 三通服务商暂时无法使用，发送短信改用云片
        // Map<String, Object> dataMap = new HashMap<String, Object>();
        // dataMap.put("loginid", ssoUser.getLoginid());
        // dataMap.put("password", passwordText);
        // String content =
        // freemarkerService.processTemplateByFileName("FAST_REGISTER_BY_MOBILE_SMS",
        // dataMap);
        // businessNotifyService.sendMsg(mobilePhone, content);
        String content = "#phone#=" + mobilePhone + "&#password#=" + passwordText;
        businessNotifyService.sendSmsByYunPian(ssoUser.getLoginid(), "835005", content);
    }

    public void chinaBabyRegister(String loginId) {
        String passwordText = RandomStringUtils.randomNumeric(6);
        String encodedPwd = passwordEncoder.encodePassword(passwordText, loginId);
        SsoUser ssoUser = new SsoUser();
        ssoUser.setPassword(encodedPwd);
        ssoUser.setLoginid(loginId);
        // ssoUser.setMobilePhone(loginId);
        ssoUser.setEnabled(Boolean.TRUE);
        // ssoUser.setProviderType(TYPE_IYOUBOX_UID);
        ssoUser.setProviderType(TYPE_CHINABABY);
        ssoUser.setProviderUid(loginId);
        ssoUser.setRegisterTime(new Date());
        ssoUser.setLastOperationSummary(ssoUser.buildLastOperationSummary(passwordText + "爱宝贝初始化密码"));
        ssoUserDao.save(ssoUser);

    }
}
