package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.SmsDetailLogDao;
import com.meiyuetao.myt.customer.entity.SmsDetailLog;

@Service
@Transactional
public class SmsDetailLogService extends BaseService<SmsDetailLog, Long> {

    @Autowired
    private SmsDetailLogDao smsDetailLogDao;

    @Override
    protected BaseDao<SmsDetailLog, Long> getEntityDao() {
        return smsDetailLogDao;
    }
}
