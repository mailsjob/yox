package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.SmsLogDao;
import com.meiyuetao.myt.customer.entity.SmsLog;

@Service
@Transactional
public class SmsLogService extends BaseService<SmsLog, Long> {

    @Autowired
    private SmsLogDao smsLogDao;

    @Override
    protected BaseDao<SmsLog, Long> getEntityDao() {
        return smsLogDao;
    }
}
