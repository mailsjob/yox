package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.WithdrawalApplyDao;
import com.meiyuetao.myt.customer.entity.WithdrawalApply;

@Service
@Transactional
public class WithdrawalApplyService extends BaseService<WithdrawalApply, Long> {

    @Autowired
    private WithdrawalApplyDao withdrawalApplyDao;

    @Override
    protected BaseDao<WithdrawalApply, Long> getEntityDao() {
        return withdrawalApplyDao;
    }

}
