package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.PushCommodityInfoDao;
import com.meiyuetao.myt.customer.entity.PushCommodityInfo;

@Service
@Transactional
public class PushCommodityInfoService extends BaseService<PushCommodityInfo, Long> {

    @Autowired
    private PushCommodityInfoDao pushCommodityInfoDao;

    @Override
    protected BaseDao<PushCommodityInfo, Long> getEntityDao() {
        return pushCommodityInfoDao;
    }

}
