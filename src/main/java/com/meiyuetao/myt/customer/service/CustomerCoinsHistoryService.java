package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import com.meiyuetao.myt.customer.entity.CustomerCoinsHistory;
import com.meiyuetao.myt.customer.dao.CustomerCoinsHistoryDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerCoinsHistoryService extends BaseService<CustomerCoinsHistory,Long>{
    
    @Autowired
    private CustomerCoinsHistoryDao customerCoinsHistoryDao;

    @Override
    protected BaseDao<CustomerCoinsHistory, Long> getEntityDao() {
        return customerCoinsHistoryDao;
    }
}
