package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.ReceiveAddressDao;
import com.meiyuetao.myt.customer.entity.ReceiveAddress;

@Service
@Transactional
public class ReceiveAddressService extends BaseService<ReceiveAddress, Long> {

    @Autowired
    private ReceiveAddressDao receiveAddressDao;

    @Override
    protected BaseDao<ReceiveAddress, Long> getEntityDao() {
        // TODO Auto-generated method stub
        return receiveAddressDao;
    }

}
