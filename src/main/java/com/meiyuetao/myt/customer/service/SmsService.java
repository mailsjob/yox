package com.meiyuetao.myt.customer.service;

import lab.s2jh.ctx.DynamicConfigService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.customer.entity.SmsLog;

public abstract class SmsService {

    private Logger logger = LoggerFactory.getLogger(SmsService.class);

    @Autowired
    private DynamicConfigService dynamicConfigService;

    public Boolean send(String phone, String content, SmsLog smsLog) {
        if (dynamicConfigService.getBoolean("cfg.sms.mock.mode", false)) {
            logger.debug("Mock sending  SMS to: {} \n--------------------\n{}\n----------", phone, content);
            return true;
        }
        if (logger.isInfoEnabled()) {
            logger.info("Sending " + this.getClass().getSimpleName() + " SMS to: {} \n--------------------\n{}\n----------", phone, content);
        }

        return bizSend(phone, content, smsLog);
    }

    // 发送短信
    protected abstract Boolean bizSend(String phone, String content, SmsLog smsLog);

}
