package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.CustomerProfileDao;
import com.meiyuetao.myt.customer.dao.NameCertificateDao;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.customer.entity.CustomerProfile.CustomCertificateTypeEnum;
import com.meiyuetao.myt.customer.entity.NameCertificate;
import com.meiyuetao.myt.customer.entity.NameCertificate.CertificateStatusEnum;
import com.meiyuetao.myt.customer.entity.NameCertificate.CertificateTypeEnum;

@Service
@Transactional
public class NameCertificateService extends BaseService<NameCertificate, Long> {

    @Autowired
    private NameCertificateDao nameCertificateDao;
    @Autowired
    private CustomerProfileDao customerProfileDao;

    @Override
    protected BaseDao<NameCertificate, Long> getEntityDao() {
        return nameCertificateDao;
    }

    @Override
    public NameCertificate save(NameCertificate entity) {
        if (entity.getCertificateStatus().equals(CertificateStatusEnum.S30COMPLETE) && entity.getCertificateType().equals(CertificateTypeEnum.S10SM)) {
            CustomerProfile customerProfile = entity.getCustomerProfile();
            if (!CustomCertificateTypeEnum.S10SM.equals(customerProfile.getCertificateType())) {
                customerProfile.setCertificateType(CustomCertificateTypeEnum.S10SM);
                customerProfileDao.save(customerProfile);
            }
        }

        return super.save(entity);
    }

}
