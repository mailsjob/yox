package com.meiyuetao.myt.customer.service;

import com.meiyuetao.myt.customer.dao.CustomerAccountHistDao;
import com.meiyuetao.myt.customer.dao.CustomerProfileDao;
import com.meiyuetao.myt.customer.entity.CustomerAccountHist;
import com.meiyuetao.myt.customer.entity.CustomerCoinsHistory;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.util.ExcelUtil;
import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CustomerProfileService extends BaseService<CustomerProfile, Long> {

    @Autowired
    private CustomerProfileDao customerProfileDao;
    @Autowired
    private CustomerAccountHistDao customerAccountHistDao;
    @Autowired
    private CustomerCoinsHistoryService customerCoinsHistoryService;

    @Override
    protected BaseDao<CustomerProfile, Long> getEntityDao() {
        return customerProfileDao;
    }

    /**
     * 查询常用数据用于缓存
     *
     * @return
     */
    public Collection<CustomerProfile> findFrequentUsedCustomerProfiles() {
        return customerProfileDao.findFrequentUsedCustomerProfiles();
    }

    public void reissueSign(CustomerProfile customerProfile, CustomerAccountHist customerAccountHist) {
        customerAccountHist.setCustomerProfile(customerProfile);
        customerAccountHistDao.save(customerAccountHist);
        if (customerProfile.getCheckInAccount() == null) {
            customerProfile.setCheckInAccount(customerAccountHist.getAmount());
        } else {
            customerProfile.setCheckInAccount(customerProfile.getCheckInAccount().add(customerAccountHist.getAmount()));
        }
        customerProfileDao.save(customerProfile);
    }

    public String importCoins(File customerExcel) throws IOException, InvalidFormatException {
        List<Row> rows = ExcelUtil.readXls(customerExcel);
        String sid = "";
        String coins = "";
        String createInfo = "";
        for (Row row : rows) {
            sid = ExcelUtil.getValue(row.getCell(0));
            coins = ExcelUtil.getValue(row.getCell(1));
            BigDecimal coinBig = BigDecimal.valueOf(Long.valueOf(coins));
            CustomerProfile cp = super.findOne(Long.valueOf(sid));

            BigDecimal oldCoin = BigDecimal.ZERO;
            BigDecimal oldFreezeCoin = BigDecimal.ZERO;
            BigDecimal oldBaseScore = BigDecimal.ZERO;
            BigDecimal oldFreezenScore = BigDecimal.ZERO;
            if (null != cp.getCoins()) {
                oldCoin = cp.getCoins();
            }
            if (null != cp.getFreezeCoins()) {
                oldFreezeCoin = cp.getFreezeCoins();
            }
            if (null != cp.getBaseNewScore()) {
                oldBaseScore = cp.getBaseNewScore();
            }
            if (null != cp.getFreezenNewScore()) {
                oldFreezenScore = cp.getFreezenNewScore();
            }
            CustomerCoinsHistory cch = new CustomerCoinsHistory();
            cch.setCoins(coinBig);
            cch.setBaseNewScore(coinBig);
            cch.setCoinsBefore(oldCoin);
            cch.setCoinsAfter(oldCoin.add(coinBig));
            cch.setCustomerProfile(cp);
            if (coinBig.compareTo(BigDecimal.ZERO) > 0) {
                cch.setOperationType(CustomerCoinsHistory.OperationTypeEnum.CHARGE);
            } else {
                if (oldBaseScore.add(coinBig).compareTo(BigDecimal.ZERO) <= 0) {
                    createInfo = "反充值失败";
                    continue;
                }
                cch.setOperationType(CustomerCoinsHistory.OperationTypeEnum.RECHARGE);
            }
            cch.setMemo("批量充值" + coinBig + "积分");
            customerCoinsHistoryService.save(cch);
            cp.setCoins(oldCoin.add(coinBig));
            cp.setBaseNewScore(oldBaseScore.add(coinBig));
            // 充值积分的30%将被冻结  2016-04-12 冻结积分暂时取消
            // cp.setFreezeCoins(oldFreezeCoin.add(coinBig.multiply(BigDecimal.valueOf(0.3))));
            // cp.setFreezenNewScore(oldFreezenScore.add(coinBig.multiply(BigDecimal.valueOf(0.3))));
            cp.setFreezeCoins(BigDecimal.ZERO);
            cp.setFreezenNewScore(BigDecimal.ZERO);
            cp.setCustomerFrom("SD");
            cp.setLastRechargeTime(new Date());
            super.save(cp);
        }
        return createInfo;

    }
}
