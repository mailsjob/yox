package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.WeixinMsgDao;
import com.meiyuetao.myt.customer.entity.WeixinMsg;

@Service
@Transactional
public class WeixinMsgService extends BaseService<WeixinMsg, Long> {

    @Autowired
    private WeixinMsgDao weixinMsgDao;

    @Override
    protected BaseDao<WeixinMsg, Long> getEntityDao() {
        return weixinMsgDao;
    }
}
