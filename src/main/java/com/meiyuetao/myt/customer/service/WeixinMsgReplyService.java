package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.WeixinMsgReplyDao;
import com.meiyuetao.myt.customer.entity.WeixinMsgReply;

@Service
@Transactional
public class WeixinMsgReplyService extends BaseService<WeixinMsgReply, Long> {

    @Autowired
    private WeixinMsgReplyDao weixinMsgReplyDao;

    @Override
    protected BaseDao<WeixinMsgReply, Long> getEntityDao() {
        return weixinMsgReplyDao;
    }
}
