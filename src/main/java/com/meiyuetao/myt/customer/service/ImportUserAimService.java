package com.meiyuetao.myt.customer.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.customer.dao.ImportUserAimDao;
import com.meiyuetao.myt.customer.entity.ImportUserAim;

@Service
@Transactional
public class ImportUserAimService extends BaseService<ImportUserAim, Long> {

    @Autowired
    private ImportUserAimDao importUserAimDao;

    @Override
    protected BaseDao<ImportUserAim, Long> getEntityDao() {
        return importUserAimDao;
    }

}
