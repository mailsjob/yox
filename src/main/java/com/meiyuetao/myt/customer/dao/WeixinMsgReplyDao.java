package com.meiyuetao.myt.customer.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.customer.entity.WeixinMsgReply;

@Repository
public interface WeixinMsgReplyDao extends BaseDao<WeixinMsgReply, Long> {

}