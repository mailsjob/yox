package com.meiyuetao.myt.customer.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.customer.entity.SmsLog;

@Repository
public interface SmsLogDao extends BaseDao<SmsLog, Long> {

}