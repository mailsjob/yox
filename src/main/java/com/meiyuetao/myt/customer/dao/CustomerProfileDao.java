package com.meiyuetao.myt.customer.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.customer.entity.CustomerProfile;

@Repository
public interface CustomerProfileDao extends BaseDao<CustomerProfile, Long> {

    @Query("from CustomerProfile where id in (select distinct customerProfile.id from SaleDelivery) or id in (select distinct customerProfile.id from BoxOrder)")
    List<CustomerProfile> findFrequentUsedCustomerProfiles();
}