package com.meiyuetao.myt.customer.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.customer.entity.InactiveUser;

@Repository
public interface InactiveUserDao extends BaseDao<InactiveUser, Long> {

}