package com.meiyuetao.myt.customer.dao;

import com.meiyuetao.myt.customer.entity.CustomerCoinsHistory;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface CustomerCoinsHistoryDao extends BaseDao<CustomerCoinsHistory, Long> {

}