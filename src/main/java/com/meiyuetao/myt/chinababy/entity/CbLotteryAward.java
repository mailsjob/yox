package com.meiyuetao.myt.chinababy.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.Region;

@Entity
@Table(name = "iyb_cb_lottery_award")
@MetaData(value = "中国宝宝抽奖奖品")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CbLotteryAward extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "抽奖码")
    @EntityAutoCode(order = 10, search = true)
    private String lotteryCode;

    @MetaData(value = "奖品名")
    @EntityAutoCode(order = 20, search = true)
    private String awardName;

    @MetaData(value = "奖品码")
    @EntityAutoCode(order = 10, search = true)
    private String awardCode;

    @MetaData(value = "总数")
    @EntityAutoCode(order = 10, search = true)
    private Integer awardTotal = 0;

    @MetaData(value = "中奖但未发送数")
    @EntityAutoCode(order = 10, search = true)
    private Integer awardWinUnsent = 0;

    @MetaData(value = "发送数")
    @EntityAutoCode(order = 10, search = true)
    private Integer awardSent = 0;

    @MetaData(value = "剩余数")
    @EntityAutoCode(order = 10, search = true)
    private Integer awardRemain = 0;

    @MetaData(value = "启用标识", comments = "禁用之后则不能登录访问系统")
    @EntityAutoCode(order = 40, search = true)
    private Boolean enable = Boolean.FALSE;

    @MetaData(value = "中奖率")
    @EntityAutoCode(order = 10, search = true)
    private BigDecimal winRate = BigDecimal.ZERO;

    @MetaData(value = "备注")
    @EntityAutoCode(order = 20, search = true)
    private String memo;

    @MetaData(value = "奖品类型")
    @EntityAutoCode(order = 20, search = true)
    private AwardTypeEnum awardType = AwardTypeEnum.LOTTERY;

    public enum AwardTypeEnum {

        @MetaData("抽奖")
        LOTTERY,

        @MetaData("满月")
        FULLMONTH,

        @MetaData("百日")
        HUNDREDDAYS,

        @MetaData("爱宝贝新用户奖品")
        CBNUEW,

        @MetaData("一周岁")
        FULLYEAR,

    }

    @MetaData(value = "商品")
    @EntityAutoCode(order = 20, search = true)
    private Commodity commodity;

    @MetaData(value = "奖品图片")
    @EntityAutoCode(order = 20, search = true)
    private String awardImg;

    @MetaData(value = "排序号")
    @EntityAutoCode(order = 20, search = true)
    private Integer orderIndex;

    @MetaData(value = "地区")
    @EntityAutoCode(order = 20, search = true)
    private Region region;

    @Column(length = 32, nullable = false)
    @JsonProperty
    public String getLotteryCode() {
        return lotteryCode;
    }

    public void setLotteryCode(String lotteryCode) {
        this.lotteryCode = lotteryCode;
    }

    @Column(length = 64, nullable = false)
    @JsonProperty
    public String getAwardName() {
        return awardName;
    }

    public void setAwardName(String awardName) {
        this.awardName = awardName;
    }

    @Column(length = 64, nullable = false)
    @JsonProperty
    public String getAwardCode() {
        return awardCode;
    }

    public void setAwardCode(String awardCode) {
        this.awardCode = awardCode;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getAwardTotal() {
        return awardTotal;
    }

    public void setAwardTotal(Integer awardTotal) {
        this.awardTotal = awardTotal;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getAwardWinUnsent() {
        return awardWinUnsent;
    }

    public void setAwardWinUnsent(Integer awardWinUnsent) {
        this.awardWinUnsent = awardWinUnsent;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getAwardSent() {
        return awardSent;
    }

    public void setAwardSent(Integer awardSent) {
        this.awardSent = awardSent;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getAwardRemain() {
        return awardRemain;
    }

    public void setAwardRemain(Integer awardRemain) {
        this.awardRemain = awardRemain;
    }

    @Column(nullable = false, name = "is_enable")
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @JsonProperty
    public BigDecimal getWinRate() {
        return winRate;
    }

    public void setWinRate(BigDecimal winRate) {
        this.winRate = winRate;
    }

    @Column(length = 512)
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public AwardTypeEnum getAwardType() {
        return awardType;
    }

    public void setAwardType(AwardTypeEnum awardType) {
        this.awardType = awardType;
    }

    @OneToOne
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public String getAwardImg() {
        return awardImg;
    }

    public void setAwardImg(String awardImg) {
        this.awardImg = awardImg;
    }

    @Column(length = 512, nullable = false)
    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "region_sid", nullable = false)
    @JsonProperty
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Transient
    public String getDisplay() {
        return this.getLotteryCode() + " " + this.getAwardName() + " " + this.getAwardCode();
    }

}
