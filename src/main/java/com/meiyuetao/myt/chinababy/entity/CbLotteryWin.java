package com.meiyuetao.myt.chinababy.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.purchase.entity.Supplier;
import com.meiyuetao.myt.sale.entity.BoxOrder;

@Entity
@Table(name = "iyb_cb_lottery_win")
@MetaData(value = "爱宝贝获奖明细")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CbLotteryWin extends MytBaseEntity {
    private static final long serialVersionUID = 1L;

    @MetaData(value = "客户")
    @EntityAutoCode(order = 10, search = true)
    private CustomerProfile customerProfile;

    @MetaData(value = "关联订单")
    @EntityAutoCode(order = 20, search = true)
    private BoxOrder boxOrder;

    @MetaData(value = "备注")
    @EntityAutoCode(order = 20, search = true)
    private String memo;

    @MetaData(value = "奖品类型")
    @EntityAutoCode(order = 20, search = true)
    private WinAwardTypeEnum awardType = WinAwardTypeEnum.LOTTERY;
    @MetaData(value = "获奖状态")
    private WinStatusEnum winStatus = WinStatusEnum.GETED;
    @MetaData(value = "销售单号")
    @EntityAutoCode(order = 20, search = true)
    private String saleDeliveryNo;
    @MetaData("快递单号")
    private String logisticsNo;

    @MetaData("快递公司")
    private Supplier logistics;

    public enum WinStatusEnum {

        @MetaData("已领取")
        GETED,

        @MetaData("发送中")
        SENT,

        @MetaData("已放弃")
        GIVEUP,

        @MetaData("其他")
        OTHER;

    }

    private List<CbLotteryWinDetail> cbLotteryWinDetails = new ArrayList<CbLotteryWinDetail>();

    public enum WinAwardTypeEnum {

        @MetaData("抽奖")
        LOTTERY,

        @MetaData("满月")
        FULLMONTH,

        @MetaData("百日")
        HUNDREDDAYS,

        @MetaData("爱宝贝新用户奖品")
        CBNUEW,

        @MetaData("一周岁")
        FULLYEAR,

    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "customer_profile_sid", nullable = false)
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @MetaData(value = "显示客户名")
    @EntityAutoCode(order = 20, search = true)
    private String displayCustomerName;

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public WinAwardTypeEnum getAwardType() {
        return awardType;
    }

    public void setAwardType(WinAwardTypeEnum awardType) {
        this.awardType = awardType;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "order_sid")
    @JsonProperty
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

    @OneToMany(mappedBy = "cbLotteryWin", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<CbLotteryWinDetail> getCbLotteryWinDetails() {
        return cbLotteryWinDetails;
    }

    public void setCbLotteryWinDetails(List<CbLotteryWinDetail> cbLotteryWinDetails) {
        this.cbLotteryWinDetails = cbLotteryWinDetails;
    }

    public String getDisplayCustomerName() {
        return displayCustomerName;
    }

    public void setDisplayCustomerName(String displayCustomerName) {
        this.displayCustomerName = displayCustomerName;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public WinStatusEnum getWinStatus() {
        return winStatus;
    }

    public void setWinStatus(WinStatusEnum winStatus) {
        this.winStatus = winStatus;
    }

    @JsonProperty
    public String getSaleDeliveryNo() {
        return saleDeliveryNo;
    }

    public void setSaleDeliveryNo(String saleDeliveryNo) {
        this.saleDeliveryNo = saleDeliveryNo;
    }

    @JsonProperty
    public String getLogisticsNo() {
        return logisticsNo;
    }

    public void setLogisticsNo(String logisticsNo) {
        this.logisticsNo = logisticsNo;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "logistics_sid", nullable = true)
    @JsonProperty
    public Supplier getLogistics() {
        return logistics;
    }

    public void setLogistics(Supplier logistics) {
        this.logistics = logistics;
    }

}
