package com.meiyuetao.myt.chinababy.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Region;

@Entity
@Table(name = "iyb_cb_hospital_r2_region")
@MetaData(value = "医院关联区域")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CbHospitalR2Region extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "医院")
    @EntityAutoCode(order = 10, search = true)
    private CbHospital cbHospital;

    @MetaData(value = "启用标识", comments = "禁用之后则不能登录访问系统")
    @EntityAutoCode(order = 40, search = true)
    private Boolean enable = Boolean.FALSE;

    @MetaData(value = "地区")
    @EntityAutoCode(order = 20, search = true)
    private Region region;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "cb_hospital_sid", nullable = false)
    public CbHospital getCbHospital() {
        return cbHospital;
    }

    public void setCbHospital(CbHospital cbHospital) {
        this.cbHospital = cbHospital;
    }

    @Column(name = "is_enable", nullable = false)
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "region_sid", nullable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

}
