package com.meiyuetao.myt.chinababy.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;

@Entity
@Table(name = "iyb_cb_lottery_win_detail")
@MetaData(value = "获奖明细")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CbLotteryWinDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "获奖")
    @EntityAutoCode(order = 20, search = true)
    private CbLotteryWin cbLotteryWin;

    @MetaData(value = "奖品")
    @EntityAutoCode(order = 20, search = true)
    private CbLotteryAward lotteryAward;

    @MetaData(value = "显示奖品名")
    @EntityAutoCode(order = 20, search = true)
    private String awardName;

    @MetaData(value = "获奖商品")
    @EntityAutoCode(order = 20, search = true)
    private Commodity commodity;

    @MetaData(value = "奖品图片")
    @EntityAutoCode(order = 20, search = true)
    private String awardImg;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "lottery_award_sid", nullable = false)
    @JsonProperty
    public CbLotteryAward getLotteryAward() {
        return lotteryAward;
    }

    public void setLotteryAward(CbLotteryAward lotteryAward) {
        this.lotteryAward = lotteryAward;
    }

    @JsonProperty
    public String getAwardName() {
        return awardName;
    }

    public void setAwardName(String awardName) {
        this.awardName = awardName;
    }

    @ManyToOne
    @JoinColumn(name = "lottery_win_sid", nullable = false)
    @JsonProperty
    public CbLotteryWin getCbLotteryWin() {
        return cbLotteryWin;
    }

    public void setCbLotteryWin(CbLotteryWin cbLotteryWin) {
        this.cbLotteryWin = cbLotteryWin;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public String getAwardImg() {
        return awardImg;
    }

    public void setAwardImg(String awardImg) {
        this.awardImg = awardImg;
    }

}
