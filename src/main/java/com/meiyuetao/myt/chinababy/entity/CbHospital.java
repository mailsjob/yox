package com.meiyuetao.myt.chinababy.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@Entity
@Table(name = "iyb_cb_hospital")
@MetaData(value = "医院")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CbHospital extends MytBaseEntity {
    private static final long serialVersionUID = 1L;

    @MetaData(value = "医院名称")
    @EntityAutoCode(order = 10, search = true)
    private String hospitalName;

    @MetaData(value = "启用标识", comments = "禁用之后则不能登录访问系统")
    @EntityAutoCode(order = 40, search = true)
    private Boolean enable = Boolean.FALSE;

    @MetaData(value = "备注")
    @EntityAutoCode(order = 20, search = true)
    private String memo;

    private List<CbHospitalR2Region> cbHospitalR2Regions = new ArrayList<CbHospitalR2Region>();

    @Column(length = 256, unique = true, nullable = false)
    @JsonProperty
    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    @Column(nullable = false, name = "is_enable")
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @Column(length = 256)
    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @OneToMany(mappedBy = "cbHospital", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    public List<CbHospitalR2Region> getCbHospitalR2Regions() {
        return cbHospitalR2Regions;
    }

    public void setCbHospitalR2Regions(List<CbHospitalR2Region> cbHospitalR2Regions) {
        this.cbHospitalR2Regions = cbHospitalR2Regions;
    }

    @Transient
    @JsonProperty
    public String getRegionNames() {
        List<String> regionNames = Lists.newArrayList();
        for (CbHospitalR2Region item : cbHospitalR2Regions) {
            regionNames.add(item.getRegion().getName());
        }
        return StringUtils.join(regionNames, ",");
    }

}
