package com.meiyuetao.myt.chinababy.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.chinababy.entity.CbHospitalR2Region;

@Repository
public interface CbHospitalR2RegionDao extends BaseDao<CbHospitalR2Region, Long> {

}