package com.meiyuetao.myt.chinababy.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.chinababy.entity.CbHospital;

@Repository
public interface CbHospitalDao extends BaseDao<CbHospital, Long> {

}