package com.meiyuetao.myt.chinababy.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.chinababy.entity.CbLotteryWin;

@Repository
public interface CbLotteryWinDao extends BaseDao<CbLotteryWin, Long> {
    @Query("from CbLotteryWin c where c.boxOrder is not null")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    List<CbLotteryWin> findRelateBoxOrder();
}