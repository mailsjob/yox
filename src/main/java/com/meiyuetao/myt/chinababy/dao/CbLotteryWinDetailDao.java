package com.meiyuetao.myt.chinababy.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.chinababy.entity.CbLotteryWinDetail;

@Repository
public interface CbLotteryWinDetailDao extends BaseDao<CbLotteryWinDetail, Long> {

}