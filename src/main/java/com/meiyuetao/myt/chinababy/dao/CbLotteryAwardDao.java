package com.meiyuetao.myt.chinababy.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.chinababy.entity.CbLotteryAward;

@Repository
public interface CbLotteryAwardDao extends BaseDao<CbLotteryAward, Long> {
    @Query("from CbLotteryAward order by lotteryCode asc")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    public List<CbLotteryAward> findAllCached();

}