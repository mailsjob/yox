package com.meiyuetao.myt.chinababy.service;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.chinababy.dao.CbLotteryAwardDao;
import com.meiyuetao.myt.chinababy.entity.CbLotteryAward;

@Service
@Transactional
public class CbLotteryAwardService extends BaseService<CbLotteryAward, Long> {

    @Autowired
    private CbLotteryAwardDao cbLotteryAwardDao;

    @Override
    protected BaseDao<CbLotteryAward, Long> getEntityDao() {
        return cbLotteryAwardDao;
    }

    public List<CbLotteryAward> findAllCached() {
        return cbLotteryAwardDao.findAllCached();
    }

    public List<CbLotteryAward> findForAutocomplete(String term) {
        GroupPropertyFilter gpf = GroupPropertyFilter.buildDefaultAndGroupFilter();
        gpf.append(new PropertyFilter(MatchType.CN, new String[] { "lotteryCode", "awardName", "awardCode" }, term));

        Sort sort = new Sort(Direction.ASC, "lotteryCode");
        return findByFilters(gpf, sort, 20);
    }

}
