package com.meiyuetao.myt.chinababy.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.chinababy.dao.CbHospitalDao;
import com.meiyuetao.myt.chinababy.entity.CbHospital;

@Service
@Transactional
public class CbHospitalService extends BaseService<CbHospital, Long> {

    @Autowired
    private CbHospitalDao cbHospitalDao;

    @Override
    protected BaseDao<CbHospital, Long> getEntityDao() {
        return cbHospitalDao;
    }
}
