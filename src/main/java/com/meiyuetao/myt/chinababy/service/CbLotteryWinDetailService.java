package com.meiyuetao.myt.chinababy.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.chinababy.dao.CbLotteryWinDetailDao;
import com.meiyuetao.myt.chinababy.entity.CbLotteryWinDetail;

@Service
@Transactional
public class CbLotteryWinDetailService extends BaseService<CbLotteryWinDetail, Long> {

    @Autowired
    private CbLotteryWinDetailDao cbLotteryWinDetailDao;

    @Override
    protected BaseDao<CbLotteryWinDetail, Long> getEntityDao() {
        return cbLotteryWinDetailDao;
    }
}
