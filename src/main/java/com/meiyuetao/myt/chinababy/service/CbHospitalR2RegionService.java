package com.meiyuetao.myt.chinababy.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.chinababy.dao.CbHospitalR2RegionDao;
import com.meiyuetao.myt.chinababy.entity.CbHospitalR2Region;

@Service
@Transactional
public class CbHospitalR2RegionService extends BaseService<CbHospitalR2Region, Long> {

    @Autowired
    private CbHospitalR2RegionDao cbHospitalR2RegionDao;

    @Override
    protected BaseDao<CbHospitalR2Region, Long> getEntityDao() {
        return cbHospitalR2RegionDao;
    }
}
