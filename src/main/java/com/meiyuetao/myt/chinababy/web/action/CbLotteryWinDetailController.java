package com.meiyuetao.myt.chinababy.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.chinababy.entity.CbLotteryWinDetail;
import com.meiyuetao.myt.chinababy.service.CbLotteryWinDetailService;
import com.meiyuetao.myt.core.web.MytBaseController;

@MetaData("CbLotteryWinDetailController")
public class CbLotteryWinDetailController extends MytBaseController<CbLotteryWinDetail, Long> {
    @Autowired
    private CbLotteryWinDetailService cbLotteryWinDetailService;

    @Override
    protected BaseService<CbLotteryWinDetail, Long> getEntityService() {
        return cbLotteryWinDetailService;
    }

    @Override
    protected void checkEntityAclPermission(CbLotteryWinDetail entity) {

    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {

        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

}