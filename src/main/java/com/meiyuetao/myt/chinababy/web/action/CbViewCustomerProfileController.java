package com.meiyuetao.myt.chinababy.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.exception.DataAccessDeniedException;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.chinababy.entity.CbLotteryWin;
import com.meiyuetao.myt.chinababy.service.CbLotteryWinService;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.customer.service.CustomerProfileService;
import com.meiyuetao.myt.customer.service.ReceiveAddressService;

@MetaData("爱宝贝客户列表")
public class CbViewCustomerProfileController extends MytBaseController<CustomerProfile, Long> {

    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private ReceiveAddressService receiveAddressService;

    @Autowired
    private CbLotteryWinService cbLotteryWinService;

    @Override
    protected BaseService<CustomerProfile, Long> getEntityService() {
        return customerProfileService;
    }

    @Override
    protected void checkEntityAclPermission(CustomerProfile entity) {
        if (!"9".equals(entity.getCustomerNameType())) {
            throw new DataAccessDeniedException();
        }
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "customerNameType", "9"));
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData(value = "获奖记录")
    public HttpHeaders getLotteryWin() {
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "customerProfile", bindingEntity));
        List<CbLotteryWin> cbLotteryWins = cbLotteryWinService.findByFilters(groupPropertyFilter);
        if (CollectionUtils.isEmpty(cbLotteryWins)) {
            setModel("客户未领奖");
        } else {
            setModel(buildPageResultFromList(cbLotteryWins));
        }
        return buildDefaultHttpHeaders();
    }

}