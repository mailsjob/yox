package com.meiyuetao.myt.chinababy.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.exception.DataAccessDeniedException;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrder.OrderFromEnum;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail;
import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;
import com.meiyuetao.myt.sale.service.BoxOrderDetailService;
import com.meiyuetao.myt.sale.service.BoxOrderService;

@MetaData("爱宝贝订单")
public class CbViewBoxOrderController extends MytBaseController<BoxOrder, Long> {

    @Autowired
    private BoxOrderService boxOrderService;
    @Autowired
    private BoxOrderDetailService boxOrderDetailService;

    @Override
    protected BaseService<BoxOrder, Long> getEntityService() {
        return boxOrderService;
    }

    @Override
    protected void checkEntityAclPermission(BoxOrder entity) {
        if (!OrderFromEnum.CHINABABY.equals(entity.getOrderFrom())) {
            throw new DataAccessDeniedException();
        }
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "orderFrom", OrderFromEnum.CHINABABY));
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData(value = "订单行项列表")
    public HttpHeaders getBoxOrderDetails() {
        List<BoxOrderDetail> boxOrderDetails = bindingEntity.getBoxOrderDetails();
        setModel(buildPageResultFromList(boxOrderDetails));
        return buildDefaultHttpHeaders();
    }

    @MetaData(value = "订单行项商品列表")
    public HttpHeaders getBoxOrderDetailCommodities() {
        String detailSid = this.getRequiredParameter("detailSid");
        BoxOrderDetail boxOrderdetail = boxOrderDetailService.findOne(Long.valueOf(detailSid));
        checkEntityAclPermission(boxOrderdetail.getBoxOrder());
        List<BoxOrderDetailCommodity> boxOrderDetailCommodities = boxOrderdetail.getBoxOrderDetailCommodities();
        setModel(buildPageResultFromList(boxOrderDetailCommodities));
        return buildDefaultHttpHeaders();
    }
}