package com.meiyuetao.myt.chinababy.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.chinababy.entity.CbLotteryWin;
import com.meiyuetao.myt.chinababy.service.CbLotteryWinService;
import com.meiyuetao.myt.core.web.MytBaseController;

@MetaData("获奖查看")
public class CbViewLotteryWinController extends MytBaseController<CbLotteryWin, Long> {

    @Autowired
    private CbLotteryWinService cbLotteryWinService;

    @Override
    protected BaseService<CbLotteryWin, Long> getEntityService() {
        return cbLotteryWinService;
    }

    @Override
    protected void checkEntityAclPermission(CbLotteryWin entity) {
        // Add acl check code logic
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

}