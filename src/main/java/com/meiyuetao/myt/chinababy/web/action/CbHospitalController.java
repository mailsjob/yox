package com.meiyuetao.myt.chinababy.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.chinababy.entity.CbHospital;
import com.meiyuetao.myt.chinababy.entity.CbHospitalR2Region;
import com.meiyuetao.myt.chinababy.service.CbHospitalR2RegionService;
import com.meiyuetao.myt.chinababy.service.CbHospitalService;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.service.RegionService;

@MetaData("CbHospitalController")
public class CbHospitalController extends MytBaseController<CbHospital, Long> {

    @Autowired
    private CbHospitalService cbHospitalService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private CbHospitalR2RegionService cbHospitalR2RegionService;

    @Override
    protected BaseService<CbHospital, Long> getEntityService() {
        return cbHospitalService;
    }

    @Override
    protected void checkEntityAclPermission(CbHospital entity) {
        // TODO Add acl check code logic
    }

    @Override
    protected void setupDetachedBindingEntity(Long id) {
        bindingEntity = getEntityService().findDetachedOne(id, "cbHospitalR2Regions");
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.isNew()) {
            for (CbHospitalR2Region cbHospitalR2Region : bindingEntity.getCbHospitalR2Regions()) {
                cbHospitalR2Region.setCbHospital(bindingEntity);
            }
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}