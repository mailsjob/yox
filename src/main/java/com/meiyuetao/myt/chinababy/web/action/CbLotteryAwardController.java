package com.meiyuetao.myt.chinababy.web.action;

import java.util.List;
import java.util.Map;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.annotation.SecurityControlIgnore;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.meiyuetao.myt.chinababy.entity.CbLotteryAward;
import com.meiyuetao.myt.chinababy.service.CbLotteryAwardService;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.service.CommodityService;

@MetaData("CbLotteryAwardController")
public class CbLotteryAwardController extends MytBaseController<CbLotteryAward, Long> {

    @Autowired
    private CbLotteryAwardService cbLotteryAwardService;

    @Autowired
    private CommodityService commodityService;

    @Override
    protected BaseService<CbLotteryAward, Long> getEntityService() {
        return cbLotteryAwardService;
    }

    @Override
    protected void checkEntityAclPermission(CbLotteryAward entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData(value = "自动提示完成数据")
    @SecurityControlIgnore
    public HttpHeaders autocomplete() {
        String term = this.getParameter(PARAM_NAME_FOR_AUTOCOMPLETE);
        List<Map<String, Object>> datas = Lists.newArrayList();
        if (StringUtils.isNotBlank(term)) {
            List<CbLotteryAward> lotteryAwards = cbLotteryAwardService.findForAutocomplete(term);
            for (CbLotteryAward lotteryAward : lotteryAwards) {
                Map<String, Object> row = Maps.newHashMap();
                row.put("id", lotteryAward.getId());
                row.put("value", lotteryAward.getLotteryCode());
                String title = lotteryAward.getLotteryCode() + " : " + lotteryAward.getAwardName() + "," + lotteryAward.getAwardCode();
                row.put("label", title);
                datas.add(row);
            }
        }

        setModel(datas);
        return buildDefaultHttpHeaders();
    }

}