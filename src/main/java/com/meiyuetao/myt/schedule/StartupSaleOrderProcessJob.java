package com.meiyuetao.myt.schedule;

import java.util.List;

import lab.s2jh.bpm.service.ActivitiService;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.schedule.BaseQuartzJobBean;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail.BoxOrderDetailStatusEnum;
import com.meiyuetao.myt.sale.service.BoxOrderDetailService;

public class StartupSaleOrderProcessJob extends BaseQuartzJobBean {

    private final static Logger logger = LoggerFactory.getLogger(StartupSaleOrderProcessJob.class);

    @Override
    protected void executeInternalBiz(JobExecutionContext context) {
        logger.debug("Just Mock: Stat database information running with Spring Quartz cluster mode...");
        ActivitiService activitiService = getSpringBean(ActivitiService.class);
        BoxOrderDetailService boxOrderDetailService = getSpringBean(BoxOrderDetailService.class);

        // 换成具体查询逻辑：查询所有activeTaskName为null，并且状态为已付款或执行中
        List<BoxOrderDetail> boxOrderDetails = Lists.newArrayList();
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(MatchType.NN, "activeTaskName", Boolean.FALSE));
        groupPropertyFilter.append(new PropertyFilter(MatchType.NU, "payTime", Boolean.FALSE));
        groupPropertyFilter.append(new PropertyFilter(MatchType.IN, "orderDetailStatus", new BoxOrderDetailStatusEnum[] { BoxOrderDetailStatusEnum.S15O,
                BoxOrderDetailStatusEnum.S20PYD, BoxOrderDetailStatusEnum.S25PYD }));
        boxOrderDetails = boxOrderDetailService.findByFilters(groupPropertyFilter);
        for (BoxOrderDetail entity : boxOrderDetails) {
            activitiService.startProcessInstanceByKey("BPM_SALE_ORDER_PROCESS", entity);
        }

    }

}
