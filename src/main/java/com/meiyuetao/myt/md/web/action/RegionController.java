package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.dao.RegionDao;
import com.meiyuetao.myt.md.entity.Region;
import com.meiyuetao.myt.md.service.RegionService;

@MetaData("RegionController")
public class RegionController extends MytBaseController<Region, Long> {
    @Autowired
    private RegionService regionService;
    @Autowired
    private RegionDao regionDao;

    @Override
    protected BaseService<Region, Long> getEntityService() {
        // TODO Auto-generated method stub
        return regionService;
    }

    @Override
    protected void checkEntityAclPermission(Region entity) {
        // TODO Auto-generated method stub

    }

    @MetaData(value = "树形表格数据")
    public HttpHeaders treeGridData() {
        setModel(regionService.findTreeDatas());
        return buildDefaultHttpHeaders();
    }
}