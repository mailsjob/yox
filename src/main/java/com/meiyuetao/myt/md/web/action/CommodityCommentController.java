package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.CommodityComment;
import com.meiyuetao.myt.md.service.CommodityCommentService;
import com.meiyuetao.myt.md.service.CommodityService;

public class CommodityCommentController extends MytBaseController<CommodityComment, Long> {
    @Autowired
    private CommodityCommentService commodityCommentService;
    @Autowired
    private CommodityService commodityService;

    @Override
    protected BaseService<CommodityComment, Long> getEntityService() {
        return commodityCommentService;
    }

    @Override
    protected void checkEntityAclPermission(CommodityComment entity) {

    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        if (groupPropertyFilter.isEmpty()) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "parent.id", true));
        }
        super.appendFilterProperty(groupPropertyFilter);
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.isNew()) {
            Commodity commodity = commodityService.findOne(Long.valueOf(getParameter("commoditySid")));
            bindingEntity.setCommodity(commodity);
        }
        bindingEntity.setIdentification(DigestUtils.md5DigestAsHex((bindingEntity.getCustomerProfile().getDisplay() + bindingEntity.getPublishTime()).getBytes()));
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}
