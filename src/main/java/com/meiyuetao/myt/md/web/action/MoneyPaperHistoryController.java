package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.MoneyPaperHistory;
import com.meiyuetao.myt.md.service.MoneyPaperHistoryService;

@MetaData("MoneyPaperHistoryController")
public class MoneyPaperHistoryController extends MytBaseController<MoneyPaperHistory, Long> {

    @Autowired
    private MoneyPaperHistoryService moneyPaperHistoryService;

    @Override
    protected BaseService<MoneyPaperHistory, Long> getEntityService() {
        return moneyPaperHistoryService;
    }

    @Override
    protected void checkEntityAclPermission(MoneyPaperHistory entity) {
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}