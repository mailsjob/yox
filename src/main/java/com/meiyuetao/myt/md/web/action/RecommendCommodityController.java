package com.meiyuetao.myt.md.web.action;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Maps;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Floor;
import com.meiyuetao.myt.md.entity.FloorMenu;
import com.meiyuetao.myt.md.entity.RecommendCommodity;
import com.meiyuetao.myt.md.service.FloorMenuService;
import com.meiyuetao.myt.md.service.FloorService;
import com.meiyuetao.myt.md.service.RecommendCommodityService;

@MetaData("RecommendCommodityController")
public class RecommendCommodityController extends MytBaseController<RecommendCommodity, Long> {

    @Autowired
    private RecommendCommodityService recommendCommodityService;
    @Autowired
    private FloorService floorService;
    @Autowired
    private FloorMenuService floorMenuService;

    @Override
    protected BaseService<RecommendCommodity, Long> getEntityService() {
        return recommendCommodityService;
    }

    @Override
    protected void checkEntityAclPermission(RecommendCommodity entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    public Map<Long, String> getFloorsMap() {
        Map<Long, String> floorsMap = new LinkedHashMap<Long, String>();
        Iterable<Floor> floors = floorService.findAllCached();
        Iterator<Floor> it = floors.iterator();
        while (it.hasNext()) {
            Floor floor = it.next();
            floorsMap.put(floor.getId(), floor.getMonthText());
        }
        return floorsMap;
    }

    public Map<Long, String> getFloorMenusMap() {
        Map<Long, String> floorMenusMap = new LinkedHashMap<Long, String>();
        Iterable<FloorMenu> floors = floorMenuService.findAllCached();
        Iterator<FloorMenu> it = floors.iterator();
        while (it.hasNext()) {
            FloorMenu floorMenu = it.next();
            floorMenusMap.put(floorMenu.getId(), floorMenu.getName());
        }
        return floorMenusMap;
    }

    public HttpHeaders floorMenusByFloor() {
        String floorId = this.getParameter("floorId");
        Map<Long, String> datas = Maps.newHashMap();
        List<FloorMenu> floorMenus = floorMenuService.findByFloor(floorService.findOne(Long.valueOf(floorId)));
        for (FloorMenu floorMenu : floorMenus) {
            datas.put(floorMenu.getId(), floorMenu.getName());
        }
        setModel(datas);
        return buildDefaultHttpHeaders();
    }
}