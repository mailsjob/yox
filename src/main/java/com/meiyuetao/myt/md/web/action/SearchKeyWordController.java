package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.SearchKeyWord;
import com.meiyuetao.myt.md.service.SearchKeyWordService;

@MetaData("SearchKeyWordController")
public class SearchKeyWordController extends MytBaseController<SearchKeyWord, Long> {

    @Autowired
    private SearchKeyWordService searchKeyWordService;

    @Override
    protected BaseService<SearchKeyWord, Long> getEntityService() {
        return searchKeyWordService;
    }

    @Override
    protected void checkEntityAclPermission(SearchKeyWord entity) {
        // TODO Add acl check code logic
    }

    @Override
    public String isDisallowUpdate() {
        SearchKeyWord searchKeyWord = searchKeyWordService.findOne(bindingEntity.getId());
        if (searchKeyWord.getYearMonth() != null && !searchKeyWord.getYearMonth().equals("0")) {
            return "有时间限制的数据不能维护";
        }

        return null;
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}