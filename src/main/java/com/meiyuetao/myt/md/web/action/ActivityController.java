package com.meiyuetao.myt.md.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.meiyuetao.myt.activity.entity.ActivityRules;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Activity;
import com.meiyuetao.myt.md.entity.ActivityR2Object;
import com.meiyuetao.myt.md.service.ActivityService;

@MetaData("ActivityController")
public class ActivityController extends MytBaseController<Activity, Long> {

    @Autowired
    private ActivityService activityService;

    @Override
    protected BaseService<Activity, Long> getEntityService() {
        return activityService;
    }

    @Override
    protected void checkEntityAclPermission(Activity entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        Assert.isTrue(bindingEntity.getBeginTime().before(bindingEntity.getEndTime()), "开始时间要小于结束时间");
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("活动详情数据")
    public HttpHeaders activityR2Objects() {
        List<ActivityR2Object> activityR2Objects = bindingEntity.getActivityR2Objects();
        setModel(buildPageResultFromList(activityR2Objects));
        return buildDefaultHttpHeaders();
    }

    @MetaData("活动规则")
    public HttpHeaders activityR2Rules() {
        List<ActivityRules> activityRules = bindingEntity.getActivityRules();
        setModel(buildPageResultFromList(activityRules));
        return buildDefaultHttpHeaders();
    }
}