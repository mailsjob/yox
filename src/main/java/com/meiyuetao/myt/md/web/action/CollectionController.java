package com.meiyuetao.myt.md.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.md.entity.Collection;
import com.meiyuetao.myt.md.entity.CollectionObject;
import com.meiyuetao.myt.md.service.CollectionService;

@MetaData("集合管理")
public class CollectionController extends BaseController<Collection, Long> {

    @Autowired
    private CollectionService collectionService;

    @Override
    protected BaseService<Collection, Long> getEntityService() {
        return collectionService;
    }

    @Override
    protected void checkEntityAclPermission(Collection entity) {
        // TODO Add acl check code logic
    }

    @MetaData("集合对象列表")
    public HttpHeaders collectionObjects() {
        List<CollectionObject> list = bindingEntity.getCollectionObjects();
        setModel(buildPageResultFromList(list));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}