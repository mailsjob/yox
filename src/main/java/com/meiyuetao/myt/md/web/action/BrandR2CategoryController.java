package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.BrandR2Category;
import com.meiyuetao.myt.md.service.BrandR2CategoryService;

@MetaData("BrandR2Category")
public class BrandR2CategoryController extends MytBaseController<BrandR2Category, Long> {

    @Autowired
    private BrandR2CategoryService brandR2CategoryService;

    @Override
    protected BaseService<BrandR2Category, Long> getEntityService() {
        return brandR2CategoryService;
    }

    @Override
    protected void checkEntityAclPermission(BrandR2Category entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}