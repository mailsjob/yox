package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.BindCommodity;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.service.BindCommodityService;
import com.meiyuetao.myt.md.service.CommodityService;

@MetaData("BindCommodityController")
public class BindCommodityController extends MytBaseController<BindCommodity, Long> {

    @Autowired
    private BindCommodityService bindCommodityService;
    @Autowired
    private CommodityService commodityService;

    @Override
    protected BaseService<BindCommodity, Long> getEntityService() {
        return bindCommodityService;
    }

    @Override
    protected void checkEntityAclPermission(BindCommodity entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        if (bindingEntity.getCommodity() != null) {
            Commodity commodity = bindingEntity.getCommodity();
            commodity.setJdSku(bindingEntity.getSku());
            commodityService.save(commodity);
        }
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.getCommodity() != null) {
            Commodity commodity = bindingEntity.getCommodity();
            commodity.setJdSku(bindingEntity.getSku());
            commodityService.save(commodity);
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        if (bindingEntity.getCommodity() != null) {
            Commodity commodity = bindingEntity.getCommodity();
            commodity.setJdSku(null);
            commodityService.save(commodity);
        }

        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}