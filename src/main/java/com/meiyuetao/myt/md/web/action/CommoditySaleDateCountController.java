package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.CommoditySaleDateCount;
import com.meiyuetao.myt.md.service.CommoditySaleDateCountService;

@MetaData("CommoditySaleDateCountController")
public class CommoditySaleDateCountController extends MytBaseController<CommoditySaleDateCount, Long> {

    @Autowired
    private CommoditySaleDateCountService commoditySaleDateCountService;

    @Override
    protected BaseService<CommoditySaleDateCount, Long> getEntityService() {
        return commoditySaleDateCountService;
    }

    @Override
    protected void checkEntityAclPermission(CommoditySaleDateCount entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}