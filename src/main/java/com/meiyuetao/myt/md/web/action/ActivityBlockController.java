package com.meiyuetao.myt.md.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.ActivityBlock;
import com.meiyuetao.myt.md.entity.ActivityBlockR2Object;
import com.meiyuetao.myt.md.service.ActivityBlockService;

@MetaData("ActivityController")
public class ActivityBlockController extends MytBaseController<ActivityBlock, Long> {

    @Autowired
    private ActivityBlockService activityBlockService;

    @Override
    protected BaseService<ActivityBlock, Long> getEntityService() {
        return activityBlockService;
    }

    @Override
    protected void checkEntityAclPermission(ActivityBlock entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {

        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("活动块详情数据")
    public HttpHeaders activityR2Objects() {
        List<ActivityBlockR2Object> activityR2Objects = bindingEntity.getActivityBlockR2Objects();
        setModel(buildPageResultFromList(activityR2Objects));
        return buildDefaultHttpHeaders();
    }
}