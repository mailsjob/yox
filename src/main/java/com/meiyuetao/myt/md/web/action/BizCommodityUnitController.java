package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.PersistableController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.md.entity.BizCommodityUnit;
import com.meiyuetao.myt.md.service.BizCommodityUnitService;

@MetaData("BizCommodityUnitController")
public class BizCommodityUnitController extends PersistableController<BizCommodityUnit, String> {

    @Autowired
    private BizCommodityUnitService bizCommodityUnitService;

    @Override
    protected BaseService<BizCommodityUnit, String> getEntityService() {
        return bizCommodityUnitService;
    }

    @Override
    protected void checkEntityAclPermission(BizCommodityUnit entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}