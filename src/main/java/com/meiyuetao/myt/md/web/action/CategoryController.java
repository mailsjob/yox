package com.meiyuetao.myt.md.web.action;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Category;
import com.meiyuetao.myt.md.entity.Category.CategoryEnum;
import com.meiyuetao.myt.md.service.CategoryService;

@MetaData("商品分类数据控制器")
public class CategoryController extends MytBaseController<Category, Long> {

    protected final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private CategoryService categoryService;

    @Override
    protected BaseService<Category, Long> getEntityService() {
        return categoryService;
    }

    @Override
    protected void checkEntityAclPermission(Category entity) {
    }

    @MetaData(value = "树形表格数据")
    public HttpHeaders categoryTreeGridData() {
        Map<String, Category> treeDatas = Maps.newLinkedHashMap();
        loopTreeGridData(treeDatas, categoryService.findRootCategories(CategoryEnum.CATEGORY));
        setModel(treeDatas.values());
        return buildDefaultHttpHeaders();
    }

    private void loopTreeGridData(Map<String, Category> treeDatas, List<Category> items) {
        for (Category item : items) {
            Category parent = item.getParent();
            List<Category> children = categoryService.findChildren(item);
            item.addExtraAttribute("level", item.getLevel());
            item.addExtraAttribute("parent", parent == null ? "" : parent.getId());
            item.addExtraAttribute("isLeaf", CollectionUtils.isEmpty(children) ? true : false);
            item.addExtraAttribute("expanded", false);
            item.addExtraAttribute("loaded", false);
            treeDatas.put(String.valueOf(item.getId()), item);
            if (!CollectionUtils.isEmpty(children)) {
                loopTreeGridData(treeDatas, children);
            }
        }
    }

    @Override
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData(value = "列表")
    public HttpHeaders categoryList() {
        List<Map<String, Object>> treeDatas = Lists.newArrayList();
        Iterable<Category> items = categoryService.findRootCategories(CategoryEnum.CATEGORY);
        for (Category item : items) {
            loopTreeData(treeDatas, item);
        }
        List<Map<String, Object>> rootList = Lists.newArrayList();
        Map<String, Object> root = Maps.newHashMap();
        rootList.add(root);
        root.put("id", "-1");
        root.put("name", "根节点");
        root.put("open", true);
        root.put("children", treeDatas);
        setModel(rootList);
        return buildDefaultHttpHeaders();
    }

    private void loopTreeData(List<Map<String, Object>> treeDatas, Category item) {
        Map<String, Object> row = Maps.newHashMap();
        treeDatas.add(row);
        row.put("id", item.getId());
        row.put("name", item.getDisplay());
        row.put("open", false);
        List<Category> children = categoryService.findChildren(item);
        if (!CollectionUtils.isEmpty(children)) {
            List<Map<String, Object>> childrenList = Lists.newArrayList();
            row.put("children", childrenList);
            for (Category child : children) {
                loopTreeData(childrenList, child);
            }
        }
    }

    public Map<String, String> getCategoryLabelsMap() {
        List<Category> categories = categoryService.findLabels();
        Map<String, String> dataMap = new LinkedHashMap<String, String>();
        for (Category category : categories) {
            dataMap.put(category.getName(), category.getName());
        }
        return dataMap;
    }

    @Override
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

}