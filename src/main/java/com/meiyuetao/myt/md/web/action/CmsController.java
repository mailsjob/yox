package com.meiyuetao.myt.md.web.action;

import java.util.Map;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.sys.service.DataDictService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.md.entity.Cms;
import com.meiyuetao.myt.md.service.CmsService;

@MetaData("cms管理")
public class CmsController extends BaseController<Cms, Long> {

    @Autowired
    private CmsService cmsService;
    @Autowired
    private DataDictService dataDictService;

    @Override
    protected BaseService<Cms, Long> getEntityService() {
        return cmsService;
    }

    @Override
    protected void checkEntityAclPermission(Cms entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    public Map<String, String> getCategoryMap() {
        Map<String, String> dataMap = dataDictService.findMapDataByPrimaryKey("IYOUBOX_CMS_CATEGORY");
        return dataMap;
    }
}