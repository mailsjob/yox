package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.FloorAd;
import com.meiyuetao.myt.md.service.FloorAdService;

@MetaData("FloorAdController")
public class FloorAdController extends MytBaseController<FloorAd, Long> {

    @Autowired
    private FloorAdService floorAdService;

    @Override
    protected BaseService<FloorAd, Long> getEntityService() {
        return floorAdService;
    }

    @Override
    protected void checkEntityAclPermission(FloorAd entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    public HttpHeaders doSave() {

        return super.doSave();
    }
}