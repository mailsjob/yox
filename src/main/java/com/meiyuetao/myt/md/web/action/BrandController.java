package com.meiyuetao.myt.md.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Brand;
import com.meiyuetao.myt.md.entity.BrandR2Category;
import com.meiyuetao.myt.md.service.BrandService;

@MetaData("BrandController")
public class BrandController extends MytBaseController<Brand, Long> {

    @Autowired
    private BrandService brandService;

    @Override
    protected BaseService<Brand, Long> getEntityService() {
        return brandService;
    }

    @Override
    protected void checkEntityAclPermission(Brand entity) {
        // TODO Add acl check code logic
    }

    /**
     * 获取品牌授权截止日期预警品牌列表 (查询品牌授权到期日期 brand的endDate在未来一周内的行项)
     * 
     * @return
     */
    public HttpHeaders getBrandEndDateWarningList() {
        GroupPropertyFilter groupFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        DateTime dt = new DateTime();
        dt = dt.plusWeeks(1);
        groupFilter.append(new PropertyFilter(MatchType.LE, "endDate", dt.toDate()));
        List<Brand> brands = brandService.findByFilters(groupFilter);
        setModel(buildPageResultFromList(brands));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("关联品牌")
    public HttpHeaders brandR2Categories() {
        List<BrandR2Category> brandR2Categories = bindingEntity.getBrandR2Categories();
        setModel(buildPageResultFromList(brandR2Categories));
        return buildDefaultHttpHeaders();
    }
}