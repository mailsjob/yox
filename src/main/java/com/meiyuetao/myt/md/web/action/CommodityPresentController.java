package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.CommodityPresent;
import com.meiyuetao.myt.md.service.CommodityPresentService;

@MetaData("CommodityPresentController")
public class CommodityPresentController extends MytBaseController<CommodityPresent, Long> {

    @Autowired
    private CommodityPresentService commodityPresentService;

    @Override
    protected BaseService<CommodityPresent, Long> getEntityService() {
        return commodityPresentService;
    }

    @Override
    protected void checkEntityAclPermission(CommodityPresent entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    protected void setupDetachedBindingEntity(Long id) {
        super.setupDetachedBindingEntity(id);
        // hack两个同类型变量由于指向同一个hibernate实例导致的struts覆盖更新问题
        bindingEntity.setCommodity(null);
        bindingEntity.setPresentCommodity(null);
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}