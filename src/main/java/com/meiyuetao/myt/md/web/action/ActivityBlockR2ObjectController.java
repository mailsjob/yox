package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.ActivityBlockR2Object;
import com.meiyuetao.myt.md.service.ActivityBlockR2ObjectService;

@MetaData("ActivityBlockR2ObjectController")
public class ActivityBlockR2ObjectController extends MytBaseController<ActivityBlockR2Object, Long> {

    @Autowired
    private ActivityBlockR2ObjectService activityBlockR2ObjectService;

    @Override
    protected BaseService<ActivityBlockR2Object, Long> getEntityService() {
        return activityBlockR2ObjectService;
    }

    @Override
    protected void checkEntityAclPermission(ActivityBlockR2Object entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}