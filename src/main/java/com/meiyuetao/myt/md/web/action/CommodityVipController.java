package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.service.CommodityService;

@MetaData("CommodityVipController")
public class CommodityVipController extends MytBaseController<Commodity, Long> {

    protected final Logger logger = LoggerFactory.getLogger(CommodityVipController.class);

    @Autowired
    protected CommodityService commodityService;

    @Override
    protected BaseService<Commodity, Long> getEntityService() {
        return commodityService;
    }

    @Override
    protected void checkEntityAclPermission(Commodity entity) {

    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}