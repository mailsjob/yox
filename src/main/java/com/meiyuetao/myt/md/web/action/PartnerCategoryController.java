package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.PartnerCategory;
import com.meiyuetao.myt.md.service.PartnerCategoryService;

@MetaData("PartnerCategoryController")
public class PartnerCategoryController extends MytBaseController<PartnerCategory, Long> {

    @Autowired
    private PartnerCategoryService partnerCategoryService;

    @Override
    protected BaseService<PartnerCategory, Long> getEntityService() {
        return partnerCategoryService;
    }

    @Override
    protected void checkEntityAclPermission(PartnerCategory entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData(value = "保存")
    public HttpHeaders doSave() {
        bindingEntity.setPartner(getLogonPartner());
        return super.doSave();
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        if (groupPropertyFilter.isEmpty()) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "parent.id", true));
        }
        super.appendFilterProperty(groupPropertyFilter);
    }

    @Override
    @MetaData(value = "查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData(value = "树形表格数据")
    public HttpHeaders treeGridData() {
        setModel(buildPageResultFromList(partnerCategoryService.findAllSorted(getLogonPartner())));
        return buildDefaultHttpHeaders();
    }

}