package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Ad;
import com.meiyuetao.myt.md.service.AdService;

@MetaData("AdController")
public class AdController extends MytBaseController<Ad, Long> {

    @Autowired
    private AdService adService;

    @Override
    protected BaseService<Ad, Long> getEntityService() {
        return adService;
    }

    @Override
    protected void checkEntityAclPermission(Ad entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}