package com.meiyuetao.myt.md.web.action;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lab.s2jh.auth.dao.UserDao;
import lab.s2jh.auth.entity.User;
import lab.s2jh.auth.service.UserService;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.google.common.collect.Maps;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.PersonalMessage;
import com.meiyuetao.myt.md.entity.PersonalMessageItem;
import com.meiyuetao.myt.md.service.PersonalMessageItemService;
import com.meiyuetao.myt.md.service.PersonalMessageService;

@MetaData("个人消息管理")
public class PersonalMessageController extends MytBaseController<PersonalMessage, Long> {

    @Autowired
    private PersonalMessageService personalMessageService;
    @Autowired
    private PersonalMessageItemService personalMessageItemService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    @Override
    protected BaseService<PersonalMessage, Long> getEntityService() {
        return personalMessageService;
    }

    @Override
    protected void checkEntityAclPermission(PersonalMessage entity) {
        // TODO Add acl check code logic
    }

    @Override
    public String isDisallowUpdate() {
        if (bindingEntity.isNotNew()) {
            GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
            groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "personalMessage", bindingEntity));
            List<PersonalMessageItem> personalMessageItems = personalMessageItemService.findByFilters(groupPropertyFilter);
            if (personalMessageItems.size() > 0) {
                return "已发送不能修改";
            }
        }
        return null;
    }

    @MetaData("发布")
    public HttpHeaders publishPage() {
        return buildDefaultHttpHeaders("inputBasic");
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        personalMessageService.publish(bindingEntity);
        setModel(OperationResult.buildSuccessResult("消息发布成功"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    public HttpHeaders items() {
        List<PersonalMessageItem> items = bindingEntity.getPersonalMessageItems();
        if (BooleanUtils.toBoolean(getParameter("clone"))) {
            for (PersonalMessageItem item : items) {
                item.resetCommonProperties();
            }
        }
        setModel(buildPageResultFromList(items));
        return buildDefaultHttpHeaders();
    }

    public Map<String, String> getTargetUsersMap() {
        Map<String, String> usersMap = new LinkedHashMap<String, String>();
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "enabled", Boolean.TRUE));
        Iterable<User> users = userService.findByFilters(groupPropertyFilter, new Sort(Direction.ASC, "signinid"));
        Iterator<User> it = users.iterator();
        while (it.hasNext()) {
            User user = it.next();
            usersMap.put(user.getDisplay(), user.getDisplay());
        }
        return usersMap;
    }

    public HttpHeaders users() {
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "enabled", Boolean.TRUE));
        Iterable<User> users = userService.findByFilters(groupPropertyFilter);
        Map<String, Object> data = Maps.newHashMap();
        for (User user : users) {
            data.put(user.getId().toString(), user.getSigninid());
        }
        setModel(data);
        return buildDefaultHttpHeaders();
    }
}