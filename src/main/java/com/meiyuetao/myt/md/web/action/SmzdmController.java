package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Smzdm;
import com.meiyuetao.myt.md.service.SmzdmService;

@MetaData("SmzdmController")
public class SmzdmController extends MytBaseController<Smzdm, Long> {

    @Autowired
    private SmzdmService smzdmService;

    @Override
    protected BaseService<Smzdm, Long> getEntityService() {
        return smzdmService;
    }

    @Override
    protected void checkEntityAclPermission(Smzdm entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}