package com.meiyuetao.myt.md.web.action;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.*;
import com.meiyuetao.myt.md.service.FloorService;
import com.meiyuetao.myt.md.service.RegionService;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import org.apache.struts2.rest.HttpHeaders;
import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@MetaData("FloorController")
public class FloorController extends MytBaseController<Floor, Long> {

    @Autowired
    private FloorService floorService;

    @Autowired
    private RegionService regionService;

    @Override
    protected BaseService<Floor, Long> getEntityService() {
        return floorService;
    }

    @Override
    protected void checkEntityAclPermission(Floor entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        /*
         * for (Iterator<FloorAd> floorAds =
         * bindingEntity.getFloorAds().iterator(); floorAds.hasNext();) {
         * FloorAd item = floorAds.next(); if (item.isMarkedRemove()) {
         * floorAds.remove(); }
         * 
         * } for (Iterator<FloorMenu> floorMenus =
         * bindingEntity.getFloorMenus().iterator(); floorMenus.hasNext();) {
         * FloorMenu item = floorMenus.next(); if (item.isMarkedRemove()) {
         * floorMenus.remove(); }
         * 
         * }
         */
        String regionId = super.getParameter("region.id");
        String regionDis = super.getParameter("region.display");
        Region region = regionService.findOne(Long.valueOf(regionId));
        if (StringUtils.isNullOrEmpty(regionDis)) {
            region = null;
        }
        bindingEntity.setRegion(region);
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("楼层菜单数据")
    public HttpHeaders floorMenus() {
        List<FloorMenu> floorMenus = bindingEntity.getFloorMenus();
        setModel(buildPageResultFromList(floorMenus));
        return buildDefaultHttpHeaders();
    }

    @MetaData("楼层广告数据")
    public HttpHeaders floorAds() {
        List<FloorAd> floorAds = bindingEntity.getFloorAds();
        setModel(buildPageResultFromList(floorAds));
        return buildDefaultHttpHeaders();
    }

    @MetaData("楼层广告数据")
    public HttpHeaders recommendCommodities() {
        List<RecommendCommodity> recommendCommodities = bindingEntity.getRecommendCommodities();
        setModel(buildPageResultFromList(recommendCommodities));
        return buildDefaultHttpHeaders();
    }

}