package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Theme;
import com.meiyuetao.myt.md.service.ThemeService;

@MetaData("ThemeController")
public class ThemeController extends MytBaseController<Theme, Long> {

    @Autowired
    private ThemeService themeService;

    @Override
    protected BaseService<Theme, Long> getEntityService() {
        return themeService;
    }

    @Override
    protected void checkEntityAclPermission(Theme entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    public HttpHeaders themeDetails() {
        setModel(buildPageResultFromList(bindingEntity.getThemeDetails()));
        return buildDefaultHttpHeaders();
    }

}