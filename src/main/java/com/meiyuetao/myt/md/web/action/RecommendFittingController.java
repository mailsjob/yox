package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.RecommendFitting;
import com.meiyuetao.myt.md.service.CategoryService;
import com.meiyuetao.myt.md.service.CommodityService;
import com.meiyuetao.myt.md.service.RecommendFittingService;

@MetaData("RecommendFittingController")
public class RecommendFittingController extends MytBaseController<RecommendFitting, Long> {

    @Autowired
    private RecommendFittingService recommendFittingService;
    @Autowired
    private CommodityService commodityService;
    @Autowired
    private CategoryService categoryService;

    @Override
    protected void checkEntityAclPermission(RecommendFitting entity) {
        // TODO Add acl check code logic
    }

    @Override
    protected BaseService<RecommendFitting, Long> getEntityService() {
        // TODO Auto-generated method stub
        return recommendFittingService;
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}