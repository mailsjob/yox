package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.ActivityR2Object;
import com.meiyuetao.myt.md.service.ActivityR2ObjectService;

@MetaData("ActivityR2ObjectController")
public class ActivityR2ObjectController extends MytBaseController<ActivityR2Object, Long> {

    @Autowired
    private ActivityR2ObjectService activityR2ObjectService;

    @Override
    protected BaseService<ActivityR2Object, Long> getEntityService() {
        return activityR2ObjectService;
    }

    @Override
    protected void checkEntityAclPermission(ActivityR2Object entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}