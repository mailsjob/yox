package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.CommodityCirclePresent;
import com.meiyuetao.myt.md.service.CommodityCirclePresentService;

@MetaData("CommodityCirclePresentController")
public class CommodityCirclePresentController extends MytBaseController<CommodityCirclePresent, Long> {

    @Autowired
    private CommodityCirclePresentService commodityCirclePresentService;

    @Override
    protected BaseService<CommodityCirclePresent, Long> getEntityService() {
        return commodityCirclePresentService;
    }

    @Override
    protected void checkEntityAclPermission(CommodityCirclePresent entity) {
        // TODO Add acl check code logic
    }

    @Override
    protected void setupDetachedBindingEntity(Long id) {
        super.setupDetachedBindingEntity(id);
        // hack两个同类型变量由于指向同一个hibernate实例导致的struts覆盖更新问题
        bindingEntity.setCommodity(null);
        bindingEntity.setPresentCommodity(null);
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}