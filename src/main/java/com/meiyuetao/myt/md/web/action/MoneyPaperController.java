package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.MoneyPaper;
import com.meiyuetao.myt.md.service.MoneyPaperService;

@MetaData("MoneyPaperController")
public class MoneyPaperController extends MytBaseController<MoneyPaper, Long> {

    @Autowired
    private MoneyPaperService moneyPaperService;

    @Override
    protected BaseService<MoneyPaper, Long> getEntityService() {
        return moneyPaperService;
    }

    @Override
    protected void checkEntityAclPermission(MoneyPaper entity) {
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    public void prepareEdit() {
        super.prepareEdit();
        if (bindingEntity.isNew()) {
            String code = RandomStringUtils.randomAlphabetic(12).toUpperCase();
            while (moneyPaperService.findByProperty("code", code) != null) {
                code = RandomStringUtils.randomAlphabetic(12).toUpperCase();
            }

            bindingEntity.setCode(code);
            String password = RandomStringUtils.randomAlphanumeric(16).toUpperCase();
            bindingEntity.setPassword(password);
        }
    }

}