package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.GiftPaperObject;
import com.meiyuetao.myt.md.service.GiftPaperObjectService;

@MetaData("GiftPaperObjectController")
public class GiftPaperObjectController extends MytBaseController<GiftPaperObject, Long> {

    @Autowired
    private GiftPaperObjectService giftPaperObjectService;

    @Override
    protected BaseService<GiftPaperObject, Long> getEntityService() {
        return giftPaperObjectService;
    }

    @Override
    protected void checkEntityAclPermission(GiftPaperObject entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}