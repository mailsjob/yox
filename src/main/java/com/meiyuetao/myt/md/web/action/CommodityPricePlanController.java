package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.service.Validation;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.CommodityPricePlan;
import com.meiyuetao.myt.md.service.CommodityPricePlanService;
import com.meiyuetao.myt.md.service.CommodityService;

@MetaData("商品价格计划表管理")
public class CommodityPricePlanController extends BaseController<CommodityPricePlan, Long> {

    @Autowired
    private CommodityPricePlanService commodityPricePlanService;
    @Autowired
    private CommodityService commodityService;

    @Override
    protected BaseService<CommodityPricePlan, Long> getEntityService() {
        return commodityPricePlanService;
    }

    @Override
    protected void checkEntityAclPermission(CommodityPricePlan entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.isNew()) {
            Commodity commodity = commodityService.findOne(Long.valueOf(getParameter("commodity.id")));
            GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
            groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "commodity", commodity));
            Long sizes = commodityPricePlanService.count(groupPropertyFilter);
            Validation.isTrue(sizes < 8, "最多维护8条数据");
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}