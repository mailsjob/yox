package com.meiyuetao.myt.md.web.action;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.service.BrandService;
import com.meiyuetao.myt.md.service.CategoryService;
import com.meiyuetao.myt.md.service.CommodityService;
import com.meiyuetao.myt.partner.entity.Partner;
import com.meiyuetao.myt.partner.service.PartnerService;

@MetaData("MgmtCommodityController")
public class MgmtCommodityController extends MytBaseController<Commodity, Long> {

    protected final Logger logger = LoggerFactory.getLogger(MgmtCommodityController.class);

    @Autowired
    private CommodityService commodityService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private PartnerService partnerService;

    @Override
    protected BaseService<Commodity, Long> getEntityService() {
        return commodityService;
    }

    @Override
    protected void checkEntityAclPermission(Commodity entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    public Map<Long, String> getPartnerMap() {
        Map<Long, String> partnerMap = new HashMap<Long, String>();
        Iterator<Partner> partners = partnerService.findAll().iterator();
        while (partners.hasNext()) {
            Partner partner = (Partner) partners.next();
            partnerMap.put(partner.getId(), "[" + partner.getCode() + "]" + partner.getCompanyName());
        }
        return partnerMap;
    }
}