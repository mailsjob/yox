package com.meiyuetao.myt.md.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.md.entity.ThemeComment;
import com.meiyuetao.myt.md.service.ThemeCommentService;

@MetaData("ThemeCommentController")
public class ThemeCommentController extends MytBaseController<ThemeComment, Long> {

    @Autowired
    private ThemeCommentService themeCommentService;

    @Override
    protected BaseService<ThemeComment, Long> getEntityService() {
        return themeCommentService;
    }

    @Override
    protected void checkEntityAclPermission(ThemeComment entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}