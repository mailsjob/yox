package com.meiyuetao.myt.md.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Box;

@Repository
public interface BoxDao extends BaseDao<Box, Long> {

    @Query("from Box order by id desc 1")
    Box getTop();

}