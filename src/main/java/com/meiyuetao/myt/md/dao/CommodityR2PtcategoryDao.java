package com.meiyuetao.myt.md.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.CommodityR2Ptcategory;

@Repository
public interface CommodityR2PtcategoryDao extends BaseDao<CommodityR2Ptcategory, Long> {

}