package com.meiyuetao.myt.md.dao;

import com.meiyuetao.myt.md.entity.CommodityRentalPrice;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface CommodityRentalPriceDao extends BaseDao<CommodityRentalPrice, Long> {

}