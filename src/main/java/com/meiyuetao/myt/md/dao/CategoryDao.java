package com.meiyuetao.myt.md.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Category;

@Repository
public interface CategoryDao extends BaseDao<Category, Long> {
    @Query("from Category")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    public List<Category> findAllCached();
}