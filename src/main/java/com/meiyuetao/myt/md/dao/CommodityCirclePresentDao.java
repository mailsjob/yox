package com.meiyuetao.myt.md.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.CommodityCirclePresent;

@Repository
public interface CommodityCirclePresentDao extends BaseDao<CommodityCirclePresent, Long> {

    List<CommodityCirclePresent> findByCommodity(Commodity commodity);
}