package com.meiyuetao.myt.md.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Region;

@Repository
public interface RegionDao extends BaseDao<Region, Long> {

    @Query("from Region order by pinyinShort ASC")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    public List<Region> findAllCached();

    @Query("from Region re where re.name like :name and re.id < 36")
    public Region findByName(@Param("name") String name);
}