package com.meiyuetao.myt.md.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.CommoditySaleDateCount;

@Repository
public interface CommoditySaleDateCountDao extends BaseDao<CommoditySaleDateCount, Long> {

}