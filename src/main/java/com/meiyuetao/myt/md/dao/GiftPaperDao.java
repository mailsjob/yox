package com.meiyuetao.myt.md.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.GiftPaper;

@Repository
public interface GiftPaperDao extends BaseDao<GiftPaper, Long> {
    List<GiftPaper> findByOrderSeq(String orderSeq);

    @Query("from GiftPaper where paperMode='G'")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    List<GiftPaper> findPaperTemplates();

    List<GiftPaper> findByCode(String code);
}