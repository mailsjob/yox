package com.meiyuetao.myt.md.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.BrandR2Category;

@Repository
public interface BrandR2CategoryDao extends BaseDao<BrandR2Category, Long> {

}