package com.meiyuetao.myt.md.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.PartnerCategory;
import com.meiyuetao.myt.partner.entity.Partner;

@Repository
public interface PartnerCategoryDao extends BaseDao<PartnerCategory, Long> {

    @Query("from PartnerCategory where partner=?")
    public List<PartnerCategory> findByPartner(Partner partner);
}