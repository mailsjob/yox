package com.meiyuetao.myt.md.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.ActivityR2Object;

@Repository
public interface ActivityR2ObjectDao extends BaseDao<ActivityR2Object, Long> {

}