package com.meiyuetao.myt.md.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.BoxR2Commodity;

@Repository
public interface BoxR2CommodityDao extends BaseDao<BoxR2Commodity, Long> {

}