package com.meiyuetao.myt.md.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Commodity;

@Repository
public interface CommodityDao extends BaseDao<Commodity, Long> {
    Commodity findBySku(String sku);

    Commodity findByTitle(String title);

    Commodity findByErpProductId(String erpProductId);
}