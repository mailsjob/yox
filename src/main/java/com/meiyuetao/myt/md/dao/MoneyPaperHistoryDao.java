package com.meiyuetao.myt.md.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.MoneyPaperHistory;
import com.meiyuetao.myt.sale.entity.BoxOrder;

@Repository
public interface MoneyPaperHistoryDao extends BaseDao<MoneyPaperHistory, Long> {
    List<MoneyPaperHistory> findByBoxOrder(BoxOrder boxOrder);
}