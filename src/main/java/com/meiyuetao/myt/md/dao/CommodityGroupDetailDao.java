package com.meiyuetao.myt.md.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.CommodityGroupDetail;

@Repository
public interface CommodityGroupDetailDao extends BaseDao<CommodityGroupDetail, Long> {
    /*
     * @Query(" from CommodityGroupDetail cgd " +
     * "where cgd.commodityGroup=:commodityGroup and cgd.propLineValX is not null "
     * + "#if(:isExistPropLineValY) and cgd.propLineValX is not null" +
     * " #else and cgd.propLineValX is null #end") List<CommodityGroupDetail>
     * findCGDArray(@Param("commodityGroup") CommodityGroup commodityGroup,
     * 
     * @Param("isExistPropLineValY") boolean isExistPropLineValY);
     */
}