package com.meiyuetao.myt.md.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.CommodityPrice;

@Repository
public interface CommodityPriceDao extends BaseDao<CommodityPrice, Long> {
    List<CommodityPrice> findByCommodity(Commodity commodity);
}