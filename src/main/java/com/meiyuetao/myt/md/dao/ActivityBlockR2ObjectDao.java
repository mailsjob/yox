package com.meiyuetao.myt.md.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.ActivityBlockR2Object;

@Repository
public interface ActivityBlockR2ObjectDao extends BaseDao<ActivityBlockR2Object, Long> {

}