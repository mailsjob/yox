package com.meiyuetao.myt.md.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.md.entity.CommodityViewStat;

@Repository
public interface CommodityViewStatDao extends BaseDao<CommodityViewStat, Long> {

    @Query("FROM CommodityViewStat WHERE customerProfile=:c ORDER BY lastViewTime")
    List<CommodityViewStat> findHistory(@Param("c") CustomerProfile c);
}