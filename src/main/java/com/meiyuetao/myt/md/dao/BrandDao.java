package com.meiyuetao.myt.md.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.md.entity.Brand;

@Repository
public interface BrandDao extends BaseDao<Brand, Long> {

    @Query("from Brand order by title asc")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    public List<Brand> findAllCached();
}