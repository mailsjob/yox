package com.meiyuetao.myt.md.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("")
@Entity
@Table(name = "iyb_commodity_view_stat")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class CommodityViewStat extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("最后行为ID")
    private String lastBehaveLogId;
    @MetaData("最后查看时间")
    private Date lastViewTime;
    @MetaData("查看数")
    private Integer viewCount;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("客户")
    private CustomerProfile customerProfile;

    @Column(length = 512)
    public String getLastBehaveLogId() {
        return lastBehaveLogId;
    }

    public void setLastBehaveLogId(String lastBehaveLogId) {
        this.lastBehaveLogId = lastBehaveLogId;
    }

    public Date getLastViewTime() {
        return lastViewTime;
    }

    public void setLastViewTime(Date lastViewTime) {
        this.lastViewTime = lastViewTime;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    @ManyToOne
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @ManyToOne
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

}
