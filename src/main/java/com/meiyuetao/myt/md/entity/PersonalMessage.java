package com.meiyuetao.myt.md.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lab.s2jh.auth.entity.User;
import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("个人消息")
@Entity
@Table(name = "iyb_personal_message")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class PersonalMessage extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("标题 ")
    private String title;
    @MetaData("发布用户")
    private User publishUser;
    @MetaData("发布时间")
    private Date publishTime;
    @MetaData("通知内容")
    private String contents;
    private String targetUsers;
    @MetaData("发送记录")
    private List<PersonalMessageItem> personalMessageItems = Lists.newArrayList();

    @Column(length = 500, nullable = false)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @JsonProperty
    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "PUBLISH_USER_SID", nullable = true)
    @JsonProperty
    public User getPublishUser() {
        return publishUser;
    }

    public void setPublishUser(User publishUser) {
        this.publishUser = publishUser;
    }

    @Column(nullable = true)
    @JsonProperty
    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    @OneToMany(mappedBy = "personalMessage", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<PersonalMessageItem> getPersonalMessageItems() {
        return personalMessageItems;
    }

    public void setPersonalMessageItems(List<PersonalMessageItem> personalMessageItems) {
        this.personalMessageItems = personalMessageItems;
    }

    @JsonProperty
    public String getTargetUsers() {
        return targetUsers;
    }

    public void setTargetUsers(String targetUsers) {
        this.targetUsers = targetUsers;
    }

}