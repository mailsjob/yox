package com.meiyuetao.myt.md.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("集合")
@Entity
@Table(name = "iyb_collection")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Collection extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("标题")
    private String title;
    @MetaData("备注")
    private String memo;
    @MetaData("是否可用")
    private Boolean enable;
    @MetaData("编码")
    private String code;

    @MetaData("集合对象")
    private List<CollectionObject> collectionObjects = Lists.newArrayList();

    @Column(length = 64, nullable = false)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 128)
    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Column(name = "is_enable", nullable = false)
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @Column(length = 32, nullable = false)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @OneToMany(mappedBy = "collection", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<CollectionObject> getCollectionObjects() {
        return collectionObjects;
    }

    public void setCollectionObjects(List<CollectionObject> collectionObjects) {
        this.collectionObjects = collectionObjects;
    }

    @Transient
    @JsonProperty
    public String getDisplay() {
        return this.getTitle();
    }
}
