package com.meiyuetao.myt.md.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lab.s2jh.core.annotation.MetaData;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.partner.entity.Partner;

@MetaData("商家分类")
@Entity
@Table(name = "iyb_ptcategory", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "PARENT_SID" }))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class PartnerCategory extends MytBaseEntity implements Comparable<PartnerCategory> {

    private static final long serialVersionUID = 1L;
    @MetaData(value = "名称")
    private String name;

    @MetaData(value = "父节点")
    private PartnerCategory parent;

    @MetaData(value = "排序号", tooltips = "相对排序号，数字越大越靠下显示")
    private Integer orderIndex = 100;

    @MetaData(value = "所属合作伙伴")
    private Partner partner;

    private List<CommodityR2Ptcategory> commodityR2Ptcategories = new ArrayList<CommodityR2Ptcategory>();

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "PARENT_SID")
    @JsonIgnore
    public PartnerCategory getParent() {
        return parent;
    }

    public void setParent(PartnerCategory parent) {
        this.parent = parent;
    }

    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @ManyToOne
    @JoinColumn(name = "Partner_SID", nullable = false)
    @JsonProperty
    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    @Override
    public int compareTo(PartnerCategory o) {
        return CompareToBuilder.reflectionCompare(this.getOrderIndex(), o.getOrderIndex());
    }

    @OneToMany(mappedBy = "partnerCategory", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    public List<CommodityR2Ptcategory> getCommodityR2Ptcategories() {
        return commodityR2Ptcategories;
    }

    public void setCommodityR2Ptcategories(List<CommodityR2Ptcategory> commodityR2Ptcategories) {
        this.commodityR2Ptcategories = commodityR2Ptcategories;
    }

}
