package com.meiyuetao.myt.md.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("商品赠品配置")
@Entity
@Table(name = "iyb_commodity_present", uniqueConstraints = @UniqueConstraint(columnNames = { "commodity_sid", "present_commodity_sid" }))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityPresent extends MytBaseEntity {
    private static final long serialVersionUID = 1L;

    @MetaData("正品")
    private Commodity commodity;

    @MetaData("赠品")
    private Commodity presentCommodity;

    @MetaData("赠品数量")
    private Integer quantity;
    @MetaData("开始时间")
    private Date beginDate;
    @MetaData("结束时间")
    private Date endDate;
    @MetaData("可用")
    private Boolean enable = Boolean.TRUE;
    @MetaData("是否叠加赠送")
    private Boolean canStacked = Boolean.FALSE;
    @MetaData("支持类型")
    private GivenTypeEnum givenType = GivenTypeEnum.NOW_BUY;

    public enum GivenTypeEnum {
        @MetaData("预定")
        RESERVATION, @MetaData("现货")
        NOW_BUY, @MetaData("全部 ")
        ALL;

    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "present_commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getPresentCommodity() {
        return presentCommodity;
    }

    public void setPresentCommodity(Commodity presentCommodity) {
        this.presentCommodity = presentCommodity;
    }

    @JsonProperty
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonProperty
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    @JsonProperty
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @JsonProperty
    @Column(name = "is_enable")
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @JsonProperty
    @Column(name = "can_stacked")
    public Boolean getCanStacked() {
        return canStacked;
    }

    public void setCanStacked(Boolean canStacked) {
        this.canStacked = canStacked;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32)
    @JsonProperty
    public GivenTypeEnum getGivenType() {
        return givenType;
    }

    public void setGivenType(GivenTypeEnum givenType) {
        this.givenType = givenType;
    }

}
