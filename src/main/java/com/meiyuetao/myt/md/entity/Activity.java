package com.meiyuetao.myt.md.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.activity.entity.ActivityRules;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("活动")
@Entity
@Table(name = "iyb_activity")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Activity extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("活动名称")
    private String activityName;
    @MetaData("活动文本")
    private String activityHtml;
    @MetaData("活动代码")
    private String activityCode;
    @MetaData("活动图片")
    private String img;
    @MetaData("开始时间")
    private Date beginTime;
    @MetaData("结束时间")
    private Date endTime;
    private List<ActivityR2Object> activityR2Objects = new ArrayList<ActivityR2Object>();
    private List<ActivityBlock> activityBlocks = new ArrayList<ActivityBlock>();
    private List<ActivityRules> activityRules = new ArrayList<ActivityRules>();

    @Column(length = 64, nullable = false)
    @JsonProperty
    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    @Lob
    public String getActivityHtml() {
        return activityHtml;
    }

    public void setActivityHtml(String activityHtml) {
        this.activityHtml = activityHtml;
    }

    @Column(length = 32, nullable = false, updatable = false)
    @JsonProperty
    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    @JsonProperty
    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @JsonProperty
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    @JsonProperty
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @OneToMany(mappedBy = "activity", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ActivityR2Object> getActivityR2Objects() {
        return activityR2Objects;
    }

    public void setActivityR2Objects(List<ActivityR2Object> activityR2Objects) {
        this.activityR2Objects = activityR2Objects;
    }

    @Override
    @Transient
    @JsonProperty
    public String getDisplay() {

        return activityCode + " " + activityName;

    }

    @OneToMany(mappedBy = "activity", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ActivityBlock> getActivityBlocks() {
        return activityBlocks;
    }

    public void setActivityBlocks(List<ActivityBlock> activityBlocks) {
        this.activityBlocks = activityBlocks;
    }

    @OneToMany(mappedBy = "activity", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ActivityRules> getActivityRules() {
        return activityRules;
    }

    public void setActivityRules(List<ActivityRules> activityRules) {
        this.activityRules = activityRules;
    }

}
