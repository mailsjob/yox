package com.meiyuetao.myt.md.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("楼层广告")
@Entity
@Table(name = "iyb_floor_ad")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FloorAd extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "楼层")
    @EntityAutoCode(order = 10, search = true)
    private Floor floor;

    @MetaData(value = "名称")
    @EntityAutoCode(order = 10, search = true)
    private String name;

    @MetaData(value = "楼层下广告方位")
    @EntityAutoCode(order = 10, search = true)
    private FloorAdPositionEnum position;

    public enum FloorAdPositionEnum {

        @MetaData("左")
        LEFT, @MetaData("右")
        RIGHT,

        @MetaData("移动端左")
        MOBLELEFT, @MetaData("移动端右")
        MOBLERIGHT;
    }

    @MetaData(value = "排序号")
    @EntityAutoCode(order = 10, search = true)
    private Integer orderIndex = 0;

    @MetaData(value = "图片")
    @EntityAutoCode(order = 10, search = true)
    private String pic;

    @MetaData(value = "跳转目标")
    @EntityAutoCode(order = 10, search = true)
    private String target;

    @MetaData(value = "广告类型")
    @EntityAutoCode(order = 10, search = true)
    private FloorAdTypeEnum adType = FloorAdTypeEnum.COMMODITY;
    private String bgPic;

    public enum FloorAdTypeEnum {

        @MetaData("商品")
        COMMODITY,

        @MetaData("主题清单")
        THEME,

        @MetaData("每日推荐(什么值得买)")
        SMZDM,

        @MetaData("活动")
        ACTIVITY,

        @MetaData("超链接")
        URL;
    }

    @MetaData(value = "是否可用")
    @EntityAutoCode(order = 10, search = true)
    private Boolean enable = Boolean.FALSE;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "floor_sid", nullable = false)
    @JsonProperty
    public Floor getFloor() {
        return floor;
    }

    public void setFloor(Floor floor) {
        this.floor = floor;
    }

    @Column(nullable = false)
    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 50, nullable = false)
    @JsonProperty
    public FloorAdPositionEnum getPosition() {
        return position;
    }

    public void setPosition(FloorAdPositionEnum position) {
        this.position = position;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @Column(nullable = false)
    @JsonProperty
    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 50, nullable = false)
    @JsonProperty
    public FloorAdTypeEnum getAdType() {
        return adType;
    }

    public void setAdType(FloorAdTypeEnum adType) {
        this.adType = adType;
    }

    @Column(name = "is_enable")
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @JsonProperty
    public String getBgPic() {
        return bgPic;
    }

    public void setBgPic(String bgPic) {
        this.bgPic = bgPic;
    }

}
