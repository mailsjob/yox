package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

/**
 * Auto Generate Javadoc
 */
@MetaData("商品评论")
@Entity
@Table(name = "iyb_commodity_comment")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class CommodityComment extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("评论标题")
    private String title;
    @MetaData("优点")
    private String advantage;
    @MetaData("缺点")
    private String disadvantage;
    @MetaData("心得")
    private String gainedKnowledge;
    @MetaData("晒单贴的sid")
    private Long showSid;

    @MetaData("晒单图片数")
    private Integer showPicCount = 0;

    @MetaData("晒单图片url(快照)，用逗号分割")
    private String showPicUrls;

    @MetaData("回复数")
    private Integer replyCount = 0;

    @MetaData("评价值，好评5分和4分，中评3分和2分，差评1分")
    private BigDecimal evalValue;

    private Integer q0Value;
    private Integer q1Value;
    private Integer q2Value;
    private Integer q3Value;
    private Integer q4Value;
    private Integer q5Value;
    private Integer q6Value;
    private Integer q7Value;
    private Integer q8Value;
    private Integer q9Value;
    private Integer q10Value;
    private Integer q11Value;
    private Integer q12Value;
    private Integer q13Value;
    private Integer q14Value;

    @MetaData("有用数")
    private Integer usefulCount = 0;
    @MetaData("无用数")
    private Integer uselessCount = 0;
    @MetaData("客户")
    private CustomerProfile customerProfile;

    @MetaData("购买时间")
    private Date boughtTime;

    @MetaData("发布时间")
    private Date publishTime;

    @MetaData("冗余评论内容")
    private String commentContent;
    @MetaData("父评论")
    private CommodityComment parent;

    private List<CommodityComment> children;
    @MetaData("商品评论来源")
    private String commentFrom;

    @MetaData("仅供外部用户显示用")
    private String displayName;
    @MetaData("是否可用")
    private Boolean enable = Boolean.TRUE;

    @MetaData("标识评论唯一性字段：评论用户名+评论时间")
    private String identification;

    @ManyToOne
    @JoinColumn(name = "commodity_sid", nullable = false)
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @Column(nullable = false)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public String getAdvantage() {
        return advantage;
    }

    public void setAdvantage(String advantage) {
        this.advantage = advantage;
    }

    @JsonProperty
    public String getDisadvantage() {
        return disadvantage;
    }

    public void setDisadvantage(String disadvantage) {
        this.disadvantage = disadvantage;
    }

    @JsonProperty
    public String getGainedKnowledge() {
        return gainedKnowledge;
    }

    public void setGainedKnowledge(String gainedKnowledge) {
        this.gainedKnowledge = gainedKnowledge;
    }

    public Long getShowSid() {
        return showSid;
    }

    public void setShowSid(Long showSid) {
        this.showSid = showSid;
    }

    @Column(nullable = false)
    public Integer getShowPicCount() {
        return showPicCount;
    }

    public void setShowPicCount(Integer showPicCount) {
        this.showPicCount = showPicCount;
    }

    @Column(length = 1540)
    public String getShowPicUrls() {
        return showPicUrls;
    }

    public void setShowPicUrls(String showPicUrls) {
        this.showPicUrls = showPicUrls;
    }

    @Column(nullable = false)
    public Integer getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(Integer replyCount) {
        this.replyCount = replyCount;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getEvalValue() {
        return evalValue;
    }

    public void setEvalValue(BigDecimal evalValue) {
        this.evalValue = evalValue;
    }

    @Column(name = "q0_value")
    public Integer getQ0Value() {
        return q0Value;
    }

    public void setQ0Value(Integer q0Value) {
        this.q0Value = q0Value;
    }

    @Column(name = "q1_value")
    public Integer getQ1Value() {
        return q1Value;
    }

    public void setQ1Value(Integer q1Value) {
        this.q1Value = q1Value;
    }

    @Column(name = "q2_value")
    public Integer getQ2Value() {
        return q2Value;
    }

    public void setQ2Value(Integer q2Value) {
        this.q2Value = q2Value;
    }

    @Column(name = "q3_value")
    public Integer getQ3Value() {
        return q3Value;
    }

    public void setQ3Value(Integer q3Value) {
        this.q3Value = q3Value;
    }

    @Column(name = "q4_value")
    public Integer getQ4Value() {
        return q4Value;
    }

    public void setQ4Value(Integer q4Value) {
        this.q4Value = q4Value;
    }

    @Column(name = "q5_value")
    public Integer getQ5Value() {
        return q5Value;
    }

    public void setQ5Value(Integer q5Value) {
        this.q5Value = q5Value;
    }

    @Column(name = "q6_value")
    public Integer getQ6Value() {
        return q6Value;
    }

    public void setQ6Value(Integer q6Value) {
        this.q6Value = q6Value;
    }

    @Column(name = "q7_value")
    public Integer getQ7Value() {
        return q7Value;
    }

    public void setQ7Value(Integer q7Value) {
        this.q7Value = q7Value;
    }

    @Column(name = "q8_value")
    public Integer getQ8Value() {
        return q8Value;
    }

    public void setQ8Value(Integer q8Value) {
        this.q8Value = q8Value;
    }

    @Column(name = "q9_value")
    public Integer getQ9Value() {
        return q9Value;
    }

    public void setQ9Value(Integer q9Value) {
        this.q9Value = q9Value;
    }

    @Column(name = "q10_value")
    public Integer getQ10Value() {
        return q10Value;
    }

    public void setQ10Value(Integer q10Value) {
        this.q10Value = q10Value;
    }

    @Column(name = "q11_value")
    public Integer getQ11Value() {
        return q11Value;
    }

    public void setQ11Value(Integer q11Value) {
        this.q11Value = q11Value;
    }

    @Column(name = "q12_value")
    public Integer getQ12Value() {
        return q12Value;
    }

    public void setQ12Value(Integer q12Value) {
        this.q12Value = q12Value;
    }

    @Column(name = "q13_value")
    public Integer getQ13Value() {
        return q13Value;
    }

    public void setQ13Value(Integer q13Value) {
        this.q13Value = q13Value;
    }

    @Column(name = "q14_value")
    public Integer getQ14Value() {
        return q14Value;
    }

    public void setQ14Value(Integer q14Value) {
        this.q14Value = q14Value;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getUsefulCount() {
        return usefulCount;
    }

    public void setUsefulCount(Integer usefulCount) {
        this.usefulCount = usefulCount;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getUselessCount() {
        return uselessCount;
    }

    public void setUselessCount(Integer uselessCount) {
        this.uselessCount = uselessCount;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getBoughtTime() {
        return boughtTime;
    }

    public void setBoughtTime(Date boughtTime) {
        this.boughtTime = boughtTime;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    @JsonProperty
    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    @OneToMany(mappedBy = "parent")
    public List<CommodityComment> getChildren() {
        return children;
    }

    public void setChildren(List<CommodityComment> children) {
        this.children = children;
    }

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_sid")
    @JsonIgnore
    public CommodityComment getParent() {
        return parent;
    }

    public void setParent(CommodityComment parent) {
        this.parent = parent;
    }

    @JsonProperty
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Column(name = "is_enable", nullable = false)
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getCommentFrom() {
        return commentFrom;
    }

    public void setCommentFrom(String commentFrom) {
        this.commentFrom = commentFrom;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

}