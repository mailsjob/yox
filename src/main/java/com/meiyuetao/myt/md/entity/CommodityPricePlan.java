package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("价格计划表")
@Entity
@Table(name = "iyb_price_plan")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityPricePlan extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("商品价格计划表")
    private Commodity commodity;
    @MetaData("价格")
    private BigDecimal price;
    @MetaData("时间类型")
    private TimeTypeEnum timeType;
    @MetaData("时间偏移量")
    private Integer timeOffset;

    public static enum TimeTypeEnum {
        @MetaData("天")
        D,
        @MetaData("周")
        W,
        @MetaData("月")
        M;
    }

    @ManyToOne
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public TimeTypeEnum getTimeType() {
        return timeType;
    }

    public void setTimeType(TimeTypeEnum timeType) {
        this.timeType = timeType;
    }

    @JsonProperty
    public Integer getTimeOffset() {
        return timeOffset;
    }

    public void setTimeOffset(Integer timeOffset) {
        this.timeOffset = timeOffset;
    }

}
