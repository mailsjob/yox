package com.meiyuetao.myt.md.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.partner.entity.Partner;

@MetaData("商品组")
@Entity
@Table(name = "iyb_commodity_group")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityGroup extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("名称")
    private String name;
    @MetaData("代码")
    private String code;
    @MetaData("合作伙伴")
    private Partner partner;
    private List<CommodityGroupDetail> commodityGroupDetails = new ArrayList<CommodityGroupDetail>();

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false, length = 32, unique = true)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "partner_sid", nullable = false)
    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    @OneToMany(mappedBy = "commodityGroup", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<CommodityGroupDetail> getCommodityGroupDetails() {
        return commodityGroupDetails;
    }

    public void setCommodityGroupDetails(List<CommodityGroupDetail> commodityGroupDetails) {
        this.commodityGroupDetails = commodityGroupDetails;
    }

    @Override
    @Transient
    public String getDisplay() {

        return code + " " + name;
    }

}