package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("搜索关键词")
@Entity
@Table(name = "iyb_search_keyword")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class SearchKeyWord extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关键词")
    private String keyword;
    @MetaData("排序号")
    private Integer orderRank = 0;
    @MetaData("计数")
    private Long count = 0L;
    @MetaData("年月")
    private String yearMonth;
    @MetaData("类型")
    private SearchTypeEnum searchType = SearchTypeEnum.COMMODITY;
    @MetaData("搜索关键词")
    private Long lastRecordCount = 0L;

    public enum SearchTypeEnum {
        @MetaData("商品")
        COMMODITY, @MetaData("主题清单")
        THEME;
    }

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getOrderRank() {
        return orderRank;
    }

    public void setOrderRank(Integer orderRank) {
        this.orderRank = orderRank;
    }

    @Column(nullable = false)
    @JsonProperty
    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Column(nullable = false)
    @JsonProperty
    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

    @Column(nullable = false)
    @JsonProperty
    public Long getLastRecordCount() {
        return lastRecordCount;
    }

    public void setLastRecordCount(Long lastRecordCount) {
        this.lastRecordCount = lastRecordCount;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, name = "search_type")
    @JsonProperty
    public SearchTypeEnum getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchTypeEnum searchType) {
        this.searchType = searchType;
    }

}
