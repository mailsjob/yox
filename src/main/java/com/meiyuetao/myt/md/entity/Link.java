package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("友情链接")
@Entity
@Table(name = "iyb_link")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Link extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("名称")
    private String name;
    @MetaData("链接")
    private String url;
    @MetaData("图片")
    private String pic;
    @MetaData("排序号")
    private Integer orderIndex = 0;
    @MetaData("可用")
    private Boolean enable = Boolean.TRUE;

    private String xmlStr1;
    private String xmlStr2;
    private String errorStr;

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty
    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @JsonProperty
    @Column(name = "is_enable")
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getXmlStr1() {
        return xmlStr1;
    }

    public void setXmlStr1(String xmlStr1) {
        this.xmlStr1 = xmlStr1;
    }

    @Lob
    public String getXmlStr2() {
        return xmlStr2;
    }

    public void setXmlStr2(String xmlStr2) {
        this.xmlStr2 = xmlStr2;
    }

    @Lob
    public String getErrorStr() {
        return errorStr;
    }

    public void setErrorStr(String errorStr) {
        this.errorStr = errorStr;
    }

}
