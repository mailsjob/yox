package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("推荐配件")
@Entity
@Table(name = "iyb_recommend_fitting")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RecommendFitting extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("源主键")
    private Long sourceSid;
    @MetaData("源名")
    private String sourceDisplay;
    @MetaData("配件主键")
    private Long destSid;
    @MetaData("配件名")
    private String destDisplay;
    @MetaData("源类型")
    private SourceTypeEnum sourceType;
    @MetaData("配件类型")
    private SourceTypeEnum destType;
    @MetaData("配件数据，dest_type当为分类的时候，在这里缓存该分类热门商品")
    private String destData;
    @MetaData("排序号")
    private Integer orderIndex = 0;

    public enum SourceTypeEnum {

        @MetaData("商品")
        CO,

        @MetaData("分类")
        CA;

    }

    @Column(nullable = false)
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 2, nullable = false)
    @JsonProperty
    public SourceTypeEnum getSourceType() {
        return sourceType;
    }

    public void setSourceType(SourceTypeEnum sourceType) {
        this.sourceType = sourceType;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 2, nullable = false)
    @JsonProperty
    public SourceTypeEnum getDestType() {
        return destType;
    }

    public void setDestType(SourceTypeEnum destType) {
        this.destType = destType;
    }

    @Column(length = 1024)
    public String getDestData() {
        return destData;
    }

    public void setDestData(String destData) {
        this.destData = destData;
    }

    @Column(nullable = false)
    @JsonProperty
    public Long getSourceSid() {
        return sourceSid;
    }

    public void setSourceSid(Long sourceSid) {
        this.sourceSid = sourceSid;
    }

    @Column(nullable = false)
    @JsonProperty
    public Long getDestSid() {
        return destSid;
    }

    public void setDestSid(Long destSid) {
        this.destSid = destSid;
    }

    @JsonProperty
    public String getSourceDisplay() {
        return sourceDisplay;
    }

    public void setSourceDisplay(String sourceDisplay) {
        this.sourceDisplay = sourceDisplay;
    }

    @JsonProperty
    public String getDestDisplay() {
        return destDisplay;
    }

    public void setDestDisplay(String destDisplay) {
        this.destDisplay = destDisplay;
    }

}
