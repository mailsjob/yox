package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("外部商品关联表")
@Entity
@Table(name = "iyb_commodity_sku_relation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommoditySkuRelation extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("商品")
    private Commodity commodity;

    @MetaData("外部商品SKU")
    private String outsideSku;

    @MetaData("外部商品标题")
    private String outsideTitle;

    @MetaData("商品来源")
    private OutsideCommoditySourceTypeEnum source;

    public enum OutsideCommoditySourceTypeEnum {
        @MetaData(value = "京东")
        JD,

        @MetaData(value = "天猫")
        TMALL;
    }

    @ManyToOne
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public String getOutsideSku() {
        return outsideSku;
    }

    public void setOutsideSku(String outsideSku) {
        this.outsideSku = outsideSku;
    }

    @JsonProperty
    public String getOutsideTitle() {
        return outsideTitle;
    }

    public void setOutsideTitle(String outsideTitle) {
        this.outsideTitle = outsideTitle;
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @JsonProperty
    public OutsideCommoditySourceTypeEnum getSource() {
        return source;
    }

    public void setSource(OutsideCommoditySourceTypeEnum source) {
        this.source = source;
    }

}
