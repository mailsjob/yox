package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.CommodityPresent.GivenTypeEnum;

@MetaData("单品")
@Entity
@Table(name = "iyb_single_goods_present")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SingleGoodsPresent extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联单品")
    private SingleGoods singleGoods;
    /*
     * @MetaData("关联商品") private Commodity commodity;
     */
    @MetaData("关联赠品商品")
    private Commodity presentCommodity;
    @MetaData("数量")
    private BigDecimal quantity;
    @MetaData("开始日期")
    private Date beginDate;
    @MetaData("结束日期")
    private Date endDate;
    @MetaData("是否可用")
    private Boolean isEnable;
    @MetaData("能否叠加")
    private Boolean canStacked;
    @MetaData("给定类型")
    private GivenTypeEnum givenType;

    @ManyToOne
    @JoinColumn(name = "single_commodity_sid")
    @JsonProperty
    public SingleGoods getSingleGoods() {
        return singleGoods;
    }

    public void setSingleGoods(SingleGoods singleGoods) {
        this.singleGoods = singleGoods;
    }

    /*
     * @OneToOne(cascade = CascadeType.DETACH)
     * 
     * @JoinColumn(name = "commodity_sid")
     * 
     * @JsonProperty public Commodity getCommodity() { return commodity; }
     * public void setCommodity(Commodity commodity) { this.commodity =
     * commodity; }
     */

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "present_commodity_sid")
    @JsonProperty
    public Commodity getPresentCommodity() {
        return presentCommodity;
    }

    public void setPresentCommodity(Commodity presentCommodity) {
        this.presentCommodity = presentCommodity;
    }

    @JsonProperty
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @JsonProperty
    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    @JsonProperty
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @JsonProperty
    public Boolean getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    @JsonProperty
    public Boolean getCanStacked() {
        return canStacked;
    }

    public void setCanStacked(Boolean canStacked) {
        this.canStacked = canStacked;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32)
    @JsonProperty
    public GivenTypeEnum getGivenType() {
        return givenType;
    }

    public void setGivenType(GivenTypeEnum givenType) {
        this.givenType = givenType;
    }

}