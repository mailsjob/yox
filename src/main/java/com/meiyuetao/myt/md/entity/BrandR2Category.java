package com.meiyuetao.myt.md.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("品牌关联分类")
@Entity
@Table(name = "iyb_category_r2_brand")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class BrandR2Category extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("品牌")
    private Brand brand;
    @MetaData("分类")
    private Category category;
    @MetaData("排序号")
    private Integer orderIndex = 0;
    @MetaData("是否隐藏")
    private Boolean hiddenFlag = Boolean.FALSE;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "brand_sid", nullable = false)
    @JsonProperty
    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "category_sid", nullable = false)
    @JsonProperty
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @JsonProperty
    public Boolean getHiddenFlag() {
        return hiddenFlag;
    }

    public void setHiddenFlag(Boolean hiddenFlag) {
        this.hiddenFlag = hiddenFlag;
    }

}
