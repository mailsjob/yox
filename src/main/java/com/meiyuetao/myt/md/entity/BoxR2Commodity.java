package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("盒子与商品关联")
@Entity
@Table(name = "iyb_box_r2_commondity", uniqueConstraints = @UniqueConstraint(columnNames = { "BOX_SID", "COMMODITY_SID" }))
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class BoxR2Commodity extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private Box box;
    private Commodity commodity;
    private BigDecimal quantity;

    @ManyToOne
    @JoinColumn(name = "BOX_SID", nullable = false)
    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    @ManyToOne
    @JoinColumn(name = "COMMODITY_SID", nullable = false)
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
