package com.meiyuetao.myt.md.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("楼层")
@Entity
@Table(name = "iyb_floor")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Floor extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "月份文本")
    @EntityAutoCode(order = 10, search = true)
    private String monthText;

    @MetaData(value = "楼层图片")
    private String floorImg;

    @MetaData(value = "楼层颜色")
    private String floorColor;

    @MetaData(value = "排序号")
    @EntityAutoCode(order = 10, search = true)
    private Integer orderIndex;

    @MetaData(value = "适用阶段")
    @EntityAutoCode(order = 10, search = true)
    private String fitStage;

    @MetaData(value = "适用起始月")
    @EntityAutoCode(order = 10, search = true)
    private Integer fitStart;

    @MetaData(value = "适用起始月")
    @EntityAutoCode(order = 10, search = true)
    private Integer fitEnd;
    // private TreeSet<String> treeSet = new TreeSet<String>();

    @MetaData(value = "楼层类型")
    @EntityAutoCode(order = 10, search = true)
    private FloorTypeEnum floorType = FloorTypeEnum.TIME;

    private List<FloorAd> floorAds = new ArrayList<FloorAd>();
    private List<FloorMenu> floorMenus = new ArrayList<FloorMenu>();
    private List<RecommendCommodity> recommendCommodities = new ArrayList<RecommendCommodity>();
    @MetaData(value = "链接cms")
    private String linkCms;
    @MetaData(value = "适用类型")
    private SiteFlagEnum siteFlag = SiteFlagEnum.MEIYUETAO;
    @MetaData("地区")
    private Region region;

    public enum SiteFlagEnum {

        @MetaData("爱宝贝")
        CHINABABY,

        @MetaData("美月淘主页索引")
        MYTHOMEINDEX,

        @MetaData("wap站楼层")
        WAP_SITE,

        @MetaData("手机主页楼层")
        MOBILE_HOME,

        @MetaData("美月淘")
        MEIYUETAO,

        @MetaData("优蜜美店手机版")
        MOBILE_MEIVD;
    }

    public enum FloorTypeEnum {

        @MetaData("按时间")
        TIME,

        @MetaData("TIME_NO")
        TIME_NO,

        @MetaData("按分类")
        CATG;
    }

    @JsonProperty
    public String getMonthText() {
        return monthText;
    }

    public void setMonthText(String monthText) {
        this.monthText = monthText;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @JsonProperty
    public String getFitStage() {
        return fitStage;
    }

    public void setFitStage(String fitStage) {
        this.fitStage = fitStage;
    }

    @JsonProperty
    public Integer getFitStart() {
        return fitStart;
    }

    public void setFitStart(Integer fitStart) {
        this.fitStart = fitStart;
    }

    @JsonProperty
    public Integer getFitEnd() {
        return fitEnd;
    }

    public void setFitEnd(Integer fitEnd) {
        this.fitEnd = fitEnd;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 4, nullable = false)
    @JsonProperty
    public FloorTypeEnum getFloorType() {
        return floorType;
    }

    public void setFloorType(FloorTypeEnum floorType) {
        this.floorType = floorType;
    }

    @OneToMany(mappedBy = "floor", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<FloorAd> getFloorAds() {
        return floorAds;
    }

    public void setFloorAds(List<FloorAd> floorAds) {
        this.floorAds = floorAds;
    }

    @OneToMany(mappedBy = "floor", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<FloorMenu> getFloorMenus() {
        return floorMenus;
    }

    public void setFloorMenus(List<FloorMenu> floorMenus) {
        this.floorMenus = floorMenus;
    }

    @Column(length = 1000)
    @JsonProperty
    public String getLinkCms() {
        return linkCms;
    }

    public void setLinkCms(String linkCms) {
        this.linkCms = linkCms;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public SiteFlagEnum getSiteFlag() {
        return siteFlag;
    }

    public void setSiteFlag(SiteFlagEnum siteFlag) {
        this.siteFlag = siteFlag;
    }

    @OneToMany(mappedBy = "floor", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<RecommendCommodity> getRecommendCommodities() {
        return recommendCommodities;
    }

    public void setRecommendCommodities(List<RecommendCommodity> recommendCommodities) {
        this.recommendCommodities = recommendCommodities;
    }

    @Transient
    @JsonProperty
    public String getDisplay() {
        return monthText;
    }

    @Transient
    @JsonProperty
    public String getDisplayLabel() {
        String siteFlagName = "";
        if (SiteFlagEnum.CHINABABY.equals(siteFlag)) {
            siteFlagName = "爱宝贝";
        } else if (SiteFlagEnum.MYTHOMEINDEX.equals(siteFlag)) {
            siteFlagName = "美月淘主页索引";
        } else {
            siteFlagName = "美月淘";
        }
        return siteFlagName + "  " + (monthText != null ? monthText : String.valueOf(id));
    }

    @JsonProperty
    public String getFloorImg() {
        return floorImg;
    }

    public void setFloorImg(String floorImg) {
        this.floorImg = floorImg;
    }

    @JsonProperty
    public String getFloorColor() {
        return floorColor;
    }

    public void setFloorColor(String floorColor) {
        this.floorColor = floorColor;
    }

    @JsonProperty
    @JoinColumn(name= "region_sid")
    @OneToOne(cascade = CascadeType.DETACH)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

}
