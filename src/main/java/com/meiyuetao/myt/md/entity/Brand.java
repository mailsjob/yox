package com.meiyuetao.myt.md.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("品牌")
@Entity
@Table(name = "iyb_brand")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Brand extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("品牌名称")
    private String title;
    @MetaData("品牌小图")
    private String smallPic;
    @MetaData("品牌中图")
    private String midPic;
    @MetaData("品牌介绍")
    private String htmlContent;
    @MetaData("标语")
    private String slogan;
    @MetaData("概要说明")
    private String summary;
    @MetaData("品牌链接")
    private String brandUrl;
    /** 同义词,以逗号间隔 */
    @MetaData("同义词")
    private String synonyms;
    @MetaData("排序")
    private Integer orderIndex = 0;
    private List<Commodity> commodities = new ArrayList<Commodity>();
    private List<BrandR2Category> brandR2Categories = new ArrayList<BrandR2Category>();

    @MetaData("是否周期购")
    private Boolean circle = Boolean.FALSE;

    @MetaData("授权开始日期")
    private Date startDate;

    @MetaData("授权截止日期")
    private Date endDate;

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 128)
    @JsonProperty
    public String getSmallPic() {
        return smallPic;
    }

    public void setSmallPic(String smallPic) {
        this.smallPic = smallPic;
    }

    @Column(length = 128)
    public String getMidPic() {
        return midPic;
    }

    public void setMidPic(String midPic) {
        this.midPic = midPic;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    @Column(length = 128)
    @JsonProperty
    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Column(length = 1024)
    @JsonProperty
    public String getBrandUrl() {
        return brandUrl;
    }

    public void setBrandUrl(String brandUrl) {
        this.brandUrl = brandUrl;
    }

    @OneToMany(mappedBy = "brand")
    @OrderBy("barcode asc")
    public List<Commodity> getCommodities() {
        return commodities;
    }

    public void setCommodities(List<Commodity> commodities) {
        this.commodities = commodities;
    }

    @JsonProperty
    public String getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(String synonyms) {
        this.synonyms = synonyms;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @Column(name = "is_circle")
    @JsonProperty
    public Boolean getCircle() {
        return circle;
    }

    public void setCircle(Boolean circle) {
        this.circle = circle;
    }

    @Transient
    @JsonProperty
    public String getDisplay() {
        return this.getTitle();
    }

    @JsonProperty
    @Column(nullable = true)
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @JsonProperty
    @Column(nullable = true)
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @OneToMany(mappedBy = "brand", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<BrandR2Category> getBrandR2Categories() {
        return brandR2Categories;
    }

    public void setBrandR2Categories(List<BrandR2Category> brandR2Categories) {
        this.brandR2Categories = brandR2Categories;
    }

}
