package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.BaseEntity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@MetaData("商品价格信息表")
@Entity
@Table(name = "iyb_commodity_vary_price")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityVaryPrice extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;

    @MetaData("商品")
    private Commodity commodity;

    @MetaData("周期购订3个月的价格")
    private BigDecimal m3CirclePrice;

    @MetaData("周期购订4个月的价格")
    private BigDecimal m4CirclePrice;

    @MetaData("周期购订5个月的价格")
    private BigDecimal m5CirclePrice;

    @MetaData("周期购订6个月的价格")
    private BigDecimal m6CirclePrice;

    @MetaData(value = "最近采购价格", comments = "创建采购单时同步更新")
    private BigDecimal lastPurchasePrice;
    @MetaData(value = "最近销售价格", comments = "创建销售单时同步更新")
    private BigDecimal lastSalePrice;

    @MetaData(value = "1级代理商销售价格")
    private BigDecimal level1AgentSalePrice;

    @MetaData(value = "2级代理商销售价格")
    private BigDecimal level2AgentSalePrice;

    @MetaData(value = "3级代理商销售价格")
    private BigDecimal level3AgentSalePrice;

    /** 流水号主键 */
    protected Long id;

    @Id
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "foreign", parameters = @Parameter(name = "property", value = "commodity"))
    @Column(name = "sid")
    @JsonProperty
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(optional = false)
    @PrimaryKeyJoinColumn
    @JsonIgnore
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @Column(name = "m3_circle_price", precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getM3CirclePrice() {
        return m3CirclePrice;
    }

    public void setM3CirclePrice(BigDecimal m3CirclePrice) {
        this.m3CirclePrice = m3CirclePrice;
    }

    @Column(name = "m4_circle_price", precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getM4CirclePrice() {
        return m4CirclePrice;
    }

    public void setM4CirclePrice(BigDecimal m4CirclePrice) {
        this.m4CirclePrice = m4CirclePrice;
    }

    @Column(name = "m5_circle_price", precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getM5CirclePrice() {
        return m5CirclePrice;
    }

    public void setM5CirclePrice(BigDecimal m5CirclePrice) {
        this.m5CirclePrice = m5CirclePrice;
    }

    @Column(name = "m6_circle_price", precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getM6CirclePrice() {
        return m6CirclePrice;
    }

    public void setM6CirclePrice(BigDecimal m6CirclePrice) {
        this.m6CirclePrice = m6CirclePrice;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getLastPurchasePrice() {
        return lastPurchasePrice;
    }

    public void setLastPurchasePrice(BigDecimal lastPurchasePrice) {
        this.lastPurchasePrice = lastPurchasePrice;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getLastSalePrice() {
        return lastSalePrice;
    }

    public void setLastSalePrice(BigDecimal lastSalePrice) {
        this.lastSalePrice = lastSalePrice;
    }

    @Override
    @Transient
    public String getDisplay() {
        return "Prices for: " + commodity.getDisplay();
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getLevel1AgentSalePrice() {
        return level1AgentSalePrice;
    }

    public void setLevel1AgentSalePrice(BigDecimal level1AgentSalePrice) {
        this.level1AgentSalePrice = level1AgentSalePrice;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getLevel2AgentSalePrice() {
        return level2AgentSalePrice;
    }

    public void setLevel2AgentSalePrice(BigDecimal level2AgentSalePrice) {
        this.level2AgentSalePrice = level2AgentSalePrice;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getLevel3AgentSalePrice() {
        return level3AgentSalePrice;
    }

    public void setLevel3AgentSalePrice(BigDecimal level3AgentSalePrice) {
        this.level3AgentSalePrice = level3AgentSalePrice;
    }

}
