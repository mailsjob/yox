package com.meiyuetao.myt.md.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

/**
 * Auto Generate Javadoc
 */
@Entity
@MetaData("地区")
@Table(name = "iyb_region")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Region extends MytBaseEntity implements Comparable<Region> {
    private static final long serialVersionUID = 1L;
    @MetaData("上级地区")
    private Region parent;
    private List<Region> children;
    @MetaData("代码")
    private String code;
    @MetaData("名称")
    private String name;
    @MetaData("省、地区")
    private Integer sinaProvincy;
    @MetaData("城市")
    private Integer sinaCity;
    @MetaData("全拼")
    private String pinyinLong;
    @MetaData("简拼")
    private String pinyinShort;
    @MetaData("派瑞编码")
    private String pairui;

    @ManyToOne
    @JoinColumn(name = "parent_sid")
    @JsonIgnore
    public Region getParent() {
        return parent;
    }

    public void setParent(Region parent) {
        this.parent = parent;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSinaProvincy() {
        return sinaProvincy;
    }

    public void setSinaProvincy(Integer sinaProvincy) {
        this.sinaProvincy = sinaProvincy;
    }

    public Integer getSinaCity() {
        return sinaCity;
    }

    public void setSinaCity(Integer sinaCity) {
        this.sinaCity = sinaCity;
    }

    public String getPinyinLong() {
        return pinyinLong;
    }

    public void setPinyinLong(String pinyinLong) {
        this.pinyinLong = pinyinLong;
    }

    @JsonProperty
    public String getPinyinShort() {
        return pinyinShort;
    }

    public void setPinyinShort(String pinyinShort) {
        this.pinyinShort = pinyinShort;
    }

    @Transient
    @JsonProperty
    public List<Region> getChildren() {
        return children;
    }

    public void setChildren(List<Region> children) {
        this.children = children;
    }

    /**
     * 计算节点所在层级，根节点以0开始
     * 
     * @return
     */
    @Transient
    @JsonIgnore
    public int getLevel() {
        int level = 0;
        return loopLevel(level, this);
    }

    private int loopLevel(int level, Region item) {
        Region parent = item.getParent();
        if (parent != null && parent.getId() != null && parent.getId() > 0) {
            return loopLevel(level + 1, item.getParent());
        }
        return level;
    }

    @Override
    public int compareTo(Region o) {
        return CompareToBuilder.reflectionCompare(this.getPinyinShort().substring(0, 1), o.getPinyinShort().substring(0, 1));
    }

    @Transient
    @Override
    public String getDisplay() {
        String path = name;
        Region cur = getParent();
        while (cur != null && cur.id != 0) {
            path = cur.getName() + " " + path;
            cur = cur.getParent();
        }
        return path;
    }

    @Column(name="pairui")
    public String getPairui() {
        return pairui;
    }

    public void setPairui(String pairui) {
        this.pairui = pairui;
    }
}