package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("广告")
@Entity
@Table(name = "iyb_ad")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Ad extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("位置代码")
    private String adPositionCode;
    @MetaData("名称")
    private String adName;
    @MetaData("图片")
    private String adBannerPic;
    @MetaData("背景图")
    private String bgImgUrl;
    @MetaData("背景色")
    private String bgColor;
    @MetaData("邮件图")
    private String emailPic;
    @MetaData("对象主键")
    private Long adObjectSid;
    @MetaData("广对象名")
    private String adObjectDisplay;
    @MetaData("排序号")
    private Integer orderIndex = 100;
    @MetaData("对象类型")
    private AdObjectTypeEnum adObjectType = AdObjectTypeEnum.COMMODITY;
    @MetaData("是否可用")
    private Boolean enable = Boolean.FALSE;
    @MetaData("链接")
    private String url;

    public enum AdObjectTypeEnum {
        @MetaData("商品")
        COMMODITY, @MetaData("主题清单")
        THEME, @MetaData("每日推荐(什么值得买)")
        SMZDM, @MetaData("活动")
        ACTIVITY, @MetaData("超链接")
        URL,
    }

    @JsonProperty
    public String getAdPositionCode() {
        return adPositionCode;
    }

    public void setAdPositionCode(String adPositionCode) {
        this.adPositionCode = adPositionCode;
    }

    @JsonProperty
    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    @JsonProperty
    public String getAdBannerPic() {
        return adBannerPic;
    }

    public void setAdBannerPic(String adBannerPic) {
        this.adBannerPic = adBannerPic;
    }

    @JsonProperty
    public Long getAdObjectSid() {
        return adObjectSid;
    }

    public void setAdObjectSid(Long adObjectSid) {
        this.adObjectSid = adObjectSid;
    }

    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public AdObjectTypeEnum getAdObjectType() {
        return adObjectType;
    }

    public void setAdObjectType(AdObjectTypeEnum adObjectType) {
        this.adObjectType = adObjectType;
    }

    @Column(name = "is_enable")
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @JsonProperty
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty
    public String getBgImgUrl() {
        return bgImgUrl;
    }

    public void setBgImgUrl(String bgImgUrl) {
        this.bgImgUrl = bgImgUrl;
    }

    @JsonProperty
    public String getEmailPic() {
        return emailPic;
    }

    public void setEmailPic(String emailPic) {
        this.emailPic = emailPic;
    }

    @JsonProperty
    public String getAdObjectDisplay() {
        return adObjectDisplay;
    }

    public void setAdObjectDisplay(String adObjectDisplay) {
        this.adObjectDisplay = adObjectDisplay;
    }

    @JsonProperty
    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

}
