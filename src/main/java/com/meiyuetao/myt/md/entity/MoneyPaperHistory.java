package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.sale.entity.BoxOrder;

@Entity
@Table(name = "iyb_money_paper_history")
@MetaData("礼品卡使用记录")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MoneyPaperHistory extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("礼品卡")
    private MoneyPaper moneyPaper;
    @MetaData("金额")
    private BigDecimal amount;
    @MetaData("说明")
    private String reason;
    @MetaData("时间")
    private Date recordDatetime;
    @MetaData("订单")
    private BoxOrder boxOrder;

    @ManyToOne
    @JoinColumn(name = "money_paper_sid", nullable = false)
    @JsonProperty
    public MoneyPaper getMoneyPaper() {
        return moneyPaper;
    }

    public void setMoneyPaper(MoneyPaper moneyPaper) {
        this.moneyPaper = moneyPaper;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(length = 1024)
    @JsonProperty
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getRecordDatetime() {
        return recordDatetime;
    }

    public void setRecordDatetime(Date recordDatetime) {
        this.recordDatetime = recordDatetime;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "order_sid")
    @JsonProperty
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

}