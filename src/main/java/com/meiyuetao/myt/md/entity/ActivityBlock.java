package com.meiyuetao.myt.md.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("活动块")
@Entity
@Table(name = "iyb_activity_block")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ActivityBlock extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联活动")
    private Activity activity;
    @MetaData("块名称")
    private String blockTitle;
    @MetaData("位置代码")
    private String locationCode;
    @MetaData("块样式")
    private BlockTemplateEnum blockTemplate = BlockTemplateEnum.SLIDER;

    public enum BlockTemplateEnum {
        @MetaData("滑动图")
        SLIDER, @MetaData("列表样式")
        LISTA;

    }

    @MetaData("排序号")
    private Integer orderIndex = 0;
    private List<ActivityBlockR2Object> activityBlockR2Objects = new ArrayList<ActivityBlockR2Object>();

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "activity_sid", nullable = true)
    @JsonProperty
    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @JsonProperty
    @Column(nullable = false)
    public String getBlockTitle() {
        return blockTitle;
    }

    public void setBlockTitle(String blockTitle) {
        this.blockTitle = blockTitle;
    }

    @JsonProperty
    @Column(nullable = false)
    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    @JsonProperty
    public BlockTemplateEnum getBlockTemplate() {
        return blockTemplate;
    }

    public void setBlockTemplate(BlockTemplateEnum blockTemplate) {
        this.blockTemplate = blockTemplate;
    }

    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @OneToMany(mappedBy = "activityBlock", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ActivityBlockR2Object> getActivityBlockR2Objects() {
        return activityBlockR2Objects;
    }

    public void setActivityBlockR2Objects(List<ActivityBlockR2Object> activityBlockR2Objects) {
        this.activityBlockR2Objects = activityBlockR2Objects;
    }

    @Override
    @Transient
    @JsonProperty
    public String getDisplay() {
        return locationCode + " " + blockTitle;
    }

}
