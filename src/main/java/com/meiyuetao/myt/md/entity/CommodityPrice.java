package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("商品价格计划表")
@Entity
@Table(name = "iyb_commodity_price", uniqueConstraints = @UniqueConstraint(columnNames = { "Commodity_SID", "priceYear", "priceMonth" }))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityPrice extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("商品价格计划表")
    private Commodity commodity;
    @MetaData("价格计划规则")
    private String ruleType;
    @MetaData("价格计划年")
    private Integer priceYear;
    @MetaData("价格计划月份")
    private Integer priceMonth;

    @MetaData("冗余字段：取priceYear+priceMonth组合")
    private Date priceTime;
    @MetaData("当月价格")
    private BigDecimal m0Price;
    @MetaData("1月后价格")
    private BigDecimal m1Price;
    @MetaData("2月后价格")
    private BigDecimal m2Price;

    private BigDecimal m3Price;
    private BigDecimal m4Price;
    private BigDecimal m5Price;
    private BigDecimal m6Price;
    private BigDecimal m7Price;
    private BigDecimal m8Price;
    private BigDecimal m9Price;
    private BigDecimal m10Price;
    private BigDecimal m11Price;
    private BigDecimal lowestPrice;

    @MetaData("缺省价格计划")
    private Boolean defaultPrice = Boolean.FALSE;
    @MetaData("冗余商品sid")
    private Long commoditySid;

    public static enum CommodityPriceRuleTypeEnum {

        DECREASE_BY_MONTH_RATE("逐月折扣递减"), FREE_INPUT("自由录入"), DECREASE_BY_GRADE("差额递减");

        private String label;

        CommodityPriceRuleTypeEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    @ManyToOne
    @JoinColumn(name = "Commodity_SID", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public Integer getPriceYear() {
        return priceYear;
    }

    public void setPriceYear(Integer priceYear) {
        this.priceYear = priceYear;
    }

    @JsonProperty
    public Integer getPriceMonth() {
        return priceMonth;
    }

    public void setPriceMonth(Integer priceMonth) {
        this.priceMonth = priceMonth;
    }

    @Temporal(TemporalType.DATE)
    @JsonSerialize(using = DateJsonSerializer.class)
    @JsonProperty
    public Date getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(Date priceTime) {
        DateTime dateTime = new DateTime(priceTime);
        this.setPriceMonth(dateTime.getMonthOfYear());
        this.setPriceYear(dateTime.getYear());
        this.priceTime = priceTime;
    }

    @Column(precision = 18, scale = 2, name = "m1_price")
    @JsonProperty
    public BigDecimal getM1Price() {
        return m1Price;
    }

    public void setM1Price(BigDecimal m1Price) {
        this.m1Price = m1Price;
    }

    @Column(precision = 18, scale = 2, name = "m2_price")
    @JsonProperty
    public BigDecimal getM2Price() {
        return m2Price;
    }

    public void setM2Price(BigDecimal m2Price) {
        this.m2Price = m2Price;
    }

    @Column(precision = 18, scale = 2, name = "m3_price")
    @JsonProperty
    public BigDecimal getM3Price() {
        return m3Price;
    }

    public void setM3Price(BigDecimal m3Price) {
        this.m3Price = m3Price;
    }

    @Column(precision = 18, scale = 2, name = "m4_price")
    @JsonProperty
    public BigDecimal getM4Price() {
        return m4Price;
    }

    public void setM4Price(BigDecimal m4Price) {
        this.m4Price = m4Price;
    }

    @Column(precision = 18, scale = 2, name = "m5_price")
    @JsonProperty
    public BigDecimal getM5Price() {
        return m5Price;
    }

    public void setM5Price(BigDecimal m5Price) {
        this.m5Price = m5Price;
    }

    @Column(precision = 18, scale = 2, name = "m6_price")
    @JsonProperty
    public BigDecimal getM6Price() {
        return m6Price;
    }

    public void setM6Price(BigDecimal m6Price) {
        this.m6Price = m6Price;
    }

    @Column(precision = 18, scale = 2, name = "m7_price")
    @JsonProperty
    public BigDecimal getM7Price() {
        return m7Price;
    }

    public void setM7Price(BigDecimal m7Price) {
        this.m7Price = m7Price;
    }

    @Column(precision = 18, scale = 2, name = "m8_price")
    @JsonProperty
    public BigDecimal getM8Price() {
        return m8Price;
    }

    public void setM8Price(BigDecimal m8Price) {
        this.m8Price = m8Price;
    }

    @Column(precision = 18, scale = 2, name = "m9_price")
    @JsonProperty
    public BigDecimal getM9Price() {
        return m9Price;
    }

    public void setM9Price(BigDecimal m9Price) {
        this.m9Price = m9Price;
    }

    @Column(precision = 18, scale = 2, name = "m10_price")
    @JsonProperty
    public BigDecimal getM10Price() {
        return m10Price;
    }

    public void setM10Price(BigDecimal m10Price) {
        this.m10Price = m10Price;
    }

    @Column(precision = 18, scale = 2, name = "m11_price")
    @JsonProperty
    public BigDecimal getM11Price() {
        return m11Price;
    }

    public void setM11Price(BigDecimal m11Price) {
        this.m11Price = m11Price;
    }

    @Column(precision = 18, scale = 2, name = "m0_price")
    @JsonProperty
    public BigDecimal getM0Price() {
        return m0Price;
    }

    public void setM0Price(BigDecimal m0Price) {
        this.m0Price = m0Price;
    }

    @Column(length = 128)
    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((priceMonth == null) ? 0 : priceMonth.hashCode());
        result = prime * result + ((priceYear == null) ? 0 : priceYear.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CommodityPrice other = (CommodityPrice) obj;
        if (priceMonth == null) {
            if (other.priceMonth != null)
                return false;
        } else if (!priceMonth.equals(other.priceMonth))
            return false;
        if (priceYear == null) {
            if (other.priceYear != null)
                return false;
        } else if (!priceYear.equals(other.priceYear))
            return false;
        return true;
    }

    @JsonProperty
    public BigDecimal getLowestPrice() {
        return lowestPrice;
    }

    public void setLowestPrice(BigDecimal lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    @Transient
    @JsonProperty
    public String getPriceMonthYear() {
        return priceYear + "-" + priceMonth;
    }

    public void setPriceMonthYear(String priceMonthYear) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        DateTime dateTime = new DateTime(sdf.parse(priceMonthYear));
        this.setPriceTime(dateTime.toDate());
        this.setPriceMonth(dateTime.getMonthOfYear());
        this.setPriceYear(dateTime.getYear());
    }

    /*
     * @JsonProperty public void setPriceMonthYear(Date priceTime) throws
     * Exception { DateTime dateTime = new DateTime(priceTime);
     * this.setPriceMonth(dateTime.getMonthOfYear());
     * this.setPriceYear(dateTime.getYear()); }
     */
    @Transient
    public Long getCommoditySid() {
        return commoditySid;
    }

    public void setCommoditySid(Long commoditySid) {
        this.commoditySid = commoditySid;
    }

    @JsonProperty
    public Boolean getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(Boolean defaultPrice) {
        this.defaultPrice = defaultPrice;
    }
}
