package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("活动")
@Entity
@Table(name = "iyb_activity_r2_object")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ActivityR2Object extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联活动")
    private Activity activity;
    @MetaData("对象主键，商品、清单(主题)或者其他的sid，和object_type对应")
    private Long objectSid;
    @MetaData("是否可用")
    private Boolean enable = Boolean.TRUE;
    @MetaData("对象类型")
    private ObjectTypeEnum objectType = ObjectTypeEnum.COMMODITY;
    @MetaData("对象主键，商品、清单(主题)等对象的显示名")
    private String objectDisplay;

    public enum ObjectTypeEnum {
        @MetaData("商品")
        COMMODITY,

        @MetaData("主题清单")
        THEME;
    }

    @ManyToOne
    @JoinColumn(name = "activity_sid", nullable = false)
    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Column(nullable = false)
    @JsonProperty
    public Long getObjectSid() {
        return objectSid;
    }

    public void setObjectSid(Long objectSid) {
        this.objectSid = objectSid;
    }

    @Column(name = "is_enable", nullable = false)
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    @JsonProperty
    public ObjectTypeEnum getObjectType() {
        return objectType;
    }

    public void setObjectType(ObjectTypeEnum objectType) {
        this.objectType = objectType;
    }

    @JsonProperty
    public String getObjectDisplay() {
        return objectDisplay;
    }

    public void setObjectDisplay(String objectDisplay) {
        this.objectDisplay = objectDisplay;
    }

}
