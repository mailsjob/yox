package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@Entity
@MetaData("商品关联图片")
@Table(name = "iyb_commodity_pic")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityPic extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("小图")
    private String smallPicUrl;
    @MetaData("中图")
    private String midPicUrl;
    @MetaData("大图")
    private String bigPicUrl;
    @MetaData("排序号")
    private Integer picIndex = 100;
    @MetaData("商品")
    private Commodity commodity;

    public Integer getPicIndex() {
        return picIndex;
    }

    public void setPicIndex(Integer picIndex) {
        this.picIndex = picIndex;
    }

    @Column(length = 128)
    public String getSmallPicUrl() {
        return smallPicUrl;
    }

    public void setSmallPicUrl(String smallPicUrl) {
        this.smallPicUrl = smallPicUrl;
    }

    @Column(length = 128)
    public String getMidPicUrl() {
        return midPicUrl;
    }

    public void setMidPicUrl(String midPicUrl) {
        this.midPicUrl = midPicUrl;
    }

    @Column(length = 128, nullable = false)
    public String getBigPicUrl() {
        return bigPicUrl;
    }

    public void setBigPicUrl(String bigPicUrl) {
        this.bigPicUrl = bigPicUrl;
    }

    @ManyToOne
    @JoinColumn(name = "Commodity_SID", nullable = false)
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

}
