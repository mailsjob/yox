package com.meiyuetao.myt.md.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("商品销售日期")
@Entity
@Table(name = "iyb_commodity_sale_date_count")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommoditySaleDateCount extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private Commodity commodity;
    @MetaData("日可购买量")
    private Integer dailyCanBuyCount = 0;
    @MetaData("销售日期")
    private Date saleDate;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid", nullable = false, updatable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public Integer getDailyCanBuyCount() {
        return dailyCanBuyCount;
    }

    public void setDailyCanBuyCount(Integer dailyCanBuyCount) {
        this.dailyCanBuyCount = dailyCanBuyCount;
    }

    @JsonProperty
    @JsonSerialize(using = DateJsonSerializer.class)
    @Column(nullable = false)
    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

}
