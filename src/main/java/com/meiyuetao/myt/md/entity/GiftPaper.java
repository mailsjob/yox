package com.meiyuetao.myt.md.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@MetaData(value = "优惠券")
@Table(name = "iyb_gift_paper")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class GiftPaper extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "代码")
    private String code;
    @MetaData(value = "名称")
    private String title;
    @MetaData(value = "密码")
    private String password;
    @MetaData(value = "类型")
    private String paperMode;
    @MetaData(value = "状态")
    private GiftPaperStatusEnum paperStatus = GiftPaperStatusEnum.S20ACT;
    @MetaData(value = "开始时间")
    private Date beginTime;
    @MetaData(value = "过期时间")
    private Date expireTime;
    @MetaData(value = "面额")
    private BigDecimal faceValue;
    @MetaData(value = "客户")
    private CustomerProfile customerProfile;
    @MetaData(value = "来源")
    private String paperSource;
    @MetaData(value = "功能代码")
    private String funcCode;
    @MetaData(value = "功能参数")
    private String funcParameter;
    @MetaData(value = "订单号")
    private String orderSeq;
    @MetaData(value = "模板号")
    private String paperTemplateCode;
    @MetaData(value = "批量件数")
    private Integer counts;

    @Column(length = 128)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty
    @Column(length = 128)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 128)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(length = 4)
    @JsonProperty
    public String getPaperMode() {
        return paperMode;
    }

    public void setPaperMode(String paperMode) {
        this.paperMode = paperMode;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public GiftPaperStatusEnum getPaperStatus() {
        return paperStatus;
    }

    public void setPaperStatus(GiftPaperStatusEnum paperStatus) {
        this.paperStatus = paperStatus;
    }

    @JsonProperty
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    @JsonProperty
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    @OneToOne
    @JoinColumn(name = "Customer_Profile_Sid", nullable = true)
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    @Column(length = 128)
    @JsonProperty
    public String getPaperSource() {
        return paperSource;
    }

    public void setPaperSource(String paperSource) {
        this.paperSource = paperSource;
    }

    @Column(length = 128)
    @JsonProperty
    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    @Column(length = 1024)
    @JsonProperty
    public String getFuncParameter() {
        return funcParameter;
    }

    public void setFuncParameter(String funcParameter) {
        this.funcParameter = funcParameter;
    }

    @Column(length = 64)
    public String getOrderSeq() {
        return orderSeq;
    }

    public void setOrderSeq(String orderSeq) {
        this.orderSeq = orderSeq;
    }

    @Column(length = 64)
    @JsonProperty
    public String getPaperTemplateCode() {
        return paperTemplateCode;
    }

    public void setPaperTemplateCode(String paperTemplateCode) {
        this.paperTemplateCode = paperTemplateCode;
    }

    @Transient
    @JsonProperty
    public Integer getCounts() {
        return counts;
    }

    public void setCounts(Integer counts) {
        this.counts = counts;
    }

    public enum GiftPaperStatusEnum {
        @MetaData("待激活")
        S10PND,

        @MetaData("激活状态")
        S20ACT,

        @MetaData("已使用")
        S30USD,

        @MetaData("已过期")
        S40EPR,

        @MetaData("已禁用")
        S50DIS;
    }
}