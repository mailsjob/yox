package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("单品")
@Entity
@Table(name = "iyb_single_goods")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class SingleGoods extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联商品")
    private Commodity commodity;
    @MetaData("最大可售数量")
    private BigDecimal maxSellQuantity;// 最大可售数量
    @MetaData("已售数量")
    private BigDecimal soldQuantity;// 已售数量
    @MetaData("商品标题")
    private String title;// 商品标题 不设置为商品标题
    @MetaData("橱窗图")
    private String pic;// 单品橱窗图片
    @MetaData("单品描述")
    private String htmlContent;// 单品详情描述 不设定为商品描述
    @MetaData("单价")
    private BigDecimal price;// 单价
    @MetaData("起售日期")
    private Date sellBeginTime;// 起售日期
    @MetaData("停售日期")
    private Date sellEndTime;// 停售日期

    @MetaData("单日限购数")
    private BigDecimal dailyUserCanBuyQuatity;// 用户每天可购买数量
    @MetaData("最多购买数量")
    private BigDecimal userBoughtMaxQuatity;// 用户购买最大数量

    private SingleGoodsTypeEnum type = SingleGoodsTypeEnum.SINGLE;

    private MYTSiteTypeEnum siteType = MYTSiteTypeEnum.MYT;

    public enum SingleGoodsTypeEnum {
        @MetaData("单品 ")
        SINGLE,

        @MetaData("闪购")
        FLASH;
    }

    @MetaData("闪购位置")
    public enum MYTSiteTypeEnum {
        @MetaData("美月淘")
        MYT,

        @MetaData("优蜜美店")
        YMMD;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @Column(name = "max_sell_quantity", nullable = false)
    @JsonProperty
    public BigDecimal getMaxSellQuantity() {
        return maxSellQuantity;
    }

    public void setMaxSellQuantity(BigDecimal maxSellQuantity) {
        this.maxSellQuantity = maxSellQuantity;
    }

    @Column(name = "sold_quantity", nullable = false)
    @JsonProperty
    public BigDecimal getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(BigDecimal soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    @Column(nullable = false)
    @JsonProperty
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getSellBeginTime() {
        return sellBeginTime;
    }

    public void setSellBeginTime(Date sellBeginTime) {
        this.sellBeginTime = sellBeginTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getSellEndTime() {
        return sellEndTime;
    }

    public void setSellEndTime(Date sellEndTime) {
        this.sellEndTime = sellEndTime;
    }

    @Column(nullable = false)
    @JsonProperty
    public BigDecimal getDailyUserCanBuyQuatity() {
        return dailyUserCanBuyQuatity;
    }

    public void setDailyUserCanBuyQuatity(BigDecimal dailyUserCanBuyQuatity) {
        this.dailyUserCanBuyQuatity = dailyUserCanBuyQuatity;
    }

    @Column(nullable = false)
    @JsonProperty
    public BigDecimal getUserBoughtMaxQuatity() {
        return userBoughtMaxQuatity;
    }

    public void setUserBoughtMaxQuatity(BigDecimal userBoughtMaxQuatity) {
        this.userBoughtMaxQuatity = userBoughtMaxQuatity;
    }

    @Transient
    @Override
    public String getDisplay() {
        return this.getId() + " " + this.getTitle();
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public SingleGoodsTypeEnum getType() {
        return type;
    }

    public void setType(SingleGoodsTypeEnum type) {
        this.type = type;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "site_type", length = 8)
    @JsonProperty
    public MYTSiteTypeEnum getSiteType() {
        return siteType;
    }

    public void setSiteType(MYTSiteTypeEnum siteType) {
        this.siteType = siteType;
    }
}
