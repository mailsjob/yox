package com.meiyuetao.myt.md.entity;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("主题清单")
@Entity
@Table(name = "iyb_theme")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Theme extends MytBaseEntity {
    private static final long serialVersionUID = 1L;

    @MetaData("客户信息")
    private CustomerProfile customerProfile;

    @MetaData("主题标题")
    private String themeTitle;

    @MetaData("主题图片")
    private String themePic;
    @MetaData("排序号")
    private Integer orderIndex = 100;

    @MetaData("关注数")
    private Integer focusCount;

    @MetaData("评论数")
    private Integer commentCount;

    @MetaData("主题所有标签")
    private String allLabels;

    @MetaData("主题状态")
    private ThemeStatusEnum themeStatus;

    @MetaData("主题细节列")
    private List<ThemeDetail> themeDetails;
    @MetaData("评论")
    private List<ThemeComment> themeComments;
    @MetaData("获得积分")
    private Integer payedScore = 0;

    @Column(name = "theme_title", length = 64)
    @JsonProperty
    public String getThemeTitle() {
        return themeTitle;
    }

    public void setThemeTitle(String themeTitle) {
        this.themeTitle = themeTitle;
    }

    @Column(name = "theme_pic", length = 512)
    @JsonProperty
    public String getThemePic() {
        return themePic;
    }

    public void setThemePic(String themePic) {
        this.themePic = themePic;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "all_labels")
    @JsonProperty
    public String getAllLabels() {
        return allLabels;
    }

    public void setAllLabels(String allLabels) {
        this.allLabels = allLabels;
    }

    @OneToOne
    @JoinColumn(name = "Customer_Profile_Sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Column(name = "order_index")
    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @Column(name = "focus_count")
    @JsonProperty
    public Integer getFocusCount() {
        return focusCount;
    }

    public void setFocusCount(Integer focusCount) {
        this.focusCount = focusCount;
    }

    @Column(name = "comment_count")
    @JsonProperty
    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public enum ThemeStatusEnum {

        @MetaData("未审批")
        UNAUDIT, @MetaData("公开")
        AUDIT, @MetaData("私有")
        DRAFT, @MetaData("删除")
        DELETE, @MetaData("下架")
        DOWNSHELF;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "theme_status", length = 512)
    @JsonProperty
    public ThemeStatusEnum getThemeStatus() {
        return themeStatus;
    }

    public void setThemeStatus(ThemeStatusEnum themeStatus) {
        this.themeStatus = themeStatus;
    }

    @OneToMany(mappedBy = "theme", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    public List<ThemeDetail> getThemeDetails() {
        return themeDetails;
    }

    public void setThemeDetails(List<ThemeDetail> themeDetails) {
        this.themeDetails = themeDetails;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getPayedScore() {
        return payedScore;
    }

    public void setPayedScore(Integer payedScore) {
        this.payedScore = payedScore;
    }

    @Override
    @Transient
    @JsonProperty
    public String getDisplay() {

        return themeTitle;
    }

    @OneToMany(mappedBy = "theme", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ThemeComment> getThemeComments() {
        return themeComments;
    }

    public void setThemeComments(List<ThemeComment> themeComments) {
        this.themeComments = themeComments;
    }

}