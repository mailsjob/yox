package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("主题清单明细")
@Entity
@Table(name = "iyb_theme_detail")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ThemeDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("主题清单")
    private Theme theme;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("排序号")
    private Integer orderIndex;

    @MetaData("年份偏移，默认为1，表示当年")
    private Integer yearOffset;

    @MetaData("如果是固定月份（自然月），1表示1月，101表示下一年1月；201表示第三年1月（暂时不用）")
    private Integer themeMonth;
    @MetaData("商品来源")
    private CommodityFromEnum commodityFrom = CommodityFromEnum.MYT;
    @MetaData("商品数量")
    private Integer commodityQuantity;

    public enum CommodityFromEnum {

        @MetaData("美月淘")
        MYT,

        @MetaData("第三方")
        DSF;
    }

    @ManyToOne
    @JoinColumn(name = "theme_sid", nullable = false)
    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @JsonProperty
    public Integer getYearOffset() {
        return yearOffset;
    }

    public void setYearOffset(Integer yearOffset) {
        this.yearOffset = yearOffset;
    }

    @JsonProperty
    public Integer getThemeMonth() {
        return themeMonth;
    }

    public void setThemeMonth(Integer themeMonth) {
        this.themeMonth = themeMonth;
    }

    @JsonProperty
    public Integer getCommodityQuantity() {
        return commodityQuantity;
    }

    public void setCommodityQuantity(Integer commodityQuantity) {
        this.commodityQuantity = commodityQuantity;
    }

    @OneToOne
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = true, length = 16)
    @JsonProperty
    public CommodityFromEnum getCommodityFrom() {
        return commodityFrom;
    }

    public void setCommodityFrom(CommodityFromEnum commodityFrom) {
        this.commodityFrom = commodityFrom;
    }

}