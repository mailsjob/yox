package com.meiyuetao.myt.md.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("新闻")
@Entity
@Table(name = "iyb_box_news")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class News extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("新闻标题")
    private String newsTitle;
    @MetaData("信息")
    private String newsInfo;
    @MetaData("日期")
    private Date newsDate;
    @MetaData("图片")
    private String newsImg;
    @MetaData("信息内容")
    private String newsInfoHtml;
    @MetaData("是否隐藏")
    private Boolean hiddenFlag = Boolean.FALSE;
    @MetaData("排序号")
    private Integer newsRank = 100;
    @MetaData("来源")
    private String newsFrom;
    @MetaData("来源链接")
    private String newsFromUrl;

    @Column(nullable = false)
    @JsonProperty
    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    @Column(nullable = false)
    @Lob
    public String getNewsInfo() {
        return newsInfo;
    }

    public void setNewsInfo(String newsInfo) {
        this.newsInfo = newsInfo;
    }

    @Column(nullable = false)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @JsonProperty
    public Date getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(Date newsDate) {
        this.newsDate = newsDate;
    }

    @Column(nullable = false)
    @JsonProperty
    public String getNewsImg() {
        return newsImg;
    }

    public void setNewsImg(String newsImg) {
        this.newsImg = newsImg;
    }

    @Lob
    public String getNewsInfoHtml() {
        return newsInfoHtml;
    }

    public void setNewsInfoHtml(String newsInfoHtml) {
        this.newsInfoHtml = newsInfoHtml;
    }

    @JsonProperty
    public Boolean getHiddenFlag() {
        return hiddenFlag;
    }

    public void setHiddenFlag(Boolean hiddenFlag) {
        this.hiddenFlag = hiddenFlag;
    }

    @JsonProperty
    public Integer getNewsRank() {
        return newsRank;
    }

    public void setNewsRank(Integer newsRank) {
        this.newsRank = newsRank;
    }

    @JsonProperty
    public String getNewsFrom() {
        return newsFrom;
    }

    public void setNewsFrom(String newsFrom) {
        this.newsFrom = newsFrom;
    }

    @JsonProperty
    public String getNewsFromUrl() {
        return newsFromUrl;
    }

    public void setNewsFromUrl(String newsFromUrl) {
        this.newsFromUrl = newsFromUrl;
    }

}
