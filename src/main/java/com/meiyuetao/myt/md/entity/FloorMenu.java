package com.meiyuetao.myt.md.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("楼层菜单")
@Entity
@Table(name = "iyb_floor_menu")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FloorMenu extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "名称")
    @EntityAutoCode(order = 10, search = true)
    private String name;

    @MetaData(value = "图标")
    @EntityAutoCode(order = 10, search = true)
    private String icon;
    @MetaData(value = "楼层")
    @EntityAutoCode(order = 10, search = true)
    private Floor floor;

    @MetaData(value = "排序号")
    @EntityAutoCode(order = 10, search = true)
    private Integer orderIndex = 0;

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "floor_sid", nullable = false)
    @JsonProperty
    public Floor getFloor() {
        return floor;
    }

    public void setFloor(Floor floor) {
        this.floor = floor;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    @Override
    @Transient
    @JsonProperty
    public String getDisplay() {

        return name;
    }

}
