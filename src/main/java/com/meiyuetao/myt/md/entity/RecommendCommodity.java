package com.meiyuetao.myt.md.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("首页商品")
@Entity
/*
 * @Table(name = "iyb_recommend_commodity", uniqueConstraints =
 * @UniqueConstraint(columnNames = { "floor_sid", "timeline_sid",
 * "commodity_sid", "promotion_type", "url" }))
 */
@Table(name = "iyb_recommend_commodity")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class RecommendCommodity extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("楼层")
    private Floor floor;
    @MetaData("时间线")
    private Category timeline;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("推荐标题")
    private String promotionTitle;
    @MetaData("推荐图片")
    private String promotionPic;
    @MetaData("推荐类型")
    private PromotionTypeEnum promotionType = PromotionTypeEnum.COMMODITY;
    @MetaData("是否固定")
    private Boolean fixed = Boolean.FALSE;
    @MetaData("排序号")
    private Integer orderRank = 0;
    @MetaData("主题清单")
    private Theme them;
    @MetaData("是否可用")
    private Boolean enable = Boolean.FALSE;
    @MetaData("楼层菜单")
    private FloorMenu floorMenu;
    @MetaData("链接")
    private String url;

    public enum PromotionTypeEnum {

        @MetaData("商品")
        COMMODITY, @MetaData("链接")
        URL, @MetaData("主题清单")
        THEME;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "floor_sid", nullable = false)
    @JsonProperty
    public Floor getFloor() {
        return floor;
    }

    public void setFloor(Floor floor) {
        this.floor = floor;
    }

    @OneToOne
    @JoinColumn(name = "timeline_sid")
    @JsonProperty
    public Category getTimeline() {
        return timeline;
    }

    public void setTimeline(Category timeline) {
        this.timeline = timeline;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @Column(length = 64)
    @JsonProperty
    public String getPromotionTitle() {
        return promotionTitle;
    }

    public void setPromotionTitle(String promotionTitle) {
        this.promotionTitle = promotionTitle;
    }

    @Column(length = 64)
    @JsonProperty
    public String getPromotionPic() {
        return promotionPic;
    }

    public void setPromotionPic(String promotionPic) {
        this.promotionPic = promotionPic;
    }

    @Column(name = "is_fixed", nullable = false)
    @JsonProperty
    public Boolean getFixed() {
        return fixed;
    }

    public void setFixed(Boolean fixed) {
        this.fixed = fixed;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getOrderRank() {
        return orderRank;
    }

    public void setOrderRank(Integer orderRank) {
        this.orderRank = orderRank;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false, name = "promotion_type")
    @JsonProperty
    public PromotionTypeEnum getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(PromotionTypeEnum promotionType) {
        this.promotionType = promotionType;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "theme_sid")
    public Theme getThem() {
        return them;
    }

    public void setThem(Theme them) {
        this.them = them;
    }

    @Column(name = "is_enable")
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "floor_menu_sid")
    @JsonProperty
    public FloorMenu getFloorMenu() {
        return floorMenu;
    }

    public void setFloorMenu(FloorMenu floorMenu) {
        this.floorMenu = floorMenu;
    }

    @JsonProperty
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}