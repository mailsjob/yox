package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("每日推荐（什么值得买）")
@Entity
@Table(name = "iyb_smzdm")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Smzdm extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("主题")
    private String title;
    @MetaData("副标题")
    private String subTitle;
    @MetaData("介绍")
    private String introduction;
    @MetaData("标签")
    private String labels;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("链接")
    private String zdmLink;
    @MetaData("联系邮箱")
    private String contactEmail;
    @MetaData("记录时间")
    private Date recordTime = new Date();
    @MetaData("概述")
    private String summary;
    @MetaData("适合阶段")
    private FitStageEnum fitStage;
    @MetaData("起始")
    private Integer fitStart;
    @MetaData("适合集合")
    private Integer fitUnit;
    @MetaData("适合人群")
    private String fitPeople;
    @MetaData("图片")
    private String img;
    @MetaData("评论数量")
    private Integer commentCount = 0;
    @MetaData("是否可用")
    private Boolean enable = Boolean.FALSE;

    @MetaData("爆料价")
    private BigDecimal price;
    @MetaData("开始时间")
    private Date beginTime;
    @MetaData("结束时间")
    private Date endTime;

    public enum FitStageEnum {
        @MetaData("孕期")
        YNQI, @MetaData("出生")
        CHSH, @MetaData("产后")
        CHOU;
    }

    @Column(length = 64, nullable = false)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 64)
    @JsonProperty
    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    @JsonProperty
    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @JsonProperty
    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public String getZdmLink() {
        return zdmLink;
    }

    public void setZdmLink(String zdmLink) {
        this.zdmLink = zdmLink;
    }

    @JsonProperty
    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    @JsonProperty
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public FitStageEnum getFitStage() {
        return fitStage;
    }

    public void setFitStage(FitStageEnum fitStage) {
        this.fitStage = fitStage;
    }

    @JsonProperty
    public Integer getFitStart() {
        return fitStart;
    }

    public void setFitStart(Integer fitStart) {
        this.fitStart = fitStart;
    }

    @JsonProperty
    public Integer getFitUnit() {
        return fitUnit;
    }

    public void setFitUnit(Integer fitUnit) {
        this.fitUnit = fitUnit;
    }

    @JsonProperty
    public String getFitPeople() {
        return fitPeople;
    }

    public void setFitPeople(String fitPeople) {
        this.fitPeople = fitPeople;
    }

    @JsonProperty
    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @JsonProperty
    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    @Column(name = "is_enable", nullable = false)
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @JsonProperty
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    @JsonProperty
    @Transient
    public String getDisplay() {
        /*
         * if (StringUtils.isNotBlank(subTitle)) { return title + "(" + subTitle
         * + ")"; }
         */
        return title;
    }

}
