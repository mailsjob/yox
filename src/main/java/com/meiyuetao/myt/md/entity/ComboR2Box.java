package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("盒子关联图片")
@Entity
@Table(name = "iyb_combo_r2_box")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ComboR2Box extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private BoxCombo boxCombo;
    private Box box;
    private BigDecimal quantity;

    @ManyToOne
    @JoinColumn(name = "BOX_COMBO_SID", nullable = false)
    public BoxCombo getBoxCombo() {
        return boxCombo;
    }

    public void setBoxCombo(BoxCombo boxCombo) {
        this.boxCombo = boxCombo;
    }

    @ManyToOne
    @JoinColumn(name = "BOX_SID", nullable = false)
    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    @Column(precision = 18, scale = 2, nullable = true)
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
