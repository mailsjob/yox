package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("盒子关联图片")
@Entity
@Table(name = "iyb_box_pic")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class BoxPic extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    /** SLIDE-盒子详情滚动图;BIG-盒子大图 */
    private String picType;
    private String picUrl;
    private Integer picIndex = 100;
    private Box box;

    @ManyToOne
    @JoinColumn(name = "BOX_SID", nullable = false)
    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    @Column(length = 8)
    public String getPicType() {
        return picType;
    }

    public void setPicType(String picType) {
        this.picType = picType;
    }

    @Column(length = 128)
    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Integer getPicIndex() {
        return picIndex;
    }

    public void setPicIndex(Integer picIndex) {
        this.picIndex = picIndex;
    }

}
