package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.constant.BizDataDictCategoryEnum;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("盒子套餐")
@Entity
@Table(name = "iyb_box_combo")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class BoxCombo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private String barcode;
    private String title;
    private BigDecimal discount;
    /**
     * 套餐分类
     * 
     * @see BizDataDictCategoryEnum.IYOUBOX_ROOT_CATEGORY
     */
    private String rootCategory;
    private String comboHead;
    /**
     * 套餐样式
     * 
     * @see BizDataDictCategoryEnum.IYOUBOX_COMBO_STYLE
     */
    private String comboStyle;
    /** 总的月份数 */
    private Integer monthCount;
    /** 最低订购月份数 */
    private Integer minOrderCount;
    /** 套餐小标题 */
    private String comboSubtitle;
    /** 套餐详情 */
    private String detail;
    /** 订购须知 */
    private String instruction;
    /** 导言 */
    private String guide;
    /** 说明 */
    private String caption;
    /** 定金数 */
    private BigDecimal cautionMoney;
    private String comboPic;
    private String promotionSlogan;
    private Integer comboRank;
    private Boolean displayFlag;
    private Integer recommendRank;

    /** 套餐与盒子的多对多关联关系 */
    private List<ComboR2Box> comboR2Boxs = new ArrayList<ComboR2Box>();

    @Column(length = 128, nullable = false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 128, unique = true)
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @OneToMany(mappedBy = "boxCombo", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ComboR2Box> getComboR2Boxs() {
        return comboR2Boxs;
    }

    public void setComboR2Boxs(List<ComboR2Box> comboR2Boxs) {
        this.comboR2Boxs = comboR2Boxs;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Column(length = 32)
    public String getRootCategory() {
        return rootCategory;
    }

    public void setRootCategory(String rootCategory) {
        this.rootCategory = rootCategory;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getComboHead() {
        return comboHead;
    }

    public void setComboHead(String comboHead) {
        this.comboHead = comboHead;
    }

    @Column(length = 16)
    public String getComboStyle() {
        return comboStyle;
    }

    public void setComboStyle(String comboStyle) {
        this.comboStyle = comboStyle;
    }

    public Integer getMonthCount() {
        return monthCount;
    }

    public void setMonthCount(Integer monthCount) {
        this.monthCount = monthCount;
    }

    public Integer getMinOrderCount() {
        return minOrderCount;
    }

    public void setMinOrderCount(Integer minOrderCount) {
        this.minOrderCount = minOrderCount;
    }

    @Column(length = 32)
    public String getComboSubtitle() {
        return comboSubtitle;
    }

    public void setComboSubtitle(String comboSubtitle) {
        this.comboSubtitle = comboSubtitle;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getCautionMoney() {
        return cautionMoney;
    }

    public void setCautionMoney(BigDecimal cautionMoney) {
        this.cautionMoney = cautionMoney;
    }

    @Column(length = 128)
    public String getComboPic() {
        return comboPic;
    }

    public void setComboPic(String comboPic) {
        this.comboPic = comboPic;
    }

    @Column(length = 32)
    public String getPromotionSlogan() {
        return promotionSlogan;
    }

    public void setPromotionSlogan(String promotionSlogan) {
        this.promotionSlogan = promotionSlogan;
    }

    public Integer getComboRank() {
        return comboRank;
    }

    public void setComboRank(Integer comboRank) {
        this.comboRank = comboRank;
    }

    public Boolean getDisplayFlag() {
        return displayFlag;
    }

    public void setDisplayFlag(Boolean displayFlag) {
        this.displayFlag = displayFlag;
    }

    public Integer getRecommendRank() {
        return recommendRank;
    }

    public void setRecommendRank(Integer recommendRank) {
        this.recommendRank = recommendRank;
    }
}
