package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("商品周期购赠品配置")
@Entity
@Table(name = "iyb_commodity_circle_present", uniqueConstraints = @UniqueConstraint(columnNames = { "commodity_sid", "present_commodity_sid", "circleMonthCount", "amount" }))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityCirclePresent extends MytBaseEntity {
    private static final long serialVersionUID = 1L;

    @MetaData("周期购正品")
    private Commodity commodity;

    @MetaData("周期购期数")
    private Integer circleMonthCount;

    @MetaData(value = "购买总金额", tooltips = "指 > 某个临界值金额")
    private BigDecimal amount;

    @MetaData(value = "送出期数", tooltips = "应该 <= 周期购期数")
    private Integer sendByCircleIndex = 1;

    @MetaData("赠品")
    private Commodity presentCommodity;

    @MetaData("赠品数量")
    private Integer quantity = 1;

    @MetaData("开始日期")
    private Date beginDate;

    @MetaData("结束日期")
    private Date endDate;

    @MetaData("是否可用")
    private Boolean enable;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public Integer getCircleMonthCount() {
        return circleMonthCount;
    }

    public void setCircleMonthCount(Integer circleMonthCount) {
        this.circleMonthCount = circleMonthCount;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonProperty
    @Column(nullable = false)
    public Integer getSendByCircleIndex() {
        return sendByCircleIndex;
    }

    public void setSendByCircleIndex(Integer sendByCircleIndex) {
        this.sendByCircleIndex = sendByCircleIndex;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "present_commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getPresentCommodity() {
        return presentCommodity;
    }

    public void setPresentCommodity(Commodity presentCommodity) {
        this.presentCommodity = presentCommodity;
    }

    @JsonProperty
    @Column(nullable = false)
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonProperty
    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    @JsonProperty
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @JsonProperty
    @Column(name = "is_enable")
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
