package com.meiyuetao.myt.md.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.sys.entity.DynaPropLineVal;

@MetaData("商品组详情")
@Entity
@Table(name = "iyb_commodity_group_detail")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityGroupDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("商品组")
    private CommodityGroup commodityGroup;
    @MetaData("一维属性")
    private DynaPropLineVal propLineValX;
    @MetaData("二维属性")
    private DynaPropLineVal propLineValY;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @ManyToOne
    @JoinColumn(name = "commodity_group_sid", nullable = false)
    public CommodityGroup getCommodityGroup() {
        return commodityGroup;
    }

    public void setCommodityGroup(CommodityGroup commodityGroup) {
        this.commodityGroup = commodityGroup;
    }

    @ManyToOne
    @JoinColumn(name = "prop_line_val_X_sid")
    @JsonProperty
    public DynaPropLineVal getPropLineValX() {
        return propLineValX;
    }

    public void setPropLineValX(DynaPropLineVal propLineValX) {
        this.propLineValX = propLineValX;
    }

    @ManyToOne
    @JoinColumn(name = "prop_line_val_Y_sid")
    @JsonProperty
    public DynaPropLineVal getPropLineValY() {
        return propLineValY;
    }

    public void setPropLineValY(DynaPropLineVal propLineValY) {
        this.propLineValY = propLineValY;
    }

}