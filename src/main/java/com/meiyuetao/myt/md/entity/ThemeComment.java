package com.meiyuetao.myt.md.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("主题清单评论")
@Entity
@Table(name = "iyb_theme_comment")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ThemeComment extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("主题清单")
    private Theme theme;
    @MetaData("客户信息")
    private CustomerProfile senderProfile;

    @MetaData("评论内容")
    private String commentContent;
    @MetaData(value = "父节点")
    private ThemeComment parent;
    @MetaData("是否可用")
    private Boolean enable = Boolean.TRUE;

    @ManyToOne
    @JoinColumn(name = "theme_sid", nullable = false)
    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    @OneToOne
    @JoinColumn(name = "sender_profile_sid")
    @JsonProperty
    public CustomerProfile getSenderProfile() {
        return senderProfile;
    }

    public void setSenderProfile(CustomerProfile senderProfile) {
        this.senderProfile = senderProfile;
    }

    @JsonProperty
    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_sid")
    @JsonIgnore
    public ThemeComment getParent() {
        return parent;
    }

    public void setParent(ThemeComment parent) {
        this.parent = parent;
    }

    @Column(name = "is_enable", nullable = false)
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

}