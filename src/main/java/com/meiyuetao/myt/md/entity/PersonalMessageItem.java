package com.meiyuetao.myt.md.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.auth.entity.User;
import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("个人消息详情")
@Entity
@Table(name = "iyb_personal_message_item")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class PersonalMessageItem extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("个人消息")
    private PersonalMessage personalMessage;
    @MetaData("目标用户")
    private User targetUser;
    @MetaData("首次查看时间")
    private Date firstReadTime;
    @MetaData("最后查看时间")
    private Date lastReadTime;
    @MetaData("总计查看次数")
    private Integer totalReadTimes = 0;
    @MetaData("是否可用")
    private Boolean enable = Boolean.TRUE;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "TARGET_USER_SID", nullable = false)
    @JsonProperty
    public User getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(User targetUser) {
        this.targetUser = targetUser;
    }

    @JsonProperty
    public Date getFirstReadTime() {
        return firstReadTime;
    }

    public void setFirstReadTime(Date firstReadTime) {
        this.firstReadTime = firstReadTime;
    }

    @JsonProperty
    public Date getLastReadTime() {
        return lastReadTime;
    }

    public void setLastReadTime(Date lastReadTime) {
        this.lastReadTime = lastReadTime;
    }

    @JsonProperty
    public Integer getTotalReadTimes() {
        return totalReadTimes;
    }

    public void setTotalReadTimes(Integer totalReadTimes) {
        this.totalReadTimes = totalReadTimes;
    }

    @ManyToOne
    @JoinColumn(name = "Personal_Message_SID", nullable = false)
    @JsonProperty
    public PersonalMessage getPersonalMessage() {
        return personalMessage;
    }

    public void setPersonalMessage(PersonalMessage personalMessage) {
        this.personalMessage = personalMessage;
    }

    @Column(name = "is_enable")
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

}