package com.meiyuetao.myt.md.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("商品管理商家自定义分类")
@Entity
@Table(name = "iyb_commodity_r2_ptcategory")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class CommodityR2Ptcategory extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private Commodity commodity;
    private PartnerCategory partnerCategory;

    @ManyToOne
    @JoinColumn(name = "commodity_sid", nullable = false)
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @ManyToOne
    @JoinColumn(name = "ptcategory_sid", nullable = false)
    public PartnerCategory getPartnerCategory() {
        return partnerCategory;
    }

    public void setPartnerCategory(PartnerCategory partnerCategory) {
        this.partnerCategory = partnerCategory;
    }

}
