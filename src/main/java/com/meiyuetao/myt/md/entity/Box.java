package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("盒子")
@Entity
@Table(name = "iyb_box")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Box extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private final static String VIRTUAL_BOX_CODE = "00000000";

    private String barcode;
    private String title;
    private String boxDescription;
    private String boxSummary;
    private BigDecimal price;
    private BigDecimal vipPrice;
    private BigDecimal marketPrice;
    private Integer score;
    private String smallPic;
    private String storeStatus;
    private String whoFit;
    private String boxMonthIndex;
    private String boxSerialIndex;

    private String boxShow;
    private String uiStyle;
    private String keywords;

    private Boolean allowSingleBuy = Boolean.TRUE;
    private Boolean isAutoBox = Boolean.FALSE;
    private String boxMonthText;
    private String richSummary;
    private Integer recommendRank;

    /** 盒子与商品的多对多关联关系 */
    private List<BoxR2Commodity> boxR2Commodities = new ArrayList<BoxR2Commodity>();

    /** 盒子关联图片集合 */
    private List<BoxPic> boxPics = new ArrayList<BoxPic>();
    /** 套餐与盒子的多对多关联关系 */
    private List<ComboR2Box> comboR2Boxs = new ArrayList<ComboR2Box>();

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Column(length = 1024)
    public String getBoxDescription() {
        return boxDescription;
    }

    public void setBoxDescription(String boxDescription) {
        this.boxDescription = boxDescription;
    }

    @Column(length = 128, unique = true)
    @JsonProperty
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @OneToMany(mappedBy = "box", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<BoxR2Commodity> getBoxR2Commodities() {
        return boxR2Commodities;
    }

    public void setBoxR2Commodities(List<BoxR2Commodity> boxR2Commodities) {
        this.boxR2Commodities = boxR2Commodities;
    }

    @OneToMany(mappedBy = "box", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ComboR2Box> getComboR2Boxs() {
        return comboR2Boxs;
    }

    public void setComboR2Boxs(List<ComboR2Box> comboR2Boxs) {
        this.comboR2Boxs = comboR2Boxs;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(BigDecimal vipPrice) {
        this.vipPrice = vipPrice;
    }

    @Column(length = 128)
    public String getSmallPic() {
        return smallPic;
    }

    public void setSmallPic(String smallPic) {
        this.smallPic = smallPic;
    }

    @Column(length = 4)
    public String getStoreStatus() {
        return storeStatus;
    }

    public void setStoreStatus(String storeStatus) {
        this.storeStatus = storeStatus;
    }

    @OneToMany(mappedBy = "box", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("picIndex desc")
    @NotAudited
    public List<BoxPic> getBoxPics() {
        return boxPics;
    }

    public void setBoxPics(List<BoxPic> boxPics) {
        this.boxPics = boxPics;
    }

    @Column(length = 128)
    public String getWhoFit() {
        return whoFit;
    }

    public void setWhoFit(String whoFit) {
        this.whoFit = whoFit;
    }

    @Column(length = 32)
    public String getBoxMonthIndex() {
        return boxMonthIndex;
    }

    public void setBoxMonthIndex(String boxMonthIndex) {
        this.boxMonthIndex = boxMonthIndex;
    }

    @Column(length = 32)
    public String getBoxSerialIndex() {
        return boxSerialIndex;
    }

    public void setBoxSerialIndex(String boxSerialIndex) {
        this.boxSerialIndex = boxSerialIndex;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getBoxShow() {
        return boxShow;
    }

    public void setBoxShow(String boxShow) {
        this.boxShow = boxShow;
    }

    @Column(length = 32)
    public String getUiStyle() {
        return uiStyle;
    }

    public void setUiStyle(String uiStyle) {
        this.uiStyle = uiStyle;
    }

    @Column(length = 512)
    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    @Column(length = 128)
    public String getBoxSummary() {
        return boxSummary;
    }

    public void setBoxSummary(String boxSummary) {
        this.boxSummary = boxSummary;
    }

    @Transient
    public String getDisplay() {
        return this.getBarcode() + "/" + this.getTitle();
    }

    @Type(type = "org.hibernate.type.NumericBooleanType")
    public Boolean getAllowSingleBuy() {
        return allowSingleBuy;
    }

    public void setAllowSingleBuy(Boolean allowSingleBuy) {
        this.allowSingleBuy = allowSingleBuy;
    }

    @Type(type = "org.hibernate.type.NumericBooleanType")
    public Boolean getIsAutoBox() {
        return isAutoBox;
    }

    public void setIsAutoBox(Boolean isAutoBox) {
        this.isAutoBox = isAutoBox;
    }

    @Column(length = 128)
    public String getBoxMonthText() {
        return boxMonthText;
    }

    public void setBoxMonthText(String boxMonthText) {
        this.boxMonthText = boxMonthText;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getRichSummary() {
        return richSummary;
    }

    public void setRichSummary(String richSummary) {
        this.richSummary = richSummary;
    }

    public Integer getRecommendRank() {
        return recommendRank;
    }

    public void setRecommendRank(Integer recommendRank) {
        this.recommendRank = recommendRank;
    }

    @Transient
    public boolean isVirtualBox() {
        return barcode.equals(VIRTUAL_BOX_CODE) ? true : false;
    }
}
