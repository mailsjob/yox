package com.meiyuetao.myt.md.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("CMS管理")
@Entity
@Table(name = "iyb_cms")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Cms extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("代码")
    private String staticId;
    @MetaData("标题")
    private String title;
    @MetaData("文本内容")
    private String htmlContent;
    @MetaData("手机文本内容")
    private String htmlContentMobile;
    @MetaData("布局")
    private String layout;
    @MetaData("手机布局")
    private String layoutMobile;
    @MetaData("分类")
    private String category;

    @MetaData("页面关键词")
    private String keyWords;

    @MetaData("页面描述")
    private String pageDescription;

    @Column(length = 32)
    @JsonProperty
    public String getStaticId() {
        return staticId;
    }

    public void setStaticId(String staticId) {
        this.staticId = staticId;
    }

    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public String getHtmlContentMobile() {
        return htmlContentMobile;
    }

    public void setHtmlContentMobile(String htmlContentMobile) {
        this.htmlContentMobile = htmlContentMobile;
    }

    @JsonProperty
    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    @JsonProperty
    public String getLayoutMobile() {
        return layoutMobile;
    }

    public void setLayoutMobile(String layoutMobile) {
        this.layoutMobile = layoutMobile;
    }

    @JsonProperty
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty
    public String getPageDescription() {
        return pageDescription;
    }

    public void setPageDescription(String pageDescription) {
        this.pageDescription = pageDescription;
    }

    @JsonProperty
    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

}
