package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("集合")
@Entity
@Table(name = "iyb_collection_object")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class CollectionObject extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("对应集合")
    private Collection collection;
    @MetaData("对象类型")
    private CollectionObjectType objectType = CollectionObjectType.COMMODITY;
    @MetaData("对象SID")
    private Long objectSid;
    @MetaData("对象SID")
    private String objectDisplay;

    @MetaData("是否可用")
    private Boolean enable = Boolean.FALSE;

    public enum CollectionObjectType {
        @MetaData("品牌")
        BRAND, @MetaData("商品")
        COMMODITY, @MetaData("分类")
        CATEGORY, @MetaData("用户")
        CUSTOMER, @MetaData("优惠券")
        GP;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    @JsonProperty
    public CollectionObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(CollectionObjectType objectType) {
        this.objectType = objectType;
    }

    @Column(length = 16, nullable = false)
    @JsonProperty
    public Long getObjectSid() {
        return objectSid;
    }

    public void setObjectSid(Long objectSid) {
        this.objectSid = objectSid;
    }

    @JsonProperty
    public String getObjectDisplay() {
        return objectDisplay;
    }

    public void setObjectDisplay(String objectDisplay) {
        this.objectDisplay = objectDisplay;
    }

    @ManyToOne
    @JoinColumn(name = "collection_sid", nullable = false)
    @JsonProperty
    public Collection getCollection() {
        return collection;
    }

    public void setCollection(Collection collection) {
        this.collection = collection;
    }

    @Column(name = "is_enable", nullable = false)
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

}
