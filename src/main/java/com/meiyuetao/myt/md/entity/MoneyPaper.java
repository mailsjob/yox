package com.meiyuetao.myt.md.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@Entity
@Table(name = "iyb_money_paper")
@MetaData("礼品卡")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MoneyPaper extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("代码")
    private String code;
    @MetaData("密码")
    private String password;
    @MetaData("名称")
    private String title;
    @MetaData("模式")
    private String paperMode = "P";
    @MetaData("状态")
    private PaperStatusEnum paperStatus = PaperStatusEnum.S10PND;
    @MetaData("开始时间")
    private Date beginTime;
    @MetaData("过期时间")
    private Date expireTime;
    @MetaData("生成时间")
    private Date genDatetime = new Date();
    @MetaData("客户")
    private CustomerProfile customerProfile;
    @MetaData("面额")
    private BigDecimal faceValue;
    @MetaData("余额")
    private BigDecimal restAmount;
    @MetaData("是否开发票")
    private Boolean receiptedInvoice = Boolean.FALSE;
    @MetaData("订单号")
    private String orderSids;
    @MetaData("分类")
    private PaperCategoryEnum paperCategory;

    public enum PaperStatusEnum {
        @MetaData("待激活")
        S10PND, @MetaData("激活状态")
        S20ACT, @MetaData("已使用")
        S30USD, @MetaData("已过期")
        S40EPR, @MetaData("已禁用")
        S50DIS;

    }

    public enum PaperCategoryEnum {
        @MetaData("全场通用")
        GENERAL;
    }

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(length = 128, nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(length = 128)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Enumerated(EnumType.STRING)
    @JsonProperty
    public PaperStatusEnum getPaperStatus() {
        return paperStatus;
    }

    public void setPaperStatus(PaperStatusEnum paperStatus) {
        this.paperStatus = paperStatus;
    }

    @JsonProperty
    @JsonSerialize(using = DateJsonSerializer.class)
    @Temporal(TemporalType.DATE)
    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    @JsonProperty
    @JsonSerialize(using = DateJsonSerializer.class)
    @Temporal(TemporalType.DATE)
    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    @Column(nullable = false)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    @Temporal(TemporalType.TIME)
    @JsonProperty
    public Date getGenDatetime() {
        return genDatetime;
    }

    public void setGenDatetime(Date genDatetime) {
        this.genDatetime = genDatetime;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getRestAmount() {
        return restAmount;
    }

    public void setRestAmount(BigDecimal restAmount) {
        this.restAmount = restAmount;
    }

    @Column(nullable = false)
    @JsonProperty
    public Boolean getReceiptedInvoice() {
        return receiptedInvoice;
    }

    public void setReceiptedInvoice(Boolean receiptedInvoice) {
        this.receiptedInvoice = receiptedInvoice;
    }

    @Column(length = 1024)
    @JsonProperty
    public String getOrderSids() {
        return orderSids;
    }

    public void setOrderSids(String orderSids) {
        this.orderSids = orderSids;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 6, nullable = false)
    @JsonProperty
    public PaperCategoryEnum getPaperCategory() {
        return paperCategory;
    }

    public void setPaperCategory(PaperCategoryEnum paperCategory) {
        this.paperCategory = paperCategory;
    }

    @JsonProperty
    public String getPaperMode() {
        return paperMode;
    }

    public void setPaperMode(String paperMode) {
        this.paperMode = paperMode;
    }

}