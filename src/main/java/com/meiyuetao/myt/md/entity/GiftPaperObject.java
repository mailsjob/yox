package com.meiyuetao.myt.md.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.activity.entity.ActivityRulesR2Collection;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import lab.s2jh.core.annotation.MetaData;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@MetaData(value = "优惠券关联对象")
@Table(name = "iyb_gift_paper_object")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class GiftPaperObject extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "优惠券")
    private GiftPaper giftPaper;
    @MetaData(value = "对象主键")
    private Long objectSid;
    @MetaData(value = "对象类型")
    private GiftPaperTypeEnum objectType;
    @MetaData(value = "是否可用")
    private Boolean enable = Boolean.TRUE;
    private String objectDisplay;
    @MetaData("白名单或黑名单")
    private ActivityRulesR2Collection.WhiteOrBlackEnum whiteOrBlack = ActivityRulesR2Collection.WhiteOrBlackEnum.WHITE;

    @ManyToOne
    @JoinColumn(name = "gift_paper_sid", nullable = false)
    public GiftPaper getGiftPaper() {
        return giftPaper;
    }

    public void setGiftPaper(GiftPaper giftPaper) {
        this.giftPaper = giftPaper;
    }

    @JsonProperty
    public Long getObjectSid() {
        return objectSid;
    }

    public void setObjectSid(Long objectSid) {
        this.objectSid = objectSid;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public GiftPaperTypeEnum getObjectType() {
        return objectType;
    }

    public void setObjectType(GiftPaperTypeEnum objectType) {
        this.objectType = objectType;
    }

    @Column(name = "is_enable", nullable = false)
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @JsonProperty
    public String getObjectDisplay() {
        return objectDisplay;
    }

    public void setObjectDisplay(String objectDisplay) {
        this.objectDisplay = objectDisplay;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "white_or_black")
    @JsonProperty
    public ActivityRulesR2Collection.WhiteOrBlackEnum getWhiteOrBlack() {
        return whiteOrBlack;
    }

    public void setWhiteOrBlack(ActivityRulesR2Collection.WhiteOrBlackEnum whiteOrBlack) {
        this.whiteOrBlack = whiteOrBlack;
    }

    public enum GiftPaperTypeEnum {
        @MetaData("商品")
        COMMODITY,
        @MetaData("分类")
        CATEGORY,
        @MetaData("商品组")
        GROUP,
        @MetaData("商品集合")
        COLLECTION;
    }

}
