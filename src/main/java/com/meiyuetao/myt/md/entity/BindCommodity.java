package com.meiyuetao.myt.md.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("外部商品对应内部商品")
@Entity
@Table(name = "iyb_bind_commodity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class BindCommodity extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联商品")
    private Commodity commodity;
    @MetaData("外部商品sku")
    private String sku;
    @MetaData("名称")
    private String name;
    @MetaData("来源")
    private ComeFromEnum comeFrom = ComeFromEnum.JD;
    @MetaData("备注")
    private String memo;

    public enum ComeFromEnum {
        @MetaData(value = "京东")
        JD;
    }

    @OneToOne
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @Column(length = 64)
    @JsonProperty
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @Column(length = 64)
    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public ComeFromEnum getComeFrom() {
        return comeFrom;
    }

    public void setComeFrom(ComeFromEnum comeFrom) {
        this.comeFrom = comeFrom;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}
