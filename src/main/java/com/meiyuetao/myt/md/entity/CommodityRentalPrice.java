package com.meiyuetao.myt.md.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import lab.s2jh.core.annotation.MetaData;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.math.BigDecimal;

@MetaData("租借商品价格表")
@Entity
@Table(name = "yox_rental_price")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommodityRentalPrice extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("价格")
    private BigDecimal price;
    @MetaData("保证金")
    private BigDecimal securityDeposit;
    @MetaData("租期")
    private Integer lease;
    @MetaData("时间类型")
    private CommodityPricePlan.TimeTypeEnum timeType;

    @ManyToOne
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @JsonProperty
    public BigDecimal getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(BigDecimal securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    @JsonProperty
    public Integer getLease() {
        return lease;
    }

    public void setLease(Integer lease) {
        this.lease = lease;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public CommodityPricePlan.TimeTypeEnum getTimeType() {
        return timeType;
    }

    public void setTimeType(CommodityPricePlan.TimeTypeEnum timeType) {
        this.timeType = timeType;
    }

}
