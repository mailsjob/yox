package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommodityVaryPriceDao;
import com.meiyuetao.myt.md.entity.CommodityVaryPrice;

@Service
@Transactional
public class CommodityVaryPriceService extends BaseService<CommodityVaryPrice, Long> {

    @Autowired
    private CommodityVaryPriceDao commodityVaryPriceDao;

    @Override
    protected BaseDao<CommodityVaryPrice, Long> getEntityDao() {
        return commodityVaryPriceDao;
    }

}
