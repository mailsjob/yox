package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.ActivityR2ObjectDao;
import com.meiyuetao.myt.md.entity.ActivityR2Object;

@Service
@Transactional
public class ActivityR2ObjectService extends BaseService<ActivityR2Object, Long> {

    @Autowired
    private ActivityR2ObjectDao activityR2ObjectDao;

    @Override
    protected BaseDao<ActivityR2Object, Long> getEntityDao() {
        return activityR2ObjectDao;
    }

}
