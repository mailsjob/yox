package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommodityDao;
import com.meiyuetao.myt.md.dao.SingleGoodsDao;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.SingleGoods;

@Service
@Transactional
public class SingleGoodsService extends BaseService<SingleGoods, Long> {

    @Autowired
    private SingleGoodsDao singleGoodsDao;
    @Autowired
    private CommodityDao commodityDao;

    @Override
    protected BaseDao<SingleGoods, Long> getEntityDao() {
        return singleGoodsDao;
    }

    @Override
    public SingleGoods save(SingleGoods entity) {
        Commodity c = commodityDao.findOne(entity.getCommodity().getId());
        // 商品标题 不设置为商品标题
        if (StringUtils.isBlank(entity.getTitle())) {
            entity.setTitle(c.getTitle());
        }
        // 单品橱窗图片 不设定默认为商品图片
        if (StringUtils.isBlank(entity.getPic())) {
            entity.setPic(c.getSmallPic());
        }
        // 单品详情描述 不设定为商品描述
        if (StringUtils.isBlank(entity.getHtmlContent())) {
            entity.setHtmlContent(c.getSpecification());
        }

        return super.save(entity);
    }

}
