package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.RecommendFittingDao;
import com.meiyuetao.myt.md.entity.RecommendFitting;

@Service
@Transactional
public class RecommendFittingService extends BaseService<RecommendFitting, Long> {

    @Autowired
    private RecommendFittingDao recommendFittingDao;

    @Override
    protected BaseDao<RecommendFitting, Long> getEntityDao() {
        return recommendFittingDao;
    }

}
