package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.MoneyPaperHistoryDao;
import com.meiyuetao.myt.md.entity.MoneyPaperHistory;

@Service
@Transactional
public class MoneyPaperHistoryService extends BaseService<MoneyPaperHistory, Long> {

    @Autowired
    private MoneyPaperHistoryDao moneyPaperHistoryDao;

    @Override
    protected BaseDao<MoneyPaperHistory, Long> getEntityDao() {
        return moneyPaperHistoryDao;
    }
}
