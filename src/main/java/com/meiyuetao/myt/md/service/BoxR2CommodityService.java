package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.BoxR2CommodityDao;
import com.meiyuetao.myt.md.entity.BoxR2Commodity;

@Service
@Transactional
public class BoxR2CommodityService extends BaseService<BoxR2Commodity, Long> {

    @Autowired
    private BoxR2CommodityDao boxR2CommodityDao;

    @Override
    protected BaseDao<BoxR2Commodity, Long> getEntityDao() {
        return boxR2CommodityDao;
    }

}
