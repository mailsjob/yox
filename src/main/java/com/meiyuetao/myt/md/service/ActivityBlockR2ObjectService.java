package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.ActivityBlockR2ObjectDao;
import com.meiyuetao.myt.md.entity.ActivityBlockR2Object;

@Service
@Transactional
public class ActivityBlockR2ObjectService extends BaseService<ActivityBlockR2Object, Long> {

    @Autowired
    private ActivityBlockR2ObjectDao activityBlockR2ObjectDao;

    @Override
    protected BaseDao<ActivityBlockR2Object, Long> getEntityDao() {
        return activityBlockR2ObjectDao;
    }

}
