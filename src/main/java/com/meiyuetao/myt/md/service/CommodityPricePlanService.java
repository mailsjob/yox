package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommodityPricePlanDao;
import com.meiyuetao.myt.md.entity.CommodityPricePlan;

@Service
@Transactional
public class CommodityPricePlanService extends BaseService<CommodityPricePlan, Long> {

    @Autowired
    private CommodityPricePlanDao commodityPricePlanDao;

    @Override
    protected BaseDao<CommodityPricePlan, Long> getEntityDao() {
        return commodityPricePlanDao;
    }

}
