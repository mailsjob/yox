package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.BoxPicDao;
import com.meiyuetao.myt.md.entity.BoxPic;

@Service
@Transactional
public class BoxPicService extends BaseService<BoxPic, Long> {

    @Autowired
    private BoxPicDao boxPicDao;

    @Override
    protected BaseDao<BoxPic, Long> getEntityDao() {
        return boxPicDao;
    }

}
