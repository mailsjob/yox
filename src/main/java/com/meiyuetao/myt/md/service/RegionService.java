package com.meiyuetao.myt.md.service;

import java.util.Collection;
import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.meiyuetao.myt.md.dao.RegionDao;
import com.meiyuetao.myt.md.entity.Region;

@Service
@Transactional
public class RegionService extends BaseService<Region, Long> {

    @Autowired
    private RegionDao regionDao;

    @Override
    public BaseDao<Region, Long> getEntityDao() {
        return regionDao;
    }

    public List<Region> findAllCached() {
        return regionDao.findAllCached();
    }

    @Transactional(readOnly = true)
    @Cacheable(value = "MytRegionSpringCache", key = "#root.methodName")
    public Collection<Region> findTreeDatas() {
        List<Region> roots = findRoots();
        for (Region item : roots) {
            loopTreeGridData(item);
        }
        return roots;
    }

    private void loopTreeGridData(Region item) {
        List<Region> children = findChildren(item);
        if (!CollectionUtils.isEmpty(children)) {
            item.setChildren(children);
        }
    }

    @Transactional(readOnly = true)
    @Cacheable(value = "MytRegionSpringCache", key = "#root.methodName")
    public List<Region> findRoots() {
        List<Region> roots = Lists.newArrayList();
        List<Region> allItems = regionDao.findAllCached();
        for (Region item : allItems) {
            if (item.getParent() != null && item.getParent().getId() == 0) {
                roots.add(item);
            }
        }
        // Collections.sort(roots);
        return roots;
    }

    @Transactional(readOnly = true)
    public List<Region> findChildren(Region parent) {
        List<Region> items = Lists.newArrayList();
        List<Region> allItems = regionDao.findAllCached();
        for (Region item : allItems) {
            if (item.getParent() == parent) {
                items.add(item);
            }
        }
        // Collections.sort(items);
        return items;
    }

    @Override
    @CacheEvict(value = "MytRegionSpringCache", allEntries = true)
    public Region save(Region entity) {
        return super.save(entity);
    }

    @Override
    @CacheEvict(value = "MytRegionSpringCache", allEntries = true)
    public List<Region> save(Iterable<Region> entities) {
        return super.save(entities);
    }

    @Override
    @CacheEvict(value = "MytRegionSpringCache", allEntries = true)
    public void delete(Region entity) {
        super.delete(entity);
    }

    @Override
    @CacheEvict(value = "MytRegionSpringCache", allEntries = true)
    public void delete(Iterable<Region> entities) {
        super.delete(entities);
    }

    public String findByName(String name) {
        return regionDao.findByName("%" + name + "%").getPairui();
    }
}
