package com.meiyuetao.myt.md.service;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.meiyuetao.myt.md.dao.GiftPaperDao;
import com.meiyuetao.myt.md.entity.GiftPaper;

@Service
@Transactional
public class GiftPaperService extends BaseService<GiftPaper, Long> {

    @Autowired
    private GiftPaperDao giftPaperDao;

    @Override
    protected BaseDao<GiftPaper, Long> getEntityDao() {
        return giftPaperDao;
    }

    public List<GiftPaper> findPaperTemplates() {

        return giftPaperDao.findPaperTemplates();
    }

    public String createUniqueCode() {
        String code = RandomStringUtils.randomAlphabetic(12).toUpperCase();
        while (!CollectionUtils.isEmpty(giftPaperDao.findByCode(code))) {
            code = RandomStringUtils.randomAlphabetic(12).toUpperCase();
        }
        return code;

    }
}
