package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.FloorAdDao;
import com.meiyuetao.myt.md.entity.FloorAd;

@Service
@Transactional
public class FloorAdService extends BaseService<FloorAd, Long> {

    @Autowired
    private FloorAdDao floorAdDao;

    @Override
    protected BaseDao<FloorAd, Long> getEntityDao() {
        return floorAdDao;
    }
}
