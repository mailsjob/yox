package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.BoxDao;
import com.meiyuetao.myt.md.entity.Box;

@Service
@Transactional
public class BoxService extends BaseService<Box, Long> {

    @Autowired
    private BoxDao boxDao;

    @Override
    protected BaseDao<Box, Long> getEntityDao() {
        return boxDao;
    }

    public Box getTop() {
        return boxDao.getTop();
    }

}
