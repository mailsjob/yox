package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommoditySkuRelationDao;
import com.meiyuetao.myt.md.entity.CommoditySkuRelation;

@Service
@Transactional
public class CommoditySkuRelationService extends BaseService<CommoditySkuRelation, Long> {

    @Autowired
    private CommoditySkuRelationDao commoditySkuRelationDao;

    @Override
    protected BaseDao<CommoditySkuRelation, Long> getEntityDao() {
        return commoditySkuRelationDao;
    }
}
