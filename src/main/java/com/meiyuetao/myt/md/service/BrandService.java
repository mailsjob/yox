package com.meiyuetao.myt.md.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.meiyuetao.myt.md.dao.BrandDao;
import com.meiyuetao.myt.md.entity.Brand;

@Service
@Transactional
public class BrandService extends BaseService<Brand, Long> {

    @Autowired
    private BrandDao brandDao;

    @Override
    protected BaseDao<Brand, Long> getEntityDao() {
        return brandDao;
    }

    public List<Brand> findAllCached() {
        return brandDao.findAllCached();
    }

    public Map<Brand, Set<String>> getAllBrandTokens() {
        Map<Brand, Set<String>> brandTokensMap = Maps.newHashMap();
        List<Brand> brands = findAllCached();
        for (Brand brand : brands) {
            Set<String> brandTokens = null;
            if (StringUtils.isNotBlank(brand.getSynonyms())) {
                brandTokens = Sets.newHashSet(brand.getSynonyms().split(","));
            } else {
                brandTokens = Sets.newHashSet();
            }
            brandTokens.add(brand.getTitle());
            brandTokensMap.put(brand, brandTokens);
        }
        return brandTokensMap;
    }
}
