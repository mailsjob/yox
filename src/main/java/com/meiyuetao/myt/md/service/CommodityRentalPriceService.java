package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import com.meiyuetao.myt.md.entity.CommodityRentalPrice;
import com.meiyuetao.myt.md.dao.CommodityRentalPriceDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CommodityRentalPriceService extends BaseService<CommodityRentalPrice,Long>{
    
    @Autowired
    private CommodityRentalPriceDao commodityRentalPriceDao;

    @Override
    protected BaseDao<CommodityRentalPrice, Long> getEntityDao() {
        return commodityRentalPriceDao;
    }
}
