package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.MoneyPaperDao;
import com.meiyuetao.myt.md.entity.MoneyPaper;

@Service
@Transactional
public class MoneyPaperService extends BaseService<MoneyPaper, Long> {

    @Autowired
    private MoneyPaperDao moneyPaperDao;

    @Override
    protected BaseDao<MoneyPaper, Long> getEntityDao() {
        return moneyPaperDao;
    }
}
