package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.ThemeCommentDao;
import com.meiyuetao.myt.md.entity.ThemeComment;

@Service
@Transactional
public class ThemeCommentService extends BaseService<ThemeComment, Long> {

    @Autowired
    private ThemeCommentDao themeCommentDao;

    @Override
    protected BaseDao<ThemeComment, Long> getEntityDao() {
        return themeCommentDao;
    }

}
