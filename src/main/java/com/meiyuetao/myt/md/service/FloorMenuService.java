package com.meiyuetao.myt.md.service;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.FloorMenuDao;
import com.meiyuetao.myt.md.entity.Floor;
import com.meiyuetao.myt.md.entity.FloorMenu;

@Service
@Transactional
public class FloorMenuService extends BaseService<FloorMenu, Long> {

    @Autowired
    private FloorMenuDao floorMenuDao;

    @Override
    protected BaseDao<FloorMenu, Long> getEntityDao() {
        return floorMenuDao;
    }

    public List<FloorMenu> findByFloor(Floor floor) {

        return floorMenuDao.findByFloor(floor);
    }

    public List<FloorMenu> findAllCached() {
        return floorMenuDao.findAllCached();
    }
}
