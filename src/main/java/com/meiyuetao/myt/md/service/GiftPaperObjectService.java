package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.GiftPaperObjectDao;
import com.meiyuetao.myt.md.entity.GiftPaperObject;

@Service
@Transactional
public class GiftPaperObjectService extends BaseService<GiftPaperObject, Long> {

    @Autowired
    private GiftPaperObjectDao giftPaperObjectDao;

    @Override
    protected BaseDao<GiftPaperObject, Long> getEntityDao() {
        return giftPaperObjectDao;
    }

}
