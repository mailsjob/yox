package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.AdDao;
import com.meiyuetao.myt.md.entity.Ad;

@Service
@Transactional
public class AdService extends BaseService<Ad, Long> {

    @Autowired
    private AdDao adDao;

    @Override
    protected BaseDao<Ad, Long> getEntityDao() {
        return adDao;
    }

}
