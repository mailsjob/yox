package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.SmzdmDao;
import com.meiyuetao.myt.md.entity.Smzdm;

@Service
@Transactional
public class SmzdmService extends BaseService<Smzdm, Long> {

    @Autowired
    private SmzdmDao smzdmDao;

    @Override
    protected BaseDao<Smzdm, Long> getEntityDao() {
        return smzdmDao;
    }

}
