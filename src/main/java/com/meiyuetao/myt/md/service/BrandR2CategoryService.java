package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.BrandR2CategoryDao;
import com.meiyuetao.myt.md.entity.BrandR2Category;

@Service
@Transactional
public class BrandR2CategoryService extends BaseService<BrandR2Category, Long> {

    @Autowired
    private BrandR2CategoryDao brandR2CategoryDao;

    @Override
    protected BaseDao<BrandR2Category, Long> getEntityDao() {
        return brandR2CategoryDao;
    }

}
