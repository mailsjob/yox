package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.md.dao.CommodityR2PtcategoryDao;
import com.meiyuetao.myt.md.entity.CommodityR2Ptcategory;

public class CommodityR2PtcategoryService extends BaseService<CommodityR2Ptcategory, Long> {
    @Autowired
    private CommodityR2PtcategoryDao commodityR2PtcategoryDao;

    @Override
    protected BaseDao<CommodityR2Ptcategory, Long> getEntityDao() {
        // TODO Auto-generated method stub
        return commodityR2PtcategoryDao;
    }

}
