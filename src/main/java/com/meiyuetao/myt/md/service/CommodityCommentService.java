package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommodityCommentDao;
import com.meiyuetao.myt.md.entity.CommodityComment;

@Service
@Transactional
public class CommodityCommentService extends BaseService<CommodityComment, Long> {

    @Autowired
    private CommodityCommentDao commodityCommentDao;

    @Override
    protected BaseDao<CommodityComment, Long> getEntityDao() {
        return commodityCommentDao;
    }
}
