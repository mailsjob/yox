package com.meiyuetao.myt.md.service;

import java.util.Collections;
import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.meiyuetao.myt.md.dao.PartnerCategoryDao;
import com.meiyuetao.myt.md.entity.PartnerCategory;
import com.meiyuetao.myt.partner.entity.Partner;

@Service
@Transactional
public class PartnerCategoryService extends BaseService<PartnerCategory, Long> {

    @Autowired
    private PartnerCategoryDao partnerCategoryDao;

    @Override
    protected BaseDao<PartnerCategory, Long> getEntityDao() {
        return partnerCategoryDao;
    }

    @Transactional(readOnly = true)
    public List<PartnerCategory> findAllSorted(Partner partner) {

        List<PartnerCategory> allPartnerItems = partnerCategoryDao.findByPartner(partner);

        // 计算排序Root集合数据
        List<PartnerCategory> sortedAllItems = Lists.newArrayList();
        List<PartnerCategory> sortedRootItems = Lists.newArrayList();
        for (PartnerCategory item : allPartnerItems) {
            if (item.getParent() == null) {
                sortedRootItems.add(item);
            }
        }
        Collections.sort(sortedRootItems);

        loopTreeData(partner, allPartnerItems, sortedAllItems, sortedRootItems);
        return sortedAllItems;
    }

    private void loopTreeData(Partner partner, List<PartnerCategory> allPartnerItems, List<PartnerCategory> sortedItems, List<PartnerCategory> curItems) {
        if (CollectionUtils.isEmpty(curItems)) {
            return;
        }
        // 计算当前批次所属层级
        int level = 0;
        PartnerCategory levelItem = curItems.get(0).getParent();
        while (levelItem != null) {
            level++;
            levelItem = levelItem.getParent();
        }

        for (PartnerCategory curItem : curItems) {
            sortedItems.add(curItem);

            // 计算排序Children集合数据
            List<PartnerCategory> children = Lists.newArrayList();
            for (PartnerCategory item : allPartnerItems) {
                if (item.getParent() != null && curItem.getId().equals(item.getParent().getId())) {
                    children.add(item);
                }
            }
            Collections.sort(children);

            curItem.addExtraAttribute("level", level);
            curItem.addExtraAttribute("parent", curItem.getParent() == null ? "" : curItem.getParent().getId());
            curItem.addExtraAttribute("isLeaf", CollectionUtils.isEmpty(children) ? true : false);
            curItem.addExtraAttribute("expanded", true);
            curItem.addExtraAttribute("loaded", true);
            loopTreeData(partner, allPartnerItems, sortedItems, children);
        }
    }
}
