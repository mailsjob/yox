package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.LinkDao;
import com.meiyuetao.myt.md.entity.Link;

@Service
@Transactional
public class LinkService extends BaseService<Link, Long> {

    @Autowired
    private LinkDao linkDao;

    @Override
    protected BaseDao<Link, Long> getEntityDao() {
        return linkDao;
    }

}
