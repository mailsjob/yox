package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.PersonalMessageItemDao;
import com.meiyuetao.myt.md.entity.PersonalMessageItem;

@Service
@Transactional
public class PersonalMessageItemService extends BaseService<PersonalMessageItem, Long> {

    @Autowired
    private PersonalMessageItemDao personalMessageItemDao;

    @Override
    protected BaseDao<PersonalMessageItem, Long> getEntityDao() {
        return personalMessageItemDao;
    }
}
