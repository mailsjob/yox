package com.meiyuetao.myt.md.service;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.FloorDao;
import com.meiyuetao.myt.md.entity.Floor;

@Service
@Transactional
public class FloorService extends BaseService<Floor, Long> {

    @Autowired
    private FloorDao floorDao;

    @Override
    protected BaseDao<Floor, Long> getEntityDao() {
        return floorDao;
    }

    public List<Floor> findAllCached() {
        return floorDao.findAllCached();
    }
}
