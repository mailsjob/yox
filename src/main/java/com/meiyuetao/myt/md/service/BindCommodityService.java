package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.BindCommodityDao;
import com.meiyuetao.myt.md.entity.BindCommodity;

@Service
@Transactional
public class BindCommodityService extends BaseService<BindCommodity, Long> {

    @Autowired
    private BindCommodityDao bindCommodityDao;

    @Override
    protected BaseDao<BindCommodity, Long> getEntityDao() {
        return bindCommodityDao;
    }

}
