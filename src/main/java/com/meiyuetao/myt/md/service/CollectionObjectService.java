package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CollectionObjectDao;
import com.meiyuetao.myt.md.entity.CollectionObject;

@Service
@Transactional
public class CollectionObjectService extends BaseService<CollectionObject, Long> {

    @Autowired
    private CollectionObjectDao collectionObjectDao;

    @Override
    protected BaseDao<CollectionObject, Long> getEntityDao() {
        return collectionObjectDao;
    }
}
