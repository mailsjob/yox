package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommodityPicDao;
import com.meiyuetao.myt.md.entity.CommodityPic;

@Service
@Transactional
public class CommodityPicService extends BaseService<CommodityPic, Long> {

    @Autowired
    private CommodityPicDao commodityPicDao;

    @Override
    protected BaseDao<CommodityPic, Long> getEntityDao() {
        return commodityPicDao;
    }
}
