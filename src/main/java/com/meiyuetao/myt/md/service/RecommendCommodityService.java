package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.RecommendCommodityDao;
import com.meiyuetao.myt.md.entity.RecommendCommodity;

@Service
@Transactional
public class RecommendCommodityService extends BaseService<RecommendCommodity, Long> {

    @Autowired
    private RecommendCommodityDao recommendCommodityDao;

    @Override
    protected BaseDao<RecommendCommodity, Long> getEntityDao() {
        return recommendCommodityDao;
    }
}
