package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.ActivityDao;
import com.meiyuetao.myt.md.entity.Activity;

@Service
@Transactional
public class ActivityService extends BaseService<Activity, Long> {

    @Autowired
    private ActivityDao activityDao;

    @Override
    protected BaseDao<Activity, Long> getEntityDao() {
        return activityDao;
    }

}
