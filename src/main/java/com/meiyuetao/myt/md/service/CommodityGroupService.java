package com.meiyuetao.myt.md.service;

import java.util.Iterator;
import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.meiyuetao.myt.md.dao.CommodityDao;
import com.meiyuetao.myt.md.dao.CommodityGroupDao;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.CommodityGroup;
import com.meiyuetao.myt.md.entity.CommodityGroupDetail;
import com.meiyuetao.myt.sys.dao.DynaPropLineValDao;
import com.meiyuetao.myt.sys.entity.DynaPropLineVal;

@Service
@Transactional
public class CommodityGroupService extends BaseService<CommodityGroup, Long> {

    @Autowired
    private CommodityGroupDao commodityGroupDao;
    @Autowired
    private DynaPropLineValDao dynaPropLineValDao;
    @Autowired
    private CommodityDao commodityDao;
    @Autowired
    private CommodityGroupDetailService commodityGroupDetailService;

    @Override
    protected BaseDao<CommodityGroup, Long> getEntityDao() {
        return commodityGroupDao;
    }

    public void saveDetails(CommodityGroup entity, CommodityGroupDetail[] cgDetails1D, CommodityGroupDetail[][] cgDetails2D) {
        List<CommodityGroupDetail> commodityGroupDetails = entity.getCommodityGroupDetails();
        for (int i = 0; i < cgDetails1D.length; i++) {
            if (cgDetails1D[i] != null) {
                CommodityGroupDetail cgdi = cgDetails1D[i];
                Assert.isTrue(cgdi.getCommodity() != null, "一维属性必须维护");
                for (CommodityGroupDetail commodityGroupDetail : commodityGroupDetails) {
                    if (commodityGroupDetail.getPropLineValY() == null && commodityGroupDetail.getPropLineValX() != null
                            && commodityGroupDetail.getPropLineValX().equals(cgdi.getPropLineValX())) {
                        if (commodityGroupDetail.getCommodity().getId() != cgdi.getCommodity().getId()) {
                            Commodity oldCommodity = commodityGroupDetail.getCommodity();
                            oldCommodity.setCommodityGroup(null);
                            commodityDao.save(oldCommodity);
                            Commodity newCommodity = commodityDao.findOne(cgdi.getCommodity().getId());
                            commodityGroupDetail.setCommodity(newCommodity);
                        }
                    }
                }
            }
        }
        for (int j = 0; j < cgDetails2D.length; j++) {
            for (int k = 0; k < cgDetails2D[j].length; k++) {
                if (cgDetails2D[j][k] != null) {
                    CommodityGroupDetail cgdjk = cgDetails2D[j][k];
                    Boolean exist = false;
                    for (Iterator<CommodityGroupDetail> cgds = commodityGroupDetails.iterator(); cgds.hasNext();) {
                        CommodityGroupDetail commodityGroupDetail = (CommodityGroupDetail) cgds.next();
                        if (commodityGroupDetail.getPropLineValY() != null && commodityGroupDetail.getPropLineValX() != null
                                && commodityGroupDetail.getPropLineValX().equals(cgdjk.getPropLineValX()) && commodityGroupDetail.getPropLineValY().equals(cgdjk.getPropLineValY())) {
                            exist = true;
                            // 删除
                            if (cgdjk.getCommodity() == null) {
                                cgds.remove();
                            } else if (commodityGroupDetail.getCommodity().getId() != cgdjk.getCommodity().getId()) {// 更新
                                Commodity oldCommodity = commodityGroupDetail.getCommodity();
                                oldCommodity.setCommodityGroup(null);
                                commodityDao.save(oldCommodity);
                                Commodity newCommodity = commodityDao.findOne(cgdjk.getCommodity().getId());
                                commodityGroupDetail.setCommodity(newCommodity);
                            }
                        }
                    }
                    if (!exist && cgdjk.getCommodity() != null) {
                        Commodity newCommodity = commodityDao.findOne(cgdjk.getCommodity().getId());
                        cgdjk.setCommodity(newCommodity);
                        commodityGroupDetails.add(cgdjk);

                    }
                }

            }
        }
        // commodityGroupDetails.clear();
        /*
         * for (int i = 0; i < cgDetails1D.length; i++) { CommodityGroupDetail
         * cgdi = cgDetails1D[i]; if (cgdi.getCommodity() != null) { Commodity
         * commodity = commodityDao.findOne(cgdi.getCommodity().getId());
         * cgdi.setCommodity(commodity); cgdi.setCommodityGroup(entity);
         * commodityGroupDetails.add(cgdi); } } for (int j = 0; j <
         * cgDetails2D.length; j++) { for (int k = 0; k < cgDetails2D[j].length;
         * k++) { if (cgDetails2D[j][k] != null) { CommodityGroupDetail cgdjk =
         * cgDetails2D[j][k]; if (cgdjk.getCommodity() != null) { Commodity
         * commodity = commodityDao.findOne(cgdjk.getCommodity().getId());
         * cgdjk.setCommodity(commodity); cgdjk.setCommodityGroup(entity);
         * commodityGroupDetails.add(cgdjk); } }
         * 
         * } }
         */
        commodityGroupDao.save(entity);

    }

    public void updateDetailsByProps(CommodityGroup bindingEntity) {
        List<CommodityGroupDetail> commodityGroupDetails = bindingEntity.getCommodityGroupDetails();
        // 取得必须维护的一维数组
        List<DynaPropLineVal> dPLVListX1D = commodityGroupDetailService.findOneArray(bindingEntity, 0);
        // 查找商品组对应的GroupCommodityDetail一维度数组,参数二表示GroupCommodityDetail.propLineValY是否存在
        // List<CommodityGroupDetail> cgDs1D =
        // commodityGroupDetailService.findCGDArray(bindingEntity, false);
        List<DynaPropLineVal> dPLVListY1D = commodityGroupDetailService.findOneArray(bindingEntity, 1);
        for (DynaPropLineVal dplv1DX : dPLVListX1D) {
            Boolean exist = false;
            for (CommodityGroupDetail cgd : commodityGroupDetails) {
                if (cgd.getPropLineValX().equals(dplv1DX)) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                for (DynaPropLineVal dplv1DY : dPLVListY1D) {
                    CommodityGroupDetail commodityGroupDetail = new CommodityGroupDetail();
                    commodityGroupDetail.setCommodityGroup(bindingEntity);
                    commodityGroupDetail.setPropLineValX(dplv1DX);
                    commodityGroupDetail.setPropLineValY(dplv1DY);
                    commodityGroupDetails.add(commodityGroupDetail);
                }
                CommodityGroupDetail commodityGroupDetail = new CommodityGroupDetail();
                commodityGroupDetail.setCommodityGroup(bindingEntity);
                commodityGroupDetail.setPropLineValX(dplv1DX);
                commodityGroupDetail.setPropLineValY(null);
                commodityGroupDetails.add(commodityGroupDetail);
            }
        }
        for (Iterator<CommodityGroupDetail> cgds = commodityGroupDetails.iterator(); cgds.hasNext();) {
            CommodityGroupDetail cgd = (CommodityGroupDetail) cgds.next();
            Boolean exist = false;
            for (DynaPropLineVal dplv1DX : dPLVListX1D) {
                if (cgd.getPropLineValX().equals(dplv1DX)) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                Commodity commodity = cgd.getCommodity();
                if (commodity != null) {
                    commodity.setCommodityGroup(null);
                    commodityDao.save(commodity);
                }
                cgds.remove();
            }
        }

        for (DynaPropLineVal dplv1DY : dPLVListY1D) {
            Boolean exist = false;
            for (CommodityGroupDetail cgd : commodityGroupDetails) {
                if (dplv1DY.equals(cgd.getPropLineValY())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                for (DynaPropLineVal dplv1DX : dPLVListX1D) {
                    CommodityGroupDetail commodityGroupDetail = new CommodityGroupDetail();
                    commodityGroupDetail.setCommodityGroup(bindingEntity);
                    commodityGroupDetail.setPropLineValX(dplv1DX);
                    commodityGroupDetail.setPropLineValY(dplv1DY);
                    commodityGroupDetails.add(commodityGroupDetail);
                }

            }
        }
        for (Iterator<CommodityGroupDetail> cgds = commodityGroupDetails.iterator(); cgds.hasNext();) {
            CommodityGroupDetail cgd = (CommodityGroupDetail) cgds.next();
            Boolean exist = false;
            if (cgd.getPropLineValY() != null) {
                for (DynaPropLineVal dplv1DY : dPLVListY1D) {
                    if (dplv1DY.equals(cgd.getPropLineValY())) {
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    Commodity commodity = cgd.getCommodity();
                    if (commodity != null) {
                        commodity.setCommodityGroup(null);
                        commodityDao.save(commodity);
                    }
                    cgds.remove();
                }
            }
        }
        bindingEntity.setCommodityGroupDetails(commodityGroupDetails);
        commodityGroupDao.save(bindingEntity);
    }

    @Override
    public void delete(CommodityGroup entity) {
        /*
         * for (CommodityGroupDetail commodityGroupDetail :
         * entity.getCommodityGroupDetails()) { Commodity commodity =
         * commodityGroupDetail.getCommodity(); if (commodity != null) {
         * commodity.setCommodityGroup(null); commodityDao.save(commodity); } }
         */
        super.delete(entity);
        /*
         * List<DynaPropLineVal> dynaPropLineVals =
         * dynaPropLineValDao.findByObjNameAndObjSid("COMMODITY_GROUP",
         * entity.getId()); List<DynaPropLineVal> parentDynaPropLineVals = new
         * ArrayList<DynaPropLineVal>(); List<DynaPropLineVal>
         * childrenDynaPropLineVals = new ArrayList<DynaPropLineVal>(); for
         * (DynaPropLineVal dynaPropLineVal : dynaPropLineVals) { if
         * (dynaPropLineVal.getChildren() == null) {
         * childrenDynaPropLineVals.add(dynaPropLineVal); } else {
         * parentDynaPropLineVals.add(dynaPropLineVal); } }
         * dynaPropLineValDao.delete(childrenDynaPropLineVals);
         * dynaPropLineValDao.delete(dynaPropLineVals);
         */

    }

}
