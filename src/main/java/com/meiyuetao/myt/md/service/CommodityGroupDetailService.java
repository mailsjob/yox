package com.meiyuetao.myt.md.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommodityGroupDetailDao;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.CommodityGroup;
import com.meiyuetao.myt.md.entity.CommodityGroupDetail;
import com.meiyuetao.myt.sys.entity.DynaPropLineVal;
import com.meiyuetao.myt.sys.service.DynaPropLineValService;

@Service
@Transactional
public class CommodityGroupDetailService extends BaseService<CommodityGroupDetail, Long> {

    @Autowired
    private CommodityGroupDetailDao commodityGroupDetailDao;
    @Autowired
    private DynaPropLineValService dynaPropLineValService;
    @Autowired
    private CommodityService commodityService;

    @Override
    protected BaseDao<CommodityGroupDetail, Long> getEntityDao() {
        return commodityGroupDetailDao;
    }

    @Override
    public CommodityGroupDetail save(CommodityGroupDetail entity) {
        CommodityGroupDetail dbCommodityGroupDetail = commodityGroupDetailDao.findOne(entity.getId());
        List<CommodityGroupDetail> commodityGroupDetails = dbCommodityGroupDetail.getCommodityGroup().getCommodityGroupDetails();
        Commodity dbCommodity = dbCommodityGroupDetail.getCommodity();
        if (dbCommodity == null) {
            if (entity.getCommodity() != null) {
                Commodity commodity = commodityService.findOne(entity.getCommodity().getId());
                commodity.setCommodityGroup(entity.getCommodityGroup());
                commodityService.save(commodity);
            }

        } else {
            if (entity.getCommodity() != null) {
                Commodity commodity = commodityService.findOne(entity.getCommodity().getId());
                if (!commodity.equals(dbCommodity)) {
                    commodity.setCommodityGroup(entity.getCommodityGroup());
                    commodityService.save(commodity);
                    int i = 0;
                    for (CommodityGroupDetail commodityGroupDetail : commodityGroupDetails) {
                        if (dbCommodity.equals(commodityGroupDetail.getCommodity())) {
                            i++;
                        }
                    }
                    if (i < 2) {
                        dbCommodity.setCommodityGroup(null);
                        commodityService.save(dbCommodity);
                    }
                }
            } else {
                int i = 0;
                for (CommodityGroupDetail commodityGroupDetail : commodityGroupDetails) {
                    if (dbCommodity.equals(commodityGroupDetail.getCommodity())) {
                        i++;
                    }
                }
                if (i < 2) {
                    dbCommodity.setCommodityGroup(null);
                    commodityService.save(dbCommodity);
                }
            }

        }
        return super.save(entity);
    }

    public List<DynaPropLineVal> findOneArray(CommodityGroup commodityGroup, int i) {
        // 举例（parent：color；children：yellow、blue、red……
        // ）先取得的排序号最大的并没有被disabled的parent，再取parent下面没有被disabled的children
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "objName", "COMMODITY_GROUP"));
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "objSid", commodityGroup.getId()));
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "disabled", Boolean.FALSE));
        groupPropertyFilter.append(new PropertyFilter(MatchType.NU, "parent", true));
        List<DynaPropLineVal> parentList = dynaPropLineValService.findByFilters(groupPropertyFilter);
        Collections.sort(parentList);
        DynaPropLineVal dPLVXY = null;
        if (parentList.size() >= i + 1) {
            dPLVXY = parentList.get(i);
        }
        List<DynaPropLineVal> dPLVList1D = new ArrayList<DynaPropLineVal>();
        if (dPLVXY != null) {
            groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
            groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "parent", dPLVXY));
            groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "disabled", Boolean.FALSE));
            dPLVList1D = dynaPropLineValService.findByFilters(groupPropertyFilter);
            Collections.sort(dPLVList1D);
        }

        return dPLVList1D;
    }

    /*
     * public List<CommodityGroupDetail> findCGDArray(CommodityGroup
     * commodityGroup, boolean isExistPropLineValY) { GroupPropertyFilter
     * groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
     * groupPropertyFilter.append(new PropertyFilter(MatchType.EQ,
     * "commodityGroup", commodityGroup)); groupPropertyFilter.append(new
     * PropertyFilter(MatchType.NN, "propLineValX", true));
     * groupPropertyFilter.append(new PropertyFilter(MatchType.NN,
     * "propLineValY", isExistPropLineValY)); List<CommodityGroupDetail>
     * cgDArray =
     * commodityGroupDetailService2.findByFilters(groupPropertyFilter); return
     * commodityGroupDetailDao.findCGDArray(commodityGroup,
     * isExistPropLineValY); }
     */

}
