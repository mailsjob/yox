package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.core.service.SolrService;
import com.meiyuetao.myt.md.dao.ThemeDao;
import com.meiyuetao.myt.md.entity.Theme;

@Service
@Transactional
public class ThemeService extends BaseService<Theme, Long> {

    @Autowired
    private ThemeDao themeDao;
    @Autowired
    private SolrService solrService;

    @Override
    protected BaseDao<Theme, Long> getEntityDao() {
        return themeDao;
    }

    @Override
    public Theme save(Theme entity) {
        // 异步更新Solr索引
        super.save(entity);
        solrService.themeIndexAsync();
        return entity;
    }

}
