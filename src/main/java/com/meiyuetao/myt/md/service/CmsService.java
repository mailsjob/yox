package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CmsDao;
import com.meiyuetao.myt.md.entity.Cms;

@Service
@Transactional
public class CmsService extends BaseService<Cms, Long> {

    @Autowired
    private CmsDao cmsDao;

    @Override
    protected BaseDao<Cms, Long> getEntityDao() {
        return cmsDao;
    }
}
