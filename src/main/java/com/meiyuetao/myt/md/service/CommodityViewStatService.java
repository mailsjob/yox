package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommodityViewStatDao;
import com.meiyuetao.myt.md.entity.CommodityViewStat;

@Service
@Transactional
public class CommodityViewStatService extends BaseService<CommodityViewStat, Long> {

    @Autowired
    private CommodityViewStatDao commodityViewStatDao;

    @Override
    protected BaseDao<CommodityViewStat, Long> getEntityDao() {
        return commodityViewStatDao;
    }

}