package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.SearchKeyWordDao;
import com.meiyuetao.myt.md.entity.SearchKeyWord;

@Service
@Transactional
public class SearchKeyWordService extends BaseService<SearchKeyWord, Long> {

    @Autowired
    private SearchKeyWordDao searchKeyWordDao;

    @Override
    protected BaseDao<SearchKeyWord, Long> getEntityDao() {
        return searchKeyWordDao;
    }

}
