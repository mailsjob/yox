package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommodityDistributRegionDao;
import com.meiyuetao.myt.md.entity.CommodityDistributRegion;

@Service
@Transactional
public class CommodityDistributRegionService extends BaseService<CommodityDistributRegion, Long> {

    @Autowired
    private CommodityDistributRegionDao commodityDistributRegionDao;

    @Override
    protected BaseDao<CommodityDistributRegion, Long> getEntityDao() {
        return commodityDistributRegionDao;
    }
}
