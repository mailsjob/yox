package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommoditySaleDateCountDao;
import com.meiyuetao.myt.md.entity.CommoditySaleDateCount;

@Service
@Transactional
public class CommoditySaleDateCountService extends BaseService<CommoditySaleDateCount, Long> {

    @Autowired
    private CommoditySaleDateCountDao commoditySaleDateCountDao;

    @Override
    protected BaseDao<CommoditySaleDateCount, Long> getEntityDao() {
        return commoditySaleDateCountDao;
    }

}
