package com.meiyuetao.myt.md.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.NewsDao;
import com.meiyuetao.myt.md.entity.News;

@Service
@Transactional
public class NewsService extends BaseService<News, Long> {

    @Autowired
    private NewsDao newsDao;

    @Override
    protected BaseDao<News, Long> getEntityDao() {
        return newsDao;
    }

}
