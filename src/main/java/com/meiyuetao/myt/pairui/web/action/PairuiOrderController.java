package com.meiyuetao.myt.pairui.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.pairui.entity.PairuiOrder;
import com.meiyuetao.myt.pairui.service.PairuiOrderService;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrder.PairuiStatusEnum;
import com.meiyuetao.myt.sale.service.BoxOrderService;

@MetaData("提交到派瑞的订单管理")
public class PairuiOrderController extends MytBaseController<PairuiOrder, Long> {

    @Autowired
    private PairuiOrderService pairuiOrderService;

    @Autowired
    private BoxOrderService boxOrderService;

    @Override
    protected BaseService<PairuiOrder, Long> getEntityService() {
        return pairuiOrderService;
    }

    @Override
    protected void checkEntityAclPermission(PairuiOrder entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    /**
     * 推送订单到派瑞系统
     * 
     * @return
     */
    public HttpHeaders buildPairuiOrder() {
        Long[] ids = this.getSelectSids();
        List<BoxOrder> boxOrders = boxOrderService.findAll(ids);
        String msg = "";
        for (BoxOrder boxOrder : boxOrders) {
            if (PairuiStatusEnum.P_1.equals(boxOrder.getPairuiStatus())) {
                msg = "订单号" + boxOrder.getOrderSeq() + "已经提交至派瑞系统，不能再次推送！";
                continue;
            }
            msg += pairuiOrderService.postPoForOut(boxOrder);
        }
        if (msg.contains("成功")) {
            setModel(OperationResult.buildSuccessResult(msg));
        } else {
            setModel(OperationResult.buildFailureResult(msg));
        }

        return buildDefaultHttpHeaders();
    }

    /**
     * 订单状态及各节点查询
     * 
     * @return
     */
    public HttpHeaders billquery() {
        Long[] ids = this.getSelectSids();
        List<BoxOrder> boxOrders = boxOrderService.findAll(ids);
        String msg = "";
        for (BoxOrder boxOrder : boxOrders) {
            if (!PairuiStatusEnum.P_1.equals(boxOrder.getPairuiStatus())) {
                msg = "该订单尚未提交至派瑞系统，请先推送订单到派瑞！";
                continue;
            }
            msg += pairuiOrderService.billquery(boxOrder);
        }
        if (msg.contains("成功")) {
            setModel(OperationResult.buildSuccessResult(msg));
        } else {
            setModel(OperationResult.buildFailureResult(msg));
        }
        return buildDefaultHttpHeaders();
    }
}