package com.meiyuetao.myt.pairui.web.action;

import lab.s2jh.core.annotation.MetaData;
import com.meiyuetao.myt.pairui.entity.DeliverySendout;
import com.meiyuetao.myt.pairui.service.DeliverySendoutService;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

@MetaData("派瑞快递更新信息管理")
public class DeliverySendoutController extends BaseController<DeliverySendout,Long> {

    @Autowired
    private DeliverySendoutService deliverySendoutService;

    @Override
    protected BaseService<DeliverySendout, Long> getEntityService() {
        return deliverySendoutService;
    }
    
    @Override
    protected void checkEntityAclPermission(DeliverySendout entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        //TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }
    
    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }
    
    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}