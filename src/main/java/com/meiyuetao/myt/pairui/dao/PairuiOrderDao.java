package com.meiyuetao.myt.pairui.dao;

import com.meiyuetao.myt.pairui.entity.PairuiOrder;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface PairuiOrderDao extends BaseDao<PairuiOrder, Long> {

}