package com.meiyuetao.myt.pairui.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.pairui.entity.DeliverySendout;

@Repository
public interface DeliverySendoutDao extends BaseDao<DeliverySendout, Long> {

    @Query("from DeliverySendout where billno=:orderSeq")
    DeliverySendout findByBillno(String orderSeq);

}