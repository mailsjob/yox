package com.meiyuetao.myt.pairui.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.pairui.dao.DeliverySendoutDao;
import com.meiyuetao.myt.pairui.entity.DeliverySendout;

@Service
@Transactional
public class DeliverySendoutService extends BaseService<DeliverySendout, Long> {

    @Autowired
    private DeliverySendoutDao deliverySendoutDao;

    @Override
    protected BaseDao<DeliverySendout, Long> getEntityDao() {
        return deliverySendoutDao;
    }

    public DeliverySendout findByBillno(String orderSeq) {
        DeliverySendout deliverySendout = deliverySendoutDao.findByBillno(orderSeq);
        if (deliverySendout == null) {
            deliverySendout = new DeliverySendout();
        }
        return deliverySendout;
    }
}
