package com.meiyuetao.myt.pairui.entity;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

import lab.s2jh.core.annotation.MetaData;

@XmlType(propOrder = { "ficno", "funit", "fqty", "fprice", "famount", "fwareno", "fprodate", "foutdate", "fblockno", "fremarkdtail" })
public class PairuiDetailOrderXml {
    @MetaData("帐套号.商品编码")
    private String ficno = "";
    @MetaData("帐套号.单位")
    private String funit = "";
    @MetaData("数量")
    private Integer fqty;
    @MetaData("单价")
    private BigDecimal fprice;
    @MetaData("金额")
    private BigDecimal famount;
    @MetaData("指定库位")
    private String fwareno = "";
    @MetaData("指定生产日期")
    private String fprodate = "";
    @MetaData("指定到期日")
    private String foutdate = "";
    @MetaData("指定批号")
    private String fblockno = "";
    @MetaData("明细备注")
    private String fremarkdtail = "";

    public String getFicno() {
        return ficno;
    }

    public void setFicno(String ficno) {
        this.ficno = ficno;
    }

    public String getFunit() {
        return funit;
    }

    public void setFunit(String funit) {
        this.funit = funit;
    }

    public Integer getFqty() {
        return fqty;
    }

    public void setFqty(Integer fqty) {
        this.fqty = fqty;
    }

    public BigDecimal getFprice() {
        return fprice;
    }

    public void setFprice(BigDecimal fprice) {
        this.fprice = fprice;
    }

    public BigDecimal getFamount() {
        return famount;
    }

    public void setFamount(BigDecimal famount) {
        this.famount = famount;
    }

    public String getFwareno() {
        return fwareno;
    }

    public void setFwareno(String fwareno) {
        this.fwareno = fwareno;
    }

    public String getFprodate() {
        return fprodate;
    }

    public void setFprodate(String fprodate) {
        this.fprodate = fprodate;
    }

    public String getFoutdate() {
        return foutdate;
    }

    public void setFoutdate(String foutdate) {
        this.foutdate = foutdate;
    }

    public String getFblockno() {
        return fblockno;
    }

    public void setFblockno(String fblockno) {
        this.fblockno = fblockno;
    }

    public String getFremarkdtail() {
        return fremarkdtail;
    }

    public void setFremarkdtail(String fremarkdtail) {
        this.fremarkdtail = fremarkdtail;
    }

}
