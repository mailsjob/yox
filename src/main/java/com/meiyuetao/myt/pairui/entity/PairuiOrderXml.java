package com.meiyuetao.myt.pairui.entity;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lab.s2jh.core.annotation.MetaData;

@XmlRootElement(name = "head")
@XmlType(name = "head", propOrder = { "fbilltype", "faccountno", "ferpfromoth", "ferporderbillno", "ferpbillno", "fcustno", "fstockinno", "fstockoutno", "fbillerno", "fcheckerno",
        "fbilldate", "fremark", "fdischargeplace", "fdeliverplace", "fconsigneekey", "fcProvince", "fcProvincename", "fcCity", "fcCityname", "fcArea", "fcAreaname", "fnotes",
        "fcZip", "fcContact1", "fcPhone", "fhgbarcode", "ftype", "fmawb", "fhwab", "fbjflag", "fbjamt" })
public class PairuiOrderXml {
    @MetaData("单据类型")
    private String fbilltype = "2";
    @MetaData("帐套号")
    private String faccountno = "001";
    @MetaData("")
    private String ferpfromoth = "";
    @MetaData("原始订单单号")
    private String ferporderbillno = "";
    @MetaData("帐套号.订单单号")
    private String ferpbillno = "";
    @MetaData("客户号")
    private String fcustno = "";
    @MetaData("erp仓库号")
    private String fstockinno = "01";
    @MetaData("erp仓库号")
    private String fstockoutno = "";
    @MetaData("制单人")
    private String fbillerno = "";
    @MetaData("审核人")
    private String fcheckerno = "";
    @MetaData("日期")
    private String fbilldate = "";
    @MetaData("备注(最多50个字)")
    private String fremark = "";
    @MetaData("虚拟快递单号")
    private String fdischargeplace = "";
    @MetaData("真实快递单号")
    private String fdeliverplace = "";
    @MetaData("收货人")
    private String fconsigneekey = "";
    @MetaData("收货人省编码")
    private String fcProvince = "";
    @MetaData("收货人省名称")
    private String fcProvincename = "";
    @MetaData("收货人城市编码")
    private String fcCity = "";
    @MetaData("收货人城市名称")
    private String fcCityname = "";
    @MetaData("收货人区/县编码")
    private String fcArea = "";
    @MetaData("收货人区县名称")
    private String fcAreaname = "";
    @MetaData("收货人地址 ")
    private String fnotes = "";
    @MetaData("邮编")
    private String fcZip = "";
    @MetaData("联系人")
    private String fcContact1 = "";
    @MetaData("联系电话")
    private String fcPhone = "";
    @MetaData("海关通关条码")
    private String fhgbarcode = "";
    @MetaData("是否急单")
    private YnEnum ftype = YnEnum.N;
    @MetaData("订单总金额")
    private BigDecimal fmawb;
    @MetaData("订单总税款金额")
    private BigDecimal fhwab;
    @MetaData("是否保价")
    private YnEnum fbjflag = YnEnum.N;
    @MetaData("保价金额")
    private BigDecimal fbjamt;

    public enum YnEnum {
        @MetaData("是")
        Y,

        @MetaData("否")
        N;
    }

    public String getFbilltype() {
        return fbilltype;
    }

    public void setFbilltype(String fbilltype) {
        this.fbilltype = fbilltype;
    }

    public String getFaccountno() {
        return faccountno;
    }

    public void setFaccountno(String faccountno) {
        this.faccountno = faccountno;
    }

    public String getFerpfromoth() {
        return ferpfromoth;
    }

    public void setFerpfromoth(String ferpfromoth) {
        this.ferpfromoth = ferpfromoth;
    }

    public String getFerporderbillno() {
        return ferporderbillno;
    }

    public void setFerporderbillno(String ferporderbillno) {
        this.ferporderbillno = ferporderbillno;
    }

    public String getFerpbillno() {
        return ferpbillno;
    }

    public void setFerpbillno(String ferpbillno) {
        this.ferpbillno = ferpbillno;
    }

    public String getFcustno() {
        return fcustno;
    }

    public void setFcustno(String fcustno) {
        this.fcustno = fcustno;
    }

    public String getFstockinno() {
        return fstockinno;
    }

    public void setFstockinno(String fstockinno) {
        this.fstockinno = fstockinno;
    }

    public String getFstockoutno() {
        return fstockoutno;
    }

    public void setFstockoutno(String fstockoutno) {
        this.fstockoutno = fstockoutno;
    }

    public String getFbillerno() {
        return fbillerno;
    }

    public void setFbillerno(String fbillerno) {
        this.fbillerno = fbillerno;
    }

    public String getFcheckerno() {
        return fcheckerno;
    }

    public void setFcheckerno(String fcheckerno) {
        this.fcheckerno = fcheckerno;
    }

    public String getFbilldate() {
        return fbilldate;
    }

    public void setFbilldate(String fbilldate) {
        this.fbilldate = fbilldate;
    }

    public String getFremark() {
        return fremark;
    }

    public void setFremark(String fremark) {
        this.fremark = fremark;
    }

    public String getFdischargeplace() {
        return fdischargeplace;
    }

    public void setFdischargeplace(String fdischargeplace) {
        this.fdischargeplace = fdischargeplace;
    }

    public String getFdeliverplace() {
        return fdeliverplace;
    }

    public void setFdeliverplace(String fdeliverplace) {
        this.fdeliverplace = fdeliverplace;
    }

    public String getFconsigneekey() {
        return fconsigneekey;
    }

    public void setFconsigneekey(String fconsigneekey) {
        this.fconsigneekey = fconsigneekey;
    }

    @XmlElement(name = "fc_province")
    public String getFcProvince() {
        return fcProvince;
    }

    public void setFcProvince(String fcProvince) {
        this.fcProvince = fcProvince;
    }

    @XmlElement(name = "fc_provincename")
    public String getFcProvincename() {
        return fcProvincename;
    }

    public void setFcProvincename(String fcProvincename) {
        this.fcProvincename = fcProvincename;
    }

    @XmlElement(name = "fc_city")
    public String getFcCity() {
        return fcCity;
    }

    public void setFcCity(String fcCity) {
        this.fcCity = fcCity;
    }

    @XmlElement(name = "fc_cityname")
    public String getFcCityname() {
        return fcCityname;
    }

    public void setFcCityname(String fcCityname) {
        this.fcCityname = fcCityname;
    }

    @XmlElement(name = "fc_area")
    public String getFcArea() {
        return fcArea;
    }

    public void setFcArea(String fcArea) {
        this.fcArea = fcArea;
    }

    @XmlElement(name = "fc_areaname")
    public String getFcAreaname() {
        return fcAreaname;
    }

    public void setFcAreaname(String fcAreaname) {
        this.fcAreaname = fcAreaname;
    }

    public String getFnotes() {
        return fnotes;
    }

    public void setFnotes(String fnotes) {
        this.fnotes = fnotes;
    }

    @XmlElement(name = "fc_zip")
    public String getFcZip() {
        return fcZip;
    }

    public void setFcZip(String fcZip) {
        this.fcZip = fcZip;
    }

    @XmlElement(name = "fc_contact1")
    public String getFcContact1() {
        return fcContact1;
    }

    public void setFcContact1(String fcContact1) {
        this.fcContact1 = fcContact1;
    }

    @XmlElement(name = "fc_phone")
    public String getFcPhone() {
        return fcPhone;
    }

    public void setFcPhone(String fcPhone) {
        this.fcPhone = fcPhone;
    }

    public String getFhgbarcode() {
        return fhgbarcode;
    }

    public void setFhgbarcode(String fhgbarcode) {
        this.fhgbarcode = fhgbarcode;
    }

    public YnEnum getFtype() {
        return ftype;
    }

    public void setFtype(YnEnum ftype) {
        this.ftype = ftype;
    }

    public BigDecimal getFmawb() {
        return fmawb;
    }

    public void setFmawb(BigDecimal fmawb) {
        this.fmawb = fmawb;
    }

    public BigDecimal getFhwab() {
        return fhwab;
    }

    public void setFhwab(BigDecimal fhwab) {
        this.fhwab = fhwab;
    }

    public YnEnum getFbjflag() {
        return fbjflag;
    }

    public void setFbjflag(YnEnum fbjflag) {
        this.fbjflag = fbjflag;
    }

    public BigDecimal getFbjamt() {
        return fbjamt;
    }

    public void setFbjamt(BigDecimal fbjamt) {
        this.fbjamt = fbjamt;
    }

}
