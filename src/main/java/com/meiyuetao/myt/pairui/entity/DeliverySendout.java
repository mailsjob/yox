package com.meiyuetao.myt.pairui.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("派瑞快递更新信息")
@Entity
@Table(name = "pairui_delivery_sendout")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class DeliverySendout extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("订单号")
    private String billno;
    @MetaData("快递单号")
    private String deliveryno;
    @MetaData("快递名称")
    private String express;
    @MetaData("订单状态")
    private String state;
    @MetaData("仓库接收时间")
    private Date receivetime;
    @MetaData("化零时间")
    private Date hltime;
    @MetaData("开始拣选时间")
    private Date pickingtime;
    @MetaData("等待过分拣线时间")
    private Date checkingtime;
    @MetaData("过完分拣线时间")
    private Date finishtime;

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getDeliveryno() {
        return deliveryno;
    }

    public void setDeliveryno(String deliveryno) {
        this.deliveryno = deliveryno;
    }

    public String getExpress() {
        return express;
    }

    public void setExpress(String express) {
        this.express = express;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getReceivetime() {
        return receivetime;
    }

    public void setReceivetime(Date receivetime) {
        this.receivetime = receivetime;
    }

    public Date getHltime() {
        return hltime;
    }

    public void setHltime(Date hltime) {
        this.hltime = hltime;
    }

    public Date getPickingtime() {
        return pickingtime;
    }

    public void setPickingtime(Date pickingtime) {
        this.pickingtime = pickingtime;
    }

    public Date getCheckingtime() {
        return checkingtime;
    }

    public void setCheckingtime(Date checkingtime) {
        this.checkingtime = checkingtime;
    }

    public Date getFinishtime() {
        return finishtime;
    }

    public void setFinishtime(Date finishtime) {
        this.finishtime = finishtime;
    }

}
