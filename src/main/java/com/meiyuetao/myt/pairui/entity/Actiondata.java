package com.meiyuetao.myt.pairui.entity;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lab.s2jh.core.annotation.MetaData;

@XmlRootElement(name = "actiondata")
@XmlType(propOrder = { "action", "data" })
public class Actiondata {
    @MetaData("Action")
    private String action;
    @MetaData("Data")
    private OrderData data;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public OrderData getData() {
        return data;
    }

    public void setData(OrderData data) {
        this.data = data;
    }

}
