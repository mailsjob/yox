package com.meiyuetao.myt.pairui.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提交到派瑞的订单")
@Entity
@Table(name = "pairui_order_info")
@Cache(usage = CacheConcurrencyStrategy.NONE)
@XmlRootElement(name = "head")
@XmlType(name = "head", propOrder = { "fbilltype", "faccountno", "ferpfromoth", "ferporderbillno", "ferpbillno", "fcustno", "fstockinno", "fstockoutno", "fbillerno", "fcheckerno",
        "fbilldate", "fremark", "fdischargeplace", "fdeliverplace", "fconsigneekey", "fcProvince", "fcProvincename", "fcCity", "fcCityname", "fcArea", "fcAreaname", "fnotes",
        "fcZip", "fcContact1", "fcPhone", "fhgbarcode", "ftype", "fmawb", "fhwab", "fbjflag", "fbjamt", "pairuiDetailOrder" })
public class PairuiOrder extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("单据类型")
    private String fbilltype = "2";
    @MetaData("帐套号")
    private String faccountno = "001";
    @MetaData("")
    private String ferpfromoth = "";
    @MetaData("原始订单单号")
    private String ferporderbillno = "";
    @MetaData("帐套号.订单单号")
    private String ferpbillno = "";
    @MetaData("客户号")
    private String fcustno = "";
    @MetaData("erp仓库号")
    private String fstockinno = "";
    @MetaData("erp仓库号")
    private String fstockoutno = "";
    @MetaData("制单人")
    private String fbillerno = "";
    @MetaData("审核人")
    private String fcheckerno = "";
    @MetaData("日期")
    private String fbilldate = "";
    @MetaData("备注(最多50个字)")
    private String fremark = "";
    @MetaData("虚拟快递单号")
    private String fdischargeplace = "";
    @MetaData("真实快递单号")
    private String fdeliverplace = "";
    @MetaData("收货人")
    private String fconsigneekey = "";
    @MetaData("收货人省编码")
    private String fcProvince = "";
    @MetaData("收货人省名称")
    private String fcProvincename = "";
    @MetaData("收货人城市编码")
    private String fcCity = "";
    @MetaData("收货人城市名称")
    private String fcCityname = "";
    @MetaData("收货人区/县编码")
    private String fcArea = "";
    @MetaData("收货人区县名称")
    private String fcAreaname = "";
    @MetaData("收货人地址 ")
    private String fnotes = "";
    @MetaData("邮编")
    private String fcZip = "";
    @MetaData("联系人")
    private String fcContact1 = "";
    @MetaData("联系电话")
    private String fcPhone = "";
    @MetaData("海关通关条码")
    private String fhgbarcode = "";
    @MetaData("是否急单")
    private YnEnum ftype;
    @MetaData("订单总金额")
    private BigDecimal fmawb;
    @MetaData("订单总税款金额")
    private BigDecimal fhwab;
    @MetaData("是否保价")
    private YnEnum fbjflag;
    @MetaData("保价金额")
    private BigDecimal fbjamt;
    @MetaData("提交到派瑞的订单明细")
    private List<PairuiDetailOrder> pairuiDetailOrder = new ArrayList<PairuiDetailOrder>();

    public enum YnEnum {
        @MetaData("是")
        Y,

        @MetaData("否")
        N;
    }

    @Column(name = "fbilltype", length = 2, nullable = false)
    public String getFbilltype() {
        return fbilltype;
    }

    public void setFbilltype(String fbilltype) {
        this.fbilltype = fbilltype;
    }

    @Column(name = "faccountno", length = 3, nullable = false)
    public String getFaccountno() {
        return faccountno;
    }

    public void setFaccountno(String faccountno) {
        this.faccountno = faccountno;
    }

    @Column(name = "ferpfromoth")
    public String getFerpfromoth() {
        return ferpfromoth;
    }

    public void setFerpfromoth(String ferpfromoth) {
        this.ferpfromoth = ferpfromoth;
    }

    @Column(name = "ferporderbillno", length = 50)
    public String getFerporderbillno() {
        return ferporderbillno;
    }

    public void setFerporderbillno(String ferporderbillno) {
        this.ferporderbillno = ferporderbillno;
    }

    @Column(name = "ferpbillno", length = 50, nullable = false)
    public String getFerpbillno() {
        return ferpbillno;
    }

    public void setFerpbillno(String ferpbillno) {
        this.ferpbillno = ferpbillno;
    }

    @Column(name = "fcustno", length = 10, nullable = false)
    public String getFcustno() {
        return fcustno;
    }

    public void setFcustno(String fcustno) {
        this.fcustno = fcustno;
    }

    @Column(name = "fstockinno", length = 3, nullable = false)
    public String getFstockinno() {
        return fstockinno;
    }

    public void setFstockinno(String fstockinno) {
        this.fstockinno = fstockinno;
    }

    @Column(name = "fstockoutno", length = 3)
    public String getFstockoutno() {
        return fstockoutno;
    }

    public void setFstockoutno(String fstockoutno) {
        this.fstockoutno = fstockoutno;
    }

    public String getFbillerno() {
        return fbillerno;
    }

    public void setFbillerno(String fbillerno) {
        this.fbillerno = fbillerno;
    }

    public String getFcheckerno() {
        return fcheckerno;
    }

    public void setFcheckerno(String fcheckerno) {
        this.fcheckerno = fcheckerno;
    }

    @Column(nullable = false)
    public String getFbilldate() {
        return fbilldate;
    }

    public void setFbilldate(String fbilldate) {
        this.fbilldate = fbilldate;
    }

    public String getFremark() {
        return fremark;
    }

    public void setFremark(String fremark) {
        this.fremark = fremark;
    }

    public String getFdischargeplace() {
        return fdischargeplace;
    }

    public void setFdischargeplace(String fdischargeplace) {
        this.fdischargeplace = fdischargeplace;
    }

    public String getFdeliverplace() {
        return fdeliverplace;
    }

    public void setFdeliverplace(String fdeliverplace) {
        this.fdeliverplace = fdeliverplace;
    }

    @Column(nullable = false)
    public String getFconsigneekey() {
        return fconsigneekey;
    }

    public void setFconsigneekey(String fconsigneekey) {
        this.fconsigneekey = fconsigneekey;
    }

    @Column(nullable = false)
    public String getFcProvince() {
        return fcProvince;
    }

    public void setFcProvince(String fcProvince) {
        this.fcProvince = fcProvince;
    }

    @Column(nullable = false)
    public String getFcProvincename() {
        return fcProvincename;
    }

    public void setFcProvincename(String fcProvincename) {
        this.fcProvincename = fcProvincename;
    }

    @Column(nullable = false)
    public String getFcCity() {
        return fcCity;
    }

    public void setFcCity(String fcCity) {
        this.fcCity = fcCity;
    }

    @Column(nullable = false)
    public String getFcCityname() {
        return fcCityname;
    }

    public void setFcCityname(String fcCityname) {
        this.fcCityname = fcCityname;
    }

    @Column(nullable = false)
    public String getFcArea() {
        return fcArea;
    }

    public void setFcArea(String fcArea) {
        this.fcArea = fcArea;
    }

    @Column(nullable = false)
    public String getFcAreaname() {
        return fcAreaname;
    }

    public void setFcAreaname(String fcAreaname) {
        this.fcAreaname = fcAreaname;
    }

    @Column(nullable = false)
    public String getFnotes() {
        return fnotes;
    }

    public void setFnotes(String fnotes) {
        this.fnotes = fnotes;
    }

    @Column(nullable = false)
    public String getFcZip() {
        return fcZip;
    }

    public void setFcZip(String fcZip) {
        this.fcZip = fcZip;
    }

    @Column(nullable = false)
    public String getFcContact1() {
        return fcContact1;
    }

    public void setFcContact1(String fcContact1) {
        this.fcContact1 = fcContact1;
    }

    @Column(nullable = false)
    public String getFcPhone() {
        return fcPhone;
    }

    public void setFcPhone(String fcPhone) {
        this.fcPhone = fcPhone;
    }

    @Column(nullable = false)
    public String getFhgbarcode() {
        return fhgbarcode;
    }

    public void setFhgbarcode(String fhgbarcode) {
        this.fhgbarcode = fhgbarcode;
    }

    public YnEnum getFtype() {
        return ftype;
    }

    public void setFtype(YnEnum ftype) {
        this.ftype = ftype;
    }

    public BigDecimal getFmawb() {
        return fmawb;
    }

    public void setFmawb(BigDecimal fmawb) {
        this.fmawb = fmawb;
    }

    public BigDecimal getFhwab() {
        return fhwab;
    }

    public void setFhwab(BigDecimal fhwab) {
        this.fhwab = fhwab;
    }

    public YnEnum getFbjflag() {
        return fbjflag;
    }

    public void setFbjflag(YnEnum fbjflag) {
        this.fbjflag = fbjflag;
    }

    public BigDecimal getFbjamt() {
        return fbjamt;
    }

    public void setFbjamt(BigDecimal fbjamt) {
        this.fbjamt = fbjamt;
    }

    @OneToMany
    @XmlElementWrapper(name = "detail")
    @XmlElement(name = "list")
    public List<PairuiDetailOrder> getPairuiDetailOrder() {
        return pairuiDetailOrder;
    }

    public void setPairuiDetailOrder(List<PairuiDetailOrder> pairuiDetailOrder) {
        this.pairuiDetailOrder = pairuiDetailOrder;
    }
}
