package com.meiyuetao.myt.pairui.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "data")
@XmlType(propOrder = { "head", "detail" })
public class OrderData {
    private PairuiOrderXml head;
    private List<PairuiDetailOrderXml> detail;

    public PairuiOrderXml getHead() {
        return head;
    }

    public void setHead(PairuiOrderXml head) {
        this.head = head;
    }

    @XmlElementWrapper(name = "detail")
    @XmlElement(name = "list")
    public List<PairuiDetailOrderXml> getDetail() {
        return detail;
    }

    public void setDetail(List<PairuiDetailOrderXml> detail) {
        this.detail = detail;
    }
}
