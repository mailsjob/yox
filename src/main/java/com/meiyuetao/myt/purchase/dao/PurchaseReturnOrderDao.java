package com.meiyuetao.myt.purchase.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.purchase.entity.PurchaseReturnOrder;

@Repository
public interface PurchaseReturnOrderDao extends BaseDao<PurchaseReturnOrder, Long> {

}