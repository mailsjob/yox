package com.meiyuetao.myt.purchase.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.purchase.entity.Supplier;

@Repository
public interface SupplierDao extends BaseDao<Supplier, Long> {

    @Query("from Supplier")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    List<Supplier> findAllCached();

}