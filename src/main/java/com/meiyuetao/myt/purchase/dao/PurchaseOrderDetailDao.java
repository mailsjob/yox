package com.meiyuetao.myt.purchase.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.purchase.entity.PurchaseOrderDetail;

@Repository
public interface PurchaseOrderDetailDao extends BaseDao<PurchaseOrderDetail, Long> {

}