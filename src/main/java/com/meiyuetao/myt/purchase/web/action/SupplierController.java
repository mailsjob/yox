package com.meiyuetao.myt.purchase.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.purchase.entity.Supplier;
import com.meiyuetao.myt.purchase.service.SupplierService;

@MetaData("供应商管理")
public class SupplierController extends MytBaseController<Supplier, Long> {

    @Autowired
    private SupplierService supplierService;

    @Override
    protected BaseService<Supplier, Long> getEntityService() {
        return supplierService;
    }

    @Override
    protected void checkEntityAclPermission(Supplier entity) {
        // Do nothing
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

}