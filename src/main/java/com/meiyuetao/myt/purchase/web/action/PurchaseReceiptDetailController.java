package com.meiyuetao.myt.purchase.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.service.VoucherNumGenerateService;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.purchase.entity.PurchaseReceiptDetail;
import com.meiyuetao.myt.purchase.service.PurchaseReceiptDetailService;
import com.meiyuetao.myt.stock.service.StorageLocationService;

@MetaData("采购单一收货记录")
public class PurchaseReceiptDetailController extends MytBaseController<PurchaseReceiptDetail, Long> {

    @Autowired
    private PurchaseReceiptDetailService purchaseReceiptDetailService;

    @Autowired
    private StorageLocationService storageLocationService;

    @Autowired
    private VoucherNumGenerateService voucherNumGenerateService;

    @Override
    protected BaseService<PurchaseReceiptDetail, Long> getEntityService() {
        return purchaseReceiptDetailService;
    }

    @Override
    protected void checkEntityAclPermission(PurchaseReceiptDetail entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}