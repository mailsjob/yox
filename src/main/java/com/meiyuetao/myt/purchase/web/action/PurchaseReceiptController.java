package com.meiyuetao.myt.purchase.web.action;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.service.Validation;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.constant.VoucherStateEnum;
import com.meiyuetao.myt.core.constant.VoucherTypeEnum;
import com.meiyuetao.myt.core.service.VoucherNumGenerateService;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.purchase.entity.PurchaseReceipt;
import com.meiyuetao.myt.purchase.entity.PurchaseReceiptDetail;
import com.meiyuetao.myt.purchase.service.PurchaseOrderService;
import com.meiyuetao.myt.purchase.service.PurchaseReceiptService;
import com.meiyuetao.myt.stock.service.StorageLocationService;

@MetaData("采购单")
public class PurchaseReceiptController extends MytBaseController<PurchaseReceipt, Long> {

    @Autowired
    private PurchaseReceiptService purchaseReceiptService;
    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @Autowired
    private StorageLocationService storageLocationService;

    @Autowired
    private VoucherNumGenerateService voucherNumGenerateService;

    @Override
    protected BaseService<PurchaseReceipt, Long> getEntityService() {
        return purchaseReceiptService;
    }

    @Override
    protected void checkEntityAclPermission(PurchaseReceipt entity) {
        // TODO Add acl check code logic
    }

    @Override
    protected void setupDetachedBindingEntity(Long id) {
        bindingEntity = getEntityService().findDetachedOne(id, "purchaseReceiptDetails");
    }

    @Override
    public void prepareEdit() {
        super.prepareEdit();
        if (bindingEntity.isNew()) {
            bindingEntity.setVoucher(voucherNumGenerateService.getVoucherNumByType(VoucherTypeEnum.JH));
            bindingEntity.setVoucherDate(new Date());
            bindingEntity.setVoucherUser(getLogonUser());
            bindingEntity.setVoucherDepartment(getLogonUser().getDepartment());
        }
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.isNew()) {
            List<PurchaseReceiptDetail> purchaseReceiptDetails = bindingEntity.getPurchaseReceiptDetails();
            for (PurchaseReceiptDetail purchaseReceiptDetail : purchaseReceiptDetails) {
                purchaseReceiptDetail.setPurchaseReceipt(bindingEntity);

            }
        }
        if (postNotConfirmedByUser()) {
            BigDecimal paymentRoadAmount = new BigDecimal(0);
            if (StringUtils.isNotBlank(getParameter("paymentRoadAmount"))) {
                paymentRoadAmount = new BigDecimal(getParameter("paymentRoadAmount"));
            }
            if (bindingEntity.getTotalRoadAmount().compareTo(paymentRoadAmount) != 0) {
                setModel(OperationResult.buildConfirmResult("收货单其他分摊费用和需要分摊的金额不一致", bindingEntity.getTotalRoadAmount() + "," + paymentRoadAmount));
                // 直接返回使用户进行Confirm确认
                return buildDefaultHttpHeaders();
            }
        }
        bindingEntity.setVoucherState(VoucherStateEnum.POST);
        return super.doSave();
    }

    @Override
    public String isDisallowUpdate() {
        if (VoucherStateEnum.REDW.equals(bindingEntity.getVoucherState())) {
            return "已红冲的不能修改";
        }
        if (VoucherStateEnum.POST.equals(bindingEntity.getVoucherState())) {
            return "已提交的不能修改";
        }
        return null;
    }

    public String isDisallowChargeAgainst() {
        if (VoucherStateEnum.REDW.equals(bindingEntity.getVoucherState())) {
            return "已红冲";
        }
        return null;
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("行项数据")
    public HttpHeaders purchaseReceiptDetails() {
        setModel(buildPageResultFromList(bindingEntity.getPurchaseReceiptDetails()));
        return buildDefaultHttpHeaders();
    }

    @MetaData("移动收货")
    public HttpHeaders scan() {
        return buildDefaultHttpHeaders("inputScan");
    }

    public HttpHeaders chargeAgainst() {
        if (!bindingEntity.isNew()) {
            Validation.isTrue(!VoucherStateEnum.REDW.equals(bindingEntity.getVoucherState()), "销售单已红冲");
            purchaseReceiptService.chargeAgainst(bindingEntity);
        }
        setModel(OperationResult.buildSuccessResult("红冲完成"));
        return buildDefaultHttpHeaders();
    }

}