package com.meiyuetao.myt.purchase.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.stock.entity.CommodityStockMoveDetail;
import com.meiyuetao.myt.stock.entity.StorageLocation;

@Entity
@MetaData("采购（收货）单行项")
@Table(name = "myt_purchase_receipt_detail")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PurchaseReceiptDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "子凭证号")
    private String subVoucher;
    @MetaData("关联采购（收货）单")
    private PurchaseReceipt purchaseReceipt;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("库存地")
    private StorageLocation storageLocation;
    @MetaData(value = "批次号", tooltips = "如果留空并且设置了过期日期，会自动以过期日期的年月YYYYMM格式作为批次号")
    private String batchNo;
    @MetaData(value = "过期时间")
    private Date expireDate;
    @MetaData("采购数量")
    private BigDecimal quantity;
    @MetaData("采购单价")
    private BigDecimal price;
    @MetaData("采购订单行项")
    private PurchaseOrderDetail purchaseOrderDetail;
    @MetaData("库存调拨单行项")
    private CommodityStockMoveDetail commodityStockMoveDetail;

    @MetaData("单位")
    private String measureUnit;

    @MetaData("原价金额")
    private BigDecimal originalAmount;

    @MetaData("折扣率(%)")
    private BigDecimal discountRate;

    @MetaData("折扣额")
    private BigDecimal discountAmount;

    @MetaData("折后金额 ")
    private BigDecimal amount;

    /*
     * @MetaData("税率(%)") private BigDecimal taxRate;
     * 
     * @MetaData("税额") private BigDecimal taxAmount;
     * 
     * @MetaData("含税总金额") private BigDecimal commodityAndTaxAmount;
     */
    @MetaData("商家运费")
    private BigDecimal deliveryAmount;
    @MetaData("第三方运费、通关费等")
    private BigDecimal roadAmount;

    @MetaData("入库成本单价")
    private BigDecimal costPrice;

    @MetaData("入库成本")
    private BigDecimal costAmount;

    @Column(length = 128, nullable = true)
    @JsonProperty
    public String getSubVoucher() {
        return subVoucher;
    }

    public void setSubVoucher(String subVoucher) {
        this.subVoucher = subVoucher;
    }

    @ManyToOne
    @JoinColumn(name = "purchase_receipt_sid", nullable = true)
    @JsonProperty
    public PurchaseReceipt getPurchaseReceipt() {
        return purchaseReceipt;
    }

    public void setPurchaseReceipt(PurchaseReceipt purchaseReceipt) {
        this.purchaseReceipt = purchaseReceipt;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "storage_location_sid", nullable = false)
    @JsonProperty
    public StorageLocation getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(StorageLocation storageLocation) {
        this.storageLocation = storageLocation;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Column(nullable = false)
    @JsonProperty
    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    @OneToOne
    @JoinColumn(name = "purchase_order_detail_sid")
    public PurchaseOrderDetail getPurchaseOrderDetail() {
        return purchaseOrderDetail;
    }

    public void setPurchaseOrderDetail(PurchaseOrderDetail purchaseOrderDetail) {
        this.purchaseOrderDetail = purchaseOrderDetail;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /*
     * @Column(precision = 18, scale = 2)
     * 
     * @JsonProperty public BigDecimal getTaxRate() { return taxRate; }
     * 
     * public void setTaxRate(BigDecimal taxRate) { this.taxRate = taxRate; }
     * 
     * @Column(precision = 18, scale = 2)
     * 
     * @JsonProperty public BigDecimal getTaxAmount() { return taxAmount; }
     * 
     * public void setTaxAmount(BigDecimal taxAmount) { this.taxAmount =
     * taxAmount; }
     * 
     * @Column(precision = 18, scale = 2)
     * 
     * @JsonProperty public BigDecimal getCommodityAndTaxAmount() { return
     * commodityAndTaxAmount; }
     * 
     * public void setCommodityAndTaxAmount(BigDecimal commodityAndTaxAmount) {
     * this.commodityAndTaxAmount = commodityAndTaxAmount; }
     */

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    @JsonProperty
    @Column(precision = 18, scale = 2)
    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
    }

    @JsonProperty
    @Column(precision = 18, scale = 2)
    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    @JsonProperty
    @Column(precision = 18, scale = 2)
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    @JsonProperty
    @Column(precision = 18, scale = 2)
    public BigDecimal getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(BigDecimal deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    @JsonProperty
    @Column(precision = 18, scale = 2)
    public BigDecimal getCostAmount() {
        return costAmount;
    }

    public void setCostAmount(BigDecimal costAmount) {
        this.costAmount = costAmount;
    }

    @OneToOne
    @JoinColumn(name = "commodity_stock_move_detail_sid")
    public CommodityStockMoveDetail getCommodityStockMoveDetail() {
        return commodityStockMoveDetail;
    }

    public void setCommodityStockMoveDetail(CommodityStockMoveDetail commodityStockMoveDetail) {
        this.commodityStockMoveDetail = commodityStockMoveDetail;
    }

    @JsonProperty
    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    @JsonSerialize(using = DateJsonSerializer.class)
    @JsonProperty
    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    @JsonProperty
    public BigDecimal getRoadAmount() {
        return roadAmount;
    }

    public void setRoadAmount(BigDecimal roadAmount) {
        this.roadAmount = roadAmount;
    }

}
