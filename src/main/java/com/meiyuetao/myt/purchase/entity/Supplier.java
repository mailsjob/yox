package com.meiyuetao.myt.purchase.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.constant.GenericSexEnum;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

/**
 * 采购供货商
 */
@Entity
@Table(name = "iyb_purchase_supplier")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Supplier extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("代码")
    private String code;
    @MetaData("名称")
    private String abbr;
    @MetaData("别名")
    private String alias;
    @MetaData("地址")
    private String address;
    @MetaData("网址")
    private String website;
    @MetaData("公司名")
    private String companyName;
    @MetaData("联系人")
    private String contactPerson;
    @MetaData("联系电话")
    private String contactPhone;
    @MetaData("联系邮箱")
    private String contactEmail;
    @MetaData("联系人性别")
    private GenericSexEnum contactSex = GenericSexEnum.U;
    @MetaData("即时通讯账号")
    private String contactIm;
    @MetaData("即时通讯账号类型")
    private String contactImType;
    @MetaData("备注")
    private String adminMemo;
    @MetaData("系统备注")
    private String sysMemo;
    private SupplierTypeEnum supplierType = SupplierTypeEnum.CG;

    public enum SupplierTypeEnum {

        @MetaData("快递公司")
        KD,

        @MetaData("采购供货商")
        CG,

        @MetaData("调拨库存")
        DB;

    }

    @Column(length = 32, unique = true, nullable = false)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(length = 32)
    @JsonProperty
    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    @Column(length = 128)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(length = 128)
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Column(length = 128)
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Column(length = 32)
    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    @Column(length = 32)
    @JsonProperty
    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    @Column(length = 32)
    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 1)
    public GenericSexEnum getContactSex() {
        return contactSex;
    }

    public void setContactSex(GenericSexEnum contactSex) {
        this.contactSex = contactSex;
    }

    @Column(length = 32)
    public String getContactIm() {
        return contactIm;
    }

    public void setContactIm(String contactIm) {
        this.contactIm = contactIm;
    }

    @Column(length = 512)
    public String getAdminMemo() {
        return adminMemo;
    }

    public void setAdminMemo(String adminMemo) {
        this.adminMemo = adminMemo;
    }

    @Column(length = 512)
    public String getSysMemo() {
        return sysMemo;
    }

    public void setSysMemo(String sysMemo) {
        this.sysMemo = sysMemo;
    }

    public String getContactImType() {
        return contactImType;
    }

    public void setContactImType(String contactImType) {
        this.contactImType = contactImType;
    }

    @Transient
    public String getDisplay() {
        return this.abbr;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public SupplierTypeEnum getSupplierType() {
        return supplierType;
    }

    public void setSupplierType(SupplierTypeEnum supplierType) {
        this.supplierType = supplierType;
    }

    @Transient
    public String getBizTradeUnitId() {
        return "SUPPLIER_" + id;
    }

    @JsonProperty
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

}