package com.meiyuetao.myt.purchase.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.auth.entity.Department;
import lab.s2jh.auth.entity.User;
import lab.s2jh.bpm.BpmTrackable;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.SkipParamBind;
import lab.s2jh.core.web.json.DateJsonSerializer;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.finance.entity.AccountSubject;

@MetaData("采购订单")
@Entity
@Table(name = "myt_purchase_return_order")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PurchaseReturnOrder extends MytBaseEntity implements BpmTrackable {
    private static final long serialVersionUID = 1L;
    @MetaData("唯一凭证号")
    private String voucher;
    @MetaData("凭证日期")
    private Date voucherDate;
    @MetaData("经办人")
    private User voucherUser;
    @MetaData("经办部门")
    private Department voucherDepartment;
    @MetaData("供货商")
    private Supplier supplier;

    @MetaData("关联采购（收货）单")
    private PurchaseReceipt purchaseReceipt;

    private List<PurchaseReturnOrderDetail> purchaseReturnOrderDetails = new ArrayList<PurchaseReturnOrderDetail>();
    @MetaData(value = "标题摘要")
    private String title;
    private PurchaseReturnOrderStatusEnum returnOrderStatus = PurchaseReturnOrderStatusEnum.S10N;

    @MetaData("订单总金额")
    private BigDecimal totalAmount;

    @MetaData("已退总金额")
    private BigDecimal actualRefundAmount;

    @MetaData(value = "预收款会计科目")
    private AccountSubject accountSubject;
    @MetaData(value = "记账摘要")
    private String accountSummary;
    @MetaData("活动节点名称")
    private String activeTaskName;
    /*
     * @MetaData("收款类型") private PurchaseOrderPayModeEnum payMode =
     * PurchaseOrderPayModeEnum.PREV;
     */
    /*
     * @MetaData(value = "关联收款凭证号列表", comments = "逗号分隔，一般用于记录其他三方系统付款的凭证信息")
     * private String paymentVouchers;
     */
    /*
     * @MetaData(value = "付款参考信息", comments = "一般用于记录其他三方系统付款的凭证信息") private
     * String paymentReference;
     */

    @MetaData("折后应收总金额")
    private BigDecimal amount;

    @MetaData("交税总金额")
    private BigDecimal totalTaxAmount;

    /*
     * @MetaData("整单优惠金额") private BigDecimal totalDiscountAmount;
     */

    @MetaData("整单运费")
    private BigDecimal totalDeliveryAmount;
    @MetaData("发货时间")
    private Date deliveryTime;

    @MetaData("快递公司")
    private Supplier logistics;

    @MetaData("快递单号")
    private String logisticsNo;

    @MetaData("提交时间")
    private Date submitDate;
    @MetaData("提交审核时间")
    private Date auditDate;
    @MetaData("红冲时间")
    private Date redwordDate;
    @MetaData(value = "最近操作摘要")
    private String lastOperationSummary;
    @MetaData("采购退货备注")
    private String purchaseReturnMemo;

    public enum PurchaseReturnOrderStatusEnum {

        @MetaData("待提交")
        S10N,

        @MetaData("提交待审")
        S20S,

        @MetaData("审核通过")
        S30AP,

        @MetaData("审核未过")
        S40ANP,

        @MetaData("已寄出")
        S50EX,

        @MetaData("确认退货成功")
        S60RECVS,

        @MetaData("已收款")
        S65RECVE,

        @MetaData("已完结")
        S75FIE,

        @MetaData("已关闭")
        S80C,

        @MetaData("已取消")
        S90CNC;

    }

    @Column(length = 128, nullable = false, unique = true, updatable = false)
    @JsonProperty
    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    @Column(nullable = false)
    @JsonProperty
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date getVoucherDate() {
        return voucherDate;
    }

    public void setVoucherDate(Date voucherDate) {
        this.voucherDate = voucherDate;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "voucher_user_id", nullable = false)
    @JsonProperty
    public User getVoucherUser() {
        return voucherUser;
    }

    public void setVoucherUser(User voucherUser) {
        this.voucherUser = voucherUser;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "voucher_department_id", nullable = false)
    @JsonProperty
    public Department getVoucherDepartment() {
        return voucherDepartment;
    }

    public void setVoucherDepartment(Department voucherDepartment) {
        this.voucherDepartment = voucherDepartment;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    @JsonProperty
    public PurchaseReturnOrderStatusEnum getReturnOrderStatus() {
        return returnOrderStatus;
    }

    public void setReturnOrderStatus(PurchaseReturnOrderStatusEnum returnOrderStatus) {
        this.returnOrderStatus = returnOrderStatus;
    }

    @OneToMany(mappedBy = "purchaseReturnOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<PurchaseReturnOrderDetail> getPurchaseReturnOrderDetails() {
        return purchaseReturnOrderDetails;
    }

    public void setPurchaseReturnOrderDetails(List<PurchaseReturnOrderDetail> purchaseReturnOrderDetails) {
        this.purchaseReturnOrderDetails = purchaseReturnOrderDetails;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "Supplier_Sid", nullable = false)
    @JsonProperty
    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    @Override
    public String toString() {
        return "PurchaseOrder:  " + voucher;
    }

    @JsonProperty
    public String getActiveTaskName() {
        return activeTaskName;
    }

    public void setActiveTaskName(String activeTaskName) {
        this.activeTaskName = activeTaskName;
    }

    @JsonProperty
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(BigDecimal totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    @JsonProperty
    public BigDecimal getTotalDeliveryAmount() {
        return totalDeliveryAmount;
    }

    public void setTotalDeliveryAmount(BigDecimal totalDeliveryAmount) {
        this.totalDeliveryAmount = totalDeliveryAmount;
    }

    @ManyToOne
    @JoinColumn(name = "account_subject_id")
    public AccountSubject getAccountSubject() {
        return accountSubject;
    }

    public void setAccountSubject(AccountSubject accountSubject) {
        this.accountSubject = accountSubject;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "logistics_sid", nullable = true)
    @JsonProperty
    public Supplier getLogistics() {
        return logistics;
    }

    public void setLogistics(Supplier logistics) {
        this.logistics = logistics;
    }

    @Column(length = 32)
    @JsonProperty
    public String getLogisticsNo() {
        return logisticsNo;
    }

    public void setLogisticsNo(String logisticsNo) {
        this.logisticsNo = logisticsNo;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    @Override
    @Transient
    @JsonProperty
    public String getDisplay() {
        return voucher;
    }

    @Override
    @Transient
    @JsonIgnore
    public String getBpmBusinessKey() {
        return voucher;
    }

    public String getAccountSummary() {
        return accountSummary;
    }

    public void setAccountSummary(String accountSummary) {
        this.accountSummary = accountSummary;
    }

    public Date getRedwordDate() {
        return redwordDate;
    }

    @SkipParamBind
    public void setRedwordDate(Date redwordDate) {
        this.redwordDate = redwordDate;
    }

    @JsonProperty
    public String getLastOperationSummary() {
        return lastOperationSummary;
    }

    public void setLastOperationSummary(String lastOperationSummary) {
        this.lastOperationSummary = lastOperationSummary;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    @SkipParamBind
    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    @JsonProperty
    public Date getSubmitDate() {
        return submitDate;
    }

    @SkipParamBind
    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    @Column(length = 2000)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 1024)
    @JsonProperty
    public String getPurchaseReturnMemo() {
        return purchaseReturnMemo;
    }

    public void setPurchaseReturnMemo(String purchaseReturnMemo) {
        this.purchaseReturnMemo = purchaseReturnMemo;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getActualRefundAmount() {
        return actualRefundAmount;
    }

    public void setActualRefundAmount(BigDecimal actualRefundAmount) {
        this.actualRefundAmount = actualRefundAmount;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "purchase_receipt_sid", nullable = true)
    @JsonIgnore
    public PurchaseReceipt getPurchaseReceipt() {
        return purchaseReceipt;
    }

    public void setPurchaseReceipt(PurchaseReceipt purchaseReceipt) {
        this.purchaseReceipt = purchaseReceipt;
    }

    @Override
    @Transient
    public String getExtraInfo() {
        return this.logisticsNo;
    }

}