package com.meiyuetao.myt.purchase.service;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.purchase.dao.SupplierDao;
import com.meiyuetao.myt.purchase.entity.Supplier;

@Service
@Transactional
public class SupplierService extends BaseService<Supplier, Long> {

    @Autowired
    private SupplierDao supplierDao;

    @Override
    protected BaseDao<Supplier, Long> getEntityDao() {
        return supplierDao;
    }

    @Transactional(readOnly = true)
    public List<Supplier> findAllCached() {
        return supplierDao.findAllCached();
    }

}
