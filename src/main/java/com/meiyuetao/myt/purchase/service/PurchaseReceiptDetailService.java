package com.meiyuetao.myt.purchase.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.purchase.dao.PurchaseReceiptDetailDao;
import com.meiyuetao.myt.purchase.entity.PurchaseReceiptDetail;

@Service
@Transactional
public class PurchaseReceiptDetailService extends BaseService<PurchaseReceiptDetail, Long> {

    @Autowired
    private PurchaseReceiptDetailDao purchaseReceiptDetailDao;

    @Override
    protected BaseDao<PurchaseReceiptDetail, Long> getEntityDao() {
        return purchaseReceiptDetailDao;
    }
}
