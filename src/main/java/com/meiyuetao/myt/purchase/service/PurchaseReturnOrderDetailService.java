package com.meiyuetao.myt.purchase.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.purchase.dao.PurchaseReturnOrderDetailDao;
import com.meiyuetao.myt.purchase.entity.PurchaseReturnOrderDetail;

@Service
@Transactional
public class PurchaseReturnOrderDetailService extends BaseService<PurchaseReturnOrderDetail, Long> {

    @Autowired
    private PurchaseReturnOrderDetailDao purchaseReturnOrderDetailDao;

    @Override
    protected BaseDao<PurchaseReturnOrderDetail, Long> getEntityDao() {
        return purchaseReturnOrderDetailDao;
    }
}
