package com.meiyuetao.myt.kuajing.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.sale.entity.BoxOrder;

@MetaData("提交到怡亚通的订单")
@Entity
@Table(name = "yyt_order_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YytOrderInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("交易码*")
    private String fbType;
    @MetaData("客户编号*")
    private String shipToCode;
    @MetaData("订单号")
    private String pocode;
    @MetaData("订单日期")
    private String podate;
    @MetaData("收货人名")
    private String shiptoname;
    @MetaData("手机")
    private String mobile;
    @MetaData("电话")
    private String tel;
    @MetaData("城市")
    private String city;
    @MetaData("地址")
    private String address;
    @MetaData("备注1")
    private String remark1;
    @MetaData("备注2")
    private String remark2;
    @MetaData("快递公司")
    private String expressCompany;
    @MetaData("快递单号")
    private String expressCode;
    @MetaData("买方ID")
    private String buyerid;
    @MetaData("海关通关号")
    private String customcode;
    private List<YytOrderDetailInfo> orderDetailInfos = new ArrayList<YytOrderDetailInfo>();
    private String xmlStr;
    private BoxOrder boxOrder;

    @JsonProperty
    public String getFbType() {
        return fbType;
    }

    public void setFbType(String fbType) {
        this.fbType = fbType;
    }

    @JsonProperty
    public String getShipToCode() {
        return shipToCode;
    }

    public void setShipToCode(String shipToCode) {
        this.shipToCode = shipToCode;
    }

    @JsonProperty
    public String getPocode() {
        return pocode;
    }

    public void setPocode(String pocode) {
        this.pocode = pocode;
    }

    @JsonProperty
    public String getPodate() {
        return podate;
    }

    public void setPodate(String podate) {
        this.podate = podate;
    }

    @JsonProperty
    public String getShiptoname() {
        return shiptoname;
    }

    public void setShiptoname(String shiptoname) {
        this.shiptoname = shiptoname;
    }

    @JsonProperty
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonProperty
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @JsonProperty
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty
    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    @JsonProperty
    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    @JsonProperty
    public String getExpressCompany() {
        return expressCompany;
    }

    public void setExpressCompany(String expressCompany) {
        this.expressCompany = expressCompany;
    }

    @JsonProperty
    public String getExpressCode() {
        return expressCode;
    }

    public void setExpressCode(String expressCode) {
        this.expressCode = expressCode;
    }

    @JsonProperty
    public String getBuyerid() {
        return buyerid;
    }

    public void setBuyerid(String buyerid) {
        this.buyerid = buyerid;
    }

    @JsonProperty
    public String getCustomcode() {
        return customcode;
    }

    public void setCustomcode(String customcode) {
        this.customcode = customcode;
    }

    @Lob
    public String getXmlStr() {
        return xmlStr;
    }

    public void setXmlStr(String xmlStr) {
        this.xmlStr = xmlStr;
    }

    @OneToMany(mappedBy = "orderInfo", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<YytOrderDetailInfo> getOrderDetailInfos() {
        return orderDetailInfos;
    }

    public void setOrderDetailInfos(List<YytOrderDetailInfo> orderDetailInfos) {
        this.orderDetailInfos = orderDetailInfos;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "box_order_sid")
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

}
