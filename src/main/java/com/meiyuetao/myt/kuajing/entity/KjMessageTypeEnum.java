package com.meiyuetao.myt.kuajing.entity;

import lab.s2jh.core.annotation.MetaData;

@MetaData("跨境报文类型")
public enum KjMessageTypeEnum {

    @MetaData(value = "商品对应关系备案")
    SKU_INFO, @MetaData(value = "商品对应关系备案异常")
    SKU_ERROR, @MetaData(value = "订单")
    ORDER_INFO, @MetaData(value = "支付单")
    PAYMENT_INFO, @MetaData(value = "订单对应分运单号")
    ORDER_SET_TRANSPORT_NO, @MetaData(value = "退还税金指令")
    TAX_RETURN_COMMAND, @MetaData(value = "税金预扣指令")
    TAX_PREPAY_COMMAND, @MetaData(value = "订单退货")
    ORDER_RETURN_INFO, @MetaData(value = "入库回执")
    MES_ASK_INFO, @MetaData(value = "商品备案回执")
    SKU_INFO_FB, @MetaData(value = "订单状态回执")
    ORDER_INFO_FB, @MetaData(value = "支付单状态回执")
    PAYMENT_INFO_FB, @MetaData(value = "订单条形码反馈")
    ORDER_BAR_CODE_FB, @MetaData(value = "运单状态反馈")
    BILL_INFO_FB;

}
