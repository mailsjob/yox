package com.meiyuetao.myt.kuajing.entity;

import lab.s2jh.core.annotation.MetaData;

@MetaData("跨境支付单状态码类型")
public enum KjPaymentStatusCodeEnum {

    @MetaData(value = "待审批")
    KJP_20, @MetaData(value = "审核通过 ")
    KJP_30, @MetaData(value = "审核退单")
    KJP_40, @MetaData(value = "退货")
    KJP_50;

}
