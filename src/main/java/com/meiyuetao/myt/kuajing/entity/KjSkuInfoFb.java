package com.meiyuetao.myt.kuajing.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@MetaData("跨境回执信息")
@Entity
@Table(name = "kj_sku_info_fb")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjSkuInfoFb extends KjHeaderInfoFb {
    private static final long serialVersionUID = 1L;
    private String sku;
    private KjSkuStatusCodeEnum skuStatusCode;
    private Date opDate;
    private String memo;
    private String adminMemo;

    @JsonProperty
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public KjSkuStatusCodeEnum getSkuStatusCode() {
        return skuStatusCode;
    }

    public void setSkuStatusCode(KjSkuStatusCodeEnum skuStatusCode) {
        this.skuStatusCode = skuStatusCode;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getOpDate() {
        return opDate;
    }

    public void setOpDate(Date opDate) {
        this.opDate = opDate;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getAdminMemo() {
        return adminMemo;
    }

    public void setAdminMemo(String adminMemo) {
        this.adminMemo = adminMemo;
    }

}
