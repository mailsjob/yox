package com.meiyuetao.myt.kuajing.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提交到怡亚通的订单")
@Entity
@Table(name = "yyt_order_detail_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YytOrderDetailInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("门店编号*")
    private String shopId;
    @MetaData("门店名称")
    private String shopName;
    @MetaData("客户商品编号")
    private String goodsId;
    @MetaData("客户商品国际码")
    private String barcode;
    @MetaData("客户商品名称")
    private String goodsName;
    @MetaData("数量")
    private BigDecimal qty;
    @MetaData("单价")
    private BigDecimal price;
    @MetaData("单位")
    private String unitName;
    private YytOrderInfo orderInfo;

    @JsonProperty
    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    @JsonProperty
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @JsonProperty
    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    @JsonProperty
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @JsonProperty
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @JsonProperty
    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    @JsonProperty
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @JsonProperty
    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @ManyToOne
    @JoinColumn(name = "order_sid")
    public YytOrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(YytOrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

}
