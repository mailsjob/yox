package com.meiyuetao.myt.kuajing.entity;

import lab.s2jh.core.annotation.MetaData;

@MetaData("跨境商品备案状态码类型")
public enum KjSkuStatusCodeEnum {

    @MetaData(value = "暂存")
    KJS_10, @MetaData(value = "待审批")
    KJS_20, @MetaData(value = "自动审批通过")
    KJS_30, @MetaData(value = "人工待审批")
    KJS_40, @MetaData(value = "人工审批通过")
    KJS_50, @MetaData(value = "人工退单")
    KJS_60, @MetaData(value = "人工挂起")
    KJS_70, @MetaData(value = "停用")
    KJS_80, @MetaData(value = "复核通过")
    KJS_90;
}
