package com.meiyuetao.myt.kuajing.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提交到怡亚通的订单")
@Entity
@Table(name = "yyt_order_info_rsp")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YytOrderInfoRsp extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("交易码*")
    private YytOrderRspEnum status;
    @MetaData("信息")
    private String message;
    private YytOrderInfo yytOrderInfo;

    private List<YytOrderInfoRspDetail> yytOrderInfoRspDetails = new ArrayList<YytOrderInfoRspDetail>();

    public enum YytOrderRspEnum {
        @MetaData(value = "全部成功")
        Y_1,

        @MetaData(value = "存在失败")
        Y_2;

    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public YytOrderRspEnum getStatus() {
        return status;
    }

    public void setStatus(YytOrderRspEnum status) {
        this.status = status;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @OneToMany(mappedBy = "orderInfoRsp", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<YytOrderInfoRspDetail> getYytOrderInfoRspDetails() {
        return yytOrderInfoRspDetails;
    }

    public void setYytOrderInfoRspDetails(List<YytOrderInfoRspDetail> yytOrderInfoRspDetails) {
        this.yytOrderInfoRspDetails = yytOrderInfoRspDetails;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "yyt_order_info_sid")
    public YytOrderInfo getYytOrderInfo() {
        return yytOrderInfo;
    }

    public void setYytOrderInfo(YytOrderInfo yytOrderInfo) {
        this.yytOrderInfo = yytOrderInfo;
    }

}
