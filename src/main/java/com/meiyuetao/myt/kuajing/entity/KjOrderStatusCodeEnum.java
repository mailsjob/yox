package com.meiyuetao.myt.kuajing.entity;

import lab.s2jh.core.annotation.MetaData;

@MetaData("跨境商品备案状态码类型")
public enum KjOrderStatusCodeEnum {

    @MetaData(value = "待审批")
    KJO_20, @MetaData(value = "审核通过 ")
    KJO_30, @MetaData(value = "审核退单")
    KJO_40, @MetaData(value = "放行未结关")
    KJO_60, @MetaData(value = "结关")
    KJO_70, @MetaData(value = "转H200")
    KJO_770, @MetaData(value = "转快件 ")
    KJO_780, @MetaData(value = "抽样送检")
    KJO_790, @MetaData(value = "转缉私")
    KJO_800, @MetaData(value = "退运货物")
    KJO_810, @MetaData(value = "其他处理")
    KJO_830, @MetaData(value = "已挂起")
    KJO_840, @MetaData(value = "没收")
    KJO_850, @MetaData(value = "退货")
    KJO_80, @MetaData(value = "强制退回")
    KJO_860, @MetaData(value = "审单挂起")
    KJO_90, @MetaData(value = "退货待审批")
    KJO_85, @MetaData(value = "退货待审批")
    KJO_510, @MetaData(value = "退货自动审批退回")
    KJO_520, @MetaData(value = "退货自动审批通过")
    KJO_530, @MetaData(value = "退货人工审批退回")
    KJO_550, @MetaData(value = "退货人工审批通过")
    KJO_560, @MetaData(value = "退货强制退回")
    KJO_570, @MetaData(value = "退货放行")
    KJO_720;

}
