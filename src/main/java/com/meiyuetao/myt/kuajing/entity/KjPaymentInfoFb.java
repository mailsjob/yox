package com.meiyuetao.myt.kuajing.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@MetaData("支付单回执状态反馈")
@Entity
@Table(name = "kj_payment_info_fb")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjPaymentInfoFb extends KjHeaderInfoFb {
    private static final long serialVersionUID = 1L;
    @MetaData("支付单编号")
    private String paymentNo;
    @MetaData("状态代码")
    private KjPaymentStatusCodeEnum paymentStatusCode;
    @MetaData("操作时间")
    private Date opDate;

    private String memo;

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public Date getOpDate() {
        return opDate;
    }

    public void setOpDate(Date opDate) {
        this.opDate = opDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    public KjPaymentStatusCodeEnum getPaymentStatusCode() {
        return paymentStatusCode;
    }

    public void setPaymentStatusCode(KjPaymentStatusCodeEnum paymentStatusCode) {
        this.paymentStatusCode = paymentStatusCode;
    }

}