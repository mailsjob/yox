package com.meiyuetao.myt.kuajing.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@MetaData("跨境订单状态回执")
@Entity
@Table(name = "kj_order_info_fb")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjOrderInfoFb extends KjHeaderInfoFb {
    private static final long serialVersionUID = 1L;
    @MetaData("订单号")
    private String orderNo;
    @MetaData("原始单号")
    private String originalOrderNo;
    @MetaData("状态")
    private KjOrderStatusCodeEnum orderStatusCode;
    @MetaData("时间")
    private Date opDate;
    @MetaData("备注")
    private String memo;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOriginalOrderNo() {
        return originalOrderNo;
    }

    public void setOriginalOrderNo(String originalOrderNo) {
        this.originalOrderNo = originalOrderNo;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getOpDate() {
        return opDate;
    }

    public void setOpDate(Date opDate) {
        this.opDate = opDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public KjOrderStatusCodeEnum getOrderStatusCode() {
        return orderStatusCode;
    }

    public void setOrderStatusCode(KjOrderStatusCodeEnum orderStatusCode) {
        this.orderStatusCode = orderStatusCode;
    }

}
