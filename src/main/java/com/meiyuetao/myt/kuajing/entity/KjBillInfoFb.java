package com.meiyuetao.myt.kuajing.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@MetaData("运单状态回执")
@Entity
@Table(name = "kj_order_bar_code_fb")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjBillInfoFb extends KjHeaderInfoFb {
    private static final long serialVersionUID = 1L;
    @MetaData("支付单编号")
    private String paymentNo;
    @MetaData("状态代码")
    private String statusCode;
    @MetaData("操作时间")
    private Date opDate;
    @MetaData("MEMO")
    private String memo;

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Date getOpDate() {
        return opDate;
    }

    public void setOpDate(Date opDate) {
        this.opDate = opDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}
