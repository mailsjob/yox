package com.meiyuetao.myt.kuajing.entity;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("跨境sku信息")
@Entity
@Table(name = "myt_kj_sku_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MytKjSkuInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("电商企业代码")
    private String eshopEntCode;
    @MetaData("电商企业名称")
    private String eshopEntName;
    @MetaData("外部系统编号")
    private String externalNo;
    @MetaData("商品货号")
    private String sku;
    @MetaData("商品名称")
    private String goodsName;

    @MetaData("规格型号")
    private String goodesSpec;
    @MetaData("申报计量单位")
    private String declareUnit;
    @MetaData("行邮税号")
    private String postTaxNO;
    @MetaData("法定计量单位")
    private String legalUnit;
    @MetaData("法定计量单位折算数量")
    private String convLegalUnitNum;

    @MetaData("商品HS编码")
    private String hsCode;
    @MetaData("入区计量单位")
    private String inAreaUnit;
    @MetaData("入区计量单位折算数量")
    private String convInAreaUnitNum;

    @MetaData("是否试点商品")
    private Boolean isExperimentGoods;
    @MetaData("国家地区代码")
    private String originCountryCode;
    @MetaData("国外生产企业是否在中国注册备案（食药监局、国家认监委）")
    private Boolean isCncaPor;
    @MetaData("食药监局、国家认监委备案附件")
    private String cncaPorDoc;
    @MetaData("原产地证书")
    private String originPlaceCert;
    @MetaData("境外官方及第三方机构的检测报告")
    private String testReport;
    @MetaData("合法采购证明（国外进货发票或小票）")
    private String legalTicket;
    @MetaData("外文标签的中文翻译件")
    private String markExchange;
    @MetaData("施检机构的代码")
    private String checkOrgCode;
    @MetaData("供应商名称")
    private String supplierName;

    @MetaData("发货单状态")
    private MytKjSkuStatusEnum status = MytKjSkuStatusEnum.EDIT;

    public enum MytKjSkuStatusEnum {
        @MetaData("编辑")
        EDIT,

        @MetaData("提交")
        POST, @MetaData("审核通过")
        PASS, @MetaData("驳回")
        REJECT, @MetaData("已拣货")
        PICK, @MetaData("已接单")
        ACCEPT, @MetaData("已发货")
        DELIVERY, @MetaData("已红冲")
        REDW;

    }

    @JsonProperty
    public String getEshopEntCode() {
        return eshopEntCode;
    }

    public void setEshopEntCode(String eshopEntCode) {
        this.eshopEntCode = eshopEntCode;
    }

    @JsonProperty
    public String getEshopEntName() {
        return eshopEntName;
    }

    public void setEshopEntName(String eshopEntName) {
        this.eshopEntName = eshopEntName;
    }

    @JsonProperty
    public String getExternalNo() {
        return externalNo;
    }

    public void setExternalNo(String externalNo) {
        this.externalNo = externalNo;
    }

    @JsonProperty
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @JsonProperty
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @JsonProperty
    public String getGoodesSpec() {
        return goodesSpec;
    }

    public void setGoodesSpec(String goodesSpec) {
        this.goodesSpec = goodesSpec;
    }

    @JsonProperty
    public String getDeclareUnit() {
        return declareUnit;
    }

    public void setDeclareUnit(String declareUnit) {
        this.declareUnit = declareUnit;
    }

    @JsonProperty
    public String getPostTaxNO() {
        return postTaxNO;
    }

    public void setPostTaxNO(String postTaxNO) {
        this.postTaxNO = postTaxNO;
    }

    @JsonProperty
    public String getLegalUnit() {
        return legalUnit;
    }

    public void setLegalUnit(String legalUnit) {
        this.legalUnit = legalUnit;
    }

    @JsonProperty
    public String getConvLegalUnitNum() {
        return convLegalUnitNum;
    }

    public void setConvLegalUnitNum(String convLegalUnitNum) {
        this.convLegalUnitNum = convLegalUnitNum;
    }

    @JsonProperty
    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    @JsonProperty
    public String getInAreaUnit() {
        return inAreaUnit;
    }

    public void setInAreaUnit(String inAreaUnit) {
        this.inAreaUnit = inAreaUnit;
    }

    @JsonProperty
    public String getConvInAreaUnitNum() {
        return convInAreaUnitNum;
    }

    public void setConvInAreaUnitNum(String convInAreaUnitNum) {
        this.convInAreaUnitNum = convInAreaUnitNum;
    }

    @JsonProperty
    public Boolean getIsExperimentGoods() {
        return isExperimentGoods;
    }

    public void setIsExperimentGoods(Boolean isExperimentGoods) {
        this.isExperimentGoods = isExperimentGoods;
    }

    @JsonProperty
    public String getOriginCountryCode() {
        return originCountryCode;
    }

    public void setOriginCountryCode(String originCountryCode) {
        this.originCountryCode = originCountryCode;
    }

    @JsonProperty
    public Boolean getIsCncaPor() {
        return isCncaPor;
    }

    public void setIsCncaPor(Boolean isCncaPor) {
        this.isCncaPor = isCncaPor;
    }

    @JsonProperty
    @Lob
    public String getCncaPorDoc() {
        return cncaPorDoc;
    }

    public void setCncaPorDoc(String cncaPorDoc) {
        this.cncaPorDoc = cncaPorDoc;
    }

    @JsonProperty
    @Lob
    public String getOriginPlaceCert() {
        return originPlaceCert;
    }

    public void setOriginPlaceCert(String originPlaceCert) {
        this.originPlaceCert = originPlaceCert;
    }

    @JsonProperty
    @Lob
    public String getTestReport() {
        return testReport;
    }

    public void setTestReport(String testReport) {
        this.testReport = testReport;
    }

    @JsonProperty
    @Lob
    public String getLegalTicket() {
        return legalTicket;
    }

    public void setLegalTicket(String legalTicket) {
        this.legalTicket = legalTicket;
    }

    @JsonProperty
    @Lob
    public String getMarkExchange() {
        return markExchange;
    }

    public void setMarkExchange(String markExchange) {
        this.markExchange = markExchange;
    }

    @JsonProperty
    public String getCheckOrgCode() {
        return checkOrgCode;
    }

    public void setCheckOrgCode(String checkOrgCode) {
        this.checkOrgCode = checkOrgCode;
    }

    @JsonProperty
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
}
