package com.meiyuetao.myt.kuajing.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.service.KuajingMessageService;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail;
import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;

@MetaData("跨境订单")
@Entity
@Table(name = "kj_order_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjOrderInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("申报海关代码")
    private String customersCode;
    @MetaData("业务类型")
    private String bizTypeCode;
    @MetaData("原始单号")
    private String originalOrderNo;
    @MetaData("电商企业代码")
    private String eshopEntCode;
    @MetaData("电商企业名称")
    private String eshopEntName;
    @MetaData("起运国")
    private String despArriCountryCode;
    @MetaData("运输方式")
    private String shipToolCode;
    @MetaData("收货人身份证号码")
    private String receiverIdNo;
    @MetaData("收货人姓名")
    private String receiverName;
    @MetaData("收货人地址")
    private String receiverAddress;
    @MetaData("收货人电话")
    private String receiverTel;
    @MetaData("货款总额")
    private BigDecimal goodsFee;
    @MetaData("税金总额")
    private BigDecimal taxFee;
    @MetaData("毛重")
    private BigDecimal grossWeight;
    @MetaData("代理企业代码")
    private String proxyEntCode;

    @MetaData("代理企业名称")
    private String proxyEntName;
    @MetaData("分拣线ID")
    private String sortlingId;
    @MetaData("运费")
    private BigDecimal transportFee;

    private List<KjOrderDetailInfo> kjOrderDetailInfos = new ArrayList<KjOrderDetailInfo>();

    private String xmlStr;

    public KjOrderInfo() {
        // TODO Auto-generated constructor stub
    }

    public KjOrderInfo(BoxOrder bo, String receiverIdNo) {
        this.customersCode = "8013";
        this.bizTypeCode = "I20";
        this.originalOrderNo = bo.getOrderSeq();
        this.eshopEntCode = KuajingMessageService.userName;
        this.eshopEntName = KuajingMessageService.eshopEntName;
        // 起运国 TODO
        this.despArriCountryCode = "324";
        // 运输方式 TODO
        this.shipToolCode = "Y";
        // 收货人身份证号码
        this.receiverIdNo = receiverIdNo;
        // 收货人姓名
        this.receiverName = bo.getReceivePerson();
        // 收货人地址
        this.receiverAddress = bo.getDeliveryAddr();
        // 收货人电话
        this.receiverTel = bo.getMobilePhone();
        // 货款总额
        this.goodsFee = bo.getActualAmount();
        // 税金总额 TODO
        this.taxFee = new BigDecimal(0);
        // 毛重 TODO
        this.grossWeight = new BigDecimal(2.0);
        // 分拣线ID TODO
        this.sortlingId = "SORTLINE02";

        for (BoxOrderDetail bod : bo.getBoxOrderDetails()) {// 2
            for (BoxOrderDetailCommodity bodc : bod.getBoxOrderDetailCommodities()) { // 3
                KjOrderDetailInfo kjOrderDetailInfo = new KjOrderDetailInfo(bodc);
                kjOrderDetailInfo.setKjOrderInfo(this);
                kjOrderDetailInfos.add(kjOrderDetailInfo);
            }
        }

        // xmlStr;
    }

    @JsonProperty
    public String getBizTypeCode() {
        return bizTypeCode;
    }

    public void setBizTypeCode(String bizTypeCode) {
        this.bizTypeCode = bizTypeCode;
    }

    @JsonProperty
    public String getOriginalOrderNo() {
        return originalOrderNo;
    }

    public void setOriginalOrderNo(String originalOrderNo) {
        this.originalOrderNo = originalOrderNo;
    }

    @JsonProperty
    public String getEshopEntCode() {
        return eshopEntCode;
    }

    public void setEshopEntCode(String eshopEntCode) {
        this.eshopEntCode = eshopEntCode;
    }

    @JsonProperty
    public String getEshopEntName() {
        return eshopEntName;
    }

    public void setEshopEntName(String eshopEntName) {
        this.eshopEntName = eshopEntName;
    }

    @JsonProperty
    public String getDespArriCountryCode() {
        return despArriCountryCode;
    }

    public void setDespArriCountryCode(String despArriCountryCode) {
        this.despArriCountryCode = despArriCountryCode;
    }

    @JsonProperty
    public String getShipToolCode() {
        return shipToolCode;
    }

    public void setShipToolCode(String shipToolCode) {
        this.shipToolCode = shipToolCode;
    }

    @JsonProperty
    public String getReceiverIdNo() {
        return receiverIdNo;
    }

    public void setReceiverIdNo(String receiverIdNo) {
        this.receiverIdNo = receiverIdNo;
    }

    @JsonProperty
    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    @JsonProperty
    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    @JsonProperty
    public String getReceiverTel() {
        return receiverTel;
    }

    public void setReceiverTel(String receiverTel) {
        this.receiverTel = receiverTel;
    }

    @JsonProperty
    public BigDecimal getGoodsFee() {
        return goodsFee;
    }

    public void setGoodsFee(BigDecimal goodsFee) {
        this.goodsFee = goodsFee;
    }

    @JsonProperty
    public BigDecimal getTaxFee() {
        return taxFee;
    }

    public void setTaxFee(BigDecimal taxFee) {
        this.taxFee = taxFee;
    }

    @JsonProperty
    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    @JsonProperty
    public String getProxyEntCode() {
        return proxyEntCode;
    }

    public void setProxyEntCode(String proxyEntCode) {
        this.proxyEntCode = proxyEntCode;
    }

    @JsonProperty
    public String getProxyEntName() {
        return proxyEntName;
    }

    public void setProxyEntName(String proxyEntName) {
        this.proxyEntName = proxyEntName;
    }

    @JsonProperty
    public String getSortlingId() {
        return sortlingId;
    }

    public void setSortlingId(String sortlingId) {
        this.sortlingId = sortlingId;
    }

    @Lob
    public String getXmlStr() {
        return xmlStr;
    }

    public void setXmlStr(String xmlStr) {
        this.xmlStr = xmlStr;
    }

    @OneToMany(mappedBy = "kjOrderInfo", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<KjOrderDetailInfo> getKjOrderDetailInfos() {
        return kjOrderDetailInfos;
    }

    public void setKjOrderDetailInfos(List<KjOrderDetailInfo> kjOrderDetailInfos) {
        this.kjOrderDetailInfos = kjOrderDetailInfos;
    }

    @JsonProperty
    public String getCustomersCode() {
        return customersCode;
    }

    public void setCustomersCode(String customersCode) {
        this.customersCode = customersCode;
    }

    public BigDecimal getTransportFee() {
        return transportFee;
    }

    public void setTransportFee(BigDecimal transportFee) {
        this.transportFee = transportFee;
    }

}
