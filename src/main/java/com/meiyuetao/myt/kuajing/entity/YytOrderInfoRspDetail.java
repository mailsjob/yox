package com.meiyuetao.myt.kuajing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提交到怡亚通的订单")
@Entity
@Table(name = "yyt_order_info_rsp_detail")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YytOrderInfoRspDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private YytOrderInfoRsp orderInfoRsp;
    private YytOrderRspDetailEnum sts;
    private String pocode;
    private String message;

    public enum YytOrderRspDetailEnum {
        @MetaData(value = "成功")
        Y_1,

        @MetaData(value = "失败")
        Y_2;

    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public YytOrderRspDetailEnum getSts() {
        return sts;
    }

    public void setSts(YytOrderRspDetailEnum sts) {
        this.sts = sts;
    }

    @JsonProperty
    public String getPocode() {
        return pocode;
    }

    public void setPocode(String pocode) {
        this.pocode = pocode;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @ManyToOne
    @JoinColumn(name = "order_rsp_sid")
    public YytOrderInfoRsp getOrderInfoRsp() {
        return orderInfoRsp;
    }

    public void setOrderInfoRsp(YytOrderInfoRsp orderInfoRsp) {
        this.orderInfoRsp = orderInfoRsp;
    }

}
