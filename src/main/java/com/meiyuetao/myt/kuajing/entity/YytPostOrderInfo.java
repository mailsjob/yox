package com.meiyuetao.myt.kuajing.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提交到怡亚通的订单")
@Entity
@Table(name = "yyt_post_order_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YytPostOrderInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("交易码*")
    private String fbType;
    @MetaData("客户编号*")
    private String shipToCode;
    @MetaData("订单号")
    private String pocode;
    @MetaData("订单日期")
    private String podate;
    @MetaData("收货人名")
    private String shiptoname;
    @MetaData("手机")
    private String mobile;
    @MetaData("电话")
    private String tel;
    @MetaData("城市")
    private String city;
    @MetaData("地址")
    private String address;
    @MetaData("备注1")
    private String remark1;
    @MetaData("备注2")
    private String remark2;
    @MetaData("快递公司")
    private String expressCompany;
    @MetaData("快递单号")
    private BigDecimal expressCode;
    @MetaData("买方ID")
    private BigDecimal buyerid;
    @MetaData("海关通关号")
    private BigDecimal customcode;
    private List<YytPostOrderDetailInfo> postOrderDetailInfo = new ArrayList<YytPostOrderDetailInfo>();
    private String xmlStr;

    public String getFbType() {
        return fbType;
    }

    public void setFbType(String fbType) {
        this.fbType = fbType;
    }

    public String getShipToCode() {
        return shipToCode;
    }

    public void setShipToCode(String shipToCode) {
        this.shipToCode = shipToCode;
    }

    public String getPocode() {
        return pocode;
    }

    public void setPocode(String pocode) {
        this.pocode = pocode;
    }

    public String getPodate() {
        return podate;
    }

    public void setPodate(String podate) {
        this.podate = podate;
    }

    public String getShiptoname() {
        return shiptoname;
    }

    public void setShiptoname(String shiptoname) {
        this.shiptoname = shiptoname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getExpressCompany() {
        return expressCompany;
    }

    public void setExpressCompany(String expressCompany) {
        this.expressCompany = expressCompany;
    }

    public BigDecimal getExpressCode() {
        return expressCode;
    }

    public void setExpressCode(BigDecimal expressCode) {
        this.expressCode = expressCode;
    }

    public BigDecimal getBuyerid() {
        return buyerid;
    }

    public void setBuyerid(BigDecimal buyerid) {
        this.buyerid = buyerid;
    }

    public BigDecimal getCustomcode() {
        return customcode;
    }

    public void setCustomcode(BigDecimal customcode) {
        this.customcode = customcode;
    }

    @OneToMany(mappedBy = "postOrderInfo", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<YytPostOrderDetailInfo> getPostOrderDetailInfo() {
        return postOrderDetailInfo;
    }

    public void setPostOrderDetailInfo(List<YytPostOrderDetailInfo> postOrderDetailInfo) {
        this.postOrderDetailInfo = postOrderDetailInfo;
    }

    @Lob
    public String getXmlStr() {
        return xmlStr;
    }

    public void setXmlStr(String xmlStr) {
        this.xmlStr = xmlStr;
    }

}
