package com.meiyuetao.myt.kuajing.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("退还税金指令")
@Entity
@Table(name = "kj_tax_return_command")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjTaxReturnCommand extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("税款预扣编号")
    private String prePayNo;
    @MetaData("订单编号")
    private String orderNo;
    @MetaData("退还税金")
    private BigDecimal taxFee;
    @MetaData("币制")
    private String currencyCode;
    @MetaData("银行编码")
    private String bankCode;
    @MetaData("银行账号")
    private String bankAccount;
    @MetaData("电商企业代码")
    private String eshopEntCode;
    @MetaData("电商企业名称")
    private String eshopEntName;

    public String getPrePayNo() {
        return prePayNo;
    }

    public void setPrePayNo(String prePayNo) {
        this.prePayNo = prePayNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getTaxFee() {
        return taxFee;
    }

    public void setTaxFee(BigDecimal taxFee) {
        this.taxFee = taxFee;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getEshopEntCode() {
        return eshopEntCode;
    }

    public void setEshopEntCode(String eshopEntCode) {
        this.eshopEntCode = eshopEntCode;
    }

    public String getEshopEntName() {
        return eshopEntName;
    }

    public void setEshopEntName(String eshopEntName) {
        this.eshopEntName = eshopEntName;
    }

}