package com.meiyuetao.myt.kuajing.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@MetaData("跨境入库回执信息")
@Entity
@Table(name = "kj_mes_ask_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjMesAskInfo extends KjHeaderInfoFb {
    private static final long serialVersionUID = 1L;
    private KjMessageTypeEnum messageBodyType;
    private String workNo;
    private Date opDate;
    private String createdNo;
    private String success;
    private String memo;
    private String udf1;
    private String udf2;
    private String udf3;
    private String udf4;
    private String udf5;

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public KjMessageTypeEnum getMessageBodyType() {
        return messageBodyType;
    }

    public void setMessageBodyType(KjMessageTypeEnum messageBodyType) {
        this.messageBodyType = messageBodyType;
    }

    @JsonProperty
    public String getWorkNo() {
        return workNo;
    }

    public void setWorkNo(String workNo) {
        this.workNo = workNo;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getOpDate() {
        return opDate;
    }

    public void setOpDate(Date opDate) {
        this.opDate = opDate;
    }

    @JsonProperty
    public String getCreatedNo() {
        return createdNo;
    }

    public void setCreatedNo(String createdNo) {
        this.createdNo = createdNo;
    }

    @JsonProperty
    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getUdf1() {
        return udf1;
    }

    public void setUdf1(String udf1) {
        this.udf1 = udf1;
    }

    public String getUdf2() {
        return udf2;
    }

    public void setUdf2(String udf2) {
        this.udf2 = udf2;
    }

    public String getUdf3() {
        return udf3;
    }

    public void setUdf3(String udf3) {
        this.udf3 = udf3;
    }

    public String getUdf4() {
        return udf4;
    }

    public void setUdf4(String udf4) {
        this.udf4 = udf4;
    }

    public String getUdf5() {
        return udf5;
    }

    public void setUdf5(String udf5) {
        this.udf5 = udf5;
    }

}
