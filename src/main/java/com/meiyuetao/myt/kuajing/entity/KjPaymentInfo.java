package com.meiyuetao.myt.kuajing.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.service.KuajingMessageService;
import com.meiyuetao.myt.sale.entity.BoxOrder;

@MetaData("支付单")
@Entity
@Table(name = "kj_payment_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjPaymentInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("申报海关代码")
    private String customsCode;
    @MetaData("业务类型")
    private BizTypeCodeEnum bizTypeCode;
    @MetaData("电商企业代码")
    private String eshopEntCode;

    public enum BizTypeCodeEnum {
        @MetaData(value = "直购进口")
        I10,

        @MetaData(value = "网购保税进口")
        I12;
    }

    @MetaData("电商企业名称")
    private String eshopEntName;
    @MetaData("支付企业代码")
    private String paymentEntCode;
    @MetaData("支付企业名称")
    private String paymentEntName;
    @MetaData("支付交易编号")
    private String paymentNo;
    @MetaData("原始订单编号")
    private String originalOrderNo;
    @MetaData("总额")
    private BigDecimal payAmount;
    @MetaData("商品货款金额")
    private BigDecimal goodsFee;
    @MetaData("税款金额")
    private BigDecimal taxFee;
    @MetaData("支付币制")
    private String currencyCode;
    @MetaData("备注")
    private String memo;
    private String xml;

    public KjPaymentInfo() {
    }

    public KjPaymentInfo(BoxOrder bo) {
        this.customsCode = "8013";
        this.bizTypeCode = BizTypeCodeEnum.I12;
        this.eshopEntCode = KuajingMessageService.userName;
        this.eshopEntName = KuajingMessageService.eshopEntName;
        // @MetaData("支付企业代码") TODO
        this.paymentEntCode = "B0005B250000001";
        // @MetaData("支付企业名称") "交通银行"
        this.paymentEntName = bo.getPayMode();
        // @MetaData("支付交易编号") TODO
        this.paymentNo = bo.getPayVoucher();
        // @MetaData("原始订单编号")
        this.originalOrderNo = bo.getOrderSeq();
        // @MetaData("总额")
        this.payAmount = bo.getActualAmount();
        // @MetaData("商品货款金额")
        this.goodsFee = bo.getActualAmount();
        // @MetaData("税款金额")
        this.taxFee = new BigDecimal(0);
        // @MetaData("支付币制")
        this.currencyCode = "124";
        // @MetaData("备注")
        this.memo = "";
    }

    public String getCustomsCode() {
        return customsCode;
    }

    public void setCustomsCode(String customsCode) {
        this.customsCode = customsCode;
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    public BizTypeCodeEnum getBizTypeCode() {
        return bizTypeCode;
    }

    public void setBizTypeCode(BizTypeCodeEnum bizTypeCode) {
        this.bizTypeCode = bizTypeCode;
    }

    public String getEshopEntCode() {
        return eshopEntCode;
    }

    public void setEshopEntCode(String eshopEntCode) {
        this.eshopEntCode = eshopEntCode;
    }

    public String getEshopEntName() {
        return eshopEntName;
    }

    public void setEshopEntName(String eshopEntName) {
        this.eshopEntName = eshopEntName;
    }

    public String getPaymentEntCode() {
        return paymentEntCode;
    }

    public void setPaymentEntCode(String paymentEntCode) {
        this.paymentEntCode = paymentEntCode;
    }

    public String getPaymentEntName() {
        return paymentEntName;
    }

    public void setPaymentEntName(String paymentEntName) {
        this.paymentEntName = paymentEntName;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getOriginalOrderNo() {
        return originalOrderNo;
    }

    public void setOriginalOrderNo(String originalOrderNo) {
        this.originalOrderNo = originalOrderNo;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public BigDecimal getGoodsFee() {
        return goodsFee;
    }

    public void setGoodsFee(BigDecimal goodsFee) {
        this.goodsFee = goodsFee;
    }

    public BigDecimal getTaxFee() {
        return taxFee;
    }

    public void setTaxFee(BigDecimal taxFee) {
        this.taxFee = taxFee;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Lob
    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

}