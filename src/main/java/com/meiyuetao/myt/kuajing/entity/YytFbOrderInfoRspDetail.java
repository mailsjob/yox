package com.meiyuetao.myt.kuajing.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提交到怡亚通的订单")
@Entity
@Table(name = "yyt_fb_order_info_rsp_detail")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YytFbOrderInfoRspDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private YytFbOrderInfoRsp fbOrderRsp;
    private Long seno;
    private String pocode;
    private OrderStsEnum sts;
    private Date dealDate;
    private String expresscompany;
    private String expresscode;

    public enum OrderStsEnum {
        @MetaData(value = "已送货")
        SED;

    }

    public Long getSeno() {
        return seno;
    }

    public void setSeno(Long seno) {
        this.seno = seno;
    }

    public String getPocode() {
        return pocode;
    }

    public void setPocode(String pocode) {
        this.pocode = pocode;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public OrderStsEnum getSts() {
        return sts;
    }

    public void setSts(OrderStsEnum sts) {
        this.sts = sts;
    }

    @JsonSerialize(using = DateJsonSerializer.class)
    @Temporal(TemporalType.DATE)
    public Date getDealDate() {
        return dealDate;
    }

    public void setDealDate(Date dealDate) {
        this.dealDate = dealDate;
    }

    public String getExpresscompany() {
        return expresscompany;
    }

    public void setExpresscompany(String expresscompany) {
        this.expresscompany = expresscompany;
    }

    public String getExpresscode() {
        return expresscode;
    }

    public void setExpresscode(String expresscode) {
        this.expresscode = expresscode;
    }

    @ManyToOne
    @JoinColumn(name = "fb_order_rsp_sid")
    public YytFbOrderInfoRsp getFbOrderRsp() {
        return fbOrderRsp;
    }

    public void setFbOrderRsp(YytFbOrderInfoRsp fbOrderRsp) {
        this.fbOrderRsp = fbOrderRsp;
    }

}
