package com.meiyuetao.myt.kuajing.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("跨境收货人身份验证")
@Entity
@Table(name = "kj_tax_prepay_command_fb")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjTaxPrepayCommandFb extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("税款预扣编号")
    private String prePayNo;
    @MetaData("银行预扣交易编号")
    private String bankPrePayNo;
    @MetaData("预扣日期")
    private Date prePayDate;
    @MetaData("处理状态")
    private PrepaySuccessEnum success;

    public enum PrepaySuccessEnum {
        @MetaData(value = "成功")
        S1,

        @MetaData(value = "失败")
        S2;
    }

    @MetaData("备注")
    private String memo;

    public String getPrePayNo() {
        return prePayNo;
    }

    public void setPrePayNo(String prePayNo) {
        this.prePayNo = prePayNo;
    }

    public String getBankPrePayNo() {
        return bankPrePayNo;
    }

    public void setBankPrePayNo(String bankPrePayNo) {
        this.bankPrePayNo = bankPrePayNo;
    }

    public Date getPrePayDate() {
        return prePayDate;
    }

    public void setPrePayDate(Date prePayDate) {
        this.prePayDate = prePayDate;
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    public PrepaySuccessEnum getSuccess() {
        return success;
    }

    public void setSuccess(PrepaySuccessEnum success) {
        this.success = success;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}