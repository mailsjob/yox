package com.meiyuetao.myt.kuajing.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("税金退还信息回执")
@Entity
@Table(name = "kj_tax_return_command_fb")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjTaxReturnCommandFb extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("税款预扣编号")
    private String prePayNo;
    @MetaData("银行退还交易编号")
    private String bankReturnNo;
    @MetaData("退还日期")
    private Date returnDate;
    @MetaData("处理状态")
    private ReturnSuccessEnum success;

    public enum ReturnSuccessEnum {
        @MetaData(value = "成功")
        S1,

        @MetaData(value = "失败")
        S2;
    }

    @MetaData("备注")
    private String memo;

    public String getPrePayNo() {
        return prePayNo;
    }

    public void setPrePayNo(String prePayNo) {
        this.prePayNo = prePayNo;
    }

    public String getBankReturnNo() {
        return bankReturnNo;
    }

    public void setBankReturnNo(String bankReturnNo) {
        this.bankReturnNo = bankReturnNo;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    public ReturnSuccessEnum getSuccess() {
        return success;
    }

    public void setSuccess(ReturnSuccessEnum success) {
        this.success = success;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}