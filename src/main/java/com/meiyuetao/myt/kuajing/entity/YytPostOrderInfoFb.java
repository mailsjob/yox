package com.meiyuetao.myt.kuajing.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提交到怡亚通的订单")
@Entity
@Table(name = "yyt_post_order_info_fb")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YytPostOrderInfoFb extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("交易码*")
    private String fbType;
    @MetaData("信息")
    private String message;

}
