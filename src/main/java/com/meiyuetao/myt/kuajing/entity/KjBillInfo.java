package com.meiyuetao.myt.kuajing.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.sale.entity.BoxOrder;

@MetaData("运单")
@Entity
@Table(name = "kj_bill_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjBillInfo extends KjHeaderInfoFb {
    private static final long serialVersionUID = 1L;
    private KjMessageTypeEnum messageBodyType;
    // 原始单号
    private String originalOrderNo;
    // 业务类型
    private String bizTypeCode;
    // 业务类型名称
    private String bizTypeName;
    // 分运单号
    private String transportBillNo;
    // 电商企业代码
    private String eshopEntCode;
    // 电商企业名称
    private String eshopEntName;
    // 关区代码
    private String customsCode;
    // 关区名称
    private String customsName;
    // 物流企业代码
    private String logisticsEntCode;
    // 物流企业名称
    private String logisticsEntName;
    // 数量
    private BigDecimal qty;
    // 收件人身份证号
    private String receiverIdNo;
    // 收件人姓名
    private String receiverName;
    // // 收件人地址
    // private String receiverAddress;
    // 收件人电话
    private String receiverTel;

    private KjBillInfoEnum statusCode;

    public KjBillInfo() {
    }

    public KjBillInfo(BoxOrder bo, String receiverIdNo) {
        // TODO 对应字段关系

    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32)
    @JsonProperty
    public KjMessageTypeEnum getMessageBodyType() {
        return messageBodyType;
    }

    public void setMessageBodyType(KjMessageTypeEnum messageBodyType) {
        this.messageBodyType = messageBodyType;
    }

    public String getOriginalOrderNo() {
        return originalOrderNo;
    }

    public void setOriginalOrderNo(String originalOrderNo) {
        this.originalOrderNo = originalOrderNo;
    }

    public String getBizTypeCode() {
        return bizTypeCode;
    }

    public void setBizTypeCode(String bizTypeCode) {
        this.bizTypeCode = bizTypeCode;
    }

    public String getBizTypeName() {
        return bizTypeName;
    }

    public void setBizTypeName(String bizTypeName) {
        this.bizTypeName = bizTypeName;
    }

    public String getTransportBillNo() {
        return transportBillNo;
    }

    public void setTransportBillNo(String transportBillNo) {
        this.transportBillNo = transportBillNo;
    }

    public String getEshopEntCode() {
        return eshopEntCode;
    }

    public void setEshopEntCode(String eshopEntCode) {
        this.eshopEntCode = eshopEntCode;
    }

    public String getEshopEntName() {
        return eshopEntName;
    }

    public void setEshopEntName(String eshopEntName) {
        this.eshopEntName = eshopEntName;
    }

    public String getCustomsCode() {
        return customsCode;
    }

    public void setCustomsCode(String customsCode) {
        this.customsCode = customsCode;
    }

    public String getCustomsName() {
        return customsName;
    }

    public void setCustomsName(String customsName) {
        this.customsName = customsName;
    }

    public String getLogisticsEntCode() {
        return logisticsEntCode;
    }

    public void setLogisticsEntCode(String logisticsEntCode) {
        this.logisticsEntCode = logisticsEntCode;
    }

    public String getLogisticsEntName() {
        return logisticsEntName;
    }

    public void setLogisticsEntName(String logisticsEntName) {
        this.logisticsEntName = logisticsEntName;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public String getReceiverIdNo() {
        return receiverIdNo;
    }

    public void setReceiverIdNo(String receiverIdNo) {
        this.receiverIdNo = receiverIdNo;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverTel() {
        return receiverTel;
    }

    public void setReceiverTel(String receiverTel) {
        this.receiverTel = receiverTel;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32)
    @JsonProperty
    public KjBillInfoEnum getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(KjBillInfoEnum statusCode) {
        this.statusCode = statusCode;
    }

}
