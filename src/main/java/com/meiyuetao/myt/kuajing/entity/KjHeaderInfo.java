package com.meiyuetao.myt.kuajing.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.envers.AuditOverride;
import org.hibernate.envers.AuditOverrides;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MappedSuperclass
@AuditOverrides({ @AuditOverride(forClass = MytBaseEntity.class) })
@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public abstract class KjHeaderInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private KjMessageTypeEnum receiptType;
    @MetaData("申报类型")
    private KjActionTypeEnum actionType = KjActionTypeEnum.NEW;

    public enum KjActionTypeEnum {
        @MetaData(value = "新增")
        NEW, @MetaData(value = "变更")
        EDIT, @MetaData(value = "暂停")
        PAUSE,
    }

    private Date messageTime;
    private String senderId;
    private String userNo;
    private String password;

    private String xmlStr;

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public KjMessageTypeEnum getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(KjMessageTypeEnum receiptType) {
        this.receiptType = receiptType;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public KjActionTypeEnum getActionType() {
        return actionType;
    }

    public void setActionType(KjActionTypeEnum actionType) {
        this.actionType = actionType;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(Date messageTime) {
        this.messageTime = messageTime;
    }

    @JsonProperty
    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    @JsonProperty
    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Lob
    public String getXmlStr() {
        return xmlStr;
    }

    public void setXmlStr(String xmlStr) {
        this.xmlStr = xmlStr;
    }

}
