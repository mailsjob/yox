package com.meiyuetao.myt.kuajing.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("跨境收货人身份验证")
@Entity
@Table(name = "kj_user_validate_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjUserValidateInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("真是姓名")
    private String trueName;
    @MetaData("身份证号")
    private String idNumber;
    @MetaData("电话")
    private String mobilePhone;
    @MetaData("备注")
    private String memo;

    @JsonProperty
    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    @JsonProperty
    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @JsonProperty
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}