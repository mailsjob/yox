package com.meiyuetao.myt.kuajing.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@MetaData("跨境订单状态回执")
@Entity
@Table(name = "kj_order_bar_code_fb")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjOrderBarCodeFb extends KjHeaderInfoFb {
    private static final long serialVersionUID = 1L;
    @MetaData("订单号")
    private String orderNo;
    @MetaData("原始单号")
    private String originalOrderNo;
    @MetaData("海关条码")
    private String barCode;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOriginalOrderNo() {
        return originalOrderNo;
    }

    public void setOriginalOrderNo(String originalOrderNo) {
        this.originalOrderNo = originalOrderNo;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

}
