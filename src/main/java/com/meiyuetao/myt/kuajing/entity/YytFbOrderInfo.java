package com.meiyuetao.myt.kuajing.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提交到怡亚通的订单")
@Entity
@Table(name = "yyt_fb_order_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YytFbOrderInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private String fbType;
    private String shipToCode;
    private Long beginno;
    private Long endno;
    private String poCodes;

    public String getFbType() {
        return fbType;
    }

    public void setFbType(String fbType) {
        this.fbType = fbType;
    }

    public String getShipToCode() {
        return shipToCode;
    }

    public void setShipToCode(String shipToCode) {
        this.shipToCode = shipToCode;
    }

    public Long getBeginno() {
        return beginno;
    }

    public void setBeginno(Long beginno) {
        this.beginno = beginno;
    }

    public Long getEndno() {
        return endno;
    }

    public void setEndno(Long endno) {
        this.endno = endno;
    }

    public String getPoCodes() {
        return poCodes;
    }

    public void setPoCodes(String poCodes) {
        this.poCodes = poCodes;
    }

}
