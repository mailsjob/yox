package com.meiyuetao.myt.kuajing.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;

@MetaData("跨境订单")
@Entity
@Table(name = "kj_order_detail_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class KjOrderDetailInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private KjOrderInfo kjOrderInfo;
    @MetaData("商品货号")
    private String sku;
    @MetaData("规格型号")
    private String goodsSpec;
    @MetaData("币制代码")
    private String currencyCode;
    @MetaData("商品单价")
    private BigDecimal price;
    @MetaData("商品数量")
    private BigDecimal qty;
    @MetaData("商品总价")
    private BigDecimal goodsFee;
    @MetaData("税款金额")
    private BigDecimal taxFee;

    public KjOrderDetailInfo() {
        // TODO Auto-generated constructor stub
    }

    public KjOrderDetailInfo(BoxOrderDetailCommodity bodc) {
        Commodity c = bodc.getCommodity();
        // @MetaData("商品货号")
        this.sku = c.getSku();
        // @MetaData("规格型号")
        this.goodsSpec = c.getKjCommodityInfo().getGoodesSpec();
        // 币制代码 TODO
        this.currencyCode = "124";
        // 商品单价
        this.price = bodc.getPrice();
        // @MetaData("商品数量")
        this.qty = bodc.getQuantity();
        // @MetaData("商品总价")
        this.goodsFee = bodc.getActualAmount();
        // @MetaData("税款金额")
        this.taxFee = new BigDecimal(40);
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getGoodsFee() {
        return goodsFee;
    }

    public void setGoodsFee(BigDecimal goodsFee) {
        this.goodsFee = goodsFee;
    }

    public BigDecimal getTaxFee() {
        return taxFee;
    }

    public void setTaxFee(BigDecimal taxFee) {
        this.taxFee = taxFee;
    }

    @ManyToOne
    @JoinColumn(name = "order_sid", nullable = false)
    public KjOrderInfo getKjOrderInfo() {
        return kjOrderInfo;
    }

    public void setKjOrderInfo(KjOrderInfo kjOrderInfo) {
        this.kjOrderInfo = kjOrderInfo;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

}
