package com.meiyuetao.myt.kuajing.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("提交到怡亚通的订单")
@Entity
@Table(name = "yyt_fb_order_info_rsp")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class YytFbOrderInfoRsp extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    private YytFbOrderRspEnum status;
    private Long maxnum;
    private String message;
    private List<YytFbOrderInfoRspDetail> yytFbOrderInfoRspDetails = new ArrayList<YytFbOrderInfoRspDetail>();

    public enum YytFbOrderRspEnum {
        @MetaData(value = "全部成功")
        Y_1,

        @MetaData(value = "存在失败")
        Y_2;

    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public YytFbOrderRspEnum getStatus() {
        return status;
    }

    public void setStatus(YytFbOrderRspEnum status) {
        this.status = status;
    }

    public Long getMaxnum() {
        return maxnum;
    }

    public void setMaxnum(Long maxnum) {
        this.maxnum = maxnum;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @OneToMany(mappedBy = "fbOrderRsp", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<YytFbOrderInfoRspDetail> getYytFbOrderInfoRspDetails() {
        return yytFbOrderInfoRspDetails;
    }

    public void setYytFbOrderInfoRspDetails(List<YytFbOrderInfoRspDetail> yytFbOrderInfoRspDetails) {
        this.yytFbOrderInfoRspDetails = yytFbOrderInfoRspDetails;
    }

}
