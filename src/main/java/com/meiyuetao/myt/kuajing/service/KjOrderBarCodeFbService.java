package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjOrderBarCodeFbDao;
import com.meiyuetao.myt.kuajing.entity.KjOrderBarCodeFb;

@Service
@Transactional
public class KjOrderBarCodeFbService extends BaseService<KjOrderBarCodeFb, Long> {

    @Autowired
    private KjOrderBarCodeFbDao kjOrderBarCodeFbDao;

    @Override
    protected BaseDao<KjOrderBarCodeFb, Long> getEntityDao() {
        return kjOrderBarCodeFbDao;
    }

}
