package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjSkuInfoFbDao;
import com.meiyuetao.myt.kuajing.entity.KjSkuInfoFb;

@Service
@Transactional
public class KjSkuInfoFbService extends BaseService<KjSkuInfoFb, Long> {

    @Autowired
    private KjSkuInfoFbDao kjSkuInfoFbDao;

    @Override
    protected BaseDao<KjSkuInfoFb, Long> getEntityDao() {
        return kjSkuInfoFbDao;
    }

}
