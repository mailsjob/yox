package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjPaymentInfoFbDao;
import com.meiyuetao.myt.kuajing.entity.KjPaymentInfoFb;

@Service
@Transactional
public class KjPaymentInfoFbService extends BaseService<KjPaymentInfoFb, Long> {

    @Autowired
    private KjPaymentInfoFbDao kjPaymentInfoFbDao;

    @Override
    protected BaseDao<KjPaymentInfoFb, Long> getEntityDao() {
        return kjPaymentInfoFbDao;
    }
}
