package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.MytKjSkuInfoDao;
import com.meiyuetao.myt.kuajing.entity.MytKjSkuInfo;

@Service
@Transactional
public class MytKjSkuInfoService extends BaseService<MytKjSkuInfo, Long> {

    @Autowired
    private MytKjSkuInfoDao mytKjSkuInfoDao;

    @Override
    protected BaseDao<MytKjSkuInfo, Long> getEntityDao() {
        return mytKjSkuInfoDao;
    }
}
