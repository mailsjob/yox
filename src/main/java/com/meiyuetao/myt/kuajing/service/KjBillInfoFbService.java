package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjBillInfoFbDao;
import com.meiyuetao.myt.kuajing.entity.KjBillInfoFb;

@Service
@Transactional
public class KjBillInfoFbService extends BaseService<KjBillInfoFb, Long> {

    @Autowired
    private KjBillInfoFbDao kjBillInfoFbDao;

    @Override
    protected BaseDao<KjBillInfoFb, Long> getEntityDao() {
        return kjBillInfoFbDao;
    }
}
