package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjCommodityInfoDao;
import com.meiyuetao.myt.md.entity.KjCommodityInfo;

@Service
@Transactional
public class KjCommodityInfoService extends BaseService<KjCommodityInfo, Long> {

    @Autowired
    private KjCommodityInfoDao KjCommodityInfoDao;

    @Override
    protected BaseDao<KjCommodityInfo, Long> getEntityDao() {
        return KjCommodityInfoDao;
    }
}
