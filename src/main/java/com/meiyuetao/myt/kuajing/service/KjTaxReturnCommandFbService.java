package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjTaxReturnCommandFbDao;
import com.meiyuetao.myt.kuajing.entity.KjTaxReturnCommandFb;

@Service
@Transactional
public class KjTaxReturnCommandFbService extends BaseService<KjTaxReturnCommandFb, Long> {

    @Autowired
    private KjTaxReturnCommandFbDao kjTaxReturnCommandFbDao;

    @Override
    protected BaseDao<KjTaxReturnCommandFb, Long> getEntityDao() {
        return kjTaxReturnCommandFbDao;
    }
}
