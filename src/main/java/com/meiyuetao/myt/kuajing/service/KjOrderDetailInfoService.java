package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjOrderDetailInfoDao;
import com.meiyuetao.myt.kuajing.entity.KjOrderDetailInfo;

@Service
@Transactional
public class KjOrderDetailInfoService extends BaseService<KjOrderDetailInfo, Long> {

    @Autowired
    private KjOrderDetailInfoDao kjOrderDetailInfoDao;

    @Override
    protected BaseDao<KjOrderDetailInfo, Long> getEntityDao() {
        return kjOrderDetailInfoDao;
    }

}
