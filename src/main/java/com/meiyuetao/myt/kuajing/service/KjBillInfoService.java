package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjBillInfoDao;
import com.meiyuetao.myt.kuajing.entity.KjBillInfo;

@Service
@Transactional
public class KjBillInfoService extends BaseService<KjBillInfo, Long> {

    @Autowired
    private KjBillInfoDao kjBillInfoDao;

    @Override
    protected BaseDao<KjBillInfo, Long> getEntityDao() {
        return kjBillInfoDao;
    }
}
