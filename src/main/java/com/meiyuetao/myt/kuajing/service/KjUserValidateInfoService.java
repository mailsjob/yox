package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjUserValidateInfoDao;
import com.meiyuetao.myt.kuajing.entity.KjUserValidateInfo;

@Service
@Transactional
public class KjUserValidateInfoService extends BaseService<KjUserValidateInfo, Long> {

    @Autowired
    private KjUserValidateInfoDao kjUserValidateInfoDao;

    @Override
    protected BaseDao<KjUserValidateInfo, Long> getEntityDao() {
        return kjUserValidateInfoDao;
    }
}
