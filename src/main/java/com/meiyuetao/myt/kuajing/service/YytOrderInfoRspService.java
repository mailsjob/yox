package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.YytOrderInfoRspDao;
import com.meiyuetao.myt.kuajing.entity.YytOrderInfoRsp;

@Service
@Transactional
public class YytOrderInfoRspService extends BaseService<YytOrderInfoRsp, Long> {

    @Autowired
    private YytOrderInfoRspDao yytOrderInfoRspDao;

    @Override
    protected BaseDao<YytOrderInfoRsp, Long> getEntityDao() {
        return yytOrderInfoRspDao;
    }

}
