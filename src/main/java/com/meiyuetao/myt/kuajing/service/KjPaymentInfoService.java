package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjPaymentInfoDao;
import com.meiyuetao.myt.kuajing.entity.KjPaymentInfo;

@Service
@Transactional
public class KjPaymentInfoService extends BaseService<KjPaymentInfo, Long> {

    @Autowired
    private KjPaymentInfoDao kjPaymentInfoDao;

    @Override
    protected BaseDao<KjPaymentInfo, Long> getEntityDao() {
        return kjPaymentInfoDao;
    }
}
