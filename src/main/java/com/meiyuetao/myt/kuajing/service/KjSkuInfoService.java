package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjSkuInfoDao;
import com.meiyuetao.myt.kuajing.entity.KjSkuInfo;

@Service
@Transactional
public class KjSkuInfoService extends BaseService<KjSkuInfo, Long> {

    @Autowired
    private KjSkuInfoDao kjSkuInfoDao;

    @Override
    protected BaseDao<KjSkuInfo, Long> getEntityDao() {
        return kjSkuInfoDao;
    }
}
