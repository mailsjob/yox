package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjOrderInfoFbDao;
import com.meiyuetao.myt.kuajing.entity.KjOrderInfoFb;

@Service
@Transactional
public class KjOrderInfoFbService extends BaseService<KjOrderInfoFb, Long> {

    @Autowired
    private KjOrderInfoFbDao kjOrderInfoFbDao;

    @Override
    protected BaseDao<KjOrderInfoFb, Long> getEntityDao() {
        return kjOrderInfoFbDao;
    }

}
