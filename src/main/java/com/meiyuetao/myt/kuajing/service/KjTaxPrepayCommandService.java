package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjTaxPrepayCommandDao;
import com.meiyuetao.myt.kuajing.entity.KjTaxPrepayCommand;

@Service
@Transactional
public class KjTaxPrepayCommandService extends BaseService<KjTaxPrepayCommand, Long> {

    @Autowired
    private KjTaxPrepayCommandDao kjTaxPrepayCommandDao;

    @Override
    protected BaseDao<KjTaxPrepayCommand, Long> getEntityDao() {
        return kjTaxPrepayCommandDao;
    }
}
