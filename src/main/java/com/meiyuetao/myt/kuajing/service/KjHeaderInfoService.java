package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjHeaderInfoDao;
import com.meiyuetao.myt.kuajing.entity.KjHeaderInfo;

@Service
@Transactional
public class KjHeaderInfoService extends BaseService<KjHeaderInfo, Long> {

    @Autowired
    private KjHeaderInfoDao kjHeaderInfoDao;

    @Override
    protected BaseDao<KjHeaderInfo, Long> getEntityDao() {
        return kjHeaderInfoDao;
    }

}
