package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjMesAskInfoDao;
import com.meiyuetao.myt.kuajing.entity.KjMesAskInfo;

@Service
@Transactional
public class KjMesAskInfoService extends BaseService<KjMesAskInfo, Long> {

    @Autowired
    private KjMesAskInfoDao kjMesAskInfoDao;

    @Override
    protected BaseDao<KjMesAskInfo, Long> getEntityDao() {
        return kjMesAskInfoDao;
    }

}
