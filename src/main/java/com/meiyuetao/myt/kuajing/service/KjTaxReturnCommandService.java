package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjTaxReturnCommandDao;
import com.meiyuetao.myt.kuajing.entity.KjTaxReturnCommand;

@Service
@Transactional
public class KjTaxReturnCommandService extends BaseService<KjTaxReturnCommand, Long> {

    @Autowired
    private KjTaxReturnCommandDao kjTaxReturnCommandDao;

    @Override
    protected BaseDao<KjTaxReturnCommand, Long> getEntityDao() {
        return kjTaxReturnCommandDao;
    }
}
