package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.YytOrderDetailInfoDao;
import com.meiyuetao.myt.kuajing.entity.YytOrderDetailInfo;

@Service
@Transactional
public class YytOrderDetailInfoService extends BaseService<YytOrderDetailInfo, Long> {

    @Autowired
    private YytOrderDetailInfoDao yytOrderDetailInfoDao;

    @Override
    protected BaseDao<YytOrderDetailInfo, Long> getEntityDao() {
        return yytOrderDetailInfoDao;
    }

}
