package com.meiyuetao.myt.kuajing.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.kuajing.dao.KjTaxPrepayCommandFbDao;
import com.meiyuetao.myt.kuajing.entity.KjTaxPrepayCommandFb;

@Service
@Transactional
public class KjTaxPrepayCommandFbService extends BaseService<KjTaxPrepayCommandFb, Long> {

    @Autowired
    private KjTaxPrepayCommandFbDao kjTaxPrepayCommandFbDao;

    @Override
    protected BaseDao<KjTaxPrepayCommandFb, Long> getEntityDao() {
        return kjTaxPrepayCommandFbDao;
    }
}
