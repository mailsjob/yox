package com.meiyuetao.myt.kuajing.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.kuajing.entity.KjSkuInfoFb;
import com.meiyuetao.myt.kuajing.service.KjSkuInfoFbService;

@MetaData("跨境sku状态回执管理")
public class KjSkuInfoFbController extends BaseController<KjSkuInfoFb, Long> {

    @Autowired
    private KjSkuInfoFbService kjSkuInfoFbService;

    @Override
    protected BaseService<KjSkuInfoFb, Long> getEntityService() {
        return kjSkuInfoFbService;
    }

    @Override
    protected void checkEntityAclPermission(KjSkuInfoFb entity) {
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}