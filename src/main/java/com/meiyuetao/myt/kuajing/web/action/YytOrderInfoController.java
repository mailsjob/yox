package com.meiyuetao.myt.kuajing.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.kuajing.entity.YytOrderDetailInfo;
import com.meiyuetao.myt.kuajing.entity.YytOrderInfo;
import com.meiyuetao.myt.kuajing.entity.YytOrderInfoRsp;
import com.meiyuetao.myt.kuajing.service.YytOrderInfoRspService;
import com.meiyuetao.myt.kuajing.service.YytOrderInfoService;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrder.YytPostStatusEnum;
import com.meiyuetao.myt.sale.service.BoxOrderService;

@MetaData("跨境sku信息管理")
public class YytOrderInfoController extends MytBaseController<YytOrderInfo, Long> {

    @Autowired
    private YytOrderInfoService yytOrderInfoService;
    @Autowired
    private BoxOrderService boxOrderService;
    @Autowired
    private YytOrderInfoRspService yytOrderInfoRspService;

    @Override
    protected BaseService<YytOrderInfo, Long> getEntityService() {
        return yytOrderInfoService;
    }

    @Override
    protected void checkEntityAclPermission(YytOrderInfo entity) {

    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    public HttpHeaders buildYytOrder() {
        Long[] ids = this.getSelectSids();
        List<BoxOrder> boxOrders = boxOrderService.findAll(ids);
        for (BoxOrder boxOrder : boxOrders) {
            if (YytPostStatusEnum.Y_1.equals(boxOrder.getYytPostStatus())) {
                continue;
            }
            yytOrderInfoService.postPoForOut(boxOrder);

        }
        setModel(OperationResult.buildSuccessResult("操作成功：提交至怡亚通"));
        return buildDefaultHttpHeaders();
    }

    public HttpHeaders updateYytOrderStatu() {
        Long[] ids = this.getSelectSids();
        List<BoxOrder> boxOrders = boxOrderService.findAll(ids);
        String[] orderSeqArray = new String[boxOrders.size()];
        int i = 0;
        for (BoxOrder boxOrder : boxOrders) {
            orderSeqArray[i++] = boxOrder.getOrderSeq();
        }
        String orderSeqs = StringUtils.join(orderSeqArray, ",");
        yytOrderInfoService.sendFbOutInfo(orderSeqs);
        setModel(OperationResult.buildSuccessResult("操作成功：提交至怡亚通"));
        return buildDefaultHttpHeaders();
    }

    public HttpHeaders orderDetailInfos() {
        List<YytOrderDetailInfo> orderDetailInfos = bindingEntity.getOrderDetailInfos();
        setModel(buildPageResultFromList(orderDetailInfos));
        return buildDefaultHttpHeaders();
    }

    /*
     * public String invokeRemoteFuc(String methodName, String pName, String
     * objectStr) { String result = ""; Service site = new Service(); Call call;
     * Object[] object = new Object[1]; object[0] = objectStr;//Object是用来存储方法的参数
     * try { call = (Call) site.createCall();
     * call.setTargetEndpointAddress(orderSendUrl);// 远程调用路径
     * call.setOperationName(methodName);// 调用的方法名
     * 
     * // 设置参数名: call.addParameter(pName, // 参数名 XMLType.XSD_STRING,//
     * 参数类型:String ParameterMode.IN);// 参数模式：'IN' or 'OUT' // 设置返回值类型：
     * call.setReturnType(XMLType.XSD_STRING);// 返回值类型：String
     * 
     * result = (String) call.invoke(object);// 远程调用
     * 
     * } catch (ServiceException e) { e.printStackTrace(); } catch
     * (RemoteException e) { e.printStackTrace(); } return result; }
     */
    public HttpHeaders getYytOrderInfoRsp() {
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "yytOrderInfo", bindingEntity));
        List<YytOrderInfoRsp> yytOrderInfoRsps = yytOrderInfoRspService.findByFilters(groupPropertyFilter);
        setModel(buildPageResultFromList(yytOrderInfoRsps));
        return buildDefaultHttpHeaders();
    }

    public HttpHeaders yytOrderInfoRspDetails() {
        YytOrderInfoRsp yytOrderInfoRsp = yytOrderInfoRspService.findOne(Long.valueOf(getParameter("orderRspId")));
        setModel(buildPageResultFromList(yytOrderInfoRsp.getYytOrderInfoRspDetails()));
        return buildDefaultHttpHeaders();
    }

}