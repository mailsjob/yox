package com.meiyuetao.myt.kuajing.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.kuajing.entity.KjMesAskInfo;
import com.meiyuetao.myt.kuajing.service.KjMesAskInfoService;

@MetaData("跨境sku信息管理")
public class KjMesAskInfoController extends BaseController<KjMesAskInfo, Long> {

    @Autowired
    private KjMesAskInfoService kjMesAskInfoService;

    @Override
    protected BaseService<KjMesAskInfo, Long> getEntityService() {
        return kjMesAskInfoService;
    }

    @Override
    protected void checkEntityAclPermission(KjMesAskInfo entity) {

    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}