package com.meiyuetao.myt.kuajing.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.kuajing.entity.KjOrderInfoFb;

@Repository
public interface KjOrderInfoFbDao extends BaseDao<KjOrderInfoFb, Long> {

}