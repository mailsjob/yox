package com.meiyuetao.myt.kuajing.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.kuajing.entity.KjPaymentInfo;

@Repository
public interface KjPaymentInfoDao extends BaseDao<KjPaymentInfo, Long> {

}