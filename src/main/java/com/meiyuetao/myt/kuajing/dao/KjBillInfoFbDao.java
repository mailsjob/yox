package com.meiyuetao.myt.kuajing.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.kuajing.entity.KjBillInfoFb;

@Repository
public interface KjBillInfoFbDao extends BaseDao<KjBillInfoFb, Long> {

}