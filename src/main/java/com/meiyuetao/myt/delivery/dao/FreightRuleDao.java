package com.meiyuetao.myt.delivery.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.delivery.entity.FreightRule;

@Repository
public interface FreightRuleDao extends BaseDao<FreightRule, Long> {

}