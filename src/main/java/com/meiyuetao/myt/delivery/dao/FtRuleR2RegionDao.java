package com.meiyuetao.myt.delivery.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.delivery.entity.FtRuleR2Region;

@Repository
public interface FtRuleR2RegionDao extends BaseDao<FtRuleR2Region, Long> {

}