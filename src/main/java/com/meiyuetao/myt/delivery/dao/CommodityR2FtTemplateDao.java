package com.meiyuetao.myt.delivery.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.delivery.entity.CommodityR2FtTemplate;

@Repository
public interface CommodityR2FtTemplateDao extends BaseDao<CommodityR2FtTemplate, Long> {

}