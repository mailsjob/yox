package com.meiyuetao.myt.delivery.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.delivery.entity.FtTemplateR2Rule;

@Repository
public interface FtTemplateR2RuleDao extends BaseDao<FtTemplateR2Rule, Long> {

}