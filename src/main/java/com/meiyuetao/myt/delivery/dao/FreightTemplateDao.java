package com.meiyuetao.myt.delivery.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.delivery.entity.FreightTemplate;

@Repository
public interface FreightTemplateDao extends BaseDao<FreightTemplate, Long> {

}