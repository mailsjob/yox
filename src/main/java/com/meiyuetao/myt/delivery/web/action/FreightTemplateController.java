package com.meiyuetao.myt.delivery.web.action;

import java.util.List;
import java.util.Set;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.delivery.entity.FreightRule;
import com.meiyuetao.myt.delivery.entity.FreightTemplate;
import com.meiyuetao.myt.delivery.entity.FtRuleR2Region;
import com.meiyuetao.myt.delivery.entity.FtTemplateR2Rule;
import com.meiyuetao.myt.delivery.service.FreightRuleService;
import com.meiyuetao.myt.delivery.service.FreightTemplateService;
import com.meiyuetao.myt.delivery.service.FtRuleR2RegionService;
import com.meiyuetao.myt.md.entity.Region;
import com.meiyuetao.myt.md.service.CommodityService;
import com.meiyuetao.myt.md.service.RegionService;

@MetaData("运费模板管理")
public class FreightTemplateController extends BaseController<FreightTemplate, Long> {

    @Autowired
    private FreightTemplateService freightTemplateService;

    @Autowired
    private FreightRuleService freightRuleService;

    @Autowired
    protected CommodityService commodityService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected FtRuleR2RegionService ftRuleR2RegionService;

    @Override
    protected BaseService<FreightTemplate, Long> getEntityService() {
        return freightTemplateService;
    }

    @Override
    protected void checkEntityAclPermission(FreightTemplate entity) {
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    protected void setupDetachedBindingEntity(Long id) {
        bindingEntity = getEntityService().findDetachedOne(id, "templateRules");
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.isNew()) {
            for (FtTemplateR2Rule ftTemplateR2Rule : bindingEntity.getTemplateRules()) {
                ftTemplateR2Rule.setTemplate(bindingEntity);
            }
        }
        for (int i = 0; i < bindingEntity.getTemplateRules().size(); i++) {
            FtTemplateR2Rule ftTemplateR2Rule = bindingEntity.getTemplateRules().get(i);
            FreightRule freightRule = ftTemplateR2Rule.getRule();
            // 新追加的规则先进行保存
            freightRuleService.save(freightRule);
            // 更新规则关联地区：先删除所有关联，再重建关联
            Set<FtRuleR2Region> ftRuleR2Regions = freightRule.getFtRuleR2Regions();
            String[] regionsIds = freightRule.getRegionsId().split(",");
            // 规则绑定区域重解 ：
            for (String regionId : regionsIds) {
                if (StringUtils.isEmpty(regionId)) {
                    continue;
                }
                Region region = regionService.findOne(Long.valueOf(regionId));
                GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
                groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "region", region));
                groupPropertyFilter.append(new PropertyFilter(MatchType.EQ, "rule", freightRule));
                List<FtRuleR2Region> ftRuleR2Regionssss = ftRuleR2RegionService.findByFilters(groupPropertyFilter);
                if (!CollectionUtils.isEmpty(ftRuleR2Regionssss)) {
                    for (FtRuleR2Region ftt : ftRuleR2Regionssss) {
                        ftRuleR2Regions.add(ftt);
                    }
                } else {
                    FtRuleR2Region ftRuleR2Region = new FtRuleR2Region();
                    ftRuleR2Region.setRule(freightRule);
                    ftRuleR2Region.setRegion(region);
                    ftRuleR2RegionService.save(ftRuleR2Region);
                    ftRuleR2Regions.add(ftRuleR2Region);
                }
            }
            bindingEntity.getTemplateRules().get(i).getRule().setFtRuleR2Regions(ftRuleR2Regions);
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}