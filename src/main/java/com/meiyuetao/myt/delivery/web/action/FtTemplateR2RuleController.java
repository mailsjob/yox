package com.meiyuetao.myt.delivery.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.delivery.entity.FtTemplateR2Rule;
import com.meiyuetao.myt.delivery.service.FtTemplateR2RuleService;

@MetaData("运费模板关联规则管理")
public class FtTemplateR2RuleController extends BaseController<FtTemplateR2Rule, Long> {

    @Autowired
    private FtTemplateR2RuleService ftTemplateR2RuleService;

    @Override
    protected BaseService<FtTemplateR2Rule, Long> getEntityService() {
        return ftTemplateR2RuleService;
    }

    @Override
    protected void checkEntityAclPermission(FtTemplateR2Rule entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}