package com.meiyuetao.myt.delivery.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.commons.collections.CollectionUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.delivery.entity.CommodityR2FtTemplate;
import com.meiyuetao.myt.delivery.service.CommodityR2FtTemplateService;

@MetaData("商品关联运费模板管理")
public class CommodityR2FtTemplateController extends BaseController<CommodityR2FtTemplate, Long> {

    @Autowired
    private CommodityR2FtTemplateService commodityR2FtTemplateService;

    @Override
    protected BaseService<CommodityR2FtTemplate, Long> getEntityService() {
        return commodityR2FtTemplateService;
    }

    @Override
    protected void checkEntityAclPermission(CommodityR2FtTemplate entity) {
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        PropertyFilter propertyFilter = new PropertyFilter(MatchType.EQ, "commodity", bindingEntity.getCommodity());
        List<CommodityR2FtTemplate> crfs = commodityR2FtTemplateService.findByFilter(propertyFilter);
        if (!CollectionUtils.isEmpty(crfs)) {
            setModel(OperationResult.buildFailureResult("该商品已经绑定了运费模板！请选择其他商品"));
            return buildDefaultHttpHeaders();
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}