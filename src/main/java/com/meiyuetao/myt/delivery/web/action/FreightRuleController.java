package com.meiyuetao.myt.delivery.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.delivery.entity.FreightRule;
import com.meiyuetao.myt.delivery.entity.FtRuleR2Region;
import com.meiyuetao.myt.delivery.service.FreightRuleService;

@MetaData("运费规则管理")
public class FreightRuleController extends BaseController<FreightRule, Long> {

    @Autowired
    private FreightRuleService freightRuleService;

    @Override
    protected BaseService<FreightRule, Long> getEntityService() {
        return freightRuleService;
    }

    @Override
    protected void checkEntityAclPermission(FreightRule entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    protected void setupDetachedBindingEntity(Long id) {
        bindingEntity = getEntityService().findDetachedOne(id, "ftRuleR2Regions");
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.isNew()) {
            for (FtRuleR2Region ftRuleR2Region : bindingEntity.getFtRuleR2Regions()) {
                ftRuleR2Region.setRule(bindingEntity);
            }
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}