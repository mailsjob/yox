package com.meiyuetao.myt.delivery.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Region;

@MetaData("运费模板")
@Entity
@Table(name = "iyb_freight_template")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class FreightTemplate extends MytBaseEntity {

    private static final long serialVersionUID = 1L;

    @MetaData("模板名称")
    private String templateName;

    @MetaData("计价方式")
    private ValuationTypeEnum valuationType;

    @MetaData("地址")
    private Region region;

    @MetaData("运费规则")
    private List<FtTemplateR2Rule> templateRules = new ArrayList<FtTemplateR2Rule>();

    public enum ValuationTypeEnum {
        @MetaData("按件数")
        COUNTS,

        @MetaData("按重量")
        WEIGHT,

        @MetaData("满包邮")
        MBY;
    }

    @Column(name = "template_name")
    @JsonProperty
    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "valuation_type")
    @JsonProperty
    public ValuationTypeEnum getValuationType() {
        return valuationType;
    }

    public void setValuationType(ValuationTypeEnum valuationType) {
        this.valuationType = valuationType;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "region_sid")
    @JsonProperty
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @OneToMany(mappedBy = "template", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    public List<FtTemplateR2Rule> getTemplateRules() {
        return templateRules;
    }

    public void setTemplateRules(List<FtTemplateR2Rule> templateRules) {
        this.templateRules = templateRules;
    }

}
