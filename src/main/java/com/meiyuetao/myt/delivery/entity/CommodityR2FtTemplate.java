package com.meiyuetao.myt.delivery.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;

@Entity
@Table(name = "iyb_commodity_r2_ft_template")
@MetaData(value = "商品关联运费模板")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class CommodityR2FtTemplate extends MytBaseEntity {

    private static final long serialVersionUID = 1L;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("运费模板")
    private FreightTemplate freightTemplate;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "freight_template_sid")
    @JsonProperty
    public FreightTemplate getFreightTemplate() {
        return freightTemplate;
    }

    public void setFreightTemplate(FreightTemplate freightTemplate) {
        this.freightTemplate = freightTemplate;
    }

}
