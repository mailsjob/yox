package com.meiyuetao.myt.delivery.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Region;

@Entity
@Table(name = "iyb_ft_rule_r2_region")
@MetaData(value = "运费规则关联区域")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class FtRuleR2Region extends MytBaseEntity {
    private static final long serialVersionUID = 1L;

    @MetaData("运费规则")
    private FreightRule rule;

    @MetaData("区域")
    private Region region;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "freight_rule_sid", nullable = false)
    public FreightRule getRule() {
        return rule;
    }

    public void setRule(FreightRule rule) {
        this.rule = rule;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "region_sid", nullable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

}
