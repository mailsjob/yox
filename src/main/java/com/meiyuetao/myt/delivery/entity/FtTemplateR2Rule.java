package com.meiyuetao.myt.delivery.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@Entity
@Table(name = "iyb_ft_template_r2_rule")
@MetaData(value = "运费模板关联规则")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class FtTemplateR2Rule extends MytBaseEntity {

    private static final long serialVersionUID = 1L;

    @MetaData("运费模板")
    private FreightTemplate template;

    @MetaData("运费规则")
    private FreightRule rule;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "freight_template_sid", nullable = false)
    @JsonProperty
    public FreightTemplate getTemplate() {
        return template;
    }

    public void setTemplate(FreightTemplate template) {
        this.template = template;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "freight_rule_sid", nullable = false)
    @JsonProperty
    public FreightRule getRule() {
        return rule;
    }

    public void setRule(FreightRule rule) {
        this.rule = rule;
    }
}
