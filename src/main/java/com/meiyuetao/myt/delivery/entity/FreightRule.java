package com.meiyuetao.myt.delivery.entity;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("运费规则")
@Entity
@Table(name = "iyb_freight_rule")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class FreightRule extends MytBaseEntity {
    private static final long serialVersionUID = 1L;

    @MetaData("首重")
    private Integer startWeight = Integer.valueOf(0);

    @MetaData("首费")
    private BigDecimal startFee = BigDecimal.ZERO;

    @MetaData("续重")
    private Integer addWeight = Integer.valueOf(0);

    @MetaData("续费")
    private BigDecimal addFee = BigDecimal.ZERO;

    private Set<FtRuleR2Region> ftRuleR2Regions = new HashSet<FtRuleR2Region>();

    @MetaData("运送地")
    private String regions = "";

    @MetaData("运送地Ids")
    private String regionsId = null;

    @Column(name = "start_weight", nullable = false)
    @JsonProperty
    public Integer getStartWeight() {
        return startWeight;
    }

    public void setStartWeight(Integer startWeight) {
        this.startWeight = startWeight;
    }

    @Column(name = "start_fee", nullable = false)
    @JsonProperty
    public BigDecimal getStartFee() {
        return startFee;
    }

    public void setStartFee(BigDecimal startFee) {
        this.startFee = startFee;
    }

    @Column(name = "add_weight", nullable = false)
    @JsonProperty
    public Integer getAddWeight() {
        return addWeight;
    }

    public void setAddWeight(Integer addWeight) {
        this.addWeight = addWeight;
    }

    @Column(name = "add_fee", nullable = false)
    @JsonProperty
    public BigDecimal getAddFee() {
        return addFee;
    }

    public void setAddFee(BigDecimal addFee) {
        this.addFee = addFee;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "rule", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    public Set<FtRuleR2Region> getFtRuleR2Regions() {
        return ftRuleR2Regions;
    }

    public void setFtRuleR2Regions(Set<FtRuleR2Region> ftRuleR2Regions) {
        this.ftRuleR2Regions = ftRuleR2Regions;
    }

    @Transient
    @JsonProperty
    public String getRegions() {
        if ("".equals(regions)) {
            for (FtRuleR2Region ftRuRegion : this.ftRuleR2Regions) {
                regions = regions + ftRuRegion.getRegion().getName() + ",";
            }
            regions = regions.substring(0, regions.length() - 1);
        }
        return regions;
    }

    public void setRegions(String regions) {
        this.regions = regions;
    }

    @Transient
    @JsonProperty
    public String getRegionsId() {
        if (regionsId == null) {
            regionsId = "";
            for (FtRuleR2Region ftRuRegion : this.ftRuleR2Regions) {
                regionsId = regionsId + ftRuRegion.getRegion().getId() + ",";
            }
            regionsId = regionsId.substring(0, regionsId.length() - 1);
        }
        return regionsId;
    }

    public void setRegionsId(String regionsId) {
        this.regionsId = regionsId;
    }

}
