package com.meiyuetao.myt.delivery.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.delivery.dao.CommodityR2FtTemplateDao;
import com.meiyuetao.myt.delivery.entity.CommodityR2FtTemplate;

@Service
@Transactional
public class CommodityR2FtTemplateService extends BaseService<CommodityR2FtTemplate, Long> {

    @Autowired
    private CommodityR2FtTemplateDao commodityR2FtTemplateDao;

    @Override
    protected BaseDao<CommodityR2FtTemplate, Long> getEntityDao() {
        return commodityR2FtTemplateDao;
    }
}
