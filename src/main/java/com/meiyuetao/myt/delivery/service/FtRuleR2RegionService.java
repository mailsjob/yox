package com.meiyuetao.myt.delivery.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.delivery.dao.FtRuleR2RegionDao;
import com.meiyuetao.myt.delivery.entity.FtRuleR2Region;

@Service
@Transactional
public class FtRuleR2RegionService extends BaseService<FtRuleR2Region, Long> {

    @Autowired
    private FtRuleR2RegionDao ftRuleR2RegionDao;

    @Override
    protected BaseDao<FtRuleR2Region, Long> getEntityDao() {
        return ftRuleR2RegionDao;
    }
}
