package com.meiyuetao.myt.delivery.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.delivery.dao.FreightTemplateDao;
import com.meiyuetao.myt.delivery.entity.FreightTemplate;

@Service
@Transactional
public class FreightTemplateService extends BaseService<FreightTemplate, Long> {

    @Autowired
    private FreightTemplateDao freightTemplateDao;

    @Override
    protected BaseDao<FreightTemplate, Long> getEntityDao() {
        return freightTemplateDao;
    }
}
