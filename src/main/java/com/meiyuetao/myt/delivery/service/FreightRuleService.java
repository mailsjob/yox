package com.meiyuetao.myt.delivery.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.delivery.dao.FreightRuleDao;
import com.meiyuetao.myt.delivery.entity.FreightRule;

@Service
@Transactional
public class FreightRuleService extends BaseService<FreightRule, Long> {

    @Autowired
    private FreightRuleDao freightRuleDao;

    @Override
    protected BaseDao<FreightRule, Long> getEntityDao() {
        return freightRuleDao;
    }
}
