package com.meiyuetao.myt.delivery.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.delivery.dao.FtTemplateR2RuleDao;
import com.meiyuetao.myt.delivery.entity.FtTemplateR2Rule;

@Service
@Transactional
public class FtTemplateR2RuleService extends BaseService<FtTemplateR2Rule, Long> {

    @Autowired
    private FtTemplateR2RuleDao ftTemplateR2RuleDao;

    @Override
    protected BaseDao<FtTemplateR2Rule, Long> getEntityDao() {
        return ftTemplateR2RuleDao;
    }
}
