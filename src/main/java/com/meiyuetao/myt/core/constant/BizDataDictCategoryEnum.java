package com.meiyuetao.myt.core.constant;

import lab.s2jh.core.annotation.MetaData;

public enum BizDataDictCategoryEnum {
    @MetaData(value = "商品标签属性列表")
    IYOUBOX_COMMODITY_SYS_TAG, @MetaData(value = "盒子库存状态")
    IYOUBOX_BOX_STORE_STATUS, @MetaData(value = "CMS分类")
    IYOUBOX_CMS_CATEGORY, @MetaData(value = "盒子图片类型")
    IYOUBOX_BOX_PIC_TYPE, @MetaData(value = "盒子标签属性列表")
    IYOUBOX_BOX_SYS_TAG, @MetaData(value = "盒子套餐样式")
    IYOUBOX_COMBO_STYLE, @MetaData(value = "套餐系列分类")
    IYOUBOX_COMBO_SERIES_CATEGORY, @MetaData(value = "套餐月份分类")
    IYOUBOX_COMBO_MONTH_CATEGORY, @MetaData(value = "套餐大类")
    IYOUBOX_ROOT_CATEGORY, @MetaData(value = "客户来源类型")
    IYOUBOX_CUSTOMER_NAME_TYPE, @MetaData(value = "客户宝宝状态")
    IYOUBOX_CUSTOMER_BABY_STATUS, @MetaData(value = "物流公司列表")
    IYOUBOX_LOGISTICS_NAME_LIST, @MetaData(value = "优惠券类型列表")
    IYOUBOX_GIFT_PAPER_MODE, @MetaData(value = "订单发票类型")
    IYOUBOX_INVOICE_TYPE, @MetaData(value = "订单送货日期模式")
    IYOUBOX_ORDER_DELIVERY_DATE_MODE, @MetaData(value = "订单送货时间模式")
    IYOUBOX_ORDER_DELIVERY_TIME_MODE, @MetaData(value = "盒子风格")
    IYOUBOX_BOX_UI_STYLE, @MetaData(value = "商品/成品计量单位")
    IYOUBOX_STOCK_MEASURE_UNIT, @MetaData(value = "交易渠道")
    IYOUBOX_TRADE_TYPE, @MetaData(value = "采购订单类型")
    IYOUBOX_PURCHASE_ORDER_TYPE, @MetaData(value = "采购总价调差类型")
    IYOUBOX_PURCHASE_ORDER_ADD_AMOUNT_TYPE, @MetaData(value = "财务支付类型")
    IYOUBOX_FINANCE_PAY_MODE, @MetaData(value = "公司规模定义列表")
    INC_COMPANY_CAPACITY, @MetaData(value = "促销样式")
    IYOUBOX_PROMO_STYLE, @MetaData(value = "地区运费")
    IYB_REGION_FARE, @MetaData(value = "商品审核标签")
    IYB_COMMODITY_AUDIT_REASON, @MetaData(value = "公告显示范围")
    IYB_MASS_NOTICE_DISPLAY_SCOPE_TYPE, @MetaData(value = "商品标签")
    COMMODITY_LABEL;

}
