package com.meiyuetao.myt.core.constant;

import lab.s2jh.core.annotation.MetaData;

/**
 * “配置参数”枚举定义
 */
public enum ConfigPropertyCodeEnum {
    @MetaData(value = "管理员Email地址")
    ADMIN_EMAIL,

    @MetaData(value = "是否显示异常明细给用户")
    SHOW_EXCEPTION_DETAIL_TO_USER, @MetaData(value = "上传文件存储起始目录")
    FILE_SAVE_ROOT_PATH,

    @MetaData(value = "定时作业调度器以暂停状态初始化")
    SCHEDLUER_INIT_STANDBY_MODE,

    @MetaData(value = "Excel导出记录数上限")
    EXCEL_EXPORT_LIMIT_SIZE, @MetaData(value = "定时消息批处理数据记录数大小")
    JMS_MESSAGE_JOB_BATCH_SIZE,

    @MetaData(value = "全局控制是否启用短信发送功能")
    CTL_SEND_SMS, @MetaData(value = "全局控制是否启用邮件发送功能")
    CTL_SEND_EMAIL,

    @MetaData(value = "中文分词扩展词典")
    LUCENE_IKAnalyzer_EXT_DICS;

}
