package com.meiyuetao.myt.core.constant;

import lab.s2jh.core.annotation.MetaData;

@MetaData("库存模式")
public enum StorageModeEnum {
    @MetaData("北京库模式")
    MU, @MetaData("德国库模式")
    AUTO, @MetaData("代发模式")
    SA;
}
