package com.meiyuetao.myt.core.constant;

import lab.s2jh.core.annotation.MetaData;

/**
 * 通用的“性别”枚举定义
 */
public enum GenericSexEnum {
    @MetaData(value = "女")
    F,

    @MetaData(value = "男")
    M,

    @MetaData(value = "保密")
    U;
}
