package com.meiyuetao.myt.core.constant;

import lab.s2jh.core.annotation.MetaData;

@MetaData("消息/状态")
public enum MessageStateEnum {
    @MetaData(value = "成功")
    OK,

    @MetaData(value = "失败")
    FAIL;
}
