package com.meiyuetao.myt.wlf.service;

import java.util.Set;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;
import com.meiyuetao.myt.wlf.dao.WlfCreditParamDao;
import com.meiyuetao.myt.wlf.dao.WlfStudentInfoDao;
import com.meiyuetao.myt.wlf.entity.WlfSchoolInfo;
import com.meiyuetao.myt.wlf.entity.WlfStudentInfo;

@Service
@Transactional
public class WlfStudentInfoService extends BaseService<WlfStudentInfo, Long> {

    @Autowired
    private WlfStudentInfoDao wlfStudentInfoDao;

    @Autowired
    private WlfCreditParamDao wlfCreditParamDao;

    // private final static String TYPE_DEFAULT = "default";

    @Override
    protected BaseDao<WlfStudentInfo, Long> getEntityDao() {
        return wlfStudentInfoDao;
    }

    /**
     * 通过教育部接口查询学生学籍信息
     * 
     * @param idCard
     * @param trueName
     * @return
     */
    public Boolean checkWlfStudentInfoByAPI(WlfStudentInfo wlfStudentInfo) {
        String idCard = wlfStudentInfo.getIdCard();
        String mockIdCardStr = "225534199110159341," + "145634199408206088," + "128392199208214024," + "350208199508212235," + "333618199502277734," + "450236199408083197,"
                + "422563199310150875," + "354908199103246711," + "130516199103297496," + "502698199501213054," + "354655199105057247," + "510352199401182506,"
                + "456929199104219558," + "510942199207309490," + "356716199301082411," + "334825199307169284," + "644463199409196097," + "418188199309197197,"
                + "354758199407079788," + "343500199406227168," + "358750199402179591," + "223707199106244592," + "500494199407212580," + "612254199012080763,"
                + "131395199205115463," + "342004199302148493," + "615189199207206063," + "435550199204053153," + "622852199106108981," + "330017199112315617,"
                + "127852199107067002," + "631374199205180390," + "433396199203296747," + "372264199510036156," + "356552199102122760," + "376936199406179608,"
                + "310868199505083662," + "451359199508266749," + "621740199012104486," + "219036199405196741," + "646630199409235304," + "620437199210243339,"
                + "154113199510253876," + "457714199305310959," + "330784199301033971," + "310421199204026436," + "437084199412178961," + "139070199306205018,"
                + "368874199209278061," + "325510199306174526";

        Set<String> mockIdCards = Sets.newHashSet(mockIdCardStr.split(","));
        // String trueName = wlfStudentInfo.getTrueName();
        if (mockIdCards.contains(idCard)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 计算基本授信额度
     * 
     * @param stu
     * @return
     */
    public Integer calcBaseCredit(WlfStudentInfo stu) {
        Integer credit = 0;
        WlfSchoolInfo schoolInfo = stu.getSchoolInfo();

        if (schoolInfo.getType() >= 985) {
            credit = Double.valueOf(800 * 12 * new Float(stu.getDegreeCategory()) * 0.5).intValue();
        } else if (schoolInfo.getType() >= 211) {
            credit = Double.valueOf(500 * 12 * new Float(stu.getDegreeCategory()) * 0.5).intValue();
        } else {
            credit = Double.valueOf(300 * 12 * new Float(stu.getDegreeCategory()) * 0.5).intValue();
        }

        if (!stu.getDegreeMode().equalsIgnoreCase("TZ")) {
            credit = Double.valueOf(credit * 0.8).intValue();
        }

        // WlfMajorInfo majorInfo = stu.getMajorInfo();
        // //学校类型授信额度
        // WlfCreditParam creditSchoolTypeDefault =
        // wlfCreditParamDao.findByCreditPrimaryTypeAndPrimaryCode(
        // CreditPrimaryTypeEnum.SCHOOL_TYPE, TYPE_DEFAULT);
        // if (creditSchoolTypeDefault != null) {
        // credit += creditSchoolTypeDefault.getCreditValue();
        // }
        // if (schoolInfo.getType211()) {
        // WlfCreditParam wlfCreditParam =
        // wlfCreditParamDao.findByCreditPrimaryTypeAndPrimaryCode(
        // CreditPrimaryTypeEnum.SCHOOL_TYPE, "211");
        // if (wlfCreditParam != null) {
        // credit += wlfCreditParam.getCreditValue();
        // }
        // }
        // if (schoolInfo.getType985()) {
        // WlfCreditParam wlfCreditParam =
        // wlfCreditParamDao.findByCreditPrimaryTypeAndPrimaryCode(
        // CreditPrimaryTypeEnum.SCHOOL_TYPE, "985");
        // if (wlfCreditParam != null) {
        // credit += wlfCreditParam.getCreditValue();
        // }
        // }
        //
        // //学校授信额度
        // WlfCreditParam creditSchoolCodeDefault =
        // wlfCreditParamDao.findByCreditPrimaryTypeAndPrimaryCode(
        // CreditPrimaryTypeEnum.SCHOOL_CODE, TYPE_DEFAULT);
        // if (creditSchoolCodeDefault != null) {
        // credit += creditSchoolCodeDefault.getCreditValue();
        // }
        // WlfCreditParam creditSchoolCode =
        // wlfCreditParamDao.findByCreditPrimaryTypeAndPrimaryCode(
        // CreditPrimaryTypeEnum.SCHOOL_CODE, schoolInfo.getCode());
        // if (creditSchoolCode != null) {
        // credit += creditSchoolCode.getCreditValue();
        // }
        //
        // //专业类型授信额度
        // WlfCreditParam creditMajorCodeDefault =
        // wlfCreditParamDao.findByCreditPrimaryTypeAndPrimaryCode(
        // CreditPrimaryTypeEnum.MAJOR_CODE, TYPE_DEFAULT);
        // if (creditMajorCodeDefault != null) {
        // credit += creditMajorCodeDefault.getCreditValue();
        // }
        // WlfCreditParam creditMajorCode =
        // wlfCreditParamDao.findByCreditPrimaryTypeAndPrimaryCode(
        // CreditPrimaryTypeEnum.MAJOR_CODE, majorInfo.getCode());
        // if (creditMajorCode != null) {
        // credit += creditMajorCode.getCreditValue();
        // }

        return credit;
    }
}
