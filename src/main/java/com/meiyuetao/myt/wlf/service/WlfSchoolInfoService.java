package com.meiyuetao.myt.wlf.service;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.wlf.dao.WlfSchoolInfoDao;
import com.meiyuetao.myt.wlf.entity.WlfSchoolInfo;

@Service
@Transactional
public class WlfSchoolInfoService extends BaseService<WlfSchoolInfo, Long> {

    @Autowired
    private WlfSchoolInfoDao wlfSchoolInfoDao;

    @Override
    protected BaseDao<WlfSchoolInfo, Long> getEntityDao() {
        return wlfSchoolInfoDao;
    }

    public List<WlfSchoolInfo> findAll() {
        return wlfSchoolInfoDao.findAllCached();
    }
}
