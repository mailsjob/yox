package com.meiyuetao.myt.wlf.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.wlf.dao.WlfSDMInfoDao;
import com.meiyuetao.myt.wlf.entity.WlfSDMInfo;

@Service
@Transactional
public class WlfSDMInfoService extends BaseService<WlfSDMInfo, Long> {

    @Autowired
    private WlfSDMInfoDao wlfSDMInfoDao;

    @Override
    protected BaseDao<WlfSDMInfo, Long> getEntityDao() {
        return wlfSDMInfoDao;
    }

}
