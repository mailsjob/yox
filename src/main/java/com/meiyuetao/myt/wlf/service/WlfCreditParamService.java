package com.meiyuetao.myt.wlf.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.wlf.dao.WlfCreditParamDao;
import com.meiyuetao.myt.wlf.entity.WlfCreditParam;

@Service
@Transactional
public class WlfCreditParamService extends BaseService<WlfCreditParam, Long> {

    @Autowired
    private WlfCreditParamDao wlfCreditParamDao;

    @Override
    protected BaseDao<WlfCreditParam, Long> getEntityDao() {
        return wlfCreditParamDao;
    }
}
