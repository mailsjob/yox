package com.meiyuetao.myt.wlf.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.wlf.dao.WlfCommodityR2PartTimeDao;
import com.meiyuetao.myt.wlf.entity.WlfCommodityR2PartTime;

@Service
@Transactional
public class WlfCommodityR2PartTimeService extends BaseService<WlfCommodityR2PartTime, Long> {

    @Autowired
    private WlfCommodityR2PartTimeDao wlfCommodityR2PartTimeDao;

    @Override
    protected BaseDao<WlfCommodityR2PartTime, Long> getEntityDao() {
        return wlfCommodityR2PartTimeDao;
    }
}
