package com.meiyuetao.myt.wlf.service;

import java.util.Date;
import java.util.UUID;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.ctx.DynamicConfigService;
import lab.s2jh.ctx.FreemarkerService;
import lab.s2jh.ctx.MailService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.core.service.SolrService;
import com.meiyuetao.myt.customer.dao.SsoUserDao;
import com.meiyuetao.myt.customer.entity.SsoUser;
import com.meiyuetao.myt.customer.service.CustomerProfileService;
import com.meiyuetao.myt.customer.service.SmsDetailLogService;
import com.meiyuetao.myt.customer.service.SsoUserService;
import com.meiyuetao.myt.job.BusinessNotifyService;
import com.meiyuetao.myt.wlf.dao.WlfUserDao;
import com.meiyuetao.myt.wlf.entity.WlfUser;
import com.meiyuetao.myt.wlf.entity.WlfUser.AuditStatusEnum;
import com.meiyuetao.myt.yryd.service.YrydStoreService;

@Service
@Transactional
public class WlfUserService extends BaseService<WlfUser, Long> {

    @Autowired
    private WlfUserDao wlfUserDao;

    @Autowired
    private SsoUserDao ssoUserDao;

    @Autowired
    private BusinessNotifyService businessNotifyService;

    @Autowired
    private DynamicConfigService dynamicConfigService;
    @Autowired
    private SmsDetailLogService smsDetailLogService;
    @Autowired
    private CustomerProfileService customerProfileService;
    @Autowired
    private SsoUserService ssoUserService;
    @Autowired
    private YrydStoreService yrydStoreService;
    @Autowired
    private SolrService solrService;
    @Autowired
    private FreemarkerService freemarkerService;
    @Autowired
    private MailService mailService;
    @SuppressWarnings("deprecation")
    @Autowired
    private PasswordEncoder passwordEncoder;

    // private static int MAX_TRY = 3;

    @Override
    protected BaseDao<WlfUser, Long> getEntityDao() {
        return wlfUserDao;
    }

    @Override
    public WlfUser save(WlfUser entity) {
        WlfUser wlfUser = null;
        if (entity.isNew()) {
            SsoUser ssoUser = new SsoUser();
            ssoUser.setProviderUid(UUID.randomUUID().toString());
            ssoUser.setProviderType("WEILAIFU");
            ssoUser.setRealName(entity.getTrueName());
            ssoUser.setLoginid(entity.getLoginid());
            // ssoUser.setEmail(entity.getEmail());
            ssoUser.setMobilePhone(entity.getMobilePhone());
            ssoUser.setRegisterTime(new Date());
            @SuppressWarnings("deprecation")
            String encodePwd = passwordEncoder.encodePassword(entity.getPassword(), ssoUser.getLoginid());
            ssoUser.setPassword(encodePwd);
            // 清空密码，避免明文密码存储
            entity.setPassword(null);
            ssoUserDao.save(ssoUser);
            wlfUser = super.save(entity);

            String content = dynamicConfigService.getString("cfg.sms.wlf.create", "");
            if (StringUtils.isNotBlank(content) && StringUtils.isNotBlank(entity.getMobilePhone())) {
                sendMsg(content, entity.getMobilePhone());
            }
        } else {
            wlfUser = super.save(entity);
        }

        return wlfUser;
    }

    @Async
    public void sendMsg(String mobilePhone, String content) {
        businessNotifyService.sendMsg(mobilePhone, content);
    }

    public void doAudit(WlfUser entity) {
        /*
         * SsoUser ssoUser = ssoUserService.findByProperty("loginid",
         * entity.getLoginid()); GroupPropertyFilter groupPropertyFilter =
         * GroupPropertyFilter.buildDefaultAndGroupFilter();
         * groupPropertyFilter.append(new PropertyFilter(MatchType.EQ,
         * "customerName", ssoUser.getProviderUid()));
         * groupPropertyFilter.append(new PropertyFilter(MatchType.EQ,
         * "customerNameType", ssoUser.getProviderType()));
         * List<CustomerProfile> customerProfiles =
         * customerProfileService.findByFilters(groupPropertyFilter);
         * Assert.isTrue(customerProfiles.size() == 1); CustomerProfile
         * customerProfile = null; if (customerProfiles.size() == 1) {
         * customerProfile = customerProfiles.get(0); } BigDecimal
         * weilaifuMaxAccount = entity.getWeilaifuMaxAccount(); if
         * (entity.getAuditTime() == null) {
         * entity.setWeilaifuAccount(weilaifuMaxAccount); if (customerProfile !=
         * null) { customerProfile.setWeilaifuMaxAccount(weilaifuMaxAccount);
         * customerProfile.setWeilaifuAccount(weilaifuMaxAccount);
         * customerProfileService.save(customerProfile); } }
         */
        entity.setAuditTime(new Date());
        entity.setAuditStatus(AuditStatusEnum.A30PASSED);
        wlfUserDao.save(entity);
        /*
         * YrydStore yrydStore = entity.getYrydStore();
         * yrydStore.setStoreStatus(StoreStatusEnum.NORMAL);
         * yrydStoreService.save(yrydStore);
         * 
         * Map<String, Object> dataMap = new HashMap<String, Object>();
         * dataMap.put("wlfUser", entity); String emailContent =
         * freemarkerService.processTemplateByFileName("WLF_USER_AUDIT_EMAIL",
         * dataMap); String email = entity.getEmail(); if
         * (StringUtils.isNotBlank(email)) { mailService.sendHtmlMail("店铺申请",
         * emailContent, false, email); }
         */
        String content = dynamicConfigService.getString("cfg.sms.wlf.audit", "");
        if (StringUtils.isNotBlank(content) && StringUtils.isNotBlank(entity.getMobilePhone())) {
            sendMsg(entity.getMobilePhone(), content);
        }
    }

}
