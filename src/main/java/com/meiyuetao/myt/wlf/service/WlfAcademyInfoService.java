package com.meiyuetao.myt.wlf.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.wlf.dao.WlfAcademyInfoDao;
import com.meiyuetao.myt.wlf.entity.WlfAcademyInfo;

@Service
@Transactional
public class WlfAcademyInfoService extends BaseService<WlfAcademyInfo, Long> {

    @Autowired
    private WlfAcademyInfoDao wlfAcademyInfoDao;

    @Override
    protected BaseDao<WlfAcademyInfo, Long> getEntityDao() {
        return wlfAcademyInfoDao;
    }

}
