package com.meiyuetao.myt.wlf.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.wlf.dao.WlfMajorInfoDao;
import com.meiyuetao.myt.wlf.entity.WlfMajorInfo;

@Service
@Transactional
public class WlfMajorInfoService extends BaseService<WlfMajorInfo, Long> {

    @Autowired
    private WlfMajorInfoDao wlfMajorInfoDao;

    @Override
    protected BaseDao<WlfMajorInfo, Long> getEntityDao() {
        return wlfMajorInfoDao;
    }

}
