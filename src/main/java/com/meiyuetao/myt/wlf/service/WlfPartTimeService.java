package com.meiyuetao.myt.wlf.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.wlf.dao.WlfPartTimeDao;
import com.meiyuetao.myt.wlf.entity.WlfPartTime;

@Service
@Transactional
public class WlfPartTimeService extends BaseService<WlfPartTime, Long> {

    @Autowired
    private WlfPartTimeDao wlfPartTimeDao;

    @Override
    protected BaseDao<WlfPartTime, Long> getEntityDao() {
        return wlfPartTimeDao;
    }
}
