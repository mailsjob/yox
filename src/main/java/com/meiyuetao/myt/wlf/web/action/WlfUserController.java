package com.meiyuetao.myt.wlf.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.wlf.entity.WlfUser;
import com.meiyuetao.myt.wlf.entity.WlfUser.AuditStatusEnum;
import com.meiyuetao.myt.wlf.service.WlfUserService;

@MetaData("未来付注册用户管理")
public class WlfUserController extends MytBaseController<WlfUser, Long> {

    @Autowired
    private WlfUserService wlfUserService;

    @Override
    protected BaseService<WlfUser, Long> getEntityService() {
        return wlfUserService;
    }

    @Override
    protected void checkEntityAclPermission(WlfUser entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    public HttpHeaders doAudit() {
        List<WlfUser> wlfUsers = null;
        Long[] ids = this.getSelectSids();
        if (ids != null && ids.length > 0) {
            wlfUsers = wlfUserService.findAll(ids);
        } else {
            GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
            wlfUsers = wlfUserService.findByFilters(groupPropertyFilter);
        }
        int i = 0;
        for (WlfUser wlfUser : wlfUsers) {
            if (wlfUser.getAuditStatus() == null || AuditStatusEnum.A10UNSUBMIT.equals(wlfUser.getAuditStatus())) {
                wlfUser.setAuditUser(getLogonUser());
                wlfUserService.doAudit(wlfUser);
                i++;
            }
        }
        setModel(OperationResult.buildSuccessResult("完成审核数据：" + i + "条"));
        return buildDefaultHttpHeaders();
    }

}