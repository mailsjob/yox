package com.meiyuetao.myt.wlf.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.wlf.entity.WlfCommodityR2PartTime;
import com.meiyuetao.myt.wlf.service.WlfCommodityR2PartTimeService;

@MetaData("商品关联兼职信息管理")
public class WlfCommodityR2PartTimeController extends BaseController<WlfCommodityR2PartTime, Long> {

    @Autowired
    private WlfCommodityR2PartTimeService wlfCommodityR2PartTimeService;

    @Override
    protected BaseService<WlfCommodityR2PartTime, Long> getEntityService() {
        return wlfCommodityR2PartTimeService;
    }

    @Override
    protected void checkEntityAclPermission(WlfCommodityR2PartTime entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}