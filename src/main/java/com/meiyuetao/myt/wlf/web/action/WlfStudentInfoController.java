package com.meiyuetao.myt.wlf.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.wlf.entity.WlfUser;
import com.meiyuetao.myt.wlf.service.WlfUserService;

@MetaData("未来付注册用户管理")
public class WlfStudentInfoController extends MytBaseController<WlfUser, Long> {

    @Autowired
    private WlfUserService wlfUserService;

    @Override
    protected BaseService<WlfUser, Long> getEntityService() {
        return wlfUserService;
    }

    @Override
    protected void checkEntityAclPermission(WlfUser entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    public HttpHeaders doAudit() {
        bindingEntity.setAuditUser(getLogonUser());
        wlfUserService.doAudit(bindingEntity);
        setModel(OperationResult.buildSuccessResult("审核完成"));
        return buildDefaultHttpHeaders();
    }

}