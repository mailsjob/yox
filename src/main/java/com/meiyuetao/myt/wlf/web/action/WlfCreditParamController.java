package com.meiyuetao.myt.wlf.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.wlf.entity.WlfCreditParam;
import com.meiyuetao.myt.wlf.service.WlfCreditParamService;

@MetaData("授信配置管理")
public class WlfCreditParamController extends BaseController<WlfCreditParam, Long> {

    @Autowired
    private WlfCreditParamService wlfCreditParamService;

    @Override
    protected BaseService<WlfCreditParam, Long> getEntityService() {
        return wlfCreditParamService;
    }

    @Override
    protected void checkEntityAclPermission(WlfCreditParam entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}