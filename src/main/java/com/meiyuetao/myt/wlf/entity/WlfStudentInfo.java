package com.meiyuetao.myt.wlf.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("未来付学生学籍信息")
@Entity
@Table(name = "wlf_student_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WlfStudentInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("学校信息")
    private WlfSchoolInfo schoolInfo;
    @MetaData("学院信息")
    private WlfAcademyInfo academyInfo;
    @MetaData("专业信息")
    private WlfMajorInfo majorInfo;
    @MetaData("学生号")
    private String studentNumber;
    @MetaData("学院")
    private String departName;
    @MetaData("教育层次")
    private String eduLevel;
    @MetaData("班级")
    private String classes;
    @MetaData("年纪")
    private String grade;
    @MetaData("入学时间")
    private Date enterTime;
    @MetaData("离校时间")
    private Date leaveTime;
    @MetaData("真实姓名")
    private String trueName;
    @MetaData("照片")
    private String photo;
    @MetaData("身份证号")
    private String idCard;
    @MetaData("性别")
    private String sex;
    @MetaData("出生日期")
    private Date bornTime;
    @MetaData("学制")
    private String degreeCategory;
    @MetaData("学历类型")
    private String degreeMode;
    @MetaData("辅助属性，存储基于学籍信息计算的授信额度 ")
    private Integer credit;

    @JsonProperty
    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }

    @JsonProperty
    public String getEduLevel() {
        return eduLevel;
    }

    public void setEduLevel(String eduLevel) {
        this.eduLevel = eduLevel;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @JsonProperty
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(Date enterTime) {
        this.enterTime = enterTime;
    }

    public Date getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(Date leaveTime) {
        this.leaveTime = leaveTime;
    }

    @JsonProperty
    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @JsonProperty
    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    @JsonProperty
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @JsonProperty
    public Date getBornTime() {
        return bornTime;
    }

    public void setBornTime(Date bornTime) {
        this.bornTime = bornTime;
    }

    @JsonProperty
    public String getDegreeCategory() {
        return degreeCategory;
    }

    public void setDegreeCategory(String degreeCategory) {
        this.degreeCategory = degreeCategory;
    }

    @JsonProperty
    public String getDegreeMode() {
        return degreeMode;
    }

    public void setDegreeMode(String degreeMode) {
        this.degreeMode = degreeMode;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "school_info_sid")
    @JsonProperty
    public WlfSchoolInfo getSchoolInfo() {
        return schoolInfo;
    }

    public void setSchoolInfo(WlfSchoolInfo schoolInfo) {
        this.schoolInfo = schoolInfo;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "major_info_sid")
    @JsonProperty
    public WlfMajorInfo getMajorInfo() {
        return majorInfo;
    }

    public void setMajorInfo(WlfMajorInfo majorInfo) {
        this.majorInfo = majorInfo;
    }

    @Transient
    @JsonProperty
    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "academy_info_sid")
    @JsonProperty
    public WlfAcademyInfo getAcademyInfo() {
        return academyInfo;
    }

    public void setAcademyInfo(WlfAcademyInfo academyInfo) {
        this.academyInfo = academyInfo;
    }

}
