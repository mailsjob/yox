package com.meiyuetao.myt.wlf.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("授信配置")
@Entity
@Table(name = "wlf_credit_param", uniqueConstraints = @UniqueConstraint(columnNames = { "creditPrimaryType", "primaryCode" }))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WlfCreditParam extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("授信额度的主依据的类型")
    private CreditPrimaryTypeEnum creditPrimaryType;
    @MetaData("授信额度的主依据的代码")
    private String primaryCode;
    @MetaData("授信额度的次依据的代码")
    private String secondaryCode;
    @MetaData("授信值")
    private Integer creditValue;

    public static enum CreditPrimaryTypeEnum {
        @MetaData(value = "学校类型", comments = "985,211")
        SCHOOL_TYPE,

        @MetaData("学校代码")
        SCHOOL_CODE,

        @MetaData("专业代码")
        MAJOR_CODE;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public CreditPrimaryTypeEnum getCreditPrimaryType() {
        return creditPrimaryType;
    }

    public void setCreditPrimaryType(CreditPrimaryTypeEnum creditPrimaryType) {
        this.creditPrimaryType = creditPrimaryType;
    }

    @Column(nullable = false)
    @JsonProperty
    public String getPrimaryCode() {
        return primaryCode;
    }

    public void setPrimaryCode(String primaryCode) {
        this.primaryCode = primaryCode;
    }

    public String getSecondaryCode() {
        return secondaryCode;
    }

    public void setSecondaryCode(String secondaryCode) {
        this.secondaryCode = secondaryCode;
    }

    @Column(nullable = false)
    @JsonProperty
    public Integer getCreditValue() {
        return creditValue;
    }

    public void setCreditValue(Integer creditValue) {
        this.creditValue = creditValue;
    }
}
