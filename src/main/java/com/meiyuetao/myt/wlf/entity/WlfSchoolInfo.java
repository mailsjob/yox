package com.meiyuetao.myt.wlf.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Region;

@MetaData("未来付学校信息")
@Entity
@Table(name = "wlf_school_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WlfSchoolInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("学校名称")
    private String name;
    @MetaData("学校代码")
    private String code;
    private List<WlfSDMInfo> wlfSDMInfos = new ArrayList<WlfSDMInfo>();

    @MetaData(value = "地区")
    private Region region;
    @MetaData("类型")
    private Integer type = 0;

    @Column(nullable = false)
    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false, unique = true)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(nullable = false)
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @OneToMany(mappedBy = "school", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    public List<WlfSDMInfo> getWlfSDMInfos() {
        return wlfSDMInfos;
    }

    public void setWlfSDMInfos(List<WlfSDMInfo> wlfSDMInfos) {
        this.wlfSDMInfos = wlfSDMInfos;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "region_sid", nullable = false)
    @NotAudited
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

}
