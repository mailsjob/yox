package com.meiyuetao.myt.wlf.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("用户兼职信息表")
@Table(name = "wlf_part_time")
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WlfPartTime extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("图片")
    private String pic;
    @MetaData("链接")
    private String url;
    @MetaData("标题")
    private String title;
    @MetaData("单价")
    private int unitPrice;
    @MetaData("单位")
    private String unit;

    // private List<WlfCommodityR2PartTime> wlfCommodityR2PartTimes = new
    // ArrayList<WlfCommodityR2PartTime>();

    @Column(nullable = false, length = 128)
    @JsonProperty
    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @Column(nullable = false, length = 500)
    @JsonProperty
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(nullable = false, length = 500)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(nullable = true, length = 18)
    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Column(nullable = true, length = 50)
    @JsonProperty
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    /*
     * @OneToMany(mappedBy = "partTime", cascade = CascadeType.ALL,
     * orphanRemoval = true)
     * 
     * @NotAudited public List<WlfCommodityR2PartTime>
     * getWlfCommodityR2PartTimes() { return wlfCommodityR2PartTimes; } public
     * void setWlfCommodityR2PartTimes( List<WlfCommodityR2PartTime>
     * wlfCommodityR2PartTimes) { this.wlfCommodityR2PartTimes =
     * wlfCommodityR2PartTimes; }
     */

    @Override
    @Transient
    @JsonProperty
    public String getDisplay() {
        return title;
    }
}
