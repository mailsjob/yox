package com.meiyuetao.myt.wlf.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("未来付专业信息")
@Entity
@Table(name = "wlf_major_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WlfMajorInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("专业名称")
    private String name;
    @MetaData("专业代码")
    private String code;

    @Column(nullable = false)
    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false, unique = true)
    @JsonProperty
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
