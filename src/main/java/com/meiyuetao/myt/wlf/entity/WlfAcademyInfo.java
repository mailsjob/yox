package com.meiyuetao.myt.wlf.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("院系信息")
@Entity
@Table(name = "wlf_academy_info")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WlfAcademyInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("院系名")
    private String name;
    private List<WlfSDMInfo> wlfSDMInfos = new ArrayList<WlfSDMInfo>();

    @Column(nullable = false)
    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "academy", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    public List<WlfSDMInfo> getWlfSDMInfos() {
        return wlfSDMInfos;
    }

    public void setWlfSDMInfos(List<WlfSDMInfo> wlfSDMInfos) {
        this.wlfSDMInfos = wlfSDMInfos;
    }

}
