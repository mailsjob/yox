package com.meiyuetao.myt.wlf.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;

@MetaData("商品关联兼职信息")
@Table(name = "wlf_commodity_r2_part_time")
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WlfCommodityR2PartTime extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("兼职信息表")
    private WlfPartTime partTime;

    @MetaData("商品信息表")
    private Commodity commodity;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "part_time_sid", nullable = false)
    @JsonProperty
    public WlfPartTime getPartTime() {
        return partTime;
    }

    public void setPartTime(WlfPartTime partTime) {
        this.partTime = partTime;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid", nullable = false)
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

}
