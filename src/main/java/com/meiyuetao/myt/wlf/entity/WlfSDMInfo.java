package com.meiyuetao.myt.wlf.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("学校，院系，专业级联对象")
@Entity
@Table(name = "wlf_academy_r2_major")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WlfSDMInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("学校")
    private WlfSchoolInfo school;
    @MetaData("院系")
    private WlfAcademyInfo academy;
    @MetaData("专业")
    private WlfMajorInfo major;

    @ManyToOne
    @JoinColumn(name = "school_sid", nullable = false)
    public WlfSchoolInfo getSchool() {
        return school;
    }

    public void setSchool(WlfSchoolInfo school) {
        this.school = school;
    }

    @ManyToOne
    @JoinColumn(name = "major_sid", nullable = false)
    public WlfMajorInfo getMajor() {
        return major;
    }

    public void setMajor(WlfMajorInfo major) {
        this.major = major;
    }

    @ManyToOne
    @JoinColumn(name = "academy_sid", nullable = false)
    public WlfAcademyInfo getAcademy() {
        return academy;
    }

    public void setAcademy(WlfAcademyInfo academy) {
        this.academy = academy;
    }

}
