package com.meiyuetao.myt.wlf.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.wlf.entity.WlfPartTime;

@Repository
public interface WlfPartTimeDao extends BaseDao<WlfPartTime, Long> {

}