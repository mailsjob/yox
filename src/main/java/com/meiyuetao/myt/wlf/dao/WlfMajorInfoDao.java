package com.meiyuetao.myt.wlf.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.wlf.entity.WlfMajorInfo;

@Repository
public interface WlfMajorInfoDao extends BaseDao<WlfMajorInfo, Long> {

}