package com.meiyuetao.myt.wlf.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.wlf.entity.WlfCreditParam;
import com.meiyuetao.myt.wlf.entity.WlfCreditParam.CreditPrimaryTypeEnum;

@Repository
public interface WlfCreditParamDao extends BaseDao<WlfCreditParam, Long> {

    WlfCreditParam findByCreditPrimaryTypeAndPrimaryCode(CreditPrimaryTypeEnum creditprimarytype, String primaryCode);
}