package com.meiyuetao.myt.wlf.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.wlf.entity.WlfSchoolInfo;

@Repository
public interface WlfSchoolInfoDao extends BaseDao<WlfSchoolInfo, Long> {

    @Query("from WlfSchoolInfo order by code asc")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    List<WlfSchoolInfo> findAllCached();
}