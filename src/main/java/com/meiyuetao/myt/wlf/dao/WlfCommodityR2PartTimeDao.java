package com.meiyuetao.myt.wlf.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.wlf.entity.WlfCommodityR2PartTime;

@Repository
public interface WlfCommodityR2PartTimeDao extends BaseDao<WlfCommodityR2PartTime, Long> {

}