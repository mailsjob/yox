package com.meiyuetao.myt.vip.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.vip.dao.VipBakMoneyHistoryDao;
import com.meiyuetao.myt.vip.entity.VipBakMoneyHistory;

@Service
@Transactional
public class VipBakMoneyHistoryService extends BaseService<VipBakMoneyHistory, Long> {

    @Autowired
    private VipBakMoneyHistoryDao vipBakMoneyHistoryDao;

    @Override
    protected BaseDao<VipBakMoneyHistory, Long> getEntityDao() {
        return vipBakMoneyHistoryDao;
    }
}
