package com.meiyuetao.myt.vip.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.vip.dao.VipScoreHistoryDao;
import com.meiyuetao.myt.vip.entity.VipScoreHistory;

@Service
@Transactional
public class VipScoreHistoryService extends BaseService<VipScoreHistory, Long> {

    @Autowired
    private VipScoreHistoryDao vipScoreHistoryDao;

    @Override
    protected BaseDao<VipScoreHistory, Long> getEntityDao() {
        return vipScoreHistoryDao;
    }
}
