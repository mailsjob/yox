package com.meiyuetao.myt.vip.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import com.meiyuetao.myt.vip.entity.VipWxQrcodeBack;
import com.meiyuetao.myt.vip.dao.VipWxQrcodeBackDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class VipWxQrcodeBackService extends BaseService<VipWxQrcodeBack,Long>{
    
    @Autowired
    private VipWxQrcodeBackDao vipWxQrcodeBackDao;

    @Override
    protected BaseDao<VipWxQrcodeBack, Long> getEntityDao() {
        return vipWxQrcodeBackDao;
    }
}
