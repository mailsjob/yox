package com.meiyuetao.myt.vip.service;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.vip.dao.VipLevelDao;
import com.meiyuetao.myt.vip.entity.VipLevel;

@Service
@Transactional
public class VipLevelService extends BaseService<VipLevel, Long> {

    @Autowired
    private VipLevelDao vipLevelDao;

    @Override
    protected BaseDao<VipLevel, Long> getEntityDao() {
        return vipLevelDao;
    }

    public List<VipLevel> findAllCached() {
        return vipLevelDao.findAllCached();
    }
}
