package com.meiyuetao.myt.vip.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.vip.dao.VipBakMoneyHistoryDetailDao;
import com.meiyuetao.myt.vip.entity.VipBakMoneyHistoryDetail;

@Service
@Transactional
public class VipBakMoneyHistoryDetailService extends BaseService<VipBakMoneyHistoryDetail, Long> {

    @Autowired
    private VipBakMoneyHistoryDetailDao vipBakMoneyHistoryDetailDao;

    @Override
    protected BaseDao<VipBakMoneyHistoryDetail, Long> getEntityDao() {
        return vipBakMoneyHistoryDetailDao;
    }
}
