package com.meiyuetao.myt.vip.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.vip.dao.VipWxQrcodeDao;
import com.meiyuetao.myt.vip.entity.VipWxQrcode;

@Service
@Transactional
public class VipWxQrcodeService extends BaseService<VipWxQrcode, Long> {

    @Autowired
    private VipWxQrcodeDao vipWxQrcodeDao;

    @Override
    protected BaseDao<VipWxQrcode, Long> getEntityDao() {
        return vipWxQrcodeDao;
    }

}
