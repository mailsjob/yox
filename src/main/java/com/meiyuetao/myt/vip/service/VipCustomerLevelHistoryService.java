package com.meiyuetao.myt.vip.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.vip.dao.VipCustomerLevelHistoryDao;
import com.meiyuetao.myt.vip.entity.VipCustomerLevelHistory;

@Service
@Transactional
public class VipCustomerLevelHistoryService extends BaseService<VipCustomerLevelHistory, Long> {

    @Autowired
    private VipCustomerLevelHistoryDao vipCustomerLevelHistoryDao;

    @Override
    protected BaseDao<VipCustomerLevelHistory, Long> getEntityDao() {
        return vipCustomerLevelHistoryDao;
    }

}
