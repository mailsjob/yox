package com.meiyuetao.myt.vip.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import com.meiyuetao.myt.vip.entity.VipWxQrcodeHistory;
import com.meiyuetao.myt.vip.dao.VipWxQrcodeHistoryDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class VipWxQrcodeHistoryService extends BaseService<VipWxQrcodeHistory,Long>{
    
    @Autowired
    private VipWxQrcodeHistoryDao vipWxQrcodeHistoryDao;

    @Override
    protected BaseDao<VipWxQrcodeHistory, Long> getEntityDao() {
        return vipWxQrcodeHistoryDao;
    }
}
