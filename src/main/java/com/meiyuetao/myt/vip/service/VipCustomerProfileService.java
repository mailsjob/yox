package com.meiyuetao.myt.vip.service;

import com.meiyuetao.myt.customer.dao.CustomerProfileDao;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.vip.dao.VipCustomerProfileDao;
import com.meiyuetao.myt.vip.dao.VipLevelDao;
import com.meiyuetao.myt.vip.entity.VipCustomerProfile;
import com.meiyuetao.myt.vip.entity.VipLevel;
import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class VipCustomerProfileService extends BaseService<VipCustomerProfile, Long> {

    @Autowired
    private CustomerProfileDao customerProfileDao;
    @Autowired
    private VipCustomerProfileDao vipCustomerProfileDao;
    @Autowired
    private VipLevelDao vipLevelDao;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected BaseDao<VipCustomerProfile, Long> getEntityDao() {
        return vipCustomerProfileDao;
    }

    @Override
    public VipCustomerProfile save(VipCustomerProfile entity) {
        VipLevel dbVipLevel = vipLevelDao.findOne(entity.getVipLevel().getId());
        entity.setVipLevelName(dbVipLevel.getName());
        CustomerProfile customerProfile = customerProfileDao.findOne(entity.getCustomerProfile().getId());
        entity.setCustomerProfileSid(customerProfile);
        return super.save(entity);
    }

    public List<VipCustomerProfile> findByUpstream(Long upstream) {
        return vipCustomerProfileDao.findByUpstream(vipCustomerProfileDao.findOne(upstream));
    }

    public List<Long> findForUpstream(Long upstream) {
        return vipCustomerProfileDao.findForUpstream(vipCustomerProfileDao.findOne(upstream));
    }

}
