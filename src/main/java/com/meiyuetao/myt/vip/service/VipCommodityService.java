package com.meiyuetao.myt.vip.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.vip.dao.VipCommodityDao;
import com.meiyuetao.myt.vip.entity.VipCommodity;

@Service
@Transactional
public class VipCommodityService extends BaseService<VipCommodity, Long> {

    @Autowired
    private VipCommodityDao vipCommodityDao;

    @Override
    protected BaseDao<VipCommodity, Long> getEntityDao() {
        return vipCommodityDao;
    }
}
