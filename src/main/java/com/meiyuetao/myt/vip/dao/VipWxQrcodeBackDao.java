package com.meiyuetao.myt.vip.dao;

import com.meiyuetao.myt.vip.entity.VipWxQrcodeBack;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface VipWxQrcodeBackDao extends BaseDao<VipWxQrcodeBack, Long> {

}