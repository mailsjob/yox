package com.meiyuetao.myt.vip.dao;

import java.util.List;

import javax.persistence.QueryHint;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.vip.entity.VipLevel;

@Repository
public interface VipLevelDao extends BaseDao<VipLevel, Long> {
    @Query("from VipLevel order by score desc")
    @QueryHints({ @QueryHint(name = org.hibernate.ejb.QueryHints.HINT_CACHEABLE, value = "true") })
    public List<VipLevel> findAllCached();

}