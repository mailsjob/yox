package com.meiyuetao.myt.vip.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.vip.entity.VipBakMoneyHistory;

@Repository
public interface VipBakMoneyHistoryDao extends BaseDao<VipBakMoneyHistory, Long> {

}