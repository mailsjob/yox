package com.meiyuetao.myt.vip.dao;

import java.util.List;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.vip.entity.VipCustomerProfile;

@Repository
public interface VipCustomerProfileDao extends BaseDao<VipCustomerProfile, Long> {

    @Query("from VipCustomerProfile where upstream=:upstream")
    List<VipCustomerProfile> findByUpstream(@Param("upstream") VipCustomerProfile upstream);


    @Query("select id from VipCustomerProfile where upstream=:upstream")
    List<Long> findForUpstream(@Param("upstream") VipCustomerProfile upstream);

}