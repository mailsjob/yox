package com.meiyuetao.myt.vip.dao;

import com.meiyuetao.myt.vip.entity.VipWxQrcodeHistory;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface VipWxQrcodeHistoryDao extends BaseDao<VipWxQrcodeHistory, Long> {

}