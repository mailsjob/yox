package com.meiyuetao.myt.vip.dao;

import com.meiyuetao.myt.sale.entity.BoxOrder;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.vip.entity.VipWxQrcode;

@Repository
public interface VipWxQrcodeDao extends BaseDao<VipWxQrcode, Long> {
}