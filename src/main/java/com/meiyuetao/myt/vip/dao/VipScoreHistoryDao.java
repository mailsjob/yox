package com.meiyuetao.myt.vip.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.vip.entity.VipScoreHistory;

@Repository
public interface VipScoreHistoryDao extends BaseDao<VipScoreHistory, Long> {

}