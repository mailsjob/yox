package com.meiyuetao.myt.vip.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.vip.entity.VipCommodity;
import com.meiyuetao.myt.vip.service.VipCommodityService;

@MetaData("vip商品管理")
public class VipCommodityController extends BaseController<VipCommodity, Long> {

    @Autowired
    private VipCommodityService vipCommodityService;

    @Override
    protected BaseService<VipCommodity, Long> getEntityService() {
        return vipCommodityService;
    }

    @Override
    protected void checkEntityAclPermission(VipCommodity entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}