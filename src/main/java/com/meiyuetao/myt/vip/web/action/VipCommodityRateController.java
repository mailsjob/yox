package com.meiyuetao.myt.vip.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.vip.entity.VipCommodityRate;
import com.meiyuetao.myt.vip.service.VipCommodityRateService;

@MetaData("VipCommodityRateController")
public class VipCommodityRateController extends BaseController<VipCommodityRate, Long> {

    @Autowired
    private VipCommodityRateService vipCommodityRateService;

    @Override
    protected BaseService<VipCommodityRate, Long> getEntityService() {
        return vipCommodityRateService;
    }

    @Override
    protected void checkEntityAclPermission(VipCommodityRate entity) {
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}