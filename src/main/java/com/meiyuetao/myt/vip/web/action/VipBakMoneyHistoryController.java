package com.meiyuetao.myt.vip.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.vip.entity.VipBakMoneyHistory;
import com.meiyuetao.myt.vip.entity.VipBakMoneyHistoryDetail;
import com.meiyuetao.myt.vip.service.VipBakMoneyHistoryService;

@MetaData("VIP积分信息历史记录管理")
public class VipBakMoneyHistoryController extends BaseController<VipBakMoneyHistory, Long> {

    @Autowired
    private VipBakMoneyHistoryService vipBakMoneyHistoryService;

    @Override
    protected BaseService<VipBakMoneyHistory, Long> getEntityService() {
        return vipBakMoneyHistoryService;
    }

    @Override
    protected void checkEntityAclPermission(VipBakMoneyHistory entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("行项数据")
    public HttpHeaders vipBakMoneyHistoryDetails() {
        List<VipBakMoneyHistoryDetail> VipBakMoneyHistoryDetails = bindingEntity.getVipBakMoneyHistoryDetails();
        setModel(buildPageResultFromList(VipBakMoneyHistoryDetails));
        return buildDefaultHttpHeaders();
    }
}