package com.meiyuetao.myt.vip.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.vip.entity.VipCustomerProfile;
import com.meiyuetao.myt.vip.entity.VipWxQrcode;
import com.meiyuetao.myt.vip.service.VipCustomerProfileService;
import com.meiyuetao.myt.vip.service.VipWxQrcodeService;

@MetaData("VIP微信二维码管理")
public class VipWxQrcodeController extends BaseController<VipWxQrcode, Long> {

    @Autowired
    private VipWxQrcodeService vipWxQrcodeService;

    @Autowired
    private VipCustomerProfileService vipCustomerProfileService;

    @Override
    protected BaseService<VipWxQrcode, Long> getEntityService() {
        return vipWxQrcodeService;
    }

    @Override
    protected void checkEntityAclPermission(VipWxQrcode entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        String vipId = super.getParameter("vipCustomerProfile.id");
        String vipDis = super.getParameter("vipCustomerProfile.display");
        if (!StringUtils.isNullOrEmpty(vipDis)) {
            VipCustomerProfile vipCustomerProfile = vipCustomerProfileService.findOne(Long.valueOf(vipId));
            bindingEntity.setVipCustomerProfile(vipCustomerProfile);
        } else {
            bindingEntity.setVipCustomerProfile(null);
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    /**
     * 判断是否该VIP客户账户是否已有二维码
     * 
     * @return
     */
    public HttpHeaders findByVipCustomer() {
        VipCustomerProfile vipustomerProfile = vipCustomerProfileService.findOne(Long.valueOf(getParameter("vipCustomer")));
        List<VipWxQrcode> vipWxQrcodes = vipWxQrcodeService.findByFilter(new PropertyFilter(MatchType.EQ, "vipCustomerProfile", vipustomerProfile));
        setModel(vipWxQrcodes);
        return buildDefaultHttpHeaders();
    }
}