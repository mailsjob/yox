package com.meiyuetao.myt.vip.web.action;

import com.meiyuetao.myt.vip.entity.VipWxQrcodeBack;
import com.meiyuetao.myt.vip.service.VipWxQrcodeBackService;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

@MetaData("VIP微信二维码扫码记录管理")
public class VipWxQrcodeBackController extends BaseController<VipWxQrcodeBack, Long> {

    @Autowired
    private VipWxQrcodeBackService vipWxQrcodeBackService;

    @Override
    protected BaseService<VipWxQrcodeBack, Long> getEntityService() {
        return vipWxQrcodeBackService;
    }

    @Override
    protected void checkEntityAclPermission(VipWxQrcodeBack entity) {
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}