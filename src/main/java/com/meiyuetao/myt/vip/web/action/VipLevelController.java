package com.meiyuetao.myt.vip.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.vip.entity.VipLevel;
import com.meiyuetao.myt.vip.service.VipLevelService;

@MetaData("VipLevelController")
public class VipLevelController extends BaseController<VipLevel, Long> {

    @Autowired
    private VipLevelService vipLevelService;

    @Override
    protected BaseService<VipLevel, Long> getEntityService() {
        return vipLevelService;
    }

    @Override
    protected void checkEntityAclPermission(VipLevel entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}