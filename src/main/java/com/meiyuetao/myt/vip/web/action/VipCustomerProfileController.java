package com.meiyuetao.myt.vip.web.action;

import com.google.common.collect.Maps;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.vip.entity.VipCustomerProfile;
import com.meiyuetao.myt.vip.entity.VipLevel;
import com.meiyuetao.myt.vip.service.VipCustomerProfileService;
import com.meiyuetao.myt.vip.service.VipLevelService;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import org.apache.struts2.rest.HttpHeaders;
import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@MetaData("VipCustomerProfile")
public class VipCustomerProfileController extends MytBaseController<VipCustomerProfile, Long> {

    @Autowired
    private VipCustomerProfileService vipCustomerProfileService;

    @Autowired
    private VipLevelService vipLevelService;

    @Override
    protected BaseService<VipCustomerProfile, Long> getEntityService() {
        return vipCustomerProfileService;
    }

    @Override
    protected void checkEntityAclPermission(VipCustomerProfile entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        String level = super.getParameter("vipLevel.id");
        String upstream = super.getParameter("upstream.id");
        if (StringUtils.isNullOrEmpty(upstream)) {
            List<VipLevel> viplevels = vipLevelService.findByFilter(new PropertyFilter(MatchType.EQ, "score", BigDecimal.valueOf(10000000.00)));
            List<VipCustomerProfile> vipCustomers = vipCustomerProfileService.findByFilter(new PropertyFilter(MatchType.EQ, "vipLevel", viplevels.get(0)));
            bindingEntity.setUpstream(null);
            bindingEntity.setUpstream(vipCustomers.get(0));
        } else {
            VipLevel vipLevel = vipLevelService.findOne(Long.valueOf(level));
            BigDecimal vipLevelScore = vipLevel.getScore();
            VipCustomerProfile vipUpstream = vipCustomerProfileService.findOne(Long.valueOf(upstream));
            BigDecimal vipUpstreamScore = vipUpstream.getVipLevel().getScore();
            if (vipLevelScore.compareTo(vipUpstreamScore) >= 0) {
                setModel(OperationResult.buildFailureResult("等级不能比直接上线等级高或相等"));
                return buildDefaultHttpHeaders();
            }
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {

        return super.findByPage();
    }

    @MetaData("行项数据")
    public HttpHeaders findParent() {
        List<VipCustomerProfile> vipCustomerProfiles = new ArrayList<VipCustomerProfile>();
        VipCustomerProfile vipCustomerProfile = bindingEntity.getUpstream();
        if (vipCustomerProfile != null) {
            vipCustomerProfiles.add(vipCustomerProfile);
        }
        setModel(buildPageResultFromList(vipCustomerProfiles));
        return buildDefaultHttpHeaders();
    }

    @MetaData("下线展示图")
    public HttpHeaders getXiaxian() {
        // 父亲层
        Map<String, Object> parent = Maps.newHashMap();
        List<Map<String, Object>> lstChildren = new ArrayList<Map<String, Object>>();
        if (org.apache.commons.lang3.StringUtils.isNotBlank(getParameter("id"))) {
            Long id = Long.valueOf(getParameter("id"));
            List<Long> lstVipChildren = vipCustomerProfileService.findForUpstream(id);
            for (Long vipChildren : lstVipChildren) {
                // 孩子层
                Map<String, Object> children = Maps.newHashMap();
                List<Map<String, Object>> lstGradeChildren = new ArrayList<Map<String, Object>>();
                List<Long> lstVipGradeChildren = vipCustomerProfileService.findForUpstream(Long.valueOf(vipChildren));
                for (Long vipGradeChildren : lstVipGradeChildren) {
                    // 孙子层 (只有name属性 没有孩子)
                    Map<String, Object> gradeChildren = Maps.newHashMap();
                    gradeChildren.put("name", vipGradeChildren);
                    lstGradeChildren.add(gradeChildren);
                }
                children.put("name", vipChildren);
                children.put("children", lstGradeChildren);
                lstChildren.add(children);
            }
            parent.put("name", getParameter("id"));
            parent.put("children", lstChildren);
        }
        setModel(parent);
        return buildDefaultHttpHeaders();
    }
//    @MetaData("下线展示图")
//    public HttpHeaders getXiaxian() {
//        // 父亲层
//        Map<String, Object> parent = Maps.newHashMap();
//        List<Map<String, Object>> lstChildren = new ArrayList<Map<String, Object>>();
//        if (org.apache.commons.lang3.StringUtils.isNotBlank(getParameter("id"))) {
//            Long id = Long.valueOf(getParameter("id"));
//            VipCustomerProfile vipParent = vipCustomerProfileService.findOne(id);
//            GroupPropertyFilter childrenGpf = GroupPropertyFilter.buildDefaultAndGroupFilter();
//            childrenGpf.append(new PropertyFilter(MatchType.EQ, "upstream", vipParent));
//            List<VipCustomerProfile> lstVipChildren = vipCustomerProfileService.findByFilters(childrenGpf);
//            for (VipCustomerProfile vipChildren : lstVipChildren) {
//                // 孩子层
//                Map<String, Object> children = Maps.newHashMap();
//                List<Map<String, Object>> lstGradeChildren = new ArrayList<Map<String, Object>>();
//                GroupPropertyFilter gradeChildrenGpf = GroupPropertyFilter.buildDefaultAndGroupFilter();
//                childrenGpf.append(new PropertyFilter(MatchType.EQ, "upstream", vipChildren));
//                List<VipCustomerProfile> lstVipGradeChildren = vipCustomerProfileService.findByFilters(gradeChildrenGpf);
//                for (VipCustomerProfile vipGradeChildren : lstVipGradeChildren) {
//                    // 孙子层 (只有name属性 没有孩子)
//                    Map<String, Object> gradeChildren = Maps.newHashMap();
//                    gradeChildren.put("name", vipGradeChildren.getDisplay());
//                    lstGradeChildren.add(gradeChildren);
//                }
//                children.put("name", vipChildren.getDisplay());
//                children.put("children", lstGradeChildren);
//                lstChildren.add(children);
//            }
//            parent.put("name", vipParent.getDisplay());
//            parent.put("children", lstChildren);
//        }
//        setModel(parent);
//        return buildDefaultHttpHeaders();
//    }
}