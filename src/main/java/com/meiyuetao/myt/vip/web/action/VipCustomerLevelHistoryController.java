package com.meiyuetao.myt.vip.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.vip.entity.VipCustomerLevelHistory;
import com.meiyuetao.myt.vip.service.VipCustomerLevelHistoryService;

@MetaData("客户历史积分")
public class VipCustomerLevelHistoryController extends BaseController<VipCustomerLevelHistory, Long> {

    @Autowired
    private VipCustomerLevelHistoryService vipCustomerLevelHistoryService;

    @Override
    protected BaseService<VipCustomerLevelHistory, Long> getEntityService() {
        return vipCustomerLevelHistoryService;
    }

    @Override
    protected void checkEntityAclPermission(VipCustomerLevelHistory entity) {

    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}