package com.meiyuetao.myt.vip.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.sale.entity.BoxOrder;

@MetaData("vip客户等级历史记录")
@Entity
@Table(name = "vip_customer_level_history")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VipCustomerLevelHistory extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("客户")
    private CustomerProfile customerProfile;
    @MetaData("等级提升积分")
    private BigDecimal levelUpScore;
    @MetaData("备注")
    private String memo;
    @MetaData("订单")
    private BoxOrder boxOrder;

    @ManyToOne
    @JoinColumn(name = "customer_profile_sid", nullable = false)
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @JsonProperty
    public BigDecimal getLevelUpScore() {
        return levelUpScore;
    }

    public void setLevelUpScore(BigDecimal levelUpScore) {
        this.levelUpScore = levelUpScore;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @ManyToOne
    @JoinColumn(name = "order_sid", nullable = false)
    @JsonProperty
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

}