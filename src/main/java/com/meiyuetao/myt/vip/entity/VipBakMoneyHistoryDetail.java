package com.meiyuetao.myt.vip.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("VIP返利信息历史记录明细")
@Entity
@Table(name = "vip_back_money_history_detail")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VipBakMoneyHistoryDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("返利信息")
    private VipBakMoneyHistory vipBakMoneyHistory;
    @MetaData("客户")
    private CustomerProfile customerProfile;
    @MetaData("VIP等级")
    private VipLevel level;
    @MetaData("等级名称")
    private String levelName;
    @MetaData("返利金额")
    private BigDecimal backMoney = BigDecimal.ZERO;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "back_money_history_sid")
    @JsonProperty
    public VipBakMoneyHistory getVipBakMoneyHistory() {
        return vipBakMoneyHistory;
    }

    public void setVipBakMoneyHistory(VipBakMoneyHistory vipBakMoneyHistory) {
        this.vipBakMoneyHistory = vipBakMoneyHistory;
    }

    @ManyToOne
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @JsonProperty
    public VipLevel getLevel() {
        return level;
    }

    public void setLevel(VipLevel level) {
        this.level = level;
    }

    @JsonProperty
    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    @JsonProperty
    public BigDecimal getBackMoney() {
        return backMoney;
    }

    public void setBackMoney(BigDecimal backMoney) {
        this.backMoney = backMoney;
    }

}
