package com.meiyuetao.myt.vip.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import lab.s2jh.core.annotation.MetaData;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * Created by zhuhui on 16-2-29.
 */
@MetaData("VIP微信二维码扫码记录")
@Entity
@Table(name = "vip_wx_qrcode_history")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class VipWxQrcodeHistory extends MytBaseEntity {
    @MetaData("openId")
    private String weixin;
    @MetaData("VIP用户")
    private VipCustomerProfile vipCustomerProfile;
    @MetaData("uninid")
    private String uninId;

    @JoinColumn(name = "vip_sid")
    @OneToOne
    @JsonProperty
    public VipCustomerProfile getVipCustomerProfile() {
        return vipCustomerProfile;
    }

    public void setVipCustomerProfile(VipCustomerProfile vipCustomerProfile) {
        this.vipCustomerProfile = vipCustomerProfile;
    }

    @JsonProperty
    @Column(name = "weixin_openID")
    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }


    @Column(name = "unin_id")
    @JsonProperty
    public String getUninId() {
        return uninId;
    }

    public void setUninId(String uninId) {
        this.uninId = uninId;
    }
}
