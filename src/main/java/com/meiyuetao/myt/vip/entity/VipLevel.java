package com.meiyuetao.myt.vip.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("vip会员等级信息表")
@Entity
@Table(name = "vip_level")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VipLevel extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("代码")
    private String code;
    @MetaData("名称")
    private String name;
    @MetaData("积分")
    private BigDecimal score;
    @MetaData("返利比率")
    private BigDecimal backMoneyRate = BigDecimal.ZERO;

    @MetaData(value = "排序号")
    private Integer levelIndex = 100;

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    @JsonProperty
    public BigDecimal getBackMoneyRate() {
        return backMoneyRate;
    }

    public void setBackMoneyRate(BigDecimal backMoneyRate) {
        this.backMoneyRate = backMoneyRate;
    }

    @JsonProperty
    public Integer getLevelIndex() {
        return levelIndex;
    }

    public void setLevelIndex(Integer levelIndex) {
        this.levelIndex = levelIndex;
    }

    @JsonProperty
    @Column(nullable = false, updatable = false)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
