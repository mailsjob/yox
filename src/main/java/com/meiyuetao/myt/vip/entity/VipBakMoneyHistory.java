package com.meiyuetao.myt.vip.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.sale.entity.BoxOrder;

@MetaData("VIP积分信息历史记录")
@Entity
@Table(name = "vip_back_money_history")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VipBakMoneyHistory extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("客户信息")
    private CustomerProfile customerProfile;
    @MetaData("返利金额")
    private BigDecimal backMoney;
    @MetaData("状态")
    private VipBakMoneyHistoryStatusEnum status;

    // LOCK:锁定 下单付款前为锁定状态;FREEZE:冻结 付款后到订单结束期间冻结;CANCEL:取消订单作废状态;ADDED:已累加 只做统计用
    public enum VipBakMoneyHistoryStatusEnum {
        @MetaData("锁定")
        LOCK, @MetaData("冻结")
        FREEZE, @MetaData("取消")
        CANCEL, @MetaData("已累加")
        ADDED, @MetaData("已转出")
        TO_BALANCE;
    }

    @MetaData("订单")
    private BoxOrder boxOrder;

    @MetaData("vip返利")
    private List<VipBakMoneyHistoryDetail> vipBakMoneyHistoryDetails = new ArrayList<VipBakMoneyHistoryDetail>();

    @JsonProperty
    public BigDecimal getBackMoney() {
        return backMoney;
    }

    public void setBackMoney(BigDecimal backMoney) {
        this.backMoney = backMoney;
    }

    @Enumerated(EnumType.STRING)
    @JsonProperty
    public VipBakMoneyHistoryStatusEnum getStatus() {
        return status;
    }

    public void setStatus(VipBakMoneyHistoryStatusEnum status) {
        this.status = status;
    }

    @ManyToOne
    @JoinColumn(name = "order_sid")
    @JsonProperty
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

    @ManyToOne
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @OneToMany(mappedBy = "vipBakMoneyHistory", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<VipBakMoneyHistoryDetail> getVipBakMoneyHistoryDetails() {
        return vipBakMoneyHistoryDetails;
    }

    public void setVipBakMoneyHistoryDetails(List<VipBakMoneyHistoryDetail> vipBakMoneyHistoryDetails) {
        this.vipBakMoneyHistoryDetails = vipBakMoneyHistoryDetails;
    }

}
