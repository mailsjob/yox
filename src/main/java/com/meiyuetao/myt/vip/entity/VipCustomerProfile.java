package com.meiyuetao.myt.vip.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.BaseEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.math.BigDecimal;

@MetaData("vip客户表")
@Entity
@Table(name = "vip_customer_profile")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class VipCustomerProfile extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
    /**
     * 流水号主键
     */
    protected Long id;
    @MetaData("客户")
    private CustomerProfile customerProfile;
    @MetaData("客户")
    private CustomerProfile customerProfileSid;
    @MetaData("VIP等级")
    private VipLevel vipLevel;
    @MetaData("VIP等级名")
    private String vipLevelName;
    @MetaData("积分")
    private BigDecimal score = BigDecimal.ZERO;
    private BigDecimal backMoney = BigDecimal.ZERO;
    @MetaData("直接上线")
    private VipCustomerProfile upstream;

    @Id
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "foreign", parameters = @Parameter(name = "property", value = "customerProfile"))
    @Column(name = "sid")
    @JsonProperty
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(optional = false)
    @PrimaryKeyJoinColumn()
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "vip_level_sid")
    @JsonProperty
    public VipLevel getVipLevel() {
        return vipLevel;
    }

    public void setVipLevel(VipLevel vipLevel) {
        this.vipLevel = vipLevel;
    }

    @JsonProperty
    public String getVipLevelName() {
        return vipLevelName;
    }

    public void setVipLevelName(String vipLevelName) {
        this.vipLevelName = vipLevelName;
    }

    @JsonProperty
    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    @JsonProperty
    public BigDecimal getBackMoney() {
        return backMoney;
    }

    public void setBackMoney(BigDecimal backMoney) {
        this.backMoney = backMoney;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "upstream_sid")
    @JsonProperty
    public VipCustomerProfile getUpstream() {
        return upstream;
    }

    public void setUpstream(VipCustomerProfile upstream) {
        this.upstream = upstream;
    }

    @Transient
    @JsonProperty
    public String getDisplay() {
        return customerProfile.getDisplay() + "," + vipLevel.getName();
    }

    @OneToOne
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfileSid() {
        return customerProfileSid;
    }

    public void setCustomerProfileSid(CustomerProfile customerProfileSid) {
        this.customerProfileSid = customerProfileSid;
    }
}
