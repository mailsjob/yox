package com.meiyuetao.myt.vip.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;

@MetaData("vip商品积分比率表")
@Entity
@Table(name = "vip_commodity_rate")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VipCommodityRate extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("VIP等级")
    private VipLevel vipLevel;
    @MetaData("等级积分比率")
    private Integer levelRate;

    @ManyToOne
    @JoinColumn(name = "commodity_sid", nullable = false)
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @ManyToOne
    @JoinColumn(name = "level_sid", nullable = false)
    @JsonProperty
    public VipLevel getVipLevel() {
        return vipLevel;
    }

    public void setVipLevel(VipLevel vipLevel) {
        this.vipLevel = vipLevel;
    }

    @JsonProperty
    public Integer getLevelRate() {
        return levelRate;
    }

    public void setLevelRate(Integer levelRate) {
        this.levelRate = levelRate;
    }

}
