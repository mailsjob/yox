package com.meiyuetao.myt.vip.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("VIP微信二维码")
@Entity
@Table(name = "vip_wx_qrcode")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class VipWxQrcode extends MytBaseEntity {

    private static final long serialVersionUID = 1L;
    @MetaData("二维码参数")
    private String actionInfo;
    @MetaData("客户信息")
    private CustomerProfile customerProfile;
    @MetaData("二维码图片")
    private String picMd5;
    @MetaData("OpenId")
    private String openId;
    @MetaData("UninId")
    private String uninId;
    @MetaData("二维码类型")
    private String qrCodeType;
    @MetaData("VIP客户信息")
    private VipCustomerProfile vipCustomerProfile;

    @Column(name = "action_info", length = 50)
    @JsonProperty
    public String getActionInfo() {
        return actionInfo;
    }

    public void setActionInfo(String actionInfo) {
        this.actionInfo = actionInfo;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "custormer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Column(name = "pic_md5", length = 32)
    @JsonProperty
    public String getPicMd5() {
        return picMd5;
    }

    public void setPicMd5(String picMd5) {
        this.picMd5 = picMd5;
    }

    @Column(name = "open_id", length = 50)
    @JsonProperty
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Column(name = "unin_id", length = 50)
    @JsonProperty
    public String getUninId() {
        return uninId;
    }

    public void setUninId(String uninId) {
        this.uninId = uninId;
    }

    @Column(name = "qr_code_type", length = 50)
    @JsonProperty
    public String getQrCodeType() {
        return qrCodeType;
    }

    public void setQrCodeType(String qrCodeType) {
        this.qrCodeType = qrCodeType;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "vip_customer_profile_sid")
    @JsonProperty
    public VipCustomerProfile getVipCustomerProfile() {
        return vipCustomerProfile;
    }

    public void setVipCustomerProfile(VipCustomerProfile vipCustomerProfile) {
        this.vipCustomerProfile = vipCustomerProfile;
    }

}
