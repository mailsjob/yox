package com.meiyuetao.myt.vip.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.BaseEntity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.md.entity.Commodity;

@MetaData("vip商品")
@Entity
@Table(name = "vip_commodity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VipCommodity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("返利金额")
    private BigDecimal backMoney = BigDecimal.ZERO;
    @MetaData("积分")
    private BigDecimal score = BigDecimal.ZERO;
    @MetaData("是否启用")
    private Boolean enable = Boolean.FALSE;
    @MetaData("商品sid")
    private Long commoditySid;
    @MetaData("背景图")
    private String backgroundPic;
    @MetaData("缩略图")
    private String smallPic;
    @MetaData("聚焦图")
    private String focusSmallPic;
    @MetaData("小标题")
    private String smallTitle;
    @MetaData("描述")
    private String description;
    @MetaData("图")
    private String pic;

    /** 流水号主键 */
    protected Long id;

    @Id
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "foreign", parameters = @Parameter(name = "property", value = "commodity"))
    @Column(name = "sid")
    @JsonProperty
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @PrimaryKeyJoinColumn
    @JsonIgnore
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @JsonProperty
    public BigDecimal getBackMoney() {
        return backMoney;
    }

    public void setBackMoney(BigDecimal backMoney) {
        this.backMoney = backMoney;
    }

    @JsonProperty
    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    @Column(name = "enable", nullable = false)
    @JsonProperty
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @Column(name = "commodity_sid")
    public Long getCommoditySid() {
        return commoditySid;
    }

    public void setCommoditySid(Long commoditySid) {
        this.commoditySid = commoditySid;
    }

    @Column(name = "background_pic")
    @JsonProperty
    public String getBackgroundPic() {
        return backgroundPic;
    }

    public void setBackgroundPic(String backgroundPic) {
        this.backgroundPic = backgroundPic;
    }

    @Column(name = "small_pic")
    @JsonProperty
    public String getSmallPic() {
        return smallPic;
    }

    public void setSmallPic(String smallPic) {
        this.smallPic = smallPic;
    }

    @Column(name = "focus_small_pic")
    @JsonProperty
    public String getFocusSmallPic() {
        return focusSmallPic;
    }

    public void setFocusSmallPic(String focusSmallPic) {
        this.focusSmallPic = focusSmallPic;
    }

    @Column(name = "small_title")
    @JsonProperty
    public String getSmallTitle() {
        return smallTitle;
    }

    public void setSmallTitle(String smallTitle) {
        this.smallTitle = smallTitle;
    }

    @Column(name = "description")
    @JsonProperty
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "pic")
    @JsonProperty
    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @Transient
    @JsonProperty
    public String getDisplay() {
        return this.commodity.getTitle();
    }
}
