package com.meiyuetao.myt.misc.web.action;

import java.util.Date;
import java.util.Map;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.util.DateUtils;
import lab.s2jh.core.web.SimpleController;

import org.apache.struts2.rest.HttpHeaders;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Maps;
import com.meiyuetao.myt.md.service.BrandService;
import com.meiyuetao.myt.md.service.CommodityService;
import com.meiyuetao.myt.purchase.service.PurchaseOrderService;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail.BoxOrderDetailStatusEnum;
import com.meiyuetao.myt.sale.service.BoxOrderDetailService;
import com.meiyuetao.myt.sale.service.SaleDeliveryService;

@MetaData("预警通知")
public class NotifyController extends SimpleController {

    @Autowired
    private BoxOrderDetailService boxOrderDetailService;

    @Autowired
    private SaleDeliveryService saleDeliveryService;

    @Autowired
    private CommodityService commodityService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @MetaData(value = "区域数据")
    public HttpHeaders list() {
        Map<String, Object> notifyDataMap = Maps.newHashMap();
        long count = 0;
        GroupPropertyFilter groupFilter = null;

        // 待提醒用户付款的订单行项
        groupFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupFilter.append(new PropertyFilter(MatchType.LE, "reserveDeliveryTime", new DateTime().plusDays(8).toDate()));
        groupFilter.append(new PropertyFilter(MatchType.IN, "orderDetailStatus", new BoxOrderDetailStatusEnum[] { BoxOrderDetailStatusEnum.S10O, BoxOrderDetailStatusEnum.S15O }));
        count = boxOrderDetailService.count(groupFilter);
        if (count > 0) {
            notifyDataMap.put("NOTIFY_TO_PAY_ORDER_DETAILS", count);
        }

        // 预约发货日期快到或过期的提醒拣货发货的销售单行项
        groupFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupFilter.append(new PropertyFilter(MatchType.LE, "reserveDeliveryTime", DateUtils.parseDate(DateUtils.formatDateNow())));
        groupFilter.append(new PropertyFilter(MatchType.NN, "auditDate", true));
        groupFilter.append(new PropertyFilter(MatchType.NU, "redwordDate", true));
        groupFilter.append(new PropertyFilter(MatchType.NU, "deliveryTime", true));
        count = saleDeliveryService.count(groupFilter);
        if (count > 0) {
            notifyDataMap.put("NOTIFY_TO_DELIVERY_SALE_DETAILS", count);
        }

        // 商品内容敏感信息预警
        count = commodityService.findContainSensitiveCommodityCount();
        if (count > 0) {
            notifyDataMap.put("NOTIFY_TO_COMMODITY", count);
        }

        // 品牌授权截止日期预警 (查询品牌授权到期日期 brand的endDate在未来一周内的行项)
        groupFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        DateTime dt = new DateTime();
        dt = dt.plusWeeks(1);
        groupFilter.append(new PropertyFilter(MatchType.LE, "endDate", dt.toDate()));
        count = brandService.count(groupFilter);
        if (count > 0) {
            notifyDataMap.put("NOTIFY_TO_BRAND", count);
        }

        // 计划入库时间已到还未入库的
        groupFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupFilter.append(new PropertyFilter(MatchType.LE, "planReceiptDate", new Date()));// 计划入库时间已到
        groupFilter.append(new PropertyFilter(MatchType.NU, "receiptAuditDate", true));// 没有入库的
        groupFilter.append(new PropertyFilter(MatchType.NU, "redwordDate", true));// 没有红冲的
        count = purchaseOrderService.count(groupFilter);
        if (count > 0) {
            notifyDataMap.put("NOTIFY_TO_PURCHASE_ORDER", count);
        }
        setModel(notifyDataMap);
        getRequest().setAttribute("notifyDataMap", notifyDataMap);
        return buildDefaultHttpHeaders("list");
    }
}