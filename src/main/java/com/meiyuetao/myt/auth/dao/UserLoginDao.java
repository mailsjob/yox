package com.meiyuetao.myt.auth.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.auth.entity.UserLogin;

@Repository
public interface UserLoginDao extends BaseDao<UserLogin, Long> {

}