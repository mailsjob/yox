package com.meiyuetao.myt.auth.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.auth.entity.User;
import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@Entity
@Table(name = "myt_user_login")
@MetaData(value = "用户登录信息")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserLogin extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联登录用户")
    private User user;
    @MetaData("最后一次登录时间")
    private Date lastLoginTime;
    @MetaData("最后一次操作时间")
    private Date lastOperateTime;
    @MetaData(value = "随机数")
    private String randomCode;

    @OneToOne
    @JoinColumn(name = "user_sid", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getRandomCode() {
        return randomCode;
    }

    public void setRandomCode(String randomCode) {
        this.randomCode = randomCode;
    }

    public Date getLastOperateTime() {
        return lastOperateTime;
    }

    public void setLastOperateTime(Date lastOperateTime) {
        this.lastOperateTime = lastOperateTime;
    }

}
