package com.meiyuetao.myt.auth.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import lab.s2jh.bpm.BpmTrackable;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.EntityAutoCode;
import lab.s2jh.core.entity.annotation.SkipParamBind;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.partner.entity.Partner;

@Entity
@Table(name = "TBL_AGENT_AUTH_SIGNUP_USER")
@MetaData(value = "代理商自助注册账号数据")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class AgentSignupUser extends MytBaseEntity implements BpmTrackable {

    private static final long serialVersionUID = 1L;
    @MetaData(value = "登录账号")
    @EntityAutoCode(order = 10, search = true)
    private String signinid;

    @MetaData(value = "登录密码")
    @EntityAutoCode(order = 10, listShow = false)
    private String password;

    @MetaData(value = "电子邮件")
    @EntityAutoCode(order = 30, search = true)
    private String email;

    @MetaData(value = "注册手机号")
    @EntityAutoCode(order = 30, search = true)
    private String mobilePhone;

    @MetaData(value = "注册时间")
    @EntityAutoCode(order = 40, edit = false, listHidden = true)
    private Date signupTime;

    @MetaData(value = "联系信息")
    @EntityAutoCode
    private String contactInfo;

    @MetaData(value = "备注说明")
    @EntityAutoCode
    private String remarkInfo;

    @MetaData(value = "审核处理时间")
    private Date auditTime;
    @MetaData("活动节点名称")
    private String activeTaskName;

    @MetaData("活动节点名称")
    private Partner agentPartner;
    @MetaData("身份证照片")
    private String idCardImg;
    @MetaData("资质照片")
    private String licenseImg1;
    @MetaData("资质照片")
    private String licenseImg2;
    @MetaData("资质照片")
    private String licenseImg3;
    @MetaData("资质照片")
    private String licenseImg4;
    @MetaData("资质照片")
    private String licenseImg5;
    @MetaData("是否驳回")
    private Boolean enable = Boolean.TRUE;

    @Size(min = 3, max = 30)
    @Column(length = 128, unique = true, nullable = false)
    public String getSigninid() {
        return signinid;
    }

    public void setSigninid(String signinid) {
        this.signinid = signinid;
    }

    @Column(updatable = false, length = 128, nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getSignupTime() {
        return signupTime;
    }

    @SkipParamBind
    public void setSignupTime(Date signupTime) {
        this.signupTime = signupTime;
    }

    @Email
    @Column(length = 128)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    @Transient
    public String getDisplay() {
        return signinid;
    }

    @Column(length = 3000)
    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    @Column(length = 3000)
    public String getRemarkInfo() {
        return remarkInfo;
    }

    public void setRemarkInfo(String remarkInfo) {
        this.remarkInfo = remarkInfo;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    @Override
    @Transient
    @JsonIgnore
    public String getBpmBusinessKey() {
        return agentPartner.getCode() + "_" + signinid;
    }

    public String getActiveTaskName() {
        return activeTaskName;
    }

    public void setActiveTaskName(String activeTaskName) {
        this.activeTaskName = activeTaskName;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "agent_partner_sid")
    @JsonProperty
    public Partner getAgentPartner() {
        return agentPartner;
    }

    public void setAgentPartner(Partner agentPartner) {
        this.agentPartner = agentPartner;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getIdCardImg() {
        return idCardImg;
    }

    public void setIdCardImg(String idCardImg) {
        this.idCardImg = idCardImg;
    }

    public String getLicenseImg1() {
        return licenseImg1;
    }

    public void setLicenseImg1(String licenseImg1) {
        this.licenseImg1 = licenseImg1;
    }

    public String getLicenseImg2() {
        return licenseImg2;
    }

    public void setLicenseImg2(String licenseImg2) {
        this.licenseImg2 = licenseImg2;
    }

    public String getLicenseImg3() {
        return licenseImg3;
    }

    public void setLicenseImg3(String licenseImg3) {
        this.licenseImg3 = licenseImg3;
    }

    public String getLicenseImg4() {
        return licenseImg4;
    }

    public void setLicenseImg4(String licenseImg4) {
        this.licenseImg4 = licenseImg4;
    }

    public String getLicenseImg5() {
        return licenseImg5;
    }

    public void setLicenseImg5(String licenseImg5) {
        this.licenseImg5 = licenseImg5;
    }

    @Column(name = "is_enable")
    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @Override
    @Transient
    public String getExtraInfo() {
        return null;
    }

}
