package com.meiyuetao.myt.auth.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.auth.dao.AgentSignupUserDao;
import com.meiyuetao.myt.auth.entity.AgentSignupUser;

@Service
@Transactional
public class AgentSignupUserService extends BaseService<AgentSignupUser, Long> {

    @Autowired
    private AgentSignupUserDao agentSignupUserDao;

    @Override
    protected BaseDao<AgentSignupUser, Long> getEntityDao() {
        return agentSignupUserDao;
    }
}
