package com.meiyuetao.myt.auth.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.auth.dao.UserLoginDao;
import com.meiyuetao.myt.auth.entity.UserLogin;

@Service
@Transactional
public class UserLoginService extends BaseService<UserLogin, Long> {

    @Autowired
    private UserLoginDao userLoginDao;

    @Override
    protected BaseDao<UserLogin, Long> getEntityDao() {
        return userLoginDao;
    }
}
