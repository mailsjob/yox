package com.meiyuetao.myt.c2c.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("店铺账户")
@Entity
public class C2cShopAccount extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("店铺名")
    private String shopName;
    @MetaData("总账")
    private BigDecimal total;
    @MetaData("主账户余额")
    private BigDecimal overage;
    @MetaData("补贴")
    private BigDecimal subsidy;
    @MetaData("保证金账户")
    private BigDecimal bail;

    @Transient
    @JsonProperty
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @Transient
    @JsonProperty
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Transient
    @JsonProperty
    public BigDecimal getOverage() {
        return overage;
    }

    public void setOverage(BigDecimal overage) {
        this.overage = overage;
    }

    @Transient
    @JsonProperty
    public BigDecimal getSubsidy() {
        return subsidy;
    }

    public void setSubsidy(BigDecimal subsidy) {
        this.subsidy = subsidy;
    }

    @Transient
    @JsonProperty
    public BigDecimal getBail() {
        return bail;
    }

    public void setBail(BigDecimal bail) {
        this.bail = bail;
    }
}
