package com.meiyuetao.myt.c2c.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("美月淘集市店铺")
@Entity
@Table(name = "c2c_shop_info")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class C2cShopInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("店铺名")
    private String shopName;
    @MetaData("店铺图片")
    private String shopPic;
    @MetaData("手机")
    private String mobilePhone;
    @MetaData("店铺地址")
    private String shopAddr;
    @MetaData("微信")
    private String weixin;
    @MetaData("QQ")
    private String qq;
    @MetaData("旺旺")
    private String wangwang;
    @MetaData("补贴额")
    private BigDecimal subsidy;
    @MetaData("补贴上限")
    private BigDecimal subsidyCap;
    @MetaData("保证金")
    private BigDecimal cautionMoney;
    @MetaData("先行赔付额")
    private BigDecimal advancePayouts;

    @Column(name = "shop_name", length = 512, nullable = false)
    @JsonProperty
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @Column(name = "shop_pic", length = 32)
    @JsonProperty
    public String getShopPic() {
        return shopPic;
    }

    public void setShopPic(String shopPic) {
        this.shopPic = shopPic;
    }

    @Column(name = "mobile_phone", length = 32)
    @JsonProperty
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Column(name = "shop_addr", length = 250)
    @JsonProperty
    public String getShopAddr() {
        return shopAddr;
    }

    public void setShopAddr(String shopAddr) {
        this.shopAddr = shopAddr;
    }

    @Column(name = "weixin", length = 250)
    @JsonProperty
    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }

    @Column(name = "qq", length = 250)
    @JsonProperty
    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    @Column(name = "wangwang", length = 250)
    @JsonProperty
    public String getWangwang() {
        return wangwang;
    }

    public void setWangwang(String wangwang) {
        this.wangwang = wangwang;
    }

    @Column(name = "subsidy", length = 18)
    @JsonProperty
    public BigDecimal getSubsidy() {
        return subsidy;
    }

    public void setSubsidy(BigDecimal subsidy) {
        this.subsidy = subsidy;
    }

    @Column(name = "subsidy_cap", length = 18)
    @JsonProperty
    public BigDecimal getSubsidyCap() {
        return subsidyCap;
    }

    public void setSubsidyCap(BigDecimal subsidyCap) {
        this.subsidyCap = subsidyCap;
    }

    @Column(name = "caution_money", length = 18)
    @JsonProperty
    public BigDecimal getCautionMoney() {
        return cautionMoney;
    }

    public void setCautionMoney(BigDecimal cautionMoney) {
        this.cautionMoney = cautionMoney;
    }

    @Column(name = "advance_payouts", length = 18)
    @JsonProperty
    public BigDecimal getAdvancePayouts() {
        return advancePayouts;
    }

    public void setAdvancePayouts(BigDecimal advancePayouts) {
        this.advancePayouts = advancePayouts;
    }
}
