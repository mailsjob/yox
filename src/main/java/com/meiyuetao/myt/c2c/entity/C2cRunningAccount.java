package com.meiyuetao.myt.c2c.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("集市流水账")
@Entity
@Table(name = "c2c_finance_account_history")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class C2cRunningAccount extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("账户")
    private C2cFinanceAccount c2cFinanceAccount;
    @MetaData("发生日期")
    private Date occureTime;
    @MetaData("发生年月")
    private String occureYearMon;
    @MetaData("发生日")
    private String occureDay;
    @MetaData("解冻日期")
    private Date unfreezeTime;
    @MetaData("流水类型")
    private BizNumTypeEnum bizNumType;
    @MetaData("订单号")
    private String bizNum;
    @MetaData("流水金额")
    private BigDecimal amount;
    @MetaData("业务备注")
    private String bizMemo;
    @MetaData("是否结算")
    private Boolean isSettled;
    @MetaData("是否冻结")
    private Boolean isFrozen;

    @MetaData("销售金额")
    private BigDecimal saleAmount;
    @MetaData("结算补贴")
    private BigDecimal settleSubsidy;
    @MetaData("结算金额")
    private BigDecimal settleAmount;
    @MetaData("退款金额")
    private BigDecimal refundAmount;
    @MetaData("反补贴")
    private BigDecimal returnSubsidy;

    public enum BizNumTypeEnum {
        @MetaData("订单")
        ORDER,

        @MetaData("结算")
        SETTLE,

        @MetaData("异常补贴")
        ERR;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "finance_account_sid")
    @JsonProperty
    public C2cFinanceAccount getC2cFinanceAccount() {
        return c2cFinanceAccount;
    }

    public void setC2cFinanceAccount(C2cFinanceAccount c2cFinanceAccount) {
        this.c2cFinanceAccount = c2cFinanceAccount;
    }

    @Column(name = "occure_time", length = 7)
    @JsonProperty
    public Date getOccureTime() {
        return occureTime;
    }

    public void setOccureTime(Date occureTime) {
        this.occureTime = occureTime;
    }

    @Column(name = "occure_year_mon", length = 7)
    @JsonProperty
    public String getOccureYearMon() {
        return occureYearMon;
    }

    public void setOccureYearMon(String occureYearMon) {
        this.occureYearMon = occureYearMon;
    }

    @Column(name = "occure_day", length = 10)
    @JsonProperty
    public String getOccureDay() {
        return occureDay;
    }

    public void setOccureDay(String occureDay) {
        this.occureDay = occureDay;
    }

    @Column(name = "unfreeze_time", length = 7)
    @JsonProperty
    public Date getUnfreezeTime() {
        return unfreezeTime;
    }

    public void setUnfreezeTime(Date unfreezeTime) {
        this.unfreezeTime = unfreezeTime;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "biz_num_type", length = 7)
    @JsonProperty
    public BizNumTypeEnum getBizNumType() {
        return bizNumType;
    }

    public void setBizNumType(BizNumTypeEnum bizNumType) {
        this.bizNumType = bizNumType;
    }

    @Column(name = "biz_num", length = 64)
    @JsonProperty
    public String getBizNum() {
        return bizNum;
    }

    @Column(name = "amount", length = 64)
    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setBizNum(String bizNum) {
        this.bizNum = bizNum;
    }

    @Column(name = "is_settled")
    @JsonProperty
    public Boolean getIsSettled() {
        return isSettled;
    }

    public void setIsSettled(Boolean isSettled) {
        this.isSettled = isSettled;
    }

    @Column(name = "is_frozen")
    @JsonProperty
    public Boolean getIsFrozen() {
        return isFrozen;
    }

    public void setIsFrozen(Boolean isFrozen) {
        this.isFrozen = isFrozen;
    }

    @JsonProperty
    @Transient
    @NotAudited
    public BigDecimal getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(BigDecimal saleAmount) {
        this.saleAmount = saleAmount;
    }

    @Column(name = "biz_memo", length = 128)
    @JsonProperty
    public String getBizMemo() {
        return bizMemo;
    }

    public void setBizMemo(String bizMemo) {
        this.bizMemo = bizMemo;
    }

    @JsonProperty
    @Transient
    @NotAudited
    public BigDecimal getSettleSubsidy() {
        return settleSubsidy;
    }

    public void setSettleSubsidy(BigDecimal settleSubsidy) {
        this.settleSubsidy = settleSubsidy;
    }

    @Transient
    @JsonProperty
    public BigDecimal getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(BigDecimal settleAmount) {
        this.settleAmount = settleAmount;
    }

    @Transient
    @JsonProperty
    public BigDecimal getReturnSubsidy() {
        return returnSubsidy;
    }

    public void setReturnSubsidy(BigDecimal returnSubsidy) {
        this.returnSubsidy = returnSubsidy;
    }

    @Transient
    @JsonProperty
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }
}
