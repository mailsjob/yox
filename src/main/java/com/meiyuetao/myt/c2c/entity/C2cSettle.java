package com.meiyuetao.myt.c2c.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("美月淘集市结算")
@Entity
@Table(name = "c2c_settle")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class C2cSettle extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("结算前金额")
    private BigDecimal beforeSettleAmount;
    @MetaData("结算金额")
    private BigDecimal settleAmount;
    @MetaData("结算后金额")
    private BigDecimal afterSettleAmount;
    @MetaData("财务账户")
    private C2cFinanceAccount financeAccount;
    @MetaData("结算类型")
    private SettleTypeEnum settleType;
    @MetaData("结算时间")
    private Date settleTime;

    public enum SettleTypeEnum {
        @MetaData("补贴结算")
        BTJS,

        @MetaData("提现")
        TX;
    }

    @Column(name = "before_settle_amount", length = 18)
    @JsonProperty
    public BigDecimal getBeforeSettleAmount() {
        return beforeSettleAmount;
    }

    public void setBeforeSettleAmount(BigDecimal beforeSettleAmount) {
        this.beforeSettleAmount = beforeSettleAmount;
    }

    @Column(name = "settle_amount", length = 18)
    @JsonProperty
    public BigDecimal getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(BigDecimal settleAmount) {
        this.settleAmount = settleAmount;
    }

    @Column(name = "after_settle_amount", length = 18)
    @JsonProperty
    public BigDecimal getAfterSettleAmount() {
        return afterSettleAmount;
    }

    public void setAfterSettleAmount(BigDecimal afterSettleAmount) {
        this.afterSettleAmount = afterSettleAmount;
    }

    @OneToOne
    @JoinColumn(name = "finance_account_sid")
    @JsonProperty
    public C2cFinanceAccount getFinanceAccount() {
        return financeAccount;
    }

    public void setFinanceAccount(C2cFinanceAccount financeAccount) {
        this.financeAccount = financeAccount;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "settle_type", length = 16)
    @JsonProperty
    public SettleTypeEnum getSettleType() {
        return settleType;
    }

    public void setSettleType(SettleTypeEnum settleType) {
        this.settleType = settleType;
    }

    @Column(name = "settle_time", length = 7)
    @JsonProperty
    public Date getSettleTime() {
        return settleTime;
    }

    public void setSettleTime(Date settleTime) {
        this.settleTime = settleTime;
    }
}
