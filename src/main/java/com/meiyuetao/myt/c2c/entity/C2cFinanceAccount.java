package com.meiyuetao.myt.c2c.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("美月淘集市财务账户")
@Entity
@Table(name = "c2c_finance_account")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class C2cFinanceAccount extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("账户类型代码")
    private AccountTypeCodeEnum accountTypeCode;
    @MetaData("主账户")
    private C2cFinanceAccount parent = null;
    @MetaData("店铺")
    private C2cShopInfo shop;
    @MetaData("金额")
    private BigDecimal amount;
    @MetaData("冻结金额")
    private BigDecimal frozenAmount;
    @MetaData("备注")
    private String memo;

    public enum AccountTypeCodeEnum {
        @MetaData("总帐")
        A00,

        @MetaData("主账户余额")
        B00,

        @MetaData("补贴")
        B01,

        @MetaData("保证金帐户")
        B02;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "account_type_code", length = 16)
    @JsonProperty
    public AccountTypeCodeEnum getAccountTypeCode() {
        return accountTypeCode;
    }

    public void setAccountTypeCode(AccountTypeCodeEnum accountTypeCode) {
        this.accountTypeCode = accountTypeCode;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "parent_sid", nullable = true)
    @JsonProperty
    public C2cFinanceAccount getParent() {
        return parent;
    }

    public void setParent(C2cFinanceAccount parent) {
        this.parent = parent;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "shop_sid", nullable = false)
    @JsonProperty
    public C2cShopInfo getShop() {
        return shop;
    }

    public void setShop(C2cShopInfo shop) {
        this.shop = shop;
    }

    @Column(name = "amount", length = 18, nullable = false)
    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "frozen_amount", length = 18)
    @JsonProperty
    public BigDecimal getFrozenAmount() {
        return frozenAmount;
    }

    public void setFrozenAmount(BigDecimal frozenAmount) {
        this.frozenAmount = frozenAmount;
    }

    @Column(name = "memo", length = 128, nullable = false)
    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}
