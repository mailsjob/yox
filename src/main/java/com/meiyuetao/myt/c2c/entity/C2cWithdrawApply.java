package com.meiyuetao.myt.c2c.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("美月淘店铺提现申请表")
@Entity
@Table(name = "c2c_withdraw_apply")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class C2cWithdrawApply extends MytBaseEntity {

    private static final long serialVersionUID = 1L;

    @MetaData(value = "店铺")
    private C2cShopInfo shop;
    @MetaData(value = "金额")
    private BigDecimal amount;
    @MetaData(value = "申请时间")
    private Date applyTime;
    @MetaData(value = "申请状态")
    private ApplyStatusEnum applyStatus;
    @MetaData(value = "外部账户类型")
    private String outAccountType;
    @MetaData(value = "外部帐户编号")
    private String outAccountCode;
    @MetaData(value = "联系方式")
    private String contact;
    @MetaData(value = "备注")
    private String memo;

    public enum ApplyStatusEnum {
        @MetaData(value = "申请中")
        APPLY,

        @MetaData(value = "处理中")
        PROCESS,

        @MetaData(value = "提现完成")
        COMPLEte,

        @MetaData(value = "已取消")
        CANCEL;
    }

    @Column(name = "shop_sid")
    @JsonProperty
    public C2cShopInfo getShop() {
        return shop;
    }

    public void setShop(C2cShopInfo shop) {
        this.shop = shop;
    }

    @Column(name = "amount")
    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "apply_time")
    @JsonProperty
    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "apply_status")
    @JsonProperty
    public ApplyStatusEnum getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(ApplyStatusEnum applyStatus) {
        this.applyStatus = applyStatus;
    }

    @Column(name = "out_account_type")
    @JsonProperty
    public String getOutAccountType() {
        return outAccountType;
    }

    public void setOutAccountType(String outAccountType) {
        this.outAccountType = outAccountType;
    }

    @Column(name = "out_account_code")
    @JsonProperty
    public String getOutAccountCode() {
        return outAccountCode;
    }

    public void setOutAccountCode(String outAccountCode) {
        this.outAccountCode = outAccountCode;
    }

    @Column(name = "contact")
    @JsonProperty
    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @Column(name = "memo")
    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}
