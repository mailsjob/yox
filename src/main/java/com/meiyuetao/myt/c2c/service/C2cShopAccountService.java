package com.meiyuetao.myt.c2c.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.c2c.entity.C2cShopAccount;

@Service
@Transactional
public class C2cShopAccountService extends BaseService<C2cShopAccount, Long> {

    // @Autowired
    // private C2cShopAccountDao c2cShopAccountDao;

    @Override
    protected BaseDao<C2cShopAccount, Long> getEntityDao() {
        return null;
    }
}
