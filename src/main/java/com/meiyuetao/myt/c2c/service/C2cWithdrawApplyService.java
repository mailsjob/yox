package com.meiyuetao.myt.c2c.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.c2c.dao.C2cWithdrawApplyDao;
import com.meiyuetao.myt.c2c.entity.C2cWithdrawApply;

@Service
@Transactional
public class C2cWithdrawApplyService extends BaseService<C2cWithdrawApply, Long> {

    @Autowired
    private C2cWithdrawApplyDao c2cWithdrawApplyDao;

    @Override
    protected BaseDao<C2cWithdrawApply, Long> getEntityDao() {
        return c2cWithdrawApplyDao;
    }
}
