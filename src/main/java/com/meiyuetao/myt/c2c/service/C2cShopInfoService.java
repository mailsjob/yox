package com.meiyuetao.myt.c2c.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.c2c.dao.C2cShopInfoDao;
import com.meiyuetao.myt.c2c.entity.C2cShopInfo;

@Service
@Transactional
public class C2cShopInfoService extends BaseService<C2cShopInfo, Long> {

    @Autowired
    private C2cShopInfoDao c2cShopInfoDao;

    @Override
    protected BaseDao<C2cShopInfo, Long> getEntityDao() {
        return c2cShopInfoDao;
    }
}
