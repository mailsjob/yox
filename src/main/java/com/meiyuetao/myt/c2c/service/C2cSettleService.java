package com.meiyuetao.myt.c2c.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.c2c.dao.C2cSettleDao;
import com.meiyuetao.myt.c2c.entity.C2cSettle;

@Service
@Transactional
public class C2cSettleService extends BaseService<C2cSettle, Long> {

    @Autowired
    private C2cSettleDao c2cSettleDao;

    @Override
    protected BaseDao<C2cSettle, Long> getEntityDao() {
        return c2cSettleDao;
    }
}
