package com.meiyuetao.myt.c2c.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.c2c.dao.C2cFinanceAccountDao;
import com.meiyuetao.myt.c2c.entity.C2cFinanceAccount;

@Service
@Transactional
public class C2cFinanceAccountService extends BaseService<C2cFinanceAccount, Long> {

    @Autowired
    private C2cFinanceAccountDao c2cFinanceAccountDao;

    @Override
    protected BaseDao<C2cFinanceAccount, Long> getEntityDao() {
        return c2cFinanceAccountDao;
    }
}
