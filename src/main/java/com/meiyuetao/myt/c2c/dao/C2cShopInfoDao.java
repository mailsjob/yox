package com.meiyuetao.myt.c2c.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.c2c.entity.C2cShopInfo;

@Repository
public interface C2cShopInfoDao extends BaseDao<C2cShopInfo, Long> {

}