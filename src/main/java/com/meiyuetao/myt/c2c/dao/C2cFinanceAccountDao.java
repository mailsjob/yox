package com.meiyuetao.myt.c2c.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.c2c.entity.C2cFinanceAccount;

@Repository
public interface C2cFinanceAccountDao extends BaseDao<C2cFinanceAccount, Long> {

}