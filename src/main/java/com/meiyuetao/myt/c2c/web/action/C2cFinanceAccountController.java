package com.meiyuetao.myt.c2c.web.action;

import java.math.BigDecimal;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.c2c.entity.C2cFinanceAccount;
import com.meiyuetao.myt.c2c.entity.C2cFinanceAccount.AccountTypeCodeEnum;
import com.meiyuetao.myt.c2c.entity.C2cShopInfo;
import com.meiyuetao.myt.c2c.service.C2cFinanceAccountService;
import com.meiyuetao.myt.c2c.service.C2cShopInfoService;

@MetaData("美月淘集市财务账户管理")
public class C2cFinanceAccountController extends BaseController<C2cFinanceAccount, Long> {

    @Autowired
    private C2cFinanceAccountService c2cFinanceAccountService;

    @Autowired
    private C2cShopInfoService c2cShopInfoService;

    @Override
    protected BaseService<C2cFinanceAccount, Long> getEntityService() {
        return c2cFinanceAccountService;
    }

    @Override
    protected void checkEntityAclPermission(C2cFinanceAccount entity) {
        // TODO Add acl check code logic
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        if (groupPropertyFilter.isEmpty()) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.NU, "parent", true));
        }
        super.appendFilterProperty(groupPropertyFilter);
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        if (bindingEntity.getParent().getId() == null) {
            bindingEntity.setParent(null);
        } else if (AccountTypeCodeEnum.B02.equals(bindingEntity.getAccountTypeCode())) {
            // 保证金帐户
            C2cShopInfo shop = bindingEntity.getShop();
            shop.setCautionMoney(bindingEntity.getAmount());
            // 初始保证金1000元，对应补贴封顶额度2万元。如果无保证金，则封顶补贴额度为5000元
            if (bindingEntity.getAmount().compareTo(BigDecimal.valueOf(1000)) >= 0) {
                shop.setSubsidyCap(BigDecimal.valueOf(20000));
            } else {
                shop.setSubsidyCap(BigDecimal.valueOf(5000));
            }
            c2cShopInfoService.save(shop);
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}