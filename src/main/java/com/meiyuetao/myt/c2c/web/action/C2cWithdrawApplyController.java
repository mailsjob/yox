package com.meiyuetao.myt.c2c.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.c2c.entity.C2cWithdrawApply;
import com.meiyuetao.myt.c2c.service.C2cWithdrawApplyService;

@MetaData("美月淘店铺提现申请表管理")
public class C2cWithdrawApplyController extends BaseController<C2cWithdrawApply, Long> {

    @Autowired
    private C2cWithdrawApplyService c2cWithdrawApplyService;

    @Override
    protected BaseService<C2cWithdrawApply, Long> getEntityService() {
        return c2cWithdrawApplyService;
    }

    @Override
    protected void checkEntityAclPermission(C2cWithdrawApply entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}