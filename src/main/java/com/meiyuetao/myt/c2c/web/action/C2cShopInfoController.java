package com.meiyuetao.myt.c2c.web.action;

import java.math.BigDecimal;
import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.meiyuetao.myt.c2c.entity.C2cFinanceAccount;
import com.meiyuetao.myt.c2c.entity.C2cFinanceAccount.AccountTypeCodeEnum;
import com.meiyuetao.myt.c2c.entity.C2cShopInfo;
import com.meiyuetao.myt.c2c.service.C2cFinanceAccountService;
import com.meiyuetao.myt.c2c.service.C2cShopInfoService;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.customer.service.CustomerProfileService;

@MetaData("美月淘集市店铺管理")
public class C2cShopInfoController extends BaseController<C2cShopInfo, Long> {

    @Autowired
    private C2cShopInfoService c2cShopInfoService;

    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private C2cFinanceAccountService c2cFinanceAccountService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected BaseService<C2cShopInfo, Long> getEntityService() {
        return c2cShopInfoService;
    }

    @Override
    protected void checkEntityAclPermission(C2cShopInfo entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[从客户表导入]")
    public HttpHeaders importFromCustomer() {
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        // String customerDis = getParameter("customerProfile.display");
        // if (StringUtils.isNullOrEmpty(customerDis)) {
        // bindingEntity.setCustomerProfile(null);
        // }
        if (bindingEntity.isNew()) {
            // 新建店铺的保证金为0
            bindingEntity.setCautionMoney(BigDecimal.ZERO);
            // 保证金为0时，不支持先行赔付
            bindingEntity.setAdvancePayouts(BigDecimal.ZERO);
            // 新建店铺的补贴额为0元
            bindingEntity.setSubsidy(BigDecimal.ZERO);
            // 保证金为0时，补贴额上限为5000元
            bindingEntity.setSubsidyCap(BigDecimal.valueOf(5000));
            super.doSave();
            // 初始化店铺时，同时初始化4个账户
            C2cFinanceAccount mainAccount = new C2cFinanceAccount();
            mainAccount.setAccountTypeCode(AccountTypeCodeEnum.A00);
            mainAccount.setShop(bindingEntity);
            mainAccount.setAmount(BigDecimal.ZERO);
            mainAccount.setFrozenAmount(BigDecimal.ZERO);
            c2cFinanceAccountService.save(mainAccount);
            // 主账户余额账户
            C2cFinanceAccount overageAccount = new C2cFinanceAccount();
            overageAccount.setParent(mainAccount);
            overageAccount.setAccountTypeCode(AccountTypeCodeEnum.B00);
            overageAccount.setShop(bindingEntity);
            overageAccount.setAmount(BigDecimal.ZERO);
            overageAccount.setFrozenAmount(BigDecimal.ZERO);
            c2cFinanceAccountService.save(overageAccount);
            // 补贴账户
            C2cFinanceAccount subsidyAccount = new C2cFinanceAccount();
            subsidyAccount.setParent(mainAccount);
            subsidyAccount.setAccountTypeCode(AccountTypeCodeEnum.B01);
            subsidyAccount.setShop(bindingEntity);
            subsidyAccount.setAmount(BigDecimal.ZERO);
            subsidyAccount.setFrozenAmount(BigDecimal.ZERO);
            c2cFinanceAccountService.save(subsidyAccount);
            // 保证金账户
            C2cFinanceAccount bailAccount = new C2cFinanceAccount();
            bailAccount.setParent(mainAccount);
            bailAccount.setAccountTypeCode(AccountTypeCodeEnum.B02);
            bailAccount.setShop(bindingEntity);
            bailAccount.setAmount(BigDecimal.ZERO);
            bailAccount.setFrozenAmount(BigDecimal.ZERO);
            c2cFinanceAccountService.save(bailAccount);
            return buildDefaultHttpHeaders();
        }
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    /**
     * 判断是否该账户是否已被其他店铺选作主账户
     * 
     * @return
     */
    public HttpHeaders findByCustomer() {
        CustomerProfile customerProfile = customerProfileService.findOne(Long.valueOf(getParameter("customer")));
        List<C2cShopInfo> shops = c2cShopInfoService.findByFilter(new PropertyFilter(MatchType.EQ, "customerProfile", customerProfile));
        setModel(shops);
        return buildDefaultHttpHeaders();
    }
}