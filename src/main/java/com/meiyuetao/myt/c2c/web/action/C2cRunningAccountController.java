package com.meiyuetao.myt.c2c.web.action;

import java.util.Date;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.util.DateUtils;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.meiyuetao.myt.c2c.entity.C2cRunningAccount;
import com.meiyuetao.myt.c2c.service.C2cRunningAccountService;

@MetaData("集市流水账管理")
public class C2cRunningAccountController extends BaseController<C2cRunningAccount, Long> {

    @Autowired
    private C2cRunningAccountService c2cRunningAccountService;

    @Override
    protected BaseService<C2cRunningAccount, Long> getEntityService() {
        return c2cRunningAccountService;
    }

    @Override
    protected void checkEntityAclPermission(C2cRunningAccount entity) {
        // TODO Add acl check code logic
    }

    @MetaData("[TODO方法作用]")
    public HttpHeaders todo() {
        // TODO
        setModel(OperationResult.buildSuccessResult("TODO操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @Override
    public void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        String date = this.getParameter("date");
        String[] dates = date.split("～");
        Date dateFrom;
        Date dateTo;
        if (StringUtils.isNotBlank(date) && dates.length >= 2) {
            dateFrom = DateUtils.parseDate(dates[0].trim());
            dateTo = DateUtils.parseDate(dates[1].trim());
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.GE, "occureTime", dateFrom));
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.LT, "occureTime", dateTo));
        } else {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.LT, "occureTime", new Date()));
        }
    }

    public HttpHeaders findByDaily() {
        GroupPropertyFilter groupFilter = GroupPropertyFilter.buildFromHttpRequest(entityClass, getRequest());
        appendFilterProperty(groupFilter);
        setModel(findByGroupAggregate(groupFilter, new Sort(Direction.DESC, "occureDay"), "occureDay", "count(bizNum)", "bizNumType", "sum(amount)"));
        return buildDefaultHttpHeaders();
    }

}