package com.meiyuetao.myt.c2c.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;

import com.meiyuetao.myt.c2c.entity.C2cFinanceAccount.AccountTypeCodeEnum;

@MetaData("C2cRunningMonlyController")
public class C2cRunningMonlyController extends C2cRunningAccountController {

    public HttpHeaders findByMonly() {
        // 检索条件： 店铺名称
        String shopName = this.getParameter("c2cFinanceAccount.shop.shopName");
        if (StringUtils.isNotBlank(shopName)) {
            setModel(findByGroupAggregate("count(bizNum)", "sum(amount)", "occureYearMon", "c2cFinanceAccount.shop.shopName", "c2cFinanceAccount.accountTypeCode", "bizNumType"));
        } else {
            setModel(findByGroupAggregate("count(bizNum)", "sum(amount)", "occureYearMon", "c2cFinanceAccount.accountTypeCode", "bizNumType"));
        }
        return buildDefaultHttpHeaders();
    }

    @Override
    public void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        String isSettled = this.getParameter("isSettled");
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "isSettled", Boolean.valueOf(isSettled)));
        // 检索条件： 店铺名称
        String shopName = this.getParameter("c2cFinanceAccount.shop.shopName");
        if (StringUtils.isNotBlank(shopName)) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "c2cFinanceAccount.shop.shopName", shopName));
        }
        // 检索条件： 账户类型
        String accountTypeCode = this.getParameter("c2cFinanceAccount.accountTypeCode");
        if (StringUtils.isNotBlank(accountTypeCode)) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "c2cFinanceAccount.accountTypeCode", AccountTypeCodeEnum.valueOf(accountTypeCode)));
        }
    }
}
