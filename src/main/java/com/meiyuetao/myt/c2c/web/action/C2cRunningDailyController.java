package com.meiyuetao.myt.c2c.web.action;

import java.util.Date;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.util.DateUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;

import com.meiyuetao.myt.c2c.entity.C2cFinanceAccount.AccountTypeCodeEnum;

@MetaData("C2cRunningDailyController")
public class C2cRunningDailyController extends C2cRunningAccountController {

    public HttpHeaders findByDaily() {
        // 检索条件： 店铺名称
        String shopName = this.getParameter("c2cFinanceAccount.shop.shopName");
        if (StringUtils.isNotBlank(shopName)) {
            setModel(findByGroupAggregate("sum(amount)", "count(bizNum)", "occureDay", "c2cFinanceAccount.shop.shopName", "c2cFinanceAccount.accountTypeCode", "bizNumType"));
        } else {
            setModel(findByGroupAggregate("sum(amount)", "count(bizNum)", "occureDay", "c2cFinanceAccount.accountTypeCode", "bizNumType"));
        }

        return buildDefaultHttpHeaders();
    }

    @Override
    public void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        String date = this.getParameter("date");
        String[] dates = date.split("～");
        Date dateFrom;
        Date dateTo;
        if (StringUtils.isNotBlank(date) && dates.length >= 2) {
            dateFrom = DateUtils.parseDate(dates[0].trim());
            dateTo = DateUtils.parseDate(dates[1].trim());
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.GE, "occureTime", dateFrom));
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.LT, "occureTime", dateTo));
        } else {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.LT, "occureTime", new Date()));
        }
        String isSettled = this.getParameter("isSettled");
        groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "isSettled", Boolean.valueOf(isSettled)));
        // 检索条件： 店铺名称
        String shopName = this.getParameter("c2cFinanceAccount.shop.shopName");
        if (StringUtils.isNotBlank(shopName)) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "c2cFinanceAccount.shop.shopName", shopName));
        }
        // 检索条件： 账户类型
        String accountTypeCode = this.getParameter("c2cFinanceAccount.accountTypeCode");
        if (StringUtils.isNotBlank(accountTypeCode)) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "c2cFinanceAccount.accountTypeCode", AccountTypeCodeEnum.valueOf(accountTypeCode)));
        }
    }
}
