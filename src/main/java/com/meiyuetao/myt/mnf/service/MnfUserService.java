package com.meiyuetao.myt.mnf.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.mnf.dao.MnfUserDao;
import com.meiyuetao.myt.mnf.entity.MnfUser;

@Service
@Transactional
public class MnfUserService extends BaseService<MnfUser, Long> {

    @Autowired
    private MnfUserDao mnfUserDao;

    @Override
    protected BaseDao<MnfUser, Long> getEntityDao() {
        return mnfUserDao;
    }

}
