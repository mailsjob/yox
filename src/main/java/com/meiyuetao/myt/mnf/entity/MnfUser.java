package com.meiyuetao.myt.mnf.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.constant.GenericSexEnum;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("明年付注册用户")
@Entity
@Table(name = "mnf_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MnfUser extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    /*  *//** yushenqingedu */
    /*
     * private Long mnfMaxAccount; /**kaishihuankuannianyueyear
     */
    /*
     * private Integer refundYear;
     *//** kaishihuankuannianyuemonth */
    /*
     * private Integer refundMonth;
     *//** zhangdanri */
    /*
     * private Integer statementDate;
     *//** niguihuanyueshu */
    /*
     * private Integer refundMonthCount;
     */
    /** 预申请额度 yushenqingedu */
    private Long mnfMaxAccount;
    /** 开始还款年 kaishihuankuannianyueyear */
    private Integer refundYear;
    /** 开始还款月份 kaishihuankuannianyuemonth */
    private Integer refundMonth;
    /** 账单日 zhangdanri */
    private Integer billDay;
    /** 拟归还月数 niguihuanyueshu */
    private Integer refundMonthCount;
    /** 账户名 zhanghuming */
    private String accountNo;
    /** 姓名 xingming */
    private String trueName;
    /** 身份证号码 shenfenzhenghaoma */
    private String idCard;
    /** 性别 xingbie */
    private GenericSexEnum sex;
    /** 婚姻状况 hunyinzhuangkuang */
    /*
     * private MaritalStatusEnum maritalStatus = MaritalStatusEnum.UNMARRIED;
     * 
     * public enum MaritalStatusEnum {
     * 
     * @MetaData("未婚") UNMARRIED,
     * 
     * @MetaData("已婚") MARRIED; }
     */
    private String maritalStatus;
    /** 教育程度 jiaoyuchengdu */
    private String eduLevel;

    /*
     * public enum EduLevelEnum {
     * 
     * @MetaData(value = "专科") DZ,
     * 
     * @MetaData(value = "本科") BK,
     * 
     * @MetaData(value = "研究生") SS,
     * 
     * @MetaData(value = "博士") BS; }
     */

    /** 现居住地省 xianjuzhudiprovince */
    private String liveProvince;

    /** 现居住地市 xianjuzhudicity */
    private String liveCity;

    /** 住宅地址 zhuzhaidizhi */
    private String liveStreet;

    /** 邮编 jibenziliaoyouzhengbianma */
    private String postCode;

    /** 有无信用卡 shifouchiyouxinyongka */
    private String haveCreditCard;

    /** 信用卡数量 chiyouxinyongkashuliang */
    private Integer haveCreditCardCount;

    /** 信用卡授信总额度 xinyongkashouxinzongedu */
    private Long totalCreditCardAmount;

    /** 是否有其他未还清贷款 shifouchiyouqitaweihuanqingdaikuan */
    private String havaDebt;

    /** 未还清贷款总额 weihuanqingdaikuanzonge */
    private Long havaDebtAmount;

    /** 剩余贷款还清年 qitajiedaiquanbuhuanqingyear */
    private Long refundDebtYear;

    /** 剩余贷款还清月 qitajiedaiquanbuhuanqingmonth */
    private Long refundDebtMonth;

    /** 工作单位 况 gongzuodanwei */
    private String company;
    /** 单位所在省 danweisuozaidiprovince */
    private String companyProvince;
    /** 单位所在市 danweisuozaidicity */
    private String companyCity;
    /** 单位所在地 zhiyexinxixiangxidizhi */
    private String companyCityStreet;
    /** 单位邮政编码 zhiyexinxiyouzhengbianma */
    private String companyPostCode;
    /** 单位所在地区号 zhiyexinxidanweidianhuaquhao */
    private String companyAreaCode;
    /** 单位电话 zhiyexinxidanweidianhuahao */
    private String companyMobile;
    /** 工作部门 renzhibumen */
    private String jobDepartment;
    /** 职务类别 zhiwuleibie */
    private String jobCategory;
    /** 工作年限 xiandanweigongzuonianxian */
    private String jobYearCount;
    /** 总工作年限 zonggongzuonianxian */
    private String totalJobYearCount;
    /** 年收入 nianshouru */
    private String annualSalary;
    /** 工作职位 xianrenzhiwei */
    private String job;
    /** 公司营业性质 xianrenzhidanweixingyexingzhi */
    private String companyBusinessType;
    /** 公司类型 xianrenzhidanweiqiyexingzhi */
    private String companyType;
    /** zx真是姓名 zxxingming */
    private String zxTrueName;
    /** 与申请人直接的关系 yushenqingrenguanxi */
    private String relationWith;
    /** zx区号 zxquhao */
    private String zxAreaCode;
    /** zx号码 zxhao */
    private String zxNo;
    /** zx手机号 zxshoujihaoma */
    private String zxMobilePhone;
    /** gr手机号 grshoujihaoma */
    private String grMobilePhone;
    /** gr短信验证码 */
    private String grMsgVerifyCode;
    /** gr电子邮件 grdianziyoujian */
    private String grEmail;
    /** 婚姻状况 */
    private CustomerProfile customerProfile;

    @Column(name = "yushenqingedu")
    @JsonProperty
    public Long getMnfMaxAccount() {
        return mnfMaxAccount;
    }

    public void setMnfMaxAccount(Long mnfMaxAccount) {
        this.mnfMaxAccount = mnfMaxAccount;
    }

    @Column(name = "kaishihuankuannianyueyear")
    @JsonProperty
    public Integer getRefundYear() {
        return refundYear;
    }

    public void setRefundYear(Integer refundYear) {
        this.refundYear = refundYear;
    }

    @Column(name = "kaishihuankuannianyuemonth")
    @JsonProperty
    public Integer getRefundMonth() {
        return refundMonth;
    }

    public void setRefundMonth(Integer refundMonth) {
        this.refundMonth = refundMonth;
    }

    @Column(name = "zhangdanri")
    @JsonProperty
    public Integer getBillDay() {
        return billDay;
    }

    public void setBillDay(Integer billDay) {
        this.billDay = billDay;
    }

    @Column(name = "niguihuanyueshu")
    @JsonProperty
    public Integer getRefundMonthCount() {
        return refundMonthCount;
    }

    public void setRefundMonthCount(Integer refundMonthCount) {
        this.refundMonthCount = refundMonthCount;
    }

    @Column(name = "zhanghuming")
    @JsonProperty
    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Column(name = "xingming")
    @JsonProperty
    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    @Column(name = "shenfenzhenghaoma")
    @JsonProperty
    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "xingbie", length = 16)
    @JsonProperty
    public GenericSexEnum getSex() {
        return sex;
    }

    public void setSex(GenericSexEnum sex) {
        this.sex = sex;
    }

    @Column(name = "hunyinzhuangkuang")
    @JsonProperty
    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @Column(name = "jiaoyuchengdu")
    @JsonProperty
    public String getEduLevel() {
        return eduLevel;
    }

    public void setEduLevel(String eduLevel) {
        this.eduLevel = eduLevel;
    }

    @Column(name = "xianjuzhudiprovince")
    @JsonProperty
    public String getLiveProvince() {
        return liveProvince;
    }

    public void setLiveProvince(String liveProvince) {
        this.liveProvince = liveProvince;
    }

    @Column(name = "xianjuzhudicity")
    @JsonProperty
    public String getLiveCity() {
        return liveCity;
    }

    public void setLiveCity(String liveCity) {
        this.liveCity = liveCity;
    }

    @Column(name = "zhuzhaidizhi")
    @JsonProperty
    public String getLiveStreet() {
        return liveStreet;
    }

    public void setLiveStreet(String liveStreet) {
        this.liveStreet = liveStreet;
    }

    @Column(name = "jibenziliaoyouzhengbianma")
    @JsonProperty
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Column(name = "shifouchiyouxinyongka")
    @JsonProperty
    public String getHaveCreditCard() {
        return haveCreditCard;
    }

    public void setHaveCreditCard(String haveCreditCard) {
        this.haveCreditCard = haveCreditCard;
    }

    @Column(name = "chiyouxinyongkashuliang")
    @JsonProperty
    public Integer getHaveCreditCardCount() {
        return haveCreditCardCount;
    }

    public void setHaveCreditCardCount(Integer haveCreditCardCount) {
        this.haveCreditCardCount = haveCreditCardCount;
    }

    @Column(name = "xinyongkashouxinzongedu")
    @JsonProperty
    public Long getTotalCreditCardAmount() {
        return totalCreditCardAmount;
    }

    public void setTotalCreditCardAmount(Long totalCreditCardAmount) {
        this.totalCreditCardAmount = totalCreditCardAmount;
    }

    @Column(name = "shifouchiyouqitaweihuanqingdaikuan")
    @JsonProperty
    public String getHavaDebt() {
        return havaDebt;
    }

    public void setHavaDebt(String havaDebt) {
        this.havaDebt = havaDebt;
    }

    @Column(name = "weihuanqingdaikuanzonge")
    @JsonProperty
    public Long getHavaDebtAmount() {
        return havaDebtAmount;
    }

    public void setHavaDebtAmount(Long havaDebtAmount) {
        this.havaDebtAmount = havaDebtAmount;
    }

    @Column(name = "qitajiedaiquanbuhuanqingyear")
    @JsonProperty
    public Long getRefundDebtYear() {
        return refundDebtYear;
    }

    public void setRefundDebtYear(Long refundDebtYear) {
        this.refundDebtYear = refundDebtYear;
    }

    @Column(name = "qitajiedaiquanbuhuanqingmonth")
    @JsonProperty
    public Long getRefundDebtMonth() {
        return refundDebtMonth;
    }

    public void setRefundDebtMonth(Long refundDebtMonth) {
        this.refundDebtMonth = refundDebtMonth;
    }

    @Column(name = "gongzuodanwei")
    @JsonProperty
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Column(name = "danweisuozaidiprovince")
    @JsonProperty
    public String getCompanyProvince() {
        return companyProvince;
    }

    public void setCompanyProvince(String companyProvince) {
        this.companyProvince = companyProvince;
    }

    @Column(name = "danweisuozaidicity")
    @JsonProperty
    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    @Column(name = "zhiyexinxixiangxidizhi")
    @JsonProperty
    public String getCompanyCityStreet() {
        return companyCityStreet;
    }

    public void setCompanyCityStreet(String companyCityStreet) {
        this.companyCityStreet = companyCityStreet;
    }

    @Column(name = "zhiyexinxiyouzhengbianma")
    public String getCompanyPostCode() {
        return companyPostCode;
    }

    public void setCompanyPostCode(String companyPostCode) {
        this.companyPostCode = companyPostCode;
    }

    @Column(name = "zhiyexinxidanweidianhuaquhao")
    public String getCompanyAreaCode() {
        return companyAreaCode;
    }

    public void setCompanyAreaCode(String companyAreaCode) {
        this.companyAreaCode = companyAreaCode;
    }

    @Column(name = "zhiyexinxidanweidianhuahao")
    public String getCompanyMobile() {
        return companyMobile;
    }

    public void setCompanyMobile(String companyMobile) {
        this.companyMobile = companyMobile;
    }

    @Column(name = "renzhibumen")
    public String getJobDepartment() {
        return jobDepartment;
    }

    public void setJobDepartment(String jobDepartment) {
        this.jobDepartment = jobDepartment;
    }

    @Column(name = "zhiwuleibie")
    public String getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(String jobCategory) {
        this.jobCategory = jobCategory;
    }

    @Column(name = "xiandanweigongzuonianxian")
    public String getJobYearCount() {
        return jobYearCount;
    }

    public void setJobYearCount(String jobYearCount) {
        this.jobYearCount = jobYearCount;
    }

    @Column(name = "zonggongzuonianxian")
    public String getTotalJobYearCount() {
        return totalJobYearCount;
    }

    public void setTotalJobYearCount(String totalJobYearCount) {
        this.totalJobYearCount = totalJobYearCount;
    }

    @Column(name = "nianshouru")
    public String getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(String annualSalary) {
        this.annualSalary = annualSalary;
    }

    @Column(name = "xianrenzhiwei")
    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    @Column(name = "xianrenzhidanweixingyexingzhi")
    public String getCompanyBusinessType() {
        return companyBusinessType;
    }

    public void setCompanyBusinessType(String companyBusinessType) {
        this.companyBusinessType = companyBusinessType;
    }

    @Column(name = "xianrenzhidanweiqiyexingzhi")
    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    @Column(name = "zxxingming")
    public String getZxTrueName() {
        return zxTrueName;
    }

    public void setZxTrueName(String zxTrueName) {
        this.zxTrueName = zxTrueName;
    }

    @Column(name = "yushenqingrenguanxi")
    public String getRelationWith() {
        return relationWith;
    }

    public void setRelationWith(String relationWith) {
        this.relationWith = relationWith;
    }

    @Column(name = "zxquhao")
    public String getZxAreaCode() {
        return zxAreaCode;
    }

    public void setZxAreaCode(String zxAreaCode) {
        this.zxAreaCode = zxAreaCode;
    }

    @Column(name = "zxhao")
    public String getZxNo() {
        return zxNo;
    }

    public void setZxNo(String zxNo) {
        this.zxNo = zxNo;
    }

    @Column(name = "zxshoujihaoma")
    public String getZxMobilePhone() {
        return zxMobilePhone;
    }

    public void setZxMobilePhone(String zxMobilePhone) {
        this.zxMobilePhone = zxMobilePhone;
    }

    @Column(name = "grshoujihaoma")
    public String getGrMobilePhone() {
        return grMobilePhone;
    }

    public void setGrMobilePhone(String grMobilePhone) {
        this.grMobilePhone = grMobilePhone;
    }

    @Column(name = "grduanxinyanzhengma")
    public String getGrMsgVerifyCode() {
        return grMsgVerifyCode;
    }

    public void setGrMsgVerifyCode(String grMsgVerifyCode) {
        this.grMsgVerifyCode = grMsgVerifyCode;
    }

    @Column(name = "grdianziyoujian")
    public String getGrEmail() {
        return grEmail;
    }

    public void setGrEmail(String grEmail) {
        this.grEmail = grEmail;
    }

    @OneToOne
    @JoinColumn(name = "customer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

}
