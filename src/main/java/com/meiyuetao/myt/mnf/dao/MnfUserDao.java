package com.meiyuetao.myt.mnf.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.mnf.entity.MnfUser;

@Repository
public interface MnfUserDao extends BaseDao<MnfUser, Long> {

}