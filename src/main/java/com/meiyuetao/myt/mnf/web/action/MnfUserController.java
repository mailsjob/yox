package com.meiyuetao.myt.mnf.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.mnf.entity.MnfUser;
import com.meiyuetao.myt.mnf.service.MnfUserService;

@MetaData("明年付付注册用户管理")
public class MnfUserController extends MytBaseController<MnfUser, Long> {

    @Autowired
    private MnfUserService mnfUserService;

    @Override
    protected BaseService<MnfUser, Long> getEntityService() {
        return mnfUserService;
    }

    @Override
    protected void checkEntityAclPermission(MnfUser entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

}