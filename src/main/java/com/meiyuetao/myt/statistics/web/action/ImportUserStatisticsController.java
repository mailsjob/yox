package com.meiyuetao.myt.statistics.web.action;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import lab.s2jh.core.web.SimpleController;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.google.common.collect.Lists;
import com.meiyuetao.myt.statistics.vo.ImportUserStatisticsVo;

public class ImportUserStatisticsController extends SimpleController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public HttpHeaders count() {
        return buildDefaultHttpHeaders("count");
    }

    public HttpHeaders dispatcher() {
        String model = this.getParameter("model");
        if (StringUtils.isBlank(model)) {
            // default
            return this.one();
        }
        return null;
    }

    private HttpHeaders one() {
        String date = this.getParameter("date");
        String[] dates = date.split("～");

        MapSqlParameterSource args = new MapSqlParameterSource();
        if (StringUtils.isNotBlank(date) && dates.length >= 2) {
            args.addValue("dateFrom", new DateTime(dates[0].trim()).toString());
            args.addValue("dateTo", new DateTime(dates[1].trim()).toString());
        } else {
            DateTime now = new DateTime();

            args.addValue("dateFrom", now.minusYears(1).toString());
            args.addValue("dateTo", now.toString());
        }

        String sql = "SELECT " + "t1.registerTime, " + "t1.total, " + "t2.notActivated, " + "t1.total-t2.notActivated as activated " + "FROM " + "( " + "SELECT " + "CONVERT ( "
                + "VARCHAR (100), " + "sso.register_time, " + "23 " + ") AS registerTime, " + "COUNT (*) AS total " + "FROM " + "t_sso_user sso " + "WHERE "
                + "sso.is_sys_import = 1 " + "GROUP BY " + "CONVERT ( " + "VARCHAR (100), " + "sso.register_time, " + "23 " + ") " + ") t1 " + "LEFT JOIN ( " + "SELECT "
                + "CONVERT ( " + "VARCHAR (100), " + "sso.register_time, " + "23 " + ") AS registerTime, " + "COUNT (*) notActivated " + "FROM " + "t_sso_user sso " + "WHERE "
                + "sso.is_sys_import = 1 " + "AND sso.last_logon_time IS NULL " + "GROUP BY " + "CONVERT ( " + "VARCHAR (100), " + "sso.register_time, " + "23 " + ") "
                + ") t2 ON t1.registerTime = t2.registerTime " + "WHERE t1.registerTime>=:dateFrom and t1.registerTime<=:dateTo " + "ORDER BY t1.registerTime DESC";
        System.out.println(">>>:" + sql);
        final List<ImportUserStatisticsVo> vos = Lists.newArrayList();
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        namedParameterJdbcTemplate.query(sql.toString(), args, new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                Date registerTime = rs.getDate("registerTime");
                int total = rs.getInt("total");
                int notActivated = rs.getInt("notActivated");
                int activated = rs.getInt("activated");

                ImportUserStatisticsVo vo = new ImportUserStatisticsVo(registerTime, total, notActivated, activated);
                vos.add(vo);
            }
        });
        setModel(buildPageResultFromList(vos));
        return buildDefaultHttpHeaders();
    }

    protected <S> Page<S> buildPageResultFromList(List<S> list) {
        Page<S> page = new PageImpl<S>(list);
        return page;
    }

}