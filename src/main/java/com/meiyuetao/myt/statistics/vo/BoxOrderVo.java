package com.meiyuetao.myt.statistics.vo;

public class BoxOrderVo {
    private String commoditySku;
    private String commodityTitle;
    private Integer orderNum;
    private Integer quantity;

    public BoxOrderVo(String commoditySku, String commodityTitle, Integer orderNum, Integer quantity) {
        this.commoditySku = commoditySku;
        this.commodityTitle = commodityTitle;
        this.orderNum = orderNum;
        this.quantity = quantity;
    }

    public String getCommoditySku() {
        return commoditySku;
    }

    public void setCommoditySku(String commoditySku) {
        this.commoditySku = commoditySku;
    }

    public String getCommodityTitle() {
        return commodityTitle;
    }

    public void setCommodityTitle(String commodityTitle) {
        this.commodityTitle = commodityTitle;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
