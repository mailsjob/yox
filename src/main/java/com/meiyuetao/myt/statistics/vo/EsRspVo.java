package com.meiyuetao.myt.statistics.vo;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Rsp")
public class EsRspVo {
    @XStreamAlias("Result")
    private String result;
    @XStreamAlias("Cause")
    private String cause;

    public EsRspVo(String result, String cause) {
        this.result = result;
        this.cause = cause;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

}
