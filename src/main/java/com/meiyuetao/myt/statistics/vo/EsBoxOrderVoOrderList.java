package com.meiyuetao.myt.statistics.vo;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class EsBoxOrderVoOrderList {

    @XStreamImplicit(itemFieldName = "OrderNO")
    private List<String> orderNO;

    public List<String> getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(List<String> orderNO) {
        this.orderNO = orderNO;
    }

}
