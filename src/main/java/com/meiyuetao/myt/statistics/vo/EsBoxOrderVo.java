package com.meiyuetao.myt.statistics.vo;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Order")
public class EsBoxOrderVo {
    @XStreamAlias("OrderCount")
    private String orderCount;
    @XStreamAlias("Result")
    private String result;
    @XStreamAlias("Cause")
    private String cause;
    @XStreamAlias("Page")
    private String page;
    @XStreamAlias("OrderList")
    private EsBoxOrderVoOrderList orderList;

    public EsBoxOrderVo(String orderCount, String result, String cause, String page) {
        this.orderCount = orderCount;
        this.result = result;
        this.cause = cause;
        this.page = page;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(String orderCount) {
        this.orderCount = orderCount;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public EsBoxOrderVoOrderList getOrderList() {
        return orderList;
    }

    public void setOrderList(EsBoxOrderVoOrderList orderList) {
        this.orderList = orderList;
    }

}
