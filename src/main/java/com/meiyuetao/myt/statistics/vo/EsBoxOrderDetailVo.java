package com.meiyuetao.myt.statistics.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.meiyuetao.myt.annotation.XStreamCDATA;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Order")
public class EsBoxOrderDetailVo {
    @XStreamAlias("OrderNO")
    private String orderNO;
    @XStreamAlias("Result")
    private String result;
    @XStreamAlias("Cause")
    private String cause;
    @XStreamAlias("DateTime")
    private String dateTime;
    @XStreamAlias("BuyerID")
    @XStreamCDATA
    private String buyerID;
    @XStreamAlias("BuyerName")
    @XStreamCDATA
    private String buyerName;
    @XStreamAlias("Country")
    @XStreamCDATA
    private String country;
    @XStreamAlias("Province")
    @XStreamCDATA
    private String province;
    @XStreamAlias("City")
    @XStreamCDATA
    private String city;
    @XStreamAlias("Town")
    @XStreamCDATA
    private String town;
    @XStreamAlias("Adr")
    @XStreamCDATA
    private String adr;

    @XStreamAlias("Zip")
    @XStreamCDATA
    private String zip;

    @XStreamAlias("Email")
    @XStreamCDATA
    private String email;

    @XStreamAlias("Phone")
    @XStreamCDATA
    private String phone;
    @XStreamAlias("Total")
    private BigDecimal total;
    @XStreamAlias("logisticsName")
    @XStreamCDATA
    private String logisticsName;
    @XStreamAlias("chargetype")
    @XStreamCDATA
    private String chargetype;

    @XStreamAlias("PayAccount")
    @XStreamCDATA
    private String payAccount;

    @XStreamAlias("PayID")
    @XStreamCDATA
    private String payID;
    @XStreamAlias("Postage")
    private String postage;
    @XStreamAlias("CustomerRemark")
    @XStreamCDATA
    private String customerRemark;
    @XStreamAlias("InvoiceTitle")
    @XStreamCDATA
    private String invoiceTitle;
    @XStreamAlias("Remark")
    @XStreamCDATA
    private String remark;
    @XStreamImplicit(itemFieldName = "Item")
    private List<EsBoxOrderDetailCommodityVo> item = new ArrayList<EsBoxOrderDetailCommodityVo>();

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getBuyerID() {
        return buyerID;
    }

    public void setBuyerID(String buyerID) {
        this.buyerID = buyerID;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getAdr() {
        return adr;
    }

    public void setAdr(String adr) {
        this.adr = adr;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getPostage() {
        return postage;
    }

    public void setPostage(String postage) {
        this.postage = postage;
    }

    public String getPayAccount() {
        return payAccount;
    }

    public void setPayAccount(String payAccount) {
        this.payAccount = payAccount;
    }

    public String getPayID() {
        return payID;
    }

    public void setPayID(String payID) {
        this.payID = payID;
    }

    public String getLogisticsName() {
        return logisticsName;
    }

    public void setLogisticsName(String logisticsName) {
        this.logisticsName = logisticsName;
    }

    public String getChargetype() {
        return chargetype;
    }

    public void setChargetype(String chargetype) {
        this.chargetype = chargetype;
    }

    public String getCustomerRemark() {
        return customerRemark;
    }

    public void setCustomerRemark(String customerRemark) {
        this.customerRemark = customerRemark;
    }

    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<EsBoxOrderDetailCommodityVo> getItem() {
        return item;
    }

    public void setItem(List<EsBoxOrderDetailCommodityVo> item) {
        this.item = item;
    }

}
