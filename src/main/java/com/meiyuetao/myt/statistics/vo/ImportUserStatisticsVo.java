package com.meiyuetao.myt.statistics.vo;

import java.util.Date;

public class ImportUserStatisticsVo {
    private Date registerTime;
    private Integer total;
    private Integer notActivated;
    private Integer activated;

    public ImportUserStatisticsVo() {
        // TODO Auto-generated constructor stub
    }

    public ImportUserStatisticsVo(Date registerTime, Integer total, Integer notActivated, Integer activated) {
        super();
        this.registerTime = registerTime;
        this.total = total;
        this.notActivated = notActivated;
        this.activated = activated;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getNotActivated() {
        return notActivated;
    }

    public void setNotActivated(Integer notActivated) {
        this.notActivated = notActivated;
    }

    public Integer getActivated() {
        return activated;
    }

    public void setActivated(Integer activated) {
        this.activated = activated;
    }

}
