package com.meiyuetao.myt.statistics.vo;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

import com.meiyuetao.myt.annotation.XStreamCDATA;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class EsBoxOrderDetailCommodityVo {
    @XStreamAlias("GoodsID")
    private String goodsID;
    @XStreamAlias("GoodsName")
    @XStreamCDATA
    private String goodsName;
    @XStreamAlias("Price")
    private BigDecimal price;
    @XStreamAlias("GoodsSpec")
    @XStreamCDATA
    private String goodsSpec;
    @XStreamAlias("Count")
    private Integer count;

    public EsBoxOrderDetailCommodityVo(String goodsID, String goodsName, String goodsSpec, Integer count, BigDecimal price) {
        this.goodsID = goodsID;
        if (StringUtils.isNotBlank(goodsName)) {
            this.goodsName = "<![CDATA[" + goodsName + "]]>";
        } else {
            this.goodsName = "<![CDATA[" + "" + "]]>";
        }
        if (StringUtils.isNotBlank(goodsSpec)) {
            this.goodsSpec = "<![CDATA[" + goodsSpec + "]]>";
        } else {
            this.goodsSpec = "<![CDATA[" + "" + "]]>";
        }
        this.count = count;
        this.price = price;

    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}
