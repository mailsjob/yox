package com.meiyuetao.myt.crawl.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.crawl.entity.ParsePropTrack;

@Repository
public interface ParsePropTrackDao extends BaseDao<ParsePropTrack, Long> {

}