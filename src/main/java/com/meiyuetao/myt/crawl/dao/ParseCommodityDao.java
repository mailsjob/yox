package com.meiyuetao.myt.crawl.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.crawl.entity.ParseCommodity;

@Repository
public interface ParseCommodityDao extends BaseDao<ParseCommodity, Long> {

}