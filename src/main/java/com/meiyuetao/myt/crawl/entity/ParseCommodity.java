package com.meiyuetao.myt.crawl.entity;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.util.DateUtils;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Brand;
import com.meiyuetao.myt.md.entity.Category;

/**
 * Auto Generate Javadoc
 */
@MetaData("爬虫商品")
@Entity
@Table(name = "t_crawl_parse_commodity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ParseCommodity extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("商品标识")
    private String uid;
    @MetaData("商品描述")
    private String description;
    @MetaData("商品sku")
    private String sourceCode;
    @MetaData("商品标题")
    private String title;
    @MetaData("商品链接")
    private String baseUrl;
    @MetaData("市场价")
    private BigDecimal marketPrice;
    @MetaData("销售价")
    private BigDecimal salePrice;
    @MetaData("分类路径")
    private String categoryPath;
    @MetaData("")
    private String averageReview;
    private String lastReview;
    @MetaData("商品来源")
    private String sourceType;
    @MetaData("库存信息")
    private String stockState;
    @MetaData("促销信息")
    private String salePrompt;
    @MetaData("备注")
    private String sysMemo;
    @MetaData("首次抓取时间Long型")
    private Long firstFetchTime;
    @MetaData("首次抓取时间String型")
    private String firstFetchTimeLabel;
    @MetaData("最近抓取时间Long型")
    private Long lastFetchTime;
    @MetaData("最佳抓取时间String型")
    private String lastFetchTimeLabel;

    @MetaData("标识操作组，如FORCE_REBUILD")
    private String operationGroup = "PARSING";
    @MetaData("导出的标题")
    private String targetTitle;
    @MetaData("导出的销售价")
    private BigDecimal targetSalePrice;
    @MetaData("导出的品牌")
    private Brand targetBrand;
    @MetaData("导出的品牌的说明")
    private String targetBrandExplain;
    @MetaData("导出的分类")
    private Category targetCategory;
    @MetaData("导出的分类的说明")
    private String targetCategoryExplain;
    @MetaData("最大匹配数")
    private Float matchMaxScore;
    @MetaData("匹配说明")
    private String matchDocs;
    @MetaData("最近导入时间")
    private Date lastImportTime;

    @MetaData("隐藏标记")
    private Boolean hiddenFlag = Boolean.FALSE;
    @MetaData("橱窗图个数")
    private Integer winImgNum;
    @MetaData("描述图个数")
    private Integer descImgNum;

    public ParseCommodity() {
    }

    public ParseCommodity(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Lob
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(length = 512)
    @JsonProperty
    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    @Column(length = 512)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 512)
    @JsonProperty
    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Column(length = 18, scale = 2)
    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    @Column(length = 18, scale = 2)
    @JsonProperty
    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    @Column(length = 512)
    @JsonProperty
    public String getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
    }

    @Column(length = 512)
    public String getAverageReview() {
        return averageReview;
    }

    public void setAverageReview(String averageReview) {
        this.averageReview = averageReview;
    }

    @Column(length = 512)
    public String getLastReview() {
        return lastReview;
    }

    public void setLastReview(String lastReview) {
        this.lastReview = lastReview;
    }

    @Column(length = 512)
    @JsonProperty
    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    @Column(length = 512)
    @JsonProperty
    public String getStockState() {
        return stockState;
    }

    public void setStockState(String stockState) {
        this.stockState = stockState;
    }

    @Column(length = 512)
    @JsonProperty
    public String getSalePrompt() {
        return salePrompt;
    }

    public void setSalePrompt(String salePrompt) {
        this.salePrompt = salePrompt;
    }

    @Column(length = 512)
    @JsonProperty
    public String getSysMemo() {
        return sysMemo;
    }

    public void setSysMemo(String sysMemo) {
        this.sysMemo = sysMemo;
    }

    @Column(length = 512)
    @JsonProperty
    public String getTargetTitle() {
        return targetTitle;
    }

    public void setTargetTitle(String targetTitle) {
        this.targetTitle = targetTitle;
    }

    @Column(length = 18, scale = 2)
    @JsonProperty
    public BigDecimal getTargetSalePrice() {
        return targetSalePrice;
    }

    public void setTargetSalePrice(BigDecimal targetSalePrice) {
        this.targetSalePrice = targetSalePrice;
    }

    public Long getFirstFetchTime() {
        return firstFetchTime;
    }

    public void setFirstFetchTime(Long firstFetchTime) {
        this.firstFetchTime = firstFetchTime;
    }

    @Column(length = 128)
    public String getFirstFetchTimeLabel() {
        return firstFetchTimeLabel;
    }

    public void setFirstFetchTimeLabel(String firstFetchTimeLabel) {
        this.firstFetchTimeLabel = firstFetchTimeLabel;
    }

    public Long getLastFetchTime() {
        return lastFetchTime;
    }

    public void setLastFetchTime(Long lastFetchTime) {
        this.lastFetchTime = lastFetchTime;
    }

    @Column(length = 128)
    @JsonProperty
    public String getLastFetchTimeLabel() {
        return lastFetchTimeLabel;
    }

    public void setLastFetchTimeLabel(String lastFetchTimeLabel) {
        this.lastFetchTimeLabel = lastFetchTimeLabel;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "brand_SID", nullable = true)
    @JsonProperty
    public Brand getTargetBrand() {
        return targetBrand;
    }

    public void setTargetBrand(Brand targetBrand) {
        this.targetBrand = targetBrand;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "category_sid", nullable = true)
    @JsonProperty
    public Category getTargetCategory() {
        return targetCategory;
    }

    public void setTargetCategory(Category targetCategory) {
        this.targetCategory = targetCategory;
    }

    public String getTargetBrandExplain() {
        return targetBrandExplain;
    }

    public void setTargetBrandExplain(String targetBrandExplain) {
        this.targetBrandExplain = targetBrandExplain;
    }

    public String getTargetCategoryExplain() {
        return targetCategoryExplain;
    }

    public void setTargetCategoryExplain(String targetCategoryExplain) {
        this.targetCategoryExplain = targetCategoryExplain;
    }

    public String getOperationGroup() {
        return operationGroup;
    }

    public void setOperationGroup(String operationGroup) {
        this.operationGroup = operationGroup;
    }

    @JsonProperty
    public Boolean getHiddenFlag() {
        return hiddenFlag;
    }

    public void setHiddenFlag(Boolean hiddenFlag) {
        this.hiddenFlag = hiddenFlag;
    }

    @JsonProperty
    public Float getMatchMaxScore() {
        return matchMaxScore;
    }

    public void setMatchMaxScore(Float matchMaxScore) {
        this.matchMaxScore = matchMaxScore;
    }

    @Column(length = 512)
    @JsonProperty
    public String getMatchDocs() {
        return matchDocs;
    }

    public void setMatchDocs(String matchDocs) {
        this.matchDocs = matchDocs;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getLastImportTime() {
        return lastImportTime;
    }

    public void setLastImportTime(Date lastImportTime) {
        this.lastImportTime = lastImportTime;
    }

    @JsonProperty
    public Integer getWinImgNum() {
        return winImgNum;
    }

    public void setWinImgNum(Integer winImgNum) {
        this.winImgNum = winImgNum;
    }

    @JsonProperty
    public Integer getDescImgNum() {
        return descImgNum;
    }

    public void setDescImgNum(Integer descImgNum) {
        this.descImgNum = descImgNum;
    }

    @Transient
    public String getLastImportTimeLabel() {

        return DateUtils.formatTime(this.lastImportTime).toString();
    }

    @Transient
    public String getEncodeBaseUrl() {
        return URLEncoder.encode(baseUrl);
    }

    public void addSysMemo(String sysMemo) {
        this.sysMemo += sysMemo + ";";
    }

    public void addDescription(String description) {
        this.description += description;
    }

    public void reset() {
        this.description = "";
        this.sysMemo = "";
        this.stockState = "";
    }

    @Column(name = "id")
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}