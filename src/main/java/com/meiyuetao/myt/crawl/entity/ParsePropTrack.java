package com.meiyuetao.myt.crawl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

/**
 * Auto Generate Javadoc
 */
@MetaData("爬虫商品的属性")
@Entity
@Table(name = "t_crawl_parseprop_track")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ParsePropTrack extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("属性标识")
    private String uid;
    @MetaData("属性value")
    private String propValue;
    @MetaData("商品链接")
    private String baseUrl;
    @MetaData("属性key")
    private String propKey;
    @MetaData("所属属性分组")
    private String propGroup;
    @MetaData("属性标签")
    private String propLabel;
    @MetaData("抓取时间Long型")
    private Long fetchTime;
    @MetaData("抓取时间String型")
    private String fetchTimeLabel;
    @MetaData("subKey")
    private String propSubKey;
    @MetaData("备注")
    private String trackMemo;
    @MetaData("排序号")
    private String orderRank;

    public ParsePropTrack() {

    }

    public ParsePropTrack(String baseUrl, String propKey) {
        this.baseUrl = baseUrl;
        this.propKey = propKey;
    }

    @Column(length = 512)
    @JsonProperty
    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    @Column(length = 512)
    @JsonProperty
    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Column(length = 512)
    @JsonProperty
    public String getPropKey() {
        return propKey;
    }

    public void setPropKey(String propKey) {
        this.propKey = propKey;
    }

    @JsonProperty
    public String getPropLabel() {
        return propLabel;
    }

    public void setPropLabel(String propLabel) {
        this.propLabel = propLabel;
    }

    public Long getFetchTime() {
        return fetchTime;
    }

    public void setFetchTime(Long fetchTime) {
        this.fetchTime = fetchTime;
    }

    @Column(length = 512)
    @JsonProperty
    public String getPropSubKey() {
        return propSubKey;
    }

    public void setPropSubKey(String propSubKey) {
        this.propSubKey = propSubKey;
    }

    public String getTrackMemo() {
        return trackMemo;
    }

    public void setTrackMemo(String trackMemo) {
        this.trackMemo = trackMemo;
    }

    @Column(length = 128)
    @JsonProperty
    public String getFetchTimeLabel() {
        return fetchTimeLabel;
    }

    public void setFetchTimeLabel(String fetchTimeLabel) {
        this.fetchTimeLabel = fetchTimeLabel;
    }

    @Column(length = 512)
    @JsonProperty
    public String getPropGroup() {
        return propGroup;
    }

    public void setPropGroup(String propGroup) {
        this.propGroup = propGroup;
    }

    @Column(length = 512)
    @JsonProperty
    public String getOrderRank() {
        return orderRank;
    }

    public void setOrderRank(String orderRank) {
        this.orderRank = orderRank;
    }

    @Column(name = "id")
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}