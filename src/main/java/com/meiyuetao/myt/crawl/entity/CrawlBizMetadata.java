package com.meiyuetao.myt.crawl.entity;

public interface CrawlBizMetadata {

    public static final String COMMODITY_PRICES = "commodity.prices";

    public static final String COMMODITY_STOCKS = "commodity.stocks";

    public static final String COMMODITY_SALE_PROMPTS = "commodity.sale.prompts";

    public static final String COMMODITY_WINDOW_IMGS = "commodity.window.imgs";

    public static final String COMMODITY_DESCRIPTION_IMGS = "commodity.description.imgs";

    public static final String COMMODITY_REVIEWS = "commodity.reviews";
}
