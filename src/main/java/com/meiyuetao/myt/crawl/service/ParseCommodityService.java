package com.meiyuetao.myt.crawl.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.crawl.dao.ParseCommodityDao;
import com.meiyuetao.myt.crawl.entity.ParseCommodity;

@Service
@Transactional
public class ParseCommodityService extends BaseService<ParseCommodity, Long> {

    @Autowired
    private ParseCommodityDao parseCommodityDao;

    @Override
    public BaseDao<ParseCommodity, Long> getEntityDao() {
        return parseCommodityDao;
    }

}
