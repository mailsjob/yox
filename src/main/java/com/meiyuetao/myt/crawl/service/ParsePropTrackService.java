package com.meiyuetao.myt.crawl.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.crawl.dao.ParsePropTrackDao;
import com.meiyuetao.myt.crawl.entity.ParsePropTrack;

@Service
@Transactional
public class ParsePropTrackService extends BaseService<ParsePropTrack, Long> {

    @Autowired
    private ParsePropTrackDao parsePropTrackDao;

    @Override
    protected BaseDao<ParsePropTrack, Long> getEntityDao() {
        return parsePropTrackDao;
    }

}
