package com.meiyuetao.myt.activity.web.action;

import java.util.Collection;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.activity.entity.ActivityRules;
import com.meiyuetao.myt.activity.service.ActivityRulesService;

@MetaData("活动规则管理")
public class ActivityRulesController extends BaseController<ActivityRules, Long> {

    @Autowired
    private ActivityRulesService activityRulesService;

    @Override
    protected BaseService<ActivityRules, Long> getEntityService() {
        return activityRulesService;
    }

    @Override
    protected void checkEntityAclPermission(ActivityRules entity) {
        // TODO Add acl check code logic
    }

    @MetaData("选择规则保存")
    public HttpHeaders doChooseRules() {
        ActivityRules activityRules = activityRulesService.findOne(bindingEntity.getRuleId());
        activityRules.setActivity(bindingEntity.getActivity());
        activityRulesService.save(activityRules);
        setModel(OperationResult.buildSuccessResult("操作完成"));
        return buildDefaultHttpHeaders();
    }

    @MetaData("去除规则保存")
    public HttpHeaders doDelRules() {
        Collection<ActivityRules> entities = this.getEntitiesByParameterIds();
        for (ActivityRules entity : entities) {
            ActivityRules activityRules = activityRulesService.findOne(entity.getId());
            activityRules.setActivity(null);
            activityRulesService.save(activityRules);
        }
        setModel(OperationResult.buildSuccessResult("操作完成"));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}