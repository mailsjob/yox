package com.meiyuetao.myt.activity.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.activity.entity.RuleTemplate;
import com.meiyuetao.myt.activity.service.RuleTemplateService;

@MetaData("活动规则模板管理")
public class RuleTemplateController extends BaseController<RuleTemplate, Long> {

    @Autowired
    private RuleTemplateService ruleTemplateService;

    @Override
    protected BaseService<RuleTemplate, Long> getEntityService() {
        return ruleTemplateService;
    }

    @Override
    protected void checkEntityAclPermission(RuleTemplate entity) {
        // TODO Add acl check code logic
    }

    @MetaData("获取模板抬头")
    public HttpHeaders findById() {
        RuleTemplate ruleTemplate = ruleTemplateService.findByProperty("id", bindingEntity.getId());
        setModel(ruleTemplate);
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}