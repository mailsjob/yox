package com.meiyuetao.myt.activity.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.activity.entity.ActivityRulesR2Collection;
import com.meiyuetao.myt.activity.service.ActivityRulesR2CollectionService;

@MetaData("活动规则to集合管理")
public class ActivityRulesR2CollectionController extends BaseController<ActivityRulesR2Collection, Long> {

    @Autowired
    private ActivityRulesR2CollectionService activityRulesR2CollectionService;

    @Override
    protected BaseService<ActivityRulesR2Collection, Long> getEntityService() {
        return activityRulesR2CollectionService;
    }

    @Override
    protected void checkEntityAclPermission(ActivityRulesR2Collection entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}