package com.meiyuetao.myt.activity.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.activity.dao.ActivityRulesR2CollectionDao;
import com.meiyuetao.myt.activity.entity.ActivityRulesR2Collection;

@Service
@Transactional
public class ActivityRulesR2CollectionService extends BaseService<ActivityRulesR2Collection, Long> {

    @Autowired
    private ActivityRulesR2CollectionDao activityRulesR2CollectionDao;

    @Override
    protected BaseDao<ActivityRulesR2Collection, Long> getEntityDao() {
        return activityRulesR2CollectionDao;
    }
}
