package com.meiyuetao.myt.activity.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.activity.dao.RuleTemplateDao;
import com.meiyuetao.myt.activity.entity.RuleTemplate;

@Service
@Transactional
public class RuleTemplateService extends BaseService<RuleTemplate, Long> {

    @Autowired
    private RuleTemplateDao ruleTemplateDao;

    @Override
    protected BaseDao<RuleTemplate, Long> getEntityDao() {
        return ruleTemplateDao;
    }

}
