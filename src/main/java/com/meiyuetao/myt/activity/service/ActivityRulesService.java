package com.meiyuetao.myt.activity.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.activity.dao.ActivityRulesDao;
import com.meiyuetao.myt.activity.entity.ActivityRules;

@Service
@Transactional
public class ActivityRulesService extends BaseService<ActivityRules, Long> {

    @Autowired
    private ActivityRulesDao activityRulesDao;

    @Override
    protected BaseDao<ActivityRules, Long> getEntityDao() {
        return activityRulesDao;
    }
}
