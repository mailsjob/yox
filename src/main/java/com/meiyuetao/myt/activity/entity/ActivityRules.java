package com.meiyuetao.myt.activity.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Activity;

@MetaData("活动规则")
@Entity
@Table(name = "myt_activity_rules")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ActivityRules extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联活动")
    private Activity activity;
    @MetaData("是否可用")
    private Boolean isEnable = false;
    @MetaData("规则开始时间")
    private Date startTime;
    @MetaData("规则结束时间")
    private Date endTime;
    private Long ruleId;
    @MetaData("规则名称")
    private String ruleTitle;
    @MetaData("规则模板")
    private RuleTemplate ruleTemplate;
    @MetaData("是否可以叠加")
    private Boolean isOver = false;
    @MetaData("规则排序")
    private Integer ruleIndex;
    @MetaData("适宜模块")
    private FitModuleEnum fitModule;
    @MetaData("能否使用优惠券")
    private Boolean canUseGiftpaper = false;
    @MetaData("规则一")
    private BigDecimal data1Value;
    @MetaData("规则二")
    private BigDecimal data2Value;
    @MetaData("规则三")
    private BigDecimal data3Value;
    @MetaData("规则四")
    private BigInteger data4Value;
    @MetaData("规则五")
    private String data5Value;
    @MetaData("规则六")
    private BigDecimal data6Value;
    @MetaData("规则七")
    private BigDecimal data7Value;
    @MetaData("规则八")
    private BigDecimal data8Value;
    @MetaData("规则九")
    private BigInteger data9Value;
    @MetaData("规则十")
    private String data10Value;

    public enum FitModuleEnum {
        @MetaData("闪购")
        SG,

        @MetaData("周期购")
        ZQG,

        @MetaData("普通购买")
        PTGM,

        @MetaData("单品")
        DP;
    }

    @OneToOne
    @JoinColumn(name = "activity_sid")
    @JsonProperty
    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Column(name = "is_enable", nullable = false)
    @JsonProperty
    public Boolean getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    @Column(name = "start_time")
    @JsonProperty
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Column(name = "end_time")
    @JsonProperty
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Transient
    @JsonProperty
    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    @Column(name = "rule_title")
    @JsonProperty
    public String getRuleTitle() {
        return ruleTitle;
    }

    public void setRuleTitle(String ruleTitle) {
        this.ruleTitle = ruleTitle;
    }

    @OneToOne
    @JoinColumn(name = "template_sid", nullable = false)
    @JsonProperty
    public RuleTemplate getRuleTemplate() {
        return ruleTemplate;
    }

    public void setRuleTemplate(RuleTemplate ruleTemplate) {
        this.ruleTemplate = ruleTemplate;
    }

    @Column(name = "is_over")
    @JsonProperty
    public Boolean getIsOver() {
        return isOver;
    }

    public void setIsOver(Boolean isOver) {
        this.isOver = isOver;
    }

    @Column(name = "rule_index")
    @JsonProperty
    public Integer getRuleIndex() {
        return ruleIndex;
    }

    public void setRuleIndex(Integer ruleIndex) {
        this.ruleIndex = ruleIndex;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "fit_module", length = 255)
    @JsonProperty
    public FitModuleEnum getFitModule() {
        return fitModule;
    }

    public void setFitModule(FitModuleEnum fitModule) {
        this.fitModule = fitModule;
    }

    @Column(name = "can_use_gift_paper")
    @JsonProperty
    public Boolean getCanUseGiftpaper() {
        return canUseGiftpaper;
    }

    public void setCanUseGiftpaper(Boolean canUseGiftpaper) {
        this.canUseGiftpaper = canUseGiftpaper;
    }

    @Column(name = "data1_value", length = 19)
    @JsonProperty
    public BigDecimal getData1Value() {
        return data1Value;
    }

    public void setData1Value(BigDecimal data1Value) {
        this.data1Value = data1Value;
    }

    @Column(name = "data2_value", length = 19)
    @JsonProperty
    public BigDecimal getData2Value() {
        return data2Value;
    }

    public void setData2Value(BigDecimal data2Value) {
        this.data2Value = data2Value;
    }

    @Column(name = "data3_value", length = 19)
    @JsonProperty
    public BigDecimal getData3Value() {
        return data3Value;
    }

    public void setData3Value(BigDecimal data3Value) {
        this.data3Value = data3Value;
    }

    @Column(name = "data4_value", length = 19)
    @JsonProperty
    public BigInteger getData4Value() {
        return data4Value;
    }

    public void setData4Value(BigInteger data4Value) {
        this.data4Value = data4Value;
    }

    @Column(name = "data5_value", length = 19)
    @JsonProperty
    public String getData5Value() {
        return data5Value;
    }

    public void setData5Value(String data5Value) {
        this.data5Value = data5Value;
    }

    @Column(name = "data6_value", length = 19)
    @JsonProperty
    public BigDecimal getData6Value() {
        return data6Value;
    }

    public void setData6Value(BigDecimal data6Value) {
        this.data6Value = data6Value;
    }

    @Column(name = "data7_value", length = 19)
    @JsonProperty
    public BigDecimal getData7Value() {
        return data7Value;
    }

    public void setData7Value(BigDecimal data7Value) {
        this.data7Value = data7Value;
    }

    @Column(name = "data8_value", length = 19)
    @JsonProperty
    public BigDecimal getData8Value() {
        return data8Value;
    }

    public void setData8Value(BigDecimal data8Value) {
        this.data8Value = data8Value;
    }

    @Column(name = "data9_value", length = 19)
    @JsonProperty
    public BigInteger getData9Value() {
        return data9Value;
    }

    public void setData9Value(BigInteger data9Value) {
        this.data9Value = data9Value;
    }

    @Column(name = "data10_value", length = 19)
    @JsonProperty
    public String getData10Value() {
        return data10Value;
    }

    public void setData10Value(String data10Value) {
        this.data10Value = data10Value;
    }

}
