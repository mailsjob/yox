package com.meiyuetao.myt.activity.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("活动规则模板")
@Entity
@Table(name = "myt_rule_template")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RuleTemplate extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("模板名称")
    private String title;
    @MetaData("模板代码")
    private String templateCode;
    @MetaData("模板一")
    private String data1Title;
    @MetaData("模板二")
    private String data2Title;
    @MetaData("模板三")
    private String data3Title;
    @MetaData("模板四")
    private String data4Title;
    @MetaData("模板五")
    private String data5Title;
    @MetaData("模板六")
    private String data6Title;
    @MetaData("模板七")
    private String data7Title;
    @MetaData("模板八")
    private String data8Title;
    @MetaData("模板九")
    private String data9Title;
    @MetaData("模板十")
    private String data10Title;

    @Column(name = "title", length = 64)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "template_code", length = 32)
    @JsonProperty
    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    @Column(name = "data1_title", length = 16)
    @JsonProperty
    public String getData1Title() {
        return data1Title;
    }

    public void setData1Title(String data1Title) {
        this.data1Title = data1Title;
    }

    @Column(name = "data2_title", length = 16)
    @JsonProperty
    public String getData2Title() {
        return data2Title;
    }

    public void setData2Title(String data2Title) {
        this.data2Title = data2Title;
    }

    @Column(name = "data3_title", length = 16)
    @JsonProperty
    public String getData3Title() {
        return data3Title;
    }

    public void setData3Title(String data3Title) {
        this.data3Title = data3Title;
    }

    @Column(name = "data4_title", length = 16)
    @JsonProperty
    public String getData4Title() {
        return data4Title;
    }

    public void setData4Title(String data4Title) {
        this.data4Title = data4Title;
    }

    @Column(name = "data5_title", length = 16)
    @JsonProperty
    public String getData5Title() {
        return data5Title;
    }

    public void setData5Title(String data5Title) {
        this.data5Title = data5Title;
    }

    @Column(name = "data6_title", length = 16)
    @JsonProperty
    public String getData6Title() {
        return data6Title;
    }

    public void setData6Title(String data6Title) {
        this.data6Title = data6Title;
    }

    @Column(name = "data7_title", length = 16)
    @JsonProperty
    public String getData7Title() {
        return data7Title;
    }

    public void setData7Title(String data7Title) {
        this.data7Title = data7Title;
    }

    @Column(name = "data8_title", length = 16)
    @JsonProperty
    public String getData8Title() {
        return data8Title;
    }

    public void setData8Title(String data8Title) {
        this.data8Title = data8Title;
    }

    @Column(name = "data9_title", length = 16)
    @JsonProperty
    public String getData9Title() {
        return data9Title;
    }

    public void setData9Title(String data9Title) {
        this.data9Title = data9Title;
    }

    @Column(name = "data10_title", length = 16)
    @JsonProperty
    public String getData10Title() {
        return data10Title;
    }

    public void setData10Title(String data10Title) {
        this.data10Title = data10Title;
    }

}
