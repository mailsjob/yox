package com.meiyuetao.myt.activity.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Collection;

@MetaData("活动规则to集合")
@Entity
@Table(name = "myt_activity_rules_r2_collection")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ActivityRulesR2Collection extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("活动规则")
    private ActivityRules activityRules;
    @MetaData("集合")
    private Collection collection;
    @MetaData("白名单或黑名单")
    private WhiteOrBlackEnum whiteOrBlack = WhiteOrBlackEnum.WHITE;

    public enum WhiteOrBlackEnum {
        @MetaData("白名单")
        WHITE,

        @MetaData("黑名单")
        BLACK;
    }

    @OneToOne
    @JoinColumn(name = "rule_sid", nullable = false)
    @JsonProperty
    public ActivityRules getActivityRules() {
        return activityRules;
    }

    public void setActivityRules(ActivityRules activityRules) {
        this.activityRules = activityRules;
    }

    @OneToOne
    @JoinColumn(name = "collection_sid", nullable = false)
    @JsonProperty
    public Collection getCollection() {
        return collection;
    }

    public void setCollection(Collection collection) {
        this.collection = collection;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "white_or_black")
    @JsonProperty
    public WhiteOrBlackEnum getWhiteOrBlack() {
        return whiteOrBlack;
    }

    public void setWhiteOrBlack(WhiteOrBlackEnum whiteOrBlack) {
        this.whiteOrBlack = whiteOrBlack;
    }
}
