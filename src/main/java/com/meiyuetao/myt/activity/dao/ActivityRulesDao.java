package com.meiyuetao.myt.activity.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.activity.entity.ActivityRules;

@Repository
public interface ActivityRulesDao extends BaseDao<ActivityRules, Long> {

}