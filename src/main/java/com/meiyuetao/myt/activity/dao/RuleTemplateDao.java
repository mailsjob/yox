package com.meiyuetao.myt.activity.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.activity.entity.RuleTemplate;

@Repository
public interface RuleTemplateDao extends BaseDao<RuleTemplate, Long> {

}
