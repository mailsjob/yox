package com.meiyuetao.myt.activity.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.activity.entity.ActivityRulesR2Collection;

@Repository
public interface ActivityRulesR2CollectionDao extends BaseDao<ActivityRulesR2Collection, Long> {

}