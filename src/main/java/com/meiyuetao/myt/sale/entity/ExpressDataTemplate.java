package com.meiyuetao.myt.sale.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.entity.annotation.SkipParamBind;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.partner.entity.Partner;

@MetaData("快递发货数据模板")
@Entity
@Table(name = "myt_express_data_template")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ExpressDataTemplate extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("模板名")
    private String templateName;
    @MetaData("发货人")
    private String senderName;
    @MetaData("发货电话")
    private String senderMobile;
    @MetaData("发货单位")
    private String senderCompany;
    @MetaData("发货地址")
    private String senderAddr;
    @MetaData("发货邮编")
    private String senderPostcode;
    @MetaData("始发地")
    private String senderOrigin;
    @MetaData("收货人")
    private String receiveName;
    @MetaData("收货电话")
    private String receiveMobile;
    @MetaData("收货单位")
    private String receiveCompany;
    @MetaData("收货地址")
    private String receiveAddr;
    @MetaData("收货邮编")
    private String receivePostcode;
    @MetaData("目的地")
    private String receiveOrigin;

    @MetaData("分销代理商")
    private Partner agentPartner;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "agent_partner_sid")
    @JsonProperty
    public Partner getAgentPartner() {
        return agentPartner;
    }

    @SkipParamBind
    public void setAgentPartner(Partner agentPartner) {
        this.agentPartner = agentPartner;
    }

    @Column(length = 128, nullable = false, updatable = false)
    @JsonProperty
    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    @JsonProperty
    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    @JsonProperty
    public String getSenderMobile() {
        return senderMobile;
    }

    public void setSenderMobile(String senderMobile) {
        this.senderMobile = senderMobile;
    }

    @JsonProperty
    public String getSenderCompany() {
        return senderCompany;
    }

    public void setSenderCompany(String senderCompany) {
        this.senderCompany = senderCompany;
    }

    @JsonProperty
    public String getSenderAddr() {
        return senderAddr;
    }

    public void setSenderAddr(String senderAddr) {
        this.senderAddr = senderAddr;
    }

    @JsonProperty
    public String getSenderOrigin() {
        return senderOrigin;
    }

    public void setSenderOrigin(String senderOrigin) {
        this.senderOrigin = senderOrigin;
    }

    public String getReceiveName() {
        return receiveName;
    }

    public void setReceiveName(String receiveName) {
        this.receiveName = receiveName;
    }

    public String getReceiveMobile() {
        return receiveMobile;
    }

    public void setReceiveMobile(String receiveMobile) {
        this.receiveMobile = receiveMobile;
    }

    public String getReceiveCompany() {
        return receiveCompany;
    }

    public void setReceiveCompany(String receiveCompany) {
        this.receiveCompany = receiveCompany;
    }

    public String getReceiveAddr() {
        return receiveAddr;
    }

    public void setReceiveAddr(String receiveAddr) {
        this.receiveAddr = receiveAddr;
    }

    public String getReceiveOrigin() {
        return receiveOrigin;
    }

    public void setReceiveOrigin(String receiveOrigin) {
        this.receiveOrigin = receiveOrigin;
    }

    @Transient
    public String getDisplay() {
        return this.templateName;
    }

    public String getSenderPostcode() {
        return senderPostcode;
    }

    public void setSenderPostcode(String senderPostcode) {
        this.senderPostcode = senderPostcode;
    }

    public String getReceivePostcode() {
        return receivePostcode;
    }

    public void setReceivePostcode(String receivePostcode) {
        this.receivePostcode = receivePostcode;
    }

}
