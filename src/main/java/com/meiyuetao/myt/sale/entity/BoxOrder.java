package com.meiyuetao.myt.sale.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.c2c.entity.C2cShopInfo;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerAccountHist;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.customer.entity.Score;
import com.meiyuetao.myt.kuajing.entity.KjOrderStatusCodeEnum;
import com.meiyuetao.myt.kuajing.entity.KjPaymentStatusCodeEnum;
import com.meiyuetao.myt.partner.entity.Partner;
import com.meiyuetao.myt.vip.entity.VipBakMoneyHistory;
import com.meiyuetao.myt.vip.entity.VipScoreHistory;
import lab.s2jh.auth.entity.User;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.util.DateUtils;
import lab.s2jh.core.web.json.DateTimeJsonSerializer;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Type;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@MetaData("销售订单")
@Entity
@Table(name = "iyb_box_order")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class BoxOrder extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("订单号")
    private String orderSeq;
    @MetaData("订单组号")
    private String orderGroupSeq;
    @MetaData("名称")
    private String title;
    @MetaData("订单积分")
    private Integer score = 0;
    @MetaData("订单状态")
    private BoxOrderStatusEnum orderStatus = BoxOrderStatusEnum.S10O;
    @MetaData("下单时间")
    private Date orderTime;
    @MetaData("下单时间(年月日格式)")
    private String orderTimeDate;
    @MetaData("订单确认时间")
    private Date confirmTime;
    @MetaData("邮件")
    private String email;
    @MetaData("收货省")
    private String deliveryProvince;
    @MetaData("收货城市")
    private String deliveryCity;
    @MetaData("收货街道")
    private String deliveryStreet;
    @MetaData("收货地址")
    private String deliveryAddr;
    @MetaData("电话号")
    private String mobilePhone;
    @MetaData("收货人")
    private String receivePerson;
    @MetaData("付款模式")
    private String payMode;
    @MetaData("送货日期类型")
    private DeliveryDateModeEnum deliveryDateMode;
    private String kjMemo;
    private String kjPaymentMemo;
    private String kjBarCode;
    @MetaData("美月淘集市店铺")
    private C2cShopInfo c2cShopInfo;
    @MetaData("送货时间类型")
    private DeliveryTimeModeEnum deliveryTimeMode;
    @MetaData("邮编")
    private String postCode;
    @MetaData("发票类型")
    private String invoiceType;
    @MetaData("发票抬头")
    private String invoiceTitle;
    @MetaData("发票单位")
    private String invoiceCompany;
    @MetaData("发票内容")
    private String invoiceContent;
    @MetaData("客户备注")
    private String customerMemo;
    @MetaData("管理备注")
    private String adminMemo;
    @MetaData("备注")
    private String memo;
    @MetaData("付款凭证号")
    private String payVoucher;
    @MetaData(value = "原价金额")
    private BigDecimal originalAmount;
    @MetaData(value = "销售折扣金额")
    private BigDecimal discountAmount = BigDecimal.ZERO;
    @MetaData(value = "实际应付款金额")
    private BigDecimal actualAmount;
    @MetaData("已付总金额")
    private BigDecimal actualPayedAmount;
    @MetaData("已退款金额")
    private BigDecimal refundAmount = BigDecimal.ZERO;
    @MetaData("交易渠道")
    private String tradeType;
    @MetaData("交易单号")
    private String tradeNo;
    /** 送礼留言属性 */
    @MetaData("是否送礼订单")
    private Boolean isPresent;
    @MetaData("礼物接收人")
    private String presentReceiver;
    @MetaData("赠送人")
    private String presentSender;
    @MetaData("赠言")
    private String presentWords;
    @MetaData("赠送的礼品券号")
    private String presentPaperCode;
    @MetaData("应付保证金，下单时计算设置")
    private BigDecimal cautionMoneyNeed = BigDecimal.ZERO;
    @MetaData("实付保证金，支付接口回调时设置金额")
    private BigDecimal cautionMoneyActual = BigDecimal.ZERO;
    @MetaData("付款记录里上传支付网关号，out_trade_no")
    private String cautionOutTradeNo;
    @MetaData("")
    private CustomerProfile customerProfile;
    @MetaData("订单明细集合")
    private List<BoxOrderDetail> boxOrderDetails = new ArrayList<BoxOrderDetail>();
    @MetaData("盒子快照商品列表")
    private List<BoxOrderDetailCommodity> boxOrderDetailCommodities = new ArrayList<BoxOrderDetailCommodity>();
    @MetaData("推荐客户")
    private CustomerProfile intermediaryCustomerProfile;
    @MetaData("发货完成时间(后台在所有订单行项发货完成后更新设置)")
    private Date deliveryFinishTime;
    @MetaData("确认付款时间")
    private Date payConfirmTime;
    @MetaData("自动收货完成时间(后台用户可修改从而延长收货时间)")
    private Date autoConfirmTime;
    @MetaData("实际收货完成时间(可能是前端用户点击收货完成更新，也可能是定时任务更新)")
    private Date actualConfirmTime;
    @MetaData("实际收货确认操作者(前端用户写入客户唯一标识，后台任务处理写入'SYS')")
    private String actualConfirmOperator;
    @MetaData("自动完结订单时间(后台用户可修改从而延长完结时间)")
    private Date autoSuccessTime;
    @MetaData("实际完结订单时间 (可能是后台用户点击提取完结，也可能是定时任务更新)")
    private Date actualSuccessTime;
    @MetaData("实际订单完结操作者，后台任务处理写入'SYS'")
    private String actualSuccessOperator;
    @MetaData("发货前是否确认")
    private Boolean isConfirmBeforeDelivery;
    /** 订单推荐提成记录集合 */
    private List<CustomerAccountHist> customerAccountHists = new ArrayList<CustomerAccountHist>();
    @MetaData("订单类型")
    private BoxOrderModeEnum orderMode = BoxOrderModeEnum.GENERAL;
    @MetaData("分期类型")
    private BoxOrderSplitPayModeEnum splitPayMode = BoxOrderSplitPayModeEnum.NO;
    @MetaData("邮费")
    private BigDecimal postage = BigDecimal.ZERO;
    @MetaData("分销备注 ")
    private String distributionMemo;
    @MetaData("导入京东淘宝等外站订单时,保留原始信息")
    private String rawData;
    @MetaData("订单来源")
    private OrderFromEnum orderFrom = OrderFromEnum.MEIYUETAO;
    @MetaData("指定客服")
    private User serviceRepresentative;
    @MetaData("是否周期购")
    private Boolean circle = Boolean.FALSE;
    @MetaData("改价备注")
    private String modifierMemo;
    @MetaData("所属代理商")
    private Partner agentPartner;
    @MetaData("操作")
    private String operationEvent;
    @MetaData("关联积分")
    private List<Score> scores = new ArrayList<Score>();
    @MetaData("360跨境订单发送状态")
    private KuaJingStatusEnum kuaJingStatus = KuaJingStatusEnum.UNSEND;
    private KjPaymentStatusCodeEnum kjPaymentStatusCode;
    private KjOrderStatusCodeEnum kjOrderStatusCode;
    @MetaData("vip积分")
    private List<VipScoreHistory> vipScoreHistories = new ArrayList<VipScoreHistory>();
    @MetaData("vip返利")
    private List<VipBakMoneyHistory> vipBakMoneyHistories = new ArrayList<VipBakMoneyHistory>();
    @MetaData("海关条码")
    private String customCode;
    @MetaData("怡亚通等合作物流快递名称")
    private String yytLogisticsName;
    @MetaData("怡亚通等合作物流快递单号")
    private String yytLogisticsNo;
    @MetaData("怡亚通提交状态")
    private YytPostStatusEnum yytPostStatus;
    @MetaData("提货方式")
    private CommodityReceiveTypeEnum receiveType = CommodityReceiveTypeEnum.SELF_GET;
    @MetaData("订单类型")
    private OrderTypeEnum orderType;
    @MetaData("派瑞等合作物流快递名称")
    private String priLogisticsName;
    @MetaData("派瑞等合作物流快递单号")
    private String priLogisticsNo;
    @MetaData("派瑞提交状态")
    private PairuiStatusEnum pairuiStatus;
    @MetaData("是否是首单")
    private Boolean isFirstPayment;
    //    @MetaData("ERP订单编号")
    //    private String billno;

    public enum CommodityReceiveTypeEnum {
        @MetaData("自提")
        SELF_GET,

        @MetaData("快递")
        EXPRESS;
    }

    public enum DeliveryDateModeEnum {
        @MetaData("只工作日送货")
        D,

        @MetaData("只休息日送货")
        W,

        @MetaData("DW")
        DW,

        @MetaData("工作日、休息日均可送货")
        WD;
    }

    public enum DeliveryTimeModeEnum {

        @MetaData("仅白天送货（8：00-18：00）")
        D,

        @MetaData("仅晚上送货（18：00-22：00）")
        N,

        @MetaData("随时可以（8：00-22：00）")
        DN;

    }

    public enum YytPostStatusEnum {
        @MetaData("提交成功")
        Y_1,

        @MetaData(value = "提交失败")
        Y_2

    }

    public enum OrderFromEnum {

        @MetaData("美月淘")
        MEIYUETAO,

        @MetaData("后台创建")
        INTERNAL,

        @MetaData("哎呦盒子")
        IYOUBOX,

        @MetaData("中国宝贝")
        CHINABABY,

        @MetaData("手机端iphone")
        IPHONE,

        @MetaData("手机android")
        ANDROID,

        @MetaData("平板ipad")
        IPAD,

        @MetaData("微信")
        WEIXIN,

        @MetaData("联通")
        LIANTONG,

        @MetaData("天猫")
        TMALL,

        @MetaData("未来付")
        WEILAIFU,

        @MetaData(value = "WIFI")
        WIFI,

        @MetaData(value = "ISTOB")
        ISTOB,

        @MetaData(value = "WAP")
        WAP,

        @MetaData(value = "辣妈创业会")
        LAMAVC,

        @MetaData("京东")
        JD;

    }

    public enum KuaJingStatusEnum {

        @MetaData("未发送")
        UNSEND,

        @MetaData(value = "已发送")
        SEND;

    }

    public enum BoxOrderStatusEnum {

        @MetaData("用户已下单")
        S10O,

        @MetaData("计划进行中")
        S15O,

        @MetaData("用户已付款")
        S20PYD,

        @MetaData("已付款待人工审核")
        S22PYDERR,

        @MetaData("订单已确认")
        S30C,

        @MetaData("部分发货")
        S40DP,

        @MetaData("全部发货")
        S50DF,

        @MetaData("收货完成")
        S60R,

        @MetaData("订单完结")
        S70CLS,

        @MetaData("订单取消")
        S90CANCLE;
    }

    public static enum BoxOrderOperationEventEnum {

        @MetaData("订单修改")
        MODIFY,

        @MetaData("订单确认")
        CONFIRM,

        @MetaData("订单发货")
        SGC,

        @MetaData("备注修改")
        ADMINMEMO,

        @MetaData("收货完成")
        RECVCFM,

        @MetaData("订单完结")
        CLOSE,

        @MetaData("订单调价")
        INDE,

        @MetaData("取消订单")
        CANCLE,

        @MetaData("更新时间")
        UPDTIME;
    }

    public static enum BoxOrderModeEnum {

        @MetaData("普通订单")
        GENERAL,

        @MetaData("月订订单")
        SUBSCRIBE,

        @MetaData("定制预定")
        DIYSUB,

        @MetaData("租赁订单")
        RENTAL;

    }

    public static enum BoxOrderSplitPayModeEnum {

        @MetaData("不分期")
        NO,

        @MetaData("经典分期")
        CLASSIC,

        @MetaData("等额分期")
        EQUAL;

    }

    public enum OrderTypeEnum {
        @MetaData("微信快购")
        WXKG,

        @MetaData("立即购买")
        BUYNOW,

        @MetaData("面对面下单")
        Promptly;
    }

    public enum PairuiStatusEnum {
        @MetaData("提交成功")
        P_1,

        @MetaData(value = "提交失败")
        P_2
    }

    @Column(length = 16)
    @JsonProperty
    public String getOrderSeq() {
        return orderSeq;
    }

    public void setOrderSeq(String orderSeq) {
        this.orderSeq = orderSeq;
    }

    @Column(length = 128, nullable = false)
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @JsonProperty
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(Date confirmTime) {
        this.confirmTime = confirmTime;
    }

    @Column(length = 128)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(length = 256)
    @JsonProperty
    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    @Column(length = 32)
    @JsonProperty
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Column(length = 16)
    @JsonProperty
    public String getReceivePerson() {
        return receivePerson;
    }

    public void setReceivePerson(String receivePerson) {
        this.receivePerson = receivePerson;
    }

    // 注意：该配置需要在数据字典中配置IYOUBOX_FINANCE_PAY_MODE下的具体类型
    @Column(length = 16)
    @JsonProperty
    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public DeliveryDateModeEnum getDeliveryDateMode() {
        return deliveryDateMode;
    }

    public void setDeliveryDateMode(DeliveryDateModeEnum deliveryDateMode) {
        this.deliveryDateMode = deliveryDateMode;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public DeliveryTimeModeEnum getDeliveryTimeMode() {
        return deliveryTimeMode;
    }

    public void setDeliveryTimeMode(DeliveryTimeModeEnum deliveryTimeMode) {
        this.deliveryTimeMode = deliveryTimeMode;
    }

    @Column(length = 4)
    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    @Column(length = 64)
    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle;
    }

    @Column(length = 64)
    public String getInvoiceContent() {
        return invoiceContent;
    }

    public void setInvoiceContent(String invoiceContent) {
        this.invoiceContent = invoiceContent;
    }

    @Column(length = 512)
    @JsonProperty
    public String getAdminMemo() {
        return adminMemo;
    }

    public void setAdminMemo(String adminMemo) {
        this.adminMemo = adminMemo;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Column(length = 128)
    public String getCustomerMemo() {
        return customerMemo;
    }

    public void setCustomerMemo(String customerMemo) {
        this.customerMemo = customerMemo;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "Customer_Profile_Sid", nullable = false)
    @NotAudited
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Column(length = 512)
    public String getPayVoucher() {
        return payVoucher;
    }

    public void setPayVoucher(String payVoucher) {
        this.payVoucher = payVoucher;
    }

    @OneToMany(mappedBy = "boxOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<BoxOrderDetail> getBoxOrderDetails() {
        return boxOrderDetails;
    }

    public void setBoxOrderDetails(List<BoxOrderDetail> boxOrderDetails) {
        this.boxOrderDetails = boxOrderDetails;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    @JsonProperty
    public BoxOrderStatusEnum getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(BoxOrderStatusEnum orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    @Column(length = 64)
    public String getInvoiceCompany() {
        return invoiceCompany;
    }

    public void setInvoiceCompany(String invoiceCompany) {
        this.invoiceCompany = invoiceCompany;
    }

    @Column(length = 16)
    @JsonProperty
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Column(length = 32)
    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    @Column(length = 128)
    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    @Type(type = "org.hibernate.type.NumericBooleanType")
    public Boolean getIsPresent() {
        return isPresent;
    }

    public void setIsPresent(Boolean isPresent) {
        this.isPresent = isPresent;
    }

    @Column(length = 128)
    public String getPresentReceiver() {
        return presentReceiver;
    }

    public void setPresentReceiver(String presentReceiver) {
        this.presentReceiver = presentReceiver;
    }

    @Column(length = 128)
    public String getPresentSender() {
        return presentSender;
    }

    public void setPresentSender(String presentSender) {
        this.presentSender = presentSender;
    }

    @Column(length = 1024)
    public String getPresentWords() {
        return presentWords;
    }

    public void setPresentWords(String presentWords) {
        this.presentWords = presentWords;
    }

    @Column(length = 32)
    public String getPresentPaperCode() {
        return presentPaperCode;
    }

    public void setPresentPaperCode(String presentPaperCode) {
        this.presentPaperCode = presentPaperCode;
    }

    @OneToOne
    @JoinColumn(name = "Intermediary_Sid", nullable = true)
    @NotAudited
    @JsonProperty
    public CustomerProfile getIntermediaryCustomerProfile() {
        return intermediaryCustomerProfile;
    }

    public void setIntermediaryCustomerProfile(CustomerProfile intermediaryCustomerProfile) {
        this.intermediaryCustomerProfile = intermediaryCustomerProfile;
    }

    public Date getDeliveryFinishTime() {
        return deliveryFinishTime;
    }

    public void setDeliveryFinishTime(Date deliveryFinishTime) {
        this.deliveryFinishTime = deliveryFinishTime;
    }

    @Type(type = "org.hibernate.type.NumericBooleanType")
    public Boolean getIsConfirmBeforeDelivery() {
        return isConfirmBeforeDelivery;
    }

    public void setIsConfirmBeforeDelivery(Boolean isConfirmBeforeDelivery) {
        this.isConfirmBeforeDelivery = isConfirmBeforeDelivery;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public BoxOrderModeEnum getOrderMode() {
        return orderMode;
    }

    public void setOrderMode(BoxOrderModeEnum orderMode) {
        this.orderMode = orderMode;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public BoxOrderSplitPayModeEnum getSplitPayMode() {
        return splitPayMode;
    }

    public void setSplitPayMode(BoxOrderSplitPayModeEnum splitPayMode) {
        this.splitPayMode = splitPayMode;
    }

    public Date getAutoConfirmTime() {
        return autoConfirmTime;
    }

    public void setAutoConfirmTime(Date autoConfirmTime) {
        this.autoConfirmTime = autoConfirmTime;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getCautionMoneyNeed() {
        return cautionMoneyNeed;
    }

    public void setCautionMoneyNeed(BigDecimal cautionMoneyNeed) {
        this.cautionMoneyNeed = cautionMoneyNeed;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getCautionMoneyActual() {
        return cautionMoneyActual;
    }

    public void setCautionMoneyActual(BigDecimal cautionMoneyActual) {
        this.cautionMoneyActual = cautionMoneyActual;
    }

    public Date getActualConfirmTime() {
        return actualConfirmTime;
    }

    public void setActualConfirmTime(Date actualConfirmTime) {
        this.actualConfirmTime = actualConfirmTime;
    }

    @Column(length = 128)
    public String getActualConfirmOperator() {
        return actualConfirmOperator;
    }

    public void setActualConfirmOperator(String actualConfirmOperator) {
        this.actualConfirmOperator = actualConfirmOperator;
    }

    public Date getAutoSuccessTime() {
        return autoSuccessTime;
    }

    public void setAutoSuccessTime(Date autoSuccessTime) {
        this.autoSuccessTime = autoSuccessTime;
    }

    public Date getActualSuccessTime() {
        return actualSuccessTime;
    }

    public void setActualSuccessTime(Date actualSuccessTime) {
        this.actualSuccessTime = actualSuccessTime;
    }

    @Column(length = 128)
    public String getActualSuccessOperator() {
        return actualSuccessOperator;
    }

    public void setActualSuccessOperator(String actualSuccessOperator) {
        this.actualSuccessOperator = actualSuccessOperator;
    }

    public Date getPayConfirmTime() {
        return payConfirmTime;
    }

    public void setPayConfirmTime(Date payConfirmTime) {
        this.payConfirmTime = payConfirmTime;
    }

    @Column(length = 18, nullable = false)
    public BigDecimal getPostage() {
        return postage;
    }

    public void setPostage(BigDecimal postage) {
        this.postage = postage;
    }

    @Column(precision = 18, scale = 2)
    @JsonProperty
    public BigDecimal getActualPayedAmount() {
        return actualPayedAmount;
    }

    public void setActualPayedAmount(BigDecimal actualPayedAmount) {
        this.actualPayedAmount = actualPayedAmount;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    @Column(length = 64)
    public String getCautionOutTradeNo() {
        return cautionOutTradeNo;
    }

    public void setCautionOutTradeNo(String cautionOutTradeNo) {
        this.cautionOutTradeNo = cautionOutTradeNo;
    }

    @Column(length = 2000)
    public String getDistributionMemo() {
        return distributionMemo;
    }

    public void setDistributionMemo(String distributionMemo) {
        this.distributionMemo = distributionMemo;
    }

    @Column(length = 64)
    public String getDeliveryProvince() {
        return deliveryProvince;
    }

    public void setDeliveryProvince(String deliveryProvince) {
        this.deliveryProvince = deliveryProvince;
    }

    @Column(length = 64)
    public String getDeliveryCity() {
        return deliveryCity;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    @Column(length = 256)
    public String getDeliveryStreet() {
        return deliveryStreet;
    }

    public void setDeliveryStreet(String deliveryStreet) {
        this.deliveryStreet = deliveryStreet;
    }

    @JsonProperty
    public String getOrderGroupSeq() {
        return orderGroupSeq;
    }

    public void setOrderGroupSeq(String orderGroupSeq) {
        this.orderGroupSeq = orderGroupSeq;
    }

    @Lob
    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    @OneToMany(mappedBy = "boxOrder")
    @NotAudited
    public List<BoxOrderDetailCommodity> getBoxOrderDetailCommodities() {
        return boxOrderDetailCommodities;
    }

    public void setBoxOrderDetailCommodities(List<BoxOrderDetailCommodity> boxOrderDetailCommodities) {
        this.boxOrderDetailCommodities = boxOrderDetailCommodities;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32, nullable = false)
    @JsonProperty
    public OrderFromEnum getOrderFrom() {
        return orderFrom;
    }

    public void setOrderFrom(OrderFromEnum orderFrom) {
        this.orderFrom = orderFrom;
    }

    @OneToOne
    @JoinColumn(name = "SERVICE_REPRESENTATIVE_SID", nullable = true)
    @JsonProperty
    public User getServiceRepresentative() {
        return serviceRepresentative;
    }

    public void setServiceRepresentative(User serviceRepresentative) {
        this.serviceRepresentative = serviceRepresentative;
    }

    @Column(name = "is_circle")
    @JsonProperty
    public Boolean getCircle() {
        return circle;
    }

    public void setCircle(Boolean circle) {
        this.circle = circle;
    }

    @JsonProperty
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "agent_partner_sid")
    @JsonProperty
    public Partner getAgentPartner() {
        return agentPartner;
    }

    public void setAgentPartner(Partner agentPartner) {
        this.agentPartner = agentPartner;
    }

    @Column(length = 2000, nullable = true)
    @JsonProperty
    public String getModifierMemo() {
        return modifierMemo;
    }

    public void setModifierMemo(String modifierMemo) {
        this.modifierMemo = modifierMemo;
    }

    @Formula("(CONVERT(varchar(100), order_time, 23))")
    @JsonIgnore
    @NotAudited
    public String getOrderTimeDate() {
        return orderTimeDate;
    }

    public void setOrderTimeDate(String orderTimeDate) {
        this.orderTimeDate = orderTimeDate;
    }

    @OneToMany(mappedBy = "boxOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    public List<CustomerAccountHist> getCustomerAccountHists() {
        return customerAccountHists;
    }

    public void setCustomerAccountHists(List<CustomerAccountHist> customerAccountHists) {
        this.customerAccountHists = customerAccountHists;
    }

    @Override
    @Transient
    public String getDisplay() {
        return orderSeq + " " + title;
    }

    @Column(length = 4000)
    public String getOperationEvent() {
        return operationEvent;
    }

    public void setOperationEvent(String operationEvent) {
        this.operationEvent = operationEvent;
    }

    public void addOperationEvent(String operation, String userPin) {
        this.operationEvent = (operationEvent == null ? "" : operationEvent) + operation + "," + userPin + "," + DateUtils.formatTime(new Date()) + ";";

    }

    @OneToMany(mappedBy = "boxOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    public List<Score> getScores() {
        return scores;
    }

    public void setScores(List<Score> scores) {
        this.scores = scores;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    public KuaJingStatusEnum getKuaJingStatus() {
        return kuaJingStatus;
    }

    public void setKuaJingStatus(KuaJingStatusEnum kuaJingStatus) {
        this.kuaJingStatus = kuaJingStatus;
    }

    public String getKjMemo() {
        return kjMemo;
    }

    public void setKjMemo(String kjMemo) {
        this.kjMemo = kjMemo;
    }

    public String getKjPaymentMemo() {
        return kjPaymentMemo;
    }

    public void setKjPaymentMemo(String kjPaymentMemo) {
        this.kjPaymentMemo = kjPaymentMemo;
    }

    @Transient
    public String getKjBarCode() {
        return kjBarCode;
    }

    public void setKjBarCode(String kjBarCode) {
        this.kjBarCode = kjBarCode;
    }

    @Transient
    public String addKjMemo(Date date, String success, String memo) {
        if ("1".equals(success)) {
            success = "成功";
        } else if ("0".equals(success)) {
            success = "失败";
        }
        if (StringUtils.isNotBlank(kjMemo)) {
            return kjMemo + ";" + DateUtils.formatDate(date) + "," + success + "," + memo;
        }
        return DateUtils.formatDate(date) + "," + success + "," + memo;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32)
    @JsonProperty
    public KjOrderStatusCodeEnum getKjOrderStatusCode() {
        return kjOrderStatusCode;
    }

    public void setKjOrderStatusCode(KjOrderStatusCodeEnum kjOrderStatusCode) {
        this.kjOrderStatusCode = kjOrderStatusCode;
    }

    @OneToMany(mappedBy = "boxOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    public List<VipScoreHistory> getVipScoreHistories() {
        return vipScoreHistories;
    }

    public void setVipScoreHistories(List<VipScoreHistory> vipScoreHistories) {
        this.vipScoreHistories = vipScoreHistories;
    }

    @OneToMany(mappedBy = "boxOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    public List<VipBakMoneyHistory> getVipBakMoneyHistories() {
        return vipBakMoneyHistories;
    }

    public void setVipBakMoneyHistories(List<VipBakMoneyHistory> vipBakMoneyHistories) {
        this.vipBakMoneyHistories = vipBakMoneyHistories;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 32)
    @JsonProperty
    public KjPaymentStatusCodeEnum getKjPaymentStatusCode() {
        return kjPaymentStatusCode;
    }

    public void setKjPaymentStatusCode(KjPaymentStatusCodeEnum kjPaymentStatusCode) {
        this.kjPaymentStatusCode = kjPaymentStatusCode;
    }

    @JsonProperty
    public String getCustomCode() {
        return customCode;
    }

    public void setCustomCode(String customCode) {
        this.customCode = customCode;
    }

    public String getYytLogisticsName() {
        return yytLogisticsName;
    }

    public void setYytLogisticsName(String yytLogisticsName) {
        this.yytLogisticsName = yytLogisticsName;
    }

    public String getYytLogisticsNo() {
        return yytLogisticsNo;
    }

    public void setYytLogisticsNo(String yytLogisticsNo) {
        this.yytLogisticsNo = yytLogisticsNo;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public YytPostStatusEnum getYytPostStatus() {
        return yytPostStatus;
    }

    public void setYytPostStatus(YytPostStatusEnum yytPostStatus) {
        this.yytPostStatus = yytPostStatus;
    }

    @ManyToOne
    @JoinColumn(name = "c2c_shop_info_sid")
    @JsonProperty
    public C2cShopInfo getC2cShopInfo() {
        return c2cShopInfo;
    }

    public void setC2cShopInfo(C2cShopInfo c2cShopInfo) {
        this.c2cShopInfo = c2cShopInfo;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "commodity_receive_type", length = 20)
    @JsonProperty
    public CommodityReceiveTypeEnum getReceiveType() {
        return receiveType;
    }

    public void setReceiveType(CommodityReceiveTypeEnum receiveType) {
        this.receiveType = receiveType;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "order_type")
    @JsonProperty
    public OrderTypeEnum getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderTypeEnum orderType) {
        this.orderType = orderType;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public PairuiStatusEnum getPairuiStatus() {
        return pairuiStatus;
    }

    public void setPairuiStatus(PairuiStatusEnum pairuiStatus) {
        this.pairuiStatus = pairuiStatus;
    }

    public String getPriLogisticsName() {
        return priLogisticsName;
    }

    public void setPriLogisticsName(String priLogisticsName) {
        this.priLogisticsName = priLogisticsName;
    }

    public String getPriLogisticsNo() {
        return priLogisticsNo;
    }

    public void setPriLogisticsNo(String priLogisticsNo) {
        this.priLogisticsNo = priLogisticsNo;
    }

    @JsonProperty
    @Column(name = "is_first_payment")
    public Boolean getFirstPayment() {
        return isFirstPayment;
    }

    public void setFirstPayment(Boolean firstPayment) {
        isFirstPayment = firstPayment;
    }

    //    public String getBillno() {
    //        return billno;
    //    }

    //    public void setBillno(String billno) {
    //        this.billno = billno;
    //    }

}
