package com.meiyuetao.myt.sale.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;

@Entity
@MetaData("商品关联图片")
@Table(name = "myt_sale_delivery_pic")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SaleDeliveryPic extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("大图")
    private String picUrl;
    @MetaData("排序号")
    private Integer picIndex = 100;
    @MetaData("销售单")
    private SaleDelivery saleDelivery;

    @JsonProperty
    public Integer getPicIndex() {
        return picIndex;
    }

    public void setPicIndex(Integer picIndex) {
        this.picIndex = picIndex;
    }

    @Column(length = 128)
    @JsonProperty
    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    @ManyToOne
    @JoinColumn(name = "sale_delivery_sid", nullable = false)
    public SaleDelivery getSaleDelivery() {
        return saleDelivery;
    }

    public void setSaleDelivery(SaleDelivery saleDelivery) {
        this.saleDelivery = saleDelivery;
    }

}
