package com.meiyuetao.myt.sale.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.meiyuetao.myt.core.entity.MytBaseEntity;

@MetaData("订单金额增减记录")
@Entity
@Table(name = "iyb_order_in_decrease_log")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class BoxOrderInDeLog extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("订单")
    private BoxOrder boxOrder;
    @MetaData("增减额")
    private BigDecimal inDeAmount;
    @MetaData("说明")
    private String reason;
    @MetaData("操作人")
    private String moneyOperator;
    @MetaData("时间")
    private Date occurTime;
    @MetaData("订单行项")
    private BoxOrderDetail boxOrderDetail;
    @MetaData("订单行项商品")
    private BoxOrderDetailCommodity boxOrderDetailCommodity;

    @ManyToOne
    @JoinColumn(name = "ORDER_SID", nullable = false)
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getInDeAmount() {
        return inDeAmount;
    }

    public void setInDeAmount(BigDecimal inDeAmount) {
        this.inDeAmount = inDeAmount;
    }

    @Column(length = 512)
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Column(length = 32)
    public String getMoneyOperator() {
        return moneyOperator;
    }

    public void setMoneyOperator(String moneyOperator) {
        this.moneyOperator = moneyOperator;
    }

    public Date getOccurTime() {
        return occurTime;
    }

    public void setOccurTime(Date occurTime) {
        this.occurTime = occurTime;
    }

    @OneToOne
    @JoinColumn(name = "box_order_detail_sid")
    public BoxOrderDetail getBoxOrderDetail() {
        return boxOrderDetail;
    }

    public void setBoxOrderDetail(BoxOrderDetail boxOrderDetail) {
        this.boxOrderDetail = boxOrderDetail;
    }

    @OneToOne
    @JoinColumn(name = "box_order_detail_commodity_sid")
    public BoxOrderDetailCommodity getBoxOrderDetailCommodity() {
        return boxOrderDetailCommodity;
    }

    public void setBoxOrderDetailCommodity(BoxOrderDetailCommodity boxOrderDetailCommodity) {
        this.boxOrderDetailCommodity = boxOrderDetailCommodity;
    }

}
