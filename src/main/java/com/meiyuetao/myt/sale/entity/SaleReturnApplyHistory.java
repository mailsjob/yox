package com.meiyuetao.myt.sale.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.sale.entity.SaleReturnApply.PostAuditResultEnum;

@MetaData("退换货申请记录")
@Entity
@Table(name = "iyb_sales_return_apply_history")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SaleReturnApplyHistory extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联退货申请")
    private SaleReturnApply saleReturnApply;
    @MetaData("操作人")
    private String operater;
    @MetaData("处理时间")
    private String processTime;
    @MetaData("当前步奏")
    private ReturnApplyStatusEnum applyHistoryStatus = ReturnApplyStatusEnum.POST;
    @MetaData("操作备注")
    private String processComment;
    @MetaData("审核状态")
    private PostAuditResultEnum processResult = PostAuditResultEnum.PROC;

    public enum ReturnApplyStatusEnum {
        @MetaData("提交申请")
        POST, @MetaData("商家审核")
        AUDIT, @MetaData("拒绝/打回")
        DENY, @MetaData("审核通过")
        PASS, @MetaData("商家收货")
        S_RECEIPT, @MetaData("原物返回")
        REBECK, @MetaData("换货")
        EXCHANGE, @MetaData("退款")
        REFUND, @MetaData("客户收货")
        C_RECEIPT, @MetaData("处理完成")
        END;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sales_return_apply_sid")
    @JsonProperty
    public SaleReturnApply getSaleReturnApply() {
        return saleReturnApply;
    }

    public void setSaleReturnApply(SaleReturnApply saleReturnApply) {
        this.saleReturnApply = saleReturnApply;
    }

    @JsonProperty
    public String getOperater() {
        return operater;
    }

    public void setOperater(String operater) {
        this.operater = operater;
    }

    @JsonProperty
    public String getProcessTime() {
        return processTime;
    }

    public void setProcessTime(String processTime) {
        this.processTime = processTime;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    @JsonProperty
    public ReturnApplyStatusEnum getApplyHistoryStatus() {
        return applyHistoryStatus;
    }

    public void setApplyHistoryStatus(ReturnApplyStatusEnum applyHistoryStatus) {
        this.applyHistoryStatus = applyHistoryStatus;
    }

    public String getProcessComment() {
        return processComment;
    }

    public void setProcessComment(String processComment) {
        this.processComment = processComment;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    @JsonProperty
    public PostAuditResultEnum getProcessResult() {
        return processResult;
    }

    public void setProcessResult(PostAuditResultEnum processResult) {
        this.processResult = processResult;
    }

}
