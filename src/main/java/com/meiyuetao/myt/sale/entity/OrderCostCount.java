package com.meiyuetao.myt.sale.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import lab.s2jh.core.annotation.MetaData;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by MYT on 2016-04-26.
 */
@MetaData("订单统计")
@Entity
public class OrderCostCount extends MytBaseEntity {
    @MetaData("日期")
    private Date orderTime;
    @MetaData("订单数")
    private Integer total;
    @MetaData("订单商品金额")
    private BigDecimal commodityPrice;
    @MetaData("邮费")
    private BigDecimal postage;
    @MetaData("支付佣金金额")
    private BigDecimal payAcc;
    @MetaData("货品成本")
    private BigDecimal commodityCost;
    @MetaData("退货退款")
    private BigDecimal backFee;

    @Transient
    @JsonProperty
    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    @Transient
    @JsonProperty
    public BigDecimal getCommodityPrice() {
        return commodityPrice;
    }

    public void setCommodityPrice(BigDecimal commodityPrice) {
        this.commodityPrice = commodityPrice;
    }

    @Transient
    @JsonProperty
    public BigDecimal getPostage() {
        return postage;
    }

    public void setPostage(BigDecimal postage) {
        this.postage = postage;
    }

    @Transient
    @JsonProperty
    public BigDecimal getPayAcc() {
        return payAcc;
    }

    public void setPayAcc(BigDecimal payAcc) {
        this.payAcc = payAcc;
    }

    @Transient
    @JsonProperty
    public BigDecimal getCommodityCost() {
        return commodityCost;
    }

    public void setCommodityCost(BigDecimal commodityCost) {
        this.commodityCost = commodityCost;
    }

    @Transient
    @JsonProperty
    public BigDecimal getBackFee() {
        return backFee;
    }

    public void setBackFee(BigDecimal backFee) {
        this.backFee = backFee;
    }

    @Transient
    @JsonProperty
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
