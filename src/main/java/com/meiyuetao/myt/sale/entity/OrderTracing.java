package com.meiyuetao.myt.sale.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;

@MetaData("订单追踪信息")
@Entity
@Table(name = "iyb_order_tracing")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class OrderTracing extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("客户")
    private CustomerProfile customerProfile;
    @MetaData("订单")
    private BoxOrder boxOrder;
    @MetaData(value = "行项号", comments = "如果为0，则表示是关于订单跟踪的信息")
    private Long OrderDetailSid;
    @MetaData("内容")
    private String tracingText;
    @MetaData("时间")
    private Date occurTime;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "customer_profile_sid", nullable = true)
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "order_sid")
    @JsonProperty
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

    public Long getOrderDetailSid() {
        return OrderDetailSid;
    }

    public void setOrderDetailSid(Long orderDetailSid) {
        OrderDetailSid = orderDetailSid;
    }

    public String getTracingText() {
        return tracingText;
    }

    public void setTracingText(String tracingText) {
        this.tracingText = tracingText;
    }

    public Date getOccurTime() {
        return occurTime;
    }

    public void setOccurTime(Date occurTime) {
        this.occurTime = occurTime;
    }

}
