package com.meiyuetao.myt.sale.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lab.s2jh.bpm.BpmTrackable;
import lab.s2jh.core.annotation.MetaData;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.finance.entity.AccountSubject;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.md.entity.Region;
import com.meiyuetao.myt.purchase.entity.Supplier;

@MetaData("退换货申请")
@Entity
@Table(name = "iyb_sales_return_apply")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SaleReturnApply extends MytBaseEntity implements BpmTrackable {
    private static final long serialVersionUID = 1L;
    @MetaData("唯一凭证号")
    private String voucher;
    @MetaData("订单行项商品")
    private BoxOrderDetailCommodity boxOrderDetailCommodity;
    @MetaData("冗余关联订单行项")
    private BoxOrderDetail boxOrderDetail;
    @MetaData("冗余关联订单")
    private BoxOrder boxOrder;
    @MetaData("冗余关联商品")
    private Commodity commodity;
    @MetaData(value = "付款会计科目")
    private AccountSubject accountSubject;
    @MetaData("客户")
    private CustomerProfile customerProfile;
    @MetaData("退货类型")
    private ProcessTypeEnum processType = ProcessTypeEnum.RTN;

    @MetaData(value = "流程当前任务节点")
    private String activeTaskName;

    public enum ProcessTypeEnum {
        @MetaData("退货")
        RTN, @MetaData("换货")
        CHG, @MetaData("返修")
        REP;

    }

    @MetaData("退货数量")
    private BigDecimal quantity = new BigDecimal(1);
    @MetaData("是否有发票")
    private Boolean hasInvoice = Boolean.FALSE;
    @MetaData("是否有检测报告")
    private Boolean hasReport = Boolean.FALSE;
    @MetaData("提交审核状态")
    private PostAuditResultEnum processResult;

    public enum PostAuditResultEnum {
        @MetaData("拒绝/打回")
        DENY, @MetaData("处理中")
        PROC, @MetaData("审核通过")
        PASS, @MetaData("处理完成")
        OVER;
    }

    @MetaData("最后审核结果")
    private LastAuditResultEnum lastAuditResult = LastAuditResultEnum.EXCHANGE;

    @MetaData(value = "最近操作摘要")
    private String lastOperationSummary;

    public enum LastAuditResultEnum {
        @MetaData("换货")
        EXCHANGE, @MetaData("退货退款")
        REFUND;
    }

    @MetaData("备注、描述")
    private String description;
    @MetaData("用户收货城市")
    private Region saleReturnCity;
    @MetaData("用户收货电话")
    private String saleReturnPhone;
    @MetaData("收货人")
    private String saleReturnName;
    @MetaData("用户收货详细地址")
    private String saleReturnDetailAddr;
    // 给客户的退货地址
    /*
     * private String sellerPhone; private String sellerName; private String
     * sellerPostCode;
     */
    @MetaData("退回地址")
    // 仅用作给用户提示
    private String sellerDetailAddr;
    private String pic1;
    private String pic2;
    private String pic3;
    private String pic4;
    private String pic5;
    @MetaData("商品图片")
    private String commodityImg;
    @MetaData("商品标题")
    private String commodityTitle;

    @MetaData("客户退货寄回的快递公司")
    private Supplier logistics;
    @MetaData("客户退货寄回的快递单号")
    private String logisticsNo;
    @MetaData("退货说明")
    private String returnlogisticsMemo;

    @MetaData("系统备注")
    private String memo;
    @MetaData("收货入库备注")
    private String receiptMemo;

    @OneToOne
    @JoinColumn(name = "order_detail_commodity_sid", nullable = false)
    @JsonProperty
    public BoxOrderDetailCommodity getBoxOrderDetailCommodity() {
        return boxOrderDetailCommodity;
    }

    public void setBoxOrderDetailCommodity(BoxOrderDetailCommodity boxOrderDetailCommodity) {
        this.boxOrderDetailCommodity = boxOrderDetailCommodity;
    }

    @OneToOne
    @JoinColumn(name = "order_detail_sid")
    @JsonProperty
    public BoxOrderDetail getBoxOrderDetail() {
        return boxOrderDetail;
    }

    public void setBoxOrderDetail(BoxOrderDetail boxOrderDetail) {
        this.boxOrderDetail = boxOrderDetail;
    }

    @OneToOne
    @JoinColumn(name = "order_sid")
    @JsonProperty
    public BoxOrder getBoxOrder() {
        return boxOrder;
    }

    public void setBoxOrder(BoxOrder boxOrder) {
        this.boxOrder = boxOrder;
    }

    @OneToOne
    @JoinColumn(name = "commodity_sid")
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @OneToOne
    @JoinColumn(name = "customer_profile_sid", nullable = false)
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8, nullable = true)
    @JsonProperty
    public ProcessTypeEnum getProcessType() {
        return processType;
    }

    public void setProcessType(ProcessTypeEnum processType) {
        this.processType = processType;
    }

    @JsonProperty
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @JsonProperty
    public Boolean getHasInvoice() {
        return hasInvoice;
    }

    public void setHasInvoice(Boolean hasInvoice) {
        this.hasInvoice = hasInvoice;
    }

    @JsonProperty
    public Boolean getHasReport() {
        return hasReport;
    }

    public void setHasReport(Boolean hasReport) {
        this.hasReport = hasReport;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    @JsonProperty
    public PostAuditResultEnum getProcessResult() {
        return processResult;
    }

    public void setProcessResult(PostAuditResultEnum processResult) {
        this.processResult = processResult;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToOne
    @JoinColumn(name = "sales_return_city")
    @JsonProperty
    public Region getSaleReturnCity() {
        return saleReturnCity;
    }

    public void setSaleReturnCity(Region saleReturnCity) {
        this.saleReturnCity = saleReturnCity;
    }

    @JsonProperty
    @Column(name = "sales_return_phone")
    public String getSaleReturnPhone() {
        return saleReturnPhone;
    }

    public void setSaleReturnPhone(String saleReturnPhone) {
        this.saleReturnPhone = saleReturnPhone;
    }

    @JsonProperty
    @Column(name = "sales_return_name")
    public String getSaleReturnName() {
        return saleReturnName;
    }

    public void setSaleReturnName(String saleReturnName) {
        this.saleReturnName = saleReturnName;
    }

    @JsonProperty
    @Column(name = "sales_return_detail_addr")
    public String getSaleReturnDetailAddr() {
        return saleReturnDetailAddr;
    }

    public void setSaleReturnDetailAddr(String saleReturnDetailAddr) {
        this.saleReturnDetailAddr = saleReturnDetailAddr;
    }

    public String getPic1() {
        return pic1;
    }

    public void setPic1(String pic1) {
        this.pic1 = pic1;
    }

    public String getPic2() {
        return pic2;
    }

    public void setPic2(String pic2) {
        this.pic2 = pic2;
    }

    public String getPic3() {
        return pic3;
    }

    public void setPic3(String pic3) {
        this.pic3 = pic3;
    }

    public String getPic4() {
        return pic4;
    }

    public void setPic4(String pic4) {
        this.pic4 = pic4;
    }

    public String getPic5() {
        return pic5;
    }

    public void setPic5(String pic5) {
        this.pic5 = pic5;
    }

    @JsonProperty
    public String getCommodityImg() {
        return commodityImg;
    }

    public void setCommodityImg(String commodityImg) {
        this.commodityImg = commodityImg;
    }

    @JsonProperty
    public String getCommodityTitle() {
        return commodityTitle;
    }

    public void setCommodityTitle(String commodityTitle) {
        this.commodityTitle = commodityTitle;
    }

    /*
     * @JsonProperty public String getSellerPhone() { return sellerPhone; }
     * 
     * public void setSellerPhone(String sellerPhone) { this.sellerPhone =
     * sellerPhone; }
     * 
     * @JsonProperty public String getSellerName() { return sellerName; }
     * 
     * public void setSellerName(String sellerName) { this.sellerName =
     * sellerName; }
     */
    /*
     * @JsonProperty public String getSellerPostCode() { return sellerPostCode;
     * }
     * 
     * public void setSellerPostCode(String sellerPostCode) {
     * this.sellerPostCode = sellerPostCode; }
     */
    @JsonProperty
    public String getSellerDetailAddr() {
        return sellerDetailAddr;
    }

    public void setSellerDetailAddr(String sellerDetailAddr) {
        this.sellerDetailAddr = sellerDetailAddr;
    }

    @Column(length = 32)
    @JsonProperty
    public String getLogisticsNo() {
        return logisticsNo;
    }

    public void setLogisticsNo(String logisticsNo) {
        this.logisticsNo = logisticsNo;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "logistics_sid", nullable = true)
    @JsonProperty
    public Supplier getLogistics() {
        return logistics;
    }

    public void setLogistics(Supplier logistics) {
        this.logistics = logistics;
    }

    @Enumerated(EnumType.STRING)
    @Column(length = 8, name = "last_audit_result")
    @JsonProperty
    public LastAuditResultEnum getLastAuditResult() {
        return lastAuditResult;
    }

    public void setLastAuditResult(LastAuditResultEnum lastAuditResult) {
        this.lastAuditResult = lastAuditResult;
    }

    @Override
    @Transient
    @JsonIgnore
    public String getBpmBusinessKey() {
        return voucher;
    }

    @JsonProperty
    public String getActiveTaskName() {
        return activeTaskName;
    }

    public void setActiveTaskName(String activeTaskName) {
        this.activeTaskName = activeTaskName;
    }

    @JsonProperty
    public String getLastOperationSummary() {
        return lastOperationSummary;
    }

    public void setLastOperationSummary(String lastOperationSummary) {
        this.lastOperationSummary = lastOperationSummary;
    }

    // @Column(length = 128, nullable = false, unique = true, updatable = false)
    @Column(length = 128)
    @JsonProperty
    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    @JsonProperty
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @ManyToOne
    @JoinColumn(name = "account_subject_id")
    public AccountSubject getAccountSubject() {
        return accountSubject;
    }

    public void setAccountSubject(AccountSubject accountSubject) {
        this.accountSubject = accountSubject;
    }

    public String getReturnlogisticsMemo() {
        return returnlogisticsMemo;
    }

    public void setReturnlogisticsMemo(String returnlogisticsMemo) {
        this.returnlogisticsMemo = returnlogisticsMemo;
    }

    public String getReceiptMemo() {
        return receiptMemo;
    }

    public void setReceiptMemo(String receiptMemo) {
        this.receiptMemo = receiptMemo;
    }

    @Transient
    @Override
    public String getExtraInfo() {
        return this.logistics.getDisplay() + " 单号：" + this.logisticsNo;
    }
}
