package com.meiyuetao.myt.sale.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.md.entity.Commodity;
import com.meiyuetao.myt.stock.entity.StorageLocation;

/**
 * 
 */
@MetaData("退货单明细")
@Entity
@Table(name = "myt_sale_return_detail")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SaleReturnDetail extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData(value = "子凭证号")
    private String subVoucher;
    @MetaData("退货单")
    private SaleReturn saleReturn;
    @MetaData("商品")
    private Commodity commodity;
    @MetaData("商品价格售出时成本价")
    private BigDecimal price;
    @MetaData("数量")
    private BigDecimal quantity;
    @MetaData("原价金额")
    private BigDecimal originalAmount;
    @MetaData("折扣金额")
    private BigDecimal discountAmount;
    @MetaData("商品售额")
    private BigDecimal amount;
    @MetaData("商品原成本价")
    private BigDecimal commodityPrice;
    @MetaData("商品成本金额")
    private BigDecimal commodityAmount;
    @MetaData("入库成本价 ")
    private BigDecimal costPrice;
    @MetaData("运费")
    private BigDecimal deliveryAmount;
    @MetaData("报损值")
    private BigDecimal lossAmount;
    @MetaData("入库成本金")
    private BigDecimal costAmount;
    @MetaData("单位")
    private String measureUnit;
    @MetaData("销售（发货）单行项")
    private SaleDeliveryDetail saleDeliveryDetail;
    @MetaData("退换货申请单")
    private SaleReturnApply saleReturnApply;
    @MetaData("库存地")
    private StorageLocation storageLocation;
    @MetaData(value = "批次号")
    private String batchNo;
    @MetaData(value = "过期时间")
    private Date expireDate;
    @MetaData(value = "是否报废")
    private Boolean scrapped = Boolean.FALSE;

    @Column(length = 128, nullable = true)
    @JsonProperty
    public String getSubVoucher() {
        return subVoucher;
    }

    public void setSubVoucher(String subVoucher) {
        this.subVoucher = subVoucher;
    }

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "commodity_sid", nullable = false)
    @JsonProperty
    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    @Column(precision = 18, scale = 2, nullable = false)
    @JsonProperty
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @ManyToOne
    @JoinColumn(name = "sale_return_sid", nullable = false)
    public SaleReturn getSaleReturn() {
        return saleReturn;
    }

    public void setSaleReturn(SaleReturn saleReturn) {
        this.saleReturn = saleReturn;
    }

    @JsonProperty
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @JsonProperty
    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
    }

    @JsonProperty
    public BigDecimal getCommodityPrice() {
        return commodityPrice;
    }

    public void setCommodityPrice(BigDecimal commodityPrice) {
        this.commodityPrice = commodityPrice;
    }

    @JsonProperty
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    @JsonProperty
    public BigDecimal getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(BigDecimal deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    @JsonProperty
    public BigDecimal getCostAmount() {
        return costAmount;
    }

    public void setCostAmount(BigDecimal costAmount) {
        this.costAmount = costAmount;
    }

    @JsonProperty
    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "sale_delivery_detail_sid")
    @JsonProperty
    public SaleDeliveryDetail getSaleDeliveryDetail() {
        return saleDeliveryDetail;
    }

    public void setSaleDeliveryDetail(SaleDeliveryDetail saleDeliveryDetail) {
        this.saleDeliveryDetail = saleDeliveryDetail;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "storage_location_sid")
    @JsonProperty
    public StorageLocation getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(StorageLocation storageLocation) {
        this.storageLocation = storageLocation;
    }

    @JsonProperty
    public BigDecimal getCommodityAmount() {
        return commodityAmount;
    }

    public void setCommodityAmount(BigDecimal commodityAmount) {
        this.commodityAmount = commodityAmount;
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "sales_return_apply_sid")
    @JsonProperty
    public SaleReturnApply getSaleReturnApply() {
        return saleReturnApply;
    }

    public void setSaleReturnApply(SaleReturnApply saleReturnApply) {
        this.saleReturnApply = saleReturnApply;
    }

    @JsonProperty
    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    @JsonSerialize(using = DateJsonSerializer.class)
    @JsonProperty
    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    @JsonProperty
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonProperty
    public BigDecimal getLossAmount() {
        return lossAmount;
    }

    public void setLossAmount(BigDecimal lossAmount) {
        this.lossAmount = lossAmount;
    }

    @Column(name = "is_scrapped")
    @JsonProperty
    public Boolean getScrapped() {
        return scrapped;
    }

    public void setScrapped(Boolean scrapped) {
        this.scrapped = scrapped;
    }

}