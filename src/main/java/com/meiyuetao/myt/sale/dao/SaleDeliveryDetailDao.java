package com.meiyuetao.myt.sale.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.sale.entity.SaleDeliveryDetail;

@Repository
public interface SaleDeliveryDetailDao extends BaseDao<SaleDeliveryDetail, Long> {

}