package com.meiyuetao.myt.sale.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.sale.entity.BoxOrder;

@Repository
public interface BoxOrderDao extends BaseDao<BoxOrder, Long> {

    @Query("from BoxOrder bo where bo.orderSeq=:orderSeq")
    BoxOrder findByOrderSeq(@Param("orderSeq") String orderSeq);

}