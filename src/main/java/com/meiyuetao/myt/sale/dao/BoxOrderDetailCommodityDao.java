package com.meiyuetao.myt.sale.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;

@Repository
public interface BoxOrderDetailCommodityDao extends BaseDao<BoxOrderDetailCommodity, Long> {

}