package com.meiyuetao.myt.sale.dao;

import com.meiyuetao.myt.sale.entity.OrderCostCount;
import lab.s2jh.core.dao.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderCostCountDao extends BaseDao<OrderCostCount, Long> {

}