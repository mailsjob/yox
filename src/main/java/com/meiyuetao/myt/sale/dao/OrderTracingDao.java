package com.meiyuetao.myt.sale.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.sale.entity.OrderTracing;

@Repository
public interface OrderTracingDao extends BaseDao<OrderTracing, Long> {

}