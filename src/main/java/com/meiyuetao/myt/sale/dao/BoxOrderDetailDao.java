package com.meiyuetao.myt.sale.dao;

import lab.s2jh.core.dao.BaseDao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail;

@Repository
public interface BoxOrderDetailDao extends BaseDao<BoxOrderDetail, Long> {
    @Query("from BoxOrderDetail bo where bo.boxOrder=:boxOrder and bo.sn =:sn")
    BoxOrderDetail findByBoxOrderAndSn(@Param("boxOrder") BoxOrder boxOrder, @Param("sn") String sn);
}