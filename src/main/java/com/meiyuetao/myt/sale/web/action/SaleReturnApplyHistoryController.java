package com.meiyuetao.myt.sale.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.web.action.BaseController;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.sale.entity.SaleReturnApplyHistory;
import com.meiyuetao.myt.sale.service.SaleReturnApplyHistoryService;

@MetaData("退换货申请记录管理")
public class SaleReturnApplyHistoryController extends BaseController<SaleReturnApplyHistory, Long> {

    @Autowired
    private SaleReturnApplyHistoryService saleReturnApplyHistoryService;

    @Override
    protected BaseService<SaleReturnApplyHistory, Long> getEntityService() {
        return saleReturnApplyHistoryService;
    }

    @Override
    protected void checkEntityAclPermission(SaleReturnApplyHistory entity) {
        // TODO Add acl check code logic
    }

    @Override
    @MetaData("创建")
    public HttpHeaders doCreate() {
        return super.doCreate();
    }

    @Override
    @MetaData("更新")
    public HttpHeaders doUpdate() {
        return super.doUpdate();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("删除")
    public HttpHeaders doDelete() {
        return super.doDelete();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }
}