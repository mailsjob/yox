package com.meiyuetao.myt.sale.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.annotation.SecurityControlIgnore;

import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.sale.entity.SaleDeliveryDetail;
import com.meiyuetao.myt.sale.service.SaleDeliveryDetailService;

@MetaData("SaleDeliveryDetailController")
public class SaleDeliveryDetailController extends MytBaseController<SaleDeliveryDetail, Long> {

    @Autowired
    private SaleDeliveryDetailService saleDeliveryDetailService;

    @Override
    protected BaseService<SaleDeliveryDetail, Long> getEntityService() {
        return saleDeliveryDetailService;
    }

    @Override
    protected void checkEntityAclPermission(SaleDeliveryDetail entity) {

    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData(value = "销售商品毛利统计")
    @SecurityControlIgnore
    public HttpHeaders findByGroupCommodity() {
        setModel(findByGroupAggregate("commodity.id", "commodity.sku", "commodity.title", "max(case(equal(amount,0),-1,quot(diff(amount,costAmount),amount)))",
                "min(case(equal(amount,0),-1,quot(diff(amount,costAmount),amount)))", "sum(diff(amount,costAmount))", "sum(amount)", "sum(quantity)",
                "case(equal(sum(amount),0),-1,quot(sum(diff(amount,costAmount)),sum(amount)))"));
        return buildDefaultHttpHeaders();
    }
}