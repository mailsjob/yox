package com.meiyuetao.myt.sale.web.action;

import java.util.List;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.exception.DataAccessDeniedException;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.security.AclService;
import lab.s2jh.core.security.AuthContextHolder;
import lab.s2jh.core.security.AuthUserDetails;
import lab.s2jh.core.service.BaseService;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.partner.entity.Partner;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrder.OrderFromEnum;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail;
import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;
import com.meiyuetao.myt.sale.service.BoxOrderDetailService;
import com.meiyuetao.myt.sale.service.BoxOrderService;

@MetaData("订单只读控制器")
public class ViewBoxOrderController extends MytBaseController<BoxOrder, Long> {

    @Autowired
    private BoxOrderService boxOrderService;
    @Autowired
    private BoxOrderDetailService boxOrderDetailService;
    @Autowired
    private AclService aclService;

    @Override
    protected BaseService<BoxOrder, Long> getEntityService() {
        return boxOrderService;
    }

    @Override
    protected void checkEntityAclPermission(BoxOrder entity) {
        AuthUserDetails authUserDetails = AuthContextHolder.getAuthUserDetails();
        String aclCode = authUserDetails.getAclCode();
        // 如果是爱宝贝，则以显示所有order_from为CHINABABY的订单
        if (OrderFromEnum.CHINABABY.name().equals(aclCode)) {
            if (!OrderFromEnum.CHINABABY.equals(entity.getOrderFrom())) {
                throw new DataAccessDeniedException();
            }
        } else {
            // 限定只能访问登录用户所在商家的数据
            String aclCodePrefix = aclService.getLogonUserAclCodePrefix();
            Partner agentPartner = entity.getAgentPartner();
            if (agentPartner != null && StringUtils.isNotBlank(aclCodePrefix)) {
                String agentPartnerCode = agentPartner.getCode();
                if (!agentPartnerCode.startsWith(aclCodePrefix)) {
                    throw new DataAccessDeniedException();
                }
            }
        }
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        AuthUserDetails authUserDetails = AuthContextHolder.getAuthUserDetails();
        String aclCode = authUserDetails.getAclCode();
        // 如果是爱宝贝，则以显示所有order_from为CHINABABY的订单
        if (OrderFromEnum.CHINABABY.name().equals(aclCode)) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.EQ, "orderFrom", OrderFromEnum.CHINABABY));
        } else {
            // 限定只能访问登录用户所在商家的数据
            String aclCodePrefix = aclService.getLogonUserAclCodePrefix();
            if (StringUtils.isNotBlank(aclCodePrefix)) {
                groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.BW, "agentPartner.code", aclCodePrefix));
            }
        }
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData(value = "订单下的行项")
    public HttpHeaders getBoxOrderDetails() {
        List<BoxOrderDetail> boxOrderDetails = bindingEntity.getBoxOrderDetails();
        setModel(buildPageResultFromList(boxOrderDetails));
        return buildDefaultHttpHeaders();
    }

    public HttpHeaders getBoxOrderDetailCommodities() {
        String detailSid = this.getRequiredParameter("detailSid");
        BoxOrderDetail boxOrderdetail = boxOrderDetailService.findOne(Long.valueOf(detailSid));
        List<BoxOrderDetailCommodity> boxOrderDetailCommodities = boxOrderdetail.getBoxOrderDetailCommodities();
        setModel(buildPageResultFromList(boxOrderDetailCommodities));
        return buildDefaultHttpHeaders();
    }
}