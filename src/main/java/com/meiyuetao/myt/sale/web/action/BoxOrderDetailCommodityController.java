package com.meiyuetao.myt.sale.web.action;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.exception.DataAccessDeniedException;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter.MatchType;
import lab.s2jh.core.security.AclService;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.partner.entity.Partner;
import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;
import com.meiyuetao.myt.sale.service.BoxOrderDetailCommodityService;
import com.meiyuetao.myt.sale.service.BoxOrderDetailService;
import com.meiyuetao.myt.sale.service.BoxOrderService;

@MetaData("BoxOrderDetailController")
public class BoxOrderDetailCommodityController extends MytBaseController<BoxOrderDetailCommodity, Long> {

    @Autowired
    private BoxOrderService boxOrderService;
    @Autowired
    private BoxOrderDetailService boxOrderDetailService;
    @Autowired
    private BoxOrderDetailCommodityService boxOrderDetailCommodityService;
    @Autowired
    private AclService aclService;

    @Override
    protected BaseService<BoxOrderDetailCommodity, Long> getEntityService() {
        return boxOrderDetailCommodityService;
    }

    @Override
    protected void checkEntityAclPermission(BoxOrderDetailCommodity entity) {
        // 限定只能访问登录用户所在商家的数据
        String aclCodePrefix = aclService.getLogonUserAclCodePrefix();
        Partner agentPartner = entity.getBoxOrder().getAgentPartner();
        if (agentPartner != null && StringUtils.isNotBlank(aclCodePrefix)) {
            String agentPartnerCode = agentPartner.getCode();
            if (!agentPartnerCode.startsWith(aclCodePrefix)) {
                throw new DataAccessDeniedException();
            }
        }
    }

    @Override
    protected void appendFilterProperty(GroupPropertyFilter groupPropertyFilter) {
        // 限定只能访问登录用户所在商家的数据
        String aclCodePrefix = aclService.getLogonUserAclCodePrefix();
        if (StringUtils.isNotBlank(aclCodePrefix)) {
            groupPropertyFilter.forceAnd(new PropertyFilter(MatchType.BW, "boxOrder.agentPartner.code", aclCodePrefix));
        }
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("保存")
    public HttpHeaders modifyPrice() {
        boxOrderDetailCommodityService.modifyPrice(bindingEntity);
        setModel(OperationResult.buildSuccessResult("行项商品改价成功"));
        return buildDefaultHttpHeaders();
    }

    @MetaData(value = "按预约发货日期汇总商品量")
    public HttpHeaders findByGroupReserveDeliveryMonth() {
        setModel(findByGroupAggregate("boxOrderDetail.reserveDeliveryMonth", "commodity.id", "commodity.sku", "commodity.title", "sum(quantity)"));
        return buildDefaultHttpHeaders();
    }

    @MetaData(value = "按预约发货日期汇总商品量")
    public HttpHeaders findByGroupReserveDeliveryTime() {
        setModel(findByGroupAggregate("boxOrderDetail.reserveDeliveryTime", "commodity.id", "commodity.sku", "commodity.title", "sum(quantity)"));
        return buildDefaultHttpHeaders();
    }

}