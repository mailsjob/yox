package com.meiyuetao.myt.sale.web.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.security.AuthContextHolder;
import lab.s2jh.core.service.BaseService;
import lab.s2jh.core.service.Validation;
import lab.s2jh.core.web.view.OperationResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.meiyuetao.myt.core.web.MytBaseController;
import com.meiyuetao.myt.finance.entity.TradeReceiptDetail;
import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;
import com.meiyuetao.myt.sale.entity.SaleReturnApply;
import com.meiyuetao.myt.sale.entity.SaleReturnApply.PostAuditResultEnum;
import com.meiyuetao.myt.sale.service.SaleReturnApplyService;

@MetaData("SaleReturnController")
public class SaleReturnApplyController extends MytBaseController<SaleReturnApply, Long> {

    @Autowired
    private SaleReturnApplyService saleReturnApplyService;

    private List<TradeReceiptDetail> tradeReceiptDetails = new ArrayList<TradeReceiptDetail>();

    @Override
    protected BaseService<SaleReturnApply, Long> getEntityService() {
        return saleReturnApplyService;
    }

    @Override
    protected void checkEntityAclPermission(SaleReturnApply entity) {
        // TODO Add acl check code logic
    }

    /*
     * @Override public String isDisallowUpdate() { if
     * (bindingEntity.getProcessTime() != null) { return "已经审核，无法修改"; } return
     * null; }
     */

    public HttpHeaders boxOrderDetailCommodities() {
        BoxOrderDetailCommodity boxOrderDetailCommodity = bindingEntity.getBoxOrderDetailCommodity();
        List<BoxOrderDetailCommodity> boxOrderDetailCommodities = Lists.newArrayList();
        boxOrderDetailCommodities.add(boxOrderDetailCommodity);
        setModel(buildPageResultFromList(boxOrderDetailCommodities));
        return buildDefaultHttpHeaders();
    }

    @Override
    @MetaData("保存")
    public HttpHeaders doSave() {
        return super.doSave();
    }

    @Override
    @MetaData("查询")
    public HttpHeaders findByPage() {
        return super.findByPage();
    }

    @MetaData("提交审核")
    public HttpHeaders bpmPostAudit() {
        Map<String, Object> variables = Maps.newHashMap();
        variables.put("auditPostTime", new Date());
        variables.put("auditPostUser", AuthContextHolder.getAuthUserPin());
        Boolean auditLevel1Pass = new Boolean(getRequiredParameter("auditPostPass"));
        variables.put("auditPostPass", auditLevel1Pass);
        variables.put("auditPostExplain", getParameter("auditPostExplain"));
        bindingEntity.setLastOperationSummary(bindingEntity.buildLastOperationSummary("提交审核"));
        if (auditLevel1Pass) {
            bindingEntity.setProcessResult(PostAuditResultEnum.PASS);
        } else {
            bindingEntity.setProcessResult(PostAuditResultEnum.DENY);
        }
        saleReturnApplyService.bpmPostAudit(bindingEntity, this.getRequiredParameter("taskId"), variables);
        setModel(OperationResult.buildSuccessResult("审核完成", bindingEntity));
        return buildDefaultHttpHeaders();
    }

    @MetaData("库房收货")
    public HttpHeaders bpmStoreReceive() {
        Map<String, Object> variables = Maps.newHashMap();
        bindingEntity.setLastOperationSummary(bindingEntity.buildLastOperationSummary("库房收货"));
        Boolean isInStore = new Boolean(getRequiredParameter("isInStore"));// 是否入库
        variables.put("isInStore", isInStore);
        if (isInStore) {
            // 入库
            String commodityStoreId = this.getRequiredParameter("commodityStoreId");
            Validation.isTrue(StringUtils.isNotBlank(commodityStoreId), "入库请选择库存批次");
            saleReturnApplyService.inStore(bindingEntity, commodityStoreId);
        }
        saleReturnApplyService.bpmStoreReceive(bindingEntity, this.getRequiredParameter("taskId"), variables);
        String msg = "库房收货完成" + (isInStore ? "(商品已入库)" : "(商品不入库)");
        setModel(OperationResult.buildSuccessResult(msg, bindingEntity));
        return buildDefaultHttpHeaders();
    }

    @MetaData("退换货审核")
    public HttpHeaders bpmSaleReturnAudit() {
        Map<String, Object> variables = Maps.newHashMap();
        bindingEntity.setLastOperationSummary(bindingEntity.buildLastOperationSummary("退换货审核"));
        saleReturnApplyService.bpmSaleReturnAudit(bindingEntity, this.getRequiredParameter("taskId"), variables);
        setModel(OperationResult.buildSuccessResult("退换货审核完成", bindingEntity));
        return buildDefaultHttpHeaders();
    }

    @MetaData("退款")
    public HttpHeaders bpmRefundment() {
        Map<String, Object> variables = Maps.newHashMap();
        bindingEntity.setLastOperationSummary(bindingEntity.buildLastOperationSummary("退款"));
        saleReturnApplyService.bpmRefundment(bindingEntity, this.getRequiredParameter("taskId"), variables, tradeReceiptDetails);
        setModel(OperationResult.buildSuccessResult("退款完成", bindingEntity));
        return buildDefaultHttpHeaders();
    }

    public List<TradeReceiptDetail> getTradeReceiptDetails() {
        return tradeReceiptDetails;
    }

    public void setTradeReceiptDetails(List<TradeReceiptDetail> tradeReceiptDetails) {
        this.tradeReceiptDetails = tradeReceiptDetails;
    }

}