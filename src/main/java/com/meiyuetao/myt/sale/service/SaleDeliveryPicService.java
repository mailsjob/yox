package com.meiyuetao.myt.sale.service;

import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.sale.dao.SaleDeliveryPicDao;
import com.meiyuetao.myt.sale.entity.SaleDeliveryPic;

@Service
@Transactional
public class SaleDeliveryPicService extends BaseService<SaleDeliveryPic, Long> {

    @Autowired
    private SaleDeliveryPicDao saleDeliveryPicDao;

    @Override
    protected SaleDeliveryPicDao getEntityDao() {
        return saleDeliveryPicDao;
    }

}
