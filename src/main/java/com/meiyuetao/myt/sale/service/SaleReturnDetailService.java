package com.meiyuetao.myt.sale.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.md.dao.CommodityDao;
import com.meiyuetao.myt.sale.dao.SaleDeliveryDetailDao;
import com.meiyuetao.myt.sale.dao.SaleReturnDetailDao;
import com.meiyuetao.myt.sale.entity.SaleReturnDetail;
import com.meiyuetao.myt.stock.dao.StockInOutDao;
import com.meiyuetao.myt.stock.dao.StorageLocationDao;

@Service
@Transactional
public class SaleReturnDetailService extends BaseService<SaleReturnDetail, Long> {

    @Autowired
    private SaleReturnDetailDao saleReturnDetailDao;
    @Autowired
    private CommodityDao commodityDao;
    @Autowired
    private SaleDeliveryDetailDao saleDeliveryDetailDao;
    @Autowired
    private StorageLocationDao storageLocationDao;
    @Autowired
    private StockInOutDao stockInOutDao;

    @Override
    protected BaseDao<SaleReturnDetail, Long> getEntityDao() {
        return saleReturnDetailDao;
    }

    @Override
    public SaleReturnDetail save(SaleReturnDetail srd) {
        // //更新成本价格
        // SaleReturn saleReturn = srd.getSaleReturn();
        // Commodity commodity =
        // commodityDao.findOne(srd.getCommodity().getId());
        // StorageLocation storageLocation =
        // storageLocationDao.findOne(srd.getStorageLocation().getId());
        // //销售订单行项上的 商品库存地
        // CommodityStock commodityStock =
        // commodityStockDao.findByCommodityAndStorageLocation(commodity,
        // storageLocation);
        // StockInOut stockInOut = null;
        // if (commodityStock != null) {
        // //更新库存信息
        // //创建库存变更明细
        // stockInOut = new StockInOut(saleReturn.getVoucher(),
        // VoucherTypeEnum.TH, commodityStock);
        // commodityStock.setCurStockAmount(commodityStock.getCurStockAmount().add(srd.getCostAmount()));
        // commodityStock.setCurStockQuantity(commodityStock.getCurStockQuantity().add(srd.getQuantity()));
        // if (commodityStock.getCurStockQuantity().compareTo(BigDecimal.ZERO)
        // == 0) {
        // commodityStock.setCurStockAmount(BigDecimal.ZERO);
        // commodityStock.setCostPrice(BigDecimal.ZERO);
        // } else {
        // commodityStock.setCostPrice(commodityStock.getCurStockAmount().divide(
        // commodityStock.getCurStockQuantity(), 2,
        // BigDecimal.ROUND_HALF_EVEN));
        // }
        // } else {
        // commodityStock = new CommodityStock();
        // stockInOut = new StockInOut(saleReturn.getVoucher(),
        // VoucherTypeEnum.TH, commodityStock);
        // commodityStock.setCommodity(commodity);
        // commodityStock.setStorageLocation(storageLocation);
        // commodityStock.setCurStockQuantity(srd.getQuantity());
        // commodityStock.setCostPrice(srd.getCostPrice());
        // commodityStock.setCurStockAmount(commodityStock.getCostPrice().multiply(
        // commodityStock.getCurStockQuantity()));
        // commodityStock.setCostAmount00(commodityStock.getCurStockAmount());
        // commodityStock.setQuantity00(commodityStock.getCurStockQuantity());
        //
        // }
        // commodityStockDao.save(commodityStock);
        // stockInOut.setFinalProperties(commodityStock);
        // stockInOut.setMemo("退货单，入库商品：" + srd.getCommodity().getDisplay() +
        // "。数量：" + srd.getQuantity());
        // stockInOutDao.save(stockInOut);
        // //关联销售单的处理
        // if (srd.getSaleDeliveryDetail() != null) {
        // SaleDeliveryDetail saleDeliveryDetail =
        // saleDeliveryDetailDao.findOne(srd.getSaleDeliveryDetail().getId());
        // //更新销售单行项"已退货量"
        // saleDeliveryDetail.setReturnQuantity(saleDeliveryDetail.getReturnQuantity().add(srd.getQuantity()));
        // saleDeliveryDetailDao.save(saleDeliveryDetail);
        // }
        return srd;
    }
}
