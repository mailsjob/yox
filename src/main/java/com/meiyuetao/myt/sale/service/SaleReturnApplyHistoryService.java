package com.meiyuetao.myt.sale.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.sale.dao.SaleReturnApplyHistoryDao;
import com.meiyuetao.myt.sale.entity.SaleReturnApplyHistory;

@Service
@Transactional
public class SaleReturnApplyHistoryService extends BaseService<SaleReturnApplyHistory, Long> {

    @Autowired
    private SaleReturnApplyHistoryDao saleReturnApplyHistoryDao;

    @Override
    protected BaseDao<SaleReturnApplyHistory, Long> getEntityDao() {
        return saleReturnApplyHistoryDao;
    }

}
