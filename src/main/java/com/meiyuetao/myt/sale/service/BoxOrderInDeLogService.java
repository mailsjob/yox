package com.meiyuetao.myt.sale.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.sale.dao.BoxOrderInDeLogDao;
import com.meiyuetao.myt.sale.entity.BoxOrderInDeLog;

@Service
@Transactional
public class BoxOrderInDeLogService extends BaseService<BoxOrderInDeLog, Long> {

    @Autowired
    private BoxOrderInDeLogDao boxOrderInDeLogDao;

    @Override
    protected BaseDao<BoxOrderInDeLog, Long> getEntityDao() {
        return boxOrderInDeLogDao;
    }

}
