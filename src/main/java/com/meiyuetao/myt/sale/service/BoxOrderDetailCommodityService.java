package com.meiyuetao.myt.sale.service;

import java.math.BigDecimal;
import java.util.Date;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.security.AuthContextHolder;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.sale.dao.BoxOrderDao;
import com.meiyuetao.myt.sale.dao.BoxOrderDetailCommodityDao;
import com.meiyuetao.myt.sale.dao.BoxOrderDetailDao;
import com.meiyuetao.myt.sale.dao.BoxOrderInDeLogDao;
import com.meiyuetao.myt.sale.entity.BoxOrder;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail;
import com.meiyuetao.myt.sale.entity.BoxOrderDetail.BoxOrderDetailStatusEnum;
import com.meiyuetao.myt.sale.entity.BoxOrderDetailCommodity;
import com.meiyuetao.myt.sale.entity.BoxOrderInDeLog;

@Service
@Transactional
public class BoxOrderDetailCommodityService extends BaseService<BoxOrderDetailCommodity, Long> {

    @Autowired
    private BoxOrderDetailCommodityDao boxOrderDetailCommodityDao;

    @Autowired
    private BoxOrderDao boxOrderDao;
    @Autowired
    private BoxOrderInDeLogDao boxOrderInDeLogDao;
    @Autowired
    private BoxOrderDetailDao boxOrderDetailDao;

    @Override
    protected BaseDao<BoxOrderDetailCommodity, Long> getEntityDao() {
        return boxOrderDetailCommodityDao;
    }

    // 行项商品改价
    public BoxOrderDetailCommodity modifyPrice(BoxOrderDetailCommodity entity) {
        /*
         * if (!entity.isNew()) { BoxOrderDetailCommodity dbEntity =
         * boxOrderDetailCommodityDao.findOne(entity.getId()); if
         * (!dbEntity.getQuantity().equals(entity.getQuantity()) ||
         * !dbEntity.getPrice().equals(entity.getPrice())) { BigDecimal diff =
         * entity.getPrice().multiply(entity.getQuantity())
         * .subtract(dbEntity.getPrice().multiply(dbEntity.getQuantity()));
         * //TODO: 添加判断逻辑：所属BoxOrderDetail的状态为“用户已下单”或“计划进行中”才允许改价，否则抛出Service异常
         * //TODO: 如果所属订单行项的quantity!=1则抛出异常拒绝改价; 否则用增量金额更新行项price值
         * BoxOrderDetail boxOrderDetail = entity.getBoxOrderDetail();
         * BoxOrderDetailStatusEnum orderDetailStatus =
         * boxOrderDetail.getOrderDetailStatus(); if
         * (BoxOrderDetailStatusEnum.S10O.equals(orderDetailStatus) ||
         * BoxOrderDetailStatusEnum.S15O.equals(orderDetailStatus)) { if
         * (!dbEntity.getBoxOrderDetail().getQuantity().equals(1)) { throw new
         * RuntimeException("订单行项的数量下数量为1，拒绝改价"); } else {
         * boxOrderDetail.setPrice(boxOrderDetail.getPrice().add(diff));
         * boxOrderDetailDao.save(boxOrderDetail); }
         * 
         * } else { throw new RuntimeException("行项状态为“用户已下单”或“计划进行中”才允许改价"); }
         * 
         * BoxOrder boxOrder = entity.getBoxOrder();
         * boxOrder.setActualAmount(boxOrder.getActualAmount().add(diff));
         * boxOrderDao.save(boxOrder); //TODO: 写入订单条件记录数据 BoxOrderInDeLog boid =
         * new BoxOrderInDeLog(); boid.setBoxOrder(boxOrder);
         * boid.setBoxOrderDetail(boxOrderDetail);
         * boid.setBoxOrderDetailCommodity(entity); boid.setInDeAmount(diff);
         * boid.setReason("行项商品调价--数量：" + entity.getQuantity() + ",单价：" +
         * entity.getPrice() + ",小计价:" +
         * entity.getPrice().multiply(entity.getQuantity()) + "  (原数量：" +
         * dbEntity.getQuantity() + ",原单价：" + dbEntity.getPrice() + ",原小计价：" +
         * dbEntity.getPrice().multiply(dbEntity.getQuantity()) + ")");
         * boid.setOccurTime(new Date());
         * boid.setMoneyOperator(AuthContextHolder.getAuthUserPin());
         * boxOrderInDeLogDao.save(boid); } }
         */

        if (!entity.isNew()) {
            BoxOrderDetailCommodity dbEntity = boxOrderDetailCommodityDao.findOne(entity.getId());

            if (!dbEntity.getActualAmount().equals(entity.getActualAmount()) || !dbEntity.getOriginalAmount().equals(entity.getOriginalAmount())) {
                // TODO:
                // 添加判断逻辑：所属BoxOrderDetail的状态为“用户已下单”或“计划进行中”才允许改价，否则抛出Service异常
                BoxOrderDetail boxOrderDetail = entity.getBoxOrderDetail();
                BoxOrder boxOrder = entity.getBoxOrder();
                BigDecimal diffActualAmount = entity.getActualAmount().subtract(dbEntity.getActualAmount());
                BigDecimal diffOriginalAmount = entity.getOriginalAmount().subtract(dbEntity.getOriginalAmount());
                if (entity.getBoxOrderDetail().getOrderDetailStatus().equals(BoxOrderDetailStatusEnum.S10O) || entity.getBoxOrderDetail().equals(BoxOrderDetailStatusEnum.S15O)) {
                    boxOrderDetail.setActualAmount(boxOrderDetail.getActualAmount().add(diffActualAmount));
                    boxOrderDetail.setPrice(boxOrderDetail.getActualAmount());
                    boxOrderDetail.setOriginalAmount(boxOrderDetail.getOriginalAmount().add(diffOriginalAmount));
                    boxOrderDetail.setDiscountAmount(boxOrderDetail.getOriginalAmount().subtract(boxOrderDetail.getActualAmount()));
                    boxOrder.setOriginalAmount(boxOrder.getOriginalAmount().add(diffOriginalAmount));
                    boxOrder.setActualAmount(boxOrder.getActualAmount().add(diffActualAmount));
                    boxOrder.setDiscountAmount(boxOrder.getOriginalAmount().subtract(boxOrder.getActualAmount()));
                    boxOrderDetailDao.save(boxOrderDetail);
                    boxOrderDao.save(boxOrder);
                } else {
                    throw new RuntimeException("行项状态为“用户已下单”或“计划进行中”才允许行项商品改价");
                }
                // TODO: 写入订单条件记录数据
                BoxOrderInDeLog boid = new BoxOrderInDeLog();
                boid.setBoxOrder(boxOrder);
                boid.setBoxOrderDetail(boxOrderDetail);
                boid.setBoxOrderDetailCommodity(entity);
                boid.setInDeAmount(diffActualAmount);
                boid.setReason(entity.getModifierMemo());
                boid.setOccurTime(new Date());
                boid.setMoneyOperator(AuthContextHolder.getAuthUserPin());
                boxOrderInDeLogDao.save(boid);
            }
        }
        return super.save(entity);
    }

}
