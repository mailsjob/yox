package com.meiyuetao.myt.sale.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.sale.dao.SaleDeliveryDetailDao;
import com.meiyuetao.myt.sale.entity.SaleDeliveryDetail;

@Service
@Transactional
public class SaleDeliveryDetailService extends BaseService<SaleDeliveryDetail, Long> {

    @Autowired
    private SaleDeliveryDetailDao saleDeliveryDetailDao;

    @Override
    protected BaseDao<SaleDeliveryDetail, Long> getEntityDao() {
        return saleDeliveryDetailDao;
    }
}
