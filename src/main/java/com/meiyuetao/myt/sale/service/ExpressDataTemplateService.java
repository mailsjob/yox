package com.meiyuetao.myt.sale.service;

import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meiyuetao.myt.sale.dao.ExpressDataTemplateDao;
import com.meiyuetao.myt.sale.entity.ExpressDataTemplate;

@Service
@Transactional
public class ExpressDataTemplateService extends BaseService<ExpressDataTemplate, Long> {

    @Autowired
    private ExpressDataTemplateDao expressDataTemplateDao;

    @Override
    protected BaseDao<ExpressDataTemplate, Long> getEntityDao() {
        return expressDataTemplateDao;
    }

}
