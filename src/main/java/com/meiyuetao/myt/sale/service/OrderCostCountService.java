package com.meiyuetao.myt.sale.service;

import com.meiyuetao.myt.sale.dao.OrderCostCountDao;
import com.meiyuetao.myt.sale.entity.OrderCostCount;
import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OrderCostCountService extends BaseService<OrderCostCount,Long>{
    
    @Autowired
    private OrderCostCountDao orderCostCountDao;

    @Override
    protected BaseDao<OrderCostCount, Long> getEntityDao() {
        return orderCostCountDao;
    }
}
