package com.meiyuetao.myt.sale.vo;

import java.math.BigDecimal;
import java.util.Date;

import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.web.json.DateJsonSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.meiyuetao.myt.core.constant.StorageModeEnum;

@MetaData("销售单、销售退货单统计报表")
public class SaleAndReturnDetailVo {
    private long sid;

    @MetaData("凭证号")
    private String voucher;

    @MetaData("销售日期")
    private Date createdDate;

    @MetaData("商品标题")
    private String commodityTitle;

    @MetaData("数量")
    private BigDecimal quantity;

    @MetaData("单价")
    private BigDecimal price;

    @MetaData("原价金额")
    private BigDecimal originalAmount;

    @MetaData("折扣额")
    private BigDecimal discountAmount;

    @MetaData("折后金额 ")
    private BigDecimal amount;

    @MetaData("含税总金额")
    private BigDecimal commodityAndTaxAmount = BigDecimal.ZERO;

    @MetaData("成本单价 （含税进价）")
    private BigDecimal costPrice = BigDecimal.ZERO;

    @MetaData(value = "库存模式")
    private StorageModeEnum storageMode = StorageModeEnum.MU;

    @MetaData(value = "单据类型")
    private String voucherType;

    public long getSid() {
        return sid;
    }

    public void setSid(long sid) {
        this.sid = sid;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    @JsonSerialize(using = DateJsonSerializer.class)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCommodityTitle() {
        return commodityTitle;
    }

    public void setCommodityTitle(String commodityTitle) {
        this.commodityTitle = commodityTitle;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getCommodityAndTaxAmount() {
        return commodityAndTaxAmount;
    }

    public void setCommodityAndTaxAmount(BigDecimal commodityAndTaxAmount) {
        this.commodityAndTaxAmount = commodityAndTaxAmount;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public StorageModeEnum getStorageMode() {
        return storageMode;
    }

    public void setStorageMode(StorageModeEnum storageMode) {
        this.storageMode = storageMode;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

}