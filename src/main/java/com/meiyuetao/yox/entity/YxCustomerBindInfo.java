package com.meiyuetao.yox.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meiyuetao.myt.core.entity.MytBaseEntity;
import com.meiyuetao.myt.customer.entity.CustomerProfile;
import lab.s2jh.core.annotation.MetaData;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * Created by zhuhui on 16-2-18.
 */

@MetaData("优享一百用户微信绑定")
@Entity
@Table(name = "yx_customer_bind_info")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class YxCustomerBindInfo extends MytBaseEntity {
    private static final long serialVersionUID = 1L;
    @MetaData("关联用户")
    private CustomerProfile customerProfile;
    @MetaData("绑定状态")
    private BindStatusEnum bindStatus;
    @MetaData("手机号")
    private String mobile;
    @MetaData("真实姓名")
    private String trueName;
    @MetaData("负责人医院")
    private String hospital;
    @MetaData("备注")
    private String memo;

    @OneToOne
    @JoinColumn(name = "custormer_profile_sid")
    @JsonProperty
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @JsonProperty
    @Enumerated(EnumType.STRING)
    @Column(name = "bind_status")
    public BindStatusEnum getBindStatus() {
        return bindStatus;
    }

    public void setBindStatus(BindStatusEnum bindStatus) {
        this.bindStatus = bindStatus;
    }

    @JsonProperty
    @Column(name = "mobile")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonProperty
    @Column(name = "true_name")
    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    @JsonProperty
    @Column(name = "hospital")
    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    @JsonProperty
    @Column(name = "memo")
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Transient
    public String getWxQrcode() {
        return "";
    }

    @Transient
    public String getUpstreamId() {
        return "";
    }

    @Transient
    public String getUpstreamNickName() {
        return "";
    }

    public enum BindStatusEnum {
        @MetaData("未关联")
        NO,
        @MetaData("已关联")
        OK;
    }

}
