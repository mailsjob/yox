package com.meiyuetao.yox.dao;

import com.meiyuetao.yox.entity.YxCustomerBindInfo;
import lab.s2jh.core.dao.BaseDao;

import org.springframework.stereotype.Repository;

@Repository
public interface YxCustomerBindInfoDao extends BaseDao<YxCustomerBindInfo, Long> {

}