package com.meiyuetao.yox.service;

import com.meiyuetao.myt.customer.entity.CustomerProfile;
import com.meiyuetao.myt.customer.service.CustomerProfileService;
import com.meiyuetao.myt.vip.entity.VipCustomerProfile;
import com.meiyuetao.myt.vip.entity.VipWxQrcode;
import com.meiyuetao.myt.vip.service.VipCustomerProfileService;
import com.meiyuetao.myt.vip.service.VipLevelService;
import com.meiyuetao.myt.vip.service.VipWxQrcodeService;
import com.meiyuetao.util.ExcelUtil;
import com.meiyuetao.yox.dao.YxCustomerBindInfoDao;
import com.meiyuetao.yox.entity.YxCustomerBindInfo;
import lab.s2jh.core.dao.BaseDao;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import lab.s2jh.core.service.BaseService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
@Transactional
public class YxCustomerBindInfoService extends BaseService<YxCustomerBindInfo, Long> {

    @Autowired
    private YxCustomerBindInfoDao yxCustomerBindInfoDao;

    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private VipLevelService vipLevelService;

    @Autowired
    private VipCustomerProfileService vipCustomerProfileService;

    @Autowired
    private VipWxQrcodeService vipWxQrcodeService;

    @Override
    protected BaseDao<YxCustomerBindInfo, Long> getEntityDao() {
        return yxCustomerBindInfoDao;
    }

    public String createCustomer(File customerExcel) throws IOException, InvalidFormatException {
        List<Row> rows = ExcelUtil.readXls(customerExcel);
        String name = "";
        String mobile = "";
        String hospital = "";
        String memo = "";
        int i = 1;
        String createInfo = "";
        for (Row row : rows) {
            name = ExcelUtil.getValue(row.getCell(0));
            mobile = ExcelUtil.getValue(row.getCell(1));
            hospital = ExcelUtil.getValue(row.getCell(2));
            memo = ExcelUtil.getValue(row.getCell(3));
            if (StringUtils.isNullOrEmpty(mobile)) {
                createInfo += "第" + i + "行的手机号为空\r\n";
                continue;
            }
            // 根据手机号判断种子用户是否已经生成，若已生成，不重复生成，否则，新建用户
            GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
            groupPropertyFilter.forceAnd(new PropertyFilter(PropertyFilter.MatchType.EQ, "mobile", mobile));
            groupPropertyFilter.forceAnd(new PropertyFilter(PropertyFilter.MatchType.EQ, "customerProfile.customerFrom", "ZHONGZI"));
            List<YxCustomerBindInfo> list = super.findByFilters(groupPropertyFilter);
            if (CollectionUtils.isEmpty(list)) {
                CustomerProfile customer = new CustomerProfile();
                VipCustomerProfile vipCustomerProfile = new VipCustomerProfile();
                customer.setTrueName(name);
                customer.setMobilePhone(mobile);
                customer.setCustomerFrom("ZHONGZI");
                customer.setCustomerNameType("8");//iyou通行证
                customer.setCustomerName("");
                customerProfileService.save(customer);
                vipCustomerProfile.setCustomerProfile(customer);
                // 生成皇冠用户
                vipCustomerProfile.setVipLevel(vipLevelService.findOne(Long.valueOf(5)));
                vipCustomerProfileService.save(vipCustomerProfile);
                // 关联微信二维码
                GroupPropertyFilter qrcodeGPF = GroupPropertyFilter.buildDefaultAndGroupFilter();
                qrcodeGPF.forceAnd(new PropertyFilter(PropertyFilter.MatchType.NU, "customerProfile", true));
                List<VipWxQrcode> listVwq = vipWxQrcodeService.findByFilters(qrcodeGPF, new Sort(Sort.Direction.DESC, "createdDate"), 1);
                VipWxQrcode vipWxQrcode = listVwq.get(0);
                vipWxQrcode.setCustomerProfile(customer);
                vipWxQrcode.setVipCustomerProfile(vipCustomerProfile);
                vipWxQrcodeService.save(vipWxQrcode);
                YxCustomerBindInfo bindInfo = new YxCustomerBindInfo();
                bindInfo.setMobile(mobile);
                bindInfo.setTrueName(name);
                bindInfo.setHospital(hospital);
                bindInfo.setMemo(memo);
                bindInfo.setCustomerProfile(customer);
                bindInfo.setBindStatus(YxCustomerBindInfo.BindStatusEnum.NO);
                super.save(bindInfo);
            } else {
                createInfo += "第" + i + "行的手机号已经存在\r\n";
                continue;
                //YxCustomerBindInfo bindInfo = list.get(0);
                //VipCustomerProfile vipCustomerProfile = bindInfo.getCustomerProfile().getVipCustomerProfile();
                //VipLevel vipLevel = vipCustomerProfile.getVipLevel();
                //if (!"XE".equals(vipLevel.getCode())) {
                //   vipCustomerProfile.setVipLevel(vipLevelService.findOne(Long.valueOf(5)));
                //   vipCustomerProfileService.save(vipCustomerProfile);
                //}
            }
        }
        return createInfo;
    }

}
