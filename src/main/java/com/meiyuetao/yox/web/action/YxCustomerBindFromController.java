package com.meiyuetao.yox.web.action;

import com.meiyuetao.yox.entity.YxCustomerBindInfo;
import com.meiyuetao.yox.service.YxCustomerBindInfoService;
import lab.s2jh.core.annotation.MetaData;
import lab.s2jh.core.pagination.GroupPropertyFilter;
import lab.s2jh.core.pagination.PropertyFilter;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@MetaData("优享一百用户微信绑定管理")
public class YxCustomerBindFromController extends YxCustomerBindInfoController {

    @Autowired
    private YxCustomerBindInfoService yxCustomerBindInfoService;

    @MetaData("检查是否是种子用户")
    public HttpHeaders check() {
        String mobile = super.getParameter("mobile");
        GroupPropertyFilter groupPropertyFilter = GroupPropertyFilter.buildDefaultAndGroupFilter();
        groupPropertyFilter.forceAnd(new PropertyFilter(PropertyFilter.MatchType.EQ, "mobile", mobile));
        groupPropertyFilter.forceAnd(new PropertyFilter(PropertyFilter.MatchType.EQ, "customerProfile.customerFrom", "ZHONGZI"));
        List<YxCustomerBindInfo> list = yxCustomerBindInfoService.findByFilters(groupPropertyFilter);
        setModel(list);
        return buildDefaultHttpHeaders();
    }
}