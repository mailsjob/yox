package com.meiyuetao.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * Created by Naka on 2015/10/20.
 * 由于JFinal-weixin的sdk中并没有提供操作素材文件的相关方法，故手动添加
 * https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID
 */
public class MediaApi {
    private static String GET_MEDIA_RUL = "https://file.api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";

    /**
     * 获取临时素材（https）
     * 视频文件不支持https下载
     *
     * @param mediaId 媒体文件ID
     * @return 文件流
     */
    public static InputStream getMedia(String accessToken, String mediaId) {
        HttpURLConnection post = HttpsUtil.post(GET_MEDIA_RUL.replace("ACCESS_TOKEN", accessToken).replace("MEDIA_ID", mediaId), null);
        if (post != null) {
            System.out.println(post.getContentType());
            try {
                return post.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
