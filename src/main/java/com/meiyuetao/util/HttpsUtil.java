package com.meiyuetao.util;

import javax.net.ssl.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Naka on 2015/10/21.
 */
public class HttpsUtil {
    private static final SSLSocketFactory SSL_SOCKET_FACTORY = initSSLSocketFactory();
    private static final HostnameVerifier HOSTNAME_VERIFIER = new HostnameVerifier() {
        public boolean verify(String s, SSLSession sslSession) {
            return true;
        }
    };

    private HttpsUtil() {
    }

    public static HttpURLConnection get(String url, Map<String, String> headers) {
        HttpURLConnection get = getHttpConnection(url, "GET", headers);
        try {
            if (get != null) {
                get.connect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return get;
    }

    public static HttpURLConnection post(String url, Map<String, String> headers) {
        HttpURLConnection post = getHttpConnection(url, "POST", headers);
        try {
            if (post != null) {
                post.connect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return post;
    }

    private static HttpURLConnection getHttpConnection(String url, String method, Map<String, String> headers) {
        try {
            URL _url = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) _url.openConnection();
            if (conn instanceof HttpsURLConnection) {
                ((HttpsURLConnection) conn).setSSLSocketFactory(SSL_SOCKET_FACTORY);
                ((HttpsURLConnection) conn).setHostnameVerifier(HOSTNAME_VERIFIER);
            }
            conn.setRequestMethod(method);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setConnectTimeout(19000);
            conn.setReadTimeout(19000);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
            if (headers != null && !headers.isEmpty()) {
                Iterator i$ = headers.entrySet().iterator();

                while (i$.hasNext()) {
                    Map.Entry entry = (Map.Entry) i$.next();
                    conn.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
                }
            }
            return conn;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static SSLSocketFactory initSSLSocketFactory() {
        try {
            TrustManager[] e = new TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            }};
            SSLContext sslContext = SSLContext.getInstance("TLS", "SunJSSE");
            sslContext.init(null, e, new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception var2) {
            throw new RuntimeException(var2);
        }
    }
}
